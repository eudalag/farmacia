﻿Imports System.Globalization
Imports System.Data.SqlClient
Public Class paginaPrincipal
    Inherits System.Web.UI.Page
    Public Sub inicio(ByVal cc As ControlCollection, ByVal titulo As String, ByVal id_menu As Integer)
        If Session("IDUSUARIO") = Nothing Then
            Response.Redirect("~/home.aspx")
        Else
            Dim permisos = tienePermiso(id_menu)
            If Not (permisos Is Nothing) Then
                procesarPermisos(cc, permisos, titulo)
            Else
                Response.Redirect("~/home.aspx")
            End If
        End If
    End Sub
    Public Function tienePermiso(ByVal id_menu As Integer) As String()
        Dim permisos() As String = New String(3) {}
        Dim ID_MODULO = id_menu
        Dim id_udsuario = getSesion("IDUSUARIO")
        Dim roles_usuario = obtenerRolUsuario(id_udsuario, ID_MODULO)
        If (roles_usuario.Rows.Count <> 0) Then
            permisos(0) = roles_usuario.Rows(0)("ACCESO")
            permisos(1) = roles_usuario.Rows(0)("ALTA")
            permisos(2) = roles_usuario.Rows(0)("ELIMINACION")
            permisos(3) = roles_usuario.Rows(0)("MODIFICACION")
        Else
            permisos = Nothing
        End If
        Return permisos
    End Function
    Public Function obtenerRolUsuario(ByVal IDUSUARIO, ByVal ID_MODULO) As DataTable
        Dim SQL = "SELECT USUARIO.ID_USUARIO,ROLES.ID_ROL,ROLES.DESC_ROL,MODULOS.ID_MODULO as MenuID,MODULOS.ID_MODULO_PADRE as ParentID,MODULOS.DESC_MODULO as Text," & _
            " MODULOS.URL as NavigateUrl,ROLES_MODULO.ACCESO, ROLES_MODULO.ALTA, ROLES_MODULO.ELIMINACION, ROLES_MODULO.MODIFICACION  FROM USUARIO " & _
            " INNER JOIN PERMISOS ON PERMISOS.ID_USUARIO =USUARIO.ID_USUARIO" & _
            " INNER JOIN ROLES ON ROLES.ID_ROL = PERMISOS.ID_ROL " & _
            " INNER JOIN ROLES_MODULO ON ROLES_MODULO.ID_ROL = ROLES.ID_ROL " & _
            " INNER JOIN MODULOS ON MODULOS.ID_MODULO = ROLES_MODULO.ID_MODULO" & _
            " WHERE USUARIO.ID_USUARIO='" & getSesion("IDUSUARIO") & "' AND ROLES_MODULO.ID_MODULO=" & ID_MODULO
        Return New mcTabla().ejecutarConsulta(SQL)
    End Function
    Protected Overrides Sub InitializeCulture()
        Dim culturaPersonal As CultureInfo = New CultureInfo("es-ES")
        culturaPersonal.DateTimeFormat.DateSeparator = "/"
        culturaPersonal.DateTimeFormat.ShortDatePattern = "dddd, dd MM yyyy"
        culturaPersonal.DateTimeFormat.LongDatePattern = "dddd, dd MM yyyy"
        culturaPersonal.NumberFormat.NumberDecimalDigits = 2
        culturaPersonal.NumberFormat.NumberDecimalSeparator = "."
        culturaPersonal.NumberFormat.NumberGroupSeparator = ","
        System.Threading.Thread.CurrentThread.CurrentCulture = culturaPersonal
    End Sub
    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim objErr As Exception = Server.GetLastError().GetBaseException()
        Dim err As String = "<h2>Un Error a Ocurrido</h2><hr><br>" & _
            "<br><b>Error en: </b>" & Request.Url.ToString() & _
            "<br><b>Error Mensaje: </b>" & objErr.Message.ToString() & _
            "<br><b>Error Detalle:</b><br>" & _
            objErr.StackTrace.ToString() & "<br/><hr>" & _
            "<ul><li><a href='/Forms/Sistema.aspx'>Retornar a la Pagina Principal</a></li></ul>"
        Response.Write(err.ToString())
        Server.ClearError()
    End Sub
   Public Sub procesarPermisos(ByVal cc As ControlCollection, ByVal Permisos As String(), ByVal titulo As String)
        Try
            Dim Acceso As Boolean = Permisos(0)
            Dim Alta As Boolean = Permisos(1)
            Dim Eliminacion As Boolean = Permisos(2)
            Dim Modificacion As Boolean = Permisos(3)
            For Each c As Control In cc
                If (TypeOf c Is WebControl) Then
                    Dim ctl = CType(c, WebControl)
                    Dim accion = ctl.ID.Split("_")(0)
                    If (Acceso) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = "hidden"
                            Case "e" ' eliminacion
                                ctl.Enabled = False
                                ctl.Attributes.Add("OnClick", "return false;")
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Attributes("OnClick") = "return false;"
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = False
                                End If
                        End Select
                    End If
                    If (Eliminacion) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = "hidden"
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return confirm('¿ Esta seguro que desea eliminar ?')")
                                ctl.Enabled = True
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Attributes("OnClick") = "return false;"
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = False
                                End If
                        End Select
                    End If
                    If (Modificacion) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = "hidden"
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return false;")
                                ctl.Enabled = False
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Attributes("OnClick") = Nothing
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = True
                                End If
                        End Select
                    End If
                    If (Alta) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = Nothing
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return false;")
                                ctl.Enabled = False
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Attributes("OnClick") = "return false;"
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = True
                                End If
                        End Select
                    End If
                    If (Alta And Eliminacion) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = Nothing
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return confirm('¿ Esta seguro que desea eliminar ?')")
                                ctl.Enabled = True
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Style("visibility") = "return false;"
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = True
                                End If
                        End Select
                    End If
                    If (Alta And Acceso And Modificacion) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = Nothing
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return false;")
                                ctl.Enabled = False
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Attributes("OnClick") = Nothing
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = True
                                End If
                        End Select
                    End If
                    If (Alta And Acceso And Modificacion And Eliminacion) Then
                        Select Case accion
                            Case "a" 'alta
                                ctl.Style("visibility") = Nothing
                            Case "e" ' eliminacion
                                ctl.Attributes.Add("OnClick", "return confirm('¿ Esta seguro que desea eliminar ?')")
                                ctl.Enabled = True
                            Case "m" ' modificacion
                                'ctl.Enabled = False
                                ctl.Style("visibility") = Nothing
                            Case Else '  textbox
                                If (TypeOf ctl Is TextBox) Or (TypeOf ctl Is CheckBox) Then
                                    ctl.Enabled = True
                                End If
                        End Select
                    End If
                End If
                If (c.HasControls() Or TypeOf c Is ListView) Then
                    procesarPermisos(c.Controls, Permisos, titulo)
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub
    Public Function getSesion(ByVal valor) As String
        If Session(valor) Is Nothing Then
            Response.Redirect("~/home.aspx")
            Return Nothing
        Else
            Return Session(valor)
        End If
    End Function
    Public Function getRequest(ByVal valor) As String
        If Request.QueryString(valor) Is Nothing Then
            Response.Redirect("~/home.aspx")
            Return Nothing
        Else
            Return Request.QueryString(valor)
        End If
    End Function
    Public Sub Mensaje(ByVal sMessage As String, ByVal titulo As String)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "msg", "alert('" & sMessage & "','" & titulo & "');", True)
    End Sub
    Private Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If (Request.ServerVariables("http_user_agent").IndexOf("Safari", StringComparison.CurrentCultureIgnoreCase) <> -1) Then
            Page.ClientTarget = "uplevel"
        End If
    End Sub    
    Function convierteFecha(ByVal sFECHA As String) As String
        Return (Date.Parse(sFECHA, New System.Globalization.CultureInfo("es-ES")).ToString("yyyyMMdd"))
    End Function
    Public Sub abreVentana(ByVal ventana As String)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ventana", "window.open('" & ventana & "');", True)
    End Sub
    Protected Sub Redireccionar(ByVal url As String)
        'If Request.QueryString("idMenu") Is Nothing Then
        '    Response.Redirect("~/home.aspx")
        'Else
        Response.Redirect(url)
        ' End If
    End Sub
    Function NuevoID(ByVal Tabla As String, ByVal Campo As String) As String
        Dim sSql = "SELECT ISNULL(MAX(" & Campo & "),0) + 1 AS NUEVO_ID FROM " & Tabla
        Dim dt As DataTable = New mcTabla().ejecutarConsulta(sSql)
        Return dt(0)("NUEVO_ID")
    End Function
    Protected Sub ordenarTabla(ByVal control As Object, ByVal evento As System.Web.UI.WebControls.ListViewSortEventArgs)
        Dim cabeceraTabla = CType(control, Control).FindControl("cabeceraTabla")
        For Each sortCelda As HtmlControl In cabeceraTabla.Controls.Cast(Of HtmlControl)().Where(Function(th) th.Controls.OfType(Of IButtonControl).Any)
            Dim sortBTN As IButtonControl = sortCelda.Controls.OfType(Of IButtonControl).Single()
            Dim sortIMG As Image = sortCelda.Controls.OfType(Of Image).Single()
            If (sortBTN.CommandArgument = evento.SortExpression) Then
                sortIMG.ImageUrl = IIf(evento.SortDirection = SortDirection.Ascending, "~/App_Themes/estandar/imagenes/ascendente.png", "~/App_Themes/estandar/imagenes/descendente.png")
                sortIMG.Visible = True
            Else
                If (sortIMG.Visible <> False) Then sortIMG.Visible = False
            End If
        Next
    End Sub
End Class

