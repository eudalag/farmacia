﻿Imports System.Web
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.IO

Public Class mostarImagen
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim tipo = context.Request.QueryString("TIPO")
        Select Case tipo
            Case "IMAGENES"
                Dim row As Int32
                If Not context.Request.QueryString("id") Is Nothing Then
                    row = Convert.ToInt32(context.Request.QueryString("id"))
                Else
                    Throw New ArgumentException("No parameter specified")
                End If
                context.Response.ContentType = "Image/png"
                Dim img = gestionProductos.dt_imagenes.Rows(row)("DATO_MINIATURA")
                Dim strm As Stream = Nothing
                Try
                    strm = New MemoryStream(CType(img, Byte()))
                    If Not strm Is Nothing Then
                        Dim buffer As Byte() = New Byte(4095) {}
                        Dim byteSeq As Integer = strm.Read(buffer, 0, 4096)
                        Do While byteSeq > 0
                            context.Response.OutputStream.Write(buffer, 0, byteSeq)
                            byteSeq = strm.Read(buffer, 0, 4096)
                        Loop
                    Else
                    End If
                Catch

                End Try
            Case "THUMBNAIL"
                Dim DATOS As New dataCappiasDataContext
                Dim IDIMAGEN = Convert.ToInt32(context.Request.QueryString("IDIMAGEN"))
                Dim IMAGEN = DATOS.IMAGENES.SingleOrDefault(Function(P) P.IDIMAGEN = IDIMAGEN)
                Dim strm As Stream = Nothing
                Try
                    strm = New MemoryStream(IMAGEN.DATO_GRANDE.ToArray)
                    If Not strm Is Nothing Then
                        Dim buffer As Byte() = New Byte(4095) {}
                        Dim byteSeq As Integer = strm.Read(buffer, 0, 4096)
                        Do While byteSeq > 0
                            context.Response.OutputStream.Write(buffer, 0, byteSeq)
                            byteSeq = strm.Read(buffer, 0, 4096)
                        Loop
                    Else
                    End If
                Catch

                End Try
        End Select

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class