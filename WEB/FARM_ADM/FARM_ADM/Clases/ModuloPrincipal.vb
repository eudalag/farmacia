﻿Imports System.Data.SqlClient
Module ModuloPrincipal
    Public Function SePuedeEliminar(ByVal sTablaEliminacion As String, ByVal sValor As String, ByRef sMensaje As String) As Boolean
        Using cnn As New SqlConnection(My.Settings.BDFarmConnectionString)
            Dim sSql As String
            sMensaje = ""
            sSql = "Select * FROM CONTROL_ELIMINACION WHERE TABLA_ELIMINACION ='" & sTablaEliminacion & "'"
            Dim cmdConsulta As New SqlCommand(sSql, cnn)
            cmdConsulta.Connection = cnn
            cnn.Open()
            cmdConsulta.ExecuteNonQuery()
            Dim Tabla As New DataTable
            Dim da As New SqlDataAdapter(cmdConsulta)
            da.Fill(Tabla)
            If Tabla.Rows.Count > 0 Then
                For Each fila In Tabla.Rows
                    sSql = "SELECT * FROM " & fila("TABLA_REFERENCIA").ToString & " WHERE " & fila("CAMPO_BUSCAR").ToString & " = '" & sValor & "'"
                    Dim cmdConsulta2 As New SqlCommand(sSql, cnn)
                    cmdConsulta2.Connection = cnn
                    cmdConsulta2.ExecuteNonQuery()
                    Dim tabla2 As New DataTable
                    Dim da2 As New SqlDataAdapter(cmdConsulta2)
                    da2.Fill(tabla2)
                    If tabla2.Rows.Count > 0 Then
                        For Each registro In tabla2.Rows
                            sMensaje = fila("TEXTO_EXPLICACION")
                            Return False
                        Next
                    End If
                Next
            End If
        End Using
		Return True
    End Function
    Public Function PuedeHacer(ByVal iId_usuario As Integer, ByVal iCOd_Modulo As String, ByVal sACCESO As String, ByRef sMensaje As String) As Boolean
        'Comprobamos si el nombre ya existe
        Using cnn As New SqlConnection(My.Settings.BDFarmConnectionString)
            Dim sSql As String
            sSql = "Select APELLIDO_NOMBRES, DESC_MODULO, " & sACCESO & " AS ACCESO FROM VW_PERMISOS Where ID_USUARIO =" & iId_usuario & " AND COD_MODULO=" & iCOd_Modulo & "  "
            Dim cmdConsulta As New SqlCommand(sSql, cnn)
            cmdConsulta.Connection = cnn
            'Abrimos la conexión
            cnn.Open()
            cmdConsulta.ExecuteNonQuery()
            'El resultado lo guardaremos en una tabla
            Dim tablaPERMISOS As New DataTable
            'Usaremos un DataAdapter para leer los datos
            Dim da As New SqlDataAdapter(cmdConsulta)
            'Llenamos la tabla con los datos leídos
            da.Fill(tablaPERMISOS)
            'recorremos la cantidad de cuotas para realizar los asientos en tango
            If tablaPERMISOS.Rows.Count > 0 Then
                'VERIFICAR EL INGRESO AL USUARIO
                sMensaje = "Sr. Usuario: " & tablaPERMISOS.Rows(0)("APELLIDO_NOMBRES").ToString & " usted no tiene permiso de " & sACCESO & " al módulo " & tablaPERMISOS.Rows(0)("DESC_MODULO").ToString
                If tablaPERMISOS.Rows(0)("ACCESO").ToString = True Then
                    Return True
                Else
                    sMensaje = "Sr. " & tablaPERMISOS.Rows(0)("APELLIDO_NOMBRES").ToString & " usted no tiene permiso de " & sACCESO & " al módulo " & tablaPERMISOS.Rows(0)("DESC_MODULO").ToString & "."
                    Return False
                End If
            Else
                'NO EXISTE EL REGISTRO EN PERMISOS
                sMensaje = "Sr. Usuario usted no tiene permiso de ingreso. Consulte con el administrador del sistema."
                Return False
            End If
        End Using
    End Function
    Function NuevoID(ByVal Tabla As String, ByVal Campo As String) As String
        Using cnn As New SqlConnection(My.Settings.BDFarmConnectionString)
            Dim sSql As String
            sSql = "SELECT MAX(" & Campo & ") FROM " & Tabla
            Dim cmdConsulta As New SqlCommand(sSql, cnn)
            cmdConsulta.Connection = cnn
            cnn.Open()
            cmdConsulta.ExecuteNonQuery()
            Dim Tabla1 As New DataTable
            Dim da As New SqlDataAdapter(cmdConsulta)
            da.Fill(Tabla1)
            Try
                Dim sFila = Tabla1.Rows.Item(0)
                Dim iCampo = CInt(sFila(0) + 1)
                Return Str(iCampo)
            Catch
                Return Str(1)
            End Try
        End Using
    End Function
    Function convierteFecha(ByVal sFECHA As String) As String
        Dim sDIA As String
        Dim sMES As String
        Dim sANIO As String
        sDIA = Left(sFECHA, 2)
        sMES = Mid(sFECHA, 4, 2)
        sANIO = Right(sFECHA, 4)
        Return (sANIO & sMES & sDIA)
    End Function
    Public Sub cerrarSesion()
        HttpContext.Current.Session.Clear()
        'HttpContext.Current.Response.Redirect("/home.aspx")
    End Sub
    Public Function esNulo(ByVal valor As Object, Optional retornar As Object = "") As Object
        If Not IsDBNull(valor) Then
            Return valor
        Else
            Return retornar
        End If
    End Function
End Module
