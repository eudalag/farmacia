﻿Public Class plan_cuentas
    Public Sub insertarMovimiento(ByVal IMPORTE As Decimal, ID_PLAN As Integer, ByVal detalle As String, ByVal referencia As Integer, ByVal fecha As String)
        Dim tbl = New mcTabla()
        tbl.tabla = "MOVIMIENTO_ECONOMICO"
        tbl.campoID = "ID_MOVIMIENTO_ECONOMICO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("IMPORTE", IMPORTE)
        tbl.agregarCampoValor("ID_PLAN_CUENTAS", ID_PLAN)
        tbl.agregarCampoValor("DETALLE", detalle)
        tbl.agregarCampoValor("REFERENCIA", referencia)
        tbl.agregarCampoValor("FECHA", fecha)
        tbl.insertar()
    End Sub
End Class
