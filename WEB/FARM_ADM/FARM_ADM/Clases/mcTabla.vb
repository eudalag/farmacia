﻿Imports System.Data.SqlClient
Imports System.Web
Imports System.Collections.Generic
Imports System.Data

Public Class mcTabla
    Structure sqlParametro
        Public parametro As String
        Public valor As Object
        Public tipoDato As System.Data.SqlDbType
    End Structure

    Private Response As Web.HttpResponse = Web.HttpContext.Current.Response
    Private cCnn As String
    Private cTabla As String
    Private cCampoID As String
    'Private cCampoValor As New Dictionary(Of String, String)
    Private cCampoValor As New Dictionary(Of String, sqlParametro)
    Private cCampos As String
    'Private cValores As String
    Private cValores As List(Of Object)
    Private cTipoDatos As List(Of System.Data.SqlDbType)
    Private cModoID As String 'Valores posibles "SQL" "Auto" "Manual"
    Private cValorID As String
    Private cOrderBy As String
    Private cMensajesError As New Collection
    Private cParametros As String

    Private Class Validacion
        Public tabla As String
        Public campo As String
        Public valor As String
        Public mensajeError As String
        Public campoError As String
    End Class
    Private cValidaciones As New Collection
    Private cnn As New SqlConnection()
    Private dr As SqlDataReader
    Public Sub New()
        Try
            Dim cadena As String = My.Settings.BDFarmConnectionString
            cnn = New SqlConnection(cadena)
            cCnn = cadena
        Catch
            cCnn = ""
        End Try
        cCampoID = "ID"
        cModoID = "sql"
    End Sub
    Public Sub New(ByVal conexion)
        cCnn = conexion
        cnn = New SqlConnection(cCnn)
        cCampoID = "ID"
        cModoID = "sql"
    End Sub
    Public Property tabla() As String
        Get
            tabla = cTabla
        End Get
        Set(ByVal value As String)
            cTabla = value
        End Set
    End Property
    Public Property campoID() As String
        Get
            campoID = cCampoID
        End Get
        Set(ByVal value As String)
            cCampoID = value
        End Set
    End Property
    Public Property modoID() As String
        Get
            modoID = cModoID
        End Get
        Set(ByVal value As String)
            cModoID = LCase(value)
        End Set
    End Property
    Public Property conexion() As String
        Get
            conexion = cCnn
        End Get
        Set(ByVal value As String)
            cCnn = value
            cnn.ConnectionString = cCnn
        End Set
    End Property
    Public Property valorID() As String
        Get
            valorID = cValorID
        End Get
        Set(ByVal value As String)
            cValorID = value
        End Set
    End Property
    Public Property orderBy() As String
        Get
            orderBy = cOrderBy
        End Get
        Set(ByVal value As String)
            cOrderBy = value
        End Set
    End Property
    Public ReadOnly Property mensajesError() As Collection
        Get
            mensajesError = cMensajesError
        End Get
    End Property

    'Public Sub agregarCampoValor(ByVal campo, ByVal valor)
    '    cCampoValor.Add(campo, valor)
    'End Sub
    Public Sub agregarCampoValor(ByVal campo As String, ByVal valor As Object, Optional ByVal tipoDato As System.Data.SqlDbType = SqlDbType.Image)
        Dim campoParametro As sqlParametro = Nothing
        campoParametro.valor = valor
        campoParametro.tipoDato = tipoDato
        cCampoValor.Add(campo, campoParametro)
    End Sub
    Public Sub agregarValidacion(ByVal tabla, ByVal campo, ByVal valor, ByVal mensaje, ByVal campoError)
        Dim verificacion As New Validacion
        verificacion.tabla = tabla
        verificacion.campo = campo
        verificacion.valor = valor
        verificacion.mensajeError = mensaje
        verificacion.campoError = campoError
        cValidaciones.Add(verificacion)
    End Sub

    Public Sub reset()
        cCampoValor.Clear()
        valorID = ""
        tabla = ""
        orderBy = ""
    End Sub
    Public Sub cerrarConexion()
        cnn.Close()
    End Sub

    Private Function obtenerCampos() As String
        Dim campos As String
        campos = ""
        For Each campo In cCampoValor
            campos = campos & campo.Key & ", "
        Next
        campos = Left(campos, Len(campos) - 2)
        Return campos
    End Function

    'Private Function obtenerValores() As String
    '    Dim valores As String
    '    valores = ""
    '    For Each valor In cCampoValor
    '        If valor.Value = "serverDateTime" Then
    '            valores = valores & "GETDATE(), "
    '        Else
    '            valores = valores & "'" & valor.Value & "', "
    '        End If
    '    Next
    '    valores = Left(valores, Len(valores) - 2)
    '    Return valores
    'End Function
    Private Function obtenerValores() As List(Of Object)
        Dim valores As New List(Of Object)
        For Each valor In cCampoValor
            Try
                If CStr(valor.Value.valor) = "serverDateTime" Then
                    valores.Add(ejecutarConsulta("select GETDATE() as dateTimeServer").Rows(0)("dateTimeServer"))
                Else
                    valores.Add(valor.Value.valor)
                End If
            Catch ex As Exception
                valores.Add(valor.Value.valor)
            End Try
        Next
        Return valores
    End Function
    Private Function obtenerTipoDatos() As List(Of System.Data.SqlDbType)
        Dim tipoDatos As New List(Of System.Data.SqlDbType)
        For Each valor In cCampoValor
            tipoDatos.Add(valor.Value.tipoDato)
        Next
        Return tipoDatos
    End Function
    Private Function obtenerParametro() As String
        Dim parametros As String
        parametros = ""
        For Each valor In cCampoValor
            parametros = parametros & "@" & valor.Key & ","
        Next
        parametros = Left(parametros, Len(parametros) - 1)
        Return parametros
    End Function

    'Private Function obtenerCampoValor() As String
    '    Dim valores As String
    '    valores = ""
    '    For Each valor In cCampoValor
    '        If valor.Value = " " Then
    '            valores = valores & valor.Key & "= null,"
    '        Else
    '            valores = valores & valor.Key & "= '" & valor.Value & "', "
    '        End If
    '    Next
    '    valores = Left(valores, Len(valores) - 2)
    '    Return valores
    'End Function
    Private Function obtenerCampoValor() As String
        Dim valores As String
        valores = ""
        For Each valor In cCampoValor
            valores = valores & valor.Key & "= @" & valor.Key & ","
        Next
        valores = Left(valores, Len(valores) - 1)
        Return valores
    End Function

    Public Function obtenerNuevoID()
        Dim sSQL As String
        Dim nuevoID As Integer
        sSQL = "SELECT MAX(" & cCampoID & ") AS NUEVO_ID FROM " & cTabla
        Dim cmd As New SqlCommand(sSQL, cnn)
        cnn.Open()
        dr = cmd.ExecuteReader()
        If dr.HasRows Then
            Try
                dr.Read()
                nuevoID = CDbl(dr(0)) + 1
            Catch e As Exception
                nuevoID = 1
            End Try
        Else
            nuevoID = 1
        End If
        obtenerNuevoID = nuevoID
        dr.Close()
        cnn.Close()
    End Function

    Public Function obtenerReader() As SqlDataReader
        Dim sSQL As String

        sSQL = "SELECT * FROM " & cTabla
        If cOrderBy <> "" Then
            sSQL = sSQL & " ORDER BY " & cOrderBy
        End If

        Dim cmd As New SqlCommand(sSQL, cnn)
        Dim dr As SqlDataReader
        cnn.Open()
        dr = cmd.ExecuteReader()
        obtenerReader = dr
    End Function

    Public Function obtenerTabla() As DataTable
        Dim sSQL As String
        sSQL = "SELECT * FROM " & cTabla

        sSQL = "SELECT * FROM " & cTabla
        If cOrderBy <> "" Then
            sSQL = sSQL & " ORDER BY " & cOrderBy
        End If

        Dim cmd As New SqlCommand(sSQL, cnn)
        Dim da As New SqlDataAdapter(cmd)
        Dim ta As New DataTable
        cnn.Open()
        cmd.ExecuteNonQuery()
        da.Fill(ta)
        obtenerTabla = ta
        cnn.Close()
    End Function

    'Public Function obtenerRegistro() As SqlDataReader
    '    Dim sSQL As String
    '    sSQL = "SELECT * FROM " & cTabla & " WHERE " & cCampoID & " LIKE '" & cValorID & "'"
    '    Dim cmd As New SqlCommand(sSQL, cnn)
    '    Dim dr As SqlDataReader
    '    cnn.Open()
    '    dr = cmd.ExecuteReader()
    '    obtenerRegistro = dr
    'End Function

    Public Function obtenerRegistro() As SqlDataReader
        Dim sSQL As String
        sSQL = "SELECT * FROM " & cTabla & " WHERE " & cCampoID & " LIKE @ID"
        Dim cmd As New SqlCommand(sSQL, cnn)
        cmd.Parameters.Add("@ID", SqlDbType.VarChar)
        cmd.Parameters("@ID").Value = cValorID
        Dim dr As SqlDataReader
        cnn.Open()
        dr = cmd.ExecuteReader()
        obtenerRegistro = dr
    End Function

    Public Function ejecutarConsulta(ByVal sConsulta) As DataTable
        Dim sSQL As String
        sSQL = sConsulta
        Dim cmd As New SqlCommand(sSQL, cnn)
        Dim da As New SqlDataAdapter(cmd)
        Dim ta As New DataTable
        cnn.Open()
        cmd.CommandTimeout = 1200
        cmd.ExecuteNonQuery()
        da.Fill(ta)
        ejecutarConsulta = ta
        cnn.Close()
    End Function
    Public Sub ejecutarSQL(ByVal sConsulta As String)
        Dim cmd As New SqlCommand(sConsulta, cnn)
        Dim da As New SqlDataAdapter(cmd)
        cnn.Open()
        cmd.ExecuteNonQuery()
        cnn.Close()
    End Sub
    Public Function obtenerDatos(ByVal sSql As String) As DataTable
        Dim tbl As DataTable = New DataTable()
        sSql += " WHERE " & cCampoID & " LIKE @ID"
        If cOrderBy <> "" Then
            sSql = sSql & " ORDER BY " & cOrderBy
        End If
        Dim cmd As New SqlCommand(sSql, cnn)
        cmd.Parameters.Add("@ID", SqlDbType.VarChar)
        cmd.Parameters("@ID").Value = cValorID
        Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
        cnn.Open()
        cmd.ExecuteNonQuery()
        da.Fill(tbl)
        cnn.Close()
        Return tbl
    End Function
    Public Function obtenerDataSet(ByVal sConsulta As String) As DataSet
        Dim sSQL As String
        sSQL = sConsulta
        Dim cmd As New SqlCommand(sSQL, cnn)
        'cmd.CommandTimeout = 0
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet
        cnn.Open()
        cmd.CommandTimeout = 1200
        cmd.ExecuteNonQuery()
        da.Fill(ds)
        cnn.Close()
        Return ds
    End Function

    'Public Sub insertar()
    '    Dim sID As String
    '    Dim sSQL As String
    '    cCampos = obtenerCampos()
    '    cValores = obtenerValores()
    '    sID = ""
    '    If cValorID <> "" Then
    '        sID = cValorID
    '    Else
    '        Select Case modoID
    '            Case "sql"
    '                sID = ""
    '            Case "auto"
    '                sID = obtenerNuevoID()
    '                cValorID = sID
    '            Case "manual"
    '                sID = cValorID
    '        End Select
    '    End If
    '    sID = "'" & sID & "', "

    '    If sID = "'', " Then
    '        sSQL = "INSERT INTO " & cTabla & " ( " & cCampos & ") VALUES (" & cValores & ")"
    '    Else
    '        sSQL = "INSERT INTO " & cTabla & " ( " & campoID & ", " & cCampos & ") VALUES (" & sID & cValores & ")"
    '    End If
    '    Dim cmd As New SqlCommand(sSQL, cnn)
    '    cnn.Open()
    '    cmd.ExecuteNonQuery()
    '    If modoID = "sql" Then
    '        sSQL = "SELECT SCOPE_IDENTITY() AS ID"
    '        Dim dr As SqlDataReader
    '        Dim cmd2 As New SqlCommand(sSQL, cnn)
    '        dr = cmd2.ExecuteReader
    '        If dr.HasRows Then
    '            dr.Read()
    '            cValorID = dr(0).ToString
    '        End If
    '    End If
    '    cnn.Close()
    'End Sub

    Public Sub insertar()
        Dim sID As String
        Dim sSQL As String
        cCampos = obtenerCampos()
        cValores = obtenerValores()
        cTipoDatos = obtenerTipoDatos()
        cParametros = obtenerParametro()
        sID = ""
        If cValorID <> "" Then
            sID = cValorID
        Else
            Select Case modoID
                Case "sql"
                    sID = ""
                Case "auto"
                    sID = obtenerNuevoID()
                    cValorID = sID
                Case "manual"
                    sID = cValorID
            End Select
        End If
        'sID = "'" & sID & "', "

        If sID = "" Then
            sSQL = "INSERT INTO " & cTabla & " ( " & cCampos & ") VALUES (" & cParametros & ")"
        Else
            sSQL = "INSERT INTO " & cTabla & " ( " & campoID & ", " & cCampos & ") VALUES (@sID, " & cParametros & ")"
        End If
        If modoID = "sql" Then
            sSQL += " SELECT SCOPE_IDENTITY() AS ID"
        End If
        Dim cmd As New SqlCommand(sSQL, cnn)
        Dim Parametros() As String = cParametros.Split(",")
        Dim pos As Integer = 0
        If sID <> "" Then
            cmd.Parameters.Add(New SqlParameter("@sID", sID))
        End If
        For Each valor In cValores
            Dim insertParametro As SqlParameter
            Dim tipo = valor.GetType
            If tipo.Name = "DBNull" Then
                insertParametro = New SqlParameter(Parametros(pos), cTipoDatos(pos))
                insertParametro.Value = DBNull.Value
            Else
                Try
                    insertParametro = New SqlParameter(Parametros(pos), IIf(LCase(CStr(valor)) = "null", DBNull.Value, valor))
                Catch ex As Exception
                    insertParametro = New SqlParameter(Parametros(pos), valor)
                End Try
            End If
            cmd.Parameters.Add(insertParametro)
            pos = pos + 1
        Next
        cnn.Open()
        If modoID = "sql" Then
            Dim leerFila = cmd.ExecuteReader
            leerFila.Read()
            cValorID = Integer.Parse(leerFila("ID"))
            leerFila.Close()
        Else
            cmd.ExecuteNonQuery()
        End If
        cnn.Close()
    End Sub

    'Public Sub modificar()
    '    Dim sSQL As String
    '    Dim campoValor As String
    '    campoValor = obtenerCampoValor()
    '    sSQL = "UPDATE " & cTabla & " SET  " & campoValor & " WHERE " & campoID & " LIKE '" & cValorID & "'"
    '    Dim cmd As New SqlCommand(sSQL, cnn)
    '    cnn.Open()
    '    cmd.ExecuteNonQuery()
    '    cnn.Close()
    'End Sub
    Public Sub modificar()
        Dim sSQL As String
        Dim campoValor As String
        campoValor = obtenerCampoValor()
        cValores = obtenerValores()
        cTipoDatos = obtenerTipoDatos()
        cParametros = obtenerParametro()
        sSQL = "UPDATE " & cTabla & " SET  " & campoValor & " WHERE " & campoID & " LIKE @cValorID "
        Dim cmd As New SqlCommand(sSQL, cnn)
        cmd.Parameters.Add(New SqlParameter("@cValorID", cValorID))
        Dim Parametros() As String = cParametros.Split(",")
        Dim pos As Integer = 0
        For Each valor In cValores
            Dim insertParametro As SqlParameter
            Dim tipo = valor.GetType
            If tipo.Name = "DBNull" Then
                insertParametro = New SqlParameter(Parametros(pos), cTipoDatos(pos))
                insertParametro.Value = DBNull.Value
            Else
                Try
                    insertParametro = New SqlParameter(Parametros(pos), IIf(LCase(CStr(valor)) = "null", DBNull.Value, valor))
                Catch ex As Exception
                    insertParametro = New SqlParameter(Parametros(pos), valor)
                End Try
            End If
            cmd.Parameters.Add(insertParametro)
            pos = pos + 1
        Next
        cnn.Open()
        cmd.ExecuteNonQuery()
        cnn.Close()
    End Sub

    'Public Function puedeEliminar() As Boolean
    '    Dim sSQL As String
    '    puedeEliminar = True
    '    Dim val As New Validacion
    '    Dim mensaje As String
    '    cnn.Open()
    '    For Each elemento In cValidaciones
    '        sSQL = ""
    '        val = elemento
    '        If val.valor = "" Then val.valor = valorID
    '        sSQL = "SELECT " & val.campoError & " AS VAL FROM " & val.tabla & " "
    '        sSQL = sSQL & "WHERE " & val.campo & " LIKE '" & val.valor & "'"
    '        Using cmd As New SqlCommand(sSQL, cnn)
    '            Dim dr As SqlDataReader
    '            dr = cmd.ExecuteReader()
    '            If dr.HasRows Then
    '                dr.Read()
    '                mensaje = val.mensajeError & " " & dr("VAL")
    '                cMensajesError.Add(mensaje)
    '                puedeEliminar = False
    '            End If
    '            dr.Close()
    '        End Using
    '    Next
    '    cnn.Close()
    'End Function
    Public Function puedeEliminar() As Boolean
        Dim sSQL As String
        puedeEliminar = True
        Dim val As New Validacion
        Dim mensaje As String
        cnn.Open()
        For Each elemento In cValidaciones
            sSQL = ""
            val = elemento
            sSQL = "SELECT " & val.campoError & " AS VAL FROM " & val.tabla
            sSQL = sSQL & " WHERE " & val.campo & " LIKE @valor "
            Using cmd As New SqlCommand(sSQL, cnn)
                cmd.Parameters.Add("@valor", SqlDbType.VarChar)
                cmd.Parameters("@valor").Value = val.valor
                Dim dr As SqlDataReader
                dr = cmd.ExecuteReader()
                If dr.HasRows Then
                    dr.Read()
                    mensaje = val.mensajeError & " " & dr("VAL")
                    cMensajesError.Add(mensaje)
                    puedeEliminar = False
                End If
                dr.Close()
            End Using
        Next
        cnn.Close()
    End Function

    'Public Sub eliminar()
    '    Dim sSQL As String
    '    sSQL = "DELETE FROM " & cTabla & " WHERE " & campoID & " LIKE '" & cValorID & "'"
    '    Dim cmd As New SqlCommand(sSQL, cnn)
    '    cnn.Open()
    '    cmd.ExecuteNonQuery()
    '    cnn.Close()
    'End Sub
    Public Sub eliminar()
        Dim sSQL As String
        'sSQL = "DELETE FROM " & cTabla & " WHERE " & campoID & " LIKE '" & cValorID & "'"
        sSQL = "DELETE FROM " & cTabla & " WHERE " & campoID & " LIKE @cValorID "
        Dim cmd As New SqlCommand(sSQL, cnn)
        cmd.Parameters.Add("@cValorID", SqlDbType.VarChar)
        cmd.Parameters("@cValorID").Value = cValorID
        cnn.Open()
        cmd.ExecuteNonQuery()
        cnn.Close()
    End Sub

End Class
