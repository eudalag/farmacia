﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" Inherits="FARM_ADM.login" StylesheetTheme="estilos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div>
         <asp:Panel runat="server" ID="panelLogin" DefaultButton="cmdAceptar"> 
 <div class="contenedor">
        <div id="cabecera"> 
            <div >
                <img alt="0" style="width:1024px; height:100px;" src="App_Themes/estandar/imagenes/banner_principal.jpg" />
            </div>
            <div>
                <img alt="0" src="App_Themes/estandar/imagenes/linea_inf_top.png" style="width:100%;" /> 
            </div>
        </div>
<div id="menu"> 
   </div>
   <div id="menubottom"></div>
     <div id="content"> 
       <div class="alinearIzq"> 
      <asp:Label ID="Label2" runat="server" Text="Ingreso al Sistema:" CssClass="tituloGris"> 
      </asp:Label> 
      <hr align="left" style="height:-12px;color:#CCCCCC" width="30%" /> 
      <br /> <br /> 
    </div>
   <table style="width: 100%;">
    <tr>
      <td> &nbsp;</td>
      <td> 
        <asp:Label ID="lblUsuario" runat="server" Text="Usuario:"></asp:Label> 
      </td>
      <td colspan="2"> 
        <asp:TextBox ID="txtUsuario" runat="server" class="textoGris" Text="" MaxLength="20"></asp:TextBox> 
        <asp:RequiredFieldValidator ID="rfvusuario" runat="server"  CssClass="textoRojo" ControlToValidate="txtUsuario" ErrorMessage=" * Especifique un Nombre de Usuario" Text="*"> 
        </asp:RequiredFieldValidator> 
      </td>
    </tr>
    <tr>
      <td style="width:35%"> &nbsp;</td>
      <td  style="width:10%"> 
        <asp:Label ID="lblPassword" runat="server" Text="Contraseña:"></asp:Label> 
      </td>
      <td   style="width:17%"> 
        <asp:TextBox ID="txtPass" runat="server" AUTOCOMPLETE="off" Text="562151" TextMode="Password" MaxLength="20"></asp:TextBox> 
        <asp:RequiredFieldValidator ID="rfvpass" runat="server"  CssClass="textoRojo" ControlToValidate="txtPass" class="textoGris" ErrorMessage=" * Especifique una contraseña" Text="*"> 
        </asp:RequiredFieldValidator> 
        
      </td>
      <td> <asp:ImageButton ID="cmdAceptar" OnClick="verificarDatosUsuario" runat="server" ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png" /> </td>
    </tr>
    <tr>
      <td colspan="4">
      <br />
      </td>
    </tr>
    <tr>
    <td></td>
      <td colspan="3"> 
        <div> 
          <asp:ValidationSummary ID="vsIMC" runat="server" CssClass="textoRojo" 
                HeaderText="Por favor verifique la siguiente información:" 
                DisplayMode="List"  /> 
        </div>
      </td>
    </tr>
  </table>
       <asp:CustomValidator ID="cvUsuario" runat="server" Display=None ErrorMessage=" * Datos Incorrectos. Verifique Usuario y/o Contraseña"></asp:CustomValidator>
   </div>
   <div id="footer"> 
      <div class="left">&#169; Copyright 2018 / Todos los derechos reservados - georgecv25@gmail.com</div>
      <div class="right">Diseñado por Ing. Jorge Calvimontes Vargas</a></div>
   </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
      <ContentTemplate> 
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none"> 
          <table>
            <tr>
              <td class="alinearCentro" colspan="2" style="background-color:#031360"> 
                <b><asp:Label ID="Label1" CssClass="textoBlanco" runat="server" Text="Cambiar Contraseña"></asp:Label></b> 
              </td>
            </tr>
            <tr>
              <td><b> Contraseña Anterior:</b></td>
              <td> 
                <asp:TextBox ID="contrasenaAnterior" runat="server" ValidationGroup="edit" Width="150px" TextMode="Password"></asp:TextBox> 
                <asp:CustomValidator ID="CustomValidator1" Display="None" runat="server" ErrorMessage="Contraseña incorrecta" ValidationGroup="edit" ValidateEmptyText="True" ControlToValidate="contrasenaAnterior" ClientValidationFunction="verificarCa"></asp:CustomValidator> 
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="CustomValidator1" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender> 
              </td>
            </tr>
            <tr>
              <td><b> Nueva Contraseña:</b></td>
              <td><asp:TextBox ID="contrasenaNueva" runat="server" ValidationGroup="edit" Width="150px" TextMode="Password"></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" runat="server" ValidationGroup="edit" ControlToValidate="contrasenaNueva" ErrorMessage="Introduzca Contraseña"></asp:RequiredFieldValidator> 
                <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ErrorMessage="Contraseñas no coinciden" ControlToValidate="contrasenaNueva" ControlToCompare="repitaContrasena" ValidationGroup="edit"></asp:CompareValidator> 
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server" ValidationGroup="edit" ControlToValidate="contrasenaNueva" ValidationExpression="(?=^.{6,15}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$" ErrorMessage="La primera contraseña debe tener tener entre 6 y 15 caracteres entre(numeros, caracteres min. y may.)"></asp:RegularExpressionValidator> 
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="CompareValidator1" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender> 
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server" TargetControlID="RegularExpressionValidator2" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender> 
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server" TargetControlID="RequiredFieldValidator2" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender> 
              </td>
            </tr>
            <tr>
              <td><b> Repita nueva contraseña:</b></td>
              <td><asp:TextBox ID="repitaContrasena" runat="server" ValidationGroup="edit" Width="150px" TextMode="Password"></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="None" runat="server" ValidationGroup="edit" ControlToValidate="repitaContrasena" ErrorMessage="Introduzca contraseña"></asp:RequiredFieldValidator> 
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="edit" ControlToValidate="repitaContrasena" ValidationExpression="(?=^.{6,15}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$" ErrorMessage="La segunda contraseña debe tener tener entre 6 y 15 caracteres entre(numeros, caracteres min. y may.)"></asp:RegularExpressionValidator> 
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server" TargetControlID="RequiredFieldValidator3" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender>                   
                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server" TargetControlID="RegularExpressionValidator1" HighlightCssClass="fondoValidador"> 
                </asp:ValidatorCalloutExtender> 
              </td>
            </tr>
            <tr>
              <td class="alinearDer" colspan="2"><asp:ImageButton ID="btnAceptarCC" runat="server" ImageUrl="~/App_Themes/estandar/imagenes/botones/aceptar.png" ValidationGroup="edit" TabIndex="2" Height="20" /></td>
            </tr>
          </table>
        </asp:Panel> 
        <asp:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="Submit1" DropShadow="true" PopupControlID="Panel1" BackgroundCssClass="modalBackground" runat="server"> 
        </asp:ModalPopupExtender> 
        <input id="Submit1" runat="server" style="visibility:hidden" type="submit" /> 
        <input id="hdUsuario" runat="server" type="hidden" value="" /> 
      </ContentTemplate> 
    </asp:UpdatePanel> 
  </div>
</asp:Panel> 
    </div>
    </form>
</body>
</html>
