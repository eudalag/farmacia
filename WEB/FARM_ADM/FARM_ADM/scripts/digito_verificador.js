﻿function es_numero(n) {
    return !isNaN(n);
}
function cuit_correcto(cuit) {
    var nSuma;
    var nResto;
    var nDiferencia;

    if (cuit.length != 13)
        return (false);

    var nTipoEntidad1 = cuit.substring(0, 1);
    if (!es_numero(nTipoEntidad1))
        return (false);

    var nTipoEntidad2 = cuit.substring(1, 2);
    if (!es_numero(nTipoEntidad2))
        return (false);

    var nNumeroIdentificacion01 = cuit.substring(3, 4);
    if (!es_numero(nNumeroIdentificacion01))
        return (false);

    var nNumeroIdentificacion02 = cuit.substring(4, 5);
    if (!es_numero(nNumeroIdentificacion02))
        return (false);

    var nNumeroIdentificacion03 = cuit.substring(5, 6);
    if (!es_numero(nNumeroIdentificacion03))
        return (false);

    var nNumeroIdentificacion04 = cuit.substring(6, 7);
    if (!es_numero(nNumeroIdentificacion04))
        return (false);

    var nNumeroIdentificacion05 = cuit.substring(7, 8);
    if (!es_numero(nNumeroIdentificacion05))
        return (false);

    var nNumeroIdentificacion06 = cuit.substring(8, 9);
    if (!es_numero(nNumeroIdentificacion06))
        return (false);

    var nNumeroIdentificacion07 = cuit.substring(9, 10);
    if (!es_numero(nNumeroIdentificacion07))
        return (false);

    var nNumeroIdentificacion08 = cuit.substring(10, 11);
    if (!es_numero(nNumeroIdentificacion08))
        return (false);

    var nDigitoVerificador = cuit.substring(12, 13);
    if (!es_numero(nDigitoVerificador))
        return (false);

    nSuma = (nTipoEntidad1 * 5) + (nTipoEntidad2 * 4) + (nNumeroIdentificacion01 * 3) + (nNumeroIdentificacion02 * 2) + (nNumeroIdentificacion03 * 7) + (nNumeroIdentificacion04 * 6) + (nNumeroIdentificacion05 * 5) + (nNumeroIdentificacion06 * 4) + (nNumeroIdentificacion07 * 3) + (nNumeroIdentificacion08 * 2);
    nResto = nSuma % 11; //Resto.

    if (nResto == 0) {
        if (nDigitoVerificador == 0)
            return (true);
        else
            return (false);
    }
    else {
        nDiferencia = 11 - nResto

        if (nDiferencia == 10)
            nDiferencia = 9

        if (nDigitoVerificador == nDiferencia)
            return (true);
        else
            return (false);
    }

    return (true);
}