﻿var Mascara = new Class({
    Implements: Options,
    options: {
        esPunto: false,
        esNegativo: false,
        focus: 0,
        decimales: 2,
        fijarMascara: '##/##/####',
        esNumero: false
    },
    initialize: function(objeto, opciones) {
        this.objeto = $(objeto);
        this.setOptions(opciones);
        if (!this.options.esNumero)
            this.objeto.value = this.usarMascara(this.objeto.value, this.options.fijarMascara);
        else {
            this.objeto.value = this.usarMascara(this.objeto.value, this.generarMascara(this.objeto.value));
            this.objeto.setStyles({ "text-align": "right" });
        }
        this.objeto.addEvent("focus", function(event) {

            this.eventoFocus(this.objeto);
        } .bind(this));
        this.objeto.addEvent("keypress", function(event) {
            event = new Event(event);
            this.eventoKeyPress(event, this.objeto);
        } .bind(this));

        this.objeto.addEvent("keydown", function(event) {
            event = new Event(event);
            this.eventoKeydown(event, this.objeto);
        } .bind(this));

        this.objeto.addEvent("click", function(event) {
            this.ponerPosicion(this.objeto);
        } .bind(this));
        this.objeto.addEvent("mouseup", function(event) {
            event = new Event(event);
            this.MouseUp(event, this.objeto);
        } .bind(this));
    },
    //Metodos mascara
    eventoKeyPress: function(event, obj) {
        var h = event.code;
        var key = event.code;
        if (key == 0) key = event.keyCode;
        if (key == undefined) key = event.keyCode;
        var caracter = String.fromCharCode(key);
        var p = this.obtenerSelectionInicio(obj);
        switch (key) {
            case 8:
                if (!this.options.esNumero) {
                    this.seleccionarAnterior(obj, this.options.fijarMascara);
                    if (p > 0)
                        if ("!#&".indexOf(this.options.fijarMascara.charAt(p - 1)) >= 0)
                        this.anteriorSelection(obj, p - 1, '_');
                    else
                        this.anteriorSelection(obj, p - 2, '_');
                } else {
                    if (this.options.focus == 1) {
                        this.options.focus = 0;
                        if (this.options.decimales == 2)
                        { obj.value = "0.00"; }
                        else {
                            if (this.options.decimales == 0) {
                                obj.value = "0";
                            } else {                                                 
                                obj.value = "0.000";
                            } 
                        }
                        this.seleccionarhastaelPunto(obj);
                    }
                    if ((obj.value.length - p) <= this.options.decimales) {
                        if (obj.value.charAt(p - 1) == ".") {
                            var val = obj.value.substring(0, p - 2);
                            obj.value = this.FormatoNumero(val) + obj.value.substring(p - 1, obj.value.length);
                            if (p == 2) p += 1
                            this.fijarSeleccion(obj, p - 3, p - 2);
                        } else
                            this.anteriorSelection(obj, p - 1, '0');
                    } else
                        this.eliminarNumero("BACKSPACE", obj, p);
                }
                break;
            case 45:
                if (this.options.esNumero && this.options.esNegativo) {
                    if (obj.value.indexOf("-") < 0) {
                        obj.value = "-" + obj.value;
                        this.fijarSeleccion(obj, 1, 2);
                    }

                }
                break;
            case 9:
            case 13:
                return true;
                break;
            case 36: // inicio
                if (!this.options.esNumero) this.seleccionarelPrimero(obj, this.options.fijarMascara);
                else this.seleccionarelPrimero(obj, this.generarMascara(obj.value));
                break;
            case 35: // final
                if (!this.options.esNumero) this.seleccionarUltimo(obj, this.options.fijarMascara);
                else this.seleccionarUltimo(obj, this.generarMascara(obj.value));
                break;
            case 37: // Left
            case 38: // Up
                if (!this.options.esNumero) this.seleccionarAnterior(obj, this.options.fijarMascara);
                else {
                    this.seleccionarAnterior(obj, this.generarMascara(obj.value));
                    this.options.esPunto = false;
                }
                break;
            case 39: // Right
            case 40: // Down
                if (!this.options.esNumero) this.seleccionarSiguiente(obj, this.options.fijarMascara);
                else {
                    this.seleccionarSiguiente(obj, this.generarMascara(obj.value));
                    this.options.esPunto = true;
                }
                break;
            case 46: // Delete
                if (!this.options.esNumero)
                    this.anteriorSelection(obj, p, '_');
                /* else
                if ((obj.value.length - p) <= 4)
                anteriorSelection(obj, p, '0');*/
                break;
            default: if (!this.options.esNumero) {
                    if (this.esentradaViable(obj, p, caracter, this.options.fijarMascara)) {
                        if (key == 16 || this.options.fijarMascara.charAt(p) == "!") this.anteriorSelection(obj, p, caracter.toUpperCase());
                        else this.anteriorSelection(obj, p, caracter);
                        this.seleccionarSiguiente(obj, this.options.fijarMascara);
                    }
                }
                else if ("1234567890".indexOf(caracter) >= 0) {
                    if (this.options.focus == 1) {
                        this.options.focus = 0;
                        if (this.options.decimales == 2)
                        { obj.value = "0.00"; }
                        else {
                            if (this.options.decimales == 0) {
                                obj.value = "0";
                            } else {                            
                                obj.value = "0.000";
                            } 
                        }
                    }
                    this.insertarNumero(obj, caracter, p);

                }
                break;
        }
        this.options.focus = 0;
        event.stop();
        return false;
    },
    eventoKeydown: function(event, obj) {
        //evento para el IE ya que con el evento keprees no hace caso los key's siguientes
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        //chrome al igual que IE en el evento keyprees no detecta las teclas especiales
        var key = event.code;
        if ((key == 8 || key == 9 || key == 35 || key == 36 || key == 37 || key == 38 || key == 39 || key == 40 || key == 46 || key == 110 || key == 190) && (!(/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) || key == 110 || key == 46))// delete o f5;
            try {
            var p = this.obtenerSelectionInicio(obj);
            switch (key) {
                case 8:
                    if (!this.options.esNumero) {
                        this.seleccionarAnterior(obj, this.options.fijarMascara);
                        if (p > 0)
                            if ("!#&".indexOf(this.options.fijarMascara.charAt(p - 1)) >= 0)
                            this.anteriorSelection(obj, p - 1, '_');
                        else
                            this.anteriorSelection(obj, p - 2, '_');
                    } else {
                        if (this.options.focus == 1) {
                            this.options.focus = 0;
                            if (this.options.decimales == 2)
                            { obj.value = "0.00"; }
                            else {
                                if (this.options.decimales == 0) {
                                    obj.value = "0";
                                } else {                                
                                    obj.value = "0.000";
                                }
                            }
                            this.seleccionarhastaelPunto(obj);
                        }
                        if ((obj.value.length - p) <= 2) {
                            if (obj.value.charAt(p - 1) == ".")
                                this.anteriorSelection(obj, p - 2, '0');
                            else
                                this.anteriorSelection(obj, p - 1, '0');
                        } else
                            this.eliminarNumero("BACKSPACE", obj, p);
                    }
                    break;
                case 9:
                    return true;
                    break;
                case 36: // inicio
                    if (!this.options.esNumero) this.seleccionarelPrimero(obj, this.options.fijarMascara);
                    else this.seleccionarelPrimero(obj, this.generarMascara(obj.value));
                    break;
                case 35: // final
                    if (!this.options.esNumero) this.seleccionarUltimo(obj, this.options.fijarMascara);
                    else this.seleccionarUltimo(obj, this.generarMascara(obj.value));
                    break;
                case 37: // Left
                case 38: // Up
                    if (!this.options.esNumero) this.seleccionarAnterior(obj, this.options.fijarMascara);
                    else {
                        this.seleccionarAnterior(obj, this.generarMascara(obj.value));
                        this.options.esPunto = false;
                    }
                    break;
                case 39: // Right
                case 40: // Down
                    if (!this.options.esNumero) this.seleccionarSiguiente(obj, this.options.fijarMascara);
                    else {
                        this.seleccionarSiguiente(obj, this.generarMascara(obj.value));
                        this.options.esPunto = true;
                    }
                    break;
                case 46: // Delete
                    if (!this.options.esNumero)
                        this.anteriorSelection(obj, p, '_');
                    else {
                        if (this.options.focus == 1) {
                            this.options.focus = 0;
                            if (this.options.decimales == 2)
                            { obj.value = "0.00"; }
                            else {
                                if (this.options.decimales == 0) {
                                    obj.value = "0";
                                } else {                                
                                    obj.value = "0.000";
                                } 
                            }
                        }
                        if ((obj.value.length - p) <= this.options.decimales)
                            this.anteriorSelection(obj, p, '0');
                        else
                            this.eliminarNumero("DELETE", obj, p);
                    }
                    break;
                case 190: //punto
                case 110: // punto numpad
                    if (this.options.esNumero) {
                        if (this.options.focus == 1) {
                            this.options.focus = 0;
                            if (this.options.decimales == 2)
                            { obj.value = "0.00"; }
                            else {
                                if (this.options.decimales == 0) {
                                    obj.value = "0";
                                } else {                                
                                    obj.value = "0.000";
                                } 
                            }
                        }
                        this.options.esPunto = true;
                        this.seleccionardespuesdelPunto(obj);
                    }
                    break;
            }
            event.returnValue = false;
            event.stop()
            this.options.focus = 0;
        }
        catch (error) {
            event.returnValue = false;
            event.stop()
        }
        ;
    },
    MouseUp: function(event, obj) {
        var p = this.obtenerSelectionInicio(obj);
        this.fijarSeleccion(obj, p, (p + 1));
    },
    ponerPosicion: function(obj) {//Focus
        if (!this.options.esNumero) {
            obj.value = this.usarMascara(obj.value, this.options.fijarMascara);
            this.seleccionarelPrimero(obj, this.options.fijarMascara);
        }
        else {

            if (obj.value == "" || obj.value == null) {
                if (this.options.decimales == 2)
                { obj.value = "0.00"; }
                else {
                    if (this.options.decimales == 0) {
                        obj.value = "0";
                    } else {                    
                        obj.value = "0.000";
                    } 
                }
            }
            this.options.esPunto = false;
            this.eventoFocus(obj);
        }
    },
    eventoFocus: function(obj) {
        this.fijarSeleccion(obj, 0, obj.value.length);
        this.options.focus = 1;

    },
    insertarNumero: function(obj, caracter, pos) {
        if ((obj.value.length - pos) > this.options.decimales) {
            var val = obj.value.substring(0, pos + 1);
            var valor = val + caracter;
            obj.value = this.FormatoNumero(valor) + obj.value.substring(pos + 1, obj.value.length);
            if (obj.value.length == (2 + this.options.decimales))
                this.fijarSeleccion(obj, pos, pos + 1);
            else
                if (obj.value.length == 5 && obj.value.charAt(0) == "-")
                this.fijarSeleccion(obj, pos, pos + 1);
            else
                this.fijarSeleccion(obj, pos + 1, pos + 2);
        } else {
            this.anteriorSelection(obj, pos, caracter);
            this.seleccionarSiguiente(obj, this.generarMascara(obj.value));
        }
    },
    eliminarNumero: function(accion, obj, pos) {
        if (accion == "DELETE") {
            if (obj.value.charAt(pos + 1) == ".") {
                var val = obj.value.substring(0, pos);
                obj.value = this.FormatoNumero(val) + obj.value.substring(pos + 1, obj.value.length);
                if (pos == 0)
                    if (obj.value.charAt(0) == "-")
                    pos = 2;
                else
                    pos = 1;
                this.fijarSeleccion(obj, pos - 1, pos);
            } else {
                var val = obj.value.substring(0, pos);
                obj.value = val + obj.value.substring(pos + 1, obj.value.length);
                this.fijarSeleccion(obj, pos, pos + 1);
            }
        }
        if (accion == "BACKSPACE") {
            if (pos != 0) {
                var val = obj.value.substring(0, pos - 1);
                obj.value = val + obj.value.substring(pos, obj.value.length);
                this.fijarSeleccion(obj, pos - 1, pos);
            }
        }
    },
    FormatoNumero: function(valor) {
        var str2 = valor;
        var str1 = "";
        for (var i = 0, len = str2.length; i < len; i++) {
            if (str2.charAt(i) == "-") {
                str1 += "-";
            }
            else
                if ('0' != str2.charAt(i)) {
                str1 += str2.substr(i);
                break;
            }
        }
        str2 = str1;
        str1 = "";
        if (str2.length == 1 && str2.charAt(0) == "-")
            str1 = "-0";
        else {
            if (str2.length == 0)
                str1 += "0";
            str1 += str2;
        }
        return str1;
    },
    obtenerSelectionInicio: function(obj) {
        var p = 0;
        if (obj.selectionStart) {
            if ($type(obj.selectionStart) == "number") //motools
                p = obj.selectionStart;
        } else if (document.selection) {
            var rd = document.selection.createRange().duplicate();
            rd.moveEnd("character", obj.value.length);
            p = obj.value.lastIndexOf(rd.text);
            if (rd.text == "") p = obj.value.length;
        }
        return p;
    },
    esposiciondeEntrada: function(obj, p, mascara) {
        var chr = mascara.charAt(p);
        if ("!#&".indexOf(chr) >= 0)
            return true;
        return false;
    },
    usarMascara: function(value, mascara) {
        var vacio = ""
        if (this.options.esNumero == true)
            vacio = "0";
        else
            var vacio = "_";

        var numeroValido = "-1234567890";
        var textoValido = "abcdefghijklmnopqrstuvwxyz";
        var salida = "";
        for (var i = 0, len = mascara.length; i < len; i++) {
            switch (mascara.charAt(i)) {
                case '#':
                    if (numeroValido.indexOf(value.charAt(i).toLowerCase()) >= 0) {
                        if (value.charAt(i) == "") { salida += vacio; }
                        else { salida += value.charAt(i); }
                    } else
                        salida += vacio;
                    break;
                case '!':
                case '&':
                    if (textoValido.indexOf(value.charAt(i).toLowerCase()) >= 0) {
                        if (value.charAt(i) == "") { salida += vacio; }
                        else { salida += value.charAt(i); }
                    } else
                        salida += vacio;
                    break;
                default:
                    salida += mascara.charAt(i);
                    break
            }
        }
        return salida;
    },
    //quitarMascara: function(numero, obj, mascara) {
    //    var value = obj.value;
    //    var vacio = '_';
    //    if ("" == value) return "";
    //    var salida = "";
    //    if (numero == "False") {
    //        for (var i = 0, len = value.length; i < len; i++) {
    //            if ((value.charAt(i) != vacio) && (esposiciondeEntrada(obj, i, mascara)))
    //            { salida += value.charAt(i); }
    //        }
    //    } else
    //        for (var i = 0, len = value.length; i < len; i++) {
    //        if ("1234567890".indexOf(value.charAt(i)) >= 0)
    //        { salida += value.charAt(i); }
    //    }
    //    return salida;
    //}
    seleccionarhastaelPunto: function(obj) {
        for (var i = 0, len = obj.value.length; i < len; i++) {
            if (obj.value.charAt(i) == ".") {
                this.fijarSeleccion(obj, i - 1, i);
                return;
            }
        }
    },
    seleccionardespuesdelPunto: function(obj) {
        for (var i = 0, len = obj.value.length; i < len; i++) {
            if (obj.value.charAt(i) == ".") {
                this.fijarSeleccion(obj, i + 1, i + 2);
                return;
            }
        }
    },
    fijarSeleccion: function(obj, a, b) {
        if (obj.setSelectionRange) {
            obj.focus();
            obj.setSelectionRange(a, b);
        } else if (obj.createTextRange) {
            var r = obj.createTextRange();
            r.collapse();
            r.moveStart("character", a);
            r.moveEnd("character", (b - a));
            r.select();
        }
    },
    SelecciondeInicio: function(obj) {
        var p = 0;
        if (obj.selectionStart) {
            if (document.selection) {
                var r = document.selection.createRange().duplicate();
                r.moveEnd("character", obj.value.length);
                p = obj.value.lastIndexOf(r.text);
                if (r.text == "") p = obj.value.length;
            }
            return p;
        }
    },
    seleccionarelPrimero: function(obj, mascara) {
        for (var i = 0, len = mascara.length; i < len; i++) {
            if (this.esposiciondeEntrada(obj, i, mascara)) {
                this.fijarSeleccion(obj, i, (i + 1));
                return;
            }
        }
    },
    seleccionarAnterior: function(obj, mascara, p) {
        if (!$chk(p))
            p = this.obtenerSelectionInicio(obj);
        if (p <= 0) {
            this.seleccionarelPrimero(obj, mascara);
        } else {
            if (this.esposiciondeEntrada(obj, (p - 1), mascara)) {
                this.fijarSeleccion(obj, (p - 1), p);
            } else {
                this.seleccionarAnterior(obj, mascara, (p - 1));
            }
        }
    },
    seleccionarSiguiente: function(obj, mascara, p) {
        if (!$chk(p)) p = this.seleccionFinal(obj);
        if (p >= mascara.length) {
            this.seleccionarUltimo(obj, mascara);
        } else {
            if (this.esposiciondeEntrada(obj, p, mascara)) {
                this.fijarSeleccion(obj, p, (p + 1));
            } else {
                this.seleccionarSiguiente(obj, mascara, (p + 1));
            }
        }
    },
    seleccionarUltimo: function(obj, mascara) {
        for (var i = (mascara.length - 1); i >= 0; i--) {
            if (this.esposiciondeEntrada(obj, i, mascara)) {
                this.fijarSeleccion(obj, i, (i + 1));
                return;
            }
        }
    },
    seleccionFinal: function(obj) {
        var p = 0;
        if (obj.selectionEnd) {
            if ($type(obj.selectionEnd) == "number")
            { p = obj.selectionEnd; }
        } else if (document.selection) {
            var r = document.selection.createRange().duplicate();
            r.moveStart("character", -obj.value.length);
            p = r.text.length;
        }
        return p;
    },

    anteriorSelection: function(obj, p, chr) {
        var value = obj.value;
        var salida = "";
        salida += value.substring(0, p);
        salida += chr;
        salida += value.substr(p + 1);
        obj.value = salida;
        this.fijarSeleccion(obj, p, (p + 1));
    },
    esentradaViable: function(obj, p, chr, mascara) {
        var vacio = '_';
        var numeroValido = "1234567890";
        var textoValido = "abcdefghijklmnopqrstuvwxyz";
        var caracter = chr.toLowerCase();
        var chMask = mascara.charAt(p);
        switch (chMask) {
            case '#':
                if (numeroValido.indexOf(caracter) >= 0) return true;
                break;
            case '!':
            case '&':
                if (textoValido.indexOf(caracter) >= 0) return true;
                break;
            default:
                return false;
                break;
        }
    },
    generarMascara: function(valor) {
        var salida = "";
        for (var i = 0, len = valor.length; i < len; i++) {
            if ("-1234567890".indexOf(valor.charAt(i)) >= 0)
                salida += "#";
            else
                salida += valor.charAt(i);
        }
        return salida;
    }

    //Fin metodos mascara
});