﻿function roundInput(input_id) {
    var input = $('#' + input_id + '');
    input.css('border', '0');
    var input_width = input.css('width'); //get the width of input
    var wrap_width = parseInt(input_width); //add 10 for padding
    wrapper = input.wrap("<div style='background-color:#fff;padding:2px'></div>").parent();
    wrapper.wrap("<div style='background:#999999;display:inline-block;'></div>"); //apply border
    wrapper.corner("round 8px").parent().css('padding', '2px').corner("round 10px"); //round box and border
}
function es_numero(caracter) {
    numeros = "0123456789";
    return (numeros.indexOf(caracter) >= 0)
}
function es_anio_bisiesto(anio) {
    if (((anio % 4 == 0) && anio % 100 != 0) || anio % 400 == 0)
        return true;
    return false;
}
function comprobar_date(fecha) {
    separador_fecha = "/";
    if (fecha.length == 0)
        return "Ok";
    var cantidad_separadores = 0;

    //Comprobación de la sintaxis de una fecha.
    for (var indice = 0; indice < fecha.length; ++indice) {
        var caracter = fecha.charAt(indice);

        if (!es_numero(caracter) && caracter != separador_fecha)
            return ("Ha introducido un caracter ilegal en la fecha.")

        if (caracter == separador_fecha)
            cantidad_separadores++
    }

    if (cantidad_separadores != 2)
        return ("El número de separadores ingresados en la fecha es incorrecto.")
    

    //Comprobación de la semántica de una fecha.
    var posicion_separador1 = fecha.indexOf(separador_fecha);
    var dia = fecha.substring(0, posicion_separador1);
    var posicion_separador2 = fecha.indexOf(separador_fecha, posicion_separador1 + 1);
    var mes = fecha.substring(posicion_separador1 + 1, posicion_separador2);
    var anio = fecha.substring(posicion_separador2 + 1, 10);

    if (anio < 0 || anio > 9999)
        return ("Ha introducido un año incorrecto.")

    if (mes < 1 || mes > 12)
        return ("Ha introducido un mes incorrecto.")
    if ((dia < 1 || dia > 31) || (mes == 4 && dia > 30) || (mes == 6 && dia > 30) || (mes == 9 && dia > 30) || (mes == 11 && dia > 30) || (mes == 2 && es_anio_bisiesto(anio) && dia > 29) || (mes == 2 && !es_anio_bisiesto(anio) && dia > 28))
        return ("Ha introducido un día incorrecto.")

    return "Ok";
}
function mostrarEquis(obj) {
    var id = obj.id;
    var id_btnEliminar = id.replace("filaEquis", "e_btnEliminar");
    var btnEliminar = document.getElementById(id_btnEliminar);
    btnEliminar.style.visibility = "visible";
}
function ocultarEquis(obj) {
    var id = obj.id;
    var id_btnEliminar = id.replace("filaEquis", "e_btnEliminar");
    var btnEliminar = document.getElementById(id_btnEliminar);
    btnEliminar.style.visibility = "hidden";
}