﻿Imports System.Data.SqlClient
Public Class farm_adm
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("IDUSUARIO") Is Nothing Then
            Response.Redirect("/home.aspx")
        End If
        Dim connStr = My.Settings.BDFarmConnectionString
        Dim SqlCommand
        Dim DbMenu As DataSet
        Dim relation As DataRelation
        Using conn = New SqlConnection(connStr)
            SqlCommand = "SELECT distinct MODULOS.ID_MODULO as MenuID,MODULOS.ID_MODULO_PADRE as ParentID,MODULOS.MOD_ORDEN,MODULOS.DESC_MODULO as Text, MODULOS.URL as NavigateUrl  " & _
            " FROM USUARIO" & _
            " INNER JOIN PERMISOS ON PERMISOS.ID_USUARIO =USUARIO.ID_USUARIO" & _
            " INNER JOIN ROLES ON ROLES.ID_ROL = PERMISOS.ID_ROL " & _
            " INNER JOIN ROLES_MODULO ON ROLES_MODULO.ID_ROL = ROLES.ID_ROL " & _
            " INNER JOIN MODULOS ON MODULOS.ID_MODULO = ROLES_MODULO.ID_MODULO" & _
            " WHERE USUARIO.ID_USUARIO=" & Session("IDUSUARIO") & _
            " ORDER BY MOD_ORDEN"
            DbMenu = New DataSet()
            Dim Adapter = New SqlDataAdapter(SqlCommand, conn)
            Adapter.Fill(DbMenu)
            Adapter.Dispose()
        End Using
        DbMenu.DataSetName = "Menus"
        DbMenu.Tables(0).TableName = "Menu"
        relation = New DataRelation("ParentChild", DbMenu.Tables("Menu").Columns("MenuID"), DbMenu.Tables("Menu").Columns("ParentID"), True)
        relation.Nested = True
        DbMenu.Relations.Add(relation)
        XmlDataSource1.Data = DbMenu.GetXml()
    End Sub
    Public Function obtenerNombreUsuario() As String
        Return Session("NOMBRE_WEB")
    End Function
    Protected Sub cerrarSession_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cerrarSession.Click
        Session.Abandon()
        Response.Redirect("/login.aspx")
    End Sub
End Class