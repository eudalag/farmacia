﻿Public Class gestionRoles
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub gestionRoles_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            'inicio(Me.Controls, "Roles")
            'CType(Master.FindControl("panelMenuH"), Panel).Controls.Add(crearMenu(Request.QueryString("idmenu_p")))
            cargarRol(CStr(Request.QueryString("codRol")))
            cargarRolesModulos(CStr(Request.QueryString("codRol")))
        End If
        inicio(Me.Controls, "Roles", 42)
    End Sub
    Protected Sub cargarRol(ByVal ID_rol As String)
        Dim tblRol As DataTable
        Dim sSql As String
        sSql = "SELECT * FROM ROLES WHERE ID_ROL = '" & ID_rol & "'"
        tblRol = New mcTabla().ejecutarConsulta(sSql)
        If tblRol.Rows.Count <> 0 Then
            txtRol.Text = CStr(tblRol.Rows(0).Item("DESC_ROL"))
        End If
    End Sub

    Protected Sub cargarRolesModulos(ByVal ID_rol As String)
        Dim m = New dataFarmDataContext()
        Dim q = From p In m.MODULOS Where p.ID_MODULO_PADRE Is Nothing _
        Order By p.DESC_MODULO Ascending Select New With {.padre = p.DESC_MODULO, p.ID_MODULO, .count = (From n In m.MODULOS Where n.ID_MODULO_PADRE = p.ID_MODULO).Count(), .ACCESO = seleccionar(1, m, p.ID_MODULO, ID_rol), .ALTA = seleccionar(2, m, p.ID_MODULO, ID_rol), .MODIFICACION = seleccionar(3, m, p.ID_MODULO, ID_rol), .ELIMINACION = seleccionar(4, m, p.ID_MODULO, ID_rol), _
                         .hijos = From c In m.MODULOS Group Join consulta In (From r In m.ROLES_MODULO Join rm In m.ROLES On r.ID_ROL Equals rm.ID_ROL Where r.ID_ROL = ID_rol Select r) On c.ID_MODULO Equals consulta.ID_MODULO Into Group From rol In Group.DefaultIfEmpty() Where c.ID_MODULO_PADRE = p.ID_MODULO _
                                  Select New With {.hijo = c.ID_MODULO, c.ID_MODULO_PADRE, .nombre = c.DESC_MODULO, c.URL, .ID_PADRE = c.ID_MODULO_PADRE, .ALTA = Me.obtenerValor(rol.ALTA), .ACCESO = Me.obtenerValor(rol.ACCESO), .MODIFICACION = Me.obtenerValor(rol.MODIFICACION), .ELIMINACION = obtenerValor(rol.ELIMINACION) _
                                  , .ACCESO_CHK = seleccionar(1, m, c.ID_MODULO_PADRE, ID_rol), .ALTA_CHK = seleccionar(2, m, c.ID_MODULO_PADRE, ID_rol), .MODIFICACION_CHK = seleccionar(3, m, c.ID_MODULO_PADRE, ID_rol), .ELIMINACION_CHK = seleccionar(4, m, c.ID_MODULO_PADRE, ID_rol)}}
        lvwModulos.DataSource = q
        lvwModulos.DataBind()
    End Sub
    Function seleccionar(ByVal op As Integer, ByVal data As dataFarmDataContext, ByVal padre As Integer, ByVal rol As Integer) As Boolean
        Dim q2 = (From h In data.MODULOS Join r In data.ROLES_MODULO On h.ID_MODULO Equals r.ID_MODULO Where h.ID_MODULO_PADRE.ToString() IsNot Nothing And r.ID_ROL = rol And h.ID_MODULO_PADRE = padre Select New With {r.ACCESO, r.ALTA, r.MODIFICACION, r.ELIMINACION, h.ID_MODULO_PADRE}).ToList
        Dim selecionado As Boolean = False
        If q2.Count = 0 Then
            Return False
        End If
        For Each Item In q2
            Select Case op
                Case 1
                    selecionado = selecionado + Item.ACCESO
                Case 2
                    selecionado = selecionado + Item.ALTA
                Case 3
                    selecionado = selecionado + Item.MODIFICACION
                Case 4
                    selecionado = selecionado + Item.ELIMINACION
            End Select
        Next
        Return selecionado
    End Function
    Function obtenerValor(ByVal val) As Boolean
        If (val Is Nothing) Then
            Return Not (val Is Nothing)
        Else
            Return val
        End If
    End Function
    Protected Function existeRolesModulos(ByVal codRol As String, ByVal codModulo As String)
        Dim sSql As String = "select * from ROLES_MODULO where ID_ROL = '" & codRol & "' and ID_MODULO = '" & codModulo & "'"
        Dim tblRolesModulos As DataTable = New mcTabla().ejecutarConsulta(sSql)
        Return tblRolesModulos.Rows.Count <> 0
    End Function

    Protected Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAceptar.Click
        Dim sSql As String
        Dim ROLES_MODULO As New mcTabla
        Dim numModulos As Integer = lvwModulos.Items.Count
        For cont = 0 To numModulos - 1
            Dim codRol As String = CStr(Request.QueryString("codRol"))
            Dim codModulo As String = CStr(CType(lvwModulos.Items(cont).FindControl("hdfcodModulo"), HiddenField).Value)
            Dim lvw_submodulos As ListView = CType(lvwModulos.Items(cont).FindControl("lvwSubmodulos"), ListView)
            Dim chkAcceso As Boolean = CType(lvwModulos.Items(cont).FindControl("chkAcceso"), HtmlInputCheckBox).Checked
            Dim chkAlta As Boolean = CType(lvwModulos.Items(cont).FindControl("chkAlta"), HtmlInputCheckBox).Checked
            Dim chkModificacion As Boolean = CType(lvwModulos.Items(cont).FindControl("chkModificacion"), HtmlInputCheckBox).Checked
            Dim chkEliminacion As Boolean = CType(lvwModulos.Items(cont).FindControl("chkEliminacion"), HtmlInputCheckBox).Checked
            Dim modulo_padre As Boolean = (chkAcceso Or chkAlta Or chkEliminacion Or chkEliminacion)
            If modulo_padre Then
                If Not existeRolesModulos(codRol, codModulo) Then
                    sSql = "INSERT INTO ROLES_MODULO (ID_ROL, ID_MODULO, ALTA, ACCESO, MODIFICACION, ELIMINACION) "
                    sSql += "VALUES ("
                    sSql += "'" & codRol & "',"
                    sSql += "'" & codModulo & "',"
                    sSql += "'" & chkAcceso & "',"
                    sSql += "'" & chkAlta & "',"
                    sSql += "'" & chkModificacion & "',"
                    sSql += "'" & chkEliminacion & "')"
                    ROLES_MODULO.ejecutarSQL(sSql)
                End If
            Else
                If existeRolesModulos(codRol, codModulo) Then
                    sSql = "DELETE FROM ROLES_MODULO "
                    sSql += " WHERE ID_MODULO LIKE '" & codModulo & "' AND ID_ROL LIKE '" & codRol & "'"
                    ROLES_MODULO.ejecutarSQL(sSql)
                End If
            End If
            Dim numSbuModulos = lvw_submodulos.Items.Count
            For c = 0 To numSbuModulos - 1
                Dim s_codModulo As String = CStr(CType(lvw_submodulos.Items(c).FindControl("hdfcodModulo"), HiddenField).Value)
                Dim s_chkAcceso As HtmlInputCheckBox = CType(lvw_submodulos.Items(c).FindControl("chkAcceso"), HtmlInputCheckBox)
                Dim s_chkAlta As HtmlInputCheckBox = CType(lvw_submodulos.Items(c).FindControl("chkAlta"), HtmlInputCheckBox)
                Dim s_chkModificacion As HtmlInputCheckBox = CType(lvw_submodulos.Items(c).FindControl("chkModificacion"), HtmlInputCheckBox)
                Dim s_chkEliminacion As HtmlInputCheckBox = CType(lvw_submodulos.Items(c).FindControl("chkEliminacion"), HtmlInputCheckBox)
                If (s_chkAcceso.Checked = False And s_chkAlta.Checked = False And s_chkModificacion.Checked = False And s_chkEliminacion.Checked = False) Then
                    If existeRolesModulos(codRol, s_codModulo) Then
                        sSql = "DELETE FROM ROLES_MODULO "
                        sSql += " WHERE ID_MODULO LIKE '" & s_codModulo & "' AND ID_ROL LIKE '" & codRol & "'"
                        ROLES_MODULO.ejecutarSQL(sSql)
                    End If
                Else
                    If existeRolesModulos(codRol, s_codModulo) Then
                        sSql = "UPDATE ROLES_MODULO SET  "
                        sSql += "ALTA = '" & s_chkAlta.Checked & "', "
                        sSql += "ACCESO = '" & s_chkAcceso.Checked & "', "
                        sSql += "ELIMINACION = '" & s_chkEliminacion.Checked & "', "
                        sSql += "MODIFICACION = '" & s_chkModificacion.Checked & "' "
                        sSql += " WHERE ID_MODULO LIKE '" & s_codModulo & "' AND ID_ROL LIKE '" & codRol & "'"
                        ROLES_MODULO.ejecutarSQL(sSql)
                    Else
                        sSql = "INSERT INTO ROLES_MODULO (ID_ROL, ID_MODULO, ALTA, ACCESO, MODIFICACION, ELIMINACION) "
                        sSql += "VALUES ("
                        sSql += "'" & codRol & "',"
                        sSql += "'" & s_codModulo & "',"
                        sSql += "'" & s_chkAlta.Checked & "',"
                        sSql += "'" & s_chkAcceso.Checked & "',"
                        sSql += "'" & s_chkModificacion.Checked & "',"
                        sSql += "'" & s_chkEliminacion.Checked & "')"
                        ROLES_MODULO.ejecutarSQL(sSql)
                    End If
                End If
            Next
        Next
        ROLES_MODULO.tabla = "ROLES"
        ROLES_MODULO.campoID = "ID_ROL"
        ROLES_MODULO.valorID = Request.QueryString("codRol")
        ROLES_MODULO.agregarCampoValor("DESC_ROL", CStr(txtRol.Text))
        ROLES_MODULO.modificar()
        Response.Redirect("roles.aspx?idmenu=" & Request.QueryString("idmenu") & "&idmenu_p=" & Request.QueryString("idmenu_p"))
    End Sub

    Protected Sub btnCancelar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelar.Click
        Response.Redirect("roles.aspx?idmenu=" & Request.QueryString("idmenu") & "&idmenu_p=" & Request.QueryString("idmenu_p"))
    End Sub
    Protected Sub HacerCambios(ByVal sender As Object, ByVal e As EventArgs)
        Dim chk As CheckBox = sender
        Dim x As ListViewDataItem = CType(chk.NamingContainer, ListViewDataItem)
        'Dim chkAcceso As CheckBox = CType(lvwRolesModulos.Items(x.DataItemIndex).FindControl("chkAcceso"), CheckBox)
        Dim chkAcceso As CheckBox = CType(x.FindControl("chkAcceso"), CheckBox)
        Dim chkAlta As CheckBox = CType(x.FindControl("chkAlta"), CheckBox)
        Dim chkEliminacion As CheckBox = CType(x.FindControl("chkEliminacion"), CheckBox)
        Dim chkModificacion As CheckBox = CType(x.FindControl("chkModificacion"), CheckBox)
        If Not chk.Checked Then
            chkAcceso.Checked = False
            chkAlta.Checked = False
            chkEliminacion.Checked = False
            chkModificacion.Checked = False
            chkAcceso.Enabled = False
            chkAlta.Enabled = False
            chkEliminacion.Enabled = False
            chkModificacion.Enabled = False
        Else
            chkAcceso.Checked = True
            chkAlta.Checked = False
            chkEliminacion.Checked = False
            chkModificacion.Checked = False
            chkAcceso.Enabled = True
            chkAlta.Enabled = True
            chkEliminacion.Enabled = True
            chkModificacion.Enabled = True
        End If

    End Sub
End Class