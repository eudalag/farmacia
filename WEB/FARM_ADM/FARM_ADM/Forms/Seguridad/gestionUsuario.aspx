﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionUsuario.aspx.vb" Inherits="FARM_ADM.gestionUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <table style="width:100%;"  cellspacing="10">
        <tr>
          <td colspan="2"> 
            <div class="alinearIzq" style="padding-bottom:10px;""> 
            <asp:Label ID="Label5" runat="server" Text="Gestion Usuarios:" CssClass="textoNegroNegrita" ></asp:Label> 
            <hr style="height:-12px;color:#CCCCCC;width:30%;text-align:left;" align="left" />
            </div>
          </td>
        </tr>
        <tr>
            <td class="textoGris" style="width:130px;">
                Nombre y Apellidos:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombreApellidos" MaxLength="200" Columns="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Telefono:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtTelefono" MaxLength="15" Columns="10" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Celular:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="15"  Columns="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Direccion:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDireccion" MaxLength="50" Columns="50"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="textoGris">
                Tipo Usuario:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoUsuario" DataSourceID="sqlTipoUsuario"
                    DataTextField="TIPO_USUARIO" DataValueField="ID_TIPO_USUARIO"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlTipoUsuario" 
                    ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                    SelectCommand="SELECT ID_TIPO_USUARIO,TIPO_USUARIO FROM TIPO_USUARIOS 
                    UNION SELECT -1,'[Seleccione un Tipo de Usuario]'
                    ORDER BY ID_TIPO_USUARIO "></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Usuario:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtUsuario" MaxLength="50" Columns="25"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Contrasena:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtContrasenha" TextMode="Password"  Columns="25"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Repita Contrasena:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtContrasenha2" TextMode="Password" Columns="25"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Aplica Descuento:
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkDescuento" />
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Cambia Contrasena:
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkCambioContrasenha" />
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Habilitado:
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkHabilitado" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblError" Text="" CssClass="textoRojo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="alinearDer" >
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
            </td>
        </tr>

    </table>
</asp:Content>
