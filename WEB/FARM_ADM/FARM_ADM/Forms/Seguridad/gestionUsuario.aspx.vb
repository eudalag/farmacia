﻿Imports System.Transactions
Public Class gestionUsuario
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub gestionUsuario_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If
        End If
    End Sub
    Protected Sub cargarDatos(ByVal idUsuario As Integer)
        Dim sSql As String = ""
        sSql = "DECLARE @USUARIO AS INTEGER;"
        sSql += " SET @USUARIO = " + idUsuario.ToString + ";"
        sSql += " SELECT * FROM USUARIO WHERE ID_USUARIO = @USUARIO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtNombreApellidos.Text = esNulo(tbl.Rows(0)("NOMBRE_APELLIDOS"))
            txtTelefono.Text = esNulo(tbl.Rows(0)("TELEFONO"))
            txtCelular.Text = esNulo(tbl.Rows(0)("CELULAR"))
            txtDireccion.Text = esNulo(tbl.Rows(0)("DIRECCION"))
            ddlTipoUsuario.SelectedValue = tbl.Rows(0)("ID_TIPO_USUARIO")
            txtUsuario.Text = esNulo(tbl.Rows(0)("USUARIO"))
            chkDescuento.Checked = CBool(tbl.Rows(0)("DESCUENTO"))
            chkCambioContrasenha.Checked = CBool(tbl.Rows(0)("CAMBIO_CONTRASENHA"))
            chkHabilitado.Checked = tbl.Rows(0)("HABILITADO")

        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("usuario.aspx")
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Try
                Using scope As New TransactionScope()
                    Dim tbl = New mcTabla()
                    tbl.tabla = "USUARIO"
                    tbl.campoID = "ID_USUARIO"
                    tbl.modoID = "auto"
                    tbl.agregarCampoValor("NOMBRE_APELLIDOS", txtNombreApellidos.Text)
                    tbl.agregarCampoValor("TELEFONO", txtTelefono.Text)
                    tbl.agregarCampoValor("CELULAR", txtCelular.Text)
                    tbl.agregarCampoValor("DIRECCION", txtDireccion.Text)
                    tbl.agregarCampoValor("ID_TIPO_USUARIO", ddlTipoUsuario.SelectedValue)
                    tbl.agregarCampoValor("USUARIO", txtUsuario.Text)
                    tbl.agregarCampoValor("CONTRASENHA", New Encriptador().GenerarHash(txtContrasenha2.Text))
                    tbl.agregarCampoValor("DESCUENTO", chkDescuento.Checked)
                    tbl.agregarCampoValor("CAMBIO_CONTRASENHA", chkCambioContrasenha.Checked)
                    tbl.agregarCampoValor("HABILITADO", chkHabilitado.Checked)
                    If Request.QueryString("ID") Is Nothing Then
                        tbl.insertar()
                    Else
                        tbl.valorID = Request.QueryString("ID")
                        tbl.modificar()
                    End If
                    scope.Complete()
                    Response.Redirect("usuarios.aspx")
                End Using
            Catch ex As Exception
                lblError.Text = ex.Message.ToString
            End Try
        End If

    End Sub
End Class