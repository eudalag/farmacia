﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="permisos.aspx.vb" Inherits="FARM_ADM.permisos1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"> 
  </asp:ScriptManager> 
  <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
    <ContentTemplate> 
      <div class="alinearIzq"> 
      <span class="tituloGris">Permisos:</span>
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC" />
      </div>
      <table width="100%">
        <tr>
          <td> 
          <span id="lbsistema" class="textoGris">Usuario :</span>
            <asp:DropDownList runat="server" ID="dwlusuarios" AutoPostBack="True" Height="18px" Width="200px">
            </asp:DropDownList> 
          </td>
          <td align="center"> 
              &nbsp;</td>
        </tr>
        <tr><td colspan="2"><br /></td></tr>
        <tr>
          <td colspan="2" class="textoGris"> 
            <span>Seleccione los Roles :</span>
          </td>
        </tr>
        <tr>
          <td colspan="2"> 
            <asp:ListView ID="lvwRoles" runat="server"> 
              <LayoutTemplate> 
                <table class="generica" id="Table1" runat="server" style="width: 100%;">
                  <thead>
                  <tr>
                    <th>
                    <asp:Label runat="server" ID="lblRol" Text="Roles" CssClass="textoBlanco"></asp:Label> </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td id="itemPlaceHolder"></td>
                  </tr>
                  </tbody>
                </table>
              </LayoutTemplate> 
              <ItemTemplate> 
                <tr>
                  <td class="alinearIzq"> <asp:HiddenField ID="hdId" Value='<%# Eval("ID_ROL") %>' runat="server" /> 
                  <asp:CheckBox Enabled='<%# esAdmin(Eval("ID_ROL"),Eval("ROL")) %>' CssClass="textoGris" ID='m_chkRol' Checked='<%# Eval("ROL") %>' Text = '<%# Eval("DESC_ROL") %>' runat="server" onclick="seleccionar(this);"/> 
                  </td>
                </tr>
              </ItemTemplate> 
            </asp:ListView> 
          </td>
        </tr>
        <caption> 
        <br /> 
        <br /> 
        <tr>
          <td colspan="2" class="alinearDer"> 
            <div class="barradebotones">
             <asp:ImageButton ID="a_imgaceptar" ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png" runat="server" />
    <asp:ImageButton ID="lnkCancel" runat="server" PostBackUrl="~/Forms/Sistema.aspx" ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png" CausesValidation="False" ></asp:ImageButton> 

            </div>
          </td>
        </tr></caption> 
      </table>
      <br /><br /> 
    </ContentTemplate> 
  </asp:UpdatePanel>
<script type="text/javascript">
    function seleccionar(obj) {
        var cheq = obj.checked;
        var inputElements = document.getElementsByTagName('input');
        for (var i = 0; i < inputElements.length; i++) {
            var myElement = inputElements[i];
            if (myElement.type == "checkbox") {
                myElement.checked = false;
            }
        }
        obj.checked = cheq;
    }
</script>
</asp:Content>
