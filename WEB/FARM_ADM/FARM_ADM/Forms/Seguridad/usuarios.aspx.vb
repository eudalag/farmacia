﻿Imports System.Transactions
Public Class usuarios
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click
        For Each item In lvwUsuario.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                Dim id = CType(item.FindControl("m_btnEditar"), LinkButton).CommandArgument
                Using scope As New TransactionScope()
                    Try
                        Dim msg As String = ""
                        If SePuedeEliminar("USUARIO", id, msg) Then
                            Dim tbl = New mcTabla()
                            tbl.tabla = "USUARIO"
                            tbl.campoID = "ID_USUARIO"
                            tbl.valorID = id
                            tbl.eliminar()
                            scope.Complete()
                        Else
                            Dim tbl = New mcTabla()
                            tbl.tabla = "USUARIO"
                            tbl.campoID = "ID_USUARIO"
                            tbl.valorID = id
                            tbl.agregarCampoValor("HABILITADO", 0)
                            tbl.modificar()
                        End If

                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('Error: Se ha producido un error de base de datos al Eliminar.');", True)
                    End Try
                End Using
                lvwUsuario.DataBind()
            End If
        Next
    End Sub

    Protected Sub lvwUsuario_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwUsuario.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionUsuario.aspx?idMenu=11&ID=" + e.CommandArgument)
        End If
    End Sub

    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionUsuario.aspx?idMenu=11")
    End Sub
End Class