﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="roles.aspx.vb" Inherits="FARM_ADM.roles1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
<div class="demo">
        <div id="dialog-confirm" title="" style="display:none;">
	        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
            <span id="texto"></span>
        </div>
</div>
  <table width="100%">
    <tr>
      <td> 
        <div class="alinearIzq"> 
          <asp:Label ID="Label2" runat="server" Text="Roles:" CssClass="tituloGris"></asp:Label> 
          <hr align="left" style="height:-12px;color:#D3D3D3" width="30%" /> 
        </div>
      </td>
    </tr>
    <tr class="margenEspacioAbajo">
      <td class="alinearDer"> 
        <asp:ImageButton ID="a_btnAgregar" runat="server" ImageUrl="~/App_Themes/estandar/imagenes/agregar.png" ToolTip="Nuevo"/>
        <asp:ModalPopupExtender ID="Modal" runat="server" TargetControlID="a_btnAgregar" PopupControlID="modalRol" BackgroundCssClass="modalBackground" CancelControlID="btnCancelar" /> 
      </td>
    </tr>
    <tr>
      <td> 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
          <ContentTemplate> 
            <asp:ListView ID="lvwRoles" runat="server" DataSourceID="dtsRol"> 
              <LayoutTemplate> 
                <table class="generica" id="Table1" runat="server" style="width: 100%;">
                  <thead>
                  <tr id="cabeceraTabla" runat="server">
                    <th>
                    <asp:LinkButton runat="server" ID="btnRol" Text="Rol" CssClass="lkButton" CommandName="Sort" CommandArgument="ID_ROL"></asp:LinkButton> 
                    <asp:Image ID="SortImage1" runat="server" ImageUrl="" Visible="false" /> </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td id="itemPlaceHolder"></td>
                  </tr>
                  </tbody>
                </table>
              </LayoutTemplate> 
              <ItemTemplate> 
                <tr id="filaEquis" runat="server">
                  <td class="alinearIzq img">
                            <asp:LinkButton runat = "server" ID= "m_lblRol" Text = '<%# Eval("DESC_ROL") %>' CommandArgument = '<%# Eval("ID_ROL") %>' CommandName = "modificar"></asp:LinkButton>

                  </td>
                </tr>
              </ItemTemplate> 
            </asp:ListView> <asp:SqlDataSource ID="dtsRol" runat="server" ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" SelectCommand="SELECT * FROM ROLES ORDER BY DESC_ROL ASC">
          </asp:SqlDataSource> 
        </ContentTemplate> 
      </asp:UpdatePanel> 
      </td>
    </tr>
    <tr>
      <td class="alinearDer paginador">
      <asp:UpdatePanel ID="UpdatePanel3" runat="server"> 
          <ContentTemplate>
             <asp:DataPager ID="DataPager2" runat="server" PagedControlID="lvwRoles" PageSize="25">
             <Fields>
                <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="True" ShowLastPageButton="False" ShowNextPageButton="false" ShowPreviousPageButton="false"
                    FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png" 
                    FirstPageText=""/> 
             </Fields>
              <Fields>
                <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="False" ShowLastPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True"
                    PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png" 
                    PreviousPageText=""  />
                    <asp:TemplatePagerField> 
                    <PagerTemplate> <b class="textoGris"> Mostrando <asp:Label runat="server" ID="CurrentPageLabel" Text="<%# IIF(Container.TotalRowCount=0,Container.StartRowIndex,Container.StartRowIndex+1) %>" /> a <asp:Label runat="server" ID="TotalPagesLabel" Text="<%# IIF(Container.StartRowIndex+Container.PageSize>Container.TotalRowCount,Container.TotalRowCount,Container.StartRowIndex+Container.PageSize) %>" /> (Total <asp:Label runat="server" ID="TotalItemsLabel" Text="<%# Container.TotalRowCount%>" /> Registros) 
                    </b> 
                    </PagerTemplate> 
                  </asp:TemplatePagerField>
             </Fields>
             <Fields>
                <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="False" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="false"
                    NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png" 
                    NextPageText=""  /> 
             </Fields>
             <Fields>
                <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="False" ShowLastPageButton="True" ShowNextPageButton="false" ShowPreviousPageButton="false"
                    LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png" 
                    LastPageText=""  /> 
             </Fields>
          </asp:DataPager>          
          </ContentTemplate> 
        </asp:UpdatePanel> 
      </td>
    </tr>
    <tr>
      <td> 
      <br />
        <div class="alinearCentro textoGris"> 
            Seleccione el botón (+) para agregar mas roles a la lista. Seleccione el Rol para modificar los datos del mismo y modificar las propiedades del mismo. Selecciona el boton (X) para eliminar el rol seleccionado. 
        </div>
      </td>
    </tr>
  </table>
  <br /> 
  <asp:Panel ID="modalRol" runat="server" CssClass="modalPopup" Width="350px" style="display : none;"> 
    <table width="100 %">
      <tr>
        <td class="alinearCentro" colspan="2"> 
           <asp:Panel runat="server" ID="titulo" CssClass="panelTitulofondo" Width="335px"> 
            <asp:Label runat="server" ID="lblTitulo" CssClass="textoGris" Text="Nuevo Rol" Font-Size="Medium" Font-Underline="true"></asp:Label> 
          </asp:Panel> 
          <div class="separadorH" /> 
        </td>
      </tr>
      <tr>
        <td class="alinearIzq"> <b> 
          <asp:Label ID="Label1" runat="server" Text="Descripción:"></asp:Label> 
          </b> 
        </td>
        <td> 
          <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="30" Width="170px"></asp:TextBox> 
        <asp:RequiredFieldValidator ID="rfvAfiliado" runat="server" Display="None" ErrorMessage="<b>Campo Requerido</b><br />Rellene descripción de nuevo rol." ControlToValidate="txtDescripcion" ValidationGroup="modal" /> 
        <asp:ValidatorCalloutExtender runat="Server" ID="NReqE" TargetControlID="rfvAfiliado" HighlightCssClass="fondoValidador" /> 
        </td>
      </tr>
      <tr>
        <td class="alinearDer" colspan="2">
            <asp:Button ID="a_btnAceptar" runat="server" Text="Aceptar" ValidationGroup="modal"/>
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="false"/>
        </td>
      </tr>
    </table>
  </asp:Panel> 
  <script type="text/javascript">
      function cancel() {
          document.getElementById('<%=btnCancelar.ClientID%>').click();
      }
  </script>
</asp:Content>
