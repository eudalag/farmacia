﻿Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO

Public Class modulos1
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ' inicio(Me.Controls, "modulos")
            cargarModulos()
            cargarDirModulos()
        Else
            sqlDatos.SelectCommand = Session("sqlModulos")
        End If
    End Sub
    Public Function obtenerDir(ByVal dir As String, ByVal rootdir As String) As String
        Dim url = ""
        Dim directorios = dir.Split("\")
        For i = 0 To directorios.Length - 1
            If (directorios(i) = rootdir) Then
                url = "~/" + rootdir
                For j = i + 1 To directorios.Length - 1
                    url += "/" + directorios(j)
                Next
                Exit For
            End If
        Next
        Return url
    End Function
    Protected Sub lvwModulos_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvwModulos.ItemCommand
        Dim SQL = New mcTabla()
        If (e.CommandName = "Insertar") Then
            Dim ID_ = NuevoID("MODULOS", "id_MODULO")
            Dim txtNombre = CType(e.Item.FindControl("Itxtnombre"), TextBox).Text
            Dim txtUrl = CType(e.Item.FindControl("lnkURL"), LinkButton).Text
            Dim idPadre = CType(e.Item.FindControl("dwlmodPadre"), DropDownList).SelectedValue
            If (idPadre = 0) Then
                idPadre = "null"
            End If
            Dim insertar = "Insert into [MODULOS] ([id_MODULO],[DESC_MODULO],[URL],[id_MODULO_PADRE]) Values(" & ID_ & ",'" & txtNombre & "','" & txtUrl & "'," & idPadre & ");"
            SQL.ejecutarSQL(insertar)
            cargarModulos()
        End If
        If (e.CommandName = "Actualizar") Then
            Dim ID_ = e.CommandArgument
            Dim txtNombre = CType(e.Item.FindControl("UtxtNombre"), TextBox).Text
            Dim txtUrl = CType(e.Item.FindControl("UlnkURL"), LinkButton).Text
            Dim idPadre = CType(e.Item.FindControl("dwlmodPadre"), DropDownList).SelectedValue
            If (idPadre = 0) Then
                idPadre = "null"
            End If
            Dim actualizar = "update [MODULOS] set [DESC_MODULO]='" & txtNombre & "',[URL]='" & txtUrl & "',[id_MODULO_PADRE]=" & idPadre & " where  [id_MODULO]=" & Convert.ToInt32(ID_)
            SQL.ejecutarSQL(actualizar)
            lvwModulos.EditIndex = -1
            cargarModulos()
        End If
        If (e.CommandName = "Eliminar") Then
            Dim msg = ""
            Dim ID_ = e.CommandArgument
            If SePuedeEliminar("MODULOS", ID_, msg) Then
                Dim eliminar = "delete from [MODULOS] where id_MODULO=" & Convert.ToInt32(ID_)
                SQL.ejecutarSQL(eliminar)
                cargarModulos()
            Else
                Mensaje(msg, "Modulos")
            End If
        End If
    End Sub

    Protected Sub lvwModulos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewSortEventArgs) Handles lvwModulos.Sorting
        Dim imgUrl As String
        If e.SortDirection = SortDirection.Ascending Then
            imgUrl = "~/App_Themes/Estandar/imagenes/botones/descendente.gif"
        Else
            imgUrl = "~/App_Themes/Estandar/imagenes/botones/ascendente.gif"
        End If
        Dim sortImage1 As Image = CType(lvwModulos.FindControl("SortImage1"), Image)
        Dim sortImage2 As Image = CType(lvwModulos.FindControl("SortImage2"), Image)
        Dim sortImage3 As Image = CType(lvwModulos.FindControl("SortImage3"), Image)
        Select Case e.SortExpression
            Case "DESC_MODULO"
                sortImage1.Visible = True
                sortImage1.ImageUrl = imgUrl
                sortImage2.Visible = False
                sortImage3.Visible = False
            Case "URL"
                sortImage1.Visible = False
                sortImage2.Visible = True
                sortImage2.ImageUrl = imgUrl
                sortImage3.Visible = False
            Case "PADRE"
                sortImage1.Visible = False
                sortImage2.Visible = False
                sortImage3.Visible = True
                sortImage3.ImageUrl = imgUrl
        End Select
    End Sub
    Protected Sub lvwModulos_ItemEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewEditEventArgs) Handles lvwModulos.ItemEditing
        lvwModulos.EditIndex = e.NewEditIndex
        lvwModulos.InsertItemPosition = InsertItemPosition.None
        cargarModulos()
    End Sub
    Protected Sub lvwModulos_ItemCanceling(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCancelEventArgs) Handles lvwModulos.ItemCanceling
        If (e.CancelMode = ListViewCancelMode.CancelingInsert) Then
            lvwModulos.InsertItemPosition = InsertItemPosition.None
        Else
            lvwModulos.EditIndex = -1
        End If
        cargarModulos()
    End Sub
    Protected Sub lvwModulos_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewUpdateEventArgs) Handles lvwModulos.ItemUpdating
    End Sub
    Protected Sub lvwModulos_PagePropertiesChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lvwModulos.PagePropertiesChanged
        lvwModulos.EditIndex = -1
        lvwModulos.InsertItemPosition = InsertItemPosition.None
        cargarModulos()
    End Sub
    Protected Sub nuevoModulo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles a_nuevoModulo.Click
        lvwModulos.EditIndex = -1
        lvwModulos.InsertItemPosition = InsertItemPosition.LastItem
        cargarModulos()
    End Sub
    Protected Sub OkButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles OkButton.Click
        If (lvwModulos.EditIndex <> -1) Then CType(lvwModulos.EditItem.FindControl("UlnkURL"), LinkButton).Text = TextBox1.Text
        If (lvwModulos.InsertItemPosition <> 0) Then CType(lvwModulos.InsertItem.FindControl("IlnkURL"), LinkButton).Text = TextBox1.Text

    End Sub

    Protected Sub dirModulos_SelectedNodeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dirModulos.SelectedNodeChanged
        If (lvwModulos.EditIndex <> -1) Then
            CType(lvwModulos.EditItem.FindControl("UlnkURL"), LinkButton).Text = dirModulos.SelectedNode.Value
        End If
        If (lvwModulos.InsertItemPosition <> 0) Then
            CType(lvwModulos.InsertItem.FindControl("IlnkURL"), LinkButton).Text = dirModulos.SelectedNode.Value
        End If
    End Sub
    Protected Sub dwlmodPadreBound(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, DropDownList).Items.Insert(0, New ListItem("", "0"))
    End Sub

    Protected Sub lvwModulos_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles lvwModulos.DataBound
        'inicio(Me.Controls, "modulos")
    End Sub
    Public Sub cargarModulos(Optional ByVal SQL = "")
        Dim sSql = "select n.id_modulo,n.desc_modulo,n.url, m.desc_modulo as padre,isnull(m.id_modulo,0) as  idpadre from modulos n left join modulos m on n.id_modulo_padre=m.id_modulo "
        sqlDatos.SelectCommand = sSql
        Session("sqlModulos") = sSql
        lvwModulos.DataBind()
    End Sub
    Public Sub cargarDirModulos()
        dirModulos.Nodes.Clear()
        Dim path = Server.MapPath("~/forms")
        dirModulos.Nodes.Add(New TreeNode())
        cargarDirectorio(path, dirModulos.Nodes(0), "~/forms")
        Dim dir As New IO.DirectoryInfo(path)
        Dim archivos As IO.FileInfo() = dir.GetFiles("*.aspx")
        For Each archivo In archivos
            dirModulos.Nodes(0).ChildNodes.Add(New TreeNode(archivo.Name, obtenerDir(path, "forms") + "/" + archivo.Name))
        Next
    End Sub
    Public Sub cargarDirectorio(ByVal directorio As String, ByRef parentNode As TreeNode, ByRef URL As String)
        Dim listaDir() = Directory.GetDirectories(directorio)
        Try
            If (listaDir.Length <> 0) Then
                For Each directory In listaDir
                    Dim subDir = directory.Substring(directory.LastIndexOf("\") + 1, directory.Length - directory.LastIndexOf("\") - 1)
                    URL += "/" + subDir
                    Dim nodo = New TreeNode(subDir)
                    nodo.SelectAction = TreeNodeSelectAction.None
                    nodo.ImageUrl = "~/App_Themes/estandar/imagenes/botones/folder.png"
                    Dim dir As New IO.DirectoryInfo(directory)
                    Dim archivos As IO.FileInfo() = dir.GetFiles("*.aspx")
                    For Each archivo In archivos
                        nodo.ChildNodes.Add(New TreeNode(archivo.Name, obtenerDir(directory, "forms") + "/" + archivo.Name))
                    Next
                    parentNode.ChildNodes.Add(nodo)
                    cargarDirectorio(directory, nodo, URL)
                Next
            Else
            End If
        Catch ex As UnauthorizedAccessException
        End Try
    End Sub
    Public Function comprobarURL(ByVal url)
        If IsDBNull(url) Then
            Return "' DESCRIBA LA URL '"
        Else
            Return url
        End If
    End Function
End Class