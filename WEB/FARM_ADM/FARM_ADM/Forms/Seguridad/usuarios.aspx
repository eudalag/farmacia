﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="usuarios.aspx.vb" Inherits="FARM_ADM.usuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%;">
        <tr>
          <td colspan="2"> 
            <div class="alinearIzq" style="padding-bottom:10px;""> 
            <asp:Label ID="Label5" runat="server" Text="Usuarios:" CssClass="textoNegroNegrita" ></asp:Label> 
            <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
            </div>
          </td>
        </tr>
        <tr>
            <td class="textoGris" style="width:100px;">
                Tipos de Usuarios:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoUsuario" DataSourceID="sqlTipoUsuario"
                    DataTextField="TIPO_USUARIO" DataValueField="ID_TIPO_USUARIO"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlTipoUsuario" 
                    ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                    SelectCommand="SELECT ID_TIPO_USUARIO,TIPO_USUARIO FROM TIPO_USUARIOS 
                    UNION SELECT -1,'[Todos los Usuario]'
                    ORDER BY ID_TIPO_USUARIO "></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
             <td class="textoGris">Buscar: </td>
             <td>
                 <asp:TextBox ID="txtBuscar" runat="server" MaxLength="100" style="width:250px; "></asp:TextBox>
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar" />
             </td>
         </tr>
         <tr>
             <td class="alinearDer" colspan="2">
                 <asp:Button ID="e_btnEliminar" runat="server" OnClientClick="return eliminar()" Text="Eliminar" />
                 <asp:Button ID="a_btnNuevo" runat="server" Text="Nuevo" />
             </td>
         </tr>
        <tr>
             <td colspan="2">
                 <asp:ListView ID="lvwUsuario" runat="server" DataSourceID="sqlUsuario">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:30px;">&nbsp; </th>
                                 <th>Nombres y Apellidos </th>
                                 <th style="width:80px;">Tipo Usuario </th>
                                 <th style="width:70px;">Aplica Desc. </th>
                                  <th style="width:350px;">Direccion</th>
                                 <th style="width:60px;">Telefono </th>
                                 <th style="width:60px;">Celular </th>                                 
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>
                             <td class='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>'>
                                 <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />
                             </td>
                             <td>
                                 <asp:LinkButton ID="m_btnEditar" runat="server" CssClass='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>' CommandArgument='<%# Eval("ID_USUARIO")%>' CommandName="modificar" Text='<%# Eval("NOMBRE_APELLIDOS")%>'></asp:LinkButton>
                             </td>
                             <td class='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>'>
                                 <%# Eval("TIPO_USUARIO")%>
                             </td>
                             <td class='<%# IIf(Eval("HABILITADO") = "1", "textoNegro alinearCentro", "textoRojo alinearCentro")%>'>
                                 <%# Eval("DESCUENTO")%>
                             </td>
                             <td class='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>'>
                                 <%# Eval("DIRECCION")%>
                             </td>
                             <td class='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>'>
                                 <%# Eval("TELEFONO")%>
                             </td>
                              <td class='<%# IIf(Eval("HABILITADO") = "1", "textoGris", "textoRojo")%>'>
                                 <%# Eval("CELULAR")%>
                             </td>
                         </tr>
                     </ItemTemplate>
                 </asp:ListView>
                 <div align="right">
                     <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvwUsuario" PageSize="10">
                         <Fields>
                             <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />
                         </Fields>
                     </asp:DataPager>
                     <asp:SqlDataSource ID="sqlUsuario" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT U.ID_USUARIO, NOMBRE_APELLIDOS, TU.TIPO_USUARIO, 
                            CASE WHEN U.DESCUENTO = 1 THEN 'Si' ELSE 'No' END AS DESCUENTO, 
                            DIRECCION, ISNULL(TELEFONO,'') AS TELEFONO, ISNULL(CELULAR,'') AS CELULAR, 
                            U.HABILITADO FROM USUARIO U
                            INNER JOIN TIPO_USUARIOS TU ON TU.ID_TIPO_USUARIO = U.ID_TIPO_USUARIO 
                            WHERE U.ID_TIPO_USUARIO = CASE WHEN @TIPO = -1 THEN U.ID_TIPO_USUARIO ELSE @TIPO END
                            AND NOMBRE_APELLIDOS LIKE '%' +  @NOMBRE + '%'">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="ddlTipoUsuario" DefaultValue="-1" Name="TIPO" 
                                 PropertyName="SelectedValue" Type="Int32" />
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                         </SelectParameters>                         
                     </asp:SqlDataSource>
                 </div>
             </td>
         </tr>
     
        </table>
</asp:Content>
