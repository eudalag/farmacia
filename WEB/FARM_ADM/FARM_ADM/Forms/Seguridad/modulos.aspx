﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="modulos.aspx.vb" Inherits="FARM_ADM.modulos1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <table width="100%">
    <tr>
      <td> 
        <div class="alinearIzq"> 
        <asp:Label ID="Label5" runat="server" Text="Modulos:" CssClass="titulo" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC" />
      </div>
      </td>
    </tr>
    <tr>
      <td class="alinearDer"> 
        <asp:UpdatePanel ID="UpdatePanel3" runat="server"> 
          <ContentTemplate> 
            <asp:ImageButton ID="a_nuevoModulo" runat="server"
                             ImageUrl="~/App_Themes/estandar/imagenes/agregar.png" />
            
          </ContentTemplate> 
        </asp:UpdatePanel> 
      </td>
    </tr>
    <tr>
      <td> 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
          <ContentTemplate> 
            <asp:ListView ID="lvwModulos" DataSourceID="sqlDatos"
                          runat="server"> 
              <LayoutTemplate> 
                <table class="generica" id="tablaModulos" runat="server"
                       width="100%">
                  <tr>
                    <th style="width:3%">
                    <asp:Image ID="Image1" runat="server" style="width:15px"
                               ImageUrl="~/App_Themes/estandar/imagenes/eliminar.png" />
                    </th>
                    <th style="width:17%">
                    <asp:LinkButton ID="lnkModulo" CssClass="vinculoBlanco"
                                    CommandName="Sort"
                                    CommandArgument="DESC_MODULO"
                                    Text="Modulo" runat="server"></asp:LinkButton>
                    
                    <asp:Image runat="server" ID="SortImage1" ImageUrl=""
                               Visible="false" /> </th>
                    <th style="width:45%">
                    <asp:LinkButton ID="lnkUrl" CssClass="vinculoBlanco"
                                    CommandName="Sort" CommandArgument="URL"
                                    Text="Url" runat="server"></asp:LinkButton>
                    
                    <asp:Image runat="server" ID="SortImage2" ImageUrl=""
                               Visible="false" /> </th>
                    <th style="width:33%">
                    <asp:LinkButton ID="LinkButton1" CssClass="vinculoBlanco"
                                    CommandName="Sort"
                                    CommandArgument="PADRE"
                                    Text="Modulo Principal" runat="server"></asp:LinkButton>
                    
                    <asp:Image runat="server" ID="SortImage3" ImageUrl=""
                               Visible="false" /> </th>
                  </tr>
                  <tr id="itemPlaceHolder" />
                </table>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwModulos" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />
                      
                    </Fields> 
                  </asp:DataPager> 
                </div>
              </LayoutTemplate> 
              <ItemTemplate> 
                <tr>
                  <td class="alinearCentro"> <asp:LinkButton ID="e_LinkButton1" runat="server"
                    Text="X" CommandName ="Eliminar"
                    CommandArgument='<%#Eval("ID_MODULO")%>' Font-Bold="False"></asp:LinkButton>
                  
                  </td>
                  <td class="alinearIzq"> <asp:LinkButton
                    ID="m_LinkButton2" runat="server" CommandName = "edit"
                    Text = '<%#Eval("DESC_MODULO")%>' CommandArgument = '<%#Eval("DESC_MODULO")%>' ></asp:LinkButton> 
                  </td>
                  <td> <%#Eval("URL")%>
                  </td>
                  <td> <%#Eval("PADRE")%>
                  </td>
                </tr>
              </ItemTemplate> 
              <EditItemTemplate> 
                <td align="center"> 
                </td>
                <td> <asp:TextBox ID="UtxtNombre" CssClass = "textoGris"
                  MaxLength="50" runat="server" Text='<%#Eval("DESC_MODULO") %>'
                  Width="120px"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            ControlToValidate="UtxtNombre"
                            runat="server" Display="Dynamic"
                            ErrorMessage="Escriba un Nombre"
                            ValidationGroup="actualizar"></asp:RequiredFieldValidator>
                  </td>
                <td> <asp:LinkButton ID="UlnkURL" Text='<%# comprobarURL(Eval("url")) %>'
                  runat="server"></asp:LinkButton> 
                    <asp:ModalPopupExtender id="ModalPopupExtender"
                                                runat="server"
                                                backgroundcssclass="modalBackground"
                                                cancelcontrolid="CancelButton"
                                                popupcontrolid="Panel1"
                                                targetcontrolid="UlnkURL">
                    </asp:ModalPopupExtender>
                                
                </td>
                <td> <asp:DropDownList ID="dwlmodPadre"
                  OnDataBound="dwlmodPadreBound" DataSourceID="sqlModulos"
                  runat="server" DataTextField="desc_modulo"
                  DataValueField="ID_modulo" SelectedValue='<%# Eval("idpadre")%>'
                  Width="110"> 
                </asp:DropDownList> <asp:ImageButton ID="lnkUpdate"
                ValidationGroup="actualizar" runat="server"
                CommandArgument='<%#Eval("ID_MODULO") %>'
                CommandName="Actualizar"
                ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png"></asp:ImageButton>
              
              <asp:ImageButton ID="lnkCancel" runat="server"
                               CommandName="Cancel"
                               ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png"></asp:ImageButton>
              
                </td>
    </tr>
  </EditItemTemplate> 
  <InsertItemTemplate> 
    <td align="center" style="width:18px;"> 
    </td>
    <td> <asp:TextBox ID="ItxtNombre" Text = '<%#Eval("DESC_MODULO")%>' CssClass = "textoGris"
      MaxLength="50" runat="server" Width="120px"></asp:TextBox> 
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                ControlToValidate="ItxtNombre" runat="server"
                                ErrorMessage="Escriba un Nombre"
                                ValidationGroup="Insertar"></asp:RequiredFieldValidator>
    
    </td>
    <td><asp:LinkButton ID="IlnkURL" Text="DESCRIBA LA URL" runat="server"></asp:LinkButton>
      
      <asp:modalpopupextender id="ModalPopupExtender" runat="server"
                                      backgroundcssclass="modalBackground"
                                      cancelcontrolid="CancelButton"
                                      popupcontrolid="Panel1"
                                      targetcontrolid="IlnkURL"></asp:modalpopupextender>
      
    </td>
    <td> 
      <asp:DropDownList ID="dwlmodPadre" OnDataBound="dwlmodPadreBound"
                        DataSourceID="sqlModulos" runat="server"
                        DataTextField="desc_modulo"
                        DataValueField="ID_modulo" Width="110"> 
      </asp:DropDownList> 
      <asp:ImageButton ID="lnkUpdate" ValidationGroup="Insertar"
                       runat="server" CommandName="Insertar"
                       ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png"></asp:ImageButton>
      
      <asp:ImageButton ID="lnkCancel" runat="server" CommandName="Cancel"
                       ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png"></asp:ImageButton>
      
    </td>
    </tr>
  </InsertItemTemplate> 
  <EmptyDataTemplate> 
    <div class="textoGris"> NO EXISTEN DATOS...... 
    </div>
  </EmptyDataTemplate> 
</asp:ListView> 
              <asp:SqlDataSource 
         ID="sqlDatos" 
        runat="server"
        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>"         >
</asp:SqlDataSource> <asp:SqlDataSource 
         ID="sqlModulos" 
        runat="server"
        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
        SelectCommand="select * from modulos where ID_modulo_padre is null" >
</asp:SqlDataSource> 
</ContentTemplate> 
</asp:UpdatePanel> 
      </td>
    </tr>
    <tr>
      <td> 
        <asp:UpdatePanel ID="UpdatePanel2" runat="server"> 
          <ContentTemplate> 
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup"
                       Style="display: none" Width="300px" Height="500px"> 
              <div> 
                <table align="center" width="100%">
                  <tr>
                    <td colspan="2" style="text-align:center"> 
                      <asp:Label ID="Label4" runat="server"
                                 Text=" SELECIONE URL DEL MODULO"
                                 Font-Bold="True"></asp:Label> 
                    </td>
                  </tr>
                  <tr>
                    <td> 
                      <asp:Label ID="Label3" runat="server" Text="Url:"
                                 Font-Bold="True"></asp:Label> 
                    </td>
                    <td> 
                      <div style=" border:solid 1px black;  Height:430px;Width:250px">
                        
                        <asp:TreeView ID="dirModulos" runat="server"
                                      ShowLines="True" ExpandDepth="1"> 
                          <HoverNodeStyle Font-Underline="True"
                                          ForeColor="#5555DD" /> 
                          <SelectedNodeStyle Font-Underline="True"
                                             ForeColor="#5555DD"
                                             HorizontalPadding="0px"
                                             VerticalPadding="0px" /> 
                          <NodeStyle Font-Names="Verdana" Font-Size="8pt"
                                     ForeColor="Black"
                                     HorizontalPadding="5px"
                                     NodeSpacing="0px" VerticalPadding="0px" />
                          
                        </asp:TreeView> 
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td> 
                      <asp:Label ID="Label2" runat="server" Text="Otro :"
                                 Font-Bold="True"></asp:Label> 
                    </td>
                    <td align="left"> 
                      <asp:TextBox ID="TextBox1" runat="server"
                                   Width="220px"></asp:TextBox> 
                    </td>
                  </tr>
                </table>
                <div class="alinearDer" style="padding-right:20px;"> 
                  <asp:ImageButton ID="OkButton"
                                   ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png"
                                   runat="server" /> 
                  <asp:ImageButton ID="CancelButton"
                                   ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png"
                                   runat="server" /> 
                </div>
              </div>
            </asp:Panel> 
          </ContentTemplate> 
        </asp:UpdatePanel> 
      </td>
    </tr>
  </table>
             <div class="alinearCentro subTitulo"> 
          <asp:Label ID="Label1" CssClass="subTitulo" runat="server"
                     Text="Lista de modulos. seleccione el botón (+) para agregar mas modulos a la lista.Seleccione el Nombre para modificar los datos del mismo. Selecciona el boton (X) para eliminar los datos del modulo."></asp:Label>
          
        </div>
</asp:Content>
