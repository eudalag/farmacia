﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionRoles.aspx.vb" Inherits="FARM_ADM.gestionRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="/App_Themes/estilos/cssRoles/gridRoles.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript">
      function cambiar(obj) {
          var idObj = obj.id.substring(obj.id.lastIndexOf("_") + 1, obj.id.length)
          var inputElements = document.getElementsByTagName('input');
          for (var i = 0; i < inputElements.length; i++) {
              var myElement = inputElements[i];
              if (myElement.type == "checkbox") {
                  if (myElement.id.indexOf(idObj) > 0 && myElement.id.indexOf("lvwSubmodulos") > 0 && myElement.value == obj.value) {
                      myElement.checked = obj.checked;
                      myElement.disabled = !obj.checked;
                  }
              }
          }
      }
      function toggleGroup(img, numberOfRows) {
          var tr = img.parentNode.parentNode;
          var table = $get('orders');
          var src = img.src;
          var startIndex = tr.rowIndex + 1;
          var stopIndex = startIndex + parseInt(numberOfRows);
          if (src.endsWith('plus.png')) {
              for (var i = startIndex; i < stopIndex; i++) {
                  Sys.UI.DomElement.removeCssClass(table.rows[i], 'hidden');
              }
              src = src.replace('plus.png', 'minus.png');
          }
          else {
              for (var i = startIndex; i < stopIndex; i++) {
                  Sys.UI.DomElement.addCssClass(table.rows[i], 'hidden');
              }
              src = src.replace('minus.png', 'plus.png');
          }
          img.src = src;
          n = 1;
      }
      function verificar(obj, n) {
          if (n == 0)
              obj.style.display = "none";
      }



  </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"> 
  </asp:ScriptManager> 
  <br /> 
    <div class="alinearIzq"> 
      <asp:Label ID="Label2" runat="server" Text="Roles:" CssClass="tituloGris"> 
      </asp:Label> 
      <hr align="left" style="height:-12px;color:#CCCCCC" width="30%" /> 
      <br /> 
    </div>
  <br /> 
  <div class="alinearIzq"> 
    <asp:Label ID="lblTitulo" runat="server" Text="Rol:"></asp:Label> 
    <span style="padding-left : 30px;"> 
    <asp:TextBox ID="txtRol" runat="server" MaxLength="30" Width="170px"></asp:TextBox> 
  <asp:RequiredFieldValidator ID="rfvRoles" runat="server" Display="Dynamic" ErrorMessage="<b>Campo Requerido:</b>Rellene descripción de nuevo rol." ControlToValidate="txtRol" /> 
  </span> 
  </div>
  <br /> 
  <div class="alinearCentro"> 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
      <ContentTemplate> 
        <div style="width:100%;"> 
          <asp:ListView ID="lvwModulos" runat="server"> 
            <LayoutTemplate> 
              <div class="grid"> 
                <h2>MODULOS</h2> 
                <table cellpadding="0" cellspacing="0" id="orders" width="100%">
                  <tr id="itemPlaceholder" runat="server" />
                </table>
              </div>
            </LayoutTemplate> 
            <ItemTemplate> 
              <tr class="group" id="row" runat="server">
                <th class="first"><img src="/App_Themes/estilos/cssRoles/img/plus.png" alt='Group: <%# Eval("ID_MODULO")%>' onload="javascript:verificar(this,'<%# Eval("count") %>');" onclick="toggleGroup(this, '<%# Eval("count") %>');" /> </th>
                <th><%# Eval("padre")%><asp:HiddenField ID="hdfcodModulo" runat="server" Value='<%#Eval("ID_MODULO") %>' EnableViewState = "true"/> </th>
                <th>
                <table>
                  <tr style="border :none;">
                    <td class="alinearIzq" style="width: 10%; border: none;"> <%--<asp:CheckBox ID="chkAcceso"  Checked=' <%# Eval("ACCESO") %>' CssClass='<%# Eval("ID_MODULO")%>' OnClick='cambiar(this);' runat="server" Text="Acceso" />--%> 
                    <input id="chkAcceso" class='<%# "1" & Eval("ID_MODULO")%>' checked = ' <%# Eval("ACCESO") %>' onclick='cambiar(this);' runat="server" value='<%# Eval("ID_MODULO")%>' type="checkbox" />Acceso 
                    </td>
                    <td class="alinearIzq"  style='width: 10%;<%# iif(Eval("ID_MODULO")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkAlta" runat="server" Checked=' <%# Eval("ALTA") %>' CssClass='<%# Eval("ID_MODULO")%>' OnClick='cambiar(this);' Text="Alta" /> --%> 
                    <input id="chkAlta" class='<%# "2" & Eval("ID_MODULO")%>' checked = ' <%# Eval("ALTA") %>' onclick='cambiar(this);' runat="server" value='<%# Eval("ID_MODULO")%>' type="checkbox" />Alta 
                    </td>
                    <td class="alinearIzq"  style='width: 10%;<%# iif(Eval("ID_MODULO")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkModificacion" runat="server" Checked=' <%# Eval("MODIFICACION") %>' CssClass='<%# Eval("ID_MODULO")%>' OnClick="cambiar(this);" Text="Modificación" /> --%> 
                    <input id="chkModificacion" class='<%# "3" & Eval("ID_MODULO")%>' checked = ' <%# Eval("MODIFICACION") %>' onclick='cambiar(this);' runat="server" value='<%# Eval("ID_MODULO")%>' type="checkbox" />Modificación 
                    </td>
                    <td class="alinearIzq"  style='width: 10%;<%# iif(Eval("ID_MODULO")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkEliminacion" runat="server" Checked=' <%# Eval("ELIMINACION") %>' CssClass='<%# Eval("ID_MODULO")%>' OnClick="cambiar(this);" Text="Eliminación" /> --%> 
                    <input id="chkEliminacion" class='<%# "4" & Eval("ID_MODULO")%>' checked = ' <%# Eval("ELIMINACION") %>' onclick='cambiar(this);' runat="server" value='<%# Eval("ID_MODULO")%>' type="checkbox" />Eliminación 
                    </td>
                  </tr>
                </table></th>
              </tr>
              <asp:ListView ID="lvwSubmodulos" runat="server" DataSource='<%# Eval("hijos") %>'> 
              <LayoutTemplate> 
                <tr id="itemPlaceholder" runat="server" />
              </LayoutTemplate> 
              <ItemTemplate> 
                <tr class="item hidden" id="row" runat="server">
                  <td class="first"></td>
                  <td> <%# Eval("nombre") %><asp:HiddenField ID="hdfcodModulo" runat="server" Value='<%#Eval("hijo") %>' EnableViewState = "true"/> 
                  </td>
                  <td> 
                    <table width="100%">
                      <tr style="border :none;">
                        <td class="alinearIzq" style="width: 50%; border: none;"> <%--<asp:CheckBox ID="chkAcceso"  Checked=' <%# Eval("ACCESO") %>' CssClass='<%# Eval("ID_MODULO_PADRE")%>' runat="server" Text="Acceso" /> --%> 
                        <input id="chkAcceso" class='<%# "1" & Eval("ID_MODULO_PADRE")%>' onchange='<%# "cambiarPadre(1" & Eval("ID_MODULO_PADRE") & ")" %>' checked = ' <%# Eval("ACCESO") %>' disabled='<%# Not(Eval("ACCESO_CHK")) %>' runat="server" value='<%# Eval("ID_MODULO_PADRE")%>' type="checkbox" />Acceso 
                        </td>
                        <td class="alinearIzq" style='width: 50%; border: none;<%# iif(Eval("ID_MODULO_PADRE")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkAlta" Checked=' <%# Eval("ALTA") %>' CssClass='<%# Eval("ID_MODULO_PADRE")%>' runat="server" Text="Alta" />--%>
                         <input id="chkAlta" class='<%# "2" & Eval("ID_MODULO_PADRE")%>' onchange='<%# "cambiarPadre(2" & Eval("ID_MODULO_PADRE") & ")" %>' checked = ' <%# Eval("ALTA") %>' disabled='<%# Not(Eval("ALTA_CHK")) %>' runat="server" value='<%# Eval("ID_MODULO_PADRE")%>' type="checkbox" />Alta 
                        </td>
                      </tr>
                      <tr style="border :none;">
                        <td class="alinearIzq" style='width: 50%; border: none;<%# iif(Eval("ID_MODULO_PADRE")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkModificacion" Checked=' <%# Eval("MODIFICACION") %>' CssClass='<%# Eval("ID_MODULO_PADRE")%>' runat="server" Text="Modificación" /> --%> 
                        <input id="chkModificacion" class='<%# "3" & Eval("ID_MODULO_PADRE")%>' onchange='<%# "cambiarPadre(3" & Eval("ID_MODULO_PADRE") & ")" %>' checked = ' <%# Eval("MODIFICACION") %>' disabled='<%# Not(Eval("MODIFICACION_CHK")) %>' runat="server" value='<%# Eval("ID_MODULO_PADRE")%>' type="checkbox" />Modificación 
                        </td>
                        <td class="alinearIzq" style='width: 50%; border: none;<%# iif(Eval("ID_MODULO_PADRE")=20,"visibility:hidden","")%>'> <%--<asp:CheckBox ID="chkEliminacion" Checked=' <%# Eval("ELIMINACION") %>' CssClass='<%# Eval("ID_MODULO_PADRE")%>' runat="server" Text="Eliminación" /> --%> 
                        <input id="chkEliminacion" class='<%# "4" & Eval("ID_MODULO_PADRE")%>' onchange='<%# "cambiarPadre(4" & Eval("ID_MODULO_PADRE") & ")" %>'  checked = ' <%# Eval("ELIMINACION") %>' disabled='<%# Not(Eval("ELIMINACION_CHK")) %>' runat="server" value='<%# Eval("ID_MODULO_PADRE")%>' type="checkbox" />Eliminación 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </ItemTemplate> 
            </asp:ListView> 
          </ItemTemplate> 
        </asp:ListView> 
        </div>
      </ContentTemplate> 
    </asp:UpdatePanel> 
  </div>
  <br /> 
  <div class="alinearDer"> 
    <asp:ImageButton ID="btnAceptar" runat="server" ImageUrl="~/App_Themes/estandar/imagenes/aceptar.png" ToolTip="Aceptar" /> 
    <asp:ImageButton ID="btnCancelar" runat="server" ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png" ToolTip="Cancelar" CausesValidation="false" /> 
  </div>
  <div class="alinearCentro subTitulo"> Seleccione el modulo y los permisos correspondiente para el rol seleccionado.<br /> 
  </div>
  <script type="text/javascript">
      function cambiarPadre(tipo) {
          cantidad = 0;
          $("." + tipo).each(function () {
              if (this.checked)
                  cantidad += 1;
          });
          if (cantidad == 1)
              $("." + tipo).each(function () {
                  this.checked = false;
              });
      }
      //      function confirmacion(msg, titulo) {
      //          $(".delete").each(function() {
      //              var btn = $(this).find(".cmdeliminar");
      //              $(this).find(".eliminar").click(function(e) {
      //                  e.preventDefault();
      //                  jConfirm(msg, titulo, function(r) {
      //                      if (r)
      //                          $(btn).click();
      //                  });
      //              });
      //          });
      //          return false;
      //      }
      //      function alert(msg, titulo) {
      //          try {
      //              jAlert(msg, titulo);
      //          } catch (e) {
      //              alert(msg);
      //          }
      //      }
  </script> 
</asp:Content>
