﻿Imports System.Data.SqlClient
Public Class permisos1
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub permisos1_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            inicio(Me.Controls, "Permisos", 41)
            '   CType(Master.FindControl("panelMenuH"), Panel).Controls.Add(crearMenu(getRequest("idmenu_p")))
            cargarUSUARIO()
            cargarRoles()
        End If
        inicio(Me.Controls, "Permisos", 41)
    End Sub
    Protected Sub imgaceptar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles a_imgaceptar.Click
        Dim sqlPermisos = New mcTabla
        For cont = 0 To lvwRoles.Items.Count - 1
            Dim rol As CheckBox = CType(lvwRoles.Items(cont).FindControl("m_chkRol"), CheckBox)
            Dim idRol As String = CStr(CType(lvwRoles.Items(cont).FindControl("hdId"), HiddenField).Value)
            Dim ID_USUARIO = dwlusuarios.SelectedValue
            Dim existeRol = tieneRol(ID_USUARIO, idRol)
            If (rol.Checked) Then
                If (Not existeRol) Then sqlPermisos.ejecutarSQL("INSERT INTO PERMISOS (ID_USUARIO, ID_ROL) VALUES(" & ID_USUARIO & " ," & idRol & ")")
            Else
                If (existeRol) Then sqlPermisos.ejecutarSQL("DELETE PERMISOS  WHERE ID_USUARIO=" & ID_USUARIO & " AND ID_ROL=" & idRol)
            End If
            Dim drt As SqlDataReader
            Dim cTabla As New mcTabla
            cTabla.tabla = "USUARIO"
            cTabla.campoID = "ID_USUARIO"
            cTabla.valorID = dwlusuarios.SelectedValue
            drt = cTabla.obtenerRegistro()
            drt.Read()
            Dim APELLIDO_NOMBRE = esnulo(drt("USUARIO"))
            Mensaje("Se asignaron los permisos al usuario " & APELLIDO_NOMBRE, "Permisos")
        Next
    End Sub
    Public Function tieneRol(ByVal ID_USUARIO, ByVal idRol) As Boolean
        Return CType(New mcTabla().ejecutarConsulta("SELECT * FROM PERMISOS WHERE ID_USUARIO=" & ID_USUARIO & " AND ID_ROL=" & idRol), DataTable).Rows.Count <> 0
    End Function
    Public Sub cargarUSUARIO()
        dwlusuarios.DataSource = New mcTabla().ejecutarConsulta("SELECT * FROM USUARIO WHERE HABILITADO='true' ORDER BY USUARIO")
        dwlusuarios.DataTextField = "USUARIO"
        dwlusuarios.DataValueField = "ID_USUARIO"
        dwlusuarios.DataBind()
    End Sub
    Public Sub cargarRoles()
        Dim sql = "select r.id_rol,r.desc_rol, CASE  ISNULL(p.id_rol,0) WHEN 0 THEN 'false' ELSE 'true' END  as rol from roles r left join permisos  p on r.id_rol=p.id_rol and p.ID_USUARIO=" & dwlusuarios.SelectedValue & " ORDER BY r.desc_rol"
        lvwRoles.DataSource = New mcTabla().ejecutarConsulta(sql)
        lvwRoles.DataBind()
    End Sub
    Protected Sub dwlUSUARIO_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dwlusuarios.SelectedIndexChanged
        cargarRoles()
    End Sub
    Public Function esAdmin(ByVal IDrol, ByVal estado) As Boolean
        If (dwlusuarios.SelectedValue <> 1) Then Return True Else Return CInt(IDrol) <> 1
    End Function
    Protected Sub lvwRoles_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles lvwRoles.DataBound
        inicio(Me.Controls, "Permisos", 41)
    End Sub
    Function esnulo(ByVal x As Object) As Object
        If IsDBNull(x) Then
            Return ""
        Else
            Return x
        End If
    End Function
End Class