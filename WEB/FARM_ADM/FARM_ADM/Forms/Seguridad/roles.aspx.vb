﻿Public Class roles1
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub lvwRoles_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvwRoles.ItemCommand
        Select Case (e.CommandName)
            Case "eliminar"    'B 
                Dim ID_Rol As String = CStr(e.CommandArgument)
                Dim msg As String = ""
                If True Then 'puedeEliminar("ROLES", ID_Rol, msg) Then
                    Dim borrarRol As New mcTabla

                    borrarRol.campoID = "ID_ROL"
                    borrarRol.valorID = ID_Rol
                    borrarRol.tabla = "PERMISOS"
                    borrarRol.eliminar()

                    borrarRol.campoID = "ID_ROL"
                    borrarRol.valorID = ID_Rol
                    borrarRol.tabla = "ROLES_MODULOS"
                    borrarRol.eliminar()

                    borrarRol.campoID = "ID_ROL"
                    borrarRol.valorID = ID_Rol
                    borrarRol.tabla = "ROLES"
                    borrarRol.eliminar()

                    lvwRoles.DataBind()
                Else
                    Mensaje("Error: NO SE PUEDE ELIMINAR, EXISTEN ROLES RELACIONADOS", "Roles")
                End If
            Case "modificar" 'M                 
                Response.Redirect("gestionRoles.aspx?codRol=" & e.CommandArgument & "&idMenu=" & Request.QueryString("idmenu"))
        End Select
    End Sub

    Protected Sub lvwRoles_ItemEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewEditEventArgs) Handles lvwRoles.ItemEditing
        lvwRoles.InsertItemPosition = InsertItemPosition.None
    End Sub

    Protected Sub lvwRoles_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewSortEventArgs) Handles lvwRoles.Sorting
        ordenarTabla(sender, e)
    End Sub

    Protected Sub lvwRoles_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles lvwRoles.DataBound
        'inicio(Me.Controls, "Roles")
    End Sub
    Private Sub a_btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles a_btnAceptar.Click
        Dim ID_Rol As String
        Dim nuevoRol As New mcTabla
        nuevoRol.campoID = "ID_ROL"
        nuevoRol.modoID = "sql"
        nuevoRol.tabla = "ROLES"
        nuevoRol.agregarCampoValor("DESC_ROL", txtDescripcion.Text)
        nuevoRol.insertar()
        ID_Rol = nuevoRol.valorID
        Response.Redirect("gestionRoles.aspx?codRol=" & ID_Rol & "&idmenu=" & Request.QueryString("idmenu") & "&idmenu_p=" & Request.QueryString("idmenu_p"))
    End Sub
End Class