﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="informe_venta.aspx.vb" Inherits="FARM_ADM.informe_venta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <style type="text/css">
    .overlay 
        {
          position: fixed;
          z-index: 98;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
            background-color: #aaa;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }
        .overlayContent
        {
          z-index: 99;
          margin: 270px auto;
          width: 300px;
          height: 80px;
        }  
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table style="width:100%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="INFORME DE VENTAS:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" >
             Caja:
         </td>
         <td colspan="3">
            <asp:DropDownList runat="server" ID="ddlCaja" DataSourceID="sqlCaja"
                DataTextField="CAJA" DataValueField="ID_BANCO"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlCaja" 
                                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                SelectCommand="SELECT ID_BANCO,ENTIDAD_FINANCIERA + ' - ' + U.NOMBRE_APELLIDOS AS CAJA  FROM BANCO B 
                                                INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO 
                                                WHERE ID_TIPO_BANCO  = 1
                                                UNION ALL SELECT -1, '[Todos las cajas]'"></asp:SqlDataSource>
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:50px; ">
             Pago:
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlPago">
                <asp:ListItem Selected="true"  Text="[Todos los Pagos]" Value="-1"></asp:ListItem>
                <asp:ListItem  Text="Efectivo" Value="1"></asp:ListItem>
                <asp:ListItem  Text="Credito" Value="2"></asp:ListItem>
            </asp:DropDownList>                
         </td>
        <td class="textoGris" >
             Comprobante:
         </td>
         <td>
            <asp:DropDownList runat="server" ID="ddlTipoComprobante" DataSourceID="sqlTipoComprobante"
                DataTextField="DETALLE" DataValueField="ID_TIPO_COMPROBANTE"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlTipoComprobante" 
                                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                SelectCommand="SELECT ID_TIPO_COMPROBANTE, DETALLE FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE IN (1,2)
UNION ALL SELECT 3, 'OTROS INGRESOS'
UNION ALL SELECT -1, '[Todos los Tipos de Comprobante]'
ORDER BY DETALLE"></asp:SqlDataSource>
         </td>
     </tr>  
    <tr>
        <td colspan="4" class="textoGris">
            Solo Ventas con Receta 
            <asp:CheckBox ID="chkCredito" runat="server" />
        </td>
    </tr>
   <tr>
        <td class="textoGris" style="width:100px; ">
            Desde:
        </td>
        <td style="width:130px; ">
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris" style="width:50px; ">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>
            <asp:Button runat="server" ID="btnCargar" Text="Buscar" />
        </td>
    </tr>
    <tr>
        <td colspan="4" class="alinearDer">
            <asp:ImageButton runat="server" ID="exportarExcel" ToolTip="Exportar Excel" OnClientClick=" return exportarExcel();" ImageUrl="~/App_Themes/estandar/imagenes/exportar.png"  />
        </td>
    </tr>
        <tr>
            <td colspan="4">
                <div style="overflow: scroll; width: 100%; height: 300px;">
                <asp:ListView runat="server" ID="lvwMovimientos" DataSourceID="sqlMovimientos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th style="width:70px;">
                                    Fecha
                                </th>
                                <th style="width:120px;">
                                    Tipo Comprobante
                                </th>
                                <th style="width:100px;">
                                    Tipo Pago
                                </th>
                                <th>
                                    Nombre Apellidos
                                </th>
                                <th style="width:90;">
                                    Nro. Comp.
                                </th>
                                <th style="width:80;">
                                    Total
                                </th>
                                <th style="width:50px; display:none;">
                                    &nbsp;
                                </th>
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="textoGris">
                                <%# String.Format(New System.Globalization.CultureInfo("es-ES"),"{0:d}",Eval("FECHA"))%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("TIPO_COMPROBANTE")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("TIPO_PAGO")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("NOMBRE_APELLIDOS")%>
                            </td>
                            <td class="textoGris ">
                                <%# Eval("NRO_VENTA")%>
                            </td>
                            <td class="textoGris alinearDer total">                                
                                <%# String.Format(New System.Globalization.CultureInfo("en-US"),"{0:n}",Eval("TOTAL_PEDIDO"))%>
                            </td>
                            <td class="alinearCentro" style="display:none;">
                                
                                <asp:Button runat="server" ID="btnVer" Text="Ver" CommandArgument='<%# Eval("ID_VENTA") %>' CommandName="ver" />
                                <asp:HiddenField runat="server" ID="hdTipo" Value='<%# Eval("TIPO") %>' />                                
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
 <table class="generica" style="width:100%;">
                            <tr>
                                 <th style="width:80px;">
                                    Fecha
                                </th>
                                <th style="width:120px;">
                                    Tipo Comprobante
                                </th>
                                <th style="width:100px;">
                                    Tipo Pago
                                </th>
                                <th>
                                    Nombre Apellidos
                                </th>
                                <th style="width:90;">
                                    Nro. Comp.
                                </th>
                                <th style="width:80;">
                                    Total
                                </th>
                                <th style="width:50px;">
                                    &nbsp;
                                </th>
                            </tr>
                            <tr>
                                <td colspan="7" class="textoGris">
                                    No Existe Movimiento.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>                 
                    <asp:SqlDataSource runat="server" ID="sqlMovimientos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM (
SELECT V.FECHA,TC.ID_TIPO_COMPROBANTE ,TC.DETALLE AS TIPO_COMPROBANTE,V.CREDITO, CASE WHEN V.CREDITO = 1 THEN 'CREDITO' ELSE 'EFECTIVO' END AS TIPO_PAGO,
CASE WHEN V.CREDITO = 1 THEN PCR.NOMBRE_APELLIDOS  ELSE P.NOMBRE_APELLIDOS END AS NOMBRE_APELLIDOS,V.NRO_VENTA,V.TOTAL_PEDIDO,V.ID_VENTA,B.ID_BANCO,V.ID_RECETA , 1 AS TIPO FROM VENTA V 
INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE =V.ID_TIPO_COMPROBANTE 
INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = V.ID_CLIENTE 
INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
INNER JOIN BANCO B ON B.ID_BANCO = V.ID_BANCO
LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA 
LEFT JOIN CLIENTE CLC ON CLC.ID_CLIENTE = CR.ID_CLIENTE
LEFT JOIN PERSONA PCR ON PCR.ID_PERSONA = CLC.ID_PERSONA  
WHERE V.ANULADA = 0
UNION ALL
SELECT CONVERT(DATE,FH_MOVIMIENTO,103) AS FECHA,3, 'OTRO INGRESO' AS TIPO_COMPROBANTE,0,'EFECTIVO' AS TIPO_PAGO,
M.GLOSA,M.ID_MOVIMIENTO,M.IMPORTE,M.ID_MOVIMIENTO, B.ID_BANCO,NULL,2 AS TIPO FROM MOVIMIENTO_BANCO M
INNER JOIN BANCO B ON B.ID_BANCO = M.ID_BANCO 
WHERE ID_TIPO_MOVIMIENTO = 1) VENTA 
WHERE ID_BANCO = CASE WHEN @BANCO = -1 THEN ID_BANCO ELSE @BANCO END
AND ISNULL(CREDITO,0) = CASE WHEN @PAGO = -1 THEN CREDITO WHEN @PAGO = 1 THEN 0 ELSE 1 END
AND ID_TIPO_COMPROBANTE  = CASE WHEN @COMPROBANTE = -1 THEN ID_TIPO_COMPROBANTE ELSE @COMPROBANTE END
AND CASE WHEN @RECETA = 'True' THEN ISNULL(ID_RECETA,0) ELSE 0 END  = CASE WHEN @RECETA = 'True' THEN ISNULL(ID_RECETA,1) ELSE 0 END
AND CONVERT(DATE,FECHA,103) >= @DESDE AND CONVERT(DATE,FECHA,103) <= @HASTA">                        
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCaja" Name="BANCO" 
                                PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlPago" Name="PAGO" 
                                                        PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlTipoComprobante" Name="COMPROBANTE" 
                                                        PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="txtDesde" Name="DESDE" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtHasta" Name="HASTA" PropertyName="Text" />
                            <asp:ControlParameter ControlID="chkCredito" Name="RECETA" 
                                PropertyName="Checked" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="alinearDer textoNegroNegrita">
                <br />
                Total:&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true" CssClass="alinearDer textoGris "></asp:TextBox>
            </td>
        </tr>
        </table>
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" >
        <ProgressTemplate>
            <div class="overlay" />
            <div class="overlayContent">
                <strong class="textoNegro">Procesando, Un Momento por Favor ...</strong>
                <img src="../../App_Themes/estandar/imagenes/cargando.gif" alt="Loading" border="0" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
<script type="text/javascript">
    function iniciarFecha() {
        jQuery.noConflict();
        jQuery("#<%=txtDesde.ClientID %>").datepicker({});
        jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
    }
    function formatNumber(num) {
        num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
        splitRight = splitRight + '00';
        splitRight = splitRight.substr(0, 3);
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
        }
        return splitLeft + splitRight;
    }
    function ReplaceAll(Source, stringToFind, stringToReplace) {
        var temp = Source;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    }
    function calcularTotal() {
        var subTotal = 0;
        jQuery(".total").each(function () {
            subTotal = subTotal + parseFloat(ReplaceAll(jQuery(this).html(), ",", ""));
        });

        jQuery("#<%= txtTotal.ClientID%>").val(formatNumber(subTotal));
        iniciarFecha()
    }
    function exportarExcel() {
        var url = "../reporte/excel.aspx?TIPO=INFORME_VENTA&DESDE=" + jQuery("#<%=txtDesde.ClientID %>").val() + "&HASTA=" + jQuery("#<%=txtHasta.ClientID %>").val() + "&CAJA=" + jQuery("#<%=ddlCaja.ClientID %>").val() + "&PAGO=" + jQuery("#<%=ddlPago.ClientID %>").val() + "&COMPROBANTE=" + jQuery("#<%=ddlTipoComprobante.ClientID %>").val() + "&RECETA=" + jQuery("#<%=chkCredito.ClientID %>").attr('checked');
        //alert(url);
        window.open(url);
        return false;
    }
</script> 
</asp:Content>
