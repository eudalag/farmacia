﻿Public Class formCreditoCobrar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub formCreditoCobrar_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "fecha", "calcularTotal();", True)
        End If
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        lvwMovimientos.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType, "fecha", "calcularTotal();", True)
    End Sub
End Class