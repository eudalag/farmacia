﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="formCreditoPagar.aspx.vb" Inherits="FARM_ADM.formCreditoPagar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
<style type="text/css">
    .overlay 
        {
          position: fixed;
          z-index: 98;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
            background-color: #aaa;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }
        .overlayContent
        {
          z-index: 99;
          margin: 270px auto;
          width: 300px;
          height: 80px;
        }  
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<table style="width:100%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="INFORME DE CUENTAS POR PAGAR:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
    
       <tr>
        <td class="textoGris" style="width:100px; ">
            Proveedor:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtCliente"  style="width:200px;"></asp:TextBox>        
            <asp:Button runat="server" ID="btnCargar" Text="Buscar" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="alinearDer">
            <asp:ImageButton runat="server" ID="exportarExcel" ToolTip="Exportar Excel" OnClientClick=" return exportarExcel();" ImageUrl="~/App_Themes/estandar/imagenes/exportar.png"  />
        </td>
    </tr>
        <tr>
            <td colspan="4">
                <div style="overflow: scroll; width: 100%; height: 300px;">
                <asp:ListView runat="server" ID="lvwMovimientos" DataSourceID="sqlMovimientos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Proveedor
                                </th>
                                <th style="width:100px;">
                                    Tipo Comprobante
                                </th>
                                <th style="width:300px;">
                                    Nro. Comprobante    
                                </th>
                                <th style="width:120px;">
                                    Importe a Pagar
                                </th>                                                                
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="textoGris">
                                <%# Eval("NOMBRE")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("DETALLE")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("NRO_COMPROBANTE")%>
                            </td>
                            
                            <td class="textoGris alinearDer total">                                
                                <%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", Eval("SALDO"))%>
                            </td>                            
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
 <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Proveedor
                                </th>
                                <th style="width:100px;">
                                    Tipo Comprobante
                                </th>
                                <th style="width:300px;">
                                    Nro. Comprobante    
                                </th>
                                <th style="width:120px;">
                                    Importe a Pagar
                                </th>                                                                
                            </tr>
                            <tr>
                                <td colspan="4" class="textoGris">
                                    No Existe Movimiento.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>                 
                    
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="alinearDer textoNegroNegrita">
                <br />
                Total:&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true" CssClass="alinearDer textoGris "></asp:TextBox>
            </td>
        </tr>
        </table>
<asp:SqlDataSource runat="server" ID="sqlMovimientos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT P.ID_PROVEEDOR,CC.ID_COMPRA_CREDITO  ,P.NOMBRE,TC.DETALLE,C.NRO_COMPROBANTE,
CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103) AS F_VENCIMIENTO,CC.SALDO  FROM COMPRA_CREDITO CC 
INNER JOIN COMPRA C ON C.ID_COMPRA = CC.ID_COMPRA 
INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR 
INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE 
WHERE NOMBRE LIKE '%' + @BUSCAR + '%' AND SALDO > 0
ORDER BY F_vENCIMIENTO DESC ">                        
                        
                    <SelectParameters>
                            <asp:ControlParameter ControlID="txtCliente" Name="BUSCAR" PropertyName="Text" DefaultValue="%" />
                        </SelectParameters>
                        
                    </asp:SqlDataSource>
</ContentTemplate>
</asp:UpdatePanel>
 <script type="text/javascript">
     function formatNumber(num) {
         num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
         num += '';
         var splitStr = num.split('.');
         var splitLeft = splitStr[0];
         var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
         splitRight = splitRight + '00';
         splitRight = splitRight.substr(0, 3);
         var regx = /(\d+)(\d{3})/;
         while (regx.test(splitLeft)) {
             splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
         }
         return splitLeft + splitRight;
     }
     function ReplaceAll(Source, stringToFind, stringToReplace) {
         var temp = Source;
         var index = temp.indexOf(stringToFind);
         while (index != -1) {
             temp = temp.replace(stringToFind, stringToReplace);
             index = temp.indexOf(stringToFind);
         }
         return temp;
     }
     function calcularTotal() {
         var subTotal = 0;
         jQuery(".total").each(function () {
             subTotal = subTotal + parseFloat(ReplaceAll(jQuery(this).html(), ",", ""));
         });

         jQuery("#<%= txtTotal.ClientID%>").val(formatNumber(subTotal));
     }
     function exportarExcel() {
         var url = "../reporte/excel.aspx?TIPO=CUENTAS_PAGAR&PROVEEDOR=" + jQuery("#<%=txtCliente.ClientID %>").val();
         //alert(url);
         window.open(url);
         return false;
     }
</script>   
</asp:Content>
