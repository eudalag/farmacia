﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="formCreditoCobrar.aspx.vb" Inherits="FARM_ADM.formCreditoCobrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
<style type="text/css">
    .overlay 
        {
          position: fixed;
          z-index: 98;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
            background-color: #aaa;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }
        .overlayContent
        {
          z-index: 99;
          margin: 270px auto;
          width: 300px;
          height: 80px;
        }  
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<table style="width:100%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="INFORME DE CUENTAS POR COBRAR:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" >
             Grupo de Cliente:
         </td>
         <td>
            <asp:DropDownList runat="server" ID="ddlGrupoPersona" DataSourceID="sqlGrupoPersona"
                DataTextField="GRUPO_PERSONAS" DataValueField="ID_GRUPO"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlGrupoPersona" 
                                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                SelectCommand="SELECT  ID_GRUPO, GRUPO_PERSONAS  FROM GRUPO_CLIENTES                                                                                                  
                                UNION ALL SELECT -1, '[Todos los Grupos]'
                                ORDER BY GRUPO_PERSONAS "></asp:SqlDataSource>
         </td>
     </tr>
       <tr>
        <td class="textoGris" style="width:100px; ">
            Cliente:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtCliente"  style="width:200px;"></asp:TextBox>        
            <asp:Button runat="server" ID="btnCargar" Text="Buscar" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="alinearDer">
            <asp:ImageButton runat="server" ID="exportarExcel" ToolTip="Exportar Excel" OnClientClick=" return exportarExcel();" ImageUrl="~/App_Themes/estandar/imagenes/exportar.png"  />
        </td>
    </tr>
        <tr>
            <td colspan="4">
                <div style="overflow: scroll; width: 100%; height: 300px;">
                <asp:ListView runat="server" ID="lvwMovimientos" DataSourceID="sqlMovimientos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Cliente
                                </th>
                                <th style="width:100px;">
                                    Celular
                                </th>
                                <th style="width:300px;">
                                    Grupo
                                </th>
                                <th style="width:120px;">
                                    Importe a Cobrar
                                </th>                                                                
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="textoGris">
                                <%# Eval("NOMBRE_APELLIDOS")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("CELULAR")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("GRUPO_PERSONAS")%>
                            </td>
                            
                            <td class="textoGris alinearDer total">                                
                                <%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", Eval("TOTAL_CREDITO"))%>
                            </td>                            
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
 <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Cliente
                                </th>
                                <th style="width:100px;">
                                    Celular
                                </th>
                                <th style="width:300px;">
                                    Grupo
                                </th>
                                <th style="width:120px;">
                                    Importe a Cobrar
                                </th>                                                                
                            </tr>
                            <tr>
                                <td colspan="4" class="textoGris">
                                    No Existe Movimiento.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>                 
                    
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="alinearDer textoNegroNegrita">
                <br />
                Total:&nbsp;&nbsp;
                <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true" CssClass="alinearDer textoGris "></asp:TextBox>
            </td>
        </tr>
        </table>
<asp:SqlDataSource runat="server" ID="sqlMovimientos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM (
SELECT CL.ID_CLIENTE, ISNULL(P.CELULAR,'') AS CELULAR,ISNULL(CL.NIT,'') AS NIT, P.NOMBRE_APELLIDOS, SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,GC.GRUPO_PERSONAS   FROM CREDITO C
INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE 
INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO 
LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P 
INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO 
GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO 
WHERE ESTADO = 0 AND GC.ID_GRUPO = CASE WHEN @GRUPO = -1 THEN GC.ID_GRUPO ELSE @GRUPO END
AND P.NOMBRE_APELLIDOS LIKE '%' + @CLIENTE + '%'
GROUP BY CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, GC.GRUPO_PERSONAS ) C   
 WHERE C.TOTAL_CREDITO > 0     
ORDER BY NOMBRE_APELLIDOS ">                        
                        
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlGrupoPersona" Name="GRUPO" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="txtCliente" Name="CLIENTE"  DefaultValue="%"
                            PropertyName="Text" />
                    </SelectParameters>
                        
                    </asp:SqlDataSource>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    function formatNumber(num) {
        num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
        splitRight = splitRight + '00';
        splitRight = splitRight.substr(0, 3);
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
        }
        return splitLeft + splitRight;
    }
    function ReplaceAll(Source, stringToFind, stringToReplace) {
        var temp = Source;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    }
    function calcularTotal() {
        var subTotal = 0;
        jQuery(".total").each(function () {
            subTotal = subTotal + parseFloat(ReplaceAll(jQuery(this).html(), ",", ""));
        });

        jQuery("#<%= txtTotal.ClientID%>").val(formatNumber(subTotal));        
    }
    function exportarExcel() {
        var url = "../reporte/excel.aspx?TIPO=CUENTAS_COBRAR&CLIENTE=" + jQuery("#<%=txtCliente.ClientID %>").val() + "&GRUPO=" + jQuery("#<%=ddlGrupoPersona.ClientID %>").val() ;
        //alert(url);
        window.open(url);
        return false;
    }
</script>
</asp:Content>
