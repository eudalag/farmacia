﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="seguimiento_banco.aspx.vb" Inherits="FARM_ADM.seguimiento_banco" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtDesde.ClientID %>").datepicker({});
            jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        });

        window.addEvent("domready", function () {
            var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Movimiento Caja:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
    <tr>
        <td style="width:100px" class="textoGris">
            Banco / Caja:
        </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlBanco" DataSourceID="sqlBanco" DataTextField="BANCO" DataValueField="ID_BANCO" AutoPostBack="true"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlBanco" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT B.ENTIDAD_FINANCIERA + CASE WHEN NRO_CUENTA IS NULL THEN '' ELSE ' - ' + NRO_CUENTA END + ' - ' + U.NOMBRE_APELLIDOS  AS BANCO, B.ID_BANCO   FROM BANCO B 
                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO AND TB.ID_TIPO_BANCO = 1
							INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO 
                            WHERE U.ID_USUARIO <> @USUARIO 
							UNION ALL SELECT '[Seleccione una Cuenta]',-1
                            ORDER BY ID_BANCO">
                            <SelectParameters>
                                <asp:SessionParameter Name="USUARIO" SessionField="IDUSUARIO" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
               
    </tr>

    <tr>
        <td class="textoGris" style="width:100px;">
            Nro. Cuenta:
        </td>
        <td style="width:230px;">
            <asp:TextBox runat="server" ID="txtNroCuenta" ReadOnly="true" style="width:200px;"></asp:TextBox>
        </td>
        <td class="textoGris" style="width:100px;">
            Nombre Cuenta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtNombreCuenta" ReadOnly="true" style="width:250px;"></asp:TextBox>
        </td>
    </tr>      
    <tr>
        <td class="textoGris">
            Desde:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>
        </td>
    </tr>
   <tr>
        <td class="textoGris">
            Tipo Movimiento:
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlTipoMovimiento" DataSourceID="sqlTipoMovimiento" 
            DataTextField="DESCRIPCION" DataValueField="ID_TIPO_MOVIMIENTO"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlTipoMovimiento" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_TIPO_MOVIMIENTO , UPPER(DESCRIPCION) AS DESCRIPCION FROM TIPO_MOVIMIENTO_CAJA WHERE ID_TIPO_MOVIMIENTO IN (1,2,3,5)
                                            UNION ALL
                                            SELECT ID_TIPO_COMPROBANTE + 5,DETALLE FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE IN (1,2)
                                            UNION ALL SELECT -1, '[TODOS LOS MOVIMIENTOS]'
                                            ORDER BY DESCRIPCION">                           
                        </asp:SqlDataSource>
            
        </td>
        <td class="textoGris">
            Tipo Pago:
        </td>   
        <td>
            <asp:DropDownList runat="server" ID="ddlTipoPago">
                <asp:ListItem Selected="True" Text="[Todos los Tipos de Pago]" Value="-1"></asp:ListItem>
                <asp:ListItem  Text="EFECTIVO" Value="1"></asp:ListItem>
                <asp:ListItem  Text="CREDITO" Value="2"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button runat="server" ID="btnActualizar" Text="Cargar" />
        </td>     
    </tr>
    <tr>
        <td class="textoGris">
            Total:
        </td>
        <td colspan="3">
            <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true" style="width:100px;"></asp:TextBox>
        </td>        
    </tr>  
    <tr>
        <td colspan="4">
            <asp:ListView runat="server" ID="lvwMovimiento" DataSourceID="sqlMovimiento">
                    <LayoutTemplate>
                        <table class="generica">
                            <tr>                                
                                <th style="width:80px;">
                                    Registro
                                </th>
                                <th style="width:80px;">
                                    Fecha
                                </th> 
                                <th style="width:60px;">
                                    Hora
                                </th>
                                <th style="width:150px;">
                                    Caja
                                </th>
                                <th >
                                    Descripcion
                                </th> 
                                <th style="width:100px;">
                                    Importe
                                </th>                              
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>                            
                            <td class="alinearCentro">
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("TRANSACCION")%>' CommandName="modificar" CommandArgument='<%# Eval("REGISTRO")%>'></asp:LinkButton>
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="lblDescripcion" Text='<%# Eval("F_MOVIMIENTO")%>'></asp:Label>                                
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label1" Text='<%# Eval("H_MOVIMIENTO")%>'></asp:Label>                                
                            </td> 
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label2" Text='<%# Eval("REGISTRO")%>'></asp:Label>                                
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label3" Text='<%# Eval("DETALLE")%>'></asp:Label>                                
                            </td>
                            <td class="alinearDer" style="padding-right:5px;">
                                <%# String.Format(New System.Globalization.CultureInfo("en-US"),"{0:#,#0.00}",Eval("VENTA"))%>
                                <asp:HiddenField runat="server" ID="hdImporte" Value='<%# Eval("VENTA") %>' />
                            </td>                         
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMovimiento" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlMovimiento" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM (
                                        SELECT RIGHT('00000000' + CAST(V.ID_CAJA AS NVARCHAR(18)),8) AS REGISTRO,CONVERT(NVARCHAR(10),V.FECHA_AUD,103) AS F_MOVIMIENTO,
                                        CONVERT(NVARCHAR(10),V.FECHA_AUD,108) AS H_MOVIMIENTO,RIGHT('00000000' + CAST(NRO_VENTA AS NVARCHAR(18)),8) AS TRANSACCION,
                                        TC.DETALLE + CASE WHEN V.CREDITO = 1 THEN ' - (VTA. AL CREDITO)' ELSE '' END + ' - ' + P.NOMBRE_APELLIDOS  AS DETALLE ,TOTAL_PEDIDO - TOTAL_DESCUENTO AS VENTA,1 AS TIPO,TC.ID_TIPO_COMPROBANTE + 5 AS ID_TIPO_COMPROBANTE,V.CREDITO FROM VENTA V
                                        INNER JOIN CAJA C ON C.ID_CAJA = V.ID_CAJA AND C.ID_BANCO = @BANCO
                                        INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE
                                        INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = V.ID_CLIENTE
                                        INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
                                        UNION ALL 
                                        SELECT RIGHT('00000000' + CAST(M.ID_CAJA AS NVARCHAR(18)),8) AS REGISTRO,CONVERT(NVARCHAR(10),M.FH_MOVIMIENTO,103) AS F_MOVIMIENTO,
                                        CONVERT(NVARCHAR(10),M.FH_MOVIMIENTO,108) AS H_MOVIMIENTO,RIGHT('00000000' + CAST(M.ID_MOVIMIENTO  AS NVARCHAR(18)),8) AS TRANSACCION,
                                        TM.DESCRIPCION,IMPORTE,2 AS TIPO, TM.ID_TIPO_MOVIMIENTO,0 AS CREDITO   FROM MOVIMIENTO M
                                        INNER JOIN TIPO_MOVIMIENTO_CAJA TM ON TM.ID_TIPO_MOVIMIENTO = M.ID_TIPO_MOVIMIENTO 
                                        INNER JOIN CAJA C ON C.ID_CAJA = M.ID_CAJA AND C.ID_BANCO = @BANCO 
                                        ) AS MOV 
                                        WHERE MOV.ID_TIPO_COMPROBANTE =  CASE WHEN @TIPO = -1 THEN MOV.ID_TIPO_COMPROBANTE ELSE @TIPO END
                                        AND CREDITO = CASE WHEN @PAGO = 1 THEN 0 WHEN @PAGO = 2 THEN 1 ELSE CREDITO END
                                        AND CONVERT(DATE,F_MOVIMIENTO,103) >= CONVERT(DATE,@DESDE,103) AND CONVERT(DATE,F_MOVIMIENTO,103) <= CONVERT(DATE,@HASTA,103)
                                        ORDER BY REGISTRO  DESC, F_MOVIMIENTO DESC,H_MOVIMIENTO DESC">                        
                        
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlBanco" Name="BANCO" 
                                PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="ddlTipoMovimiento" Name="TIPO" 
                                PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="ddlTipoPago" Name="PAGO" 
                                PropertyName="SelectedValue" />
<asp:ControlParameter ControlID="txtDesde" Name="DESDE" 
                                PropertyName="Text" />
<asp:ControlParameter ControlID="txtHasta" Name="HASTA" 
                                PropertyName="Text" />
                        </SelectParameters>
                        
                    </asp:SqlDataSource>
                </div>
        </td>
    </tr>
    </table>
             </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
    function iniciarFecha() {
        jQuery.noConflict();
        jQuery("#<%=txtDesde.ClientID %>").datepicker({});
        jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
    }
</script>
</asp:Content>
