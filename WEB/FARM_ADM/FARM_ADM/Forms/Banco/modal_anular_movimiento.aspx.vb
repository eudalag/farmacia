﻿Public Class modal_anular_movimiento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Sub cargarDatos(ByVal idMovimiento As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @MOVIMIENTO AS INTEGER;"
        sSql += " SET @MOVIMIENTO = " + idMovimiento.ToString + ";"
        sSql += " SELECT RIGHT( '00000000' + CAST(ID_MOVIMIENTO AS NVARCHAR(8)),8) AS REGISTRO,"
        sSql += " TM.DESCRIPCION AS TIPO_MOVIMIENTO, MB.GLOSA,MB.IMPORTE "
        sSql += " FROM MOVIMIENTO_BANCO MB"
        sSql += " INNER JOIN TIPO_MOVIMIENTO_CAJA TM ON TM.ID_TIPO_MOVIMIENTO = MB.ID_TIPO_MOVIMIENTO "
        sSql += " WHERE ID_MOVIMIENTO = @MOVIMIENTO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtRegistro.Text = tbl.Rows(0)("REGISTRO")
            txtTipoMovimiento.Text = tbl.Rows(0)("TIPO_MOVIMIENTO")
            txtGlosa.Text = tbl.Rows(0)("GLOSA")
            txtImporte.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("IMPORTE"))
        End If
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Try
            Using scope As New Transactions.TransactionScope
                Dim tbl = New mcTabla()
                tbl.tabla = "MOVIMIENTO_BANCO"
                tbl.campoID = "ID_MOVIMIENTO"
                tbl.valorID = CInt(Request.QueryString("ID"))
                tbl.agregarCampoValor("ANULADO", 1)
                tbl.agregarCampoValor("DETALLE_ANULADO", txtDetalle.Text)                
                tbl.modificar()
                scope.Complete()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub modal_anular_movimiento_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            cargarDatos(Request.QueryString("ID"))
        End If
    End Sub
End Class