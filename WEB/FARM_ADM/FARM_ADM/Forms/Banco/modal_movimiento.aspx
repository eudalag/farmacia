﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modal_movimiento.aspx.vb" Inherits="FARM_ADM.modal_movimiento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../../scripts/mascara/js/mootools-1.2.4.js" type="text/javascript"></script>
    <script src="../../scripts/mascara/js/Mascara.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery.min.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        window.addEvent("domready", function () {
            var txtBolivianos = new Mascara("txtBolivianos", { 'fijarMascara': '##/##/####', 'esNumero': true, 'esNegativo': true });            
        });
        </script>
</head>
<body>
    <form id="form1" runat="server">
<br /><br />
    <div>    
            <table style="width:100%;">
                <tr>
                     <td class="textoGris" style="width:120px; ">
                         Tipo de Movimiento :
                     </td>
                     <td>
                         <asp:DropDownList runat="server" id="ddlMovimiento" DataSourceID="sqlMovimiento" onchange="javaScript: tipoMovimiento(this);"
                            DataTextField="DESCRIPCION" DataValueField="ID_TIPO_MOVIMIENTO" ></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlMovimiento" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            
                             SelectCommand="SELECT ID_TIPO_MOVIMIENTO, DESCRIPCION FROM TIPO_MOVIMIENTO_CAJA WHERE ID_TIPO_MOVIMIENTO IN (1,2,3,8)
                            UNION ALL SELECT -1,'[Seleccione un tipo de Movimiento]' ORDER BY ID_TIPO_MOVIMIENTO "></asp:SqlDataSource>
                     </td>
                </tr>
<tr>
                     <td class="textoGris" style="width:120px; ">
                         Cuenta :
                     </td>
                     <td>
                         <asp:DropDownList runat="server" id="ddlTipoCuenta" DataSourceID="sqlTipoCuenta"  Enabled="false"
                            DataTextField="CUENTA" DataValueField="ID_PLAN_CUENTAS" ></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlTipoCuenta" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>"                             
                             SelectCommand="SELECT ID_PLAN_CUENTAS, CUENTA FROM PLAN_CUENTAS PC 
WHERE  ID_PLAN_CUENTA_PADRE IN (13,15,18,19,20) 
ORDER BY ID_PLAN_CUENTA_PADRE,CODIGO "></asp:SqlDataSource>
                     </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Detalle
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDetalle" style="width:250px;" TextMode="MultiLine" Rows="2" class="textoGris" MaxLength="100"></asp:TextBox> 
                    </td>
                </tr>
                <tr>
                     <td class="textoGris">
                         Importe (Bs.):
                         
                     </td>
                     <td>
                         <asp:TextBox ID="txtBolivianos" runat="server" class="textoGris"  style="width:100px;" Text="0.00" ></asp:TextBox >                         
                     </td>
                 </tr>                
                <tr>
                    <td colspan="2" class="alinearDer">
                        <br />
                        <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                        <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="cerrarInformacion();" CausesValidation="false" />
                    </td>                   
                </tr>
            </table>
        </div>
    </form>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
        function tipoMovimiento(obj) {
            if (parseInt(obj.value) == 2) {
                document.getElementById("ddlTipoCuenta").disabled = false;
            } else {
                document.getElementById("ddlTipoCuenta").disabled = true;
            }
        }
    </script>
</body>
</html>
