﻿Imports System.Transactions
Public Class modal_movimiento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Using scope As New TransactionScope
                Try
                    Dim importe As Decimal = Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US"))
                    Dim tbl = New mcTabla()
                    tbl.tabla = "MOVIMIENTO_BANCO"
                    tbl.modoID = "auto"
                    tbl.campoID = "ID_MOVIMIENTO"
                    tbl.agregarCampoValor("ID_BANCO", Request.QueryString("ID"))
                    tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", ddlMovimiento.SelectedValue)
                    tbl.agregarCampoValor("IMPORTE", importe * IIf(ddlMovimiento.SelectedValue = 2, -1, 1))
                    tbl.agregarCampoValor("GLOSA", txtDetalle.Text)
                    tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                    If ddlMovimiento.SelectedValue = 2 Then
                        tbl.agregarCampoValor("ID_PLAN_CUENTA", ddlTipoCuenta.SelectedValue)
                    Else
                        tbl.agregarCampoValor("ID_PLAN_CUENTA", "null")
                    End If
                    tbl.insertar()
                    If ddlMovimiento.SelectedValue = 2 Then
                        Dim cuenta = New plan_cuentas()
                        cuenta.insertarMovimiento(importe, ddlTipoCuenta.SelectedValue, "MOVIMIENTO_BANCO", tbl.valorID, "serverDateTime")
                    End If                    
                    scope.Complete()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
                Catch ex As Exception

                End Try
            End Using
        End If
    End Sub
End Class