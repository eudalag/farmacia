﻿Imports System.Transactions
Public Class modal_transferencia
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub procesarTransferenciaBanco()

        Dim tbl = New mcTabla()

        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")) * -1)
        tbl.agregarCampoValor("GLOSA", "Transferencia a " + ddlMovimiento.SelectedItem.Text)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", Request.QueryString("ID"))
        tbl.insertar()

        tbl.reset()

        Dim tipoCta As Integer
        Dim idCta As Integer
        obtenerCuenta(ddlMovimiento.SelectedValue, tipoCta, idCta)
        Select Case tipoCta
            Case 1
                tbl.tabla = "MOVIMIENTO"
                tbl.campoID = "ID_MOVIMIENTO"
                tbl.modoID = "sql"
                tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
                tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")))
                tbl.agregarCampoValor("GLOSA", "Transferencia de " + Session("NOMBRE_WEB"))
                tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                tbl.agregarCampoValor("ID_CAJA", idCta)
                tbl.insertar()
            Case 2
                tbl.tabla = "MOVIMIENTO_BANCO"
                tbl.campoID = "ID_MOVIMIENTO"
                tbl.modoID = "auto"
                tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
                tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")))
                tbl.agregarCampoValor("GLOSA", "Transferencia de " + Session("NOMBRE_WEB"))
                tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                tbl.agregarCampoValor("ID_BANCO", idCta)
                tbl.insertar()
        End Select        

    End Sub
    Protected Sub obtenerCuenta(ByVal banco As Integer, ByRef tipo As Integer, ByRef id As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @BANCO AS INTEGER;"
        sSql += " SET @BANCO = " + banco.ToString + ";"
        sSql += " SELECT C.ID_CAJA FROM BANCO B "
        sSql += " LEFT JOIN CAJA C ON C.ID_BANCO = B.ID_BANCO AND C.F_CIERRE IS NULL"
        sSql += " WHERE B.ID_BANCO = @BANCO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If IsDBNull(tbl.Rows(0)(0)) Then
            tipo = 2
            id = banco
        Else
            tipo = 1
            id = tbl.Rows(0)(0)
        End If
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Using scope As New TransactionScope()
            Try
                procesarTransferenciaBanco()
                scope.Complete()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
            Catch ex As Exception

            End Try
        End Using
    End Sub
End Class