﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="banca_personal.aspx.vb" Inherits="FARM_ADM.banca_personal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtDesde.ClientID %>").datepicker({});
            jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        });

        window.addEvent("domready", function () {
            var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
<style type="text/css">
    .anulado
    {
        background-color:Red;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <table style="width:100%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Movimiento Banco:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
    <tr>
        <td style="width:100px" class="textoGris">
            Banco / Caja:
        </td>
                    <td colspan="3">
                        <asp:DropDownList runat="server" ID="ddlBanco" DataSourceID="sqlBanco" DataTextField="BANCO" DataValueField="ID_BANCO" AutoPostBack="true"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlBanco" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" SelectCommand="SELECT TB.DESC_TIPO_BANCO + CASE WHEN NRO_CUENTA IS NULL THEN '' ELSE ' - ' + NRO_CUENTA END + ' - ' + ENTIDAD_FINANCIERA AS BANCO, B.ID_BANCO   FROM BANCO B 
                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO 
                            WHERE ID_USUARIO = @USUARIO 
                            ORDER BY ID_BANCO">
                            <SelectParameters>
                                <asp:SessionParameter Name="USUARIO" SessionField="IDUSUARIO" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
               
    </tr>
    <tr>
        <td class="textoGris" style="width:100px;">
            Nro. Cuenta:
        </td>
        <td style="width:230px;">
            <asp:TextBox runat="server" ID="txtNroCuenta" ReadOnly="true" style="width:200px;"></asp:TextBox>
        </td>
        <td class="textoGris" style="width:100px;">
            Nombre Cuenta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtNombreCuenta" ReadOnly="true" style="width:250px;"></asp:TextBox>
        </td>
    </tr>
<tr>
        <td class="textoGris">
            Saldo Actual:
        </td>
        <td colspan = "3">
            <asp:TextBox runat="server" ID="txtSaldoActual" CssClass="textoNegroNegrita alinearDer" ReadOnly="true" style="width:100px;background-color:yellow;"></asp:TextBox>
        </td>        
    </tr>
      
    <tr>
        <td class="textoGris">
            Desde:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>
        </td>
    </tr>
   <tr>
        <td class="textoGris">
            Tipo Movimiento:
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlTipoMovimiento" DataSourceID="sqlTipoMovimiento" DataTextField="DESCRIPCION" DataValueField="ID_TIPO_MOVIMIENTO" AutoPostBack="true"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlTipoMovimiento" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_TIPO_MOVIMIENTO, DESCRIPCION FROM TIPO_MOVIMIENTO_CAJA WHERE ID_TIPO_MOVIMIENTO IN (1,2,3,5)
                                            UNION ALL
                                            SELECT -1, '[Todos los Movimientos]' ORDER BY DESCRIPCION">                           
                        </asp:SqlDataSource>
            
        </td>      
      
        <td class="textoGris">
            Tipo de Cuenta:
        </td>
        <td>
            <asp:DropDownList runat="server" ID="ddlCuenta" DataSourceID="sqlCuenta" DataTextField="CUENTA" DataValueField="ID_PLAN_CUENTAS"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlCuenta" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT PC.ID_PLAN_CUENTAS,
CAST(PC1.CODIGO AS NVARCHAR(3)) + '.' + CAST(PC2.CODIGO AS NVARCHAR(3)) + '.' + 
CAST(PC3.CODIGO AS NVARCHAR(3)) + '.' + CAST(PC4.CODIGO AS NVARCHAR(3)) + '.' + 
CAST(PC.CODIGO AS NVARCHAR(3)) + ' - ' + PC.CUENTA  AS CUENTA FROM PLAN_CUENTAS PC
INNER JOIN (
SELECT ID_PLAN_CUENTAS,CODIGO, CUENTA,ID_PLAN_CUENTA_PADRE  FROM PLAN_CUENTAS WHERE NIVEL = 4
) PC4 ON PC4.ID_PLAN_CUENTAS = PC.ID_PLAN_CUENTA_PADRE 
INNER JOIN (
SELECT ID_PLAN_CUENTAS,CODIGO, CUENTA,ID_PLAN_CUENTA_PADRE  FROM PLAN_CUENTAS WHERE NIVEL = 3
) PC3 ON PC3.ID_PLAN_CUENTAS = PC4.ID_PLAN_CUENTA_PADRE 
INNER JOIN (
SELECT ID_PLAN_CUENTAS,CODIGO, CUENTA,ID_PLAN_CUENTA_PADRE  FROM PLAN_CUENTAS WHERE NIVEL = 2
) PC2 ON PC2.ID_PLAN_CUENTAS = PC3.ID_PLAN_CUENTA_PADRE 
INNER JOIN (
SELECT ID_PLAN_CUENTAS,CODIGO, CUENTA,ID_PLAN_CUENTA_PADRE  FROM PLAN_CUENTAS WHERE NIVEL = 1
) PC1 ON PC1.ID_PLAN_CUENTAS = PC2.ID_PLAN_CUENTA_PADRE 
WHERE NIVEL = 5 AND PC1.CODIGO = @MOVIMIENTO
UNION ALL SELECT -1, '[Todas las cuentas]'
ORDER BY CUENTA">                           
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlTipoMovimiento" Name="MOVIMIENTO" 
                                    PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
            <asp:Button runat="server" ID="btnActualizar" Text="Cargar" />
        </td>         
    </tr>
    <tr>
        <td class="textoGris">
            Saldo Inicial:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtSaldoInicial" CssClass="textoNegroNegrita alinearDer" ReadOnly="true" style="width:100px;"></asp:TextBox>
        </td>
        <td class="textoGris">
            Total Rango Fecha:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtSaldoRango" CssClass="textoNegroNegrita alinearDer" ReadOnly="true" style="width:100px;"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="alinearDer">
            <asp:Button runat="server" ID="btnEliminar" Text="Eliminar" OnClientClick="return eliminar();" />
            <asp:Button runat="server" ID="btnMovimiento" Text="Nuevo Movimiento" OnClientClick=" return nuevoMovimiento();" />
            <asp:Button runat="server" ID="btnTransferencia" Text="Transferencia" OnClientClick="return nuevaTransferencia();" />
            <asp:Button runat="server" ID="btnCargar" Text="cargar"  CausesValidation="false" style="display:none" />                    
        </td>
    </tr>
    <tr>
        <td colspan="4">
<div style="overflow: scroll; width: 100%; height: 300px;">
            <asp:ListView runat="server" ID="lvwMovimiento">
                    <LayoutTemplate>
                        <table class="generica">
                            <tr>
                                <th style="width:10px;">
                                    &nbsp;
                                </th>                                                            
                                <th style="width:70px;">
                                    Registro
                                </th>
                                <th style="width:70px;">
                                    Fecha
                                </th> 
                                <th style="width:50px;">
                                    Hora
                                </th>
                                <th style="width:280px;">
                                    Tipo Movimiento
                                </th>
                                <th >
                                    Descripcion
                                </th> 
                                <th style="width:70px;">
                                    Importe
                                </th>                              
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# IIf(Eval("ANULADO") = 1 , "anulado","") %>'>      
                            <td class="alinearCentro">
                                <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />  
                                <asp:HiddenField runat="server" ID="hdEliminar" Value='<%# Eval("ID_MOVIMIENTO") %>'   />
                            </td>                         
                            <td class="alinearCentro">
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("REGISTRO")%>' CommandName="modificar" CommandArgument='<%# Eval("ID_MOVIMIENTO")%>'></asp:LinkButton>
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="lblDescripcion" Text='<%# Eval("F_MOVIMIENTO")%>'></asp:Label>                                
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label1" Text='<%# Eval("H_MOVIMIENTO")%>'></asp:Label>                                
                            </td> 
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label2" Text='<%# Eval("DESCRIPCION")%>'></asp:Label>                                
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="Label3" Text='<%# Eval("GLOSA")%>'></asp:Label>                                
                            </td>
                            <td class="alinearDer" style="padding-right:5px;">
                                <%# String.Format(New System.Globalization.CultureInfo("en-US"),"{0:#,#0.00}",Eval("IMPORTE"))%>
                            </td>                         
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                </div>
        </td>
    </tr>
    </table>
             </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
    function cancelarModal() {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");

    }
    function aceptarModal() {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");        
        jQuery("#<%=btnCargar.ClientID%>").click()
        return false;
    }
    
    function nuevoMovimiento() {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var id = jQuery("#<%=ddlBanco.ClientID%>").val();
        var url = "modal_movimiento.aspx?ID=" + id;
       
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "MOVIMIENTO ECONOMICO", width: 550, height: 200 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }

    function nuevaTransferencia() {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var id = jQuery("#<%=ddlBanco.ClientID%>").val();
        var url = "modal_Transferencia.aspx?ID=" + id;        
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "TRANSFERENCIA ENTRE CUENTAS", width: 500, height: 150 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }

    
    
    function formatNumber(num) {
        num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
        splitRight = splitRight + '00';
        splitRight = splitRight.substr(0, 3);
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
        }
        return splitLeft + splitRight;
    }
    function ReplaceAll(Source, stringToFind, stringToReplace) {
        var temp = Source;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    }
    function iniciarFecha() {
        jQuery.noConflict();
        jQuery("#<%=txtDesde.ClientID %>").datepicker({});
        jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
    }
    function seleccionar_item(obj) {
        var estado = obj.checked;
        jQuery(".checked").each(function () {
            jQuery(this).attr("checked", false);
        });
        var chk = jQuery(obj).attr("checked", estado);
        return false;
    }
    function eliminar() {
        var idremplazo = '';
        var estado = '';
        var descEstado = '';
        var result = false;
        jQuery(".checked").each(function () {
            var chk = jQuery(this).is(':checked');
            if (chk) {
                id = jQuery(this).attr('id');
                idremplazo = id.replace("chkEliminar", "hdEliminar");
                var hdEliminar = $get(idremplazo);                
                anularMovimiento(hdEliminar.value);                
            }
        });
    }
    function anularMovimiento(idMovimiento) {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var url = "modal_anular_movimiento.aspx?ID=" + idMovimiento;
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "ANULAR MOVIMIENTOS", width: 420, height: 220 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }
</script>  
</asp:Content>
