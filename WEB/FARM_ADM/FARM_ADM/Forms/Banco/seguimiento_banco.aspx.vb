﻿Public Class seguimiento_banco
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub seguimiento_banco_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            ddlBanco.DataBind()
            ddlTipoMovimiento.DataBind()
            Dim aux = Now.ToString("dd/MM/yyyy")
            Dim fecha = DateSerial(Now.ToString("yyyy"), Now.ToString("MM"), 1)
            txtDesde.Text = fecha.ToString("dd/MM/yyyy")
            txtHasta.Text = aux
            CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
        End If
    End Sub
    Protected Sub CargarCabecera(ByVal idBanco As Integer, ByVal desde As String, ByVal hasta As String)
        Dim sSql As String = ""


        sSql += " DECLARE @DESDE AS DATE,@HASTA AS DATE,@BANCO AS INTEGER,@TIPO AS INTEGER;"
        sSql += " SET @DESDE = CONVERT (DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT (DATE,'" + hasta + "',103);"
        sSql += " SET @BANCO = " + idBanco.ToString + ";"
        sSql += " SET @TIPO = " + ddlTipoMovimiento.SelectedValue + ";"

        sSql += " SELECT U.NOMBRE_APELLIDOS, B.SALDO,ISNULL(B.NRO_CUENTA,'') AS NRO_CUENTA,"
        sSql += " TB.DESC_TIPO_BANCO + ' - ' +  B.ENTIDAD_FINANCIERA AS NOMBRE_CUENTA,"
        sSql += " ISNULL(I.SALDO_INICIAL,0) AS SALDO_INICIAL, ISNULL(MR.TOTAL_RANGO,0) AS TOTAL_RANGO,"
        sSql += " ISNULL(T.TOTAL_BANCO,0) AS TOTAL_BANCO  FROM BANCO B "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO "
        sSql += " INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO, ISNULL(SUM(IMPORTE),0) AS SALDO_INICIAL "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE CONVERT(DATE,FH_MOVIMIENTO,103) < @DESDE AND ID_BANCO = @BANCO"
        sSql += " AND ID_TIPO_MOVIMIENTO = CASE WHEN @TIPO = -1 THEN ID_TIPO_MOVIMIENTO ELSE @TIPO END"
        sSql += " GROUP BY ID_BANCO) I ON I.ID_BANCO = B.ID_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO,ISNULL(SUM(IMPORTE),0) AS TOTAL_RANGO "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE CONVERT(DATE,FH_MOVIMIENTO,103) >= @DESDE AND"
        sSql += " CONVERT(DATE,FH_MOVIMIENTO,103) <= @HASTA"
        sSql += " AND ID_BANCO = @BANCO"
        sSql += " AND ID_BANCO = @BANCO AND ID_TIPO_MOVIMIENTO = CASE WHEN @TIPO = -1 THEN ID_TIPO_MOVIMIENTO ELSE @TIPO END"
        sSql += " GROUP BY ID_BANCO) MR ON MR.ID_BANCO = B.ID_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO,ISNULL(SUM(IMPORTE),0) AS TOTAL_BANCO "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE ID_BANCO = @BANCO"
        sSql += " GROUP BY ID_BANCO) T ON T.ID_BANCO = B.ID_BANCO "
        sSql += " WHERE B.ID_BANCO = @BANCO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            'lblNroCuenta.Text = tbl.Rows(0)("NRO_CUENTA")
            'lblSaldoActual.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", tbl.Rows(0)("SALDO"))
            txtNroCuenta.Text = tbl.Rows(0)("NRO_CUENTA")
            txtNombreCuenta.Text = tbl.Rows(0)("NOMBRE_CUENTA")            
        End If
    End Sub

    Protected Sub ddlBanco_TextChanged(sender As Object, e As EventArgs) Handles ddlBanco.TextChanged
        CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
        lvwMovimiento.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
        lvwMovimiento.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub lvwMovimiento_DataBound(sender As Object, e As EventArgs) Handles lvwMovimiento.DataBound
        Dim total As Decimal = 0
        For Each item In lvwMovimiento.Items
            total = total + Decimal.Parse(CType(item.FindControl("hdImporte"), HiddenField).Value, New Globalization.CultureInfo("en-US"))
        Next
        txtTotal.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", total)
    End Sub

    Protected Sub lvwMovimiento_PagePropertiesChanging(sender As Object, e As System.Web.UI.WebControls.PagePropertiesChangingEventArgs) Handles lvwMovimiento.PagePropertiesChanging
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub
End Class