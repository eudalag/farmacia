﻿Public Class banca_personal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " declare @BANCO AS INTEGER, @MOVIMIENTO AS INTEGER,@DESDE AS DATE, @HASTA AS DATE,@CUENTA AS INTEGER;"
        sSql += " SET @BANCO = " + ddlBanco.SelectedValue.ToString + ";"
        sSql += " SET @MOVIMIENTO = " + ddlTipoMovimiento.SelectedValue.ToString + ";"
        sSql += " SET @DESDE = CONVERT (DATE,'" + txtDesde.Text + "',103);"
        sSql += " SET @HASTA = CONVERT (DATE,'" + txtHasta.Text + "',103);"
        sSql += " SET @CUENTA = " + ddlCuenta.SelectedValue + ";"
        sSql += " SELECT ID_MOVIMIENTO,RIGHT ( '00000000' + CAST(ID_MOVIMIENTO AS NVARCHAR(18)),8) AS REGISTRO,"
        sSql += " CONVERT(NVARCHAR(10),FH_MOVIMIENTO,103) AS F_MOVIMIENTO,"
        sSql += " CONVERT(NVARCHAR(8),FH_MOVIMIENTO,108) AS H_MOVIMIENTO,"
        sSql += " TM.DESCRIPCION + CASE WHEN PC.CUENTA IS NULL THEN  '' ELSE ' - ' + PC.CUENTA  END AS DESCRIPCION, GLOSA, IMPORTE,ISNULL(ANULADO,0) AS ANULADO FROM MOVIMIENTO_BANCO MB "
        sSql += " INNER JOIN TIPO_MOVIMIENTO_CAJA TM ON TM.ID_TIPO_MOVIMIENTO = MB.ID_TIPO_MOVIMIENTO "
        sSql += " LEFT JOIN PLAN_CUENTAS PC ON PC.ID_PLAN_CUENTAS = MB.ID_PLAN_CUENTA "
        sSql += " WHERE MB.ID_BANCO =  @BANCO AND ISNULL(ANULADO,0) = 0   "
        sSql += " AND TM.ID_TIPO_MOVIMIENTO = CASE WHEN @MOVIMIENTO = -1 THEN TM.ID_TIPO_MOVIMIENTO ELSE @MOVIMIENTO END "
        sSql += " AND CONVERT(NVARCHAR(10),FH_MOVIMIENTO,103) >= @DESDE AND CONVERT(NVARCHAR(10),FH_MOVIMIENTO,103) <= @HASTA"
        sSql += " AND ISNULL(MB.ID_PLAN_CUENTA,0) = CASE WHEN @CUENTA = -1 THEN ISNULL(MB.ID_PLAN_CUENTA,0) ELSE @CUENTA END"
        sSql += " ORDER BY FH_MOVIMIENTO DESC"

        Dim tbl = New mcTabla().ejecutarConsulta(sSql)

        lvwMovimiento.DataSource = tbl
        lvwMovimiento.DataBind()
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        lvwMovimiento.DataBind()
        CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
    End Sub
    Protected Sub CargarCabecera(ByVal idBanco As Integer, ByVal desde As String, ByVal hasta As String)
        Dim sSql As String = ""


        sSql += " DECLARE @DESDE AS DATE,@HASTA AS DATE,@BANCO AS INTEGER,@TIPO AS INTEGER;"
        sSql += " SET @DESDE = CONVERT (DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT (DATE,'" + hasta + "',103);"
        sSql += " SET @BANCO = " + idBanco.ToString + ";"
        sSql += " SET @TIPO = " + ddlTipoMovimiento.SelectedValue + ";"

        sSql += " SELECT U.NOMBRE_APELLIDOS, B.SALDO,ISNULL(B.NRO_CUENTA,'') AS NRO_CUENTA,"
        sSql += " TB.DESC_TIPO_BANCO + ' - ' +  B.ENTIDAD_FINANCIERA AS NOMBRE_CUENTA,"
        sSql += " ISNULL(I.SALDO_INICIAL,0) AS SALDO_INICIAL, ISNULL(MR.TOTAL_RANGO,0) AS TOTAL_RANGO,"
        sSql += " ISNULL(T.TOTAL_BANCO,0) AS TOTAL_BANCO  FROM BANCO B "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO "
        sSql += " INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO, ISNULL(SUM(IMPORTE),0) AS SALDO_INICIAL "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE CONVERT(DATE,FH_MOVIMIENTO,103) < @DESDE AND ID_BANCO = @BANCO"
        sSql += " AND ID_TIPO_MOVIMIENTO = CASE WHEN @TIPO = -1 THEN ID_TIPO_MOVIMIENTO ELSE @TIPO END"
        sSql += " GROUP BY ID_BANCO) I ON I.ID_BANCO = B.ID_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO,ISNULL(SUM(IMPORTE),0) AS TOTAL_RANGO "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE CONVERT(DATE,FH_MOVIMIENTO,103) >= @DESDE AND"
        sSql += " CONVERT(DATE,FH_MOVIMIENTO,103) <= @HASTA"
        sSql += " AND ID_BANCO = @BANCO"
        sSql += " AND ID_BANCO = @BANCO AND ID_TIPO_MOVIMIENTO = CASE WHEN @TIPO = -1 THEN ID_TIPO_MOVIMIENTO ELSE @TIPO END"
        sSql += " GROUP BY ID_BANCO) MR ON MR.ID_BANCO = B.ID_BANCO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_BANCO,ISNULL(SUM(IMPORTE),0) AS TOTAL_BANCO "
        sSql += " FROM MOVIMIENTO_BANCO "
        sSql += " WHERE ID_BANCO = @BANCO"
        sSql += " GROUP BY ID_BANCO) T ON T.ID_BANCO = B.ID_BANCO "
        sSql += " WHERE B.ID_BANCO = @BANCO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            'lblNroCuenta.Text = tbl.Rows(0)("NRO_CUENTA")
            'lblSaldoActual.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", tbl.Rows(0)("SALDO"))
            txtNroCuenta.Text = tbl.Rows(0)("NRO_CUENTA")
            txtNombreCuenta.Text = tbl.Rows(0)("NOMBRE_CUENTA")
            txtSaldoActual.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", tbl.Rows(0)("TOTAL_BANCO"))
            txtSaldoInicial.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", tbl.Rows(0)("SALDO_INICIAL"))
            txtSaldoRango.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", tbl.Rows(0)("TOTAL_RANGO"))

        End If
    End Sub

    Private Sub banca_personal_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            ddlBanco.DataBind()
            ddlTipoMovimiento.DataBind()
            ddlCuenta.DataBind()
            Dim aux = Now.ToString("dd/MM/yyyy")
            Dim fecha = DateSerial(Now.ToString("yyyy"), Now.ToString("MM"), 1)
            txtDesde.Text = fecha.ToString("dd/MM/yyyy")
            txtHasta.Text = aux
            CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
            cargarDatos()
        End If
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
        cargarDatos()        
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub ddlBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBanco.SelectedIndexChanged
        CargarCabecera(ddlBanco.SelectedValue, txtDesde.Text, txtHasta.Text)
        cargarDatos()        
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub lvwMovimiento_PagePropertiesChanged(sender As Object, e As EventArgs) Handles lvwMovimiento.PagePropertiesChanged
        cargarDatos()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub ddlTipoMovimiento_TextChanged(sender As Object, e As EventArgs) Handles ddlTipoMovimiento.TextChanged
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub
End Class