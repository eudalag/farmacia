﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modal_transferencia.aspx.vb" Inherits="FARM_ADM.modal_transferencia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <div>    
            <table style="width:90%;">
                <tr>
                     <td class="textoGris" style="width:120px; ">
                         Tipo de Movimiento :
                     </td>
                     <td>
                         <asp:DropDownList runat="server" id="ddlMovimiento" DataSourceID="sqlMovimiento" 
                            DataTextField="NOMBRE_APELLIDOS" DataValueField="ID_BANCO"></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlMovimiento" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT CTA.ID_BANCO  , NOMBRE_APELLIDOS + ' - ' + CTA.DESC_TIPO_BANCO AS NOMBRE_APELLIDOS  FROM USUARIO U
                                            INNER JOIN (SELECT B.ID_BANCO, TB.DESC_TIPO_BANCO, B.ID_USUARIO FROM CAJA C 
                                            INNER JOIN BANCO B ON B.ID_BANCO = C.ID_BANCO 
                                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO 
                                            WHERE F_CIERRE IS NULL		
                                            UNION ALL
                                            SELECT B.ID_BANCO,
                                            B.ENTIDAD_FINANCIERA + ' ' +
                                            CASE WHEN B.NRO_CUENTA IS NULL THEN '' ELSE '(' + B.NRO_CUENTA + ')'  END AS DESC_TIPO_BANCO,
                                            B.ID_USUARIO FROM BANCO B 
                                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO
                                            WHERE B.ID_TIPO_BANCO = 3) CTA ON CTA.ID_USUARIO = U.ID_USUARIO 
                                            WHERE U.ID_USUARIO <> @USUARIO 
                                            UNION ALL SELECT -1 , '[Seleccione Un Usuario]'
                                            ORDER BY NOMBRE_APELLIDOS">
                            <SelectParameters>
                                <asp:SessionParameter Name="USUARIO" SessionField="IDUSUARIO" />
                            </SelectParameters>
                         </asp:SqlDataSource>
                     </td>
                </tr>               
                <tr>
                     <td class="textoGris">
                         Importe (Bs.):
                         
                     </td>
                     <td>
                         <asp:TextBox ID="txtBolivianos" runat="server" class="textoGris"  style="width:100px;" Text="0.00" ></asp:TextBox >                         
                     </td>
                 </tr>                
                <tr>
                    <td colspan="2" class="alinearDer">
                        <br />
                        <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                        <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="cerrarInformacion();" CausesValidation="false" />
                    </td>                   
                </tr>
            </table>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }        
    </script>
</body>
</html>
