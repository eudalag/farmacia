﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modal_anular_movimiento.aspx.vb" Inherits="FARM_ADM.modal_anular_movimiento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:100%;">
            <tr>
                <td class="textoGris" style="width:100px;">
                    Registro:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRegistro" ReadOnly="true" style="width:100px;"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td class="textoGris">
                    Tipo Movimiento:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTipoMovimiento" ReadOnly="true" style="width:200px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">
                    Glosa:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtGlosa" ReadOnly="true" style="width:250px;" TextMode="MultiLine" Rows="2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">
                    Importe:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtImporte" ReadOnly="true" CssClass="alinearDer textoGris"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">
                    Detalle:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDetalle" TextMode="MultiLine" Rows="3" style="width:250px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="alinearDer">
                    <asp:Button runat="server" ID="btnAceptar" Text="Anular" />
                        <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="cerrarInformacion();" CausesValidation="false" />
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }        
    </script>
</body>
</html>
