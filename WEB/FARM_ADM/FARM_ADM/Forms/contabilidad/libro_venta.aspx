﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="libro_venta.aspx.vb" Inherits="FARM_ADM.libro_venta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtDesde.ClientID %>").datepicker({});
            jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        });

        window.addEvent("domready", function () {
            var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:60%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Exportar Libro de Venta:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
 <tr>
        <td class="textoGris">
            Desde:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="4" class="alinearDer">
              <asp:Button runat="server" ID="btnAceptar" Text="Exportar" OnClientClick="return exportarExcel();" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
        </td>
    </tr>
    </table>
 <script type="text/javascript" >     
     function exportarExcel() {
         window.open("../reporte/excel.aspx?TIPO=VENTA&DESDE=" + jQuery("#<%=txtDesde.ClientID %>").val() + "&HASTA=" + jQuery("#<%=txtHasta.ClientID %>").val());
         return false;
     }
    </script>
</asp:Content>
