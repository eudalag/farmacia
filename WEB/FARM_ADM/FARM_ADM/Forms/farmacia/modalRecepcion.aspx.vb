﻿Imports System.Transactions
Public Class modalRecepcion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Using scope As New TransactionScope()
            Try
                Dim fecha As String
                fecha = txtFechaVencimiento.Text ' DateSerial(Right(txtFechaVencimiento.Text, 4), CInt(Left(txtFechaVencimiento.Text, 2)) + 1, 0)                
                Dim idKardex = obtenerKardex(txtNroLote.Text, fecha, Request.QueryString("MEDICAMENTO"))
                procesarKardex(idKardex)
                scope.Complete()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
            Catch ex As Exception

            End Try
        End Using
    End Sub
    Protected Sub procesarKardex(ByVal idKardex As Integer)
        Dim tblKardex = New mcTabla()
        tblKardex.tabla = "KARDEX"
        tblKardex.campoID = "ID_KARDEX"
        tblKardex.modoID = "sql"
        tblKardex.agregarCampoValor("NRO_LOTE", IIf(CBool(chkSinLote.Checked), "null", txtNroLote.Text))
        Dim fecha As String
        fecha = txtFechaVencimiento.Text '(Right(txtFechaVencimiento.Text, 4), CInt(Left(txtFechaVencimiento.Text, 2)) + 1, 0)        
        tblKardex.agregarCampoValor("F_VENCIMIENTO", IIf(CBool(chkSinLote.Checked), "null", fecha))
        tblKardex.agregarCampoValor("SIN_LOTE", chkSinLote.Checked)
        If idKardex = -1 Then
            tblKardex.agregarCampoValor("ID_MEDICAMENTO", Request.QueryString("MEDICAMENTO"))
            tblKardex.insertar()
            idKardex = tblKardex.valorID()
        Else
            tblKardex.valorID = idKardex
            tblKardex.modificar()
        End If
        procesarMovimientoKardex(idKardex)
    End Sub
    Protected Sub procesarMovimientoKardex(ByVal idKardex As Integer)
        Dim tblMovimiento = New mcTabla()
        tblMovimiento.tabla = "MOVIMIENTO_KARDEX"
        tblMovimiento.campoID = "ID_MOVIMIENTO"
        tblMovimiento.modoID = "sql"
        tblMovimiento.agregarCampoValor("ID_KARDEX", idKardex)
        tblMovimiento.agregarCampoValor("ID_TIPO_MOVIMIENTO", 2)
        tblMovimiento.agregarCampoValor("CANTIDAD", CInt(txtCantidad.Text))
        tblMovimiento.agregarCampoValor("FECHA", "serverDateTime")
        tblMovimiento.agregarCampoValor("ID_USUARIO", Session("IDUSUARIO"))

        If Request.QueryString("MOVIMIENTO") Is Nothing Then
            tblMovimiento.agregarCampoValor("ID_COMPRA_MEDICAMENTOS", Request.QueryString("COMPRA"))
            tblMovimiento.insertar()
        Else
            tblMovimiento.valorID = Request.QueryString("MOVIMIENTO")
            tblMovimiento.modificar()
        End If
    End Sub
    Protected Function obtenerKardex(ByVal nroLote As String, ByVal f_vencimiento As String, ByVal idMedicamento As Integer)
        If Request.QueryString("KARDEX") Is Nothing Then
            Dim sSql As String = ""
            If Not chkSinLote.Checked Then
                sSql += " DECLARE @LOTE AS NVARCHAR(15), @F_VENCIMIENTO AS DATE, @MEDICAMENTO AS INTEGER;"
                sSql += " SET @LOTE = '" + Trim(nroLote) + "';"
                sSql += " SET @F_VENCIMIENTO = CONVERT (DATE, '" + f_vencimiento + "', 103);"
                sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
                sSql += " SELECT ID_KARDEX FROM KARDEX WHERE NRO_LOTE =  @LOTE AND F_VENCIMIENTO = @F_VENCIMIENTO AND ID_MEDICAMENTO = @MEDICAMENTO;"
                Dim tbl = New mcTabla().ejecutarConsulta(sSql)
                If tbl.Rows.Count > 0 Then
                    Return tbl.Rows(0)(0)
                Else
                    Return -1
                End If
            Else
                sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
                sSql += " SET @MEDICAMENTO = '" + Request.QueryString("MEDICAMENTO") + "';"

                sSql += " SELECT ID_KARDEX FROM KARDEX WHERE ID_MEDICAMENTO =  @MEDICAMENTO AND SIN_LOTE = 1;"
                Dim tbl = New mcTabla().ejecutarConsulta(sSql)
                If tbl.Rows.Count > 0 Then
                    Return tbl.Rows(0)(0)
                Else
                    Return -1
                End If
            End If

        Else
            Return Request.QueryString("KARDEX")
        End If

    End Function

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Using scope As New TransactionScope()
            Try
                Dim tbl = New mcTabla()
                tbl.tabla = "MOVIMIENTO_KARDEX"
                tbl.campoID = "ID_MOVIMIENTO"
                tbl.valorID = Request.QueryString("MOVIMIENTO")
                tbl.eliminar()
                scope.Complete()
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
            Catch ex As Exception

            End Try
        End Using
    End Sub
    Protected Sub cargarDatos(ByVal kardex As Integer, ByVal movimiento As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @KARDEX AS INTEGER, @MOVIMIENTO AS INTEGER;"
        sSql += " SET @KARDEX = " + kardex.ToString + ";"
        sSql += " SET @MOVIMIENTO = " + movimiento.ToString + ";"

        sSql += " SELECT MK.CANTIDAD,MK.ID_COMPRA_MEDICAMENTOS, K.NRO_LOTE, K.SIN_LOTE, CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103) AS F_VENCIMIENTO  FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.ID_KARDEX = @KARDEX AND MK.ID_MOVIMIENTO = @MOVIMIENTO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        txtCantidad.Text = tbl.Rows(0)("CANTIDAD")
        txtNroLote.Text = tbl.Rows(0)("NRO_LOTE")
        chkSinLote.Checked = CBool(tbl.Rows(0)("SIN_LOTE"))
        txtFechaVencimiento.Text = String.Format(New Globalization.CultureInfo("es-ES"), "{0:dd/MM/yyyy}", tbl.Rows(0)("F_VENCIMIENTO"))
    End Sub

    Private Sub modalRecepcion_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not (Request.QueryString("KARDEX") Is Nothing And Request.QueryString("MOVIMIENTO") Is Nothing) Then
                cargarDatos(Request.QueryString("KARDEX"), Request.QueryString("MOVIMIENTO"))
                btnEliminar.Visible = True
            Else
                btnEliminar.Visible = False
            End If
        End If
    End Sub

End Class