﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionRecepcion.aspx.vb" Inherits="FARM_ADM.gestionRecepcion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link type="text/css" href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />   
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        
    <div>
        <table style="width:100%">
            <tr>
              <td colspan="2"> 
                <div class="alinearIzq" style="padding-bottom:10px;""> 
                <asp:Label ID="Label5" runat="server" Text="Recepion de Medicamentos:" CssClass="textoNegroNegrita" ></asp:Label> 
                <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
                </div>
              </td>
            </tr>
            <tr>
               <td class="textoGris" style="width:120px;">
                Nit:
                </td>
                <td style="width:230px;">
                    <asp:Label runat="server" ID="lblNit" ></asp:Label>
                </td>
                 <td class="textoGris" style="width:100px;">
                     Razon Social:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblRazonSocial" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="textoGris">
                     Tipo de Comprobante:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblComprobante" ></asp:Label>
                </td>
                 <td class="textoGris">
                    Tipo de Moneda:
                </td>
                <td>
                    <asp:Label runat="server" ID="lblMoneda" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Fecha: </td>
                <td>
                    <asp:Label runat="server" ID="lblFecha" ></asp:Label>
                </td>
                 <td class="textoGris">Nro. Comprobante: </td>
                <td>
                    <asp:Label runat="server" ID="lblNroComprobante" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Button runat="server" ID="btnCargar" Text="cargar"  CausesValidation="false" style="display:none;" />
                    <asp:ListView runat="server" ID="lvwGrilla" DataSourceID="sqlMedicamentos">
                        <LayoutTemplate>
                                 <table class="generica" style="width:100%; border-collapse:collapse;">
                                     <tr>                                         
                                         <th colspan="2">Facturado</th>
                                         <th colspan="2">Recepcionado</th>
                                         <th rowspan="2" class="rowspan">Medicamento </th>
                                         <th colspan="2">Compra</th>
                                         <th colspan="2">Venta</th>
                                         <th style="width:60px;" rowspan="2" class="rowspan">Informacion </th>                                                                                    
                                     </tr>
                                    <tr>
                                        <th style="width:40px;" >Paq.</th>
                                         <th style="width:40px;" >Unid</th>
                                        <th style="width:40px;" >Paq.</th>
                                         <th style="width:40px;" >Unid.</th>
                                        <th style="width:40px;" >
                                            Paquete
                                        </th>
                                        <th style="width:40px;" >
                                            Unidad
                                        </th>
                                        <th style="width:40px;" >
                                            Paquete
                                        </th>
                                        <th style="width:40px;" >
                                            Unidad
                                        </th>
                                    </tr>
                                     <tr>
                                         <td id="itemPlaceHolder" runat="server"></td>
                                     </tr>
                                 </table>
                             </LayoutTemplate>
                            <ItemTemplate>
                                <tr>                                   
                                    <td class="textoGris">
                                        <%# Eval("PAQUETES")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("UNIDADES")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("PAQUETE")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("UNIDAD")%>
                                    </td>
                                    <td class="textoGris">
                                        <asp:linkbutton ID="Linkbutton1" runat="server"  text='<%# Eval("MEDICAMENTO")%>' OnClientClick='<%# String.Concat("return gestionLote(", Eval("ID_COMPRA"), ",", Eval("ID_MEDICAMENTO"), ")")%>'></asp:linkbutton>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("PRECIO_PAQUETE_COMPRA")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("PRECIO_UNITARIO_COMPRA")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("PRECIO_PAQUETE")%>
                                    </td>
                                    <td class="textoGris">
                                        <%# Eval("PRECIO_UNITARIO")%>
                                    </td>
                                    <td class="textoGris">
                                        <asp:Button runat="server" ID="btnActualizar" CssClass="textoGris" Text="Actualizar" OnClientClick='<%# String.Concat("return actualizarMedicamento(", Eval("ID_MEDICAMENTO"), "," , Eval("ID_COMPRA_MEDICAMENTOS"), ");")%>' />
                                    </td>
                                </tr>      
                                <tr>
                                    <td colspan="10" class="alinearCentro"   >
                                        <asp:HiddenField runat="server" ID="hidID_MEDICAMENTO" Value='<%# Eval("ID_MEDICAMENTO")%>' />
                                        <asp:ListView runat="server" ID="lvwLotes" DataSourceID="sqlLotes">
                                            <LayoutTemplate>
                                                <table style="width:90% ; border-collapse:collapse;" class="generica">
                                                    <tr>
                                                        <th class="alterna">
                                                            Reg. Kardex
                                                        </th>
                                                        <th class="alterna">
                                                            Lote
                                                        </th>
                                                        <th class="alterna">
                                                            Fecha de Venc.
                                                        </th>
                                                        <th class="textoGris alterna" style="width:100px;">
                                                            Paq.
                                                        </th>
                                                        <th class="textoGris alterna" style="width:100px;">
                                                            Unid.
                                                        </th>
                                                        <th class="textoGris alterna" style="width:100px;">
                                                            Total Unidades.
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td runat="server" id="itemPlaceHolder">

                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton runat="server" ID="btnEditar" Text='<%# Eval("ID_KARDEX")%>' OnClientClick='<%# String.Concat("return modificarLote(", Eval("ID_KARDEX"), ",", Eval("ID_MOVIMIENTO"),")")%>' ></asp:LinkButton>
                                                    </td>
                                                    <td class="textoGris">
                                                        <%# Eval("NRO_LOTE")%>
                                                    </td>
                                                    <td class="textoGris">
                                                        <%# Eval("F_VENCIMIENTO")%>
                                                    </td>
                                                     <td class="textoNegro alinearDer " style=" padding-right:5px;">
                                                        <asp:label runat="server" ID="lblPaquete" text='<%# Eval("PAQUETE")%>'></asp:label>
                                                    </td>
                                                     <td class="textoNegro  alinearDer "  style=" padding-right:5px;">
                                                        <asp:label runat="server" ID="lblUnidad" text='<%# Eval("UNIDAD")%>'></asp:label>                                                         
                                                    </td>
                                                    <td class="textoNegro  alinearDer "  style=" padding-right:5px;">
                                                        <asp:label runat="server" ID="Label1" text='<%# Eval("CANTIDAD")%>'></asp:label>                                                         
                                                    </td>
                                                </tr>                                               
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <asp:SqlDataSource runat="server" ID="sqlLotes"
                                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                            SelectCommand="SELECT MK.ID_MOVIMIENTO,K.ID_KARDEX,CASE WHEN LEN(ISNULL(K.NRO_LOTE,'')) = 0 THEN 'Sin Lote' ELSE K.NRO_LOTE END AS NRO_LOTE , CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103) AS F_VENCIMIENTO, 
                                            MK.CANTIDAD,M.CANTIDAD_PAQUETE,
                                            CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,1) > 1 THEN  MK.CANTIDAD  % ISNULL(M.CANTIDAD_PAQUETE,1) ELSE MK.CANTIDAD END  AS UNIDAD , 
											CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,1) > 1 THEN CAST(MK.CANTIDAD / ISNULL(M.CANTIDAD_PAQUETE,1) AS INTEGER) ELSE 0 END AS PAQUETE 
                                            FROM KARDEX K
                                            INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX 
                                            INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = K.ID_MEDICAMENTO WHERE MK.ID_COMPRA_MEDICAMENTOS = @ID AND K.ID_MEDICAMENTO = @MEDICAMENTO">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="-1" Name="ID" QueryStringField="ID" />
                            <asp:ControlParameter ControlID="hidID_MEDICAMENTO" Name="MEDICAMENTO" PropertyName="Value" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                                    </td>
                                </tr>                                 
                                                          
                            </ItemTemplate>
                    </asp:ListView>
                    <asp:SqlDataSource runat="server" ID="sqlMedicamentos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT CM.ID_MEDICAMENTO,C.ID_COMPRA,CM.ID_COMPRA_MEDICAMENTOS , CM.PAQUETES, CM.UNIDADES,
ISNULL(SUM(MK.CANTIDAD),0) AS CANTIDAD_RECEPCIONADA,ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_PAQUETE ,
CAST(ISNULL(SUM(MK.CANTIDAD),0) / ISNULL(M.CANTIDAD_PAQUETE,1) AS INTEGER) AS PAQUETE,
CAST(ISNULL(SUM(MK.CANTIDAD),0) % ISNULL(M.CANTIDAD_PAQUETE,1) AS INTEGER) AS UNIDAD,
M.MEDICAMENTO  + ' x ' + CAST(ISNULL(M.CANTIDAD_PAQUETE,1) AS NVARCHAR(6))  + ' - ' + M.FORMA_FARMACEUTICA  + ' (' + M.LABORATORIO   + ')'  AS MEDICAMENTO,
M.PRECIO_PAQUETE, M.PRECIO_UNITARIO, CM.PRECIO_PAQUETE AS PRECIO_PAQUETE_COMPRA, CM.PRECIO_UNIDAD AS PRECIO_UNITARIO_COMPRA
FROM COMPRA C 
INNER JOIN COMPRA_MEDICAMENTOS CM ON CM.ID_COMPRA = C.ID_COMPRA 
INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = CM.ID_MEDICAMENTO 
LEFT JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO 
LEFT JOIN MOVIMIENTO_KARDEX MK ON MK.ID_COMPRA_MEDICAMENTOS = C.ID_COMPRA AND MK.ID_KARDEX = K.ID_KARDEX 
WHERE C.ID_COMPRA = @ID
GROUP BY CM.ID_MEDICAMENTO,C.ID_COMPRA,CM.ID_COMPRA_MEDICAMENTOS , CM.PAQUETES, CM.UNIDADES,
 M.MEDICAMENTO,M.CANTIDAD_PAQUETE,M.FORMA_FARMACEUTICA, M.LABORATORIO,
 M.PRECIO_PAQUETE, M.PRECIO_UNITARIO, CM.PRECIO_PAQUETE, CM.PRECIO_UNIDAD  ">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="-1" Name="ID" QueryStringField="ID" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="alinearDer ">
                    <asp:Button runat="server" ID="btnAceptar" Text="Concluir Recepcion" />
                    &nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnCerrar" Text="Cerrar" CausesValidation="false" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label runat="server" ID="lblError" CssClass="textoRojo"></asp:Label>
                </td>
            </tr>
                
       </table>     
    </div>
            </ContentTemplate>
    </asp:UpdatePanel>
        
    <script type="text/javascript">
        function cancelarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");

        }
        function aceptarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");
            jQuery("#<%=btnCargar.ClientID%>").click()
            return false;
        }
        function gestionLote(idCompra, idMedicamento) {            
                jQuery.noConflict();
                var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
                var frame = dialog.children("iframe");
                var url = "modalRecepcion.aspx?COMPRA=" + idCompra + "&MEDICAMENTO=" + idMedicamento;
                frame.attr("src", url);
                dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "GESTION RECEPCION", width: 475, height: 250 });
                dialog.dialog('open');
                dialog.bind('dialogclose', function () {
                    jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
                });           
            return false;
        }
        function modificarLote(kardex, movimiento) {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "modalRecepcion.aspx?KARDEX=" + kardex + "&MOVIMIENTO=" + movimiento;
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "GESTION RECEPCION", width: 475, height: 250 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function actualizarMedicamento(idMedicamento, compraMedicamento) {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "modalActualizarMedicamento.aspx?ID=" + idMedicamento + "&COMPRA=" + compraMedicamento;
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "GESTION RECEPCION", width: 800, height: 700 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
    </script>
</asp:Content>
