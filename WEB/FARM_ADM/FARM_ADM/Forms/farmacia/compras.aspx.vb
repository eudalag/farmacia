﻿Imports System.Transactions
Public Class compras
    Inherits paginaPrincipal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionCompras.aspx")
    End Sub

    Protected Function tienePagosCredito(ByVal idCompraCredito As Integer) As Boolean
        Dim sSql As String = ""
        sSql += " DECLARE @ID AS INTEGER;"
        sSql += " SET @ID  = " + idCompraCredito.ToString + ";"
        sSql += " SELECT COUNT (*) AS TOTAL FROM PAGO_CREDITO_COMPRA WHERE ID_COMPRA_CREDITO = @ID"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        Return CInt(tbl.Rows(0)(0)) > 0
    End Function

    Protected Function tieneCredito(ByVal idCompra As Integer) As Integer
        Dim sSql As String = ""
        sSql += "DECLARE @ID AS INTEGER;"
        sSql += " SET @ID  = " + idCompra.ToString + ";"
        sSql += " SELECT ID_COMPRA_CREDITO FROM COMPRA_CREDITO WHERE ID_COMPRA = @ID"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        Dim idCredito As Integer = -1
        If tbl.Rows.Count > 0 Then
            idCredito = tbl.Rows(0)(0)
        End If
        Return idCredito
    End Function

    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click
        For Each item In lvwGrilla.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                Dim id = CType(item.FindControl("m_btnEditar"), LinkButton).CommandArgument
                Using scope As New TransactionScope()
                    Try
                        Dim idCredito = tieneCredito(id)
                        If Not tienePagosCredito(idCredito) Then
                            Dim tblCompra = New mcTabla()
                            tblCompra.tabla = "COMPRA"
                            tblCompra.campoID = "ID_COMPRA"
                            tblCompra.valorID = id
                            tblCompra.agregarCampoValor("ID_ESTADO", 3)
                            tblCompra.modificar()
                            If idCredito <> -1 Then
                                tblCompra.reset()
                                tblCompra.tabla = "COMPRA_CREDITO"
                                tblCompra.campoID = "ID_COMPRA_CREDITO"
                                tblCompra.valorID = idCredito
                                tblCompra.eliminar()
                            End If
                            scope.Complete()
                        Else
                            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error3", "alert('Error: el Comprobante de Compra, presenta pagos de credito, por favor revise e intente nuevamente.');", True)
                        End If
                        


                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('Error: Se ha producido un error de base de datos al Eliminar.');", True)
                    End Try
                End Using
                lvwGrilla.DataBind()
            End If
        Next
    End Sub

    Private Sub compras_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Session("COMPRA") Is Nothing Or Not Session("COMPRA") = "" Then
                Dim cadena = Session("COMPRA").ToString.Split(";")
                txtFechaDesde.Text = cadena(0)
                txtFechaHasta.Text = cadena(1)
                ddlTipo.DataBind()
                ddlTipo.SelectedValue = cadena(2)
                ddlEstado.DataBind()
                ddlEstado.SelectedValue = cadena(3)
                txtBuscar.Text = cadena(4)
            Else
                txtFechaDesde.Text = Now.AddDays(-30).ToString("dd/MM/yyyy")
                txtFechaHasta.Text = Now.ToString("dd/MM/yyyy")
                ddlTipo.DataBind()
                ddlEstado.DataBind()
                ddlEstado.SelectedValue = -1

            End If
            lvwGrilla.DataBind()


        End If
    End Sub

    Protected Sub lvwGrilla_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwGrilla.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionCompras.aspx?ID=" + e.CommandArgument)
        End If
    End Sub


    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Session("COMPRA") = txtFechaDesde.Text + ";" + txtFechaHasta.Text + ";" + ddlTipo.SelectedValue.ToString + ";" + ddlEstado.SelectedValue.ToString + ";" + txtBuscar.Text
        lvwGrilla.DataBind()
    End Sub
End Class