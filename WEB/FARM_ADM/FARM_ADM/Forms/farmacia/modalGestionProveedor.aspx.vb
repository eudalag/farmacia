﻿Imports System.Transactions
Public Class modalGestionProveedor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub modalGestionProveedor_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            crearTabla()
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
                hdIdProveedor.Value = Request.QueryString("ID")
            End If
            cargarTabla()
        End If
    End Sub
    Protected Sub cargarDatos(ByVal ID As Integer)
        Dim sSql As String = ""
        sSql = "DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + ID.ToString + ";"
        sSql += " SELECT * FROM PROVEEDORES WHERE ID_PROVEEDOR = @ID;"
        sSql += " SELECT LP.*,L.DESCRIPCION FROM PROVEEDORES_LABORATORIO LP "
        sSql += "INNER JOIN LABORATORIO L  ON L.ID_LABORATORIO = LP.ID_LABORATORIO WHERE LP.ID_PROVEEDOR = @ID;"
        Dim ds = New mcTabla().obtenerDataSet(sSql)
        Dim tbl = ds.Tables(0)
        If tbl.Rows.Count > 0 Then
            txtNombre.Text = esNulo(tbl.Rows(0)("NOMBRE"))
            txtNit.Text = esNulo(tbl.Rows(0)("NIT"))
            txtDireccion_.Text = esNulo(tbl.Rows(0)("DIRECCION"))
            txtCelular.Text = esNulo(tbl.Rows(0)("CELULAR"))
            txtFax.Text = esNulo(tbl.Rows(0)("FAX"))
            txtCorreo.Text = tbl.Rows(0)("CORREO")
        End If
        For Each laboratorio In ds.Tables(1).Rows
            nuevoItem(laboratorio("ID_LABORATORIO"), laboratorio("DESCRIPCION"), 0, 0)
        Next
    End Sub


    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Try
                Using scope As New TransactionScope()
                    Dim tbl = New mcTabla()
                    tbl.tabla = "PROVEEDORES"
                    tbl.campoID = "ID_PROVEEDOR"
                    tbl.modoID = "sql"
                    tbl.agregarCampoValor("NOMBRE", txtNombre.Text)
                    tbl.agregarCampoValor("NIT", txtNit.Text)
                    tbl.agregarCampoValor("DIRECCION", txtDireccion_.Text)
                    tbl.agregarCampoValor("CELULAR", txtCelular.Text)
                    tbl.agregarCampoValor("FAX", txtFax.Text)
                    tbl.agregarCampoValor("CORREO", txtCorreo.Text)
                    If Request.QueryString("ID") Is Nothing Then
                        tbl.insertar()
                    Else
                        tbl.valorID = Request.QueryString("ID")
                        tbl.modificar()
                    End If
                    procesarLaboratorio(tbl.valorID)
                    scope.Complete()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModalProveedor('" + txtNit.Text + "');", True)
                End Using
            Catch ex As Exception
                lblError.Text = ex.Message.ToString
            End Try
        End If
    End Sub
    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_LABORATORIO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "DESCRIPCION"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        ViewState("tblLaboratorio") = tblItems
    End Sub
    Protected Sub nuevoItem(ByVal id_laboratorio As Integer, ByVal descripcion As String, ByVal nuevo As Integer, ByVal accion As Integer)
        Dim tblLaboratorio As DataTable = ViewState("tblLaboratorio")
        Dim foundRows() As DataRow
        foundRows = tblLaboratorio.Select("NRO_FILA = MAX(NRO_FILA)")
        Dim dr As DataRow
        dr = tblLaboratorio.NewRow
        If foundRows.Length = 0 Then
            dr("NRO_FILA") = 1
        Else
            dr("NRO_FILA") = CInt(tblLaboratorio.Select("NRO_FILA = MAX(NRO_FILA)")(0)("NRO_FILA")) + 1
        End If
        dr("ID_LABORATORIO") = id_laboratorio
        dr("DESCRIPCION") = descripcion
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblLaboratorio.Rows.Add(dr)
        tblLaboratorio.AcceptChanges()
        ViewState("tblLaboratorio") = tblLaboratorio
    End Sub
    Public Sub cargarTabla()
        Dim tblLaboratorio As DataTable = ViewState("tblLaboratorio")
        Dim tbl = (From c In tblLaboratorio Where c!ACCION <> 3)
        If tbl.AsDataView.Count > 0 Then
            lvwLaboratorio.DataSource = tbl.CopyToDataTable
        End If
        lvwLaboratorio.DataBind()
    End Sub

    Protected Sub actualizarTabla()
        Dim tblLaboratorio As DataTable = ViewState("tblLaboratorio")
        For Each fila In lvwLaboratorio.Items
            For Each tabla In tblLaboratorio.Rows
                If (tabla("NRO_FILA") = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value) Then
                    Dim nroFila As Integer = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value
                    Dim id_laboratorio As String = CType(fila.FindControl("hfIdLaboratorio"), HiddenField).Value
                    Dim descripcion As String = CType(fila.FindControl("lblDescripcion"), Label).Text

                    If tabla("ID_LABORATORIO") <> id_laboratorio Then
                        tabla("ACCION") = 2
                        tabla("ID_LABORATORIO") = id_laboratorio
                        tabla("DESCRIPCION") = descripcion
                    End If
                End If
            Next
        Next
        tblLaboratorio.AcceptChanges()
        ViewState("tblLaboratorio") = tblLaboratorio
    End Sub
    Protected Sub procesarLaboratorio(ByVal idProveedor As Integer)
        Dim tblDatos = New mcTabla
        Dim tblLaboratorio = CType(ViewState("tblLaboratorio"), DataTable)
        For Each item As DataRow In tblLaboratorio.Select("ACCION <> 0 AND DESCRIPCION <> ''", "NRO_FILA ASC, ACCION DESC")
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    tblDatos.tabla = "PROVEEDORES_LABORATORIO"
                    tblDatos.modoID = "manual"
                    tblDatos.agregarCampoValor("ID_LABORATORIO", item("ID_LABORATORIO"))
                    tblDatos.agregarCampoValor("ID_PROVEEDOR", idProveedor)
                    tblDatos.insertar()
                End If
            Else
                Select Case item("ACCION")
                    Case "2"

                    Case "3"
                        Dim sSql As String = ""
                        sSql += "DECLARE @LABORATORIO AS INTEGER, @PROVEEDOR AS INTEGER;"
                        sSql += " SET @LABORATORIO = " + item("ID_LABORATORIO").ToString + " ;"
                        sSql += " SET @PROVEEDOR = " + idProveedor.ToString + ";"
                        sSql += "DELETE FROM PROVEEDORES_LABORATORIO WHERE ID_LABORATORIO = @LABORATORIO AND ID_PROVEEDOR = @PROVEEDOR;"
                        tblDatos.ejecutarConsulta(sSql)
                End Select
            End If
            tblDatos.reset()
        Next
        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "A", "alert('" + proceso + "')", True)      
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        nuevoItem(hdIdLaboratorio.Value, hdDescripcion.Value, 1, 1)
        cargarTabla()
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        actualizarTabla()
        Dim nroFila As Integer
        For Each item In lvwLaboratorio.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                nroFila = CType(item.FindControl("hfNRO_FILA"), HiddenField).Value
                Exit For
            End If
        Next
        Dim tblItems As DataTable = ViewState("tblLaboratorio")
        For Each row As DataRow In tblItems.Rows
            If CStr(row("NRO_FILA")) = nroFila Then
                row("ACCION") = 3
                Exit For
            End If
        Next
        tblItems.AcceptChanges()
        cargarTabla()
    End Sub
End Class