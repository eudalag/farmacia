﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalRecepcion.aspx.vb" Inherits="FARM_ADM.modalRecepcion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">   
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#txtFechaVencimiento").datepicker({});
        });
        window.addEvent("domready", function () {
            var fecha = new Mascara("txtFechaVencimiento", { 'fijarMascara': '##/##/####', 'esNumero': false });
            
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:100%;">
            <tr>
                <td class="textoGris" style="width:120px;">
                    Cantidad:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCantidad" style="width:50px;">
                    </asp:TextBox>                    
                </td>
            </tr>
            <tr>
                <td class="textoGris" >
                    Nro de Lote:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNroLote">
                    </asp:TextBox>
                    <asp:CheckBox runat="server" ID="chkSinLote" Text="Medicamento Sin Lote" onclick="return sinLote(this);" />
                </td>
            </tr>
            <tr>
                <td class="textoGris">
                    Fecha de Vencimiento:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaVencimiento" style="width:80px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="alinearDer">                                    
                    <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                    <asp:Button runat="server" ID="btnEliminar" Text="Eliminar" OnClientClick="return confirmarEliminacion();" />
                    <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" OnClientClick="return cerrarInformacion();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
        function sinLote(obj) {
            if (obj.checked) {
                document.getElementById("txtNroLote").disabled = true;
            } else {
                document.getElementById("txtNroLote").disabled = false;
            }
        }
        function confirmarEliminacion() {
            return confirm("Esta seguro de eliminar el medicamento Recepcionado?")
        }
    </script>
</body>
</html>
