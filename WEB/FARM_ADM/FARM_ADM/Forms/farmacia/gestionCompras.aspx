﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionCompras.aspx.vb" Inherits="FARM_ADM.gestionCompras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript"  src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtFecha.ClientID%>").datepicker({});
            jQuery("#<%=txtFechaVencimientoCredito.ClientID%>").datepicker({});

        });
        window.addEvent("domready", function () {
            var fecha = new Mascara("<%=txtFecha.ClientID%>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtFechaVencimientoCredito = new Mascara("<%=txtFechaVencimientoCredito.ClientID%>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>          
    <table style="width:100%">
    <tr>
      <td > 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Gestion Compras:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr> 
    <tr>
        <td >
            <fieldset>
                <legend class="textoNegroNegrita">Proveedor</legend>
                <table>
                    <tr>
                            <td class="textoGris" style="width:120px;">
                                Nit:
                            </td>
                            <td style="width:230px;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>  
                                        <asp:TextBox runat="server" ID="txtNit" AutoPostBack="true" MaxLength="15" style="width:100px;" CausesValidation="false"></asp:TextBox>
                                        <asp:Button runat="server" ID="btnBuscarProveedor" Text="Buscar" OnClientClick="return buscarProveedor();" />
                                        <asp:Button runat="server" ID="btnCargar" Text="cargar" style="display:none" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="textoGris" style="width:100px;">
                                Razon Social:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>  
                                        <asp:TextBox runat="server" ID="txtRazonSocial" ReadOnly="true" style="width:250px;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="(*) Ingrese Proveedor" ControlToValidate="txtRazonSocial" CssClass="textoGris" ></asp:RequiredFieldValidator>
                                        <asp:HiddenField runat="server" ID="hdIdProveedor" Value="-1" />
                                        <asp:Button runat="server" ID="btnNuevoProveedor" Text="Nuevo" CausesValidation="false" OnClientClick="return nuevoProveedor();" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                </table>
            </fieldset>
        </td>
    </tr>
    <tr>
        <td >
            <fieldset>
                <legend class="textoNegroNegrita">Comprobante</legend>
                <table> 
                    <tr>
                        <td class="textoGris">
                            Tipo de Comprobante:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" id="ddlTipo" DataSourceID="sqlTipoComprobante" 
                                        DataTextField="DETALLE" DataValueField="ID_TIPO_COMPROBANTE" ></asp:DropDownList>                        
                                    <asp:SqlDataSource runat="server" ID="sqlTipoComprobante" 
                                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                        SelectCommand="SELECT ID_TIPO_COMPROBANTE, DETALLE FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE <> 3
                                        UNION ALL SELECT -1 , '[Seleccione un Tipo Comprobante]'
                                        ORDER BY DETALLE"></asp:SqlDataSource>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="selecionoItem" ErrorMessage="(*) Seleccione una opcion" ControlToValidate="ddlTipo" Display="Dynamic" CssClass="textoGris"></asp:CustomValidator>
                        </td>
                        <td class="textoGris">
                            Tipo de Moneda:
                        </td>
                        <td>
                            <div style="float:left; width: 220px;">
                                <asp:DropDownList runat="server" id="ddlTipoMoneda" DataSourceID="sqlTipoMoneda" 
                                        DataTextField="MONEDA" DataValueField="ID_TIPO_MONEDA"></asp:DropDownList>                        
                                    <asp:SqlDataSource runat="server" ID="sqlTipoMoneda" 
                                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                        SelectCommand="SELECT ID_TIPO_MONEDA, MONEDA FROM TIPO_MONEDA 
                                        UNION ALL SELECT -1 , '[Seleccione un Tipo de Moneda]'
                                        ORDER BY ID_TIPO_MONEDA"></asp:SqlDataSource>
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="selecionoItem" ErrorMessage="(*) Seleccione una opcion" ControlToValidate="ddlTipoMoneda" Display="Dynamic"  CssClass="textoGris"></asp:CustomValidator>
                            </div>
                            <div style="float:left ;" class="textoNegroNegrita">
                                (
                                T/C:
                                <asp:Label runat="server" ID="lblTipoCambio" Text="6.96"></asp:Label>
                                )
                            </div>
                
                        </td>
                    </tr>
                        <tr>
                        <td class="textoGris">Fecha: </td>
                        <td>
                             <asp:TextBox runat="server" ID="txtFecha" style="width:70px;"></asp:TextBox>
                        </td>
                         <td class="textoGris">Nro. Comprobante: </td>
                        <td>
                             <asp:TextBox runat="server" ID="txtNroFactura" style="width:70px;" MaxLength="50"></asp:TextBox>
                        </td>            
                    </tr>
                    <tr>
                       <td class="textoGris">
                            Nro. Autorizacion: 
                        </td>
                         <td>
                            <asp:TextBox runat="server" ID="txtNroAutorizacion" style="width:120px;" MaxLength="50"></asp:TextBox>
                        </td>
                        <td class="textoGris">
                            Codigo Control: 
                        </td>
                         <td>
                            <asp:TextBox runat="server" ID="txtCodigoControl" style="width:120px;" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr> 
                </table>
            </fieldset>
        </td>
    </tr>   
    <tr>
        <td colspan="2">
            <fieldset>
                <legend class="textoNegroNegrita">Detalle Economico</legend>
                <table> 
                    <tr>
                        <td class="textoGris">
                            Tipo de Pago:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlTipoPago">
                                <asp:ListItem Text ="[Seleccione Tipo Pago]" Value="-1"></asp:ListItem>
                                <asp:ListItem Text ="Efectivo" Value="1"></asp:ListItem>
                                <asp:ListItem Text ="Credito" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField runat="server" ID="hidCredito" Value="-1" />
                        </td>
                        <td class="textoGris">
                            Fecha de Limite de Pago:
                        </td>
                        <td>
                             <asp:TextBox runat="server" ID="txtFechaVencimientoCredito" style="width:70px;"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>     
                       
         <tr>
             <td class="alinearDer">
                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate> 
                         <asp:Button ID="e_btnEliminar" runat="server" OnClientClick="return eliminar()" Text="Eliminar" />
                         <asp:Button ID="a_btnNuevo" runat="server" Text="Nuevo Items" OnClientClick="return buscarMedicamento()" />
                        <asp:HiddenField runat="server" id="hdIdMedicamento" Value="-1" />                        
                        <asp:Button runat="server" ID="btnCargarGrilla" Text="cargar" style="display:none;"/>
                    </ContentTemplate>
                </asp:UpdatePanel>
             </td>
         </tr>
      <tr>
             <td>
                 <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate> 
                         <asp:ListView ID="lvwGrilla" runat="server">
                             <LayoutTemplate>
                                 <table class="generica" style="width:100%;">
                                     <tr>
                                         <th style="width:25px;">&nbsp; </th>
                                         <th style="width:40px;">Paq. </th>
                                         <th style="width:40px;">Unid. </th>
                                         <th>Medicamento </th>
                                         <th style="width:60px;">Cant. Paq. </th>                                            
                                         <th style="width:70px;">Precio Paq. </th>
                                         <th style="width:70px;">Precio Unit. </th>
                                         <th style="width:70px;">Sub Total </th>
                                         <th style="width:70px;">Descuento </th>
                                         <th style="width:70px;">Descuento 2</th>
                                         <th style="width:70px;">Total</th>
                                     </tr>
                                     <tr>
                                         <td id="itemPlaceHolder" runat="server"></td>
                                     </tr>
                                 </table>
                             </LayoutTemplate>
                             <ItemTemplate>
                                 <tr>
                                     <td>
                                         <input type="checkbox" runat="server" id="chkEliminar" class="checked textoGris" onchange="seleccionar_item(this)" />
                                         <asp:HiddenField runat="server" ID="hfNRO_FILA" Value='<%#Eval("NRO_FILA")%>' />                                         
                                     </td>
                                     <td class="alinearDer">
                                         <asp:TextBox runat="server" ID="txtPaquetes" CssClass="textoGris numero" onblur="calcularTotalUnidades(this,1);" Style="width:30px;" Text='<%# Eval("PAQUETES")%>'></asp:TextBox>
                                     </td>
                                     <td class="textoGris">
                                         <asp:TextBox runat="server" ID="txtUnidades" CssClass="textoGris numero" onblur="calcularTotalUnidades(this,2);" Style="width:30px;"  Text='<%# Eval("UNIDADES")%>'></asp:TextBox>
                                     </td>
                                     <td class="textoGris">
                                         <asp:Label ID="lblMedicamento" runat="server" Text='<%# Eval("MEDICAMENTO")%>'></asp:Label>
                                         <asp:HiddenField runat="server" ID="hfIdMedicamento" Value='<%# Eval("ID_MEDICAMENTO")%>' />
                                     </td>
                                     <td class="textoGris">
                                         <asp:TextBox runat="server" ID="txtCantPaquete" onblur="calcularTotalUnidades(this,7);" CssClass="textoGris" Style="width:30px;"  Text='<%# Eval("CANTIDAD_PAQUETE")%>'></asp:TextBox>
                                     </td>                                    
                                     <td class="textoGris">
                                         <asp:TextBox ID="txtPrecioPaquete" CssClass="textoGris decimal" Style="width:60px;" onblur="calcularTotalUnidades(this,3);"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:0.0000}", Eval("PRECIO_PAQUETE"))%>' runat="server"></asp:TextBox>
                                     </td>
                                    <td class="textoGris">
                                         <asp:label runat="server" ID="lblPrecioUnitario" CssClass="textoGris" Style="width:60px;"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", Eval("PRECIO_UNIDAD"))%>'></asp:label>
                                         <asp:HiddenField runat="server" ID="hdPrecioUnitario" Value='<%# Eval("PRECIO_UNIDAD") %>' />
                                     </td>
                                     <td class="textoGris">
                                         <asp:label runat="server" ID="lblSubTotal" CssClass="textoGris" Style="width:60px;"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", ((Eval("PRECIO_PAQUETE") * Eval("PAQUETES")) + (Eval("UNIDADES") *  Eval("PRECIO_UNIDAD"))))%>'></asp:label>
                                     </td>
                                      <td class="textoGris">
                                          <input type="checkbox" runat="server" id="chkDescuento" class="chk" onclick="return calcularTotalComprobante(this,4);" checked='<%# IIF(Eval("APLICA_DESC_UNITARIO") = "True","true","false") %>' />
                                          <asp:TextBox ID="txtDescuento" Style="width:40px;" CssClass="textoGris decimal" onblur="calcularTotalComprobante(this,5);"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:0.00}",Eval("DESCUENTO"))%>' runat="server"></asp:TextBox>
                                     </td>
                                      <td class="textoGris">
                                          <asp:TextBox ID="txtDescuento2"  Style="width:40px;" CssClass="textoGris decimal" onblur="calcularTotalComprobante(this,6);"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:0.00}",Eval("DESCUENTO2"))%>' runat="server"></asp:TextBox>
                                     </td>
                                     <td class="textoGris alinearDer">
                                         <asp:Label ID="lblTotal" runat="server" CssClass="textoGris total" Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", (((Eval("PRECIO_PAQUETE") * Eval("PAQUETES")) + (Eval("UNIDADES") * (Eval("PRECIO_PAQUETE") / Eval("CANTIDAD_PAQUETE")))) - (Eval("DESCUENTO") + Eval("DESCUENTO2"))))%>'></asp:Label>
                                     </td>                                     
                                 </tr>
                             </ItemTemplate>
                             <EmptyDataTemplate>
                                 <table class="generica" style="width:100%;">
                                      <tr>
                                         <th style="width:25px;">&nbsp; </th>
                                         <th style="width:60px;">Paquete </th>
                                         <th style="width:65px;">Unidades </th>
                                         <th>Medicamento </th>
                                         <th style="width:100px;">Cant. Paquete </th>                                         
                                         <th style="width:100px;">Precio Paq. </th>
                                         <th style="width:100px;">Precio Unit. </th>
                                         <th style="width:100px;">Sub Total</th>
                                         <th style="width:70px;">Descuento </th>
                                          <th style="width:70px;">Descuento 2</th>
                                         <th style="width:100px;">Total</th>
                                     </tr>
                                     <tr>
                                         <td colspan="11" class="textoGris alinearCentro">
                                             No Existen Datos.
                                         </td>
                                     </tr>
                                </table>
                             </EmptyDataTemplate>
                         </asp:ListView> 
                     </ContentTemplate>
                </asp:UpdatePanel>                
             </td>
         </tr> 
        <tr>
            <td>
                <table style="width:100%">
                    <tr>
                        <td class="alinearDer" >
                            SubTotal:
                        </td>
                        <td style="width:100px;" class="alinearDer">
                            <asp:TextBox ID="txtSubTotal" Style="width:60px;" ReadOnly="true" CssClass="alinearDer textoGris" Text="0.00"  runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="alinearDer">
                            Descuento Unitario:
                        </td>
                        <td class="alinearDer">                            
                            <asp:TextBox ID="txtTotalDescuentoUnitario" Style="width:60px;" CssClass="alinearDer textoGris" ReadOnly="true" Text="0.00" runat="server"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="alinearDer">
                            Descuento General 1:
                        </td>
                        <td class="alinearDer">                            
                            <asp:TextBox ID="txtDescuentoGeneral" onblur="calcularSubtotal();" Style="width:60px;" CssClass="alinearDer textoGris" Text="0.00" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="alinearDer">
                            Descuento General 2:
                        </td>
                        <td class="alinearDer">
                            <asp:TextBox ID="txtDescuentoGeneral2" onblur="calcularSubtotal();" Style="width:60px;" CssClass="alinearDer textoGris" Text="0.00" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="alinearDer">
                            Total:
                        </td>
                        <td class="alinearDer">
                            <asp:TextBox runat="server" ID="txtTotalNeto" Style="width:60px;" disabled="true" CssClass="alinearDer textoGris" Text="0.00"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hfTotalNeto" Value="0" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td     class="alinearDer" style="padding-top:15px;">
                <asp:Button runat="server" ID="btnBorrador" Text="Guardar Borrador" />&nbsp;
                <asp:Button runat="server" ID="btnAceptar" Text="Concluir Comprobante" />&nbsp;
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" />
            </td>
        </tr>   
        <tr>
            <td >
                <asp:Label runat="server" ID="lblError" CssClass="textoRojo"></asp:Label>
            </td>
        </tr> 
        </table> 
            
    <script type="text/javascript">
        function cancelarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");

        }
        function aceptarModalProveedor(nit) {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");
            jQuery("#<%=txtNit.ClientID%>").val(nit);
            jQuery("#<%=btnCargar.ClientID%>").click()
            return false;
        }
        function nuevoProveedor() {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "modalGestionProveedor.aspx";
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR PROVEEDOR", width: 700, height: 600 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function buscarProveedor() {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "buscarProveedores.aspx";
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR PROVEEDOR", width: 600, height: 550 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function aceptarModal(idMedicamento) {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");            
            jQuery("#<%=hdIdMedicamento.ClientID%>").val(idMedicamento);
            jQuery("#<%=btnCargarGrilla.ClientID%>").click()
            return false;
        }
        function buscarMedicamento() {
            var valido = Page_ClientValidate('');
            if (valido) {
                jQuery.noConflict();
                var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
                var frame = dialog.children("iframe");
                var url = "../inventarios/buscarMedicamento.aspx?PROVEEDOR=" + jQuery("#<%=hdIdProveedor.ClientID%>").val();
                frame.attr("src", url);
                dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR MEDICAMENTOS", width: 800, height: 600 });
                dialog.dialog('open');
                dialog.bind('dialogclose', function () {
                    jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
                });
            }
            return false;
        }

        function selecionoItem(source, args) {
            if (args.Value != "-1")
                args.IsValid = true;
            else
                args.IsValid = false;
        }
        function formatNumber(num) {
            num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
            splitRight = splitRight + '00';
            splitRight = splitRight.substr(0, 3);
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
            }
            return splitLeft + splitRight;
        }
        function ReplaceAll(Source, stringToFind, stringToReplace) {
            var temp = Source;
            var index = temp.indexOf(stringToFind);
            while (index != -1) {
                temp = temp.replace(stringToFind, stringToReplace);
                index = temp.indexOf(stringToFind);
            }
            return temp;
        }
        function calcularTotalUnidades(obj, tipo) {
            var idObjeto = "";
            switch (tipo) {
                case 1:
                    idObjeto = "txtPaquetes";
                    break;
                case 2:
                    idObjeto = "txtUnidades";
                    break;
                case 3:
                    idObjeto = "txtPrecioPaquete";
                    break;
                case 7:
                    idObjeto = "txtCantPaquete";
            }
            var txtPaquetes = parseInt(jQuery("#" + obj.id.replace(idObjeto, "txtPaquetes")).val());
            
            var txtUnidades = parseInt(jQuery("#" + obj.id.replace(idObjeto, "txtUnidades")).val());
            
            var txtCantPaquete = parseInt(jQuery("#" + obj.id.replace(idObjeto, "txtCantPaquete")).val());
            
            var precioPaquete = parseFloat(jQuery("#" + obj.id.replace(idObjeto, "txtPrecioPaquete")).val());

            var hdPrecioUnitario = parseFloat(jQuery("#" + obj.id.replace(idObjeto, "hdPrecioUnitario")).val());
            
            var precioUnitario = precioPaquete / txtCantPaquete;
            
            
            var total = (txtPaquetes * precioPaquete) + (txtUnidades * (precioUnitario));

            jQuery("#" + obj.id.replace(idObjeto, "lblPrecioUnitario")).html(formatNumber(precioUnitario));
            jQuery("#" + obj.id.replace(idObjeto, "hdPrecioUnitario")).val(precioUnitario);
            jQuery("#" + obj.id.replace(idObjeto, "lblSubTotal")).html(formatNumber(total));
            calcularTotalComprobante(obj, tipo);
        }
        function calcularTotalComprobante(obj, tipo) {
            var idObjeto = "";
            switch (tipo) {
                case 1:
                    idObjeto = "txtPaquetes";
                    break;
                case 2:
                    idObjeto = "txtUnidades";
                    break;
                case 3:
                    idObjeto = "txtPrecioPaquete";
                    break;
                case 4:
                    idObjeto = "chkDescuento";
                    break;
                case 5:
                    idObjeto = "txtDescuento";
                case 6:
                    idObjeto = "txtDescuento2";
                case 7:
                    idObjeto = "txtCantPaquete";
                    break;
            }
            var SubTotal = parseFloat(ReplaceAll(jQuery("#" + obj.id.replace(idObjeto, "lblSubTotal")).html(), ",", ""));
            var chkDescuento = jQuery("#" + obj.id.replace(idObjeto, "chkDescuento")).attr("checked")
            var descuento = chkDescuento ? 0 : parseFloat(jQuery("#" + obj.id.replace(idObjeto, "txtDescuento")).val());
            var descuento2 = parseFloat(jQuery("#" + obj.id.replace(idObjeto, "txtDescuento2")).val());
            var total = (SubTotal - (descuento + descuento2))
            jQuery("#" + obj.id.replace(idObjeto, "lblTotal")).html(formatNumber(total));
            calcularTotalDescuentoUnitario();
            calcularSubtotal();
        }
        function calcularTotalDescuentoUnitario() {
            var idObj = "";
            var txtPaquetes = 0;
            var descuento = 0;
            var subTotal = 0;
            jQuery(".chk").each(function () {
                if (jQuery(this).attr("checked")) {
                    idObj = jQuery(this).attr("id");
                    idObj = idObj.replace("chkDescuento", "txtPaquetes")
                    txtPaquetes = parseInt(jQuery("#" + idObj).val());
                    idObj = jQuery(this).attr("id");
                    idObj = idObj.replace("chkDescuento", "txtDescuento")
                    txtDescuento = parseFloat(jQuery("#" + idObj).val());
                    subTotal = subTotal + (parseFloat(txtPaquetes) * parseFloat(txtDescuento))
                }
            });
            jQuery("#<%= txtTotalDescuentoUnitario.ClientID%>").val(formatNumber(subTotal));
        }
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblMedicamento");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar el medicamento:" + lblDescripcion.innerHTML));
                }
            });
        }
        function calcularSubtotal() {
            var subTotal = 0;
            jQuery(".total").each(function () {
                subTotal = subTotal + parseFloat(ReplaceAll(jQuery(this).html(), ",", ""));
            });

            jQuery("#<%= txtSubTotal.ClientID%>").val(formatNumber(subTotal));
            var neto = subTotal - (parseFloat(jQuery("#<%= txtTotalDescuentoUnitario.ClientID%>").val()) + parseFloat(jQuery("#<%= txtDescuentoGeneral.ClientID%>").val()) + parseFloat(jQuery("#<%= txtDescuentoGeneral2.ClientID%>").val()));
            jQuery("#<%= txtTotalNeto.ClientID%>").val(formatNumber(neto));
            jQuery("#<%= hfTotalNeto.ClientID%>").val(neto);
        }
    </script>

</asp:Content>
