﻿Imports System.Transactions
Public Class gestionCompras
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)        
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_COMPRA_MEDICAMENTOS"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "MEDICAMENTO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "PAQUETES"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "UNIDADES"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "CANTIDAD_PAQUETE"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_PAQUETE"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_UNIDAD"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Boolean")
        columna.ColumnName = "APLICA_DESC_UNITARIO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "DESCUENTO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "DESCUENTO2"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        ViewState("tblMedicamento") = tblItems
    End Sub
    Protected Sub nuevoItem(ByVal idMedicamento As Integer, ByVal idCompraMedicamento As Integer, ByVal medicamento As String, ByVal cant_paquete As Integer, ByVal nuevo As Integer, ByVal accion As Integer, Optional ByVal aplicaDescuento As Boolean = True)
        Dim tblMedicamento As DataTable = ViewState("tblMedicamento")
        Dim foundRows() As DataRow
        foundRows = tblMedicamento.Select("NRO_FILA = MAX(NRO_FILA)")
        Dim dr As DataRow
        dr = tblMedicamento.NewRow
        If foundRows.Length = 0 Then
            dr("NRO_FILA") = 1
        Else
            dr("NRO_FILA") = CInt(tblMedicamento.Select("NRO_FILA = MAX(NRO_FILA)")(0)("NRO_FILA")) + 1
        End If
        dr("ID_MEDICAMENTO") = idMedicamento
        dr("ID_COMPRA_MEDICAMENTOS") = idCompraMedicamento
        dr("MEDICAMENTO") = medicamento
        dr("PAQUETES") = 0
        dr("UNIDADES") = 0
        dr("CANTIDAD_PAQUETE") = IIf(cant_paquete = 0, 1, cant_paquete)
        dr("PRECIO_PAQUETE") = 0
        dr("PRECIO_UNIDAD") = 0
        dr("APLICA_DESC_UNITARIO") = aplicaDescuento
        dr("DESCUENTO") = 0
        dr("DESCUENTO2") = 0
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblMedicamento.Rows.Add(dr)
        tblMedicamento.AcceptChanges()
        ViewState("tblMedicamento") = tblMedicamento
    End Sub
    Public Sub cargarGrilla()
        Dim tblMedicamento As DataTable = ViewState("tblMedicamento")
        Dim tbl = (From c In tblMedicamento Where c!ACCION <> 3)
        If tbl.AsDataView.Count > 0 Then
            lvwGrilla.DataSource = tbl.CopyToDataTable
        End If
        lvwGrilla.DataBind()
    End Sub

    Protected Sub actualizarTabla()
        Dim tblMedicamento As DataTable = ViewState("tblMedicamento")
        For Each fila In lvwGrilla.Items
            For Each tabla In tblMedicamento.Rows
                If (tabla("NRO_FILA") = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value) Then
                    Dim nroFila As Integer = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value
                    Dim idMedicamento As String = CType(fila.FindControl("hfIdMedicamento"), HiddenField).Value
                    Dim medicamento As String = CType(fila.FindControl("lblMedicamento"), Label).Text
                    Dim paquetes As Integer = Decimal.Parse(CType(fila.FindControl("txtPaquetes"), TextBox).Text, New Globalization.CultureInfo("en-US"))
                    Dim unidades As Integer = Decimal.Parse(CType(fila.FindControl("txtUnidades"), TextBox).Text, New Globalization.CultureInfo("en-US"))
                    Dim cantidad_paquete As Integer = Decimal.Parse(CType(fila.FindControl("txtCantPaquete"), TextBox).Text, New Globalization.CultureInfo("en-US"))
                    Dim precioPaquete As Decimal = Decimal.Parse(CType(fila.FindControl("txtPrecioPaquete"), TextBox).Text, New Globalization.CultureInfo("en-US"))
                    Dim precioUnidad As Decimal = Decimal.Parse(CType(fila.FindControl("hdPrecioUnitario"), HiddenField).Value, New Globalization.CultureInfo("en-US"))
                    Dim chkDescuento As Boolean = CBool(CType(fila.FindControl("chkDescuento"), HtmlInputCheckBox).Checked)
                    Dim descuento As Decimal = Decimal.Parse(CType(fila.FindControl("txtDescuento"), TextBox).Text, New Globalization.CultureInfo("en-US"))
                    Dim descuento2 As Decimal = Decimal.Parse(CType(fila.FindControl("txtDescuento2"), TextBox).Text, New Globalization.CultureInfo("en-US"))                    


                    If tabla("ID_MEDICAMENTO") <> idMedicamento Or tabla("PAQUETES") <> paquetes Or tabla("UNIDADES") <> unidades Or tabla("PRECIO_PAQUETE") <> precioPaquete Or tabla("DESCUENTO") <> descuento Or tabla("DESCUENTO2") <> descuento2 Or tabla("CANTIDAD_PAQUETE") <> cantidad_paquete Then
                        tabla("ACCION") = 2
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("MEDICAMENTO") = medicamento
                        tabla("PAQUETES") = paquetes
                        tabla("UNIDADES") = unidades
                        tabla("CANTIDAD_PAQUETE") = cantidad_paquete
                        tabla("PRECIO_UNIDAD") = precioUnidad
                        tabla("PRECIO_PAQUETE") = precioPaquete
                        tabla("APLICA_DESC_UNITARIO") = chkDescuento
                        tabla("DESCUENTO") = descuento
                        tabla("DESCUENTO2") = descuento2
                    End If
                End If
            Next
        Next
        tblMedicamento.AcceptChanges()
        ViewState("tblMedicamento") = tblMedicamento
    End Sub
    Protected Sub procesarMedicamento(ByVal idCompra As Integer)
        Dim tblDatos = New mcTabla
        Dim tblCodigoBarra = CType(ViewState("tblMedicamento"), DataTable)
        For Each item As DataRow In tblCodigoBarra.Select("ACCION <> 0", "NRO_FILA ASC, ACCION DESC")
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    tblDatos.tabla = "COMPRA_MEDICAMENTOS"
                    tblDatos.campoID = "ID_COMPRA_MEDICAMENTOS"
                    tblDatos.modoID = "sql"
                    tblDatos.agregarCampoValor("ID_COMPRA", idCompra)
                    tblDatos.agregarCampoValor("ID_MEDICAMENTO", item("ID_MEDICAMENTO"))
                    tblDatos.agregarCampoValor("PAQUETES", item("PAQUETES"))
                    tblDatos.agregarCampoValor("UNIDADES", item("UNIDADES"))
                    tblDatos.agregarCampoValor("PRECIO_PAQUETE", item("PRECIO_PAQUETE"))
                    tblDatos.agregarCampoValor("PRECIO_UNIDAD", item("PRECIO_UNIDAD"))
                    tblDatos.agregarCampoValor("APLICA_DESC_UNITARIO", item("APLICA_DESC_UNITARIO"))
                    tblDatos.agregarCampoValor("DESCUENTO", item("DESCUENTO"))
                    tblDatos.agregarCampoValor("DESCUENTO2", item("DESCUENTO2"))                    
                    tblDatos.insertar()
                    actualizarCantPaquete(item("ID_MEDICAMENTO"), item("CANTIDAD_PAQUETE"))
                End If
            Else
                Select Case item("ACCION")
                    Case "2"
                        tblDatos.tabla = "COMPRA_MEDICAMENTOS"
                        tblDatos.campoID = "ID_COMPRA_MEDICAMENTOS"
                        tblDatos.valorID = item("ID_COMPRA_MEDICAMENTOS")
                        tblDatos.agregarCampoValor("ID_MEDICAMENTO", item("ID_MEDICAMENTO"))
                        tblDatos.agregarCampoValor("PAQUETES", item("PAQUETES"))
                        tblDatos.agregarCampoValor("UNIDADES", item("UNIDADES"))
                        tblDatos.agregarCampoValor("PRECIO_PAQUETE", item("PRECIO_PAQUETE"))
                        tblDatos.agregarCampoValor("PRECIO_UNIDAD", item("PRECIO_UNIDAD"))
                        tblDatos.agregarCampoValor("APLICA_DESC_UNITARIO", item("APLICA_DESC_UNITARIO"))
                        tblDatos.agregarCampoValor("DESCUENTO", item("DESCUENTO"))
                        tblDatos.agregarCampoValor("DESCUENTO2", item("DESCUENTO2"))                        
                        tblDatos.modificar()
                        actualizarCantPaquete(item("ID_MEDICAMENTO"), item("CANTIDAD_PAQUETE"))
                    Case "3"
                        tblDatos.tabla = "COMPRA_MEDICAMENTOS"
                        tblDatos.campoID = "ID_COMPRA_MEDICAMENTOS"
                        tblDatos.valorID = item("ID_COMPRA_MEDICAMENTOS")
                        tblDatos.eliminar()
                End Select
            End If
            tblDatos.reset()
        Next
        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "A", "alert('" + proceso + "')", True)      
    End Sub


    Protected Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged
        Dim sSql As String = ""
        sSql += " DECLARE @NIT AS NVARCHAR(50);"
        sSql += " SET @NIT = '" + Trim(txtNit.Text) + "';"
        sSql += " SELECT ID_PROVEEDOR,NOMBRE,NIT FROM PROVEEDORES "
        sSql += " WHERE NIT = @NIT"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows().Count > 0 Then
            txtRazonSocial.Text = tbl.Rows(0)("NOMBRE")
            hdIdProveedor.Value = tbl.Rows(0)("ID_PROVEEDOR")
        Else
            hdIdProveedor.Value = -1
            txtRazonSocial.Text = ""
        End If
        actualizarTabla()
        cargarGrilla()
    End Sub

    Protected Sub btnCargarGrilla_Click(sender As Object, e As EventArgs) Handles btnCargarGrilla.Click
        actualizarTabla()
        Dim idMedicamento As Integer = hdIdMedicamento.Value

        Dim sSql As String = ""
        Dim tbl As DataTable

        sSql += "SELECT ID_MEDICAMENTO ,MEDICAMENTO +  ' (' + LABORATORIO + ') x ' + CAST(ISNULL(CANTIDAD_PAQUETE,1) AS NVARCHAR(10)) + ' ' + FORMA_FARMACEUTICA AS MEDICAMENTO,ISNULL(CANTIDAD_PAQUETE,1) AS CANTIDAD_PAQUETE FROM VW_MEDICAMENTO WHERE ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
        tbl = New mcTabla().ejecutarConsulta(sSql)


        nuevoItem(tbl.Rows(0)("ID_MEDICAMENTO"), -1, tbl.Rows(0)("MEDICAMENTO"), tbl.Rows(0)("CANTIDAD_PAQUETE"), 1, 1)
        cargarGrilla()
    End Sub

    Private Sub gestionCompras_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then

            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            Else
                crearTabla()
                txtFecha.Text = Now.ToString("dd/MM/yyyy")
                cargarGrilla()
            End If

        End If
    End Sub
    Protected Sub cargarDatos(ByVal id As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + id.ToString + ";"
        sSql += " SELECT P.NIT, P.ID_PROVEEDOR, P.NOMBRE, C.ID_TIPO_COMPROBANTE , C.ID_MONEDA , C.TC,"
        sSql += " CONVERT(NVARCHAR(10),C.FECHA,103) AS FECHA, C.NRO_COMPROBANTE, C.NRO_AUTORIZACION, "
        sSql += " C.CODIGO_CONTROL,C.DESCUENTO_GENERAL,C.DESCUENTO_GENERAL2,"
        sSql += " CASE WHEN CC.ID_COMPRA_CREDITO IS NULL THEN 1 ELSE 2 END AS ID_TIPO_PAGO,"
        sSql += " ISNULL(CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103),'') AS VENCIMIENTO,ISNULL(CC.ID_COMPRA_CREDITO,-1) AS ID_COMPRA_CREDITO    FROM COMPRA C"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR "
        sSql += " LEFT JOIN COMPRA_CREDITO CC ON CC.ID_COMPRA = C.ID_COMPRA "
        sSql += " WHERE C.ID_COMPRA = @ID;"

        sSql += " SELECT CM.ID_MEDICAMENTO , CM.ID_COMPRA_MEDICAMENTOS,ROW_NUMBER() OVER (ORDER BY CM.ID_MEDICAMENTO ) AS NRO_FILA,"
        sSql += " MEDICAMENTO +  ' (' + LABORATORIO + ') x ' + CAST(ISNULL(CANTIDAD_PAQUETE,1) AS NVARCHAR(10)) + ' ' + FORMA_FARMACEUTICA AS MEDICAMENTO,"
        sSql += " CM.PAQUETES,CM.UNIDADES,ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_PAQUETE ,CM.PRECIO_PAQUETE,CM.PRECIO_UNIDAD,CM.APLICA_DESC_UNITARIO,CM.DESCUENTO,CM.DESCUENTO2, 0 AS NUEVO, 0 AS ACCION FROM COMPRA_MEDICAMENTOS CM"
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = CM.ID_MEDICAMENTO "
        sSql += " WHERE ID_COMPRA = @ID"
        Dim ds = New mcTabla().obtenerDataSet(sSql)
        Dim tbl = ds.Tables(0)
        txtNit.Text = tbl.Rows(0)("NIT")
        hdIdProveedor.Value = tbl.Rows(0)("ID_PROVEEDOR")
        txtRazonSocial.Text = tbl.Rows(0)("NOMBRE")
        ddlTipo.SelectedValue = tbl.Rows(0)("ID_TIPO_COMPROBANTE")
        ddlTipoMoneda.SelectedValue = tbl.Rows(0)("ID_MONEDA")
        lblTipoCambio.Text = tbl.Rows(0)("TC")
        txtFecha.Text = tbl.Rows(0)("FECHA")
        txtNroFactura.Text = tbl.Rows(0)("NRO_COMPROBANTE")
        txtNroAutorizacion.Text = tbl.Rows(0)("NRO_AUTORIZACION")
        txtCodigoControl.Text = tbl.Rows(0)("CODIGO_CONTROL")
        txtDescuentoGeneral.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("DESCUENTO_GENERAL"))
        txtDescuentoGeneral2.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("DESCUENTO_GENERAL2"))
        txtFechaVencimientoCredito.Text = tbl.Rows(0)("VENCIMIENTO")
        ddlTipoPago.SelectedValue = tbl.Rows(0)("ID_TIPO_PAGO")
        hidCredito.Value = tbl.Rows(0)("ID_COMPRA_CREDITO")
        ViewState("tblMedicamento") = ds.Tables(1)
        cargarGrilla()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Cargar", "calcularSubtotal();", True)

    End Sub

    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click

        actualizarTabla()
        Dim tblMedicamento As DataTable = ViewState("tblMedicamento")
        For Each item In lvwGrilla.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                For Each row As DataRow In tblMedicamento.Rows
                    If CStr(row("NRO_FILA")) = CType(item.FindControl("hfNRO_FILA"), HiddenField).Value Then
                        row("ACCION") = 3
                        Exit For
                    End If
                Next
                Exit For
            End If
        Next
        tblMedicamento.AcceptChanges()
        cargarGrilla()

    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            actualizarTabla()
            Using scope As New TransactionScope()
                Try
                    Dim idCompra As Integer = procesarCompra(2)
                    procesarMedicamento(idCompra)
                    If ddlTipoPago.SelectedValue = 2 Then
                        procesarCredito(idCompra)
                        If CInt(hidCredito.Value) <> -1 Then
                            Dim tblCredito = New mcTabla()
                            tblCredito.tabla = "COMPRA_CREDITO"
                            tblCredito.campoID = "ID_COMPRA_CREDITO"
                            tblCredito.valorID = hidCredito.Value
                            tblCredito.eliminar()
                        End If
                    End If
                    scope.Complete()
                    Response.Redirect("compras.aspx?idMenu=15")
                Catch ex As Exception
                    lblError.Text = ex.Message.ToString
                End Try
            End Using
        End If
    End Sub
    Function procesarCompra(ByVal Estado As Integer)
        Dim tblCompra = New mcTabla()
        tblCompra.tabla = "COMPRA"
        tblCompra.modoID = "sql"
        tblCompra.campoID = "ID_COMPRA"
        tblCompra.agregarCampoValor("ID_ORDEN_COMPRA", "NULL")
        tblCompra.agregarCampoValor("ID_MONEDA", ddlTipoMoneda.SelectedValue)
        tblCompra.agregarCampoValor("ID_ESTADO", Estado)
        tblCompra.agregarCampoValor("ID_TIPO_COMPROBANTE", ddlTipo.SelectedValue)
        tblCompra.agregarCampoValor("TC", lblTipoCambio.Text)
        tblCompra.agregarCampoValor("ID_PROVEEDOR", hdIdProveedor.Value)
        tblCompra.agregarCampoValor("FECHA", txtFecha.Text, SqlDbType.Date)
        tblCompra.agregarCampoValor("NRO_AUTORIZACION", txtNroAutorizacion.Text)
        tblCompra.agregarCampoValor("CODIGO_CONTROL", txtCodigoControl.Text)
        tblCompra.agregarCampoValor("TOTAL", hfTotalNeto.Value)
        tblCompra.agregarCampoValor("NRO_COMPROBANTE", txtNroFactura.Text)
        tblCompra.agregarCampoValor("DESCUENTO_GENERAL", txtDescuentoGeneral.Text)
        tblCompra.agregarCampoValor("DESCUENTO_GENERAL2", txtDescuentoGeneral2.Text)
        If Request.QueryString("ID") Is Nothing Then
            tblCompra.insertar()
        Else
            tblCompra.valorID = Request.QueryString("ID")
            tblCompra.modificar()
        End If
        Return tblCompra.valorID
    End Function
    Protected Sub procesarCredito(ByVal idCompra As Integer)
        Dim tblCredito = New mcTabla()
        'Dim sSql = "SELECT ID_COMPRA_CREDITO AS TOTAL FROM COMPRA_CREDITO WHERE ID_COMPRA = " + idCompra.ToString
        'Dim tblAux = tblCredito.ejecutarConsulta(sSql)
        tblCredito.tabla = "COMPRA_CREDITO"
        tblCredito.campoID = "ID_COMPRA_CREDITO"
        tblCredito.modoID = "sql"
        tblCredito.agregarCampoValor("ID_COMPRA", idCompra)
        tblCredito.agregarCampoValor("IMPORTE_CREDITO", hfTotalNeto.Value)
        tblCredito.agregarCampoValor("ESTADO", 1)
        tblCredito.agregarCampoValor("SALDO", hfTotalNeto.Value)
        tblCredito.agregarCampoValor("F_VENCIMIENTO", txtFechaVencimientoCredito.Text)
        tblCredito.agregarCampoValor("DESCUENTO", 0)
        If CInt(hidCredito.Value) <> -1 Then
            Dim idCredito = hidCredito.Value
            tblCredito.valorID = idCredito
            tblCredito.modificar()
        Else
            tblCredito.insertar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("compras.aspx?idMenu=15")
    End Sub

    Protected Sub btnBorrador_Click(sender As Object, e As EventArgs) Handles btnBorrador.Click
        If Page.IsValid Then

            Using scope As New TransactionScope()
                Try
                    actualizarTabla()
                    Dim idCompra As Integer = procesarCompra(1)
                    procesarMedicamento(idCompra)
                    If ddlTipoPago.SelectedValue = 2 Then
                        procesarCredito(idCompra)
                    Else
                        If CInt(hidCredito.Value) <> -1 Then
                            Dim tblCredito = New mcTabla()
                            tblCredito.tabla = "COMPRA_CREDITO"
                            tblCredito.campoID = "ID_COMPRA_CREDITO"
                            tblCredito.valorID = hidCredito.Value
                            tblCredito.eliminar()
                        End If
                    End If
                    scope.Complete()
                    Response.Redirect("compras.aspx?idMenu=15")
                Catch ex As Exception
                    lblError.Text = ex.Message.ToString
                End Try
            End Using
        End If
    End Sub
    Protected Sub actualizarCantPaquete(ByVal idMedicamento As Integer, ByVal cantPaquete As Decimal)
        Dim tbl = New mcTabla()
        tbl.tabla = "MEDICAMENTO"
        tbl.campoID = "ID_MEDICAMENTO"
        tbl.valorID = idMedicamento
        tbl.agregarCampoValor("CANTIDAD_PAQUETE", cantPaquete)
        tbl.modificar()
    End Sub
End Class