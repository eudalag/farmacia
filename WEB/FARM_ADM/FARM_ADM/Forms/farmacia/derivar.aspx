﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="derivar.aspx.vb" Inherits="FARM_ADM.derivar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <table style="width:100%;">                
                <tr>
                    <td class="textoGris" style="width:125px;">
                        Estado:
                    </td>
                    <td>
                         <asp:DropDownList runat="server" id="ddlEstado" DataSourceID="sqlEstado" 
                            DataTextField="DESCRIPCION" DataValueField="ID_ESTADO"></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlEstado" 
                            ConnectionString="<%$ ConnectionStrings:FARMACIA_WEB.My.MySettings.cnnFarmacia %>" 
                            SelectCommand="SELECT ID_ESTADO, DESCRIPCION FROM ESTADO_COMPRA WHERE ID_ESTADO <> 3
                            UNION ALL SELECT -1, '[Seleccione un Estado]'
                            ORDER BY DESCRIPCION"></asp:SqlDataSource>                                                                                               
                    </td>                    
                </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" class="alinearDer">
                    <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="return cerrarInformacion();" CausesValidation="false" />
                </td>
            </tr>
            </table>
    </div>
    </form>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
    </script> 
</body>
</html>
