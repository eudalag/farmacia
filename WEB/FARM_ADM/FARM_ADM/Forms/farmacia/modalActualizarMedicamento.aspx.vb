﻿Imports System.Transactions
Public Class modalActualizarMedicamento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    
    Protected Sub cargarPrecio(ByVal idMedicamento As Integer)
        Dim sSql As String = "DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + idMedicamento.ToString + ";"
        sSql += " SELECT * FROM PRECIOS_EXTRAS WHERE ID_PRECIO_MEDICAMENTO = 1 AND ID_MEDICAMENTO = @ID;"
        sSql += " SELECT * FROM PRECIOS_EXTRAS WHERE ID_PRECIO_MEDICAMENTO = 2 AND ID_MEDICAMENTO = @ID;"
        Dim ds = New mcTabla().obtenerDataSet(sSql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtPrecio1.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", ds.Tables(0).Rows(0)("PRECIO"))
            hdPrecio1.Value = ds.Tables(0).Rows(0)("ID_PRECIO")
        End If
        If ds.Tables(1).Rows.Count > 0 Then
            txtPrecio2.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", ds.Tables(1).Rows(0)("PRECIO"))
            hdPrecio2.Value = ds.Tables(1).Rows(0)("ID_PRECIO")
        End If
    End Sub
    Protected Sub cargarDatos(ByVal idMedicamento As Integer, ByVal idCompra As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_COMPRA AS INTEGER, @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_COMPRA = " + idCompra.ToString + ";"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT M.ID_MEDICAMENTO ,M.NOMBRE AS MEDICAMENTO, COMPOSICION, L.ID_LABORATORIO , ID_TIPO_MEDICAMENTO AS ID_FORMA_FARMACEUTICA,COD_REG_SAN,CONCENTRACION,CODIGO_BARRA,CONTROLADO,"
        sSql += " ACCION AS ACCION_TERAPEUTICA, CANTIDAD_PAQUETE,CM.PRECIO_PAQUETE AS PRECIO_PAQUETE_COMPRA,M.PRECIO_COMPRA AS PRECIO_COMPRA_ANT, CM.PRECIO_UNIDAD AS PRECIO_UNIDAD_COMPRA,"
        sSql += " M.PRECIO_PAQUETE,M.PRECIO_UNITARIO,ISNULL(M.UTILIDAD,L.UTILIDAD) AS UTILIDAD  FROM COMPRA_MEDICAMENTOS CM"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = CM.ID_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " WHERE ID_COMPRA_MEDICAMENTOS  = @ID_COMPRA AND M.ID_MEDICAMENTO = @ID_MEDICAMENTO "


        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
            txtComposicion.Text = esNulo(tbl.Rows(0)("COMPOSICION"))
            ddlLaboratorio.SelectedValue = tbl.Rows(0)("ID_LABORATORIO")
            ddlFormaFarmaceutica.SelectedValue = tbl.Rows(0)("ID_FORMA_FARMACEUTICA")
            txtAccionTerapeutica.Text = esNulo(tbl.Rows(0)("ACCION_TERAPEUTICA"))
            txtCantidad.Text = esNulo(tbl.Rows(0)("CANTIDAD_PAQUETE"), 1)
            Dim precioPaquete As Decimal = esNulo(tbl.Rows(0)("PRECIO_PAQUETE_COMPRA"), 0)
            txtPrecioCompraPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", precioPaquete)
            lblCompraPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_COMPRA_ANT"), 0))
            Dim precioUnidad As Decimal = esNulo(tbl.Rows(0)("PRECIO_UNIDAD_COMPRA"), 0)
            txtPrecioCompraUnidad.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", precioUnidad)
            Dim utilidad = esNulo(tbl.Rows(0)("UTILIDAD"), 0)
            txtUtilidad.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", utilidad)
            txtPrecioPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", Decimal.Round(precioPaquete + (precioPaquete * (utilidad / 100)), 2))
            txtPrecioUnitario.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", Decimal.Round(precioUnidad + (precioUnidad * (utilidad / 100)), 2))
            lblPrecioUnitario.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_UNITARIO"), 0))
            lblPrecioPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_PAQUETE"), 0))
            txtRegistroSanitario.Text = esNulo(tbl.Rows(0)("COD_REG_SAN"), "")
            txtConcentracion.Text = esNulo(tbl.Rows(0)("CONCENTRACION"), "")
            txtCodigoBarra.Text = esNulo(tbl.Rows(0)("CODIGO_BARRA"), "")
            chkControlado.Checked = CBool(tbl.Rows(0)("CONTROLADO"))
            cargarPrecio(idMedicamento)
        End If
    End Sub
    


    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Dim tblMedicamento = New mcTabla()
            tblMedicamento.tabla = "MEDICAMENTO"
            tblMedicamento.campoID = "ID_MEDICAMENTO"
            tblMedicamento.modoID = "auto"
            tblMedicamento.agregarCampoValor("NOMBRE", txtMedicamento.Text)
            tblMedicamento.agregarCampoValor("COMPOSICION", txtComposicion.Text)
            tblMedicamento.agregarCampoValor("ID_TIPO_MEDICAMENTO", ddlFormaFarmaceutica.SelectedValue)
            tblMedicamento.agregarCampoValor("ACTIVO", 1)
            tblMedicamento.agregarCampoValor("PRECIO_UNITARIO", Decimal.Parse(txtPrecioUnitario.Text, New Globalization.CultureInfo("en-US")))
            tblMedicamento.agregarCampoValor("CONTROLADO", chkControlado.Checked)
            tblMedicamento.agregarCampoValor("ID_LABORATORIO", ddlLaboratorio.Text)
            tblMedicamento.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
            tblMedicamento.agregarCampoValor("ACCION", txtAccionTerapeutica.Text)
            tblMedicamento.agregarCampoValor("CANTIDAD_PAQUETE", CInt(txtCantidad.Text))
            tblMedicamento.agregarCampoValor("COD_REG_SAN", txtRegistroSanitario.Text)
            tblMedicamento.agregarCampoValor("PRECIO_PAQUETE", Decimal.Parse(txtPrecioPaquete.Text, New Globalization.CultureInfo("en-US")))
            tblMedicamento.agregarCampoValor("UTILIDAD", Decimal.Parse(txtUtilidad.Text, New Globalization.CultureInfo("en-US")))
            tblMedicamento.agregarCampoValor("CODIGO_BARRA", txtCodigoBarra.Text)            
            If Not Request.QueryString("ID") Is Nothing Then
                tblMedicamento.valorID = Request.QueryString("ID")
                tblMedicamento.modificar()
            Else
                tblMedicamento.insertar()
            End If
            gestionPrecios(tblMedicamento.valorID)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
        End If
    End Sub

    Protected Sub gestionPrecios(ByVal idMedicamento As Integer)
        Dim precio1 As Decimal = Decimal.Parse(txtPrecio1.Text, New Globalization.CultureInfo("en-US"))
        Dim precio2 As Decimal = Decimal.Parse(txtPrecio2.Text, New Globalization.CultureInfo("en-US"))
        Dim tbl = New mcTabla()
        tbl.tabla = "PRECIOS_EXTRAS"
        tbl.campoID = "ID_PRECIO"
        If CInt(hdPrecio1.Value) = -1 Then
            If precio1 > 0 Then
                tbl.modoID = "auto"
                tbl.agregarCampoValor("ID_PRECIO_MEDICAMENTO", 1)
                tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                tbl.agregarCampoValor("PRECIO", precio1)
                tbl.insertar()
            End If
        Else
            tbl.valorID = hdPrecio1.Value
            If precio1 > 0 Then
                tbl.agregarCampoValor("PRECIO", precio1)
                tbl.modificar()
            Else
                tbl.eliminar()
            End If
        End If
        tbl.reset()
        tbl.tabla = "PRECIOS_EXTRAS"
        tbl.campoID = "ID_PRECIO"
        If CInt(hdPrecio2.Value) = -1 Then
            If precio2 > 0 Then
                tbl.modoID = "auto"
                tbl.agregarCampoValor("ID_PRECIO_MEDICAMENTO", 2)
                tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                tbl.agregarCampoValor("PRECIO", precio2)
                tbl.insertar()
            End If
        Else
            tbl.valorID = hdPrecio2.Value
            If precio2 > 0 Then
                tbl.agregarCampoValor("PRECIO", precio2)
                tbl.modificar()
            Else
                tbl.eliminar()
            End If
        End If

    End Sub


    

    Private Sub gestionMedicamentos_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim sSql As String = "SELECT * FROM PRECIO_MEDICAMENTO ORDER BY ID_PRECIO_MEDICAMENTO"
            Dim tbl = New mcTabla().ejecutarConsulta(sSql)
            lblPrecio1.Text = tbl.Rows(0)("DETALLE")
            lblPrecio2.Text = tbl.Rows(1)("DETALLE")
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"), Request.QueryString("COMPRA"))
            End If

        End If
    End Sub

    Protected Sub cargarPrecioCompra(ByVal idCompra As Integer)

    End Sub
End Class