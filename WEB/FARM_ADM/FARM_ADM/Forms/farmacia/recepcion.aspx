﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="recepcion.aspx.vb" Inherits="FARM_ADM.recepcion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>        
    <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Recepion de Medicamentos:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr> 
        <tr>
            <td class="textoGris">
                Tipo de Compra
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTipoCompra" AutoPostBack="true" >
                    <asp:ListItem Text ="[Todas las Compras]" Value="-1"></asp:ListItem>
                    <asp:ListItem Text ="Compra Directa" Value="1"></asp:ListItem>
                    <asp:ListItem Text ="Orden de Compra" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="textoGris">
                Proveedor:
            </td>
            <td>
                <asp:DropDownList runat="server" id="ddlProveedor" DataSourceID="sqlLaboratorio" AutoPostBack="true"  
                            DataTextField="NOMBRE" DataValueField="ID_PROVEEDOR" ></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlLaboratorio" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_PROVEEDOR, NOMBRE FROM PROVEEDORES  
                            UNION ALL SELECT -1 , '[Seleccione un Proveedor]'
                            ORDER BY NOMBRE"></asp:SqlDataSource>
            </td>
        </tr>
        
                <tr>
            <td class="textoGris">
                Buscar Nro. Comprobante
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
                <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
            </td>
        </tr>                  
      <tr>
             <td colspan="4">
                 <asp:ListView ID="lvwGrilla" runat="server" DataSourceID="sqlProveedores">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>                                 
                                 <th>Proveedor </th>  
                                 <th style="width:100px;">Nro. Orden Compra </th>
                                 <th style="width:100px;">Nro. Comprobante </th>
                                 <th style="width:60px;">Comprobante </th>
                                 <th style="width:65px;">Fecha </th>                                                                              
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>    
                                <td class="textoGris">
                                 <asp:LinkButton  ID="m_btnEditar" runat="server" Text='<%# Eval("PROVEEDOR")%>' CommandName="recepcionar" CommandArgument='<%# Eval("ID_COMPRA")%>'></asp:LinkButton>
                             </td>  
                             <td class="textoGris">
                                 <asp:label  ID="Label1" runat="server"   Text='<%# Eval("NRO_ORDEN_COMPRA")%>'></asp:label>
                             </td>                       
                              <td class="textoGris">
                                 <asp:label  ID="lblComprobante" runat="server"   Text='<%# Eval("NRO_COMPROBANTE")%>'></asp:label>
                             </td>
                             <td class="alinearCentro">
                                 <asp:label ID="lblTipoComprobante" runat="server"   Text='<%# Eval("COMPROBANTE")%>'></asp:label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblFecha" runat="server" Text='<%# String.Format(New system.Globalization.CultureInfo("es-ES"), "{0:d}", Eval("FECHA"))%>'></asp:Label>
                             </td>                                                                                                         
                         </tr>
                     </ItemTemplate>
                     <EmptyDataTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                               <th>Proveedor </th>  
                                 <th style="width:100px;">Nro. Comprobante </th>
                                 <th style="width:60px;">Comprobante </th>
                                 <th style="width:65px;">Fecha </th>                                                                                               
                             </tr>
                             <tr>
                                 <td colspan="4" class="textoNegro alinearCentro">
                                     No Existen Datos.
                                 </td>
                             </tr>

                     </EmptyDataTemplate>
                 </asp:ListView>
                 <div align="right">
                     <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvwGrilla" PageSize="10">
                         <Fields>
                             <asp:NextPreviousPagerField ButtonType="Image" FirstPageImageUrl="~/App_Themes/estandar/imagenes/botones/First.png" FirstPageText="" LastPageImageUrl="~/App_Themes/estandar/imagenes/botones/Last.png" LastPageText="" NextPageImageUrl="~/App_Themes/estandar/imagenes/botones/Next.png" NextPageText="" PreviousPageImageUrl="~/App_Themes/estandar/imagenes/botones/Previous.png" PreviousPageText="" RenderDisabledButtonsAsLabels="True" ShowFirstPageButton="True" ShowLastPageButton="True" />
                         </Fields>
                     </asp:DataPager>
                     <asp:SqlDataSource ID="sqlProveedores" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT ID_COMPRA,NRO_COMPROBANTE, TC.DETALLE AS COMPROBANTE, CAST(FECHA AS DATE) AS FECHA, P.NOMBRE AS PROVEEDOR,
ISNULL(CAST(C.ID_ORDEN_COMPRA AS NVARCHAR(8)),'') AS NRO_ORDEN_COMPRA, CASE WHEN C.ID_ORDEN_COMPRA IS NULL THEN 1 ELSE 2 END AS TIPO
FROM COMPRA C 
INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE 
INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR 
WHERE C.ID_ESTADO = 2 
AND CASE WHEN C.ID_ORDEN_COMPRA IS NULL THEN 1 ELSE 2 END = CASE WHEN @TIPO = -1 THEN CASE WHEN C.ID_ORDEN_COMPRA IS NULL THEN 1 ELSE 2 END ELSE @TIPO END
AND P.ID_PROVEEDOR = CASE WHEN @PROVEEDOR = -1 THEN P.ID_PROVEEDOR ELSE @PROVEEDOR END 
AND (NRO_COMPROBANTE LIKE '%' + @BUSCAR + '%' OR ISNULL(CAST(C.ID_ORDEN_COMPRA AS NVARCHAR(8)),'') LIKE '%' + @BUSCAR + '%')">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="BUSCAR" PropertyName="Text" Type="String" />
                             <asp:ControlParameter ControlID="ddlTipoCompra" Name="TIPO" PropertyName="selectedValue" Type="String" />
                             <asp:ControlParameter ControlID="ddlProveedor" Name="PROVEEDOR" PropertyName="selectedValue" Type="String" />
                         </SelectParameters>                       
                     </asp:SqlDataSource>
                 </div>
             </td>
         </tr>     
        </table> 
            </ContentTemplate>
    </asp:UpdatePanel>    
    <script type="text/javascript">               
    </script>
</asp:Content>
