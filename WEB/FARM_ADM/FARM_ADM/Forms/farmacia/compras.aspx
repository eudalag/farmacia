﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="compras.aspx.vb" Inherits="FARM_ADM.compras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script src="../../scripts/mascara/js/Mascara.js"></script>
    <script src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtFechaDesde.ClientID%>").datepicker({});
            jQuery("#<%=txtFechaHasta.ClientID%>").datepicker({});
        });
        window.addEvent("domready", function () {
            var fecha = new Mascara("<%=txtFechaDesde.ClientID%>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var fecha = new Mascara("<%=txtFechaHasta.ClientID%>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>        
    <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Compras:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr> 
        <tr>
            <td class="textoGris" style="width:120px; ">Desde: </td>
            <td style="width:120px; ">
                <asp:TextBox runat="server" ID="txtFechaDesde" style="width:70px;"></asp:TextBox>
            </td>
            <td class="textoGris" style="width:50px; ">Hasta: </td>
             <td>
                <asp:TextBox runat="server" ID="txtFechaHasta" style="width:70px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">Tipo de Comprobante: </td>
             <td>
                <asp:DropDownList runat="server" id="ddlTipo" DataSourceID="sqlTipoComprobante" 
                            DataTextField="DETALLE" DataValueField="ID_TIPO_COMPROBANTE"></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlTipoComprobante" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_TIPO_COMPROBANTE, DETALLE FROM TIPO_COMPROBANTE 
                            UNION ALL SELECT -1 , '[Todos Los Comprobantes]'
                            ORDER BY DETALLE"></asp:SqlDataSource>
             </td>
            <td class="textoGris">Estado: </td>
            <td>
                <asp:DropDownList runat="server" id="ddlEstado" DataSourceID="sqlEstado"
                            DataTextField="DESCRIPCION" DataValueField="ID_ESTADO"></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlEstado" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_ESTADO, DESCRIPCION FROM ESTADO_COMPRA 
                            UNION ALL SELECT -1, '[Todas las Compra]'
                            ORDER BY DESCRIPCION"></asp:SqlDataSource>
            </td>
        </tr>
         <tr>
             <td class="textoGris" style="width:50px; ">Buscar: </td>
             <td colspan="3">
                 <asp:TextBox ID="txtBuscar" runat="server" MaxLength="100" style="width:250px; "></asp:TextBox>
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar" />
             </td>
         </tr>
         <tr>
             <td class="alinearDer" colspan="4">
                 <asp:Button ID="e_btnEliminar" runat="server" OnClientClick="return eliminar()" Text="Anular" />                 
                 <asp:Button ID="a_btnNuevo" runat="server" Text="Nueva Compra" />
             </td>
         </tr>
      <tr>
             <td colspan="4">
                 <asp:ListView ID="lvwGrilla" runat="server" DataSourceID="sqlProveedores">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:25px;">&nbsp; </th>                                 
                                 <th style="width:60px;">Comprobante </th>
                                 <th style="width:65px;">Fecha </th>
                                 <th>Proveedor </th>
                                 <th style="width:100px;">Nro. Comprobante </th>
                                 <th style="width:70px;">Total </th>
                                 <th style="width:130px;">Pago </th>
                                 <th style="width:50px;">Accion</th>   
                                 <th style="width:80px;">Estado </th>
                                 <th style="width:20px">&nbsp;</th>
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr class='<%# IIf(Eval("ID_ESTADO") = 3, "anulado", "normal")%>'>
                             <td>
                                 <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" Visible='<%# IIf(Eval("ID_ESTADO") = 3, "false", "true")%>'  />
                             </td>                           
                             <td class="alinearCentro">
                                 <asp:LinkButton ID="m_btnEditar" runat="server" CommandArgument='<%# Eval("ID_COMPRA")%>' CommandName="modificar" Text='<%# Eval("COMPROBANTE")%>' Enabled='<%# IIf(Eval("ID_ESTADO") = 3, "false", "true")%>' ></asp:LinkButton>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblFecha" runat="server" Text='<%# String.Format(New system.Globalization.CultureInfo("es-ES"), "{0:d}", Eval("FECHA"))%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblProveedor" runat="server" Text='<%# Eval("PROVEEDOR")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblNroComprobante" runat="server" Text='<%# Eval("NRO_COMPROBANTE")%>'></asp:Label>
                             </td>
                             <td class="textoNegro alinearDer">
                                 <asp:Label ID="lblTotal" runat="server" Text='<%# String.Format(New System.Globalization.CultureInfo("es-ES"), "{0:n}", Eval("TOTAL_COMPRA"))%>'></asp:Label>
                             </td>
                            <td class="textoGris">
                                 <asp:Label ID="Label1" runat="server" Text='<%# IIF(Eval("PAGO") = "Credito",String.concat(Eval("PAGO")," - ",Eval("VENCIMIENTO")),Eval("PAGO"))%>'></asp:Label>
                             </td>
                            <td>
                                <asp:Button runat="server" ID="btnAccion" Text="Pagar" OnClientClick='<%# String.Concat("return nuevoMovimiento(",Eval("ID_COMPRA"),");") %>' Visible='<%# IIf(Eval("PAGO") = "Efectivo" AND Eval("ID_ESTADO") = 5, "true", "false")%>' />
                            </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblEstado" runat="server" Text='<%# Eval("ESTADO_PAGO")%>'></asp:Label>
                             </td>
                             <td>                                 
                                 <asp:ImageButton runat="server" ID ="btnImprimir" ImageUrl="~/App_Themes/estandar/imagenes/printer.png" ToolTip ="Imprimir Compra" OnClientClick= '<%# String.Concat("return verReporte(", Eval("ID_COMPRA"), ");")%>' style="width:20px;" Visible='<%# IIf(Eval("ID_ESTADO") = 5, "true", "false")%>' />
                             </td>
                         </tr>
                     </ItemTemplate>
                     <EmptyDataTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:25px;">&nbsp; </th>
                                 <th style="width:60px;">Comprobante </th>
                                 <th style="width:65px;">Fecha </th>
                                 <th>Proveedor </th>
                                 <th style="width:100px;">Nro. Comprobante </th>
                                 <th style="width:100px;">Total </th>
                                 <th style="width:100px;">Estado </th>
                             </tr>
                             <tr>
                                 <td colspan="7" class="textoNegro alinearCentro">
                                     No Existen Datos.
                                 </td>
                             </tr>

                     </EmptyDataTemplate>
                 </asp:ListView>
                 <div align="right">
                     <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvwGrilla" PageSize="10">
                         <Fields>
                             <asp:NextPreviousPagerField ButtonType="Image" FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png" FirstPageText="" LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png" LastPageText="" NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png" NextPageText="" PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png" PreviousPageText="" RenderDisabledButtonsAsLabels="True" ShowFirstPageButton="True" ShowLastPageButton="True" />
                         </Fields>
                     </asp:DataPager>
                     <asp:SqlDataSource ID="sqlProveedores" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT C.ID_COMPRA,TC.DETALLE AS COMPROBANTE, FECHA, P.NOMBRE AS PROVEEDOR, NRO_COMPROBANTE, 
                            TOTAL AS TOTAL_COMPRA, EC.DESCRIPCION AS ESTADO,EC.ID_ESTADO  ,
							CASE WHEN CC.ID_COMPRA_CREDITO IS NULL THEN 'Efectivo' ELSE 'Credito' END AS PAGO,
							ISNULL(CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103),'') AS VENCIMIENTO,
							CASE WHEN ISNULL(CC.SALDO,0) = 0 AND EC.ID_ESTADO = 6  THEN 'Cancelado' WHEN EC.ID_ESTADO  = 3 THEN EC.DESCRIPCION  Else '' END AS ESTADO_PAGO
							FROM COMPRA C
                            INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE 
                            INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR 
                            INNER JOIN ESTADO_COMPRA EC ON EC.ID_ESTADO = C.ID_ESTADO
							LEFT JOIN COMPRA_CREDITO CC ON CC.ID_COMPRA = C.ID_COMPRA  
                            WHERE TC.ID_TIPO_COMPROBANTE = CASE WHEN @TIPO = -1 THEN TC.ID_TIPO_COMPROBANTE ELSE @TIPO END
                            AND (FECHA >= @DESDE AND FECHA <= @HASTA)
                            AND (P.NOMBRE LIKE '%' + @BUSCAR + '%' OR NRO_COMPROBANTE LIKE '%' + @BUSCAR + '%')
                            AND C.ID_ESTADO = CASE WHEN @ESTADO = -1 THEN C.ID_ESTADO ELSE @ESTADO END
							ORDER BY C.FECHA,VENCIMIENTO ">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="BUSCAR" PropertyName="Text" Type="String" />
                             <asp:ControlParameter ControlID="txtFechaDesde" DbType="Date" Name="DESDE" PropertyName="Text" />
                             <asp:ControlParameter ControlID="txtFechaHasta" DbType="Date" Name="HASTA" PropertyName="Text" />
                             <asp:ControlParameter ControlID="ddlTipo" Name="TIPO" PropertyName="SelectedValue" />
                             <asp:ControlParameter ControlID="ddlEstado" Name="ESTADO" PropertyName="SelectedValue" />
                         </SelectParameters>                         
                     </asp:SqlDataSource>
                 </div>
             </td>
         </tr>     
        </table> 
            </ContentTemplate>
    </asp:UpdatePanel>    
    <script type="text/javascript">
        function cancelarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");

        }
        function aceptarModal() {
            jQuery.noConflict
            jQuery("#jqueryuidialog").dialog("close");
            jQuery("#<%=btnBuscar.ClientID%>").click()
            return false;
        }
        function derivarCompra(compra) {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "derivar.aspx?COMPRA=" + compra;
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "DERIVAR COMPRA", width: 300, height: 130 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblNroComprobante");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar el Comprobante Nro.:" + lblDescripcion.innerHTML));
                }
            });
        }
        function verReporte(compra) {
            var url;
            url = "../reporte/visualizadorReporte.aspx?TIPO=RECEPCION&ID=" + compra;
            window.open(url, "_blank");
            return false;
        }
        function nuevoMovimiento(id) {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");            
            var url = "modalPago.aspx?ID=" + id;

            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "PAGO A PROVEEDOR", width: 550, height: 200 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
    </script>
</asp:Content>
