﻿Imports System.Transactions
Public Class modalPago
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @COMPRA AS INTEGER;"
        sSql += " SET @COMPRA = " + Request.QueryString("ID") + ";"
        sSql += " SELECT P.NOMBRE,C.NRO_COMPROBANTE,C.FECHA,C.TOTAL  FROM COMPRA C"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR "
        sSql += " WHERE C.ID_COMPRA = @COMPRA"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtBolivianos.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:###0.00}", tbl.Rows(0)("TOTAL"))
            txtDetalle.Text = "Compra Efectivo a " + tbl.Rows(0)("NOMBRE") + " comprobante Nro. " + tbl.Rows(0)("NRO_COMPROBANTE") + " de fecha " + tbl.Rows(0)("FECHA")
        End If
    End Sub

    Private Sub modalPago_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        cargarDatos()
        txtFecha.Text = Now.ToString("dd/MM/yyyy")
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Using scope As New TransactionScope
                Try
                    Dim importe As Decimal = Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US"))
                    Dim tbl = New mcTabla()
                    tbl.tabla = "MOVIMIENTO_BANCO"
                    tbl.modoID = "auto"
                    tbl.campoID = "ID_MOVIMIENTO"
                    tbl.agregarCampoValor("ID_BANCO", ddlBanco.SelectedValue)
                    tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 2)
                    tbl.agregarCampoValor("IMPORTE", importe * -1)
                    tbl.agregarCampoValor("GLOSA", txtDetalle.Text)
                    tbl.agregarCampoValor("FH_MOVIMIENTO", txtFecha.Text)

                    tbl.agregarCampoValor("ID_PLAN_CUENTA", 12)
                    tbl.insertar()
                    Dim cuenta = New plan_cuentas()
                    cuenta.insertarMovimiento(importe, 2, "MOVIMIENTO_BANCO", tbl.valorID, "serverDateTime")
                    tbl.reset()

                    tbl.tabla = "COMPRA"
                    tbl.campoID = "ID_COMPRA"
                    tbl.valorID = Request.QueryString("ID")
                    tbl.agregarCampoValor("ID_ESTADO", 6)
                    tbl.modificar()
                    scope.Complete()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
                Catch ex As Exception

                End Try
            End Using
        End If
    End Sub
End Class