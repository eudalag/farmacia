﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalActualizarMedicamento.aspx.vb" Inherits="FARM_ADM.modalActualizarMedicamento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
  <script type="text/javascript">
      jQuery.noConflict();
      
      window.addEvent("domready", function () {
          var txtPrecioUnitario = new Mascara("txtPrecioUnitario", { 'fijarMascara': '##/##/####', 'esNumero': true });
          var txtPrecioPaquete = new Mascara("txtPrecioPaquete", { 'fijarMascara': '##/##/####', 'esNumero': true });
          var txtPrecio1 = new Mascara("txtPrecio1", { 'fijarMascara': '##/##/####', 'esNumero': true });
          var txtPrecio2 = new Mascara("txtPrecio2", { 'fijarMascara': '##/##/####', 'esNumero': true });
          var txtUtilidad = new Mascara("txtUtilidad", { 'fijarMascara': '##/##/####', 'esNumero': true });
      });
    </script>
</head>
<body>
  <form id="form1" runat="server">
        <div>
            <fieldset>
            <legend class="textoNegroNegrita">
                Medicamento                
            </legend>
            <table style="width:100%;">
                <tr>
         <td class="textoGris" style="width:190px; ">
             Medicamento:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtMedicamento" MaxLength="150" style="width:450px; "></asp:TextBox> 
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="(*)" ControlToValidate="txtMedicamento" CssClass="TextoRojo"></asp:RequiredFieldValidator>            
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:190px; ">
             Controlado:
         </td>
         <td>
            <asp:CheckBox runat="server" ID="chkControlado" />
         </td>
     </tr>
        <tr>
            <td class="textoGris" style="width:150px; ">
             Concentracion:
            </td>
            <td>
             <asp:TextBox runat="server" ID="txtConcentracion" MaxLength="150" style="width:300px; "></asp:TextBox>             
         </td>
        </tr>
        <tr>
         <td class="textoGris">
             Nombre Generico:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtComposicion" MaxLength="300" style="width:600px; "></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Accion Terapeutica:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtAccionTerapeutica" style="width:500px;" TextMode="MultiLine" Rows="3"></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Forma Farmaceutica (Presentacion):
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlFormaFarmaceutica" DataSourceID="sqlFormaFarmaceutica" DataTextField="DESCRIPCION" DataValueField="ID_FORMA_FARMACEUTICA"></asp:DropDownList>            
             <asp:SqlDataSource runat="server" ID="sqlFormaFarmaceutica" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString  %>" 
                 SelectCommand="SELECT [ID_FORMA_FARMACEUTICA], [DESCRIPCION] FROM [FORMA_FARMACEUTICA] 
                                UNION ALL 
                                SELECT -1, '[Seleccione Una Forma Farmaceutica]'
                                ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
         </td>
     </tr>
         <tr>
         <td class="textoGris">
             Laboratorio:
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlLaboratorio" DataSourceID="sqlLaboratorio" DataTextField="DESCRIPCION" DataValueField="ID_LABORATORIO"></asp:DropDownList>            
             <asp:SqlDataSource runat="server" ID="sqlLaboratorio" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString  %>" 
                 SelectCommand="SELECT [ID_LABORATORIO], [DESCRIPCION] FROM [LABORATORIO]
                                UNION ALL 
                                SELECT -1, '[Seleccione Un Laboratorio]'
                                ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
         </td>     
         <tr>
         <td class="textoGris">
             Cod. Registro Sanitario:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtRegistroSanitario" MaxLength="50" style="width:100px; "></asp:TextBox>             
         </td>
     </tr>
         <tr>
         <td class="textoGris">
             Codigo Barra:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtCodigoBarra" MaxLength="50" style="width:150px; "></asp:TextBox>             
         </td>
     </tr>
         <tr>
                    <td class="textoGris" style="width:190px;">
                        Cantidad por paquete (Unitario):
                    </td>
                    <td>
                        <asp:TextBox ID="txtCantidad" runat="server" Text="0.00" MaxLength="18" style="width:50px;"></asp:TextBox>            
                    </td>                     
                </tr>
    <tr>
        <td colspan="2">
<fieldset>
    <legend class="textoNegroNegrita">
                <b>Informacion Compra</b>
    </legend>   
    <table>

         <tr>
                    <td class="textoGris" style="width:125px;">
                        Precio Paquete:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioCompraPaquete" runat="server" Text="0.00" MaxLength="18" style="width:50px;" Enabled="false"></asp:TextBox>
                        <br />
                        <span class="textoAyuda">(Anterior:<asp:Label runat="Server" ID="lblCompraPaquete" CssClass="textoAyuda" Text="0.00"></asp:Label>)</span>
                    </td>                     
                </tr>
                <tr>
                    <td class="textoGris" style="width:125px;">
                        Precio Unitario:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioCompraUnidad" runat="server" Text="0.00" MaxLength="18" style="width:50px;" Enabled="false"></asp:TextBox>
                    </td>                     
                </tr>
    </table>
</fieldset>
<fieldset>
            <legend class="textoNegroNegrita">
                <b>Precios Venta</b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Utilidad:
                    </td>
                    <td class="textoGris">
                        <asp:TextBox ID="txtUtilidad" Text="0.00" style="width:70px;" runat="server" onblur="calcularPrecio(1);"></asp:TextBox> %
                        <br />
                        <span class="textoAyuda">(Anterior:<asp:Label runat="Server" ID="lblUtilidad" CssClass="textoAyuda" Text="0.00"></asp:Label>)</span>
                    </td>                     
                </tr>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Precio por Unidad:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioUnitario" Text="0.00" style="width:70px;" runat="server" onblur="calcularPrecio(2);"></asp:TextBox>
                        <br />
                        <span class="textoAyuda">(Anterior:<asp:Label runat="Server" ID="lblPrecioUnitario" CssClass="textoAyuda" Text="0.00" ></asp:Label>)</span>
                    </td>                     
                </tr>
                  <tr>
                    <td class="textoGris">
                        Precio por Mayor / Paquete:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPrecioPaquete" Text="0.00" style="width:70px;" onblur="calcularPrecio(2);"></asp:TextBox>  
                        <br />
                        <span class="textoAyuda">(Anterior:<asp:Label runat="Server" ID="lblPrecioPaquete" CssClass="textoAyuda" Text="0.00" ></asp:Label>)</span>                      
                    </td>
                </tr>                  
                  </table>             
        </fieldset>             
 <fieldset>
            <legend class="textoNegroNegrita">
                <b>Otros Precios </b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        <asp:Label runat="server" ID="lblPrecio1" Text="Precio1:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPrecio1" style="width:120px;" ></asp:TextBox>  
                        <asp:HiddenField runat="server" ID="hdPrecio1" Value="-1" />                                              
                    </td>                     
                </tr>
                   <tr>
                    <td class="textoGris" style="width:190px;">
                        <asp:Label runat="server" ID="lblPrecio2" Text="Precio2:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPrecio2" style="width:120px;" ></asp:TextBox> 
                        <asp:HiddenField runat="server" ID="hdPrecio2" Value="-1" />                                                                                             
                    </td>                     
                </tr>
              </table>                
        </fieldset>
        </td>
    </tr>
            </table>               
                
            <div class="alinearDer" >
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="return cerrarInformacion();" CausesValidation="false" />
            </div>
            </fieldset>
        </div>
  
    <script type="text/javascript" >

//        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(inicializar);
//        function inicializar() {
//            jQuery(".cantidad").each(function () {
//                jQuery(this).numeric({});
//            });
//            var fecha
//            jQuery(".fecha").each(function () {
//                jQuery(this).datepicker({});
//            });
//        }


        function formatNumber(num) {
            num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
            splitRight = splitRight + '00';
            splitRight = splitRight.substr(0, 3);
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
            }
            return splitLeft + splitRight;
        }
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
        function calcularPrecio(tipo) { 
            jQuery.noConflict();
            var utilidad = parseFloat(jQuery("#txtUtilidad").val());
            var precPaquete = parseFloat(jQuery("#txtPrecioPaquete").val());
            var precUnitario = parseFloat(jQuery("#txtPrecioUnitario").val());
            var precPaqueteCompra = parseFloat(jQuery("#txtPrecioCompraPaquete").val());
            var preUnitarioCompra = parseFloat(jQuery("#txtPrecioCompraUnidad").val());            
            switch (parseInt(tipo)) {
                case 1:
                    var calculo = precPaqueteCompra + (precPaqueteCompra * (utilidad / 100));
                    jQuery("#txtPrecioPaquete").val(formatNumber(calculo));
                    calculo = preUnitarioCompra + (preUnitarioCompra * (utilidad / 100));
                    jQuery("#txtPrecioUnitario").val(formatNumber(calculo));
                    break;
                case 2:
                    var calculo = ((precUnitario / preUnitarioCompra) - 1) * 100;
                    jQuery("#txtUtilidad").val(formatNumber(calculo));
                    break;
                case 3:

                    break;
            }
        }
    </script>
        
    </form>
</body>
</html>
