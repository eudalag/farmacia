﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="buscarProveedores.aspx.vb" Inherits="FARM_ADM.buscarProveedores" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server" defaultbutton="btnBuscar">  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>                        
    <div>
        <asp:ListView runat="server" ID="lvwProveedores" DataSourceID="sqlProveedores">
            <LayoutTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                        <th>
                            Proveedor
                        </th>
                        <th style="width:120px;">
                            Nit
                        </th>                       
                    </tr>
                    <tr runat="server" id="itemPlaceHolder"></tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" id="btnProveedor" Text='<%# Eval("NOMBRE")%>' CommandArgument='<%# Eval("NIT")%>' CommandName='seleccionar' ></asp:LinkButton>                        
                    </td>
                    <td class="textoGris">
                        <%# Eval("NIT")%>
                    </td>                                       
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <table style="width:100%" class="generica">
                     <tr>
                        <th>
                            Proveedor
                        </th>
                        <th style="width:120px;">
                            Nit
                        </th>                       
                    </tr>
                    <tr>
                        <td colspan="2" class="textoGris">
                            No Existen Datos.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>        
        <div align="right" style="padding-top:5px;"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwProveedores" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager>  
            <asp:SqlDataSource ID="sqlProveedores" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT ID_PROVEEDOR,NOMBRE,NIT FROM PROVEEDORES 
                        WHERE  NIT <> '0' AND (NOMBRE LIKE '%' + @BUSCAR + '%' OR  NIT LIKE '%' + @BUSCAR + '%')
                        ORDER BY NOMBRE">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="BUSCAR" PropertyName="Text" Type="String" />                             
                         </SelectParameters>                         
                     </asp:SqlDataSource>                   
                </div>
        <div class="alinearCentro">
            <div>
            <span class="textoGris">
                Buscar:
            </span>
            <span>
                <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
                <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
            </span>
            </div>
            <div class="textoAyuda">
                <u>Parametros de Busqueda:</u> PROVEEDOR  o  NUMERO DE NIT <br />
            Para Filtrar los proveedores en lista debera ingresar el nombre del proveedor o su Numero de NIT registrado en sistema.
            </div>
        </div>        
    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script src="../../JavaScript/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
    </script>
</body>
</html>
