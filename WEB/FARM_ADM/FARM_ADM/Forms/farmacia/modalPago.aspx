﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalPago.aspx.vb" Inherits="FARM_ADM.modalPago" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#txtFecha").datepicker({});
        });
        window.addEvent("domready", function () {
            var fecha = new Mascara("txtFecha", { 'fijarMascara': '##/##/####', 'esNumero': false });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:100%;">
                <tr>
                     <td class="textoGris" style="width:120px; ">
                         Tipo de Movimiento :
                     </td>
                     <td class="textoGris" >
                         Egreso
                     </td>
                </tr>
<tr>
                     <td class="textoGris" style="width:120px; ">
                         Cuenta :
                     </td>
                     <td class="textoGris" >
                         PAGO A PROVEEDORES
                     </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Detalle
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDetalle" style="width:250px;" TextMode="MultiLine" Rows="2" class="textoGris" ReadOnly="true"></asp:TextBox> 
                    </td>
                </tr>
                <tr>
                     <td class="textoGris">
                         Importe (Bs.):
                         
                     </td>
                     <td>
                         <asp:TextBox ID="txtBolivianos" runat="server" class="textoGris"  style="width:100px;" Text="0.00" ReadOnly="true" ></asp:TextBox >                         
                     </td>
                 </tr>   
                <tr>
                    <td class="textoGris">
                         Fecha de Pago:
                         
                     </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtFecha" style="width:80px;"></asp:TextBox>
                    </td>
                </tr>
<tr>
        <td style="width:100px" class="textoGris">
            Banco / Caja:
        </td>
                    <td colspan="3">
                        <asp:DropDownList runat="server" ID="ddlBanco" DataSourceID="sqlBanco" DataTextField="BANCO" DataValueField="ID_BANCO" AutoPostBack="true"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlBanco" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" SelectCommand="SELECT TB.DESC_TIPO_BANCO + CASE WHEN NRO_CUENTA IS NULL THEN '' ELSE ' - ' + NRO_CUENTA END + ' - ' + ENTIDAD_FINANCIERA AS BANCO, B.ID_BANCO   FROM BANCO B 
                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO 
                            WHERE ID_USUARIO = @USUARIO 
                            ORDER BY ID_BANCO">
                            <SelectParameters>
                                <asp:SessionParameter Name="USUARIO" SessionField="IDUSUARIO" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
               
    </tr>
                <tr>
                    <td colspan="2" class="alinearDer">
                        <br />
                        <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                        <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="cerrarInformacion();" CausesValidation="false" />
                    </td>                   
                </tr>
            </table>
    </div>
    </form>
 <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
</script>
</body>
</html>
