﻿Imports System.Transactions
Public Class gestionRecepcion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_COMPRA AS INTEGER;"
        sSql += " SET @ID_COMPRA = " + Request.QueryString("ID") + ";"
        sSql += " SELECT P.NIT, P.NOMBRE, TC.DETALLE, TM.MONEDA, CONVERT(NVARCHAR(10),C.FECHA,103) AS FECHA,C.NRO_COMPROBANTE FROM COMPRA_MEDICAMENTOS CM"
        sSql += " INNER JOIN COMPRA C ON C.ID_COMPRA = CM.ID_COMPRA"
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE "
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR "
        sSql += " INNER JOIN TIPO_MONEDA TM ON TM.ID_TIPO_MONEDA = C.ID_MONEDA "
        sSql += " WHERE C.ID_COMPRA = @ID_COMPRA "
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            lblNit.Text = tbl.Rows(0)("NIT")
            lblRazonSocial.Text = tbl.Rows(0)("NOMBRE")
            lblMoneda.Text = tbl.Rows(0)("MONEDA")
            lblComprobante.Text = tbl.Rows(0)("DETALLE")
            lblFecha.Text = tbl.Rows(0)("FECHA")
            lblNroComprobante.Text = tbl.Rows(0)("NRO_COMPROBANTE")
        End If
    End Sub

    Private Sub gestionRecepcion_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            cargarDatos()
        End If
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        lvwGrilla.DataBind()
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Using scope As New TransactionScope()
            Try
                Dim tbl = New mcTabla()
                tbl.tabla = "COMPRA"
                tbl.campoID = "ID_COMPRA"
                tbl.valorID = Request.QueryString("ID")
                tbl.agregarCampoValor("ID_ESTADO", 5)
                tbl.modificar()
                scope.Complete()
                Response.Redirect("recepcion.aspx")
            Catch ex As Exception
                lblError.Text = ex.Message.ToString
            End Try
        End Using

    End Sub

    Protected Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Response.Redirect("recepcion.aspx")
    End Sub
End Class