﻿Public Class historico_clinico
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        Dim sSql As String = ""
        sSql += " DECLARE @PACIENTE AS INTEGER;"
        sSql += " SET @PACIENTE = " + hdIdPaciente.Value + ";"
        sSql += " SELECT ID_PACIENTE,NOMBRE_APELLIDOS,CI,F_NACIMIENTO, CASE WHEN dateadd(year, datediff(year, F_NACIMIENTO, GETDATE()), F_NACIMIENTO) > GETDATE()"
        sSql += " THEN DATEDIFF(year, F_NACIMIENTO, GETDATE()) - 1 ELSE DATEDIFF(year, F_NACIMIENTO, GETDATE())  END  AS EDAD FROM PACIENTE"
        sSql += " WHERE ID_PACIENTE = @PACIENTE"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtCarnetIdentidad.Text = tbl.Rows(0)("CI")
            txtFechaNacimiento.Text = tbl.Rows(0)("F_NACIMIENTO")
            txtEdad.Text = tbl.Rows(0)("EDAD")
            txtCliente.Text = tbl.Rows(0)("NOMBRE_APELLIDOS")
        End If
    End Sub

    Private Sub historico_clinico_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            txtFecha.Text = Now.ToString("dd/MM/yyyy")
            crearTabla()
            cargarGrilla()


        End If
    End Sub

    Function nuevaColumna(ByVal nombre As String, ByVal tipo As String) As DataColumn
        Dim columna As DataColumn
        columna = New DataColumn()
        columna.DataType = System.Type.GetType(tipo)
        columna.ColumnName = nombre
        Return columna
    End Function
    Protected Sub crearTabla()
        Dim tbl = New DataTable
        tbl.Columns.Add(nuevaColumna("ID_MEDICAMENTO", "System.Int32"))
        tbl.Columns.Add(nuevaColumna("NRO_FILA", "System.Int32"))
        tbl.Columns.Add(nuevaColumna("MEDICAMENTO", "System.String"))        
        tbl.Columns.Add(nuevaColumna("CANTIDAD", "System.Int32"))
        tbl.Columns.Add(nuevaColumna("PRECIO_UNITARIO", "System.Decimal"))        
        tbl.Columns.Add(nuevaColumna("SUBTOTAL", "System.Decimal"))
        tbl.Columns.Add(nuevaColumna("DOSIFICACION", "System.String"))
        tbl.Columns.Add(nuevaColumna("NUEVO", "System.Int32"))
        tbl.Columns.Add(nuevaColumna("ACCION", "System.Int32"))
        tbl.AcceptChanges()
        ViewState("tblItems") = tbl
    End Sub
    Protected Sub nuevoItem(ByVal idMedicamento As Integer, ByVal medicamento As String, ByVal cantidad As Integer, ByVal precio As Decimal, ByVal subtotal As Decimal, ByVal dosificacion As String, ByVal nuevo As Integer, ByVal accion As Integer)
        Dim tblMedicamento As DataTable = ViewState("tblItems")
        Dim foundRows() As DataRow
        foundRows = tblMedicamento.Select("NRO_FILA = MAX(NRO_FILA)")
        Dim dr As DataRow
        dr = tblMedicamento.NewRow
        If foundRows.Length = 0 Then
            dr("NRO_FILA") = 1
        Else
            dr("NRO_FILA") = CInt(tblMedicamento.Select("NRO_FILA = MAX(NRO_FILA)")(0)("NRO_FILA")) + 1
        End If
        dr("ID_MEDICAMENTO") = idMedicamento

        dr("MEDICAMENTO") = medicamento
        dr("CANTIDAD") = cantidad
        dr("PRECIO_UNITARIO") = precio
        dr("SUBTOTAL") = subtotal
        dr("DOSIFICACION") = dosificacion
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblMedicamento.Rows.Add(dr)
        tblMedicamento.AcceptChanges()
        ViewState("tblItems") = tblMedicamento
    End Sub
    Public Sub cargarGrilla()
        Dim tblItems As DataTable = ViewState("tblItems")
        Dim tbl = (From c In tblItems Where c!ACCION <> 3)
        If tbl.AsDataView.Count > 0 Then
            lvwGrilla.DataSource = tbl.CopyToDataTable
        Else
            lvwGrilla.DataSource = tblItems.Clone()
        End If
        lvwGrilla.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "total", "calcularTotal();", True)
    End Sub

    Protected Sub actualizarTabla()
        Dim tblMedicamento As DataTable = ViewState("tblItems")
        For Each fila In lvwGrilla.Items
            For Each tabla In tblMedicamento.Rows
                If (tabla("NRO_FILA") = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value) Then
                    Dim nroFila As Integer = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value
                    Dim idMedicamento As String = CType(fila.FindControl("hfIdMedicamento"), HiddenField).Value
                    Dim medicamento As String = CType(fila.FindControl("lblMedicamento"), Label).Text

                    Dim cantidad As Integer = CInt(CType(fila.FindControl("txtCantidad"), TextBox).Text)
                    Dim precio As Decimal = Decimal.Parse(CType(fila.FindControl("hdPrecio"), HiddenField).Value, New Globalization.CultureInfo("en-US"))
                    Dim subTotal As Decimal = Decimal.Parse(CType(fila.FindControl("hdSubtotal"), HiddenField).Value, New Globalization.CultureInfo("en-US"))
                    Dim dosificacion As String = CType(fila.FindControl("txtDosificacion"), TextBox).Text


                    If tabla("ID_MEDICAMENTO") <> idMedicamento Or tabla("CANTIDAD") <> cantidad Or tabla("DOSIFICACION") <> dosificacion Then
                        tabla("ACCION") = 2
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("MEDICAMENTO") = medicamento
                        tabla("CANTIDAD") = cantidad
                        tabla("PRECIO_UNITARIO") = precio
                        tabla("SUBTOTAL") = subTotal
                        tabla("DOSIFICACION") = dosificacion
                    End If
                End If
            Next
        Next

        If tblMedicamento.Rows.Count > 0 Then
            tblMedicamento.AcceptChanges()
            ViewState("tblItems") = tblMedicamento
        End If
    End Sub

    Protected Sub btnCargarMedicamento_Click(sender As Object, e As EventArgs) Handles btnCargarMedicamento.Click
        actualizarTabla()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
        sSql += " SET @MEDICAMENTO = " + hdIdMedicamento.Value + ";"
        sSql += " SELECT MEDICAMENTO + ' (' + LABORATORIO + ') - ' + FORMA_FARMACEUTICA AS MEDICAMENTO, ID_MEDICAMENTO,PRECIO_UNITARIO FROM VW_MEDICAMENTO"
        sSql += " WHERE ID_MEDICAMENTO = @MEDICAMENTO "
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            nuevoItem(tbl.Rows(0)("ID_MEDICAMENTO"), tbl.Rows(0)("MEDICAMENTO"), 1, tbl.Rows(0)("PRECIO_UNITARIO"), tbl.Rows(0)("PRECIO_UNITARIO"), "", 1, 1)
        End If
        cargarGrilla()
    End Sub


    Protected Sub procesarHistorico()
        Dim tbl = New mcTabla()
        tbl.tabla = "HISTORICO_CLINICO"
        tbl.campoID = "ID_HISTORICO"
        tbl.modoID = "auto"

        tbl.agregarCampoValor("ID_PACIENTE", hdIdPaciente.Value)
        tbl.agregarCampoValor("ANTECEDENTES", txtAntecedentes.Text)
        tbl.agregarCampoValor("CONCLUSIONES", txtDiagnostico.Text)
        tbl.agregarCampoValor("PESO", IIf(Trim(txtPeso.Text) = "", "null", txtPeso.Text))
        tbl.agregarCampoValor("TEMPERATURA", IIf(Trim(txtTemperatura.Text) = "", "null", txtTemperatura.Text))
        tbl.agregarCampoValor("FH_HISTORICO", "serverDateTime")
        tbl.insertar()
        procesarReceta(tbl.valorID)
    End Sub
    Protected Sub procesarReceta(ByVal idHistorico As Integer)
        Dim tbl = New mcTabla()
        tbl.tabla = "RECETA"
        tbl.campoID = "ID_RECETA"
        tbl.modoID = "auto"

        tbl.agregarCampoValor("ID_PACIENTE", hdIdPaciente.Value)
        tbl.agregarCampoValor("ID_MEDICO", Session("IDUSUARIO"))
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("VIGENTE", 1)
        tbl.agregarCampoValor("ID_HISTORICO", idHistorico)
        tbl.insertar()
        Dim idReceta = tbl.valorID
        tbl.reset()
        Dim tblMedicamento As DataTable = ViewState("tblItems")
        
        For Each item In tblMedicamento.Rows
            tbl.tabla = "RECETA_DETALLE"
            tbl.campoID = "ID_RECETA_DETALLE"
            tbl.modoID = "auto"
            tbl.agregarCampoValor("ID_RECETA", idReceta)
            tbl.agregarCampoValor("ID_MEDICAMENTO", item("ID_MEDICAMENTO"))
            tbl.agregarCampoValor("CANTIDAD", item("CANTIDAD"))
            tbl.agregarCampoValor("INDICACIONES", item("DOSIFICACION"))
            tbl.insertar()
            tbl.reset()
        Next
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            actualizarTabla()
            procesarHistorico()
            Response.Redirect("consultas.aspx")
        End If
    End Sub

    Protected Sub txtCarnetIdentidad_TextChanged(sender As Object, e As EventArgs) Handles txtCarnetIdentidad.TextChanged
        Dim sSql As String = ""
        sSql += " DECLARE @CI AS NVARCHAR(20);"
        sSql += " SET @CI = " + txtCarnetIdentidad.Text + ";"
        sSql += " SELECT ID_PACIENTE,NOMBRE_APELLIDOS,CI,F_NACIMIENTO, CASE WHEN dateadd(year, datediff(year, F_NACIMIENTO, GETDATE()), F_NACIMIENTO) > GETDATE()"
        sSql += " THEN DATEDIFF(year, F_NACIMIENTO, GETDATE()) - 1 ELSE DATEDIFF(year, F_NACIMIENTO, GETDATE())  END  AS EDAD FROM PACIENTE"
        sSql += " WHERE CI = @CI"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            hdIdPaciente.Value = tbl.Rows(0)("ID_PACIENTE")
            txtFechaNacimiento.Text = tbl.Rows(0)("F_NACIMIENTO")
            txtEdad.Text = tbl.Rows(0)("EDAD")
            txtCliente.Text = tbl.Rows(0)("NOMBRE_APELLIDOS")

        End If
    End Sub

    Protected Sub lvwGrilla_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvwGrilla.ItemCommand
        If e.CommandName = "eliminar" Then
            actualizarTabla()
            Dim tblItems As DataTable = ViewState("tblItems")
            For Each row As DataRow In tblItems.Rows
                If CStr(row("NRO_FILA")) = e.CommandArgument Then
                    row("ACCION") = 3
                    Exit For
                End If
            Next
            tblItems.AcceptChanges()
            cargarGrilla()
        End If
    End Sub
End Class