﻿Imports System.IO
Imports System.Transactions

Public Class gestionMedico
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Sub cargarDatos(ByVal id As Integer)
        Dim sSql = ""
        sSql = "SELECT * FROM MEDICO WHERE ID_MEDICO = " + id.ToString        
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        txtNombre.Text = tbl.Rows(0)("NOMBRE")
        txtEspecialidad.Text = tbl.Rows(0)("ESPECIALIDAD")
        txtMatricula.Text = tbl.Rows(0)("MATRICULA")
        txtCelular.Text = tbl.Rows(0)("TELEFONO")
    End Sub
    Protected Function verificaTipoArchivo(archivo) As Boolean
        Dim tipo As String
        tipo = Path.GetExtension(archivo).ToString.ToLower
        If tipo <> ".png" And tipo <> ".jpg" And tipo <> ".ico" And tipo <> ".bmp" And tipo <> ".gif" And tipo <> ".jpeg" Then
            Return False
        Else : Return True
        End If
    End Function
    Protected Sub mensajeAlerta(ByVal sMensaje As String)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "alerta", "alert('" & sMensaje & "');", True)
    End Sub
    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Using scope As New TransactionScope()
            Try
                Dim hpf = fileUpload.PostedFile
                Dim nombreImagen As String
                Dim tbl = New mcTabla()
                tbl.tabla = "MEDICO"
                tbl.campoID = "ID_MEDICO"
                tbl.modoID = "sql"
                tbl.agregarCampoValor("NOMBRE", txtNombre.Text.ToString.ToUpper)
                tbl.agregarCampoValor("ESPECIALIDAD", txtEspecialidad.Text.ToUpper)
                tbl.agregarCampoValor("MATRICULA", txtMatricula.Text.ToUpper)
                tbl.agregarCampoValor("TELEFONO", txtCelular.Text)
                If Not hpf Is Nothing Then
                    If verificaTipoArchivo(hpf.FileName) Then
                        Dim Imagen As System.Drawing.Image
                        Imagen = System.Drawing.Image.FromStream(hpf.InputStream)
                        nombreImagen = hpf.FileName
                        Dim extension = nombreImagen.Substring(nombreImagen.IndexOf(".")).ToLower

                        Dim imgGrande As MemoryStream = New MemoryStream()
                        Select Case extension
                            Case ".png"
                                Imagen.Save(imgGrande, System.Drawing.Imaging.ImageFormat.Png)
                            Case ".gif"
                                Imagen.Save(imgGrande, System.Drawing.Imaging.ImageFormat.Gif)
                            Case Else
                                Imagen.Save(imgGrande, System.Drawing.Imaging.ImageFormat.Jpeg)
                        End Select

                        tbl.agregarCampoValor("FIRMA", imgGrande.ToArray(), SqlDbType.Image)

                    Else
                        mensajeAlerta("Los formatos de imagen admitidos son .png .jpg .ico .bmp .gif y .jpeg")
                    End If
                Else

                End If

                If Request.QueryString("ID") Is Nothing Then
                    tbl.insertar()
                Else
                    tbl.valorID = Request.QueryString("ID")
                    tbl.modificar()
                End If
                scope.Complete()
                Response.Redirect("medico.aspx")
            Catch ex As Exception

            End Try
        End Using
        
        
    End Sub

    Private Sub gestionMedico_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If
        End If
    End Sub
End Class