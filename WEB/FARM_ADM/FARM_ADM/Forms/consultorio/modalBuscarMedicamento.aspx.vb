﻿Public Class modalBuscarMedicamento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        Dim parametros = txtBuscar.Text.Split("*")
        sSql += " DECLARE @MED AS NVARCHAR(MAX),@FF AS NVARCHAR(100), @LAB AS NVARCHAR(150), @COMP AS NVARCHAR (MAX);"
        sSql += " SET @MED = '" + Trim(parametros(0)) + "';"
        If parametros.Length > 1 Then
            sSql += " SET @COMP = '" + Trim(parametros(1)) + "';"
        Else
            sSql += " SET @COMP = '';"
        End If
        If parametros.Length > 2 Then
            sSql += " SET @FF = '" + Trim(parametros(2)) + "';"
        Else
            sSql += " SET @FF = '';"
        End If
        If parametros.Length > 3 Then
            sSql += " SET @LAB ='" + Trim(parametros(3)) + "';"
        Else
            sSql += " SET @LAB ='';"
        End If

        sSql += "  SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO, "
        sSql += "  UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END AS MEDICAMENTO, "
        sSql += "  LABORATORIO, UPPER(FORMA_FARMACEUTICA) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION,  M.MINIMO,"
        sSql += "  ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO,  "
        sSql += "  ISNULL(S.CANTIDAD,0) AS STOCK, ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA,"
        sSql += "  ISNULL(CONVERT(NVARCHAR(10),KD.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO"
        sSql += "  FROM VW_MEDICAMENTO M "     
        sSql += "  LEFT JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += "  LEFT JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY K.ID_MEDICAMENTO ORDER BY K.ID_MEDICAMENTO, K.F_VENCIMIENTO) AS NRO_KARDEX, K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS STOCK FROM KARDEX K "
        sSql += "  INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += "  WHERE K.SALDOS > 0"
        sSql += "  GROUP BY K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO) KD ON KD.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND KD.NRO_KARDEX = 1"

        'sSql += "  WHERE M.MEDICAMENTO LIKE '%' + @BUSCAR + '%' OR M.COMPOSICION LIKE '%' + @BUSCAR + '%'"
        sSql += " WHERE  M.MEDICAMENTO LIKE '%' + @MED +'%' AND M.FORMA_FARMACEUTICA LIKE '%' + @FF +'%' AND M.LABORATORIO LIKE '%' + @LAB +'%' "
        sSql += " AND M.COMPOSICION LIKE '%' + @COMP +'%'"

        ' sSql += "  AND ISNULL(S.CANTIDAD,0) > 0"



            'sSql += " SELECT MED.ID_MEDICAMENTO, MEDICAMENTO, FORMA_FARMACEUTICA, LABORATORIO, COMPOSICION,MED.ID_LABORATORIO,
            'sSql += " MED.PRECIO_UNITARIO,CONVERT(NVARCHAR(10),KD.F_VENCIMIENTO,103) AS VENCIMIENTO FROM VW_MEDICAMENTO AS MED "
            'sSql += " INNER JOIN INVENTARIO I ON I.ID_MEDICAMENTO = MED.ID_MEDICAMENTO "
            'sSql += " INNER JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY K.ID_MEDICAMENTO ORDER BY K.ID_MEDICAMENTO, K.F_VENCIMIENTO) AS NRO_KARDEX, K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS STOCK FROM KARDEX K "
            'sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
            'sSql += " WHERE(K.SALDOS > 0)"
            'sSql += " GROUP BY K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO) KD ON KD.ID_MEDICAMENTO = MED.ID_MEDICAMENTO AND KD.NRO_KARDEX = 1"
            'sSql += " WHERE  MED.MEDICAMENTO LIKE '%' + @MED +'%' AND MED.FORMA_FARMACEUTICA LIKE '%' + @FF +'%' AND MED.LABORATORIO LIKE '%' + @LAB +'%' "
            'sSql += " AND MED.COMPOSICION LIKE '%' + @COMP +'%' AND I.CANTIDAD > 0"
            'sSql += " ORDER BY MEDICAMENTO"





            'sSql += " SELECT ID_MEDICAMENTO, MEDICAMENTO, FORMA_FARMACEUTICA, LABORATORIO, COMPOSICION,MED.ID_LABORATORIO,MED.PRECIO_UNITARIO  FROM VW_MEDICAMENTO AS MED "

            'sSql += " WHERE  MED.MEDICAMENTO LIKE '%' + @MED +'%' AND MED.FORMA_FARMACEUTICA LIKE '%' + @FF +'%' AND MED.LABORATORIO LIKE '%' + @LAB +'%' "
            'sSql += " AND MED.COMPOSICION LIKE '%' + @COMP +'%'"
            'sSql += " ORDER BY MEDICAMENTO"

            lvwMedicamentos.DataSource = New mcTabla().ejecutarConsulta(sSql)
            lvwMedicamentos.DataBind()
    End Sub

    Private Sub buscarMedicamento_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then           
            txtBuscar.Focus()
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        cargarDatos()
        txtBuscar.Focus()
    End Sub

    Protected Sub lvwMedicamentos_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwMedicamentos.ItemCommand
        Select Case e.CommandName
            Case "seleccionar"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModalMedicamento('" + e.CommandArgument + "');", True)
        End Select
    End Sub

    Protected Sub lvwMedicamentos_PagePropertiesChanged(sender As Object, e As EventArgs) Handles lvwMedicamentos.PagePropertiesChanged
        cargarDatos()
        txtBuscar.Focus()
    End Sub
End Class