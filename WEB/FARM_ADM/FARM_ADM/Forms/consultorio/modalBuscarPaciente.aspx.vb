﻿Public Class modalBuscarPaciente
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub modalBuscarPaciente_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then            
            txtBuscar.Focus()
        End If
    End Sub

    Protected Sub lvwMedicamentos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lvwMedicamentos.ItemCommand
        Select Case e.CommandName
            Case "seleccionar"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal('" + e.CommandArgument + "');", True)
        End Select
    End Sub

    Protected Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If Page.IsValid Then
            Dim tbl = New mcTabla()
            tbl.tabla = "PACIENTE"
            tbl.modoID = "sql"
            tbl.campoID = "ID_PACIENTE"
            tbl.agregarCampoValor("NOMBRE_APELLIDOS", txtNombre.Text)
            tbl.agregarCampoValor("CI", txtCarnetIdentidad.Text)
            tbl.agregarCampoValor("F_NACIMIENTO", Date.Parse(txtNacimiento.Text, New Globalization.CultureInfo("es-ES")))
            tbl.insertar()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal('" + tbl.valorID + "');", True)
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        lvwMedicamentos.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha;", True)
    End Sub
End Class