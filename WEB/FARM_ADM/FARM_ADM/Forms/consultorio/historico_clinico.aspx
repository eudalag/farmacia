﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="historico_clinico.aspx.vb" Inherits="FARM_ADM.historico_clinico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
<div style="width:90%;">
<fieldset>
    <legend class="textoNegroNegrita">Paciente</legend>
    <table style="width:100%">
        <tr>
            <td style="width:80%">
                <table style="width:100%">
        <tr>
                <td class="textoGris" style="width:120px;">
                    Carnet de Identidad:
                </td>
                <td style="width:200px;">
                    <asp:TextBox runat="Server" ID="txtCarnetIdentidad" AutoPostBack="true" >
                    </asp:TextBox>                    
                    <asp:Button runat="server" ID="btnCargar" Text="cargar"  CausesValidation="false" style="display:none" />                    
                    <asp:Button runat="server" ID="btnBuscar" Text="Buscar" OnClientClick="buscarPaciente();" />
                </td>
                <td class="textoGris" style="width:120px;">
                    Fecha de Nacimiento:
                </td>
                <td style="width:120px;">
                    <asp:TextBox runat="server" ID="txtFechaNacimiento"  style="width:70px;" enabled="false">
                    </asp:TextBox>                                      
                </td>
                <td class="textoGris" style="width:50px;">
                    Edad:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtEdad"  style="width:50px;" Enabled="false">
                    </asp:TextBox>                                      
                </td>              
            </tr>

            <tr>
                   <td class="textoGris">
                    Paciente:
                </td>
                <td colspan="5">
                    <asp:TextBox runat="server" ID="txtCliente"  style="width:470px;" enabled="false">
                    </asp:TextBox>
                    <asp:HiddenField runat="server" ID="hidIdCliente" Value ="-1" />                    
                </td>  
            </tr>
    </table>
</fieldset>

<fieldset>
    <legend class="textoNegroNegrita">Historico Clinico</legend>
    <table style="width:100%">
        <tr>
                <td class="textoGris" style="width:120px;">
                    Fecha Historico:
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtFecha" disabled="true" style="width:70px;"></asp:TextBox>                    
                </td>                
        </tr>
        <tr>
                <td class="textoGris">
                    Peso:
                </td>
                <td style="width:120px;">
                    <asp:TextBox runat="Server" ID="txtPeso" style="width:50px;"  >
                    </asp:TextBox> &nbsp;Kg.                                        
                </td>
                <td class="textoGris" style="width:120px;">
                    Temperatura:
                </td>
                <td >
                    <asp:TextBox runat="server" ID="txtTemperatura"  style="width:50px;">
                    </asp:TextBox> &nbsp;°C                                     
                </td>
                              
            </tr>
    </table>
</fieldset>

<fieldset>
    <legend class="textoNegroNegrita">Evaluacion Medica</legend>
    <table style="width:100%">
        <tr>
            <td class="textoGris">
                    Motivo Consulta:
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="txtAntecedentes" TextMode="MultiLine" style="width:80%" Rows="5">
                    </asp:TextBox>  
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                    Diagnostico:
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="txtDiagnostico" TextMode="MultiLine" style="width:80%" Rows="5" >
                    </asp:TextBox>    
            </td>
        </tr>
    </table>
</fieldset>

            </td>
            <td style="width:20%; vertical-align:top;">
                <fieldset>
                    <legend class="textoNegroNegrita">Historial Medico</legend>                
                <asp:ListView runat="server" ID="lvwHistorico" DataSourceID="sqlHistorico">
                    <LayoutTemplate>
                        <table style="width:100%" class="generica">
                        <thead>
                            <tr>
                                <th>
                                    Fecha
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                        </tbody>
                    </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="alinearCentro">
                                <asp:LinkButton runat="server" ID="btnHistorialPaciente" Text='<%#Eval("F_HISTORICO") %>' OnClientClick='<%# String.concat("return verHistorial(",Eval("ID_HISTORICO"),");") %>'></asp:LinkButton>                                
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <asp:SqlDataSource runat="server" ID="sqlHistorico" 
                    ConnectionString="<%$ ConnectionStrings:BDFarmConnectionString %>" SelectCommand="SELECT CONVERT(NVARCHAR(10),FH_HISTORICO,103) AS F_HISTORICO, ID_HISTORICO FROM HISTORICO_CLINICO 
WHERE ID_PACIENTE = @PACIENTE">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdIdPaciente" Name="PACIENTE" 
                            PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
</fieldset>
            </td>
        </tr>
    </table>
    
<fieldset>
    <legend class="textoNegroNegrita">Receta</legend>
    <table style="width:100%">

    </table>
    <table style="width:100%">
             <tr>
                <td class="alinearDer">
                    <asp:Button runat="server" ID="btnNuevoItem" Text="Nuevo Medicamento" OnClientClick="return buscarMedicamento()" />
                    <asp:HiddenField runat="server" id="hdIdPaciente" Value="-1" />
                    <asp:HiddenField runat="server" id="hdIdMedicamento" Value="-1" />
                        <asp:HiddenField runat="server" id="hdTipo" Value="-1" />
                        <asp:Button runat="server" ID="btnCargarMedicamento" Text="cargar"  style="display:none;"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ListView ID="lvwGrilla" runat="server">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:20px;">&nbsp;</th>                                
                                 <th>Medicamento </th>
                                 <th style="width:40px;">Cantidad. </th>   
                                 <th style="width:40px;">Precio </th> 
                                 <th style="width:40px;">SubTotal </th>                              
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>
                             <td>
                                 <asp:ImageButton runat="server" ID="btnEliminar" CommandName="eliminar" CommandArgument='<%# Eval("NRO_FILA")%>' ImageUrl="~/App_Themes/estandar/imagenes/cancelar.png" />
                             </td>
                             <td class='alinearIzq'>   
                                 <asp:HiddenField runat="server" ID="hfNRO_FILA" Value='<%# Eval("NRO_FILA") %>' />                              
<asp:HiddenField runat="server" ID="hfIdMedicamento" Value='<%# Eval("ID_MEDICAMENTO") %>' />                              
                                 <asp:label runat="server" ID="lblMedicamento" Text='<%# Eval("MEDICAMENTO")%>' style="width:60px; text-align:right;"></asp:label >                                                              
                             </td>
                             <td class="textoGris">                                 
                                 <asp:TextBox runat="server" ID="txtCantidad" onfocus="this.select();" Text='<%# Eval("CANTIDAD")%>'  style="width:20px; text-align:right;" onblur="return calcularSubTotal(this);"></asp:TextBox>                                                              
                             </td>     
                             <td class="alinearDer">
                                <%# Eval("PRECIO_UNITARIO")%>
                                <asp:HiddenField runat="server" ID="hdPrecio" Value='<%# String.format(new system.globalization.cultureInfo("en-US"),"{0:n}",Eval("PRECIO_UNITARIO"))%>' />
                            </td> 
                            <td  class="alinearDer">
                                <asp:label runat="server" ID="lblSubTotal" CssClass="textoGris total" Style="width:60px;"  Text='<%# String.Format(New System.Globalization.CultureInfo("en-US"), "{0:n}", Eval("PRECIO_UNITARIO"))%>'></asp:label>
                                <asp:HiddenField runat="server" ID="hdSubTotal" Value='<%# Eval("PRECIO_UNITARIO")%>' />
                            </td>                                                  
                         </tr>
                         <tr style="background:#EBEDEF;">
                            <td colspan="5" class="textoGris" style="background:#EBEDEF;">
                                Dosificacion:
                                <asp:TextBox runat="server" ID="txtDosificacion" Text='<%# Eval("DOSIFICACION")%>' style="width:500px;"></asp:TextBox>
                            </td>
                         </tr>
                     </ItemTemplate>
                     <EmptyDataTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:20px;">&nbsp;</th>                                
                                 <th>Medicamento </th>
                                 <th style="width:40px;">Cantidad. </th> 
                                 <th style="width:40px;">Precio </th> 
                                 <th style="width:40px;">SubTotal </th> 
                             </tr>
                             <tr>
                                 <td colspan="3" class="textoNegro alinearCentro">
                                     No Existen Datos.
                                 </td>
                             </tr>

                     </EmptyDataTemplate>
                 </asp:ListView>
                <div class="alinearDer">
                    Total: <asp:TextBox runat="server" CssClass="alinearDer textoGris " ID="txtTotal" Text="0.00" Enabled="false"></asp:TextBox>
                </div>
                </td>
            </tr>
    </table>
</fieldset>
<br />

<div class="alinearDer">
    <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                     <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" PostBackUrl="~/Forms/consultorio/consultas.aspx" />
</div>
</div>
            </ContentTemplate>
    </asp:UpdatePanel>
<script type="text/javascript">
    function cancelarModal() {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");

    }
    function aceptarModal(idPaciente) {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");
        jQuery("#<%=hdIdPaciente.ClientID%>").val(idPaciente);
        jQuery("#<%=btnCargar.ClientID%>").click()
        return false;
    }
    function aceptarModalMedicamento(idMedicamento) {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");
        jQuery("#<%=hdIdMedicamento.ClientID%>").val(idMedicamento);
        jQuery("#<%=btnCargarMedicamento.ClientID%>").click()
        return false;
    }
    function buscarPaciente() {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var url = "modalBuscarPaciente.aspx";
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR PACIENTE", width: 800, height: 600 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }

    function buscarMedicamento() {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var url = "modalBuscarMedicamento.aspx";
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR MEDICAMENTOS", width: 1024, height: 600 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }
    function verHistorial(id) {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var url = "modalHistoricoPaciente.aspx?ID=" + id;
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "HISTORIAL MEDICO", width: 800, height: 600 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }
    function formatNumber(num) {
        num = Math.round(parseFloat(num) * Math.pow(10, 2)) / Math.pow(10, 2)
        num += '';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '.00';
        splitRight = splitRight + '00';
        splitRight = splitRight.substr(0, 3);
        var regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
        }
        return splitLeft + splitRight;
    }
    function ReplaceAll(Source, stringToFind, stringToReplace) {
        var temp = Source;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    }
    function calcularSubTotal(obj) {
        var idObjeto = "";
        
                idObjeto = "txtCantidad";
              
        var cantidad = parseInt(jQuery("#" + obj.id).val());

        var precio = parseFloat(jQuery("#" + obj.id.replace(idObjeto, "hdPrecio")).val());

        

        var subtotal = cantidad * precio;



        jQuery("#" + obj.id.replace(idObjeto, "lblSubTotal")).html(formatNumber(subtotal));
        jQuery("#" + obj.id.replace(idObjeto, "hdSubTotal")).html(subtotal);
        calcularTotal();
    }

    function calcularTotal() {
        var subTotal = 0;
        jQuery(".total").each(function () {
            subTotal = subTotal + parseFloat(ReplaceAll(jQuery(this).html(), ",", ""));
        });

        jQuery("#<%= txtTotal.ClientID%>").val(formatNumber(subTotal));
        
    }
</script>    
</asp:Content>
