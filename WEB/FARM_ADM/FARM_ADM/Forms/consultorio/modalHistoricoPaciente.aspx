﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalHistoricoPaciente.aspx.vb" Inherits="FARM_ADM.modalHistoricoPaciente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
 <fieldset>
    <legend class="textoNegroNegrita">Historico Clinico</legend>
    <table style="width:100%">
        <tr>
                <td class="textoGris" style="width:120px;">
                    Fecha Historico:
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtFecha" disabled="true" style="width:70px;"></asp:TextBox>                    
                </td>                
        </tr>
        <tr>
                <td class="textoGris">
                    Peso:
                </td>
                <td style="width:120px;">
                    <asp:TextBox runat="Server" ID="txtPeso" style="width:50px;" Enabled="false"  >
                    </asp:TextBox> &nbsp;Kg.                                        
                </td>
                <td class="textoGris" style="width:120px;">
                    Temperatura:
                </td>
                <td >
                    <asp:TextBox runat="server" ID="txtTemperatura"  style="width:50px;" Enabled="false">
                    </asp:TextBox> &nbsp;°C                                     
                </td>
                              
            </tr>
    </table>
</fieldset>

<fieldset>
    <legend class="textoNegroNegrita">Evaluacion Medica</legend>
    <table style="width:100%">
        <tr>
            <td class="textoGris">
                    Motivo Consulta:
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="txtAntecedentes" TextMode="MultiLine" style="width:80%" Rows="5" Enabled="false">
                    </asp:TextBox>  
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                    Diagnostico:
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="txtDiagnostico" TextMode="MultiLine" style="width:80%" Rows="5" Enabled="false" >
                    </asp:TextBox>    
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend class="textoNegroNegrita">Receta</legend>
    <table style="width:100%">

    </table>
    <table style="width:100%">
             <tr>
                <td>
                    <asp:ListView ID="lvwGrilla" runat="server" DataSourceID="sqlReceta">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>                                 
                                 <th>Medicamento </th>
                                 <th style="width:40px;">Cantidad. </th>                                
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>                             
                             <td class='alinearIzq'>                                    
                                 <%# Eval("MEDICAMENTO")%>' 
                             </td>
                             <td class="textoGris">                                 
                                 <%# Eval("CANTIDAD")%>
                             </td>                                                       
                         </tr>
                         <tr style="background:#EBEDEF;">
                            <td colspan="3" class="textoGris" style="background:#EBEDEF;">
                                Dosificacion:
                                <asp:label runat="server" ID="lblDosificacion" Text='<%# Eval("DOSIFICACION")%>'></asp:label>
                            </td>
                         </tr>
                     </ItemTemplate>
                     <EmptyDataTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>                                 
                                 <th>Medicamento </th>
                                 <th style="width:40px;">Cantidad. </th>                                  
                             </tr>
                             <tr>
                                 <td colspan="2" class="textoNegro alinearCentro">
                                     No Existen Datos.
                                 </td>
                             </tr>

                     </EmptyDataTemplate>
                 </asp:ListView>
<asp:SqlDataSource runat="server" ID="sqlReceta" 
                        ConnectionString="<%$ ConnectionStrings:BDFarmConnectionString %>" SelectCommand="SELECT MEDICAMENTO + ' (' + LABORATORIO + ') - ' + FORMA_FARMACEUTICA AS MEDICAMENTO, RD.CANTIDAD,RD.INDICACIONES AS DOSIFICACION  FROM RECETA R 
INNER JOIN RECETA_DETALLE RD ON RD.ID_RECETA = R.ID_RECETA 
INNER JOIN VW_MEDICAMENTO M ON  M.ID_MEDICAMENTO = RD.ID_MEDICAMENTO 
WHERE R.ID_HISTORICO = @HISTORICO">
    <SelectParameters>
        <asp:QueryStringParameter Name="HISTORICO" QueryStringField="ID" />
    </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
    </table>
</fieldset>
    </form>
</body>
</html>
