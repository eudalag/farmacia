﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalBuscarPaciente.aspx.vb" Inherits="FARM_ADM.modalBuscarPaciente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#txtNacimiento").datepicker({});
        });
        window.addEvent("domready", function () {
            var fecha = new Mascara("txtNacimiento", { 'fijarMascara': '##/##/####', 'esNumero': false });
        });
    </script>
</head>
<body>
<form id="form1" runat="server" defaultbutton="btnBuscar">  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>                        
    <div>
        <asp:ListView runat="server" ID="lvwMedicamentos" DataSourceID="sqlPaciente">
            <LayoutTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                        <th >
                            Paciente
                        </th>
                        <th style="width:200px;">
                            Nro. Carnet Identidad
                        </th>
                        <th style="width:100px;">
                            Fecha Nacimiento
                        </th>
                        <th style="width:50px;">
                            Edad
                        </th>
                        
                    </tr>
                    <tr runat="server" id="itemPlaceHolder"></tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" id="btnMedicamento" Text='<%# Eval("NOMBRE_APELLIDOS")%>' CommandArgument='<%# Eval("ID_PACIENTE")%>' CommandName='seleccionar' ></asp:LinkButton>                        
                    </td>
                    <td class="textoGris">
                        <%# Eval("CI")%>
                    </td>
                    <td class="textoGris">
                        <%# Eval("F_NACIMIENTO")%>
                    </td>                     
                     <td class="textoGris">
                        <%# Eval("EDAD")%>
                    </td>
                     
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                       <th >
                            Paciente
                        </th>
                        <th style="width:150px;">
                            Nro. Carnet Identidad
                        </th>
                        <th style="width:100px;">
                            Fecha Nacimiento
                        </th>
                        <th style="width:50px;">
                            Edad
                        </th>
                        
                    </tr>
                    <tr>
                        <td colspan="4" class="textoGris">
                            No Existen Datos.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>
        <asp:SqlDataSource runat="server" ID="sqlPaciente" 
            ConnectionString="<%$ ConnectionStrings:BDFarmConnectionString %>" SelectCommand="SELECT ID_PACIENTE,NOMBRE_APELLIDOS,CI,CONVERT(NVARCHAR(10),F_NACIMIENTO,103) AS F_NACIMIENTO, CASE WHEN dateadd(year, datediff(year, F_NACIMIENTO, GETDATE()), F_NACIMIENTO) &gt; GETDATE()
THEN DATEDIFF(year, F_NACIMIENTO, GETDATE()) - 1 ELSE DATEDIFF(year, F_NACIMIENTO, GETDATE())  END  AS EDAD FROM PACIENTE
WHERE NOMBRE_APELLIDOS LIKE '%' + @BUSCAR + '%' OR CI LIKE '%' + @BUSCAR + '%'">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="BUSCAR" 
                    PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMedicamentos" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager>                     
                </div>
        <div class="alinearCentro">
        <div >
            <span class="textoGris">
                Buscar:
            </span>
            <span>
                <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
                <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
            </span>
        </div>
<br />
<br />
        <div style="width:100%;display: flex; justify-content: center; align-items: center;">
         <fieldset>
            <legend class="textoNegroNegrita">Nuevo Paciente</legend>
            <table class="alinearIzq">
                <tr>
                    <td class="textoGris">
                        Nombre(s) y Apellido(s):
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNombre" style="width:400px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Carnet de Identidad:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCarnetIdentidad" style="width:100px"></asp:TextBox>
                    </td>
                </tr>

                <tr>
                    <td class="textoGris">
                        Fecha de Nacimiento:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNacimiento" style="width:70px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="alinearDer">
                        <asp:Button runat="Server" ID="btnNuevo" Text="Nuevo" />
                    </td>
                </tr>
            </table>
        </fieldset>
            
        </div>         
        <br />      
        </div>       
    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
        function iniciarFecha() {
            jQuery.noConflict();
            jQuery("#txtNacimiento").datepicker({});
            var txtNacimiento = new Mascara("txtNacimiento", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }
    </script>
</body>
</html>
