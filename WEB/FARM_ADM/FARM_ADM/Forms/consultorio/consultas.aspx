﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="consultas.aspx.vb" Inherits="FARM_ADM.consultas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Consultas:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
         <tr>
         <td colspan="2" class="alinearDer">
             <asp:Button runat="server" ID="a_btnNuevo" Text="Nueva Consulta" PostBackUrl="~/Forms/consultorio/historico_clinico.aspx"  />
         </td>
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwBaseDatos" DataSourceID="sqlBaseDatos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th style="width:50px;">
                                    Nro. Hist.
                                </th>
                                <th style="width:70px;">
                                    Fecha
                                </th>       
                                <th style="width:50px;">
                                    Hora
                                </th>
                                <th>
                                    Paciente
                                </th>
                                <th style="width:100px;">
                                    Carnet Identidad
                                </th>
                                <th style="width:50px;">
                                    Edad
                                </th>
                                <th style="width:50px;">
                                    Peso
                                </th>
                                <th style="width:70px;">
                                    Temperatura
                                </th>
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("ID_HISTORICO")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("F_HISTORICO")%>
                            </td>  
                            <td class="textoGris">
                                <%# Eval("H_HISTORICO")%>
                            </td> 
                            <td class="textoGris">
                                <%# Eval("NOMBRE_APELLIDOS")%>
                            </td>   
                            <td class="textoGris">
                                <%# Eval("CI")%>
                            </td>     
                            <td class="textoGris">
                                <%# Eval("EDAD")%>
                            </td>      
                            <td class="textoGris">
                                <%# Eval("PESO")%>
                            </td>         
                            <td class="textoGris">
                                <%# Eval("TEMPERATURA")%>
                            </td>                     
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwBaseDatos" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlBaseDatos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT HC.ID_HISTORICO,CONVERT(NVARCHAR(10),HC.FH_HISTORICO,103) AS F_HISTORICO,CONVERT(NVARCHAR(10),HC.FH_HISTORICO,108) AS H_HISTORICO,P.ID_PACIENTE, P.NOMBRE_APELLIDOS,P.CI, CASE WHEN dateadd(year, datediff(year, P.F_NACIMIENTO, GETDATE()), P.F_NACIMIENTO) > GETDATE()
                                            THEN DATEDIFF(year, P.F_NACIMIENTO, GETDATE()) - 1
                                            ELSE DATEDIFF(year, P.F_NACIMIENTO, GETDATE())  
                                            END  AS EDAD,HC.TEMPERATURA,HC.PESO FROM HISTORICO_CLINICO HC
                                        INNER JOIN PACIENTE P ON P.ID_PACIENTE = HC.ID_PACIENTE
                                        LEFT JOIN RECETA R ON R.ID_HISTORICO = HC.ID_HISTORICO ">                      
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
</asp:Content>
