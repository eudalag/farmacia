﻿Public Class modalHistoricoPaciente
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos(ByVal idHistorico As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @HISTORICO AS INTEGER"
        sSql += " SET @HISTORICO = " + idHistorico.ToString + ";"
        sSql += " SELECT CONVERT(NVARCHAR(10),HC.FH_HISTORICO,103) AS F_HISTORICO, ISNULL(CAST (HC.PESO AS NVARCHAR(10)),'') AS PESO, ISNULL(CAST(HC.TEMPERATURA AS NVARCHAR(10)),'') AS TEMPERATURA,"
        sSql += " HC.ANTECEDENTES, HC.CONCLUSIONES  FROM HISTORICO_CLINICO HC "
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = HC.ID_PACIENTE "
        sSql += " WHERE HC.ID_HISTORICO = @HISTORICO"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtAntecedentes.Text = tbl.Rows(0)("ANTECEDENTES")
            txtDiagnostico.Text = tbl.Rows(0)("CONCLUSIONES")
            txtPeso.Text = tbl.Rows(0)("PESO")
            txtTemperatura.Text = tbl.Rows(0)("TEMPERATURA")
            txtFecha.Text = tbl.Rows(0)("F_HISTORICO")
        End If
    End Sub

    Private Sub modalHistoricoPaciente_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            cargarDatos(Request.QueryString("ID"))
        End If
    End Sub
End Class