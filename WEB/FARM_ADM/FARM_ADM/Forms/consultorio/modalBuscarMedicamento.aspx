﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalBuscarMedicamento.aspx.vb" Inherits="FARM_ADM.modalBuscarMedicamento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="form1" runat="server" defaultbutton="btnBuscar">  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>                        
    <div>
        <asp:ListView runat="server" ID="lvwMedicamentos">
            <LayoutTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                        <th style="width:250px;">
                            Medicamento
                        </th>
                        <th>
                            Nombre Generico
                        </th>
                        <th style="width:100px;">
                            Presentacion
                        </th>
                        <th style="width:100px;">
                            Laboratorio
                        </th>
                         <th style="width:50px;">
                            Precio 
                        </th>
                        <th style="width:50px;">
                            Stock 
                        </th>
                        <th style="width:80px;">
                            Vencimiento 
                        </th>
                    </tr>
                    <tr runat="server" id="itemPlaceHolder"></tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" id="btnMedicamento" Text='<%# Eval("MEDICAMENTO")%>' CommandArgument='<%# Eval("ID_MEDICAMENTO")%>' CommandName='seleccionar' ></asp:LinkButton>                        
                    </td>
                    <td class="textoGris">
                        <%# Eval("COMPOSICION")%>
                    </td>
                    <td class="textoGris">
                        <%# Eval("DESC_TIPO_MEDICAMENTO")%>
                    </td>                     
                     <td class="textoGris">
                        <%# Eval("LABORATORIO")%>
                    </td>
                      <td class="textoGris alinearDer ">
                        <%# String.Format(New System.Globalization.CultureInfo("en-US"),"{0:#,##0.00}",Eval("PRECIO_UNITARIO"))%>
                    </td>
 <td class="textoGris">
                        <%# Eval("STOCK")%>
                    </td>
 <td class="textoGris">
                        <%# Eval("F_VENCIMIENTO")%>
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                        <th style="width:250px;">
                            Medicamento
                        </th>
                        <th>
                            Nombre Generico
                        </th>
                        <th style="width:120px;">
                            Presentacion
                        </th>
                        <th style="width:120px;">
                            Laboratorio
                        </th>
                        <th style="width:120px;">
                            Precio
                        </th>
                    </tr>
                    <tr>
                        <td colspan="5" class="textoGris">
                            No Existen Datos.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>
        <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMedicamentos" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager>                     
                </div>
        <div class="alinearCentro">
        <div >
            <span class="textoGris">
                Buscar:
            </span>
            <span>
                <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
                <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
            </span>
        </div> 
        <br />
        <div class="textoAyuda">
            <u>Parametros de Busqueda:</u> MEDICAMENTO<b>(*)</b>NOMBRE_GENERICO<b>(*)</b>PRESENTACION<b>(*)</b>LABORATORIO <br />
            Para Filtrar los medicamentos debera separa con un signo asterisco (*) en el orden de busqueda que desee.
        </div>
        </div>       
    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }        
    </script>
</body>
</html>
