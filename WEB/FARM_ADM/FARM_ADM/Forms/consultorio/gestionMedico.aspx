﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionMedico.aspx.vb" Inherits="FARM_ADM.gestionMedico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%;">
        <tr>
          <td colspan="2"> 
            <div class="alinearIzq" style="padding-bottom:10px;"> 
            <asp:Label ID="Label5" runat="server" Text="Gestion Proveedores:" CssClass="textoNegroNegrita" ></asp:Label> 
            <hr style="height:-12px;color:#CCCCCC;width:30%;text-align:left;" align="left" />
            </div>
          </td>
        </tr>
        <tr>
            <td class="textoGris" style="width:130px;">
                Nombre y Apellidos:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombre" MaxLength="150" Columns="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="(*) Campo Obligatorio" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
                <asp:HiddenField runat="server" ID="hdIdProveedor" Value="0" />
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Especialidad:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEspecialidad" MaxLength="50" Columns="15" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="(*) Campo Obligatorio" ControlToValidate="txtEspecialidad"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Matricula:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtMatricula" MaxLength="150"  Columns="100"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="(*) Campo Obligatorio" ControlToValidate="txtMatricula"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td class="textoGris">
                Telefono o Celular:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="25" Columns="25"></asp:TextBox>
            </td>
        </tr>
        
        <tr>
            <td class="textoGris">
                Firma:
            </td>
            <td>
                <asp:FileUpload ID="fileUpload" CssClass="textoNegro" runat="server" />
            </td>
        </tr>                                      
        
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblError" Text="" CssClass="textoRojo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="alinearDer" >
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" />
            </td>
        </tr>

    </table>
</asp:Content>
