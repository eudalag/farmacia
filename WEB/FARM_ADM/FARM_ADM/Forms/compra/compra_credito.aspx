﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="compra_credito.aspx.vb" Inherits="FARM_ADM.compra_credito1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
<script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width:100%">
                <tr>
                  <td colspan="2"> 
                    <div class="alinearIzq" style="padding-bottom:10px;""> 
                    <asp:Label ID="Label5" runat="server" Text="CUENTAS  POR PAGAR:" CssClass="textoNegroNegrita" ></asp:Label> 
                    <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
                    </div>
                  </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Buscar:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtBuscar" style="width:150px;"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="alinearDer">
                        <asp:Button runat="server" ID="btnNuevoPago" Text="Nuevo Pago" OnClientClick="nuevoPago();" />
                        <asp:Button runat="server" ID="btnCargar" Text="cargar"  CausesValidation="false" style="display:none" />                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                            <asp:ListView runat="server" ID="lvwMovimientos" DataSourceID="sqlMovimientos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th style="width:25px;">
                                    &nbsp;
                                </th>
                                <th>
                                    Proveedor
                                </th>
                                <th style="width:100px;">
                                    Tipo Comprobante
                                </th>
                                <th style="width:100px;">
                                    Nro. Comprobante
                                </th>
                                <th style="width:80px;">
                                    Vencimiento
                                </th>
                                <th style="width:100px;">
                                    Importe
                                </th>
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>    
                            <td class="textoGris">
                                <input type="checkbox" runat="server" id="chkCredito" class="checked textoNegro" onchange="seleccionar_item(this)" />  
                                <asp:HiddenField runat="server" ID="hdCredito" Value='<%# Eval("ID_COMPRA_CREDITO") %>'   />
                            </td>
                            <td class="textoGris">
                                <%# Eval("NOMBRE")%>
                            </td>

                            <td class="textoGris">
                                <%# Eval("DETALLE")%>
                            </td>

                            <td class="textoGris">
                                <%# Eval("NRO_COMPROBANTE")%>
                            </td>

                            <td class="textoGris">
                                <%# Eval("F_VENCIMIENTO")%>
                            </td>

                            <td class="textoGris">
                                <%# Eval("SALDO")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                    </EmptyDataTemplate>
                </asp:ListView>
<div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMovimientos" PageSize="20"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlMovimientos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT P.ID_PROVEEDOR,CC.ID_COMPRA_CREDITO  ,P.NOMBRE,TC.DETALLE,C.NRO_COMPROBANTE,
CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103) AS F_VENCIMIENTO,CC.SALDO  FROM COMPRA_CREDITO CC 
INNER JOIN COMPRA C ON C.ID_COMPRA = CC.ID_COMPRA 
INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR 
INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE 
WHERE NOMBRE LIKE '%' + @BUSCAR + '%' AND SALDO > 0
ORDER BY F_vENCIMIENTO DESC ">                        

                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" Name="BUSCAR" PropertyName="Text" DefaultValue="%" />
                        </SelectParameters>

                    </asp:SqlDataSource>
                </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    function seleccionar_item(obj) {
        var estado = obj.checked;
        jQuery(".checked").each(function () {
            jQuery(this).attr("checked", false);
        });
        var chk = jQuery(obj).attr("checked", estado);
        return false;
    }
    function nuevoPago() {
        var idremplazo = '';
        var estado = '';
        var descEstado = '';
        var result = false;
        jQuery(".checked").each(function () {
            var chk = jQuery(this).is(':checked');
            if (chk) {
                id = jQuery(this).attr('id');
                idremplazo = id.replace("chkCredito", "hdCredito");
                var hdEliminar = $get(idremplazo);
                pago(hdEliminar.value);
            }
        });
    }
    function pago(idCredito) {
        jQuery.noConflict();
        var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
        var frame = dialog.children("iframe");
        var url = "modal_pago_credito.aspx?ID=" + idCredito;
        frame.attr("src", url);
        dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "PAGO A PROVEEDORES", width: 500, height: 280 });
        dialog.dialog('open');
        dialog.bind('dialogclose', function () {
            jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
        });
        return false;
    }
    function aceptarModal() {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");
        jQuery("#<%=btnCargar.ClientID%>").click()
        return false;
    }
    function cancelarModal() {
        jQuery.noConflict();
        jQuery("#jqueryuidialog").dialog("close");

    }
</script>
</asp:Content>
