﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modal_pago_credito.aspx.vb" Inherits="FARM_ADM.modal_pago_credito" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
 <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#txtFechaPago").datepicker({});           
        });
        window.addEvent("domready", function () {
            var txtPago = new Mascara("txtPago", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtFecha = new Mascara("txtFechaPago", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:100%">
            <tr>
                <td class="textoGris">Nit: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtNit" ReadOnly="true" CssClass="textoGris" style="width:150px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Razon Social: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtRazonSocial" ReadOnly="true" CssClass="textoGris" style="width:300px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris" style="width:80px;">Comprobante: </td>
                <td style="width:140px;">
                    <asp:TextBox runat="server" ID="txtComprobante" ReadOnly="true" CssClass="textoGris" style="width:120px;"></asp:TextBox>
                </td>
                <td class="textoGris" style="width:50px;">Nro.: </td>
                <td style="width:80px;">
                    <asp:TextBox runat="server" ID="txtNumero" ReadOnly="true" CssClass="textoGris" style="width:50px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Fecha: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtFecha" ReadOnly="true" CssClass="textoGris" style="width:80px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Tipo Pago: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtTipoPago" ReadOnly="true" CssClass="textoGris" style="width:100px;"></asp:TextBox>
                </td>
                <td class="textoGris">F. Limite: </td>
                <td>
                    <asp:TextBox runat="server" ID="txtFechaLimite" ReadOnly="true" CssClass="textoGris" style="width:80px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Total Credito: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtTotalCredito" ReadOnly="true" CssClass="textoGris" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Pago Parcial: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtParcial" ReadOnly="true" CssClass="textoGris"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Saldo: </td>
                <td colspan="3">
                    <br />
                    <asp:TextBox runat="server" ID="txtSaldo" ReadOnly="true" CssClass="textoNegroNegrita"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td class="textoGris">Banco: </td>
                <td colspan="3">
                    <asp:DropDownList runat="server" ID="ddlBanco" DataSourceID="sqlBanco" DataTextField="BANCO" DataValueField="ID_BANCO" AutoPostBack="true"></asp:DropDownList>
                        <asp:SqlDataSource runat="server" ID="sqlBanco" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" SelectCommand="SELECT TB.DESC_TIPO_BANCO + CASE WHEN NRO_CUENTA IS NULL THEN '' ELSE ' - ' + NRO_CUENTA END + ' - ' + ENTIDAD_FINANCIERA AS BANCO, B.ID_BANCO   FROM BANCO B 
                            INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO 
                            WHERE ID_USUARIO = @USUARIO 
                            ORDER BY ID_BANCO">
                            <SelectParameters>
                                <asp:SessionParameter Name="USUARIO" SessionField="IDUSUARIO" />
                            </SelectParameters>
                        </asp:SqlDataSource>
<asp:HiddenField runat="server" ID="hdCompra" Value="" />
                </td>
            </tr>
            <tr>
                <td class="textoGris">Fecha Pago: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtFechaPago" CssClass="textoGris"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="textoGris">Total Pago: </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="txtPago" CssClass="textoGris"></asp:TextBox>
                </td>
            </tr>
            <tr>
                    <td colspan="4" class="alinearDer">
                        <br />
                        <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                        <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" OnClientClick="cerrarInformacion();" CausesValidation="false" />
                    </td>                   
                </tr>
        </table>
    </div>
    </form>
  <script type="text/javascript">
      function cerrarInformacion() {
          parent.cancelarModal();
          return false;
      }        
    </script>
</body>
</html>
