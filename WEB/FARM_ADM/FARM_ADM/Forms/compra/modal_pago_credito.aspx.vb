﻿Imports System.Transactions
Public Class modal_pago_credito
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarCabecera()
        Dim sSql As String = ""
        sSql += " DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + Request.QueryString("ID") + ";"

        sSql += " SELECT P.NIT,P.NOMBRE AS RAZON_SOCIAL, TC.DETALLE AS COMPROBANTE,"
        sSql += " CONVERT(NVARCHAR(10),C.FECHA,103) AS FECHA, C.NRO_COMPROBANTE, CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103) AS F_VENCIMIENTO, ISNULL(PP.TOTAL_PAGO,0) AS PAGO_PARCIAL, CC.SALDO,CC.IMPORTE_CREDITO,CC.ID_COMPRA   FROM COMPRA_CREDITO CC "
        sSql += " INNER JOIN COMPRA C ON C.ID_COMPRA = CC.ID_COMPRA"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR"
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE"
        sSql += " LEFT JOIN (SELECT ID_COMPRA_CREDITO, SUM(IMPORTE_PAGO) AS TOTAL_PAGO FROM PAGO_CREDITO_COMPRA "
        sSql += " GROUP BY ID_COMPRA_CREDITO) PP ON PP.ID_COMPRA_CREDITO = CC.ID_COMPRA_CREDITO "
        sSql += " WHERE CC.ID_COMPRA_CREDITO = @ID"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtNit.Text = tbl.Rows(0)("NIT")
            txtRazonSocial.Text = tbl.Rows(0)("RAZON_SOCIAL")
            txtComprobante.Text = tbl.Rows(0)("COMPROBANTE")
            txtFecha.Text = tbl.Rows(0)("FECHA")
            txtNumero.Text = tbl.Rows(0)("NRO_COMPROBANTE")
            txtFechaLimite.Text = tbl.Rows(0)("F_VENCIMIENTO")
            txtParcial.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("PAGO_PARCIAL"))
            txtTotalCredito.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("IMPORTE_CREDITO"))
            txtSaldo.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tbl.Rows(0)("SALDO"))
            txtTipoPago.Text = "CREDITO"
            txtPago.Text = "0.00"
            hdCompra.Value = tbl.Rows(0)("ID_COMPRA")
        End If
    End Sub

    Private Sub modal_pago_credito_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            cargarCabecera()
            txtFechaPago.Text = Now().ToString("dd/MM/yyyy")
        End If
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then

            Using scope As New TransactionScope
                Try
                    procesarPagoCredito()
                    procesarMovimientoBanco()
                    scope.Complete()
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal();", True)
                Catch ex As Exception

                End Try
            End Using

        End If
    End Sub
    Protected Sub procesarPagoCredito()
        Dim pago As Decimal = Decimal.Parse(txtPago.Text, New Globalization.CultureInfo("en-US"))
        Dim saldo As Decimal = Decimal.Parse(txtSaldo.Text, New Globalization.CultureInfo("en-US"))
        Dim tbl = New mcTabla()
        tbl.tabla = "PAGO_CREDITO_COMPRA"
        tbl.campoID = "ID_PAGO_CREDITO_COMPRA"
        tbl.modoID = "sql"
        tbl.agregarCampoValor("ID_COMPRA_CREDITO", Request.QueryString("ID"))
        tbl.agregarCampoValor("FH_PAGO", txtFechaPago.Text)
        tbl.agregarCampoValor("IMPORTE_PAGO", Decimal.Parse(txtPago.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("ID_TIPO_MONEDA", 1)
        tbl.agregarCampoValor("TC", "0")
        tbl.insertar()
        If saldo <= pago Then
            tbl.reset()
            tbl.tabla = "COMPRA"
            tbl.campoID = "ID_COMPRA"
            tbl.valorID = hdCompra.Value
            tbl.agregarCampoValor("ID_ESTADO", 6)
            tbl.modificar()
        End If
       
    End Sub
    Protected Sub procesarMovimientoBanco()

        Dim importe As Decimal = Decimal.Parse(txtPago.Text, New Globalization.CultureInfo("en-US"))        
        Dim saldo As Decimal = Decimal.Parse(txtSaldo.Text, New Globalization.CultureInfo("en-US"))
        Dim pago As Decimal = 0
        If saldo <= importe Then
            pago = saldo
        Else
            pago = importe
        End If
        Dim tbl = New mcTabla()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.modoID = "auto"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_BANCO", ddlBanco.SelectedValue())
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 2)
        tbl.agregarCampoValor("IMPORTE", pago * (-1))
        tbl.agregarCampoValor("GLOSA", "Pago Credito a Proveedor: " + txtRazonSocial.Text + " - " + txtComprobante.Text + " Nro.: " + txtNumero.Text)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_PLAN_CUENTA", 12)
        tbl.insertar()
        Dim cuenta = New plan_cuentas()
        cuenta.insertarMovimiento(importe, 12, "MOVIMIENTO_BANCO", tbl.valorID, "serverDateTime")
    End Sub
End Class