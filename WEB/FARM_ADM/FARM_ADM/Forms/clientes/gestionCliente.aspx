﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionCliente.aspx.vb" Inherits="FARM_ADM.gestionCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:90%" cellpadding="2" cellspacing="2">
    <tr>
      <td colspan="6"> 
        <div class="alinearIzq" style="padding-bottom:10px;"> 
        <asp:Label ID="Label5" runat="server" Text="Gestion Clientes:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:150px; ">
             Nombre y Apellidos:
         </td>
         <td colspan="5">
             <asp:TextBox runat="server" ID="txtNombreApellido" MaxLength="150" style="width:250px; "></asp:TextBox>             
            <asp:HiddenField runat="server" ID="hdPersona" Value="-1" />
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:150px; ">
             Carnet de Identidad:
         </td>
         <td >
             <asp:TextBox runat="server" ID="txtCarnetIdentidad" MaxLength="150" style="width:250px; "></asp:TextBox>             
         </td>
        <td class="textoGris">
             Celular:
         </td>
         <td colspan="3">
             <asp:TextBox runat="server" ID="txtCelular" MaxLength="10" style="width:150px; "></asp:TextBox>             
         </td>
     </tr>
<tr>
         <td class="textoGris">
             Direccion:
         </td>
         <td colspan="5">
             <asp:TextBox runat="server" ID="txtDireccion" MaxLength="250" style="width:500px; "></asp:TextBox>             
         </td>
     </tr>
<tr>
         <td class="textoGris">
             Empresa de Referencia:
         </td>
         <td colspan="5">
             <asp:TextBox runat="server" ID="txtEmpresa" MaxLength="250" style="width:500px; "></asp:TextBox>             
         </td>
     </tr>
<tr>
         <td class="textoGris">
             Correo Electronico:
         </td>
         <td colspan="5">
             <asp:TextBox runat="server" ID="txtCorreo" MaxLength="250" style="width:500px; "></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Nit:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtNit" MaxLength="15" style="width:80px; "></asp:TextBox>             
         </td>
        <td class="textoGris">
             Nombre para Factura:
         </td>
        <td colspan="3">
             <asp:TextBox runat="server" ID="txtNombreFactura" MaxLength="150" style="width:250px; "></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Credito:
         </td>
         <td>
             <asp:CheckBox ID="chkCredito" runat="server" />
         </td>
        <td class="textoGris">
             Limite Credito:
         </td>
        <td>
             <asp:TextBox runat="server" ID="txtLimite" Text="0.00" MaxLength="7" style="width:100px; "></asp:TextBox>             
         </td>
        <td class="textoGris">
             Estado Cuenta:
         </td>
            <td>
             <asp:TextBox runat="server" CssClass="textoNegroNegrita alinearDer" ID="txtEstado" 
                    Text="0.00" ReadOnly="True"></asp:TextBox>             
         </td>
     </tr>
      <tr>
         <td class="textoGris">
             Grupo:
         </td>
         <td colspan="5">
             <asp:DropDownList ID="ddlGrupo" runat="server" DataSourceID="sqlGrupo"
                DataTextField="GRUPO_PERSONAS" DataValueField="ID_GRUPO">
             </asp:DropDownList>
             <asp:SqlDataSource ID="sqlGrupo" runat="server" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                 SelectCommand="SELECT [ID_GRUPO], [GRUPO_PERSONAS] FROM [GRUPO_CLIENTES] ORDER BY [ID_GRUPO]"></asp:SqlDataSource>
         </td>
     </tr>  
        <tr>
         <td class="textoGris">
             Observacion:
         </td>
         <td colspan="5">
             <asp:TextBox runat="server" ID="txtObservacion" MaxLength="250" style="width:500px; "></asp:TextBox>             
         </td>
     </tr>
        <tr>
            <td colspan="6" class="alinearDer">
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" PostBackUrl="~/Forms/clientes/clientes.aspx" />
            </td>
        </tr>
    </table>
</asp:Content>
