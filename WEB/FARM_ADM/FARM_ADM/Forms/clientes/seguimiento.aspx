﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="seguimiento.aspx.vb" Inherits="FARM_ADM.seguimiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtDesde.ClientID %>").datepicker({});
            jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        });

        window.addEvent("domready", function () {
            var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table style="width:100%">
    <tr>
      <td colspan="4"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="SEGUIMIENTO DE VENTAS POR CLIENTE:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" >
             Grupo Economico:
         </td>
         <td colspan="3">
            <asp:DropDownList runat="server" ID="ddlGrupo" DataSourceID="sqlGrupo"
                DataTextField="GRUPO_PERSONAS" DataValueField="ID_GRUPO" AutoPostBack="true"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlGrupo" 
                                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                SelectCommand="SELECT ID_GRUPO, GRUPO_PERSONAS FROM GRUPO_CLIENTES 
                UNION ALL SELECT -1, '[Seleccione un Grupo Economico]'
                ORDER BY GRUPO_PERSONAS"></asp:SqlDataSource>
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:50px; ">
             Clientes:
         </td>
         <td colspan="3">
             <asp:DropDownList runat="server" ID="ddlCliente" DataSourceID="sqlCliente"
                DataTextField="NOMBRE_APELLIDOS" DataValueField="ID_CLIENTE"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="sqlCliente" 
                                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                SelectCommand="SELECT CL.ID_CLIENTE, P.NOMBRE_APELLIDOS FROM CLIENTE CL 
                                                INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
                                                WHERE CL.ID_CLIENTE <> 1 AND CL.ID_GRUPO = @GRUPO UNION ALL SELECT -1 ,'[Seleccione un Cliente]' ORDER BY NOMBRE_APELLIDOS">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlGrupo" Name="GRUPO" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
             </asp:SqlDataSource>
         </td>
     </tr>  
    <tr>
        <td colspan="4" class="textoGris">
            Solo Ventas / Pagos Credito 
            <asp:CheckBox ID="chkCredito" runat="server" />
        </td>
    </tr>
   <tr>
        <td class="textoGris" style="width:100px; ">
            Desde:
        </td>
        <td style="width:130px; ">
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris" style="width:50px; ">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>
            <asp:Button runat="server" ID="btnCargar" Text="Buscar" />
        </td>
    </tr>
        <tr>
            <td colspan="4">
                <asp:ListView runat="server" ID="lvwMovimientos" DataSourceID="sqlMovimientos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th style="width:100px;">
                                    Fecha
                                </th>
                                <th>
                                    Tipo Comprobante
                                </th>
                                <th style="width:200px;">
                                    Nro. Comprobante
                                </th>
                                <th style="width:150px;">
                                    Venta al Credito
                                </th>
                                <th style="width:150;">
                                    Importe
                                </th>
                                <th style="width:50px;">
                                    &nbsp;
                                </th>
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="textoGris">
                                <%# String.Format(New System.Globalization.CultureInfo("es-ES"),"{0:d}",Eval("FECHA"))%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("TIPO_COMPROBANTE")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("NRO_COMPROBANTE")%>
                            </td>
                            <td class="textoGris">
                                <%# IIf(CBOOL(Eval("CREDITO")),"Si","No")%>
                            </td>
<td class="textoGris alinearDer ">
                                <%# Eval("IMPORTE")%>
                            </td>
                            <td class="alinearCentro">
                                
                                <asp:Button runat="server" ID="btnVer" Text="Ver" CommandArgument='<%# Eval("ID") %>' CommandName="ver" />
                                <asp:HiddenField runat="server" ID="hdTipo" Value='<%# Eval("TIPO") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
 <table class="generica" style="width:100%;">
                            <tr>
                                <th style="width:100px;">
                                    Fecha
                                </th>
                                <th>
                                    Tipo Comprobante
                                </th>
                                <th style="width:200px;">
                                    Nro. Comprobante
                                </th>
                                <th style="width:150px;">
                                    Credito
                                </th>
                                <th style="width:150;">
                                    Importe
                                </th>
                                <th style="width:50px;">
                                    &nbsp;
                                </th>
                            </tr>
                            <tr>
                                <td colspan="6" class="textoGris">
                                    No Existe Movimiento.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMovimientos" PageSize="20"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlMovimientos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM (
SELECT CL.ID_CLIENTE, V.FECHA, TC.DETALLE AS TIPO_COMPROBANTE,V.CREDITO,V.NRO_VENTA AS NRO_COMPROBANTE,V.TOTAL_PEDIDO AS IMPORTE,V.ID_VENTA AS ID, 1 AS TIPO,@CREDITO as pp  FROM VENTA V 
INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = V.ID_CLIENTE 
INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE 
INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
WHERE CL.ID_CLIENTE = @CLIENTE
UNION ALL
SELECT CL.ID_CLIENTE,PG.FECHA,'PAGO CREDITO',1,PG.ID_PAGO,PG.IMPORTE_PAGO,PG.ID_PAGO,2 AS TIPO,@CREDITO FROM CREDITO C 
INNER JOIN PAGO_CREDITO PC ON PC.ID_CREDITO = C.ID_CREDITO 
INNER JOIN PAGOS PG ON PG.ID_PAGO = PC.ID_PAGO 
INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE 
INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
WHERE CL.ID_CLIENTE = @CLIENTE
UNION ALL
SELECT CL.ID_CLIENTE,CONVERT(DATE,'01/03/2019',103) AS FECHA,'CREDITO SALDO INICIAL',1,C.ID_CREDITO,C.IMPORTE_CREDITO ,C.ID_CREDITO,3 AS TIPO ,@CREDITO FROM CREDITO C 
INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE 
INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
WHERE 
CL.ID_CLIENTE = @CLIENTE AND 
C.ID_VENTA IS NULL ) V
WHERE CONVERT(DATE,FECHA,103) >= @DESDE AND CONVERT(DATE,FECHA,103) <= @HASTA
AND CREDITO =  CASE WHEN @CREDITO = 'True'THEN @CREDITO ELSE CREDITO END 
ORDER BY FECHA">                        
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlCliente" Name="CLIENTE" 
                                PropertyName="SelectedValue" />
                            <asp:ControlParameter ControlID="txtDesde" Name="DESDE" PropertyName="Text" />
                            <asp:ControlParameter ControlID="txtHasta" Name="HASTA" PropertyName="Text" />
                            <asp:ControlParameter ControlID="chkCredito" Name="CREDITO" 
                                PropertyName="Checked" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
</ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    function iniciarFecha() {
        jQuery.noConflict();
        jQuery("#<%=txtDesde.ClientID %>").datepicker({});
        jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
    }
</script> 
</asp:Content>
