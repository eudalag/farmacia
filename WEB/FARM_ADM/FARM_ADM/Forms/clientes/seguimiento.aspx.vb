﻿Public Class seguimiento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub ddlGrupo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGrupo.SelectedIndexChanged
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
        lvwMovimientos.DataBind()
    End Sub

    Private Sub seguimiento_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            txtDesde.Text = Now.ToString("dd/MM/yyyy")
            txtHasta.Text = Now.ToString("dd/MM/yyyy")
        End If
    End Sub
End Class