﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="clientes.aspx.vb" Inherits="FARM_ADM.clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Clientes:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px; ">
             Buscar:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:50px; ">
             Grupo de Clientes:
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlGrupo" DataSourceID="sqlGrupo"
                DataTextField="GRUPO_PERSONAS" DataValueField="ID_GRUPO" AutoPostBack="true"></asp:DropDownList>
<asp:SqlDataSource runat="server" ID="sqlGrupo" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
SelectCommand="SELECT ID_GRUPO, GRUPO_PERSONAS FROM GRUPO_CLIENTES 
UNION ALL SELECT -1, '[Todos los Grupos]'
ORDER BY GRUPO_PERSONAS"></asp:SqlDataSource>
         </td>
     </tr>
    <tr>
         <td colspan="2" class="alinearDer">
             <asp:Button runat="server" ID="a_btnNuevo" Text="Nuevo" />
         </td>
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwBaseDatos" DataSourceID="sqlBaseDatos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Cliente
                                </th>
                                <th style="width:100px;">
                                    Nit
                                </th>
                                <th style="width:100px;">
                                    Celular
                                </th>
                                <th style="width:230px;">
                                    Grupo
                                </th>
 <th style="width:100;">
                                    Credito
                                </th>
 <th style="width:80px;">
                                    Total Credito
                                </th>
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("NOMBRE_APELLIDOS")%>' CommandName="modificar" CommandArgument='<%# Eval("ID_CLIENTE")%>'></asp:LinkButton>
                            </td>
                            <td class="textoGris">
                                <%# Eval("NIT")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("CELULAR")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("GRUPO_PERSONAS")%>
                            </td>
<td class="textoGris alinearCentro ">
                                <%# Eval("CREDITO")%>
                            </td>
<td class="textoGris alinearDer ">
                                <%# Eval("TOTAL_CREDITO")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwBaseDatos" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlBaseDatos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT  CL.ID_CLIENTE,P.NOMBRE_APELLIDOS,Cl.NIT,GC.GRUPO_PERSONAS,CASE WHEN ISNULL(Cl.CREDITO,0) = 0 THEN 'No' ELSE 'Si' END AS CREDITO
                                    , P.CELULAR, ISNULL(LIMITE_CREDITO,0) - ISNULL(TOTAL_CREDITO, 0) AS TOTAL_CREDITO FROM CLIENTE CL
INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA 
INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO  
LEFT JOIN 
(
SELECT SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,C.ID_CLIENTE    FROM CREDITO C
LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P 
INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO 
GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO 
WHERE ESTADO = 0
GROUP BY C.ID_CLIENTE) C ON C.ID_CLIENTE = CL.ID_CLIENTE
WHERE cL.ID_CLIENTE <> 1 AND ([NOMBRE_APELLIDOS] LIKE '%' + @NOMBRE + '%' OR NIT LIKE '%' + @NOMBRE + '%' ) AND GC.ID_GRUPO = CASE WHEN @GRUPO = -1 THEN GC.ID_GRUPO ELSE @GRUPO END

                        ORDER BY NOMBRE_APELLIDOS">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="ddlGrupo" DefaultValue="" Name="GRUPO" 
                                PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
</ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
