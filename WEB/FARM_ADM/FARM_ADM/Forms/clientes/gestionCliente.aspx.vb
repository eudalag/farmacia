﻿Public Class gestionCliente
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            guardarCliente()
            Response.Redirect("clientes.aspx")
        End If
    End Sub
    Protected Sub guardarCliente()
        Dim tbl = New mcTabla()
        tbl.tabla = "CLIENTE"
        tbl.campoID = "ID_CLIENTE"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_PERSONA", guardarPersona())
        tbl.agregarCampoValor("NIT", txtNit.Text)
        tbl.agregarCampoValor("NOMBRE_FACTURA", txtNombreFactura.Text)
        tbl.agregarCampoValor("REFERENCIA_EMPRESA", txtEmpresa.Text)
        tbl.agregarCampoValor("LIMITE_CREDITO", Decimal.Parse(txtLimite.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("OBSERVACIONES", txtObservacion.Text)
        tbl.agregarCampoValor("CREDITO", CBool(chkCredito.Checked))
        tbl.agregarCampoValor("ID_GRUPO", ddlGrupo.SelectedValue)
        If Request.QueryString("ID") Is Nothing Then
            tbl.insertar()
        Else
            tbl.valorID = Request.QueryString("ID")
            tbl.modificar()
        End If
        'If Decimal.Parse(txtEstado.Text, New Globalization.CultureInfo("en-US")) > 0 Then
        '    cargarEstadoCliente(tbl.valorID)
        'End If
    End Sub
    Function guardarPersona() As Integer
        Dim tbl = New mcTabla()
        tbl.tabla = "PERSONA"
        tbl.modoID = "auto"
        tbl.campoID = "ID_PERSONA"
        tbl.agregarCampoValor("NOMBRE_APELLIDOS", txtNombreApellido.Text)
        tbl.agregarCampoValor("DIRECCION", txtDireccion.Text)
        tbl.agregarCampoValor("CELULAR", txtCelular.Text)
        tbl.agregarCampoValor("CORREO", txtCorreo.Text)
        tbl.agregarCampoValor("NRO_CI", txtCarnetIdentidad.Text)
        If CInt(hdPersona.Value) > 0 Then
            tbl.valorID = CInt(hdPersona.Value)
            tbl.modificar()
        Else
            tbl.insertar()
        End If
        Return tbl.valorID()
    End Function
    Protected Sub cargarEstadoCliente(ByVal idCliente As Integer)
        Dim tbl = New mcTabla()
        tbl.tabla = "CREDITO"
        tbl.modoID = "sql"
        tbl.campoID = "ID_CREDITO"
        tbl.agregarCampoValor("ID_CLIENTE", idCliente)
        tbl.agregarCampoValor("ID_VENTA", "null")
        tbl.agregarCampoValor("IMPORTE_CREDITO", Decimal.Parse(txtEstado.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("ESTADO", 0)
        tbl.agregarCampoValor("OBSERVACION", "Saldo Inicial, Credito Anterior Sistema")
        tbl.agregarCampoValor("TIPO", "2")
        tbl.insertar()
    End Sub

    Private Sub gestionCliente_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then        
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If            
        End If
    End Sub
    Protected Sub cargarDatos(ByVal ID As Integer)
        Dim sSql As String = ""

        sSql += " DECLARE @ID_CLIENTE AS INTEGER;"
        sSql += " SET @ID_CLIENTE = " + ID.ToString + ";"
        sSql += " SELECT  CL.ID_PERSONA,ISNULL(P.NRO_CI,'') AS NRO_CI ,ISNULL(NIT,'') AS NIT,ISNULL(NOMBRE_FACTURA,'') AS NOMBRE_FACTURA,ISNULL(REFERENCIA_EMPRESA,'') AS REFERENCIA_EMPRESA,ISNULL(REFERENCIA_EMPRESA,'') AS NIT,"
        sSql += " ISNULL(OBSERVACIONES,'') AS OBSERVACIONES,ISNULL(CREDITO,0) AS CREDITO,ISNULL(CL.ID_GRUPO,1) AS ID_GRUPO,ISNULL(P.CORREO,'') AS CORREO,"
        sSql += " CL.ID_CLIENTE, P.CELULAR,ISNULL(P.DIRECCION,'') AS DIRECCION ,CL.NIT, P.NOMBRE_APELLIDOS, ISNULL(TOTAL_CREDITO, 0) TOTAL_CREDITO , ISNULL(LIMITE_CREDITO,0) AS LIMITE_CREDITO, ISNULL(LIMITE_CREDITO,0) - ISNULL(TOTAL_CREDITO, 0) AS SALDO FROM CLIENTE CL"
        sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO  "
        sSql += " LEFT JOIN "
        sSql += " ("
        sSql += " SELECT SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,C.ID_CLIENTE    FROM CREDITO C"
        sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
        sSql += " WHERE ESTADO = 0"
        sSql += " GROUP BY C.ID_CLIENTE) C ON C.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " WHERE CL.ID_CLIENTE = @ID_CLIENTE"

        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtNombreApellido.Text = esNulo(tbl.Rows(0)("NOMBRE_APELLIDOS"))
            txtCarnetIdentidad.Text = esNulo(tbl.Rows(0)("NRO_CI"))
            txtCelular.Text = esNulo(tbl.Rows(0)("CELULAR"))
            txtDireccion.Text = esNulo(tbl.Rows(0)("DIRECCION"))
            txtEmpresa.Text = esNulo(tbl.Rows(0)("REFERENCIA_EMPRESA"))
            txtCorreo.Text = esNulo(tbl.Rows(0)("CORREO"))
            txtNit.Text = esNulo(tbl.Rows(0)("NIT"))
            txtNombreFactura.Text = esNulo(tbl.Rows(0)("NOMBRE_FACTURA"))
            chkCredito.Checked = CBool(esNulo(tbl.Rows(0)("CREDITO"), 0))
            txtLimite.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:0.00}", esNulo(tbl.Rows(0)("LIMITE_CREDITO"), 0))
            ddlGrupo.DataBind()
            ddlGrupo.SelectedValue = esNulo(tbl.Rows(0)("ID_GRUPO"), 1)
            txtObservacion.Text = esNulo(tbl.Rows(0)("OBSERVACIONES"))
            hdPersona.Value = tbl.Rows(0)("ID_PERSONA")
            txtEstado.Text = tbl.Rows(0)("SALDO")
        Else
            hdPersona.Value = "-1"
        End If
    End Sub
End Class