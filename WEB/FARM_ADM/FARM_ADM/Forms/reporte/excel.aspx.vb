﻿Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports ClosedXML.Excel

Public Class excel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sSql As String = ""
        Select Case Request.QueryString("TIPO")
            Case "VENTA"
                sSql = " "
                sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE;"
                sSql += " SET @DESDE = CONVERT(DATE,'" + Request.QueryString("DESDE") + "',103);"
                sSql += " SET @HASTA = CONVERT(DATE,'" + Request.QueryString("HASTA") + "',103);"
                sSql += " SELECT "
                sSql += " 3 AS ESP,"
                sSql += " ROW_NUMBER() OVER (ORDER BY NRO_VENTA) AS NRO, "
                sSql += " F.FECHA,V.FECHA_AUD , F.NRO_FACTURA AS NRO_VENTA,"
                sSql += " D.NUMERO_AUTORIZACION,V.ANULADA,"
                sSql += " CASE WHEN F.ANULADO  = 1 THEN 'A' ELSE 'V' END AS ESTADO,"
                sSql += " CL.NIT ,P.NOMBRE_APELLIDOS ,"
                sSql += " F.TOTAL_FACTURA AS TOTAL_PEDIDO,V.TOTAL_DESCUENTO, F.TOTAL_FACTURA - V.TOTAL_DESCUENTO AS IMPORTE_DEBITO, (V.TOTAL_PEDIDO - V.TOTAL_DESCUENTO) * 0.13 AS DEBITO_FISCAL,F.CODIGO_CONTROL "
                sSql += " FROM VENTA V"
                sSql += " INNER JOIN FACTURA F ON F.ID_VENTA = V.ID_VENTA"
                sSql += " INNER JOIN DOSIFICACION D ON D.ID_DOSIFICACION = F.ID_DOSIFICACION "
                sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = F.ID_CLIENTE "
                sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
                sSql += " WHERE CONVERT(DATE,F.FECHA,103) >= @DESDE AND CONVERT(DATE,F.FECHA,103) <= @HASTA "
                Dim tblVentas As DataTable = New mcTabla().ejecutarConsulta(sSql)
                exportarlibroVenta(tblVentas)
            Case "INFORME_VENTA"
                sSql += " DECLARE @BANCO AS INTEGER, @PAGO AS INTEGER, @COMPROBANTE AS INTEGER, @RECETA AS BIT,@DESDE AS DATE, @HASTA AS DATE;"
                sSql += " SET @BANCO = " + Request.QueryString("CAJA") + ";"
                sSql += " SET @PAGO = " + Request.QueryString("PAGO") + ";"
                sSql += " SET @COMPROBANTE = " + Request.QueryString("COMPROBANTE") + "; "
                sSql += " SET @RECETA = " + IIf(Request.QueryString("RECETA") = "true", "1", "0") + ";"
                sSql += " SET @DESDE = CONVERT(DATE,'" + Request.QueryString("DESDE") + "',103);"
                sSql += " SET @HASTA = CONVERT(DATE,'" + Request.QueryString("HASTA") + "',103);"
                sSql += " SELECT * FROM ("
                sSql += " SELECT V.FECHA,TC.ID_TIPO_COMPROBANTE ,TC.DETALLE AS TIPO_COMPROBANTE,V.CREDITO, CASE WHEN V.CREDITO = 1 THEN 'CREDITO' ELSE 'EFECTIVO' END AS TIPO_PAGO,"
                sSql += " CASE WHEN V.CREDITO = 1 THEN PCR.NOMBRE_APELLIDOS  ELSE P.NOMBRE_APELLIDOS END AS NOMBRE_APELLIDOS,V.NRO_VENTA,V.TOTAL_PEDIDO,V.ID_VENTA,B.ID_BANCO,V.ID_RECETA , 1 AS TIPO FROM VENTA V "
                sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE =V.ID_TIPO_COMPROBANTE "
                sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = V.ID_CLIENTE "
                sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
                sSql += " INNER JOIN BANCO B ON B.ID_BANCO = V.ID_BANCO"
                sSql += " LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
                sSql += " LEFT JOIN CLIENTE CLC ON CLC.ID_CLIENTE = CR.ID_CLIENTE"
                sSql += " LEFT JOIN PERSONA PCR ON PCR.ID_PERSONA = CLC.ID_PERSONA  "
                sSql += " WHERE V.ANULADA = 0"
                sSql += " UNION ALL"
                sSql += " SELECT CONVERT(DATE,FH_MOVIMIENTO,103) AS FECHA,3, 'OTRO INGRESO' AS TIPO_COMPROBANTE,0,'EFECTIVO' AS TIPO_PAGO,"
                sSql += " M.GLOSA,M.ID_MOVIMIENTO,M.IMPORTE,M.ID_MOVIMIENTO, B.ID_BANCO,NULL,2 AS TIPO FROM MOVIMIENTO_BANCO M"
                sSql += " INNER JOIN BANCO B ON B.ID_BANCO = M.ID_BANCO "
                sSql += " WHERE ID_TIPO_MOVIMIENTO = 1) VENTA "
                sSql += " WHERE ID_BANCO = CASE WHEN @BANCO = -1 THEN ID_BANCO ELSE @BANCO END"
                sSql += " AND ISNULL(CREDITO,0) = CASE WHEN @PAGO = -1 THEN CREDITO WHEN @PAGO = 1 THEN 0 ELSE 1 END"
                sSql += " AND ID_TIPO_COMPROBANTE  = CASE WHEN @COMPROBANTE = -1 THEN ID_TIPO_COMPROBANTE ELSE @COMPROBANTE END"
                sSql += " AND CASE WHEN @RECETA = 'True' THEN ISNULL(ID_RECETA,0) ELSE 0 END  = CASE WHEN @RECETA = 'True' THEN ISNULL(ID_RECETA,1) ELSE 0 END"
                sSql += " AND CONVERT(DATE,FECHA,103) >= @DESDE AND CONVERT(DATE,FECHA,103) <= @HASTA"
                Dim tblVentas As DataTable = New mcTabla().ejecutarConsulta(sSql)
                exportarInformeVentas(tblVentas)
            Case "CUENTAS_COBRAR"
                sSql = " DECLARE @GRUPO AS INTEGER, @CLIENTE AS NVARCHAR(MAX);"
                sSql += " SET @GRUPO = " + Request.QueryString("GRUPO") + ";"
                sSql += " SET @CLIENTE = '" + Request.QueryString("CLIENTE") + "';"
                sSql += " SELECT * FROM ("
                sSql += " SELECT CL.ID_CLIENTE, ISNULL(P.CELULAR,'') AS CELULAR,ISNULL(CL.NIT,'') AS NIT, P.NOMBRE_APELLIDOS, SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,GC.GRUPO_PERSONAS   FROM CREDITO C"
                sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
                sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
                sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
                sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
                sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
                sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
                sSql += " WHERE ESTADO = 0 AND GC.ID_GRUPO = CASE WHEN @GRUPO = -1 THEN GC.ID_GRUPO ELSE @GRUPO END"
                sSql += " AND P.NOMBRE_APELLIDOS LIKE '%' + @CLIENTE + '%'"
                sSql += " GROUP BY CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, GC.GRUPO_PERSONAS ) C      "
                sSql += " WHERE C.TOTAL_CREDITO > 0     "
                sSql += " ORDER BY NOMBRE_APELLIDOS "
                Dim tblVentas As DataTable = New mcTabla().ejecutarConsulta(sSql)
                exportarInformeCredito(tblVentas)
            Case "CUENTAS_PAGAR"
                sSql += " DECLARE @BUSCAR AS NVARCHAR(MAX);"
                sSql += " SET @BUSCAR = '';"
                sSql += " SELECT P.ID_PROVEEDOR,CC.ID_COMPRA_CREDITO  ,P.NOMBRE,TC.DETALLE,C.NRO_COMPROBANTE,"
                sSql += " CONVERT(NVARCHAR(10),CC.F_VENCIMIENTO,103) AS F_VENCIMIENTO,CC.SALDO  FROM COMPRA_CREDITO CC "
                sSql += " INNER JOIN COMPRA C ON C.ID_COMPRA = CC.ID_COMPRA "
                sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR "
                sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE "
                sSql += " WHERE NOMBRE LIKE '%' + @BUSCAR + '%' AND SALDO > 0"
                sSql += " ORDER BY F_vENCIMIENTO DESC "

                Dim tblVentas As DataTable = New mcTabla().ejecutarConsulta(sSql)
                exportarInformeCreditoCompra(tblVentas)
        End Select
        Response.Clear()
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.ContentType = "application/xls"
        Response.Charset = ""
        'Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", "DEUDAS_TESORERIA"))
        Me.EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        Dim dg As New DataGrid
        dg.DataSource = CType(New mcTabla().ejecutarConsulta(sSql), DataTable)
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        Response.Flush()
        Response.Write(stringWrite.ToString())
        Response.End()
    End Sub

    Private Sub exportarInformeVentas(tblVentas As DataTable)

        Dim wb As New XLWorkbook()
        Dim memStream As New MemoryStream()
        Dim ws = wb.Worksheets.Add("VENTAS")
        Dim i As Integer = 1
        Dim pos As String = "0"
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("Fecha")
        ws.Cell("B" & pos).SetValue("Tipo Comprobante")
        ws.Cell("C" & pos).SetValue("Tipo Pago")
        ws.Cell("D" & pos).SetValue("Nombre y Apellidos")
        ws.Cell("E" & pos).SetValue("Nro. Comprobante")
        ws.Cell("F" & pos).SetValue("Total")
        Dim rngTable2
        rngTable2 = ws.Range("A" & pos & ":F" & pos)
        rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        rngTable2.SetAutoFilter()
        i = i + 1

        For Each item In tblVentas.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(item("FECHA"))
            ws.Cell("A" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("A" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("B" & pos).SetValue(item("TIPO_COMPROBANTE"))
            ws.Cell("B" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("B" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("C" & pos).SetValue(item("TIPO_PAGO"))
            ws.Cell("C" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("C" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("D" & pos).SetValue(item("NOMBRE_APELLIDOS"))
            ws.Cell("D" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("D" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("E" & pos).SetValue(item("NRO_VENTA"))
            ws.Cell("E" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("E" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("F" & pos).SetValue(item("TOTAL_PEDIDO"))
            ws.Cell("F" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("F" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin
            ws.Cell("F" & pos).Style.Border.RightBorder = XLBorderStyleValues.Thin

            i = i + 1
        Next        
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("TOTAL").Style.Font.SetBold(True)
        ws.Cell("A" & pos).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
        ws.Cell("A" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        ws.Range("A" & pos & ":E" & pos).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin

        ws.Cell("F" & pos).SetFormulaA1("=SUM(F2:F" + (CInt(pos) - 1).ToString + ")").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
        ws.Cell("F" & pos).Style.Font.SetBold(True)
        ws.Cell("F" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)

        ws.ColumnsUsed.AdjustToContents()
        ws.Column("A").Style.DateFormat.Format = "##/##/####"
        ws.Column("F").Style.NumberFormat.Format = "#,##0.00"
        'ws.Column("E").Width = 30
        ws.ColumnsUsed.AdjustToContents()
        wb.SaveAs(memStream)
        Dim context As HttpContext = HttpContext.Current

        With context.Response
            .Clear()
            .ContentType = "application/xls"
            Dim archivo = "VENTAS_" & Date.Parse(Request.QueryString("HASTA"), New Globalization.CultureInfo("es-ES")).ToString("MMyyyy")
            .AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", archivo))
            .Charset = Encoding.Unicode.WebName
            .ContentEncoding = Encoding.Unicode
            .BinaryWrite(memStream.ToArray)
            memStream.Close()
            .End()
        End With
    End Sub
    Private Sub exportarInformeCredito(tblVentas As DataTable)

        Dim wb As New XLWorkbook()
        Dim memStream As New MemoryStream()
        Dim ws = wb.Worksheets.Add("CREDITOS VENTAS")
        Dim i As Integer = 1
        Dim pos As String = "0"
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("Cliente")
        ws.Cell("B" & pos).SetValue("Celular")
        ws.Cell("C" & pos).SetValue("Grupo de Cliente")
        ws.Cell("D" & pos).SetValue("Importe")

        Dim rngTable2
        rngTable2 = ws.Range("A" & pos & ":D" & pos)
        rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        rngTable2.SetAutoFilter()
        i = i + 1

        For Each item In tblVentas.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(item("NOMBRE_APELLIDOS"))
            ws.Cell("A" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("A" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("B" & pos).SetValue(item("CELULAR"))
            ws.Cell("B" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("B" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("C" & pos).SetValue(item("GRUPO_PERSONAS"))
            ws.Cell("C" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("C" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("D" & pos).SetValue(item("TOTAL_CREDITO"))
            ws.Cell("D" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("D" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin
            ws.Cell("D" & pos).Style.Border.RightBorder = XLBorderStyleValues.Thin

            i = i + 1
        Next
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("TOTAL").Style.Font.SetBold(True)
        ws.Cell("A" & pos).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
        ws.Cell("A" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        ws.Range("A" & pos & ":C" & pos).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin

        ws.Cell("D" & pos).SetFormulaA1("=SUM(D2:D" + (CInt(pos) - 1).ToString + ")").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
        ws.Cell("D" & pos).Style.Font.SetBold(True)
        ws.Cell("D" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)

        ws.ColumnsUsed.AdjustToContents()
        ws.Column("D").Style.NumberFormat.Format = "#,##0.00"
        'ws.Column("E").Width = 30
        ws.ColumnsUsed.AdjustToContents()
        wb.SaveAs(memStream)
        Dim context As HttpContext = HttpContext.Current

        With context.Response
            .Clear()
            .ContentType = "application/xls"
            Dim archivo = "CREDITO_VENTA"
            .AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", archivo))
            .Charset = Encoding.Unicode.WebName
            .ContentEncoding = Encoding.Unicode
            .BinaryWrite(memStream.ToArray)
            memStream.Close()
            .End()
        End With
    End Sub
    Private Sub exportarInformeCreditoCompra(tblVentas As DataTable)

        Dim wb As New XLWorkbook()
        Dim memStream As New MemoryStream()
        Dim ws = wb.Worksheets.Add("CREDITOS COMPRA")
        Dim i As Integer = 1
        Dim pos As String = "0"
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("Proveedor")
        ws.Cell("B" & pos).SetValue("Tipo Comprobante")
        ws.Cell("C" & pos).SetValue("Nro. Comprobante")
        ws.Cell("D" & pos).SetValue("Importe")

        Dim rngTable2
        rngTable2 = ws.Range("A" & pos & ":D" & pos)
        rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        rngTable2.SetAutoFilter()
        i = i + 1

        For Each item In tblVentas.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(item("NOMBRE"))
            ws.Cell("A" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("A" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("B" & pos).SetValue(item("DETALLE"))
            ws.Cell("B" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("B" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("C" & pos).SetValue(item("NRO_COMPROBANTE"))
            ws.Cell("C" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("C" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin

            ws.Cell("D" & pos).SetValue(item("SALDO"))
            ws.Cell("D" & pos).Style.Border.LeftBorder = XLBorderStyleValues.Thin
            ws.Cell("D" & pos).Style.Border.BottomBorder = XLBorderStyleValues.Thin
            ws.Cell("D" & pos).Style.Border.RightBorder = XLBorderStyleValues.Thin

            i = i + 1
        Next
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("TOTAL").Style.Font.SetBold(True)
        ws.Cell("A" & pos).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
        ws.Cell("A" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)
        ws.Range("A" & pos & ":C" & pos).Merge().Style.Border.OutsideBorder = XLBorderStyleValues.Thin

        ws.Cell("D" & pos).SetFormulaA1("=SUM(D2:D" + (CInt(pos) - 1).ToString + ")").Style.Border.OutsideBorder = XLBorderStyleValues.Thin
        ws.Cell("D" & pos).Style.Font.SetBold(True)
        ws.Cell("D" & pos).Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)

        ws.ColumnsUsed.AdjustToContents()
        ws.Column("D").Style.NumberFormat.Format = "#,##0.00"
        'ws.Column("E").Width = 30
        ws.ColumnsUsed.AdjustToContents()
        wb.SaveAs(memStream)
        Dim context As HttpContext = HttpContext.Current

        With context.Response
            .Clear()
            .ContentType = "application/xls"
            Dim archivo = "CREDITO_COMPRA"
            .AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", archivo))
            .Charset = Encoding.Unicode.WebName
            .ContentEncoding = Encoding.Unicode
            .BinaryWrite(memStream.ToArray)
            memStream.Close()
            .End()
        End With
    End Sub

    Private Sub exportarlibroVenta(tblVentas As DataTable)
        Dim wb As New XLWorkbook()
        Dim memStream As New MemoryStream()
        Dim ws = wb.Worksheets.Add("VENTAS")
        Dim i As Integer = 1
        Dim pos As String = "0"
        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("ESP")
        ws.Cell("B" & pos).SetValue("NRO")
        ws.Cell("C" & pos).SetValue("FECHA FACTURA")
        ws.Cell("D" & pos).SetValue("Nº DE FACTURA")
        ws.Cell("E" & pos).SetValue("AUTORIZACION")
        ws.Cell("F" & pos).SetValue("ESTADO")
        ws.Cell("G" & pos).SetValue("NIT/CI CLIENTE")
        ws.Cell("H" & pos).SetValue("NOMBRE O RAZON SOCIAL")
        ws.Cell("I" & pos).SetValue("IMPORTE TOTAL DE VENTA (A)")
        ws.Cell("J" & pos).SetValue("IMPORTE ICE/IEHD/TASAS    (B)")
        ws.Cell("K" & pos).SetValue("EXPORTACIONES Y OPERACIONES EXTENTAS (C) ")
        ws.Cell("L" & pos).SetValue("VENTAS GRAVADAS TASA CERO (D)")
        ws.Cell("M" & pos).SetValue("SUB TOTAL (E)=(A-B-C-D)")
        ws.Cell("N" & pos).SetValue("DSCTOS Y BONIFICAC, REBAJAS OTORGADAS (F)")
        ws.Cell("O" & pos).SetValue("IMPORTE BASE PARA DEBITO FISCAL (G)=(E-F)")
        ws.Cell("P" & pos).SetValue("DEBITO FISCAL(H)=(G*13%)")
        ws.Cell("Q" & pos).SetValue("CODIGO DE CONTROL")
        Dim rngTable2
        rngTable2 = ws.Range("A" & pos & ":Q" & pos)
        rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(255, 217, 102)).Font.SetBold(True)

        i = i + 1

        For Each item In tblVentas.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(item("ESP"))
            ws.Cell("B" & pos).SetValue(item("NRO"))
            ws.Cell("C" & pos).SetValue(item("FECHA"))
            ws.Cell("D" & pos).SetValue(item("NRO_VENTA"))
            ws.Cell("E" & pos).SetValue(item("NUMERO_AUTORIZACION"))
            ws.Cell("F" & pos).SetValue(item("ESTADO"))
            ws.Cell("G" & pos).SetValue(IIf(item("ANULADA"), "0", item("NIT")))
            ws.Cell("H" & pos).SetValue(IIf(item("ANULADA"), "ANULADA", item("NOMBRE_APELLIDOS")))

            ws.Cell("I" & pos).SetValue(IIf(item("ANULADA"), 0, Decimal.Parse(item("TOTAL_PEDIDO"), New Globalization.CultureInfo("en-US"))))
            ws.Cell("J" & pos).SetValue(0)
            ws.Cell("K" & pos).SetValue(0)
            ws.Cell("L" & pos).SetValue(0)
            ws.Cell("M" & pos).SetValue(IIf(item("ANULADA"), 0, Decimal.Parse(item("TOTAL_PEDIDO"), New Globalization.CultureInfo("en-US"))))
            ws.Cell("N" & pos).SetValue(IIf(item("ANULADA"), 0, Decimal.Parse(item("TOTAL_DESCUENTO"), New Globalization.CultureInfo("en-US"))))
            ws.Cell("O" & pos).SetValue(IIf(item("ANULADA"), 0, Decimal.Parse(item("IMPORTE_DEBITO"), New Globalization.CultureInfo("en-US"))))
            ws.Cell("P" & pos).SetValue(IIf(item("ANULADA"), 0, Decimal.Parse(item("DEBITO_FISCAL"), New Globalization.CultureInfo("en-US"))))
            ws.Cell("Q" & pos).SetValue(IIf(item("ANULADA"), "", item("CODIGO_CONTROL")))
            i = i + 1
        Next

        ws.ColumnsUsed.AdjustToContents()

        'ws.Column("E").Width = 30
        wb.SaveAs(memStream)
        Dim context As HttpContext = HttpContext.Current

        With context.Response
            .Clear()
            .ContentType = "application/xls"
            Dim archivo = "VENTAS_" & Date.Parse(Request.QueryString("HASTA"), New Globalization.CultureInfo("es-ES")).ToString("MMyyyy") & "_4149110018"
            .AppendHeader("Content-Disposition", String.Format("attachment; filename={0}.xls", archivo))
            .Charset = Encoding.Unicode.WebName
            .ContentEncoding = Encoding.Unicode
            .BinaryWrite(memStream.ToArray)
            memStream.Close()
            .End()
        End With
    End Sub
End Class