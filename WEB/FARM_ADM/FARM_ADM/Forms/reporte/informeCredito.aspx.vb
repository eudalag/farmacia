﻿Imports Microsoft.Reporting.WebForms

Public Class informeCredito
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Function obtenerEstadoDeudas() As DataTable
        Dim sSql As String = ""        

        sSql += " DECLARE @GRUPO AS INTEGER,@DESDE AS DATE, @HASTA AS DATE;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + txtDesde.Text + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + txtHasta.Text + "',103);"
        sSql += " SET @GRUPO = " + ddlGrupoEconomico.SelectedValue + ";"
        sSql += " SELECT * FROM ("
        sSql += " SELECT CL.ID_CLIENTE, P.CELULAR,CL.NIT,P.NRO_CI, P.NOMBRE_APELLIDOS, SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,GC.GRUPO_PERSONAS,GC.ID_GRUPO    FROM CREDITO C"
        sSql += " INNER JOIN VENTA  V ON V.ID_VENTA = C.ID_VENTA "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
        sSql += " WHERE ESTADO = 0 AND"
        sSql += " CONVERT(DATE,V.FECHA,103) >= @DESDE AND CONVERT(DATE,V.FECHA,103) <= @HASTA"
        sSql += " GROUP BY CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, GC.GRUPO_PERSONAS,GC.ID_GRUPO,P.NRO_CI  )"
        sSql += " C WHERE C.TOTAL_CREDITO > 0 AND ID_GRUPO = @GRUPO"
        sSql += " ORDER BY NOMBRE_APELLIDOS "
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        Return tbl
    End Function

    'Protected Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
    '    Dim tbl = obtenerEstadoDeudas()
    '    rptViewer.Reset()
    '    rptViewer.Height = 800
    '    rptViewer.Width = 650
    '    Dim param As New List(Of Microsoft.Reporting.WebForms.ReportParameter)
    '    Dim fechaDesde As New Microsoft.Reporting.WebForms.ReportParameter("desde", txtDesde.Text, False)
    '    Dim fechaHasta As New Microsoft.Reporting.WebForms.ReportParameter("hasta", txtHasta.Text, False)
    '    param.Add(fechaDesde)
    '    param.Add(fechaHasta)
    '    Dim rds As ReportDataSource = New ReportDataSource()
    '    rds.Name = "tblCliente"
    '    rds.Value = tbl
    '    'Dim RDS As New Microsoft.Reporting.WebForms.ReportDataSource("dsFactura_TBLCOMPRA", tbl)
    '    rptViewer.LocalReport.DataSources.Add(rds)
    '    rptViewer.LocalReport.ReportPath = "Forms/reporte/credito/rptCredito.rdlc"
    '    rptViewer.LocalReport.SetParameters(param)
    '    rptViewer.LocalReport.Refresh()
    '    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fecha", "iniciarFecha();", True)
    'End Sub
End Class