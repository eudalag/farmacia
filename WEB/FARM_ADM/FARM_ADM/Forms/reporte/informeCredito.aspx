﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="informeCredito.aspx.vb" Inherits="FARM_ADM.informeCredito" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#<%=txtDesde.ClientID %>").datepicker({});
            jQuery("#<%=txtHasta.ClientID %>").datepicker({});
        });

        window.addEvent("domready", function () {
            var txtDesde = new Mascara("<%=txtDesde.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
            var txtHasta = new Mascara("<%=txtHasta.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': false });
        }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" ScriptMode="Release" runat="server">
      </asp:ScriptManager>
  <div>
      <div class="alinearIzq"> 
      <asp:Label ID="Label2" runat="server" Text="Reporte Creditos:" CssClass="tituloGris"> 
      </asp:Label> 
      <hr align="left" style="height:-12px;color:#CCCCCC" width="30%" /> 
      <br /> 
    </div>
    <table width="60%" cellspacing="10px">
      <tr>
        <td style="width:50px;"> 
          <asp:Label ID="Label4" runat="server" Text="Grupo Economico:"></asp:Label></td>
        <td  colspan="3"> 
          <asp:DropDownList ID="ddlGrupoEconomico" runat="server" DataSourceID="sqlGrupo" 
        AppendDataBoundItems="true" DataTextField="GRUPO_PERSONAS" DataValueField="ID_GRUPO"> 
          <asp:ListItem Value="-1" Text="[Seleccione Grupo Economico]"></asp:ListItem>
          </asp:DropDownList> 
            <asp:SqlDataSource ID="sqlGrupo" runat="server" 
            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
            SelectCommand="SELECT [ID_GRUPO], [GRUPO_PERSONAS] FROM [GRUPO_CLIENTES]">
        </asp:SqlDataSource> 
        </td>
      </tr> 
<tr>
        <td class="textoGris" style="width:100px; ">
            Desde:
        </td>
        <td style="width:130px; ">
            <asp:TextBox runat="server" ID="txtDesde"  style="width:80px;"></asp:TextBox>
        </td>
        <td class="textoGris" style="width:50px; ">
            Hasta:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtHasta"  style="width:80px;"></asp:TextBox>            
        </td>
    </tr>     
            <tr>
        <td align="right" colspan="4"> 
            <asp:Button ID="btnGenerar" runat="server" Text="Generar Reporte" OnClientClick="return verReporte();" />
                <asp:Button  ID="btnCancelar" runat="server" PostBackUrl="~/Forms/Sistema.aspx" Text="Cancelar" CausesValidation="False"></asp:button> 
 
        </td>
      </tr>
     
    </table>
  </div>
<script type="text/javascript">
    function verReporte() {
        window.open("visualizadorReporte.aspx?TIPO=VENTA&DESDE=" + jQuery("#<%=txtDesde.ClientID %>").val() + "&HASTA=" + jQuery("#<%=txtHasta.ClientID %>").val() + "&GRUPO=" + jQuery("#<%=ddlGrupoEconomico.ClientID %>").val());
        return false;
     }
</script>
</asp:Content>
