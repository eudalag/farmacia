﻿Public Class vademecun
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionVademecun.aspx")
    End Sub

    Protected Sub lvwBaseDatos_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwMedicamentos.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionVademecun.aspx?ID=" + e.CommandArgument)
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Session("VADEMECUM") = ddlLaboratorio.SelectedValue.ToString + ";" + txtBuscar.Text
        lvwMedicamentos.DataBind()
    End Sub

    Private Sub vademecun_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Session("VADEMECUM") Is Nothing Or Not Session("VADEMECUM") = "" Then
                ddlLaboratorio.DataBind()
                Dim Cadena = Session("VADEMECUM").ToString.Split(";")
                ddlLaboratorio.SelectedValue = Cadena(0)
                txtBuscar.Text = Cadena(1)
            End If
        End If
    End Sub
End Class