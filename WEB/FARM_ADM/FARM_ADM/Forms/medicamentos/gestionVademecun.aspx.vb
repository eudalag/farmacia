﻿Public Class gestionVademecun
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then            
        Dim tblMedicamento = New mcTabla()
            tblMedicamento.tabla = "MEDICAMENTO"
            tblMedicamento.campoID = "ID_MEDICAMENTO"
            tblMedicamento.modoID = "auto"
            tblMedicamento.agregarCampoValor("NOMBRE", txtMedicamento.Text)
            tblMedicamento.agregarCampoValor("COMPOSICION", txtComposicion.Text)
            tblMedicamento.agregarCampoValor("ID_TIPO_MEDICAMENTO", ddlFormaFarmaceutica.SelectedValue)
            tblMedicamento.agregarCampoValor("ACTIVO", 1)
            tblMedicamento.agregarCampoValor("PRECIO_UNITARIO", Decimal.Parse(txtPrecioUnitario.Text, New Globalization.CultureInfo("en-US")))
            tblMedicamento.agregarCampoValor("CONTROLADO", chkControlado.Checked)
            tblMedicamento.agregarCampoValor("ID_LABORATORIO", ddlLaboratorio.Text)
            tblMedicamento.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
            tblMedicamento.agregarCampoValor("ACCION", txtAccionTerapeutica.Text)
            tblMedicamento.agregarCampoValor("MINIMO", CInt(txtMinimo.Text))
            tblMedicamento.agregarCampoValor("MAXIMO", CInt(txtMaximo.Text))
            tblMedicamento.agregarCampoValor("MARCA", txtMarca.Text)
            tblMedicamento.agregarCampoValor("OBSERVACION", txtObservacion.Text)

            tblMedicamento.agregarCampoValor("CANTIDAD_PAQUETE", CInt(txtCantidad.Text))            
            tblMedicamento.agregarCampoValor("COD_REG_SAN", txtRegistroSanitario.Text)
            tblMedicamento.agregarCampoValor("PRECIO_PAQUETE", Decimal.Parse(txtPrecioPaquete.Text, New Globalization.CultureInfo("en-US")))
            tblMedicamento.agregarCampoValor("CODIGO_BARRA", txtCodigoBarra.Text)
            tblMedicamento.agregarCampoValor("PRECIO_COMPRA", Decimal.Parse(txtPrecio.Text, New Globalization.CultureInfo("en-US")))
            If Not Request.QueryString("ID") Is Nothing Then
                tblMedicamento.valorID = Request.QueryString("ID")
                tblMedicamento.modificar()
            Else
                tblMedicamento.insertar()
            End If
            gestionPrecios(tblMedicamento.valorID)
            Response.Redirect("vademecun.aspx")
        End If
    End Sub

    Protected Sub gestionPrecios(ByVal idMedicamento As Integer)
        Dim precio1 As Decimal = Decimal.Parse(txtPrecio1.Text, New Globalization.CultureInfo("en-US"))
        Dim precio2 As Decimal = Decimal.Parse(txtPrecio2.Text, New Globalization.CultureInfo("en-US"))
        Dim tbl = New mcTabla()
        tbl.tabla = "PRECIOS_EXTRAS"
        tbl.campoID = "ID_PRECIO"
        If CInt(hdPrecio1.Value) = -1 Then
            If precio1 > 0 Then                
                tbl.modoID = "auto"
                tbl.agregarCampoValor("ID_PRECIO_MEDICAMENTO", 1)
                tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                tbl.agregarCampoValor("PRECIO", precio1)
                tbl.insertar()
            End If
        Else
            tbl.valorID = hdPrecio1.Value
            If precio1 > 0 Then
                tbl.agregarCampoValor("PRECIO", precio1)
                tbl.modificar()
            Else
                tbl.eliminar()
            End If
        End If
        tbl.reset()
        tbl.tabla = "PRECIOS_EXTRAS"
        tbl.campoID = "ID_PRECIO"
        If CInt(hdPrecio2.Value) = -1 Then
            If precio2 > 0 Then
                tbl.modoID = "auto"
                tbl.agregarCampoValor("ID_PRECIO_MEDICAMENTO", 2)
                tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                tbl.agregarCampoValor("PRECIO", precio2)
                tbl.insertar()
            End If
        Else
            tbl.valorID = hdPrecio2.Value
            If precio2 > 0 Then
                tbl.agregarCampoValor("PRECIO", precio2)
                tbl.modificar()
            Else
                tbl.eliminar()
            End If
        End If
    End Sub
    Protected Sub cargarPrecio(ByVal idMedicamento As Integer)
        Dim sSql As String = "DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + idMedicamento.ToString + ";"
        sSql += " SELECT * FROM PRECIOS_EXTRAS WHERE ID_PRECIO_MEDICAMENTO = 1 AND ID_MEDICAMENTO = @ID;"
        sSql += " SELECT * FROM PRECIOS_EXTRAS WHERE ID_PRECIO_MEDICAMENTO = 2 AND ID_MEDICAMENTO = @ID;"
        Dim ds = New mcTabla().obtenerDataSet(sSql)
        If ds.Tables(0).Rows.Count > 0 Then
            txtPrecio1.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", ds.Tables(0).Rows(0)("PRECIO"))
            hdPrecio1.Value = ds.Tables(0).Rows(0)("ID_PRECIO")
        End If
        If ds.Tables(1).Rows.Count > 0 Then
            txtPrecio2.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", ds.Tables(1).Rows(0)("PRECIO"))
            hdPrecio2.Value = ds.Tables(1).Rows(0)("ID_PRECIO")
        End If
    End Sub
    Protected Sub cargarDatos(ByVal id As Integer)
        Dim sSql As String = ""
        sSql += "SELECT *,RIGHT ('0000' + CAST(ID_MEDICAMENTO AS NVARCHAR(5)),5) AS REGISTRO FROM VW_MEDICAMENTO WHERE ID_MEDICAMENTO = " + id.ToString + ";"
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
            txtComposicion.Text = esNulo(tbl.Rows(0)("COMPOSICION"))
            ddlLaboratorio.SelectedValue = tbl.Rows(0)("ID_LABORATORIO")
            ddlFormaFarmaceutica.SelectedValue = tbl.Rows(0)("ID_FORMA_FARMACEUTICA")
            txtAccionTerapeutica.Text = esNulo(tbl.Rows(0)("ACCION_TERAPEUTICA"))
            txtCantidad.Text = esNulo(tbl.Rows(0)("CANTIDAD_PAQUETE"), 1)
            txtPrecioUnitario.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_UNITARIO"), 0))
            txtPrecioPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_PAQUETE"), 0))
            txtRegistroSanitario.Text = esNulo(tbl.Rows(0)("COD_REG_SAN"), "")
            txtMinimo.Text = esNulo(tbl.Rows(0)("MINIMO"), 0)
            txtMaximo.Text = esNulo(tbl.Rows(0)("MAXIMO"), 0)
            txtMarca.Text = esNulo(tbl.Rows(0)("MARCA"), "")
            txtObservacion.Text = esNulo(tbl.Rows(0)("OBSERVACION"), "")
            txtConcentracion.Text = esNulo(tbl.Rows(0)("CONCENTRACION"), "")            
            txtCodigoBarra.Text = esNulo(tbl.Rows(0)("CODIGO_BARRA"), "")
            txtPrecio.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:##0.00}", esNulo(tbl.Rows(0)("PRECIO_COMPRA"), 0))
            chkControlado.Checked = CBool(tbl.Rows(0)("CONTROLADO"))
            chkVentaMayor.Checked = CBool(tbl.Rows(0)("VENTA_MAYOR"))
            txtPrecioPaquete.Enabled = CBool(tbl.Rows(0)("VENTA_MAYOR"))
            cargarPrecio(id)
        End If
    End Sub

    Private Sub gestion_base_datos_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim sSql As String = "SELECT * FROM PRECIO_MEDICAMENTO ORDER BY ID_PRECIO_MEDICAMENTO"
            Dim tbl = New mcTabla().ejecutarConsulta(sSql)
            lblPrecio1.Text = tbl.Rows(0)("DETALLE")            
            lblPrecio2.Text = tbl.Rows(1)("DETALLE")
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("vademecun.aspx")
    End Sub
End Class