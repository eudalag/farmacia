﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="vademecun.aspx.vb" Inherits="FARM_ADM.vademecun" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Medicamentos:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px;">
             Laboratorio:
         </td>
         <td>
             <asp:DropDownList ID="ddlLaboratorio" runat="server" DataSourceID="sqlLaboratorio" DataTextField="DESCRIPCION" DataValueField="ID_LABORATORIO">
             </asp:DropDownList>
             <asp:SqlDataSource ID="sqlLaboratorio" runat="server" ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" SelectCommand="SELECT ID_LABORATORIO, LTRIM(DESCRIPCION) AS DESCRIPCION FROM [LABORATORIO] UNION SELECT -1 , '[Todos Los Laboratorios]'  ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
         </td>
         <tr>
             <td class="textoGris" style="width:50px; ">Buscar: </td>
             <td>
                 <asp:TextBox ID="txtBuscar" runat="server" MaxLength="100" style="width:250px; "></asp:TextBox>
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar" />
             </td>
         </tr>
         <tr>
             <td class="alinearDer" colspan="2">
                 <asp:Button ID="e_btnEliminar" runat="server" OnClientClick="return eliminar()" Text="Eliminar" />
                 <asp:Button ID="a_btnNuevo" runat="server" Text="Nuevo" />
             </td>
         </tr>
         <tr>
             <td colspan="2">
                 <asp:ListView ID="lvwMedicamentos" runat="server" DataSourceID="sqlMedicamentos">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:25px;">&nbsp; </th>
                                 <th style="width:60px;">Registro </th>
                                 <th>Medicamento </th>
                                 <th style="width:120px;">Concentracion </th>
                                 <th style="width:150px;">Presentacion </th>
                                 <th style="width:120px;">Laboratorio </th>
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>
                             <td>
                                 <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />
                             </td>
                             <td class="alinearCentro">
                                 <asp:LinkButton ID="m_btnEditar" runat="server" CommandArgument='<%# Eval("ID_MEDICAMENTO")%>' CommandName="modificar" Text='<%# Eval("REGISTRO")%>'></asp:LinkButton>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("MEDICAMENTO")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblConcentracion" runat="server" Text='<%# Eval("CONCENTRACION")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblPresentacion" runat="server" Text='<%# Eval("PRESENTACION")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblLaboratorio" runat="server" Text='<%# Eval("LABORATORIO")%>'></asp:Label>
                             </td>
                         </tr>
                     </ItemTemplate>
                 </asp:ListView>
                 <div align="right">
                     <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvwMedicamentos" PageSize="10">
                         <Fields>
                             <asp:NextPreviousPagerField ButtonType="Image" FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png" FirstPageText="" LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png" LastPageText="" NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png" NextPageText="" PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png" PreviousPageText="" RenderDisabledButtonsAsLabels="True" ShowFirstPageButton="True" ShowLastPageButton="True" />
                         </Fields>
                     </asp:DataPager>
                     <asp:SqlDataSource ID="sqlMedicamentos" runat="server" ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT ID_MEDICAMENTO, RIGHT ('0000' + CAST(ID_MEDICAMENTO AS NVARCHAR(5)),5) AS REGISTRO,MEDICAMENTO, COMPOSICION, CONCENTRACION,
                        LABORATORIO,  FORMA_FARMACEUTICA AS PRESENTACION, ACCION_TERAPEUTICA   FROM VW_MEDICAMENTO
                        WHERE MEDICAMENTO LIKE '%' + @NOMBRE + '%' AND ID_LABORATORIO = CASE WHEN @LABORATORIO = -1 THEN ID_LABORATORIO ELSE @LABORATORIO END
                        ORDER BY MEDICAMENTO, LABORATORIO ">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                         </SelectParameters>
                         <SelectParameters>
                             <asp:ControlParameter ControlID="ddlLaboratorio" DefaultValue="-1" Name="LABORATORIO" PropertyName="SelectedValue" Type="Int32" />
                         </SelectParameters>
                     </asp:SqlDataSource>   
                 </div>
             </td>
         </tr>
     </tr>
        </table>
             </ContentTemplate>
    </asp:UpdatePanel>
    <script src="../../JavaScript/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript">
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblDescripcion");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar el medicamento:" + lblDescripcion.innerHTML));
                }
            });
        }
    </script>
</asp:Content>
