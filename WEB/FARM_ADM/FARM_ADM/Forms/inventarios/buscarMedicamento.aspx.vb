﻿Public Class buscarMedicamento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        Dim parametros = txtBuscar.Text.Split("*")
        sSql += " DECLARE @MED AS NVARCHAR(MAX),@FF AS NVARCHAR(100), @LAB AS NVARCHAR(150), @COMP AS NVARCHAR (MAX);"
        sSql += " SET @MED = '" + Trim(parametros(0)) + "';"
        If parametros.Length > 1 Then
            sSql += " SET @COMP = '" + Trim(parametros(3)) + "';"
        Else
            sSql += " SET @COMP = '';"
        End If
        If parametros.Length > 2 Then
            sSql += " SET @FF = '" + Trim(parametros(1)) + "';"
        Else
            sSql += " SET @FF = '';"
        End If
        If parametros.Length > 3 Then
            sSql += " SET @LAB ='" + Trim(parametros(2)) + "';"
        Else
            sSql += " SET @LAB ='';"
        End If
       
        sSql += " SELECT ID_MEDICAMENTO, MEDICAMENTO, FORMA_FARMACEUTICA, LABORATORIO, COMPOSICION,MED.ID_LABORATORIO  FROM VW_MEDICAMENTO AS MED "
        If Not Request.QueryString("PROVEEDOR") Is Nothing Then
            sSql += " INNER JOIN PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO = MED.ID_LABORATORIO AND PL.ID_PROVEEDOR = " + Request.QueryString("PROVEEDOR").ToString
        End If        
        sSql += " WHERE  MED.MEDICAMENTO LIKE '%' + @MED +'%' AND MED.FORMA_FARMACEUTICA LIKE '%' + @FF +'%' AND MED.LABORATORIO LIKE '%' + @LAB +'%' "
        sSql += " AND MED.COMPOSICION LIKE '%' + @COMP +'%'"
        sSql += " ORDER BY MEDICAMENTO"

        lvwMedicamentos.DataSource = New mcTabla().ejecutarConsulta(sSql)
        lvwMedicamentos.DataBind()
    End Sub

    Private Sub buscarMedicamento_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            cargarDatos()
            txtBuscar.Focus()
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        cargarDatos()
        txtBuscar.Focus()
    End Sub

    Protected Sub lvwMedicamentos_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwMedicamentos.ItemCommand
        Select Case e.CommandName
            Case "seleccionar"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal('" + e.CommandArgument + "');", True)
        End Select
    End Sub

    Protected Sub lvwMedicamentos_PagePropertiesChanged(sender As Object, e As EventArgs) Handles lvwMedicamentos.PagePropertiesChanged
        cargarDatos()
        txtBuscar.Focus()
    End Sub
End Class