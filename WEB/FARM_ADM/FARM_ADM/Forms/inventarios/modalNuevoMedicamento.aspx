﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="modalNuevoMedicamento.aspx.vb" Inherits="FARM_ADM.modalNuevoMedicamento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

<link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../../scripts/mascara/js/mootools-1.2.4.js" type="text/javascript"></script>
    <script src="../../scripts/mascara/js/Mascara.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery.min.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        window.addEvent("domready", function () {
            var txtUtilidad = new Mascara("txtCantidad", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtPrecio = new Mascara("txtPrecio", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtPrecioUnitario = new Mascara("txtPrecioUnitario", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtPrecioPaquete = new Mascara("txtPrecioPaquete", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtPrecio1 = new Mascara("txtPrecio1", { 'fijarMascara': '##/##/####', 'esNumero': true });
            var txtPrecio2 = new Mascara("txtPrecio2", { 'fijarMascara': '##/##/####', 'esNumero': true });
        });
        </script>
</head>
<body>
    <form id="form1" runat="server">
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;"> 
        <asp:Label ID="Label5" runat="server" Text="Gestion de Medicamentos:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:190px; ">
             Medicamento:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtMedicamento" MaxLength="150" style="width:450px; "></asp:TextBox> 
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="(*)" ControlToValidate="txtMedicamento" CssClass="TextoRojo"></asp:RequiredFieldValidator>            
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:190px; ">
             Controlado:
         </td>
         <td>
            <asp:CheckBox runat="server" ID="chkControlado" />
         </td>
     </tr>
        <tr>
            <td class="textoGris" style="width:150px; ">
             Concentracion:
            </td>
            <td>
             <asp:TextBox runat="server" ID="txtConcentracion" MaxLength="150" style="width:300px; "></asp:TextBox>             
         </td>
        </tr>
        <tr>
         <td class="textoGris">
             Nombre Generico:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtComposicion" MaxLength="300" style="width:600px; "></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Accion Terapeutica:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtAccionTerapeutica" style="width:500px;" TextMode="MultiLine" Rows="3"></asp:TextBox>             
         </td>
     </tr>
        <tr>
         <td class="textoGris">
             Forma Farmaceutica (Presentacion):
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlFormaFarmaceutica" DataSourceID="sqlFormaFarmaceutica" DataTextField="DESCRIPCION" DataValueField="ID_FORMA_FARMACEUTICA"></asp:DropDownList>            
             <asp:SqlDataSource runat="server" ID="sqlFormaFarmaceutica" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString  %>" 
                 SelectCommand="SELECT [ID_FORMA_FARMACEUTICA], [DESCRIPCION] FROM [FORMA_FARMACEUTICA] 
                                UNION ALL 
                                SELECT -1, '[Seleccione Una Forma Farmaceutica]'
                                ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
         </td>
     </tr>
         <tr>
         <td class="textoGris">
             Laboratorio:
         </td>
         <td>
             <asp:DropDownList runat="server" ID="ddlLaboratorio" DataSourceID="sqlLaboratorio" DataTextField="DESCRIPCION" DataValueField="ID_LABORATORIO"></asp:DropDownList>            
             <asp:SqlDataSource runat="server" ID="sqlLaboratorio" 
                 ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString  %>" 
                 SelectCommand="SELECT [ID_LABORATORIO], [DESCRIPCION] FROM [LABORATORIO]
                                UNION ALL 
                                SELECT -1, '[Seleccione Un Laboratorio]'
                                ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
         </td>     
         <tr>
         <td class="textoGris">
             Cod. Registro Sanitario:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtRegistroSanitario" MaxLength="50" style="width:100px; "></asp:TextBox>             
         </td>
     </tr>
         <tr>
         <td class="textoGris">
             Codigo Barra:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtCodigoBarra" MaxLength="50" style="width:150px; "></asp:TextBox>             
         </td>
     </tr>
    <tr>
        <td colspan="2">
<fieldset>
    <legend class="textoNegroNegrita">
                <b>Informacion Referencial de Compra</b>
    </legend>   
    <table>
         <tr>
                    <td class="textoGris" style="width:190px;">
                        Cantidad por paquete (Unitario):
                    </td>
                    <td>
                        <asp:TextBox ID="txtCantidad" runat="server" Text="0.00" MaxLength="18" style="width:50px;"></asp:TextBox>            
                    </td>                     
                </tr>
         <tr>
                    <td class="textoGris" style="width:125px;">
                        Precio Referencial Compra:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecio" runat="server" Text="0.00" MaxLength="18" style="width:50px;"></asp:TextBox>
                    </td>                     
                </tr>
    </table>
</fieldset>
<fieldset>
            <legend class="textoNegroNegrita">
                <b>Precios Referenciales Venta</b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Precio por Unidad:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioUnitario" Text="0.00" style="width:70px;" runat="server"></asp:TextBox>
                    </td>                     
                </tr>                  
                  <tr>
                      <td class="textoGris">
                        Precio por Mayor / Paquete:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioPaquete" EsNumero="true" style="width:70px;" runat="server"></asp:TextBox>
                    </td>
                  </tr>
                  </table>             
        </fieldset>
        <fieldset>
            <legend class="textoNegroNegrita">
                <b>Configuracion de Alerta</b> 
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Minimos:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMinimo" style="width:50px;" MaxLength="3"></asp:TextBox>                                                
                    </td>                     
                </tr>
                  <tr>
                    <td class="textoGris">
                        Maximo:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaximo" style="width:50px;" MaxLength="3"></asp:TextBox>                                                
                    </td>                     
                </tr>
              </table>                
        </fieldset>
     <fieldset>
            <legend class="textoNegroNegrita">
                <b>Otros Datos </b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Marca:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMarca" style="width:120px;" ></asp:TextBox>                                                
                    </td>                     
                </tr>
                  <tr>
                    <td class="textoGris">
                        Observacion:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtObservacion" style="width:150px;"></asp:TextBox>                                                
                    </td>                     
                </tr>
              </table>                
        </fieldset>
 <fieldset>
            <legend class="textoNegroNegrita">
                <b>Otros Precios </b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        <asp:Label runat="server" ID="lblPrecio1" Text="Precio1:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPrecio1" style="width:120px;" ></asp:TextBox>  
                        <asp:HiddenField runat="server" ID="hdPrecio1" Value="-1" />                                              
                    </td>                     
                </tr>
                   <tr>
                    <td class="textoGris" style="width:190px;">
                        <asp:Label runat="server" ID="lblPrecio2" Text="Precio2:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPrecio2" style="width:120px;" ></asp:TextBox> 
                        <asp:HiddenField runat="server" ID="hdPrecio2" Value="-1" />                                                                                             
                    </td>                     
                </tr>
              </table>                
        </fieldset>
        </td>
    </tr>

        <tr>
            <td colspan="2" class="alinearDer">
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" OnClientClick="cerrarInformacion();" />
            </td>
        </tr>
    </table>
    </form>
<script type="text/javascript">
    function cerrarInformacion() {
        parent.cancelarModal();
        return false;
    }        
    </script>
</body>
</html>
