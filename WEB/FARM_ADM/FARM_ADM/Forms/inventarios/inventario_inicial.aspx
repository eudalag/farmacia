﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="inventario_inicial.aspx.vb" Inherits="FARM_ADM.inventario_inicial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Inventario Inicial:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>     
     <tr>
         <td class="textoGris" style="width:130px; ">
             Buscar Medicamento:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>
     <tr>
         <td colspan="2" class="alinearDer">
             <asp:Button runat="server" ID="a_btnNuevo" Text="Nuevo Medicamento" />
         </td>
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwInicial" DataSourceID="sqlInicial">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;border-collapse:collapse;" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <th>
                                    Medicamento
                                </th>
                                <th style="width:200px;">
                                    Nombre Generico
                                </th>
                                <th style="width:120px;">
                                    Laboratorio
                                </th> 
                                <th style="width:150px;">
                                    Forma Farmaceutica
                                </th>                                
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="alinearCentro">
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("MEDICAMENTO")%>' CommandName="modificar" CommandArgument='<%# Eval("ID_MEDICAMENTO")%>'></asp:LinkButton>
                            </td>
                             <td class="textoGris">
                                <%# Eval("COMPOSICION")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("LABORATORIO")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("FORMA_FARMACEUTICA")%>
                            </td>                            
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:ListView runat="server" ID="lvwKardex" DataSourceID="sqlKardex">
                                    <LayoutTemplate>
                                    <table class="generica" style="width:50%;">
                                        <tr>
                                            <td class="textoNegroNegrita ">
                                                Nro. Lote
                                            </td>
                                            <td style="width:120px;" class="textoNegroNegrita ">
                                                Fecha Vencimiento
                                            </td> 
                                            <td style="width:150px;" class="textoNegroNegrita ">
                                                Cantidad
                                            </td>                                
                                        </tr>
                                        <tr>
                                            <td runat="server" id="itemPlaceHolder">
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="alinearCentro">
                                            <%# Eval("NRO_LOTE")%>
                                        </td>
                                        <td class="textoGris">
                                            <%# String.Format(New system.globalization.CultureInfo("es-ES"),"{0:d}",Eval("F_VENCIMIENTO"))%>
                                        </td>
                                        <td class="textoGris">
                                            <%# Eval("CANTIDAD")%>
                                        </td>                            
                                    </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                                <asp:SqlDataSource runat="server" ID="sqlKardex" 
                                    ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                                    SelectCommand='<%# String.concat("DECLARE @ID_MEDICAMENTO AS INTEGER;SET @ID_MEDICAMENTO = ", Eval ("ID_MEDICAMENTO") ,";SELECT KM.ID_MOVIMIENTO ,K.NRO_LOTE, K.F_VENCIMIENTO, KM.CANTIDAD   FROM  KARDEX K INNER JOIN MOVIMIENTO_KARDEX KM ON KM.ID_KARDEX = K.ID_KARDEX WHERE KM.ID_TIPO_MOVIMIENTO = 4 AND K.ID_MEDICAMENTO = @ID_MEDICAMENTO ") %>'>                        
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </ItemTemplate>   
                    <EmptyDataTemplate>
                        <table class="generica" style="width:100%;border-collapse:collapse;" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <th>
                                    Medicamento
                                </th>
                                <th style="width:120px;">
                                    Laboratorio
                                </th> 
                                <th style="width:150px;">
                                    Forma Farmaceutica
                                </th>                                
                            </tr>
                            <tr>
                                <td class="textoNegro alinearCentro" colspan="3">
                                    No Existen Datos.
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>                 
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwInicial" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlInicial" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT  DISTINCT M.ID_MEDICAMENTO,M.MEDICAMENTO,M.COMPOSICION ,  LABORATORIO, M.FORMA_FARMACEUTICA  FROM VW_MEDICAMENTO M 
                                        INNER JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO 
                                        INNER JOIN MOVIMIENTO_KARDEX KM ON KM.ID_KARDEX = K.ID_KARDEX 
                                        WHERE KM.ID_TIPO_MOVIMIENTO  = 4">                        
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>   
</asp:Content>
