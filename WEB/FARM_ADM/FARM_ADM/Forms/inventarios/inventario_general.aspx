﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="inventario_general.aspx.vb" Inherits="FARM_ADM.inventario_general" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Inventario General:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px; ">
             Buscar:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>
     <tr>        
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwMedicamentos" DataSourceID="sqlMedicamentos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>                                
                                <th >
                                    Medicamento
                                </th>
                                <th style="width:150px;">
                                    Presentacion
                                </th > 
<th style="width:150px;">
                                    Laboratorio
                                </th > 
<th style="width:100px;">
                                    Cantidad
                                </th>                               
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>                               
                            <td >
                                <%# Eval("MEDICAMENTO")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("FORMA_FARMACEUTICA")%>
                            </td>
                            <td class="textoGris">
                                <%# Eval("LABORATORIO")%>
                            </td>  
                            <td class="textoGris">
                                <%# Eval("CANTIDAD")%>
                            </td>                             
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwMedicamentos" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlMedicamentos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT MEDICAMENTO, FORMA_FARMACEUTICA, LABORATORIO, ACCION_TERAPEUTICA, CANTIDAD FROM VW_MEDICAMENTO M 
                        INNER JOIN INVENTARIO I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO 
                        WHERE (MEDICAMENTO  LIKE '%' + @NOMBRE + '%') ORDER BY MEDICAMENTO">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
             </ContentTemplate>
    </asp:UpdatePanel> 
</asp:Content>
