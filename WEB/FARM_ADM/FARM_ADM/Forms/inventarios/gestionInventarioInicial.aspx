﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionInventarioInicial.aspx.vb" Inherits="FARM_ADM.gestionInventarioInicial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/mascara/js/mootools-1.2.4.js"></script>
    <script type="text/javascript" src="../../scripts/mascara/js/Mascara.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.numeric.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>        
    <div>
        <fieldset>
            <legend class="textoNegroNegrita">
                Medicamento                
            </legend>
            <table>
                <tr>
                     <td class="textoGris" style="width:50px; ">
                         Codigo de Barra:
                     </td>
                     <td>
                         <asp:TextBox runat="server" ID="txtCodigoBarra"></asp:TextBox>
                        <asp:CheckBox runat="server" ID="chkActualizar" Text="Actualizar Informacion" TextAlign="Right" />
                     </td>
                 </tr>
                <tr>
                    <td class="textoGris" style="width:125px;">
                        Medicamento:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMedicamento"  style="width:200px;"></asp:TextBox>                        
                        <asp:Button runat="server" ID="btnBuscar" Text="Buscar" OnClientClick="return buscarMedicamento();" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button runat="server" ID="btnNuevoMedicamento" Text="Nuevo Medicamento" OnClientClick="return adicionarMedicamento();" />
                        <asp:HiddenField runat="server" id="hdIdMedicamento" Value="-1" />                        
                        <asp:Button runat="server" ID="btnCargar" Text="cargar" style="display:none;" />
                    </td>                     
                </tr>
                <tr>
                    <td class="textoGris">
                        Nombre Generico:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtComposicion"  style="width:400px;"></asp:TextBox>                        
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Concentracion:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtConcentracion" style="width:400px;"></asp:TextBox>                        
                    </td>
                </tr>                
                <tr>
                    <td class="textoGris">
                        Laboratorio:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" id="ddlLaboratorio" DataSourceID="sqlLaboratorio" 
                            DataTextField="DESCRIPCION" DataValueField="ID_LABORATORIO" ></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlLaboratorio" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT ID_LABORATORIO, DESCRIPCION FROM LABORATORIO 
UNION ALL SELECT -1, '[Seleccione Laboratorio]' ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Forma Farmaceutica (Presentacion):                   
                    </td>
                    <td>
                        <asp:DropDownList runat="server" id="ddlFormaFarmaceutica" DataSourceID="sqlFormaFarmaceutica" 
                            DataTextField="DESCRIPCION" DataValueField="ID_FORMA_FARMACEUTICA" ></asp:DropDownList>                        
                        <asp:SqlDataSource runat="server" ID="sqlFormaFarmaceutica" 
                            ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                            SelectCommand="SELECT * FROM FORMA_FARMACEUTICA  
UNION ALL SELECT -1, '[Seleccione Forma Farmaceutica]' ORDER BY [DESCRIPCION]"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Cod. de Reg. Sanitario:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtRegSanitario"  style="width:100px;"></asp:TextBox>                        
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Accion Terapeutica:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtAccionTerapeutica" style="width:500px;" TextMode="MultiLine" Rows="3"></asp:TextBox>             
                    </td>
                </tr>
                <tr>
                    <td class="textoGris">
                        Cantidad por Paquete:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCantidad"  style="width:50px;" MaxLength="3"></asp:TextBox>                        
                    </td>
                </tr>                
                <tr>
                    <td class="textoGris">
                        Controlado:
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkControlado"  />
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="textoNegroNegrita">
                <b>Precios Referenciales Venta</b>
            </legend>   
              <table>
                <tr>
                    <td class="textoGris" style="width:190px;">
                        Precio por Unidad:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioUnitario" Text="0.00" style="width:70px;" runat="server"></asp:TextBox>
                    </td>                     
                </tr>                  
                  <tr>
                      <td class="textoGris">
                        Precio por Mayor / Paquete:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioPaquete" EsNumero="true" style="width:70px;" runat="server"></asp:TextBox>
                    </td>
                  </tr>
                  </table>             
        </fieldset>
        <fieldset>
            <legend class="textoNegroNegrita">
                Kardex
            </legend>
            <div style="width:75%;" class="alinearDer">
                <asp:Button runat="server" ID="btnNuevoKardex" Text="Nuevo" />
            </div>
            <div style="width:100%;" class="alinearCentro">
                <asp:ListView runat="server" ID="lvwKardex">
                    <LayoutTemplate>
                        <table class="generica" style="width:50%;">
                            <tr> 
                                <th style="width:15px;">
                                    &nbsp;
                                </th>
                                <th style="width:100px;">
                                    Reg. Kardex
                                </th>
                                <th style="width:15px;">
                                    S/L
                                </th>
                                <th >
                                    Nro. Lote
                                </th>
                                <th style="width:95px;">
                                    F. Vencimiento
                                </th>  
                                 <th style="width:100px;">
                                    Cantidad
                                </th>                             
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>   
                            <td>
                                <input type="checkbox" runat="server" id="chkEliminar"  class="checked_kardex textoGris" onchange="seleccionar_item_kardex(this)" />
                            </td>    
                            <td>
                                <asp:Label runat="server" ID="lblIdKardex" Text='<%#Eval("ID_KARDEX")%>'></asp:Label>
                                <asp:HiddenField runat="server" ID="hfNRO_FILA" Value='<%#Eval("NRO_FILA")%>' />
                            </td> 
                            <td>
                                <input type="checkbox" runat="server" id="chkSinLote" class="textoGris" onchange="sinlote(this);" checked='<%# IIf(Eval("SIN_LOTE"), "true", "false")%>' />
                            </td>                                                
                            <td class="textoGris"> 
                                <asp:TextBox runat="server"  ID="txtNroLote" Text='<%# Eval("NRO_LOTE")%>' style="width:80px;"></asp:TextBox>
                            </td> 
                            <td class="textoGris">
                                <asp:TextBox  ID="txtFechaVencimiento" style="width:60px" runat="server" Text='<%# String.Format(new system.globalization.cultureInfo("es-ES"),"{0:dd/MM/yyyy}",Eval("F_VENCIMIENTO"))%>' CssClass="textoGris fecha"></asp:TextBox >
                            </td> 
                            <td class="textoGris">
                                <asp:TextBox runat="server" CssClass="cantidad textoGris"  ID="txtCantidad" Text='<%# Eval("CANTIDAD")%>' style="width:50px; text-align:right;"></asp:TextBox>
                            </td>                            
                        </tr>
                    </ItemTemplate>                    
                    <EmptyDataTemplate>
                        <table class="generica" style="width:50%;">
                            <tr>
                                <th style="width:100px;">
                                    Nro. Lote
                                </th>
                                <th >
                                    F. Vencimiento
                                </th>  
                                 <th >
                                    Cantidad
                                </th>                             
                            </tr>
                            <tr>
                                <td colspan="3" class="textoGris alinearCentro">
                                    No existen Datos.   
                                </td>
                            </tr>
                            </table>
                    </EmptyDataTemplate>
                </asp:ListView>                                                          
            </div>
        </fieldset>
    </div>
            <div class="alinearDer" >
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
            </div>
            </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript" >

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(inicializar);
        function inicializar() {
            jQuery(".cantidad").each(function () {
                jQuery(this).numeric({});
            });
            var fecha
            jQuery(".fecha").each(function () {
                jQuery(this).datepicker({});
            });
        }

        function sinlote(obj) {
            var estado = obj.checked;
            idremplazo = obj.id.replace("chkSinLote", "txtNroLote");
            document.getElementById(idremplazo).disabled = estado;
        }

        function cancelarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");

        }
        function aceptarModal(idMedicamento) {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");           
            jQuery("#<%=hdIdMedicamento.ClientID%>").val(idMedicamento);
            jQuery("#<%=btnCargar.ClientID%>").click()
            return false;
        }
        function buscarMedicamento() {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "buscarMedicamento.aspx";
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR MEDICAMENTOS", width: 800, height: 600 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function adicionarMedicamento() {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "modalNuevoMedicamento.aspx";
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "GESTION MEDICAMENTOS", width: 800, height: 600 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function seleccionar_item_kardex(obj) {
            var estado = obj.checked;
            jQuery(".checked_kardex").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
    </script>
</asp:Content>
