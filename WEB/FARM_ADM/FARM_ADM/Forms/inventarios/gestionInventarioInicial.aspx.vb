﻿Imports System.Transactions
Public Class gestionInventarioInicial
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub  

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click

        Dim tblKardex As DataTable = ViewState("tblKardex")
        tblKardex.Clear()
        ViewState("tblKardex") = tblKardex

        cargarDatos(hdIdMedicamento.Value)
        cargarKardex()

    End Sub
    Protected Sub cargarDatos(ByVal idMedicamento As Integer)
        Dim sSql As String = ""
        Dim ds
        sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += "SELECT * FROM VW_MEDICAMENTO WHERE ID_MEDICAMENTO = @MEDICAMENTO;"
        sSql += " SELECT K.ID_KARDEX,MK.ID_MOVIMIENTO,K.SIN_LOTE,K.NRO_LOTE, ISNULL(CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103),'__/__/____') AS F_VENCIMIENTO,MK.CANTIDAD FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.ID_MEDICAMENTO = @MEDICAMENTO;"
        ds = New mcTabla().obtenerDataSet(sSql)
        Dim tbl = ds.Tables(0)
        txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
        txtConcentracion.Text = esNulo(tbl.Rows(0)("CONCENTRACION"), "")
        txtComposicion.Text = esNulo(tbl.Rows(0)("COMPOSICION"))
        ddlLaboratorio.SelectedValue = tbl.Rows(0)("ID_LABORATORIO")
        ddlFormaFarmaceutica.SelectedValue = tbl.Rows(0)("ID_FORMA_FARMACEUTICA")
        txtAccionTerapeutica.Text = esNulo(tbl.Rows(0)("ACCION_TERAPEUTICA"))
        txtCantidad.Text = esNulo(tbl.Rows(0)("CANTIDAD_PAQUETE"), 1)
        chkControlado.Checked = esNulo(CBool(tbl.Rows(0)("CONTROLADO")))
        txtPrecioUnitario.Text = esNulo(tbl.Rows(0)("PRECIO_REFERENCIAL"), "0.00")
        txtPrecioPaquete.Text = esNulo(tbl.Rows(0)("PRECIO_REFERENCIAL_PAQUETE"), "0.00")
        txtRegSanitario.Text = esNulo(tbl.Rows(0)("COD_REG_SAN"), "")        
        Dim tblKardex = ds.Tables(1)
            For Each kardex In tblKardex.Rows
                nuevoItem(kardex("ID_KARDEX"), kardex("ID_MOVIMIENTO"), esNulo(kardex("NRO_LOTE"), ""), kardex("SIN_LOTE"), esNulo(kardex("F_VENCIMIENTO"), ""), kardex("CANTIDAD"), 0, 1)
            Next
    End Sub
    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_KARDEX"
        tblItems.Columns.Add(columna)        
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_MOVIMIENTO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Boolean")
        columna.ColumnName = "SIN_LOTE"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NRO_LOTE"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.DateTime")
        columna.ColumnName = "F_VENCIMIENTO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        ViewState("tblKardex") = tblItems
    End Sub
    Protected Sub nuevoItem(ByVal idKardex As Integer, ByVal idMovimiento As Integer, ByVal nroLote As String, ByVal sinLote As Boolean, ByVal fVencimiento As String, ByVal cantidad As Integer, ByVal nuevo As Integer, ByVal accion As Integer)
        Dim tblKardex As DataTable = ViewState("tblKardex")
        Dim foundRows() As DataRow
        foundRows = tblKardex.Select("NRO_FILA = MAX(NRO_FILA)")
        Dim dr As DataRow
        dr = tblKardex.NewRow
        If foundRows.Length = 0 Then
            dr("NRO_FILA") = 1
        Else
            dr("NRO_FILA") = CInt(tblKardex.Select("NRO_FILA = MAX(NRO_FILA)")(0)("NRO_FILA")) + 1
        End If
        dr("ID_KARDEX") = idKardex
        dr("ID_MOVIMIENTO") = idMovimiento
        dr("SIN_LOTE") = sinLote
        dr("NRO_LOTE") = nroLote
        If fVencimiento = "" Or fVencimiento = "__/__/____" Then
            dr("F_VENCIMIENTO") = DBNull.Value
        Else
            dr("F_VENCIMIENTO") = Date.Parse(fVencimiento, New Globalization.CultureInfo("es-ES"))
        End If        
        dr("CANTIDAD") = cantidad
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblKardex.Rows.Add(dr)
        tblKardex.AcceptChanges()
        ViewState("tblKardex") = tblKardex
    End Sub
    Public Sub cargarKardex()
        Dim tblKardex As DataTable = ViewState("tblKardex")
        Dim tbl = (From c In tblKardex Where c!ACCION <> 3)
        If tbl.AsDataView.Count > 0 Then
            lvwKardex.DataSource = tbl.CopyToDataTable
        End If
        lvwKardex.DataBind()
    End Sub

    Protected Sub actualizarTabla()
        Dim tblKardex As DataTable = ViewState("tblKardex")
        For Each fila In lvwKardex.Items
            For Each tabla In tblKardex.Rows
                If (tabla("NRO_FILA") = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value) Then
                    Dim nroFila As Integer = CType(fila.FindControl("hfNRO_FILA"), HiddenField).Value
                    Dim idKardex As String = CType(fila.FindControl("lblIdKardex"), Label).Text
                    Dim nroLote As String = CType(fila.FindControl("txtNroLote"), TextBox).Text
                    Dim fVencimiento As String = CType(fila.FindControl("txtFechaVencimiento"), TextBox).Text
                    Dim cantidad As Decimal = Decimal.Parse(IIf(CType(fila.FindControl("txtCantidad"), TextBox).Text.ToString = "", 0, CType(fila.FindControl("txtCantidad"), TextBox).Text), New Globalization.CultureInfo("en-US"))
                    Dim sinLote As Boolean = CType(fila.FindControl("chkSinLote"), HtmlInputCheckBox).Checked

                    If tabla("NRO_LOTE") <> nroLote Or esNulo(tabla("F_VENCIMIENTO"), "") <> fVencimiento Or tabla("CANTIDAD") <> cantidad Or tabla("SIN_LOTE") <> sinLote Then
                        tabla("ACCION") = 2
                        tabla("SIN_LOTE") = sinLote
                        tabla("NRO_LOTE") = nroLote
                        If fVencimiento = "" Or fVencimiento = "__/__/____" Then
                            tabla("F_VENCIMIENTO") = DBNull.Value
                        Else
                            tabla("F_VENCIMIENTO") = Date.Parse(fVencimiento, New Globalization.CultureInfo("es-ES"))
                        End If                        
                        tabla("CANTIDAD") = cantidad
                    End If
                End If
            Next
        Next
        tblKardex.AcceptChanges()
        ViewState("tblKardex") = tblKardex
    End Sub
    Protected Function obtenerKardex(ByVal nroLote As String, ByVal f_vencimiento As String, ByVal idMedicamento As Integer, ByVal chkSinLote As Boolean)
        If Request.QueryString("KARDEX") Is Nothing Then
            Dim sSql As String = ""
            If Not chkSinLote Then
                sSql += " DECLARE @LOTE AS NVARCHAR(15), @F_VENCIMIENTO AS DATE, @MEDICAMENTO AS INTEGER;"
                sSql += " SET @LOTE = '" + Trim(nroLote) + "';"
                sSql += " SET @F_VENCIMIENTO = CONVERT (DATE, '" + f_vencimiento + "', 103);"
                sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
                sSql += " SELECT ID_KARDEX FROM KARDEX WHERE NRO_LOTE =  @LOTE AND F_VENCIMIENTO = @F_VENCIMIENTO AND ID_MEDICAMENTO = @MEDICAMENTO;"
                Dim tbl = New mcTabla().ejecutarConsulta(sSql)
                If tbl.Rows.Count > 0 Then
                    Return tbl.Rows(0)(0)
                Else
                    Return -1
                End If
            Else
                sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
                sSql += " SET @MEDICAMENTO = '" + Request.QueryString("MEDICAMENTO") + "';"

                sSql += " SELECT ID_KARDEX FROM KARDEX WHERE ID_MEDICAMENTO =  @MEDICAMENTO AND SIN_LOTE = 1;"
                Dim tbl = New mcTabla().ejecutarConsulta(sSql)
                If tbl.Rows.Count > 0 Then
                    Return tbl.Rows(0)(0)
                Else
                    Return -1
                End If
            End If

        Else
            Return Request.QueryString("KARDEX")
        End If

    End Function
    Protected Sub procesarKardexInicial(ByVal idMedicamento As Integer)
        Dim tblInventario = New mcTabla
        Dim tblKardex = CType(ViewState("tblKardex"), DataTable)
        Dim idKardex As Integer
        For Each item As DataRow In tblKardex.Select("ACCION <> 0", "NRO_FILA ASC, ACCION DESC")
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    idKardex = obtenerKardex(item("NRO_LOTE"), esNulo(item("F_VENCIMIENTO"), "null"), idMedicamento, item("SIN_LOTE"))
                    If idKardex = -1 Then
                        tblInventario.tabla = "KARDEX"
                        tblInventario.campoID = "ID_KARDEX"
                        tblInventario.modoID = "sql"
                        tblInventario.agregarCampoValor("NRO_LOTE", IIf(CBool(item("SIN_LOTE")), "null", item("NRO_LOTE")))
                        tblInventario.agregarCampoValor("F_VENCIMIENTO", esNulo(item("F_VENCIMIENTO"), "null"))
                        tblInventario.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                        tblInventario.agregarCampoValor("SALDOS", item("CANTIDAD"))
                        tblInventario.agregarCampoValor("PRECIO_COMPRA", "NULL")
                        tblInventario.agregarCampoValor("PRECIO_VENTA_UNIDAD", txtPrecioUnitario.Text)
                        tblInventario.agregarCampoValor("PRECIO_VENTA_PAQUETE", txtPrecioPaquete.Text)
                        tblInventario.agregarCampoValor("PORC_UNIDAD", 0)
                        tblInventario.agregarCampoValor("PORC_PAQUETE", 0)
                        tblInventario.agregarCampoValor("SIN_LOTE", item("SIN_LOTE"))
                        tblInventario.insertar()
                        idKardex = tblInventario.valorID
                        tblInventario.reset()
                    End If                    

                    tblInventario.tabla = "MOVIMIENTO_KARDEX"
                    tblInventario.campoID = "ID_MOVIMIENTO"
                    tblInventario.modoID = "sql"
                    tblInventario.agregarCampoValor("ID_KARDEX", idKardex)
                    tblInventario.agregarCampoValor("ID_TIPO_MOVIMIENTO", 4)
                    tblInventario.agregarCampoValor("CANTIDAD", item("CANTIDAD"))
                    tblInventario.agregarCampoValor("FECHA", Date.Parse(Now.ToString("dd/MM/yyyy"), New Globalization.CultureInfo("es-ES")))
                    tblInventario.agregarCampoValor("ID_USUARIO", Session("IDUSUARIO"))
                    tblInventario.insertar()
                End If
            Else
                Select Case item("ACCION")
                    Case "2"
                        'tblInventario.tabla = "KARDEX"
                        'tblInventario.campoID = "ID_KARDEX"
                        'tblInventario.valorID = item("ID_KARDEX")
                        'tblInventario.agregarCampoValor("NRO_LOTE", IIf(CBool(item("SIN_LOTE")), "null", item("NRO_LOTE")))
                        'tblInventario.agregarCampoValor("F_VENCIMIENTO", item("F_VENCIMIENTO"))
                        'tblInventario.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                        'tblInventario.agregarCampoValor("SALDOS", item("CANTIDAD"))
                        'tblInventario.agregarCampoValor("PRECIO_COMPRA", "NULL")
                        'tblInventario.agregarCampoValor("PRECIO_VENTA_UNIDAD", txtPrecioUnitario.Text)
                        'tblInventario.agregarCampoValor("PRECIO_VENTA_PAQUETE", txtPrecioPaquete.Text)
                        'tblInventario.agregarCampoValor("PORC_UNIDAD", 0)
                        'tblInventario.agregarCampoValor("PORC_PAQUETE", 0)
                        'tblInventario.agregarCampoValor("SIN_LOTE", item("SIN_LOTE"))
                        'tblInventario.modificar()
                        'idKardex = tblInventario.valorID
                        'tblInventario.reset()
                        'tblInventario.tabla = "MOVIMIENTO_KARDEX"
                        'tblInventario.campoID = "ID_MOVIMIENTO"
                        'tblInventario.valorID = item("ID_MOVIMIENTO")
                        'tblInventario.agregarCampoValor("ID_KARDEX", idKardex)
                        'tblInventario.agregarCampoValor("ID_TIPO_MOVIMIENTO", 4)
                        'tblInventario.agregarCampoValor("CANTIDAD", item("CANTIDAD"))
                        'tblInventario.agregarCampoValor("FECHA", Date.Parse(Now.ToString("dd/MM/yyyy"), New Globalization.CultureInfo("es-ES")))
                        'tblInventario.agregarCampoValor("ID_USUARIO", Session("IDUSUARIO"))
                        'tblInventario.modificar()
                    Case "3"
                        tblInventario.tabla = "MOVIMIENTO_KARDEX"
                        tblInventario.campoID = "ID_MOVIMIENTO"
                        tblInventario.valorID = item("ID_MOVIMIENTO")
                        tblInventario.eliminar()
                End Select
            End If
            tblInventario.reset()
        Next
        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "A", "alert('" + proceso + "')", True)      
    End Sub

    Private Sub gestionInventarioInicial_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            crearTabla()            
            If Not Request.QueryString("ID") Is Nothing Then                
                hdIdMedicamento.Value = Request.QueryString("ID")
                cargarDatos(Request.QueryString("ID"))
            End If
            cargarKardex()            
        End If
    End Sub

    Protected Sub btnNuevoKardex_Click(sender As Object, e As EventArgs) Handles btnNuevoKardex.Click
        actualizarTabla()
        nuevoItem(0, 0, "", False, "", 0, 1, 1)
        cargarKardex()
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then

            Using scope As New TransactionScope()
                Try
                    If chkActualizar.Checked Then
                        gestionMedicamento()
                    End If
                    actualizarTabla()
                    procesarKardexInicial(hdIdMedicamento.Value)
                    scope.Complete()
                    Response.Redirect("inventario_inicial.aspx")
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "alert('Error');", True)
                End Try
            End Using
        End If
    End Sub
    Protected Sub gestionMedicamento()
        Dim tblMedicamento = New mcTabla()
        tblMedicamento.tabla = "MEDICAMENTO"
        tblMedicamento.campoID = "ID_MEDICAMENTO"
        tblMedicamento.modoID = "auto"
        tblMedicamento.agregarCampoValor("NOMBRE", txtMedicamento.Text)
        tblMedicamento.agregarCampoValor("COMPOSICION", txtComposicion.Text)
        tblMedicamento.agregarCampoValor("ID_TIPO_MEDICAMENTO", ddlFormaFarmaceutica.SelectedValue)
        tblMedicamento.agregarCampoValor("ACTIVO", 1)
        tblMedicamento.agregarCampoValor("PRECIO_UNITARIO", Decimal.Parse(txtPrecioUnitario.Text, New Globalization.CultureInfo("en-US")))
        tblMedicamento.agregarCampoValor("CONTROLADO", chkControlado.Text)
        tblMedicamento.agregarCampoValor("ID_LABORATORIO", ddlLaboratorio.Text)
        tblMedicamento.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
        tblMedicamento.agregarCampoValor("ACCION", txtAccionTerapeutica.Text)
        tblMedicamento.agregarCampoValor("CANTIDAD_PAQUETE", txtCantidad.Text)
        tblMedicamento.agregarCampoValor("COD_REG_SAN", txtRegSanitario.Text)
        tblMedicamento.agregarCampoValor("PRECIO_PAQUETE", Decimal.Parse(txtPrecioPaquete.Text, New Globalization.CultureInfo("en-US")))
        tblMedicamento.valorID = hdIdMedicamento.Value
        tblMedicamento.modificar()
    End Sub


    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("inventario_inicial.aspx")
    End Sub

End Class