﻿Public Class tipo_cambio
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim tbl = New mcTabla()
        tbl.tabla = "CONFIGURACION"
        tbl.campoID = "ID_CONFIGURACION"
        tbl.valorID = 1
        tbl.agregarCampoValor("TC_ME", Decimal.Parse(txtUtilidad.Text, New Globalization.CultureInfo("en-US")))
        tbl.modificar()
    End Sub

    Private Sub tipo_cambio_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim sSql = "SELECT TC_ME FROM CONFIGURACION"
            txtUtilidad.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,#0.00}", New mcTabla().ejecutarConsulta(sSql).Rows(0)(0))
        End If
    End Sub
End Class