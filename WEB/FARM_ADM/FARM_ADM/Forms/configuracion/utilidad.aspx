﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="utilidad.aspx.vb" Inherits="FARM_ADM.utilidad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../../scripts/mascara/js/mootools-1.2.4.js" type="text/javascript"></script>
    <script src="../../scripts/mascara/js/Mascara.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery.min.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        window.addEvent("domready", function () {
            var txtUtilidad = new Mascara("<%=txtUtilidad.ClientID %>", { 'fijarMascara': '##/##/####', 'esNumero': true });
        });
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:50%">
    <tr>
      <td colspan="3"> 
        <div class="alinearIzq" style="padding-bottom:10px;"> 
        <asp:Label ID="Label5" runat="server" Text="Utilidad:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
        <td style="width:100px; "> &nbsp;</td>
         <td class="textoGris" style="width:150px; ">
             Utilidad Sobre Venta:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtUtilidad" MaxLength="5" style="width:50px;"></asp:TextBox> %             
         </td>
     </tr>
         <tr>
            <td colspan="3" class="alinearDer">
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
            </td>
        </tr>
   </table>
</asp:Content>
