﻿Imports System.Transactions
Public Class importarMedicamentos
    Inherits System.Web.UI.Page
    Shared pila As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Using scope As New TransactionScope()
            Try
                importarMedicamentos()
                'scope.Complete()
            Catch ex As Exception
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "alert('Error');", True)
            End Try
        End Using
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "alert('Exito');", True)

    End Sub

    Protected Function esCodigoBarra(ByVal codigo As String)
        Return (codigo >= 12 And codigo <= 13)
    End Function

    Protected Sub importarMedicamentos()
        Dim sSQl As String = ""
        sSQl += " SELECT * FROM IMPORTAR  ORDER BY ID_MED_IMPORTAR"
        Dim tbl = New mcTabla().ejecutarConsulta(sSQl)
        Dim codBarra = ""
        Dim med = ""
        Dim lab As Integer = 0
        Dim gen As String = ""
        Dim prec As Decimal = 0
        Dim prec1 As Decimal = 0
        Dim prec2 As Decimal = 0
        Dim cant As Integer = 0
        Dim lote As String = ""
        Dim idMed = -1        
        For Each item In tbl.Rows
            codBarra = IIf(esCodigoBarra(item("CODIGO_BARRA")), item("CODIGO_BARRA"), "")
            med = item("MEDICAMENTO")
            pila = pila + med + "***"
            If item("LABORATORIO") = "" Then
                lab = -1
            Else
                lab = CInt(item("LABORATORIO"))
            End If
            gen = item("NOMBRE_GENERICO")
            prec = CDec(item("PRECIO_NORMAL"))
            prec1 = CDec(item("PRECIO_2"))
            prec2 = CDec(item("PRECIO_3"))
            cant = item("CANTIDAD")
            lote = item("LOTE")
            idMed = item("ID_MEDICAMENTO")
            If CInt(idMed) = -1 Then
                idMed = procesarMedicamento(med, lab, gen, prec, prec1, prec2, codBarra)
                If prec1 > 0 Then
                    procesaPreciosExtra(idMed, prec1, 1)
                End If
                If prec2 > 0 Then
                    procesaPreciosExtra(idMed, prec2, 2)
                End If
            End If

            'If cant > 0 Then
            procesarLotes(idMed, lote, cant)
            'End If
        Next
    End Sub
    Protected Function procesarMedicamento(ByVal medicamento As String, ByVal codLaboratorio As Integer, ByVal nombreGenerico As String, ByVal precio As Decimal, ByVal precio1 As Decimal, ByVal precio2 As Decimal, ByVal codBarra As String) As Integer
        Dim tbl = New mcTabla()

        tbl.tabla = "MEDICAMENTO"
        tbl.modoID = "auto"
        tbl.campoID = "ID_MEDICAMENTO"
        tbl.agregarCampoValor("NOMBRE", medicamento)
        tbl.agregarCampoValor("COMPOSICION", nombreGenerico)
        tbl.agregarCampoValor("MINIMO", 5)
        tbl.agregarCampoValor("ID_TIPO_MEDICAMENTO", 10496)
        tbl.agregarCampoValor("ACTIVO", 1)
        tbl.agregarCampoValor("PRECIO_UNITARIO", precio)
        tbl.agregarCampoValor("CONTROLADO", 0)
        tbl.agregarCampoValor("ID_LABORATORIO", codLaboratorio)
        tbl.agregarCampoValor("CANTIDAD_PAQUETE", 1)
        tbl.agregarCampoValor("CODIGO_BARRA", IIf(codBarra = "", "null", codBarra))
        tbl.agregarCampoValor("UTILIDAD", 25)
        tbl.insertar()
        Return tbl.valorID
    End Function
    Protected Sub procesaPreciosExtra(ByVal idMedicamento As Integer, ByVal precio As Decimal, ByVal tipo As Integer)
        Dim tbl = New mcTabla()
        tbl.tabla = "PRECIOS_EXTRAS"
        tbl.campoID = "ID_PRECIO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
        tbl.agregarCampoValor("ID_PRECIO_MEDICAMENTO", tipo)
        tbl.agregarCampoValor("PRECIO", precio)
        tbl.insertar()
    End Sub
    Function obtenerKardex(ByVal nroLote As String, ByVal vencimiento As String, ByVal idMedicamento As Integer, ByRef cantidadAnterior As Integer) As Integer
        Dim sSql As String = ""
        sSql += " DECLARE @LOTE AS NVARCHAR(18), @VENCIMIENTO AS NVARCHAR(10),@MEDICAMENTO AS INTEGER;"
        sSql += " SET @LOTE = " + IIf(nroLote = "null", "NULL", "'" + nroLote + "'") + ";"
        sSql += " SET @VENCIMIENTO = " + IIf(vencimiento = "null", "NULL", "'" + vencimiento + "'") + ";"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString

        sSql += " SELECT K.ID_KARDEX,K.ID_MEDICAMENTO,NRO_LOTE,F_VENCIMIENTO, MK.CANTIDAD  FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX AND MK.ID_TIPO_MOVIMIENTO = 1"
        sSql += " WHERE K.ID_MEDICAMENTO = @MEDICAMENTO AND "
        sSql += " ISNULL(NRO_LOTE,-1) = CASE WHEN @LOTE IS NULL THEN -1 ELSE @LOTE END AND "
        sSql += " ISNULL(CONVERT(NVARCHAR(10),F_VENCIMIENTO,103),'') = CASE WHEN @VENCIMIENTO IS NULL THEN '' ELSE @VENCIMIENTO END"
        Dim tblKardex = New mcTabla().ejecutarConsulta(sSql)
        If tblKardex.Rows.Count > 0 Then
            cantidadAnterior = tblKardex.Rows(0)("CANTIDAD")
            Return tblKardex.Rows(0)("ID_KARDEX")
        Else
            cantidadAnterior = -1
            Return -1
        End If
    End Function
    Protected Sub procesarLotes(ByVal idMedicamento As Integer, ByVal GrupoLotes As String, ByVal cantidad As Integer)
        Dim lotes = Split(GrupoLotes, "  ")
        Dim det
        Dim cant As Integer = 0
        Dim vencimiento As String
        Dim idKardex As Integer
        Dim tblInventario = New mcTabla()
        Dim nroLote = "20190310"
        Dim cantidadAnterior = -1
        Dim dif As Integer = 0
        For Each lote In lotes
            det = lote.Split("*")
            If Trim(lote) = "" Then
                vencimiento = "null"
                cant = cantidad
            Else
                vencimiento = det(1)
                cant = det(0)
            End If




            'If cant > 0 Then
            Dim venc As Date
            If vencimiento = "null" Then
                idKardex = obtenerKardex("null", "null", idMedicamento, cantidadAnterior)
            Else
                Dim fecha = det(1).split("-")
                Dim anho = fecha(0)
                If CInt(anho < 1900) Then
                    anho = 2000 + CInt(anho)
                End If
                Dim mes = fecha(1)
                Dim dia = fecha(2)
                venc = New Date(anho, mes, dia)
                idKardex = obtenerKardex(nroLote, venc.ToString("dd/MM/yyyy"), idMedicamento, cantidadAnterior)
            End If

            If idKardex = -1 Then
                If cant > 0 Then
                    tblInventario.tabla = "KARDEX"
                    tblInventario.campoID = "ID_KARDEX"
                    tblInventario.modoID = "sql"
                    If vencimiento = "null" Then
                        tblInventario.agregarCampoValor("NRO_LOTE", "null")
                        tblInventario.agregarCampoValor("F_VENCIMIENTO", "null")
                        tblInventario.agregarCampoValor("SIN_LOTE", 1)
                    Else
                        tblInventario.agregarCampoValor("NRO_LOTE", nroLote)
                        tblInventario.agregarCampoValor("F_VENCIMIENTO", venc)
                        tblInventario.agregarCampoValor("SIN_LOTE", 0)
                    End If


                    tblInventario.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                    tblInventario.agregarCampoValor("SALDOS", cant)
                    tblInventario.agregarCampoValor("CERRADO", 0)

                    tblInventario.insertar()
                    idKardex = tblInventario.valorID
                    tblInventario.reset()
                End If
                
            Else
                dif = cant - cantidadAnterior
            End If

            If idKardex <> -1 And ((cantidadAnterior <> -1 And dif <> 0) Or (cantidadAnterior = -1 And dif = 0)) Then
                tblInventario.tabla = "MOVIMIENTO_KARDEX"
                tblInventario.campoID = "ID_MOVIMIENTO"
                tblInventario.modoID = "sql"
                tblInventario.agregarCampoValor("ID_KARDEX", idKardex)
                tblInventario.agregarCampoValor("ID_TIPO_MOVIMIENTO", IIf(dif = 0, 1, IIf(dif > 0, 4, 5)))
                tblInventario.agregarCampoValor("CANTIDAD", IIf(dif = 0, cant, dif))
                tblInventario.agregarCampoValor("FECHA", "serverDateTime")
                tblInventario.agregarCampoValor("ID_USUARIO", Session("IDUSUARIO"))
                tblInventario.insertar()
                tblInventario.reset()
            End If
            
            'End If
            dif = 0
            cantidadAnterior = -1
        Next
    End Sub
End Class