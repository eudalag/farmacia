﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionLaboratorios.aspx.vb" Inherits="FARM_ADM.gestionLaboratorios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="../../scripts/mascara/js/mootools-1.2.4.js" type="text/javascript"></script>
    <script src="../../scripts/mascara/js/Mascara.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery.min.js" type="text/javascript"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js" type="text/javascript"></script>
 <script type="text/javascript">
     jQuery.noConflict();
     window.addEvent("domready", function () {
         var txtUtilidad = new Mascara("<%=txtUtilidad.ClientID%>", { 'fijarMascara': '##/##/####', 'esNumero': true });         
     });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;"> 
        <asp:Label ID="Label5" runat="server" Text="Gestion Laboratorios:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:150px; ">
             Laboratorio:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50" style="width:250px; "></asp:TextBox>             
         </td>
     </tr>
    <tr>
         <td class="textoGris" style="width:150px; ">
             Margen de Utilidad:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtUtilidad" Text="0.00" MaxLength="5" style="width:100px; "></asp:TextBox>  &nbsp; %         
         </td>
     </tr>
         <tr>
            <td colspan="2" class="alinearDer">
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
            </td>
        </tr>
   </table>
</asp:Content>
