﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="presentacion.aspx.vb" Inherits="FARM_ADM.presentacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Forma Farmaceutica:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px; ">
             Buscar:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>
     <tr>
         <td colspan="2" class="alinearDer">
             <asp:Button runat="server" ID="e_btnEliminar" Text ="Eliminar" OnClientClick="return eliminar()" />
             <asp:Button runat="server" ID="a_btnNuevo" Text="Nuevo" />
         </td>
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwFormaFarmaceutica" DataSourceID="sqlFormaFarmaceutica">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                               <th style="width:25px;">
                                   &nbsp;
                               </th>
                                <th style="width:100px;">
                                    Registro
                                </th>
                                <th >
                                    Laboratorio
                                </th>                               
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />     
                            </td>
                            <td class="alinearCentro">
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("ID_FORMA_FARMACEUTICA")%>' CommandName="modificar" CommandArgument='<%# Eval("ID_FORMA_FARMACEUTICA")%>'></asp:LinkButton>
                            </td>
                            <td class="textoGris">
                                <asp:Label runat="server" ID="lblDescripcion" Text='<%# Eval("DESCRIPCION")%>'></asp:Label>                                
                            </td>                            
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwFormaFarmaceutica" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlFormaFarmaceutica" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM FORMA_FARMACEUTICA  
                        WHERE (DESCRIPCION  LIKE '%' + @NOMBRE + '%') ORDER BY DESCRIPCION">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
                    </ContentTemplate>
    </asp:UpdatePanel>
    <script src="../../JavaScript/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript">
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblDescripcion");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar la Forma Farmaceutica:" + lblDescripcion.innerHTML));
                }
            });
        }
    </script>
</asp:Content>
