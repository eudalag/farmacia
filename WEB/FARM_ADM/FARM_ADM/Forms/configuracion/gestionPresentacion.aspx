﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionPresentacion.aspx.vb" Inherits="FARM_ADM.gestionPresentacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;"> 
        <asp:Label ID="Label5" runat="server" Text="Gestion Forma Farmaceutica:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:150px; ">
             Descripcion:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtDescripcion" MaxLength="50" style="width:200px; "></asp:TextBox>             
         </td>
     </tr>
         <tr>
            <td colspan="2" class="alinearDer">
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" />
            </td>
        </tr>
   </table>
</asp:Content>
