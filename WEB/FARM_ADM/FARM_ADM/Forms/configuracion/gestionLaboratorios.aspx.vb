﻿Public Class gestionLaboratorios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos(ByVal id As Integer)
        Dim sSql As String = ""
        sSql += " SELECT * FROM LABORATORIO WHERE ID_LABORATORIO = " + id.ToString
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtDescripcion.Text = tbl.Rows(0)("DESCRIPCION")
            txtUtilidad.Text = String.Format(New Globalization.CultureInfo("en-Us"), "{0:##0.00}", tbl.Rows(0)("UTILIDAD"))
        End If
    End Sub
    Private Sub gestionLaboratorios_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If
        End If
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("laboratorio.aspx")
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Dim tbl = New mcTabla()
            tbl.tabla = "LABORATORIO"
            tbl.campoID = "ID_LABORATORIO"
            tbl.modoID = "auto"
            tbl.agregarCampoValor("DESCRIPCION", txtDescripcion.Text)
            tbl.agregarCampoValor("UTILIDAD", Decimal.Parse(txtUtilidad.Text, New Globalization.CultureInfo("en-US")))
            If Request.QueryString("ID") Is Nothing Then
                tbl.insertar()
            Else
                tbl.valorID = Request.QueryString("ID")
                tbl.modificar()
            End If
            Response.Redirect("laboratorio.aspx")
        End If
    End Sub
End Class