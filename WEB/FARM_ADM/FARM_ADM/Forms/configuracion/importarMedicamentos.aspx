﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="importarMedicamentos.aspx.vb" Inherits="FARM_ADM.importarMedicamentos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    .sinStock
    {
        background-color: red;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Importar Medicamentos:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px; ">
             Buscar:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>
     <tr>
         <td colspan="2" class="alinearDer">             
             <asp:Button runat="server" ID="a_btnNuevo" Text="Importar Medicamentos" />
         </td>
     </tr>
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwLaboratorios" DataSourceID="sqlLaboratorios">
                    <LayoutTemplate>
                        <table class="generica">
                            <tr>                                
                                <th style="width:100px;">
                                    Codigo de Barra
                                </th>
                                <th >
                                    Medicamento
                                </th> 
                                <th style="width:120px;">
                                    Laboratorio
                                </th> 
                                <th style="width:150px;">
                                    Nombre Generico
                                </th> 
                                <th style="width:50px;">
                                    Precio
                                </th>      
<th style="width:50px;">
                                    Precio 1 
                                </th> 
<th style="width:50px;">
                                    Precio 2
                                </th > 
<th style="width:30px;">
                                    Cant.
                                </th>    
<th style="width:80px;">
                                    Lote
                                </th>                      
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# IIF(Cint(EVAL("CANTIDAD")) = 0,"sinStock","normal") %>'> 

                            <td class="textoGris">
                                <%# Eval("CODIGO_BARRA")%>
                            </td>
<td class="textoGris">
                                <%# Eval("MEDICAMENTO")%>
                            </td>
<td class="textoGris">
                                <%# Eval("LABORATORIO")%>
                            </td>
<td class="textoGris">
                                <%# Eval("NOMBRE_GENERICO")%>
                            </td>
<td class="textoGris">
                                <%# Eval("PRECIO_NORMAL")%>
                            </td>
<td class="textoGris">
                                <%# Eval("PRECIO_2")%>
                            </td>
<td class="textoGris">
                                <%# Eval("PRECIO_3")%>
                            </td>
<td class="textoGris">
                                <%# Eval("CANTIDAD")%>
                            </td>      
                            <td class="textoGris">
                                <%# Eval("LOTE")%>
                            </td>                 
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwLaboratorios" PageSize="15"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlLaboratorios" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString  %>" 
                        SelectCommand="SELECT * FROM IMPORTAR 
                        WHERE (MEDICAMENTO  LIKE '%' + @NOMBRE + '%') OR NOMBRE_GENERICO  LIKE '%' + @NOMBRE + '%' ORDER BY MEDICAMENTO">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>
</asp:Content>
