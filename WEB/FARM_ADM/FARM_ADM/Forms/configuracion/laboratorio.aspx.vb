﻿Imports System.Transactions
Public Class laboratorio1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionLaboratorios.aspx")
    End Sub

    Protected Sub lvwLaboratorios_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwLaboratorios.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionLaboratorios.aspx?ID=" + e.CommandArgument)
        End If
    End Sub

    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click
        For Each item In lvwLaboratorios.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                Dim id = CType(item.FindControl("m_btnEditar"), LinkButton).Text
                Using scope As New TransactionScope()
                    Try
                        Dim msg As String = ""
                        If SePuedeEliminar("LABORATORIO", id, msg) Then
                            Dim tbl = New mcTabla()
                            tbl.tabla = "LABORATORIO"
                            tbl.campoID = "ID_LABORATORIO"
                            tbl.valorID = id
                            tbl.eliminar()
                            scope.Complete()
                        Else
                            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('" + msg + "');", True)
                        End If

                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('Error: Se ha producido un error de base de datos al Eliminar.');", True)
                    End Try
                End Using
                lvwLaboratorios.DataBind()
            End If
        Next
    End Sub
End Class