﻿Imports System.Transactions
Public Class presentacion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub lvwFormaFarmaceutica_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwFormaFarmaceutica.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionPresentacion.aspx?ID=" + e.CommandArgument)
        End If
    End Sub

    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionPresentacion.aspx")
    End Sub

    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click
        For Each item In lvwFormaFarmaceutica.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                Dim id = CType(item.FindControl("m_btnEditar"), LinkButton).Text
                Using scope As New TransactionScope()
                    Try
                        Dim msg As String = ""
                        If SePuedeEliminar("FORMA_FARMACEUTICA", id, msg) Then
                            Dim tbl = New mcTabla()
                            tbl.tabla = "FORMA_FARMACEUTICA"
                            tbl.campoID = "ID_FORMA_FARMACEUTICA"
                            tbl.valorID = id
                            tbl.eliminar()
                            scope.Complete()
                        Else
                            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('" + msg + "');", True)
                        End If

                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('Error: Se ha producido un error de base de datos al Eliminar.');", True)
                    End Try
                End Using
                lvwFormaFarmaceutica.DataBind()
            End If
        Next
    End Sub
End Class