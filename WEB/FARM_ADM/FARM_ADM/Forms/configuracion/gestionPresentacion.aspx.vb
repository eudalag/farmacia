﻿Public Class gestionPresentacion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub cargarDatos(ByVal id As Integer)
        Dim sSql As String = ""
        sSql += " SELECT * FROM FORMA_FARMACEUTICA WHERE ID_FORMA_FARMACEUTICA = " + id.ToString
        Dim tbl = New mcTabla().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtDescripcion.Text = tbl.Rows(0)("DESCRIPCION")
        End If
    End Sub
    Private Sub gestionLaboratorios_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            If Not Request.QueryString("ID") Is Nothing Then
                cargarDatos(Request.QueryString("ID"))
            End If
        End If
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Response.Redirect("presentacion.aspx")
    End Sub

    Protected Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Page.IsValid Then
            Dim tbl = New mcTabla()
            tbl.tabla = "FORMA_FARMACEUTICA"
            tbl.campoID = "ID_FORMA_FARMACEUTICA"
            tbl.modoID = "sql"
            tbl.agregarCampoValor("DESCRIPCION", txtDescripcion.Text)

            If Request.QueryString("ID") Is Nothing Then
                tbl.insertar()
            Else
                tbl.valorID = Request.QueryString("ID")
                tbl.modificar()
            End If
            Response.Redirect("presentacion.aspx")
        End If
    End Sub
End Class