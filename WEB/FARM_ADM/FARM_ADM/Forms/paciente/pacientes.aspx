﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="pacientes.aspx.vb" Inherits="FARM_ADM.pacientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Pacientes:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr>
     <tr>
         <td class="textoGris" style="width:50px; ">
             Buscar:
         </td>
         <td>
             <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
             <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
         </td>
     </tr>     
        <tr>
            <td colspan="2">
                <asp:ListView runat="server" ID="lvwBaseDatos" DataSourceID="sqlBaseDatos">
                    <LayoutTemplate>
                        <table class="generica" style="width:100%;">
                            <tr>
                                <th>
                                    Paciente    
                                </th>
                                <th style="width:150px;">
                                    Fecha de Nacimiento
                                </th>                                
                            </tr>
                            <tr>
                                <td runat="server" id="itemPlaceHolder">
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="m_btnEditar" Text='<%# Eval("NOMBRE_APELLIDOS")%>' CommandName="modificar" CommandArgument='<%# Eval("ID_PACIENTE")%>'></asp:LinkButton>
                            </td>
                            <td class="textoGris">
                                <%# String.Format(New Globalization.CultureInfo("es-ES"), "0:d", Eval("F_NACIMIENTO"))%>
                            </td>                            
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
                <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwBaseDatos" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager> 
                    <asp:SqlDataSource runat="server" ID="sqlBaseDatos" 
                        ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                        SelectCommand="SELECT * FROM PACIENTE
                        WHERE ([NOMBRE_APELLIDOS] LIKE '%' + @NOMBRE + '%' OR CI LIKE '%' + @NOMBRE + '%' )
                        ORDER BY NOMBRE_APELLIDOS">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </td>
        </tr>
        </table>

</asp:Content>
