﻿Imports System.Transactions
Public Class proveedores1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub a_btnNuevo_Click(sender As Object, e As EventArgs) Handles a_btnNuevo.Click
        Response.Redirect("gestionProveedores.aspx?" + Request.QueryString.ToString)
    End Sub

    Protected Sub e_btnEliminar_Click(sender As Object, e As EventArgs) Handles e_btnEliminar.Click
        For Each item In lvwGrilla.Items
            If CType(item.FindControl("chkEliminar"), HtmlInputCheckBox).Checked Then
                Dim id = CType(item.FindControl("m_btnEditar"), LinkButton).CommandArgument
                Using scope As New TransactionScope()
                    Try
                        Dim msg As String = ""
                        If SePuedeEliminar("PROVEEDORES", id, msg) Then
                            Dim tbl = New mcTabla()
                            tbl.tabla = "PROVEEDORES_LABORATORIO"
                            tbl.campoID = "ID_PROVEEDOR"
                            tbl.valorID = id
                            tbl.eliminar()
                            tbl.reset()
                            tbl.tabla = "PROVEEDORES"
                            tbl.campoID = "ID_PROVEEDOR"
                            tbl.valorID = id
                            tbl.eliminar()
                            scope.Complete()
                        Else
                            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "alert('" + msg + "');", True)
                        End If

                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "error2", "alert('Error: Se ha producido un error de base de datos al Eliminar.');", True)
                    End Try
                End Using
                lvwGrilla.DataBind()
            End If
        Next
    End Sub

    Protected Sub lvwGrilla_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwGrilla.ItemCommand
        If e.CommandName = "modificar" Then
            Response.Redirect("gestionProveedores.aspx?" + Request.QueryString.ToString + "&ID=" + e.CommandArgument)
        End If
    End Sub
End Class