﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="buscarLaboratorio.aspx.vb" Inherits="FARM_ADM.buscarLaboratorio" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>                        
    <div>
        <asp:ListView runat="server" ID="lvwLaboratorio" DataSourceID="sqlLaboratorio">
            <LayoutTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                        <th style="width:150px;">
                            Registro
                        </th>
                        <th>
                            Laboratorio
                        </th>                        
                    </tr>
                    <tr runat="server" id="itemPlaceHolder"></tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" id="btnMedicamento" Text='<%# Eval("ID_LABORATORIO")%>' CommandArgument='<%# Eval("ID_LABORATORIO")%>' CommandName='seleccionar' ></asp:LinkButton>                        
                    </td>
                    <td class="textoGris">
                        <asp:Label runat="server" ID="lblDescripcion" Text='<%# Eval("DESCRIPCION")%>'></asp:Label>                        
                    </td>                                         
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <table style="width:100%" class="generica">
                    <tr>
                       <th style="width:150px;">
                            Registro
                        </th>
                        <th>
                            Laboratorio
                        </th>   
                    </tr>
                    <tr>
                        <td colspan="4" class="textoGris">
                            No Existen Datos.
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
        </asp:ListView>
        <div align="right"> 
                  <asp:DataPager ID="DataPager1" runat="server"
                                 PagedControlID="lvwLaboratorio" PageSize="10"> 
                    <Fields> 
                      <asp:NextPreviousPagerField ButtonType="Image"
                                                  ShowFirstPageButton="True"
                                                  ShowLastPageButton="True"
                                                  FirstPageImageUrl="~/App_Themes/estandar/imagenes/First.png"
                                                  FirstPageText=""
                                                  LastPageImageUrl="~/App_Themes/estandar/imagenes/Last.png"
                                                  LastPageText=""
                                                  NextPageImageUrl="~/App_Themes/estandar/imagenes/Next.png"
                                                  NextPageText=""
                                                  PreviousPageImageUrl="~/App_Themes/estandar/imagenes/Previous.png"
                                                  PreviousPageText=""
                                                  RenderDisabledButtonsAsLabels="True" />                      
                    </Fields> 
                  </asp:DataPager>                     
                </div>
         <asp:SqlDataSource ID="sqlLaboratorio" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT L.ID_LABORATORIO,L.DESCRIPCION, PL.ID_PROVEEDOR  FROM LABORATORIO L
                          LEFT JOIN  PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO = L.ID_LABORATORIO 
                          WHERE ISNULL(PL.ID_PROVEEDOR,-1) <> @ID AND L.DESCRIPCION LIKE '%' + @PROVEEDOR + '%' AND ESTADO = 1 ORDER BY DESCRIPCION ">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="PROVEEDOR" PropertyName="Text" Type="String" />
                             <asp:QueryStringParameter QueryStringField="ID" Name="ID" />
                         </SelectParameters>                         
                     </asp:SqlDataSource>
        <div>
            <span class="textoGris">
                Buscar:
            </span>
            <span>
                <asp:TextBox runat="server" ID="txtBuscar" style="width:250px; " MaxLength="100"></asp:TextBox>
                <asp:Button runat="server" ID="btnBuscar" Text="Buscar" />
            </span>
        </div>        
    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </form>    
    <script type="text/javascript">
        function cerrarInformacion() {
            parent.cancelarModal();
            return false;
        }
    </script>
</body>
</html>
