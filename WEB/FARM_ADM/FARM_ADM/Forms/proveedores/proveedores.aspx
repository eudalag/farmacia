﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="proveedores.aspx.vb" Inherits="FARM_ADM.proveedores1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <table style="width:100%">
    <tr>
      <td colspan="2"> 
        <div class="alinearIzq" style="padding-bottom:10px;""> 
        <asp:Label ID="Label5" runat="server" Text="Proveedores:" CssClass="textoNegroNegrita" ></asp:Label> 
        <hr width="30%"  align="left" style="height:-12px;color:#CCCCCC"/>
        </div>
      </td>
    </tr> 
         <tr>
             <td class="textoGris" style="width:50px; ">Buscar: </td>
             <td>
                 <asp:TextBox ID="txtBuscar" runat="server" MaxLength="100" style="width:250px; "></asp:TextBox>
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar" />
             </td>
         </tr>
         <tr>
             <td class="alinearDer" colspan="2">
                 <asp:Button ID="e_btnEliminar" runat="server" OnClientClick="return eliminar()" Text="Eliminar" />
                 <asp:Button ID="a_btnNuevo" runat="server" Text="Nuevo" />
             </td>
         </tr>
         <tr>
             <td colspan="2">
                 <asp:ListView ID="lvwGrilla" runat="server" DataSourceID="sqlProveedores">
                     <LayoutTemplate>
                         <table class="generica" style="width:100%;">
                             <tr>
                                 <th style="width:25px;">&nbsp; </th>
                                 <th style="width:60px;">Registro </th>
                                 <th>Proveedor </th>
                                 <th style="width:120px;">Nit </th>
                                 <th style="width:150px;">Direccion </th>
                                 <th style="width:120px;">Celular </th>
                             </tr>
                             <tr>
                                 <td id="itemPlaceHolder" runat="server"></td>
                             </tr>
                         </table>
                     </LayoutTemplate>
                     <ItemTemplate>
                         <tr>
                             <td>
                                 <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />
                             </td>
                             <td class="alinearCentro">
                                 <asp:LinkButton ID="m_btnEditar" runat="server" CommandArgument='<%# Eval("ID_PROVEEDOR")%>' CommandName="modificar" Text='<%# Eval("ID_PROVEEDOR")%>'></asp:LinkButton>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblDescripcion" runat="server" Text='<%# Eval("NOMBRE")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblConcentracion" runat="server" Text='<%# Eval("NIT")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblPresentacion" runat="server" Text='<%# Eval("DIRECCION")%>'></asp:Label>
                             </td>
                             <td class="textoGris">
                                 <asp:Label ID="lblLaboratorio" runat="server" Text='<%# Eval("CELULAR")%>'></asp:Label>
                             </td>
                         </tr>
                     </ItemTemplate>
                 </asp:ListView>
                 <div align="right">
                     <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvwGrilla" PageSize="10">
                         <Fields>
                             <asp:NextPreviousPagerField ButtonType="Image" FirstPageImageUrl="~/App_Themes/estandar/imagenes/botones/First.png" FirstPageText="" LastPageImageUrl="~/App_Themes/estandar/imagenes/botones/Last.png" LastPageText="" NextPageImageUrl="~/App_Themes/estandar/imagenes/botones/Next.png" NextPageText="" PreviousPageImageUrl="~/App_Themes/estandar/imagenes/botones/Previous.png" PreviousPageText="" RenderDisabledButtonsAsLabels="True" ShowFirstPageButton="True" ShowLastPageButton="True" />
                         </Fields>
                     </asp:DataPager>
                     <asp:SqlDataSource ID="sqlProveedores" runat="server" 
                         ConnectionString="<%$ ConnectionStrings:FARM_ADM.My.MySettings.BDFarmConnectionString %>" 
                         SelectCommand="SELECT ID_PROVEEDOR, NOMBRE, NIT, DIRECCION, CELULAR FROM PROVEEDORES 
                         WHERE NOMBRE <> '[Varios]' AND (NOMBRE LIKE '%' + @NOMBRE + '%' OR NIT LIKE '%' + @NOMBRE + '%')
                         ORDER BY NOMBRE, ID_PROVEEDOR ">
                         <SelectParameters>
                             <asp:ControlParameter ControlID="txtBuscar" DefaultValue="%" Name="NOMBRE" PropertyName="Text" Type="String" />
                         </SelectParameters>                         
                     </asp:SqlDataSource>
                 </div>
             </td>
         </tr>     
        </table>
     <script src="../../JavaScript/jcalendar/js/jquery.min.js"></script>
    <script type="text/javascript">
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblDescripcion");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar el Proveedor:" + lblDescripcion.innerHTML));
                }
            });
        }
    </script>
</asp:Content>
