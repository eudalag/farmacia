﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/farm_adm.Master" CodeBehind="gestionProveedores.aspx.vb" Inherits="FARM_ADM.gestionProveedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <table style="width:100%;">
        <tr>
          <td colspan="2"> 
            <div class="alinearIzq" style="padding-bottom:10px;"> 
            <asp:Label ID="Label5" runat="server" Text="Gestion Proveedores:" CssClass="textoNegroNegrita" ></asp:Label> 
            <hr style="height:-12px;color:#CCCCCC;width:30%;text-align:left;" align="left" />
            </div>
          </td>
        </tr>
        <tr>
            <td class="textoGris" style="width:130px;">
                Nombre o Razon Social:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNombre" MaxLength="50" Columns="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="(*) Campo Obligatorio" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
                <asp:HiddenField runat="server" ID="hdIdProveedor" Value="0" />
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Nit:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNit" MaxLength="10" Columns="15" ></asp:TextBox>
                
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Direccion:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtDireccion_" MaxLength="150"  Columns="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Celular:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCelular" MaxLength="25" Columns="25"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="textoGris">
                Correo:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtCorreo" MaxLength="50" Columns="50" ></asp:TextBox>
            </td>
        </tr>   
        <tr>
            <td class="textoGris">
                Fax:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtFax" MaxLength="25" Columns="25"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="textoGris">
                Laboratorios:
           </td>
        </tr>
        <tr>
            <td colspan="2" class="alinearDer">
                <asp:Button runat="server" ID="btnEliminar" Text="Eliminar" OnClientClick="return eliminar()" CausesValidation="false" />
                <asp:Button runat="server" ID="Nuevo" Text="Nuevo" OnClientClick="return buscarLaboratorio();" /> 
                <asp:HiddenField runat="server" id="hdIdLaboratorio" Value="-1" />
                        <asp:HiddenField runat="server" id="hdDescripcion" Value="-1" />
                        <asp:Button runat="server" ID="btnCargar" Text="cargar" style="display:none;" CausesValidation="false" />              
            </td>    
         </tr>                  
        <tr>
            <td colspan="2">
                 <asp:ListView runat="server" ID="lvwLaboratorio">
                                <LayoutTemplate>
                                    <table style="width:100%" class="generica">
                                        <tr>
                                            <th style="width:15px;">
                                                &nbsp;
                                            </th>
                                            <th class="textoGris" style="width:100px;">
                                                Registro
                                            </th>
                                            <th class="textoGris">
                                                Laboratorio
                                            </th>
                                        </tr>
                                        <tr runat="server" id="itemPlaceHolder"></tr>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <input type="checkbox" runat="server" id="chkEliminar" class="checked textoNegro" onchange="seleccionar_item(this)" />
                                            <asp:HiddenField runat="server" ID="hfNRO_FILA" Value='<%# Eval("NRO_FILA")%>' />
                                             <asp:HiddenField runat="server" ID="hfIdLaboratorio" Value='<%# Eval("ID_LABORATORIO")%>' />
                                        </td>
                                        <td class="textoGris">
                                            <%# Eval("ID_LABORATORIO")%>
                                        </td>
                                        <td class="textoGris">
                                            <asp:Label runat="server" ID="lblDescripcion" Text='<%# Eval("DESCRIPCION")%>'></asp:Label>
                                            
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <table style="width:100%" class="generica">
                                        <tr>
                                            <th style="width:15px;">
                                                &nbsp;
                                            </th>
                                           <th class="textoGris" style="width:100px;">
                                                Registro
                                            </th>
                                            <th class="textoGris">
                                                Laboratorio
                                            </th>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="textoNegro alinearCentro">
                                                No existen Datos
                                            </td>
                                        </tr>
                                        </table>
                                </EmptyDataTemplate>
                            </asp:ListView>
                <asp:CustomValidator ID="cvSeleccionoItem" runat="server" ErrorMessage="(*) Debe Seleccionar Laboratorios" ClientValidationFunction="seleccionoItem"></asp:CustomValidator>
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblError" Text="" CssClass="textoRojo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="alinearDer" >
                <asp:Button runat="server" ID="btnAceptar" Text="Aceptar" />
                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar" CausesValidation="false" />
            </td>
        </tr>

    </table>
    <link href="../../scripts/jcalendar/css/jquery-ui.css" rel="stylesheet" />
    <script src="../../scripts/jcalendar/js/jquery.min.js"></script>
    <script src="../../scripts/jcalendar/js/jquery-ui.min.js"></script>
    <script type="text/javascript">
        function cancelarModal() {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");

        }
        function aceptarModal(idLaboratorio, descripcion) {
            jQuery.noConflict();
            jQuery("#jqueryuidialog").dialog("close");
            jQuery("#<%=hdIdLaboratorio.ClientID%>").val(idLaboratorio);
            jQuery("#<%=hdDescripcion.ClientID%>").val(descripcion);
            jQuery("#<%=btnCargar.ClientID%>").click()
            return false;
        }
        function buscarLaboratorio() {
            jQuery.noConflict();
            var dialog = jQuery('<div id="jqueryuidialog" style="resize:none;"><iframe width="100%" height="100%" frameborder="0"></iframe></div>').hide();
            var frame = dialog.children("iframe");
            var url = "buscarLaboratorio.aspx?ID=" + jQuery("#<%=hdIdProveedor.ClientID%>").val();
            frame.attr("src", url);
            dialog.dialog({ modal: true, resizable: false, autoOpen: false, title: "BUSCAR LABORATORIO", width: 600, height: 400 });
            dialog.dialog('open');
            dialog.bind('dialogclose', function () {
                jQuery(this).dialog('destroy').remove(); // this line causes error, without it we have no error but we collect dialog iframes in DOM
            });
            return false;
        }
        function seleccionar_item(obj) {
            var estado = obj.checked;
            jQuery(".checked").each(function () {
                jQuery(this).attr("checked", false);
            });
            var chk = jQuery(obj).attr("checked", estado);
            return false;
        }
        function eliminar() {
            var idremplazo = '';
            var estado = '';
            var descEstado = '';
            var result = false;
            jQuery(".checked").each(function () {
                var chk = jQuery(this).is(':checked');
                if (chk) {
                    id = jQuery(this).attr('id');
                    idremplazo = id.replace("chkEliminar", "lblDescripcion");
                    var lblDescripcion = $get(idremplazo);
                    result = (confirm("Desea eliminar el Laboratorio: " + lblDescripcion.innerHTML));
                }
            });
            if (idremplazo == '') {
                alert("Seleccione un laboratorio a eliminar.");
                return false;
            }
            return result;
        }
        function seleccionoItem(source, args) {
            var cont = 0;
            jQuery(".checked").each(function () {
                cont++;
            });
            if (cont == 0) {
                args.IsValid = false;
            } else {
                args.IsValid = true;
            }

        }      
    </script>
</asp:Content>
