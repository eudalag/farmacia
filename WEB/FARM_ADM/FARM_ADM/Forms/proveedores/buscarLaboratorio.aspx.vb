﻿Public Class buscarLaboratorio
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Protected Sub lvwLaboratorio_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles lvwLaboratorio.ItemCommand
        Select Case e.CommandName
            Case "seleccionar"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "seleccionar", "parent.aceptarModal('" + e.CommandArgument + "','" + CType(e.Item.FindControl("lblDescripcion"), Label).Text + "');", True)
        End Select
    End Sub

    Private Sub buscarLaboratorio_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            lvwLaboratorio.DataBind()
            txtBuscar.Focus()
        End If
    End Sub
End Class