﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/cappias_camiri.Master" CodeBehind="Oops.aspx.vb" Inherits="Cappias_Camiri.Oops" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        Un Error a Ocurrido</h2>
        <br />
    <p>
        Ocurrió un error inesperado en nuestro sitio web. El administrador del sitio web ha sido notificado.</p>
    <ul><li><asp:HyperLink ID="lnkHome" runat="server" NavigateUrl="~/Forms/Sistema.aspx">Retornar a la Pagina Principal</asp:HyperLink></li></ul>
</asp:Content>
