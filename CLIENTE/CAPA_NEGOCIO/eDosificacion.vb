﻿Imports CAPA_DATOS
Imports System.Data
Public Class eDosificacion
    Private _ID_DOSIFICACION
    Private _NUMERO_AUTORIZACION
    Private _F_LIMITE_EMISION
    Private _LLAVE_DOSIFICACION
    Private _N_FACT_INICIAL
    Private _ACTIVO
    Private _ERROR As String

    Public Property ID_DOSIFICACION
        Get
            Return _ID_DOSIFICACION
        End Get
        Set(value)
            _ID_DOSIFICACION = value
        End Set
    End Property

    Public Property NUMERO_AUTORIZACION
        Get
            Return _NUMERO_AUTORIZACION
        End Get
        Set(value)
            _NUMERO_AUTORIZACION = value
        End Set
    End Property

    Public Property F_LIMITE_EMISION
        Get
            Return _F_LIMITE_EMISION
        End Get
        Set(value)
            _F_LIMITE_EMISION = value
        End Set
    End Property

    Public Property LLAVE_DOSIFICACION
        Get
            Return _LLAVE_DOSIFICACION
        End Get
        Set(value)
            _LLAVE_DOSIFICACION = value
        End Set
    End Property

    Public Property N_FACT_INICIAL
        Get
            Return _N_FACT_INICIAL
        End Get
        Set(value)
            _N_FACT_INICIAL = value
        End Set
    End Property

    Public Property ACTIVO
        Get
            Return _ACTIVO
        End Get
        Set(value)
            _ACTIVO = value
        End Set
    End Property

    Public ReadOnly Property mensajesError() As String
        Get
            Return _ERROR
        End Get
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal idDosificacion As Integer)
        _ID_DOSIFICACION = idDosificacion
        cargarDatos()
    End Sub


    Public Sub insertar()
        If Not existeDosificacionActiva() Then
            Dim datos = New datos()
            datos.tabla = "DOSIFICACION"
            datos.campoID = "ID_DOSIFICACION"
            datos.modoID = "auto"            
            datos.agregarCampoValor("NUMERO_AUTORIZACION", _NUMERO_AUTORIZACION)
            datos.agregarCampoValor("F_LIMITE_EMISION", _F_LIMITE_EMISION)
            datos.agregarCampoValor("LLAVE_DOSIFICACION", _LLAVE_DOSIFICACION)
            datos.agregarCampoValor("N_FACT_INICIAL", _N_FACT_INICIAL)
            datos.agregarCampoValor("ACTIVO", _ACTIVO)
            datos.insertar()
            _ID_DOSIFICACION = datos.valorID
            _ERROR = ""
        Else
            _ERROR = "El sistema presenta una dosificación activa."
        End If

    End Sub
    Public Sub modificar()
        If Not existeDosificacionActiva(_ID_DOSIFICACION) Then

            Dim datos = New datos()
            datos.tabla = "DOSIFICACION"
            datos.campoID = "ID_DOSIFICACION"
            datos.valorID = _ID_DOSIFICACION
            datos.agregarCampoValor("NUMERO_AUTORIZACION", _NUMERO_AUTORIZACION)
            datos.agregarCampoValor("F_LIMITE_EMISION", _F_LIMITE_EMISION)
            datos.agregarCampoValor("LLAVE_DOSIFICACION", _LLAVE_DOSIFICACION)
            datos.agregarCampoValor("N_FACT_INICIAL", _N_FACT_INICIAL)
            datos.agregarCampoValor("ACTIVO", _ACTIVO)
            datos.modificar()
            _ERROR = ""
        Else
            _ERROR = "El sistema presenta una dosificación activa."
        End If
    End Sub
    Public Sub eliminar()
        If puedoEliminar(_ID_DOSIFICACION) Then
            Dim datos = New datos()
            datos.tabla = "DOSIFICACION"
            datos.campoID = "ID_DOSIFICACION"
            datos.valorID = _ID_DOSIFICACION
            datos.eliminar()
        Else
            _ERROR = "La dosificación que desea eliminar presenta facturas en sistema."
        End If
        
    End Sub
    Public Sub cargarDatos()
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM DOSIFICACION WHERE ID_DOSIFICACION = " & _ID_DOSIFICACION
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _NUMERO_AUTORIZACION = tblConsulta.Rows(0)("NUMERO_AUTORIZACION")
            _F_LIMITE_EMISION = tblConsulta.Rows(0)("F_LIMITE_EMISION")
            _LLAVE_DOSIFICACION = tblConsulta.Rows(0)("LLAVE_DOSIFICACION")
            _N_FACT_INICIAL = tblConsulta.Rows(0)("N_FACT_INICIAL")
            _ACTIVO = tblConsulta.Rows(0)("ACTIVO")
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Sub cargarDatosDosificacionActiva()
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM DOSIFICACION WHERE ACTIVO = 1 AND F_LIMITE_EMISION >= GETDATE()"
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_DOSIFICACION = tblConsulta.Rows(0)("ID_DOSIFICACION")
            _NUMERO_AUTORIZACION = tblConsulta.Rows(0)("NUMERO_AUTORIZACION")
            _F_LIMITE_EMISION = tblConsulta.Rows(0)("F_LIMITE_EMISION")
            _LLAVE_DOSIFICACION = tblConsulta.Rows(0)("LLAVE_DOSIFICACION")
            _N_FACT_INICIAL = tblConsulta.Rows(0)("N_FACT_INICIAL")
            _ACTIVO = tblConsulta.Rows(0)("ACTIVO")
        Else
            _ERROR = "No Existe Dosificación Valida para realizar la Facturación."
        End If
    End Sub
    Public Function mostrarDosificacion() As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT *, CASE WHEN ACTIVO = 1 THEN 'Si' ELSE 'No' END AS ACTIVO_M FROM DOSIFICACION ORDER BY F_LIMITE_EMISION DESC"
        Return datos.ejecutarConsulta(sSql)
    End Function

    Private Function existeDosificacionActiva(Optional ByVal idDosificacion As Integer = -1) As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "SELECT COUNT (*) FROM DOSIFICACION WHERE ACTIVO = 1 "
        '"AND F_LIMITE_EMISION <= CONVERT(DATE,GETDATE(),103)"
        If idDosificacion <> -1 Then
            sSql += " AND ID_DOSIFICACION <> " & idDosificacion
        End If
        Return CInt(datos.ejecutarConsulta(sSql).Rows(0)(0)) > 0
    End Function
    Private Function puedoEliminar(ByVal idDosificacion As Integer) As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "SELECT COUNT(*) FROM VENTA WHERE ID_DOSIFICACION =  " & idDosificacion.ToString        
        Return CInt(datos.ejecutarConsulta(sSql).Rows(0)(0)) = 0
    End Function


End Class
