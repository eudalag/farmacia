﻿Imports CAPA_DATOS
Imports System.Data
Public Class eMedicamento
    Private _ID_MEDICAMENTO As Integer
    Private _NOMBRE As String
    Private _COMPOSICION As String
    Private _MINIMO As Integer
    Private _ACTIVO As Boolean
    Private _ID_TIPO_MEDICAMENTO As Integer
    Private _TIPO_MEDICAMENTO As eTipoMedicamento
    Private _PRECIO_UNITARIO As Decimal
    Private _OBSERVACION As String
    Private _MARCA As String
    Private _CONTROLADO As Boolean
    Private _ID_LABORATORIO As Integer
    Private _CONCENTRACION As String
    Private _PRECIO_PAQ As Decimal
    Private _POR_PAQ As Decimal
    Private _POR_UNITARIO As Decimal
    Private _CANTIDAD_PAQUETE As Integer
    Private _VENTA_MAYOR As Boolean


    Private _ERROR As String
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    Public Property ID_MEDICAMENTO() As Integer
        Get
            Return _ID_MEDICAMENTO
        End Get
        Set(ByVal value As Integer)
            _ID_MEDICAMENTO = value
        End Set
    End Property
    Public Property NOMBRE() As String
        Get
            Return _NOMBRE
        End Get
        Set(ByVal value As String)
            _NOMBRE = value
        End Set
    End Property
    Public Property COMPOSICION() As String
        Get
            Return _COMPOSICION
        End Get
        Set(ByVal value As String)
            _COMPOSICION = value
        End Set
    End Property

    Public Property OBSERVACION() As String
        Get
            Return _OBSERVACION
        End Get
        Set(ByVal value As String)
            _OBSERVACION = value
        End Set
    End Property
    Public Property MARCA() As String
        Get
            Return _MARCA
        End Get
        Set(ByVal value As String)
            _MARCA = value
        End Set
    End Property
    Public Property CONTROLADO() As Boolean
        Get
            Return _CONTROLADO
        End Get
        Set(ByVal value As Boolean)
            _CONTROLADO = value
        End Set
    End Property
    Public Property MINIMO() As Integer
        Get
            Return _MINIMO
        End Get
        Set(ByVal value As Integer)
            _MINIMO = value
        End Set
    End Property
    Public Property ACTIVO() As Boolean
        Get
            Return _ACTIVO
        End Get
        Set(ByVal value As Boolean)
            _ACTIVO = value
        End Set
    End Property
    Public Property ID_TIPO_MEDICAMENTO() As Integer
        Get
            Return _ID_TIPO_MEDICAMENTO
        End Get
        Set(ByVal value As Integer)
            _ID_TIPO_MEDICAMENTO = value
        End Set
    End Property
    Public Property TIPO_MEDICAMENTO() As eTipoMedicamento
        Get
            Return _TIPO_MEDICAMENTO
        End Get
        Set(ByVal value As eTipoMedicamento)
            _TIPO_MEDICAMENTO = value
        End Set
    End Property
    Public Property PRECIO_UNITARIO() As Decimal
        Get
            Return _PRECIO_UNITARIO
        End Get
        Set(ByVal value As Decimal)
            _PRECIO_UNITARIO = value
        End Set
    End Property
    Public Property ID_LABORATORIO() As Integer
        Get
            Return _ID_LABORATORIO
        End Get
        Set(ByVal value As Integer)
            _ID_LABORATORIO = value
        End Set
    End Property
    Public Property CONCENTRACION() As String
        Get
            Return _CONCENTRACION
        End Get
        Set(ByVal value As String)
            _CONCENTRACION = value
        End Set
    End Property

    Public Property PRECIO_PAQUETE() As Decimal
        Get
            Return _PRECIO_PAQ
        End Get
        Set(ByVal value As Decimal)
            _PRECIO_PAQ = value
        End Set
    End Property

    Public Property PORC_UNITARIO() As Decimal
        Get
            Return _POR_UNITARIO
        End Get
        Set(ByVal value As Decimal)
            _POR_UNITARIO = value
        End Set
    End Property
    Public Property PORC_PAQUETE() As Decimal
        Get
            Return _POR_PAQ
        End Get
        Set(ByVal value As Decimal)
            _POR_PAQ = value
        End Set
    End Property
    Public Property CANTIDAD_PAQUETE() As Integer
        Get
            Return _CANTIDAD_PAQUETE
        End Get
        Set(ByVal value As Integer)
            _CANTIDAD_PAQUETE = value
        End Set
    End Property
    Public Property VENTA_MAYOR() As Boolean
        Get
            Return _VENTA_MAYOR
        End Get
        Set(ByVal value As Boolean)
            _VENTA_MAYOR = value
        End Set
    End Property
    Public Sub New()
        _ID_MEDICAMENTO = 0
        _TIPO_MEDICAMENTO = New eTipoMedicamento()
    End Sub
    Public Sub New(ByVal id_medicamento As Integer)
        cargarDatos(id_medicamento)
        _TIPO_MEDICAMENTO = New eTipoMedicamento(_ID_TIPO_MEDICAMENTO)
    End Sub
    Public Sub cargarDatos(ByVal id_medicamento)
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT NOMBRE,ID_TIPO_MEDICAMENTO,ISNULL(COMPOSICION,'') AS COMPOSICION,ISNULL(CONCENTRACION,'') AS CONCENTRACION,"
        sSql += " ISNULL(MARCA,'') AS MARCA,  PRECIO_UNITARIO, ISNULL(MINIMO,0)AS MINIMO,"
        sSql += " ISNULL(OBSERVACION ,'') AS OBSERVACION, CONTROLADO,ACTIVO,ID_LABORATORIO,"
        sSql += " ISNULL(PRECIO_PAQUETE,0) AS PRECIO_PAQUETE,"
        sSql += " ISNULL(CANTIDAD_PAQUETE,1) AS CANTIDAD_PAQUETE,ISNULL(VENTA_MAYOR,0) AS VENTA_MAYOR FROM MEDICAMENTO"
        sSql += " WHERE ID_MEDICAMENTO = " & id_medicamento
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_MEDICAMENTO = id_medicamento
            _NOMBRE = tblConsulta.Rows(0)("NOMBRE")
            _ID_TIPO_MEDICAMENTO = tblConsulta.Rows(0)("ID_TIPO_MEDICAMENTO")
            _ID_LABORATORIO = tblConsulta.Rows(0)("ID_LABORATORIO")
            _COMPOSICION = tblConsulta.Rows(0)("COMPOSICION")
            _CONCENTRACION = tblConsulta.Rows(0)("CONCENTRACION")
            _MARCA = tblConsulta.Rows(0)("MARCA")
            _PRECIO_UNITARIO = tblConsulta.Rows(0)("PRECIO_UNITARIO")
            _MINIMO = tblConsulta.Rows(0)("MINIMO")
            _OBSERVACION = tblConsulta.Rows(0)("OBSERVACION")
            _CONTROLADO = tblConsulta.Rows(0)("CONTROLADO")
            _ACTIVO = tblConsulta.Rows(0)("ACTIVO")
            _PRECIO_PAQ = tblConsulta.Rows(0)("PRECIO_PAQUETE")
            _CANTIDAD_PAQUETE = tblConsulta.Rows(0)("CANTIDAD_PAQUETE")
            _VENTA_MAYOR = tblConsulta.Rows(0)("VENTA_MAYOR")
            '_TIPO_MEDICAMENTO.cargarDatos(_ID_TIPO_MEDICAMENTO)
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "MEDICAMENTO"
        datos.campoID = "ID_MEDICAMENTO"
        datos.modoID = "auto"
        datos.agregarCampoValor("NOMBRE", _NOMBRE)
        datos.agregarCampoValor("COMPOSICION", _COMPOSICION)
        datos.agregarCampoValor("MINIMO", _MINIMO)
        datos.agregarCampoValor("ACTIVO", _ACTIVO)
        datos.agregarCampoValor("ID_TIPO_MEDICAMENTO", _ID_TIPO_MEDICAMENTO)
        datos.agregarCampoValor("PRECIO_UNITARIO", _PRECIO_UNITARIO)
        datos.agregarCampoValor("ID_LABORATORIO", _ID_LABORATORIO)
        datos.agregarCampoValor("OBSERVACION", _OBSERVACION)
        datos.agregarCampoValor("MARCA", _MARCA)
        datos.agregarCampoValor("CONTROLADO", _CONTROLADO)
        datos.agregarCampoValor("CONCENTRACION", _CONCENTRACION)
        datos.agregarCampoValor("PRECIO_PAQUETE", _PRECIO_PAQ)
        datos.agregarCampoValor("PORC_PAQUETE", _POR_PAQ)
        datos.agregarCampoValor("PORC_UNITARIO", _POR_UNITARIO)
        datos.agregarCampoValor("CANTIDAD_PAQUETE", _CANTIDAD_PAQUETE)
        datos.agregarCampoValor("VENTA_MAYOR", _VENTA_MAYOR)
        datos.insertar()
        _ID_MEDICAMENTO = datos.valorID
    End Sub
    Public Sub modificar()
        Dim datos = New datos()
        datos.tabla = "MEDICAMENTO"
        datos.campoID = "ID_MEDICAMENTO"
        datos.valorID = _ID_MEDICAMENTO
        datos.agregarCampoValor("NOMBRE", _NOMBRE)
        datos.agregarCampoValor("COMPOSICION", _COMPOSICION)
        datos.agregarCampoValor("MINIMO", _MINIMO)
        datos.agregarCampoValor("ACTIVO", _ACTIVO)
        datos.agregarCampoValor("ID_TIPO_MEDICAMENTO", _ID_TIPO_MEDICAMENTO)
        datos.agregarCampoValor("PRECIO_UNITARIO", _PRECIO_UNITARIO)
        datos.agregarCampoValor("ID_LABORATORIO", _ID_LABORATORIO)
        datos.agregarCampoValor("OBSERVACION", _OBSERVACION)
        datos.agregarCampoValor("MARCA", _MARCA)
        datos.agregarCampoValor("CONTROLADO", _CONTROLADO)
        datos.agregarCampoValor("CONCENTRACION", _CONCENTRACION)
        datos.agregarCampoValor("PRECIO_PAQUETE", _PRECIO_PAQ)
        datos.agregarCampoValor("PORC_PAQUETE", _POR_PAQ)
        datos.agregarCampoValor("PORC_UNITARIO", _POR_UNITARIO)
        datos.agregarCampoValor("CANTIDAD_PAQUETE", _CANTIDAD_PAQUETE)
        datos.agregarCampoValor("VENTA_MAYOR", _VENTA_MAYOR)
        datos.modificar()
    End Sub
    Public Sub eliminar()
        Dim datos = New datos()
        datos.tabla = "MEDICAMENTO"
        datos.campoID = "ID_MEDICAMENTO"
        datos.valorID = _ID_MEDICAMENTO
        datos.eliminar()
    End Sub
    Public Function mostrarMedicamento(Optional ByVal buscar As String = "", Optional stockCero As Boolean = False, Optional proveedor As String = "-1") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        buscar = buscar.Replace("'", "")
        buscar = buscar.Replace("--", "")
        Dim parametros = buscar.Split("*")

        sSql += " DECLARE @BUSCAR AS NVARCHAR(150),@TIPO_MEDICAMENTO AS INTEGER, @PROVEEDOR AS INTEGER,@ID_LABORATORIO AS INTEGER, @FORM_FARM AS NVARCHAR(50);"
        sSql += " SET @BUSCAR = '" + parametros(0).ToString + "';"
        sSql += " SET @PROVEEDOR =  " + proveedor + " ;"

        sSql += "  SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO, "
        sSql += "  UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END  AS MEDICAMENTO, "
        sSql += "  LABORATORIO, UPPER(FORMA_FARMACEUTICA) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION,  M.MINIMO,"
        sSql += "  ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO,  "
        sSql += "  ISNULL(KI.TOTAL,0) AS STOCK, CASE WHEN M.VENTA_MAYOR = 1 THEN ISNULL(M.CANTIDAD_PAQUETE,1) ELSE 1 END AS CANTIDAD_MAYOR, CASE WHEN M.VENTA_MAYOR = 1 THEN ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) ELSE M.PRECIO_UNITARIO END AS PRECIO_MAYOR, ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA,"
        sSql += "  ISNULL(CONVERT(NVARCHAR(10),KD.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO,M.VENTA_MAYOR "
        sSql += "  FROM VW_MEDICAMENTO M "
        If CInt(proveedor) <> -1 Then
            sSql += " INNER JOIN PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO = M.ID_LABORATORIO "
        End If        
        sSql += " LEFT JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY K.ID_MEDICAMENTO ORDER BY K.ID_MEDICAMENTO, K.F_VENCIMIENTO) AS NRO_KARDEX, K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS STOCK FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.SALDOS > 0"
        sSql += " GROUP BY K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO) KD ON KD.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND KD.NRO_KARDEX = 1"
        sSql += " INNER JOIN VW_INVENTARIO KI ON KI.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE M.MEDICAMENTO LIKE '%' + @BUSCAR + '%' OR M.COMPOSICION LIKE '%' + @BUSCAR + '%'"
        If CInt(proveedor) <> -1 Then
            sSql += "  AND PL.ID_PROVEEDOR = " + proveedor.ToString
            If Not stockCero Then
                sSql += "  AND ISNULL(S.CANTIDAD,0) > 0"
            End If
        Else
            If Not stockCero Then
                sSql += "  AND ISNULL(KD.STOCK,0) > 0"
            End If
        End If
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarInventarioMedicamento(Optional ByVal buscar As String = "", Optional ByVal tipoMedicamento As String = "-1") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT * FROM VW_INVENTARIO "
        If Trim(buscar) <> "" Then
            sSql += "WHERE NOMBRE LIKE '%" + buscar + "%' OR COMPOSICION LIKE '%" & buscar & "%'"
            If CInt(tipoMedicamento) <> -1 Then
                sSql += " AND ID_TIPO_MEDICAMENTO = " & tipoMedicamento
            End If
        Else
            If CInt(tipoMedicamento) <> -1 Then
                sSql += " WHERE ID_TIPO_MEDICAMENTO = " & tipoMedicamento
            End If
        End If
        sSql += " ORDER BY NOMBRE, DESCRIPCION "
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarInventarioCompra(Optional ByVal buscar As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += "SELECT  NOMBRE + '(' + DESC_TIPO_MEDICAMENTO + ')' AS NOMBRE_TIPO,ID_MEDICAMENTO,PRECIO_UNITARIO FROM VW_MEDICAMENTO WHERE ACTIVO = 1"
        If Trim(buscar) <> "" Then
            sSql += "AND NOMBRE LIKE '%" + buscar + "%' OR COMPOSICION LIKE '%" & buscar & "%'"
        End If
        sSql += " ORDER BY NOMBRE, COMPOSICION "
        Return datos.ejecutarConsulta(sSql)
    End Function
    Private Function puedeEliminar() As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT COUNT(*) FROM MEDICAMENTO M "
        sSql += " INNER JOIN COMPRA_MEDICAMENTO CM ON CM.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE M.ID_MEDICAMENTO = " & _ID_MEDICAMENTO
        sSql += " SELECT COUNT(*) FROM MEDICAMENTO M "
        sSql += " INNER JOIN RECETA R ON R.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE M.ID_MEDICAMENTO = " & _ID_MEDICAMENTO
        Dim ds As DataSet = datos.obtenerDataSet(sSql)
        Return (CInt(ds.Tables(0).Rows(0)(0)) = 0 And CInt(ds.Tables(1).Rows(0)(0)) = 0)
    End Function
End Class
