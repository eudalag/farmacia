﻿Imports CAPA_DATOS
Imports System.Data
Public Class eTipoMedicamento
    Private _ID_TIPO_MEDICAMENTO As Integer
    Private _DESCRIPCION As String
    Private _ACTIVO As Boolean
    Private _ERROR As String
    Public Property ID_TIPO_MEDICAMENTO() As Integer
        Get
            Return _ID_TIPO_MEDICAMENTO
        End Get
        Set(ByVal value As Integer)
            _ID_TIPO_MEDICAMENTO = value
        End Set
    End Property
    Public Property DESCRIPCION() As String
        Get
            Return _DESCRIPCION
        End Get
        Set(ByVal value As String)
            _DESCRIPCION = value
        End Set
    End Property
    Public Property ACTIVO As Boolean
        Set(value As Boolean)
            _ACTIVO = value
        End Set
        Get
            Return _ACTIVO
        End Get
    End Property
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property

    Public Sub New()
        _ID_TIPO_MEDICAMENTO = 0
    End Sub
    Public Sub New(ByVal id_tipo_medicamento As Integer)
        cargarDatos(id_tipo_medicamento)
    End Sub

    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "TIPO_MEDICAMENTO"
        datos.campoID = "ID_TIPO_MEDICAMENTO"
        datos.modoID = "auto"
        datos.agregarCampoValor("DESCRIPCION", _DESCRIPCION)
        datos.agregarCampoValor("ACTIVO", _ACTIVO)
        datos.insertar()
        _ID_TIPO_MEDICAMENTO = datos.valorID
    End Sub
    Public Sub modificar()        
        Dim datos = New datos()
        datos.tabla = "TIPO_MEDICAMENTO"
        datos.campoID = "ID_TIPO_MEDICAMENTO"
        datos.valorID = _ID_TIPO_MEDICAMENTO
        datos.agregarCampoValor("DESCRIPCION", _DESCRIPCION)
        datos.agregarCampoValor("ACTIVO", _ACTIVO)
        datos.modificar()
    End Sub
    Public Sub eliminar()
        If puedeEliminar() Then
            Dim datos = New datos()
            datos.tabla = "TIPO_MEDICAMENTO"
            datos.campoID = "ID_TIPO_MEDICAMENTO"
            datos.valorID = _ID_TIPO_MEDICAMENTO
            datos.eliminar()
        Else
            _ERROR = "El Tipo de Medicamento que intenta eliminar tiene vinculos con los registros de Medicamento."
        End If        
        
    End Sub
    
    Public Function mostrarTipoMedicamento(Optional ByVal buscar As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT ID_FORMA_FARMACEUTICA,DESCRIPCION FROM FORMA_FARMACEUTICA "
        If Trim(buscar) <> "" Then
            sSql += "WHERE DESCRIPCION LIKE '%" + buscar + "%' "        
        End If
        sSql += " ORDER BY DESCRIPCION"
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Sub cargarDatos(ByVal id_tipo_medicamento)
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM FORMA_FARMACEUTICA WHERE ID_FORMA_FARMACEUTICA = " & id_tipo_medicamento
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_TIPO_MEDICAMENTO = id_tipo_medicamento
            _DESCRIPCION = tblConsulta.Rows(0)("DESCRIPCION")            
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Function mostrarComboTipoMedicamento() As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""        
        sSql += " SELECT * FROM ( SELECT ID_TIPO_MEDICAMENTO, DESCRIPCION FROM TIPO_MEDICAMENTO WHERE ACTIVO = 1"
        sSql += " UNION SELECT -1, '[Todos]') AS TI ORDER BY DESCRIPCION "
        Return datos.ejecutarConsulta(sSql)
    End Function
    Private Function puedeEliminar() As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT COUNT(*) FROM MEDICAMENTO M "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE TM.ID_TIPO_MEDICAMENTO = " & _ID_TIPO_MEDICAMENTO        
        Return CInt(datos.ejecutarConsulta(sSql).Rows(0)(0)) = 0
    End Function

End Class
