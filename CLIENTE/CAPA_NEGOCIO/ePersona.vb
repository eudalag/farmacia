﻿Imports CAPA_DATOS
Imports System.Data
Public Class ePersona
    Private _ID_PERSONA As Integer
    Private _NOMBRE_APELLIDOS As String
    Private _DIRECCION As String
    Private _CELULAR As String
    Private _CORREO As String
    Private _NRO_CI As String
    Private _ERROR As String

    Public Property ID_PERSONA() As Integer
        Get
            Return _ID_PERSONA
        End Get
        Set(ByVal value As Integer)
            _ID_PERSONA = value
        End Set
    End Property
    Public Property NOMBRE_APELLIDOS() As String
        Get
            Return _NOMBRE_APELLIDOS
        End Get
        Set(ByVal value As String)
            _NOMBRE_APELLIDOS = value
        End Set
    End Property
    Public Property DIRECCION() As String
        Get
            Return _DIRECCION
        End Get
        Set(ByVal value As String)
            _DIRECCION = value
        End Set
    End Property
    Public Property CELULAR() As String
        Get
            Return _CELULAR
        End Get
        Set(ByVal value As String)
            _CELULAR = value
        End Set
    End Property
    Public Property CORREO() As String
        Get
            Return _CORREO
        End Get
        Set(ByVal value As String)
            _CORREO = value
        End Set
    End Property
    Public Property NRO_CI() As String
        Get
            Return _NRO_CI
        End Get
        Set(ByVal value As String)
            _NRO_CI = value
        End Set
    End Property
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "PERSONA"
        datos.campoID = "ID_PERSONA"
        datos.modoID = "auto"
        datos.agregarCampoValor("NOMBRE_APELLIDOS", _NOMBRE_APELLIDOS)        
        datos.agregarCampoValor("DIRECCION", IIf(_DIRECCION Is Nothing, "", _DIRECCION))
        datos.agregarCampoValor("CELULAR", IIf(_CELULAR Is Nothing, "", _CELULAR))
        datos.agregarCampoValor("CORREO", IIf(_CORREO Is Nothing, "", _CORREO))
        datos.agregarCampoValor("NRO_CI", IIf(_NRO_CI Is Nothing, "", _NRO_CI))
        datos.insertar()
        _ID_PERSONA = datos.valorID
    End Sub
    Public Sub modificar()
        Dim datos = New datos()
        datos.tabla = "PERSONA"
        datos.campoID = "ID_PERSONA"
        datos.valorID = _ID_PERSONA
        datos.agregarCampoValor("NOMBRE_APELLIDOS", _NOMBRE_APELLIDOS)
        datos.agregarCampoValor("DIRECCION", IIf(_DIRECCION Is Nothing, "", _DIRECCION))
        datos.agregarCampoValor("CELULAR", IIf(_CELULAR Is Nothing, "", _CELULAR))
        datos.agregarCampoValor("CORREO", IIf(_CORREO Is Nothing, "", _CORREO))
        datos.agregarCampoValor("NRO_CI", IIf(_NRO_CI Is Nothing, "", _NRO_CI))
        datos.modificar()
    End Sub
    Public Sub eliminar()
        Dim datos = New datos()
        datos.tabla = "PERSONA"
        datos.campoID = "ID_PERSONA"
        datos.valorID = _ID_PERSONA
        datos.eliminar()
    End Sub

    Public Sub cargarDatos()
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM PERSONA WHERE ID_PERSONA = " & _ID_PERSONA
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _NOMBRE_APELLIDOS = tblConsulta.Rows(0)("NOMBRE_APELLIDOS")            
            _DIRECCION = esNulo(tblConsulta.Rows(0)("DIRECCION"))
            _CELULAR = esNulo(tblConsulta.Rows(0)("CELULAR"))
            _NRO_CI = esNulo(tblConsulta.Rows(0)("NRO_CI"))
            _CORREO = esNulo(tblConsulta.Rows(0)("CORREO"))
        Else
            _ERROR = "No Existe Registro."
        End If        
    End Sub
    Private Function esNulo(ByVal campo As Object) As Object
        If IsDBNull(campo) Then
            Return ""
        Else
            Return campo
        End If
    End Function

End Class
