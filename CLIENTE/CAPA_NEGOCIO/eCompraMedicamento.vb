﻿Imports CAPA_DATOS
Imports System.Data
Public Class eCompraMedicamento
    Private _ID_COMPRA As Integer
    Private _FECHA_COMPRA As Date    
    Private _DETALLE As String
    Private _ERROR As String
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    Public Property ID_COMPRA() As Integer
        Get
            Return _ID_COMPRA
        End Get
        Set(ByVal value As Integer)
            _ID_COMPRA = value
        End Set
    End Property
    Public Property FECHA_COMPRA() As Date
        Get
            Return _FECHA_COMPRA
        End Get
        Set(ByVal value As Date)
            _FECHA_COMPRA = value
        End Set
    End Property
    Public Property DETALLE() As String
        Get
            Return _DETALLE
        End Get
        Set(ByVal value As String)
            _DETALLE = value
        End Set
    End Property
   
    Public Sub New()
        _ID_COMPRA = 0        
    End Sub
    Public Sub New(ByVal id_compra As Integer)
        cargarDatos(id_compra)        
    End Sub
    Public Sub cargarDatos(ByVal id_compra As Integer)
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM COMPRA_MEDICAMENTO WHERE ID_COMPRA = " & id_compra
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_COMPRA = id_compra
            _FECHA_COMPRA = tblConsulta.Rows(0)("FECHA_COMPRA")
            _DETALLE = tblConsulta.Rows(0)("DETALLE")
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "COMPRA_MEDICAMENTO"
        datos.campoID = "ID_COMPRA"
        datos.modoID = "auto"
        datos.agregarCampoValor("FECHA_COMPRA", _FECHA_COMPRA.ToString("yyyyMMdd"))
        datos.agregarCampoValor("DETALLE", _DETALLE)
        datos.insertar()
        _ID_COMPRA = datos.valorID
    End Sub
    Public Sub modificar()
        Dim datos = New datos()
        datos.tabla = "COMPRA_MEDICAMENTO"
        datos.campoID = "ID_COMPRA"
        datos.valorID = _ID_COMPRA
        datos.agregarCampoValor("FECHA_COMPRA", _FECHA_COMPRA.ToString("yyyyMMdd"))
        datos.agregarCampoValor("DETALLE", _DETALLE)        
        datos.modificar()
    End Sub
    Public Sub eliminar()
        Dim datos = New datos()
        datos.tabla = "COMPRA_MEDICAMENTO"
        datos.campoID = "ID_COMPRA"
        datos.valorID = _ID_COMPRA
        datos.eliminar()
    End Sub
    Public Function mostrarCompra(Optional ByVal buscar As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT C.ID_COMPRA,FECHA_COMPRA,DETALLE,SUM(CANTIDAD * PRECIO_UNITARIO) AS TOTAL_COMPRA FROM COMPRA_MEDICAMENTO C"
        sSql += " INNER JOIN INVENTARIO I ON I.ID_COMPRA = C.ID_COMPRA "
        sSql += " GROUP BY C.ID_COMPRA, FECHA_COMPRA, DETALLE "
        sSql += " ORDER BY FECHA_COMPRA, ID_COMPRA, DETALLE, TOTAL_COMPRA "
        Return datos.ejecutarConsulta(sSql)
    End Function


End Class
