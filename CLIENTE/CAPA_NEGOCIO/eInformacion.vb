﻿Imports CAPA_DATOS
Imports System.Data
Public Class eInformacion
    Private capa_datos As datos = New datos()
    Public Function obtenerFechaEmision() As String
        Dim sSql As String = ""
        sSql += " SELECT TOP 1 F_LIMITE_EMISION FROM DOSIFICACION  WHERE ACTIVO = 1 ORDER BY ID_DOSIFICACION DESC"
        Dim tbl As DataTable
        tbl = capa_datos.ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            Return tbl.Rows(0)(0)
        Else
            Return ""
        End If
    End Function

    Public Function obtenerCantProdStock() As DataTable
        Dim sSql As String = ""
        sSql += " SELECT M.ID_MEDICAMENTO,M.NOMBRE  ,MINIMO,SUM(CANTIDAD) AS CANTIDAD   FROM INVENTARIO I "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
        sSql += " WHERE Not MINIMO Is NULL"
        sSql += " GROUP BY M.ID_MEDICAMENTO,MINIMO,CANTIDAD ,M.NOMBRE   "
        sSql += " HAVING CANTIDAD <= MINIMO"
        Dim tbl As DataTable
        tbl = capa_datos.ejecutarConsulta(sSql)
        Return tbl                
    End Function

    Public Function obtenerCantProdVencidos() As Integer
        Return 0
    End Function

    Public Function obtenerCantProdVencerse() As Integer
       Return 0
    End Function

End Class
