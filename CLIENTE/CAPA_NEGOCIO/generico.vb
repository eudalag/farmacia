﻿Imports CAPA_DATOS
Imports System.Data
Public Class generico
    Public Function apertura(ByVal codUsuario As Integer, ByRef idCaja As String, ByRef idBanco As Integer) As Decimal
        Dim datos = New datos()
        Dim sSql = "SELECT ID_CAJA,IMPORTE_APERTURA_MN AS IMPORTE_APERTURA,ISNULL(ID_BANCO,-1) AS ID_BANCO FROM CAJA WHERE ID_USUARIO = " & codUsuario.ToString & " AND F_CIERRE IS NULL"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Dim importeApertura As Decimal = -1
        If tblResultado.Rows.Count > 0 Then
            importeApertura = tblResultado.Rows(0)("IMPORTE_APERTURA")
            idCaja = tblResultado.Rows(0)("ID_CAJA")
            idBanco = tblResultado.Rows(0)("ID_BANCO")
        End If
        Return importeApertura
    End Function
    Public Function movimientos(ByVal idCaja As String) As Decimal
        Dim datos = New datos()
        Dim sSql = ""
        sSql += " DECLARE @CAJA AS INTEGER;"
        sSql += " SET @CAJA = " + idCaja + ";"
        sSql += " SELECT ISNULL(SUM(M.IMPORTE * TM.FACTOR),0) AS MOVIMIENTO "
        sSql += " FROM MOVIMIENTO M"
        sSql += " INNER JOIN TIPO_MOVIMIENTO_CAJA TM ON TM.ID_TIPO_MOVIMIENTO = M.ID_TIPO_MOVIMIENTO "
        sSql += " WHERE M.ID_CAJA = @CAJA"

        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado.Rows(0)(0)
    End Function
    Public Function pedidos(ByVal idUsuario As String, ByVal idCaja As String) As Decimal
        Dim datos = New datos()
        Dim sSql = ""
        sSql += " SELECT ISNULL(SUM(V.TOTAL_PEDIDO - V.TOTAL_DESCUENTO),0) AS TOTAL_PEDIDO"
        sSql += " FROM VENTA V INNER JOIN CAJA C ON C.ID_CAJA = V.ID_CAJA "
        sSql += " WHERE C.ID_USUARIO =" & idUsuario & " And V.ID_CAJA = " & idCaja & " AND ID_TIPO_COMPROBANTE <> 3 AND V.ANULADA = 0"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        If tblResultado.Rows.Count > 0 Then
            Return tblResultado.Rows(0)(0)
        Else
            Return 0
        End If

    End Function
    Public Sub aperturarCaja(ByVal idUsuario As Integer, ByVal importe As String, ByRef idCaja As String, ByVal idBanco As Integer)
        Dim datos = New datos()
        datos.tabla = "CAJA"
        datos.campoID = "ID_CAJA"
        datos.agregarCampoValor("F_APERTURA", "serverDateTime")
        datos.agregarCampoValor("IMPORTE_APERTURA_MN", Decimal.Parse(importe, New Globalization.CultureInfo("en-US")))
        datos.agregarCampoValor("ID_USUARIO", idUsuario)
        datos.agregarCampoValor("ID_BANCO", idBanco)
        datos.insertar()
        idCaja = datos.valorID
    End Sub
    Public Sub modificarCaja(ByVal importe As String, ByVal idCaja As Integer)
        Dim datos = New datos()
        datos.tabla = "CAJA"
        datos.campoID = "ID_CAJA"
        datos.valorID = idCaja
        'datos.agregarCampoValor("F_APERTURA", "serverDateTime")
        datos.agregarCampoValor("IMPORTE_APERTURA_MN", importe)
        datos.modificar()
    End Sub
    Public Sub insertarMovimientoEconomico(ByVal id_caja As Integer, ByVal id_tipo As Integer, ByVal importe As Decimal, ByVal glosa As String)
        Dim datos = New datos()
        datos.tabla = "MOVIMIENTO"
        datos.modoID = "sql"
        datos.agregarCampoValor("ID_CAJA", id_caja)
        datos.agregarCampoValor("ID_TIPO_MOVIMIENTO", id_tipo)
        datos.agregarCampoValor("IMPORTE", importe)
        datos.agregarCampoValor("GLOSA", glosa)
        datos.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        datos.insertar()
    End Sub
    Public Sub cerrarCaja(ByVal idCaja As Integer, ByVal importe As Decimal)
        Dim datos = New datos()
        datos.tabla = "CAJA"
        datos.campoID = "ID_CAJA"
        datos.valorID = idCaja
        datos.agregarCampoValor("F_CIERRE", "serverDateTime")
        datos.agregarCampoValor("IMPORTE_CIERRE_MN", importe)
        datos.modificar()
    End Sub
    Function obtenerInsumo(ByVal idItem As String) As String
        Dim datos = New datos()
        Dim sSql = " SELECT DETALLE + ' (' + CAST(PRECIO_UNITARIO AS NVARCHAR) + ' Bs.-)' AS ITEM FROM ITEM WHERE ID_ITEM = " & idItem
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado.Rows(0)(0)
    End Function
    Function obtenerApertura(ByVal idCaja As String) As datatable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_CAJA AS INTEGER;"
        sSql += " SET @ID_CAJA = " + idCaja + ";"
        sSql += " SELECT U.NOMBRE + ' ' + U.APELLIDOS AS NOMBRE_APELLIDOS, CONVERT(NVARCHAR(10),F_APERTURA,103) AS F_APERTURA,IMPORTE_APERTURA FROM CAJA C "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = C.ID_USUARIO "
        sSql += " WHERE ID_CAJA = @ID_CAJA;"

        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function

    Function obtenerCierreCaja(ByVal idCaja As String) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""

        'sSql += " DECLARE @ID_CAJA AS INTEGER,@MOV_ING AS DECIMAL(18,2),@MOV_EG AS DECIMAL(18,2),"
        'sSql += " @MOV_EX AS DECIMAL(18,2),@MOV_TRAN AS DECIMAL(18,2),@PED AS DECIMAL(18,2);"
        'sSql += " SET @ID_CAJA = " + idCaja + ";"

        'sSql += " SELECT @MOV_ING = ISNULL(SUM(IMPORTE),0) FROM MOVIMIENTO MI "
        'sSql += " WHERE ID_TIPO_MOVIMIENTO  = 1 AND MI.ID_CAJA = @ID_CAJA;"
        'sSql += " SELECT @MOV_EG = ISNULL(SUM(IMPORTE),0) * - 1 FROM MOVIMIENTO ME "
        'sSql += " WHERE ID_TIPO_MOVIMIENTO = 2 AND ME.ID_CAJA = @ID_CAJA; "
        'sSql += " SELECT @MOV_EX = ISNULL(SUM(IMPORTE),0) FROM MOVIMIENTO MEX"
        'sSql += " WHERE ID_TIPO_MOVIMIENTO = 3 AND MEX.ID_CAJA = @ID_CAJA;"
        'sSql += " SELECT @MOV_TRAN = ISNULL(SUM(IMPORTE),0) FROM MOVIMIENTO MEX"
        'sSql += " WHERE ID_TIPO_MOVIMIENTO = 5 AND MEX.ID_CAJA = @ID_CAJA;"

        'sSql += " SELECT @PED = ISNULL(SUM(P.TOTAL_PEDIDO),0) FROM VENTA P "
        'sSql += " WHERE P.ID_CAJA = @ID_CAJA;"

        'sSql += " SELECT U.NOMBRE_APELLIDOS,UR.NOMBRE_APELLIDOS AS USUARIO_RECEPCION , "
        'sSql += " CONVERT(NVARCHAR(10),F_APERTURA,103) AS F_APERTURA, CONVERT(NVARCHAR(10),F_CIERRE,103) AS F_CIERRE,"
        'sSql += " CASE WHEN F_CIERRE IS NULL THEN 'Abierta' ELSE 'Cerrada' END AS ESTADO,"
        'sSql += " IMPORTE_APERTURA_MN AS IMPORTE_APERTURA, @MOV_ING AS MOV_INGRESO, @MOV_EG AS MOV_EGRESO, "
        'sSql += " @MOV_EX AS MOV_EXCEDENTE, @PED AS TOTAL_PEDIDOS,C.IMPORTE_CIERRE_MN AS IMPORTE_CIERRE,"
        'sSql += " @MOV_TRAN AS MOV_TRANSFERENCIA FROM CAJA C"
        'sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = C.ID_USUARIO "
        'sSql += " INNER JOIN CIERRE CR ON CR.ID_CAJA = C.ID_CAJA "
        'sSql += " INNER JOIN USUARIO UR ON UR.ID_USUARIO = CR.ID_USUARIO_REC "
        'sSql += " WHERE C.ID_CAJA = @ID_CAJA "

        sSql += " DECLARE @CAJA AS INTEGER"
        sSql += " SET @CAJA = " + idCaja.ToString + ";"

        sSql += " SELECT CJ.ID_CAJA,U.NOMBRE_APELLIDOS,CONVERT(NVARCHAR(10),CJ.F_APERTURA,103) AS F_APERTURA,"
        sSql += " ISNULL(CONVERT(NVARCHAR(10),CJ.F_CIERRE,103),'')AS F_CIERRE,"
        sSql += " CJ.IMPORTE_APERTURA_MN,ISNULL(CJ.IMPORTE_CIERRE_MN,0) AS IMPORTE_CIERRE_MN, ISNULL(VE.TOTAL_EFECTIVO,0) AS TOTAL_EFECTIVO,"
        sSql += " ISNULL(VC.TOTAL_CREDITO,0) AS TOTAL_CREDITO,ISNULL(MI.TOTAL_INGRESO ,0) AS TOTAL_INGRESO,"
        sSql += " ISNULL(MT.TOTAL_TRANSFERENCIA  ,0) AS TOTAL_TRANSFERENCIA,"
        sSql += " ISNULL(ME.TOTAL_EGRESO   ,0) AS TOTAL_EGRESO,"
        sSql += " ISNULL(MA.TOTAL_AJUSTE    ,0) AS TOTAL_AJUSTE,"
        sSql += " ISNULL(CC.TOTAL_CTA_X_COBRAR    ,0) AS TOTAL_CTA_X_COBRAR,ISNULL(UR.NOMBRE_APELLIDOS,'') AS USUARIO_RECEPCION "
        sSql += " FROM CAJA CJ "
        sSql += " INNER JOIN BANCO B ON B.ID_BANCO = CJ.ID_BANCO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = CJ.ID_USUARIO "
        sSql += " LEFT  JOIN CIERRE CR ON CR.ID_CAJA = CJ.ID_CAJA"
        sSql += " LEFT JOIN USUARIO UR ON UR.ID_USUARIO = CR.ID_USUARIO_REC"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA,SUM(V.TOTAL_PEDIDO) AS TOTAL_EFECTIVO FROM VENTA V "
        sSql += " WHERE V.CREDITO = 0 AND ANULADA = 0"
        sSql += " GROUP BY ID_CAJA"
        sSql += " ) VE ON VE.ID_CAJA = CJ.ID_CAJA AND CJ.ID_CAJA = @CAJA"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA,SUM(V.TOTAL_PEDIDO) AS TOTAL_CREDITO FROM VENTA V "
        sSql += " WHERE V.CREDITO = 1 AND ANULADA = 0 AND ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " ) VC ON VC.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(IMPORTE) AS TOTAL_INGRESO FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO = 1 AND ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )MI ON MI.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(IMPORTE) AS TOTAL_EGRESO FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO = 2 AND ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )ME ON ME.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(IMPORTE) AS TOTAL_AJUSTE FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO = 3 AND ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )MA ON MA.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(IMPORTE) AS TOTAL_TRANSFERENCIA FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO = 5 AND ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )MT ON MT.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(TOTAL_PAGO) AS TOTAL_CTA_X_COBRAR FROM PAGOS"
        sSql += " WHERE  ID_CAJA = @CAJA"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )CC ON CC.ID_CAJA = CJ.ID_CAJA "
        sSql += " WHERE CJ.ID_CAJA = @CAJA"
        sSql += " ORDER BY F_APERTURA DESC"

        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function
    Function obtenerCajasInforme(ByVal fecha As String) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT ID_CAJA,CASE WHEN F_CIERRE IS  NULL THEN 'Abierta (' ELSE 'Cerrada (' END + CAST(ROW_NUMBER() OVER (ORDER BY ID_CAJA) AS NVARCHAR(2)) + ')' AS ESTADO FROM CAJA"
        sSql += " WHERE F_APERTURA = '" & fecha & "' "
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function

    Function obtenerExistenciaLaboratorios(ByVal idLaboratorio As Integer, ByVal cantCero As Boolean, ByVal cantNeg As Boolean) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @LABORATORIO AS INTEGER;"
        sSql += " SET @LABORATORIO = " + idLaboratorio.ToString + ";"

        'sSql += " SELECT M.ID_MEDICAMENTO,M.MEDICAMENTO,I.CANTIDAD,K.SALDOS,K.NRO_LOTE,CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103) AS F_VENCIMIENTO,K.SIN_LOTE,SUM(MK.CANTIDAD) AS CANTIDAD_LOTE  FROM VW_MEDICAMENTO M "
        'sSql += " INNER JOIN INVENTARIO I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        'sSql += " INNER JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        'sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        'sSql += " WHERE M.ID_LABORATORIO = @LABORATORIO "
        'sSql += " GROUP BY M.ID_MEDICAMENTO,M.MEDICAMENTO,I.CANTIDAD,K.SALDOS,K.NRO_LOTE,K.F_VENCIMIENTO,K.SIN_LOTE"

        sSql += " SELECT M.ID_MEDICAMENTO,M.MEDICAMENTO,ISNULL(I.CANTIDAD,0) AS CANTIDAD,ISNULL(K.SALDOS,0) AS SALDOS,ISNULL(K.NRO_LOTE,'') AS NRO_LOTE,ISNULL(CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO,ISNULL(K.SIN_LOTE,0) AS SIN_LOTE,ISNULL(SUM(MK.CANTIDAD),0) AS CANTIDAD_LOTE  FROM VW_MEDICAMENTO M "
        sSql += " LEFT JOIN INVENTARIO I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE M.ID_LABORATORIO = @LABORATORIO "
        sSql += " GROUP BY M.ID_MEDICAMENTO,M.MEDICAMENTO,I.CANTIDAD,K.SALDOS,K.NRO_LOTE,K.F_VENCIMIENTO,K.SIN_LOTE"

        If cantCero Then
            If Not cantNeg Then
                sSql += " HAVING "
                sSql += " ISNULL(SUM(MK.CANTIDAD),0) >= 0"
            End If
        Else
            sSql += " HAVING "
            sSql += " ISNULL(SUM(MK.CANTIDAD),0) > 0"
        End If
        sSql += " ORDER BY M.MEDICAMENTO, K.F_VENCIMIENTO"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function
    Function obtenerRecetaIndicaciones(ByVal idReceta As Integer) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @RECETA AS INTEGER;"
        sSql += " SET @RECETA = " + idReceta.ToString + ";"
        sSql += " SELECT DISTINCT HC.ID_HISTORICO,P.NOMBRE_APELLIDOS,R.ID_MEDICO ,
        sSql += " CONVERT(NVARCHAR(10),HC.FH_HISTORICO,103) AS F_HISTORICO,M.NOMBRE,RD.INDICACIONES    FROM RECETA R "
        sSql += " INNER JOIN RECETA_DETALLE RD ON RD.ID_RECETA = R.ID_RECETA "
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = R.ID_PACIENTE "
        sSql += " INNER JOIN HISTORICO_CLINICO HC ON HC.ID_PACIENTE =P.ID_PACIENTE AND R.ID_HISTORICO = HC.ID_HISTORICO "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = RD.ID_MEDICAMENTO "
        sSql += " INNER JOIN VENTA V ON V.ID_RECETA = R.ID_RECETA "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA AND VM.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE R.ID_RECETA = @RECETA;"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function
    Function obtenerDatosMedico(ByVal idUsuario As Integer) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @USUARIO AS INTEGER;
        sSql += " SET @USUARIO = " + idUsuario.ToString + ";"
        sSql += " SELECT NOMBRE, MATRICULA, FIRMA FROM MEDICO WHERE ID_USUARIO = @USUARIO"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function
    Function obtenerVentasCredito(ByVal idVenta As Integer)
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @VENTA AS INTEGER;"
        sSql += " SET @VENTA = " + idVenta.ToString + ";"
        sSql += " SELECT  RIGHT('000000' + CAST(C.ID_CREDITO AS NVARCHAR(8)),8) AS NRO_CREDITO, CONVERT(NVARCHAR(103),V.FECHA) AS FECHA,"
        sSql += " P.NOMBRE_APELLIDOS,P.DIRECCION,GC.GRUPO_PERSONAS,VM.CANTIDAD,VM.PRECIO_UNITARIO,M.MEDICAMENTO AS DESC_MEDICAMENTO ,M.FORMA_FARMACEUTICA  AS DESC_TIPO_MEDICAMENTO,"
        sSql += " V.TOTAL_PEDIDO, U.NOMBRE_APELLIDOS AS USUARIO,V.TOTAL_PEDIDO "
        sSql += " FROM CREDITO C"
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = C.ID_VENTA "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA  = V.ID_VENTA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO "
        sSql += " INNER JOIN BANCO BC ON BC.ID_BANCO = V.ID_BANCO"
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = BC.ID_USUARIO "
        sSql += " WHERE V.ID_VENTA = @VENTA"
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        Return tblResultado
    End Function

    Function obtenerBancoFarmacia(ByVal idUsuario As Integer) As Integer
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @USUARIO AS INTEGER;"
        sSql += " SET @USUARIO = " + idUsuario.ToString + ""
        sSql += " SELECT ID_BANCO FROM BANCO WHERE ID_USUARIO = @USUARIO "
        Dim tblResultado = datos.ejecutarConsulta(sSql)
        If tblResultado.Rows.Count = 0 Then
            Return -1
        Else
            Return tblResultado.Rows(0)(0)
        End If

    End Function

    Function obtenerDetallePago(ByVal idPago As Integer) As DataSet
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @PAGO AS INTEGER"
        sSql += " SET @PAGO = " + idPago.ToString + ";"

        sSql += " SELECT ISNULL(CONVERT(NVARCHAR(10),V.FECHA_AUD,103),'10/03/2019') AS FECHA, ISNULL(V.NRO_VENTA,C.ID_CREDITO) AS NRO_COMPROBANTE, PC.IMPORTE,V.ID_VENTA   FROM PAGO_CREDITO PC"
        sSql += " INNER JOIN CREDITO C ON C.ID_CREDITO = PC.ID_CREDITO "
        sSql += " LEFT JOIN VENTA V ON V.ID_VENTA = C.ID_VENTA  WHERE ID_PAGO = @PAGO"

        sSql += " SELECT DISTINCT RIGHT('00000000' + CAST(P.ID_PAGO AS NVARCHAR(10)),8) AS NRO_PAGO,PP.NOMBRE_APELLIDOS,"
        sSql += " GC.GRUPO_PERSONAS, CONVERT(NVARCHAR(10),P.FECHA,103) AS FECHA, TP.DESC_TIPO_PAGO,"
        sSql += " ISNULL(P.TOTAL_PAGO,0) AS TOTAL_PAGO,P.IMPORTE_PAGO, P.IMPORTE_PAGO - ISNULL(P.TOTAL_PAGO,0) AS CAMBIO,"
        sSql += " U.USUARIO   FROM PAGOS P"
        sSql += " INNER JOIN TIPO_PAGO TP ON TP.ID_TIPO_PAGO = P.ID_TIPO_PAGO  "
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " INNER JOIN CREDITO C ON C.ID_CREDITO = PC.ID_CREDITO "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE"
        sSql += " INNER JOIN PERSONA PP ON PP.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO"
        sSql += " INNER JOIN BANCO B ON B.ID_BANCO = P.ID_BANCO  "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO "
        sSql += " WHERE P.ID_PAGO = @PAGO"
        Return datos.obtenerDataSet(sSql)        
    End Function

    Public Function obtenerMedicamentosVencidos(ByVal desde As String, ByVal hasta As String)
        Dim sSql As String = ""
        sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SELECT M.ID_MEDICAMENTO,M.MEDICAMENTO,M.LABORATORIO,M.FORMA_FARMACEUTICA ,K.ID_KARDEX, NRO_LOTE, CONVERT(NVARCHAR(10),F_VENCIMIENTO,103) AS F_VENCIMIENTO, TOTAL_STOCK FROM KARDEX K "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = K.ID_MEDICAMENTO "
        sSql += " INNER JOIN ("
        sSql += " SELECT ID_KARDEX, SUM(CANTIDAD) AS TOTAL_STOCK FROM MOVIMIENTO_KARDEX MK "
        sSql += " GROUP BY ID_KARDEX "
        sSql += " HAVING SUM(CANTIDAD) > 0 ) MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE CONVERT(DATE,K.F_VENCIMIENTO,103) >= @DESDE AND CONVERT(DATE,K.F_VENCIMIENTO,103) <= @HASTA"
        sSql += " ORDER BY F_VENCIMIENTO, MEDICAMENTO "

        Return New datos().ejecutarConsulta(sSql)
    End Function

    Function obtenerDetalleConsultaCampanha(ByVal idConsulta As Integer) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @ID_CONSULTA AS INTEGER;"
        sSql += " SET @ID_CONSULTA = " + idConsulta.ToString + ";"
        sSql += " SELECT C.ID_CONSULTA,CONVERT(NVARCHAR(10),C.F_PAGO,103) AS F_PAGO,PR.NOMBRE_APELLIDOS,CL.NIT,CM.NOMBRE_CAMPANHA, M.NOMBRE AS NOMBRE_MEDICO,ISNULL(P.NOMBRE_APELLIDOS,'') AS PACIENTE, C.TOTAL_CONSULTA, U.USUARIO  FROM CONSULTA C"
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE"
        sSql += " INNER JOIN PERSONA PR ON PR.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN CAMPANHA_MEDICA CM ON CM.ID_CAMPANHA = C.ID_CAMPANHA"
        sSql += " INNER JOIN MEDICO M  ON M.ID_MEDICO = CM.ID_MEDICO"
        sSql += " INNER JOIN BANCO B ON B.ID_BANCO = C.ID_BANCO"
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO "
        sSql += " LEFT JOIN PACIENTE P ON P.ID_PACIENTE = C.ID_PACIENTE"
        sSql += " WHERE C.ID_CONSULTA = @ID_CONSULTA "

        Return New datos().ejecutarConsulta(sSql)

    End Function
End Class
