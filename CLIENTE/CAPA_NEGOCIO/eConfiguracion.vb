﻿Imports CAPA_DATOS
Imports System.Data
Public Class eConfiguracion
    Private _IMPRIMIR_RECIBO As Boolean    
    Private _NOMBRE As String
    Private _DIRECCION As String
    Private _TELEFONO As String
    Private _CONTROL_STOCK As Boolean
    Private _TC_ME As Decimal
    Private _NIT As String
    Private _CODIGO_BARRA As String
    Private _ERROR As String
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    Public Property IMPRIMIR_RECIBO() As Boolean
        Get
            Return _IMPRIMIR_RECIBO
        End Get
        Set(ByVal value As Boolean)
            _IMPRIMIR_RECIBO = value
        End Set
    End Property
    Public Property CONTROL_STOCK() As Boolean
        Get
            Return _CONTROL_STOCK
        End Get
        Set(ByVal value As Boolean)
            _CONTROL_STOCK = value
        End Set
    End Property
    
    Public Property NOMBRE() As String
        Get
            Return _NOMBRE
        End Get
        Set(ByVal value As String)
            _NOMBRE = value
        End Set
    End Property
    Public Property DIRECCION() As String
        Get
            Return _DIRECCION
        End Get
        Set(ByVal value As String)
            _DIRECCION = value
        End Set
    End Property
    Public Property TELEFONO() As String
        Get
            Return _TELEFONO
        End Get
        Set(ByVal value As String)
            _TELEFONO = value
        End Set
    End Property
    Public Property TC_ME()
        Get
            Return _TC_ME
        End Get
        Set(ByVal value)
            _TC_ME = value
        End Set
    End Property
    Public Property NIT()
        Get
            Return _NIT
        End Get
        Set(ByVal value)
            _NIT = value
        End Set
    End Property

    Public Property CODIGO_BARRA()
        Get
            Return _CODIGO_BARRA
        End Get
        Set(ByVal value)
            _CODIGO_BARRA = value
        End Set
    End Property
    Public Sub New()
        cargarDatos()
    End Sub
    Public Sub cargarDatos()
        Dim datos = New datos()
        Dim sSql As String
        sSql = "SELECT * FROM CONFIGURACION "
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _IMPRIMIR_RECIBO = tblConsulta.Rows(0)("IMPRIMIR_RECIBO")            
            _NOMBRE = tblConsulta.Rows(0)("NOMBRE")
            _DIRECCION = tblConsulta.Rows(0)("DIRECCION")
            _TELEFONO = tblConsulta.Rows(0)("TELEFONO")
            _CONTROL_STOCK = tblConsulta.Rows(0)("CONTROL_STOCK")
            _TC_ME = tblConsulta.Rows(0)("TC_ME")
            _NIT = tblConsulta.Rows(0)("NIT")
            If Not IsDBNull(tblConsulta.Rows(0)("CODIGO_BARRA")) Then
                _CODIGO_BARRA = tblConsulta.Rows(0)("CODIGO_BARRA")
            Else
                _CODIGO_BARRA = ""
            End If
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Sub modificar()
        Dim datos = New datos()
        datos.tabla = "CONFIGURACION"
        datos.campoID = "ID_CONFIGURACION"
        datos.valorID = 1
        datos.agregarCampoValor("IMPRIMIR_RECIBO", _IMPRIMIR_RECIBO)        
        datos.agregarCampoValor("NOMBRE", _NOMBRE)
        datos.agregarCampoValor("DIRECCION", _DIRECCION)
        datos.agregarCampoValor("TELEFONO", _TELEFONO)
        datos.agregarCampoValor("CONTROL_STOCK", _CONTROL_STOCK)
        datos.agregarCampoValor("TC_ME", _TC_ME)
        datos.agregarCampoValor("NIT", _NIT)
        datos.agregarCampoValor("CODIGO_BARRA", _CODIGO_BARRA)
        datos.modificar()
    End Sub
End Class
