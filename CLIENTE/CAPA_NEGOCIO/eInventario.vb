﻿
Imports CAPA_DATOS
Imports System.Data

Public Class eInventario
    Private _ID_INVENTARIO As Integer
    Private _ID_MEDICAMENTO As Integer
    Private _ID_COMPRA
    Private _ID_VENTA
    Private _CANTIDAD As Integer
    Private _F_VENCIMIENTO
    Private _CONTROL_FECHA As Boolean
    Private _MEDICAMENTO As eMedicamento
    Private _PRECIO As Decimal
    Private _LOTE As String
    Private _ID_USUARIO As Integer
    Private _ID_PERSONA As Integer

    Public Property ID_INVENTARIO() As Integer
        Get
            Return _ID_INVENTARIO
        End Get
        Set(ByVal value As Integer)
            _ID_INVENTARIO = value
        End Set
    End Property
    Public Property ID_MEDICAMENTO() As Integer
        Get
            Return _ID_MEDICAMENTO
        End Get
        Set(ByVal value As Integer)
            _ID_MEDICAMENTO = value
        End Set
    End Property

    Public Property ID_COMPRA()
        Get
            Return _ID_COMPRA
        End Get
        Set(ByVal value)
            _ID_COMPRA = value
        End Set
    End Property
    Public Property ID_VENTA()
        Get
            Return _ID_VENTA
        End Get
        Set(ByVal value)
            _ID_VENTA = value
        End Set
    End Property
    Public Property CANTIDAD() As Integer
        Get
            Return _CANTIDAD
        End Get
        Set(ByVal value As Integer)
            _CANTIDAD = value
        End Set
    End Property

    Public Property PRECIO() As Decimal
        Get
            Return _PRECIO
        End Get
        Set(ByVal value As Decimal)
            _PRECIO = value
        End Set
    End Property

    Public Property F_VENCIMIENTO()
        Get
            Return _F_VENCIMIENTO
        End Get
        Set(ByVal value)
            _F_VENCIMIENTO = value
        End Set
    End Property
    Public Property CONTROL_FECHA As Boolean
        Get
            Return _CONTROL_FECHA
        End Get
        Set(value As Boolean)
            _CONTROL_FECHA = value
        End Set
    End Property

    Public Property MEDICAMENTO() As eMedicamento
        Get
            Return _MEDICAMENTO
        End Get
        Set(ByVal value As eMedicamento)
            _MEDICAMENTO = value
        End Set
    End Property
    Public Property LOTE As String
        Get
            Return _LOTE
        End Get
        Set(value As String)
            _LOTE = value
        End Set
    End Property
    Public Property ID_USUARIO As Integer
        Get
            Return _ID_USUARIO
        End Get
        Set(value As Integer)
            _ID_USUARIO = value
        End Set
    End Property
    Public Property ID_PERSONA As Integer
        Get
            Return _ID_PERSONA
        End Get
        Set(value As Integer)
            _ID_PERSONA = value
        End Set
    End Property
    Public Sub New()
        ID_INVENTARIO = 0
        _MEDICAMENTO = New eMedicamento()
    End Sub
    Public Sub New(ByVal id_inventario)
        cargarDatos(id_inventario)
        _MEDICAMENTO = New eMedicamento(_ID_MEDICAMENTO)
    End Sub
    Private Sub cargarDatos(ByVal id_inventario As Integer)
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql = "SELECT * FROM INVENTARIO WHERE ID_INVENTARIO = " & ID_MEDICAMENTO.ToString
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_INVENTARIO = tblConsulta.Rows(0)("ID_INVENTARIO")
            _ID_MEDICAMENTO = tblConsulta.Rows(0)("ID_MEDICAMENTO")
            _ID_COMPRA = tblConsulta.Rows(0)("ID_COMPRA")
            _ID_VENTA = tblConsulta.Rows(0)("ID_VENTA")
            _CANTIDAD = tblConsulta.Rows(0)("CANTIDAD")
            _F_VENCIMIENTO = tblConsulta.Rows(0)("F_VENCIMIENTO")
            _CONTROL_FECHA = tblConsulta.Rows(0)("_CONTROL_FECHA")
            _LOTE = tblConsulta.Rows(0)("LOTE")
            _ID_USUARIO = tblConsulta.Rows(0)("ID_USUARIO")
            _ID_PERSONA = tblConsulta.Rows(0)("ID_PERSONA")
        End If
    End Sub
    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "INVENTARIO"
        datos.campoID = "ID_INVENTARIO"
        datos.modoID = "sql"
        datos.agregarCampoValor("ID_MEDICAMENTO", _ID_MEDICAMENTO)
        
        datos.agregarCampoValor("CANTIDAD", _CANTIDAD)
        
        'datos.agregarCampoValor("FECHA", "serverDateTime")
        datos.insertar()
        _ID_COMPRA = datos.valorID
    End Sub
    Public Sub modificar()
        Dim datos = New datos()
        datos.tabla = "INVENTARIO"
        datos.campoID = "ID_INVENTARIO"
        datos.valorID = _ID_INVENTARIO
        datos.agregarCampoValor("ID_MEDICAMENTO", _ID_MEDICAMENTO)
        datos.agregarCampoValor("ID_COMPRA", _ID_COMPRA)
        datos.agregarCampoValor("ID_VENTA", _ID_VENTA)
        datos.agregarCampoValor("CANTIDAD", _CANTIDAD)
        datos.agregarCampoValor("CONTROL_FECHA", _CONTROL_FECHA)
        datos.agregarCampoValor("PRECIO_UNITARIO", _PRECIO)
        If _CONTROL_FECHA Then
            datos.agregarCampoValor("F_VENCIMIENTO", _F_VENCIMIENTO.ToString("yyyyMMdd"))
        End If
        datos.agregarCampoValor("LOTE", _LOTE)
        datos.modificar()
    End Sub
    Public Sub eliminar()
        Dim datos = New datos()
        datos.tabla = "INVENTARIO"
        datos.campoID = "ID_INVENTARIO"
        datos.valorID = _ID_INVENTARIO
        datos.eliminar()
    End Sub
    Public Function mostrarInventario(Optional ByVal buscar As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT ID_INVENTARIO,M.DESCRIPCION,M.DESC_TIPO_MEDICAMENTO,I.CANTIDAD * CASE WHEN  I.ID_COMPRA IS NULL AND NOT I.ID_VENTA IS NULL THEN -1 ELSE 1 END AS CANTIDAD, "
        sSql += " CASE WHEN I.ID_COMPRA IS NULL AND I.ID_VENTA IS NULL THEN 'Inicial' WHEN NOT I.ID_COMPRA IS NULL AND I.ID_VENTA IS NULL THEN 'Compra' WHEN I.ID_COMPRA IS NULL AND NOT I.ID_VENTA IS NULL THEN 'Venta' END AS TIPO   "
        sSql += " FROM INVENTARIO I INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
        If Trim(buscar) <> "" Then
            sSql += "WHERE M.DESCRIPCION LIKE '%" & buscar & "%'"
        End If
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarInventarioCompra(ByVal id_Compra) As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT ROW_NUMBER() OVER (ORDER BY M.ID_MEDICAMENTO) AS NRO_ITEM,I.ID_MEDICAMENTO,M.NOMBRE,M.DESC_TIPO_MEDICAMENTO,"
        sSql += " M.PRECIO_UNITARIO AS PRECIO_COMPRA ,CANTIDAD, 0 AS TOTAL,F_VENCIMIENTO,CONTROL_FECHA,I.ID_INVENTARIO FROM INVENTARIO I "
        sSql += " INNER JOIN COMPRA_MEDICAMENTO C ON C.ID_COMPRA = I.ID_COMPRA "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
        sSql += " WHERE C.ID_COMPRA = " & id_Compra
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarInventarioInicial(ByVal gestion As Boolean) As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT ROW_NUMBER() OVER (ORDER BY M.ID_MEDICAMENTO) AS NRO_ITEM,M.ID_MEDICAMENTO,M.NOMBRE, L.DESCRIPCION AS LABORATORIO,I.LOTE,"
        sSql += " TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO,I.F_VENCIMIENTO,I.CANTIDAD,M.PRECIO_UNITARIO AS PRECIO,CONTROL_FECHA,I.ID_INVENTARIO,I.PRECIO_UNITARIO AS PRECIO_UNITARIO  FROM INVENTARIO I "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO  "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO"
        sSql += " WHERE I.ID_COMPRA Is NULL And I.ID_VENTA Is NULL AND M.ID_MEDICAMENTO = -1"
        sSql += " ORDER BY LABORATORIO, NOMBRE,F_VENCIMIENTO DESC"
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarInventarioTotal(Optional ByVal tipo As Integer = -1, Optional medicamento As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " DECLARE @TIPO AS INTEGER, @BUSCAR AS NVARCHAR(100)"
        sSql += " SET @TIPO = " + tipo.ToString + ";"
        sSql += " SET @BUSCAR = '" + medicamento + "';"
        sSql += " SELECT NOMBRE,COMPOSICION  ,DESC_TIPO_MEDICAMENTO,SUM(CANTIDAD) AS TOTAL,MINIMO,DESCRIPCION_LABORATORIO,ISNULL(CONVERT(NVARCHAR(10),F_VENCIMIENTO,103),'') AS F_VENCIMIENTO "
        sSql += " FROM INVENTARIO I INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO"
        sSql += " WHERE M.ID_TIPO_MEDICAMENTO = CASE WHEN @TIPO = -1 THEN M.ID_TIPO_MEDICAMENTO ELSE @TIPO END"
        sSql += " AND "
        sSql += " ((M.NOMBRE LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (M.DESC_TIPO_MEDICAMENTO  LIKE '%' + @BUSCAR + '%') OR (M.DESCRIPCION_LABORATORIO  LIKE '%' + @BUSCAR + '%'))"
        sSql += " GROUP BY NOMBRE, COMPOSICION,DESC_TIPO_MEDICAMENTO,DESCRIPCION_LABORATORIO,F_VENCIMIENTO,MINIMO"
        Return datos.ejecutarConsulta(sSql)
    End Function

End Class
