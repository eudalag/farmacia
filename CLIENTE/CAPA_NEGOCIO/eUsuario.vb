﻿Imports CAPA_DATOS
Imports System.Data
Public Class eUsuario
    Private _ID_USUARIO As Integer    
    Private _USUARIO As String
    Private _CONTRASENHA As String
    Private _HABILITADO As Boolean
    Private _ADMINISTRADOR As Boolean
    Private _CAMBIO_CONTRASENHA As Boolean
    Private _COMISION As Integer
    Private _DESCUENTO As Boolean
    Private _CELULAR As String
    Private _TELEFONO As String
    Private _DIRECCION As String
    Private _NOMBRE_APELLIDOS As String
    Private _ID_TIPO_USUARIO As Integer
    Private _ERROR As String

    Public Property ID_USUARIO() As Integer
        Get
            Return _ID_USUARIO
        End Get
        Set(ByVal value As Integer)
            _ID_USUARIO = value
        End Set
    End Property

    
    Public Property USUARIO() As String
        Get
            Return _USUARIO
        End Get
        Set(ByVal value As String)
            _USUARIO = value
        End Set
    End Property
    Public Property CONTRASENHA() As String
        Get
            Return _CONTRASENHA
        End Get
        Set(ByVal value As String)
            _CONTRASENHA = value
        End Set
    End Property

    Public Property HABILITADO() As Boolean
        Get
            Return _HABILITADO
        End Get
        Set(ByVal value As Boolean)
            _HABILITADO = value
        End Set
    End Property
    Public Property ADMINISTRADOR() As Boolean
        Get
            Return _ADMINISTRADOR
        End Get
        Set(ByVal value As Boolean)
            _ADMINISTRADOR = value
        End Set
    End Property
    Public Property CAMBIO_CONTRASENHA() As Boolean
        Get
            Return _CAMBIO_CONTRASENHA
        End Get
        Set(ByVal value As Boolean)
            _CAMBIO_CONTRASENHA = value
        End Set
    End Property
    Public Property DESCUENTO() As Boolean
        Get
            Return _DESCUENTO
        End Get
        Set(ByVal value As Boolean)
            _DESCUENTO = value
        End Set
    End Property
    Public Property COMISION() As Integer
        Get
            Return _COMISION
        End Get
        Set(ByVal value As Integer)
            _COMISION = value
        End Set
    End Property
    Public Property CELULAR() As String
        Get
            Return _CELULAR
        End Get
        Set(ByVal value As String)
            _CELULAR = value
        End Set
    End Property
    Public Property TELEFONO() As String
        Get
            Return _TELEFONO
        End Get
        Set(ByVal value As String)
            _TELEFONO = value
        End Set
    End Property
    Public Property DIRECCION() As String
        Get
            Return _DIRECCION
        End Get
        Set(ByVal value As String)
            _DIRECCION = value
        End Set
    End Property
    Public Property NOMBRE_APELLIDOS() As String
        Get
            Return _NOMBRE_APELLIDOS
        End Get
        Set(ByVal value As String)
            _NOMBRE_APELLIDOS = value
        End Set
    End Property
    Public Property ID_TIPO_USUARIO() As Integer
        Get
            Return _ID_TIPO_USUARIO
        End Get
        Set(ByVal value As Integer)
            _ID_TIPO_USUARIO = value
        End Set
    End Property
    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    
    Public Sub insertar()
        If Not existeUsuario(_USUARIO) Then

            Dim datos = New datos()
            datos.tabla = "USUARIO"
            datos.campoID = "ID_USUARIO"
            datos.modoID = "auto"            
            datos.agregarCampoValor("USUARIO", _USUARIO)
            datos.agregarCampoValor("CONTRASENHA", _CONTRASENHA)
            datos.agregarCampoValor("HABILITADO", _HABILITADO)
            datos.agregarCampoValor("ADMINISTRADOR", _ADMINISTRADOR)
            datos.agregarCampoValor("CAMBIO_CONTRASENHA", _CAMBIO_CONTRASENHA)
            datos.agregarCampoValor("COMISION", _COMISION)
            datos.agregarCampoValor("DESCUENTO", _DESCUENTO)
            datos.agregarCampoValor("TELEFONO", _TELEFONO)
            datos.agregarCampoValor("CELULAR", _CELULAR)
            datos.agregarCampoValor("DIRECCION", _DIRECCION)
            datos.agregarCampoValor("NOMBRE_APELLIDOS", _NOMBRE_APELLIDOS)
            datos.agregarCampoValor("ID_TIPO_USUARIO", _ID_TIPO_USUARIO)
            datos.insertar()
            _ID_USUARIO = datos.valorID

            _ERROR = ""
        Else
            _ERROR = "Usuario ya existente."
        End If
        
    End Sub
    Public Sub modificar()
        If Not existeUsuario(_USUARIO, _ID_USUARIO) Then            
            Dim datos = New datos()
            datos.tabla = "USUARIO"
            datos.campoID = "ID_USUARIO"
            datos.valorID = _ID_USUARIO
            datos.agregarCampoValor("USUARIO", _USUARIO)
            datos.agregarCampoValor("CONTRASENHA", _CONTRASENHA)
            datos.agregarCampoValor("HABILITADO", _HABILITADO)
            datos.agregarCampoValor("ADMINISTRADOR", _ADMINISTRADOR)
            datos.agregarCampoValor("CAMBIO_CONTRASENHA", _CAMBIO_CONTRASENHA)
            datos.agregarCampoValor("COMISION", _COMISION)
            datos.agregarCampoValor("DESCUENTO", _DESCUENTO)
            datos.agregarCampoValor("TELEFONO", _TELEFONO)
            datos.agregarCampoValor("CELULAR", _CELULAR)
            datos.agregarCampoValor("DIRECCION", _DIRECCION)
            datos.agregarCampoValor("NOMBRE_APELLIDOS", _NOMBRE_APELLIDOS)
            datos.agregarCampoValor("ID_TIPO_USUARIO", _ID_TIPO_USUARIO)
            datos.modificar()
            _ERROR = ""
        Else
            _ERROR = "Usuario ya existente."
        End If
        
    End Sub
    Public Sub eliminar()
        If puedeEliminar() Then            
            Dim datos = New datos()
            datos.tabla = "USUARIO"
            datos.campoID = "ID_USUARIO"
            datos.agregarCampoValor("HABILITADO", 0)
            datos.valorID = _ID_USUARIO
            datos.modificar()            
            _ERROR = ""
        Else
            _ERROR = "El Usuario que intenta eliminar tiene registros en el sistema."
        End If
        
    End Sub
    Public Sub cargarDatos()
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "SELECT * FROM USUARIO WHERE ID_USUARIO = " & _ID_USUARIO

        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then

            _USUARIO = tblConsulta.Rows(0)("USUARIO")
            _CONTRASENHA = tblConsulta.Rows(0)("CONTRASENHA")
            _HABILITADO = tblConsulta.Rows(0)("HABILITADO")
            _ADMINISTRADOR = tblConsulta.Rows(0)("ADMINISTRADOR")
            _CAMBIO_CONTRASENHA = tblConsulta.Rows(0)("CAMBIO_CONTRASENHA")            
            If IsDBNull(tblConsulta.Rows(0)("COMISION")) Then
                _COMISION = 0
            Else
                _COMISION = tblConsulta.Rows(0)("COMISION")
            End If
            If IsDBNull(tblConsulta.Rows(0)("DESCUENTO")) Then
                _DESCUENTO = 0
            Else
                _DESCUENTO = tblConsulta.Rows(0)("DESCUENTO")
            End If

            _CELULAR = tblConsulta.Rows(0)("CELULAR")
            _TELEFONO = tblConsulta.Rows(0)("TELEFONO")
            _DIRECCION = tblConsulta.Rows(0)("DIRECCION")
            _NOMBRE_APELLIDOS = tblConsulta.Rows(0)("NOMBRE_APELLIDOS")
            _ID_TIPO_USUARIO = tblConsulta.Rows(0)("ID_TIPO_USUARIO")
            _ERROR = ""
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Function mostrarUsuario(Optional ByVal buscar As String = "") As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT * FROM USUARIO "
        If Trim(buscar) <> "" Then
            sSql += "WHERE NOMBRE_APELLIDOS LIKE '%" + buscar + "%' OR USUARIO LIKE '%" & buscar & "%'"
        End If
        sSql += " ORDER BY NOMBRE_APELLIDOS , U.USUARIO "
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Sub sesion()
        Dim datos = New datos()
        Dim tabla As DataTable = datos.iniciarSesion(_USUARIO, _CONTRASENHA)
        If tabla.Rows.Count <> 0 Then
            _ID_USUARIO = tabla.Rows(0)("ID_USUARIO")            
            cargarDatos()
        Else
            _error = "Datos Incorrectos. Verifique Usuario y/o Contraseña"
        End If
    End Sub
    Private Function existeUsuario(ByVal usuario As String, Optional ByVal cod_usuario As Integer = -1) As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "SELECT * FROM USUARIO WHERE USUARIO = '" & usuario & "'"
        If cod_usuario <> -1 Then
            sSql += " AND ID_USUARIO <> " & cod_usuario
        End If
        Return datos.ejecutarConsulta(sSql).Rows.Count > 0
    End Function
    Public Sub actualizarContrasenha()
        Dim datos = New datos()
        datos.tabla = "USUARIO"
        datos.campoID = "ID_USUARIO"
        datos.valorID = _ID_USUARIO
        datos.agregarCampoValor("CONTRASENHA", _CONTRASENHA)
        datos.agregarCampoValor("CAMBIO_CONTRASENHA", 0)
        datos.modificar()
    End Sub
    Private Function puedeEliminar() As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT COUNT(*) AS TOTAL FROM VENTA V "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = V.ID_USUARIO "
        sSql += " WHERE U.ID_USUARIO = " & _ID_USUARIO
        Return CInt(datos.ejecutarConsulta(sSql).Rows(0)(0)) = 0
    End Function
    Public Function mostrarComboUsuario() As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT U.ID_USUARIO, NOMBRE_APELLIDOS FROM USUARIO U"
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = U.ID_PERSONA "
        sSql += " WHERE U.ADMINISTRADOR = 0"
        sSql += " UNION SELECT -1 , '[Todos]' ORDER BY NOMBRE_APELLIDOS "

        Return datos.ejecutarConsulta(sSql)
    End Function
    
End Class
