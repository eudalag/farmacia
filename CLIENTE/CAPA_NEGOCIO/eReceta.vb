﻿Imports CAPA_DATOS
Imports System.Data
Public Class eReceta
    Private _id_venta As Integer
    Private _id_medicamento As Integer
    Private _cantidad As Integer
    Private _precio_unitario As Decimal
    Private _precio_paquete As Decimal
    Public Property ID_VENTA As Integer
        Get
            Return _id_venta
        End Get
        Set(value As Integer)
            _id_venta = value
        End Set
    End Property
    Public Property ID_MEDICAMENTO As Integer
        Get
            Return _id_medicamento
        End Get
        Set(value As Integer)
            _id_medicamento = value
        End Set
    End Property
    Public Property CANTIDAD As Integer
        Get
            Return _cantidad
        End Get
        Set(value As Integer)
            _cantidad = value
        End Set
    End Property
    Public Property PRECIO_UNITARIO As Decimal
        Get
            Return _precio_unitario
        End Get
        Set(value As Decimal)
            _precio_unitario = value
        End Set
    End Property
    Public Property PRECIO_PAQUETE As Decimal
        Get
            Return _precio_paquete
        End Get
        Set(value As Decimal)
            _precio_paquete = value
        End Set
    End Property
    Public Sub insertar()
        Dim datos = New datos()
        datos.tabla = "VENTA_MEDICAMENTOS"
        datos.modoID = "sql"
        datos.agregarCampoValor("ID_VENTA", _id_venta)
        datos.agregarCampoValor("ID_MEDICAMENTO", _id_medicamento)
        datos.agregarCampoValor("CANTIDAD", _cantidad)
        datos.agregarCampoValor("PRECIO_UNITARIO", _precio_unitario)
        datos.agregarCampoValor("PRECIO_PAQUETE", _precio_paquete)
        datos.insertar()        
    End Sub        
    Public Function mostrarReceta(ByVal idVenta As Integer) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT ROW_NUMBER() OVER (ORDER BY R.ID_VENTA) AS NRO_ITEM,R.ID_VENTA,"
        sSql += " R.ID_MEDICAMENTO,R.CANTIDAD,R.PRECIO_UNITARIO,M.NOMBRE + ISNULL(M.DESCRIPCION,'') AS NOMBRE,0 AS TOTAL FROM RECETA R"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        sSql += " WHERE R.ID_VENTA = " & idVenta
        Return datos.ejecutarConsulta(sSql)
    End Function
End Class
