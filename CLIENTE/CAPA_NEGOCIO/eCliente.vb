﻿Imports CAPA_DATOS
Imports System.Data
Public Class eCliente
    Private _ID_CLIENTE As Integer
    Private _ID_PERSONA As Integer
    Private _NIT As String

    Private _NOMBRE_FACTURA As String
    Private _REFERENCIA_EMPRESA As String
    Private _LIMITE_CREDITO As Decimal
    Private _OBSERVACIONES As String
    Private _CREDITO As Boolean
    Private _ID_GRUPO As Integer
    Private _PERSONA As ePersona
    Private _ERROR As String

    Public Sub New()
        _PERSONA = New ePersona()
    End Sub

    Public Property ID_CLIENTE() As Integer
        Get
            Return _ID_CLIENTE
        End Get
        Set(ByVal value As Integer)
            _ID_CLIENTE = value
        End Set
    End Property
    Public Property ID_PERSONA() As Integer
        Get
            Return _ID_PERSONA
        End Get
        Set(ByVal value As Integer)
            _ID_PERSONA = value
        End Set
    End Property
    Public Property NIT As String
        Get
            Return _NIT
        End Get
        Set(ByVal value As String)
            _NIT = value
        End Set
    End Property
    Public Property PERSONA As ePersona
        Get
            Return _PERSONA
        End Get
        Set(ByVal value As ePersona)
            _PERSONA = value
        End Set
    End Property

    Public Property NOMBRE_FACTURA As String
        Get
            Return _NOMBRE_FACTURA
        End Get
        Set(ByVal value As String)
            _NOMBRE_FACTURA = value
        End Set
    End Property


    Public Property REFERENCIA_EMPRESA As String
        Get
            Return _REFERENCIA_EMPRESA
        End Get
        Set(ByVal value As String)
            _REFERENCIA_EMPRESA = value
        End Set
    End Property


    Public Property LIMITE_CREDITO As Decimal
        Get
            Return _LIMITE_CREDITO
        End Get
        Set(ByVal value As Decimal)
            _LIMITE_CREDITO = value
        End Set
    End Property


    Public Property OBSERVACIONES As String
        Get
            Return _OBSERVACIONES
        End Get
        Set(ByVal value As String)
            _OBSERVACIONES = value
        End Set
    End Property


    Public Property CREDITO As Boolean
        Get
            Return _CREDITO
        End Get
        Set(ByVal value As Boolean)
            _CREDITO = value
        End Set
    End Property

    Public Property ID_GRUPO As Integer
        Get
            Return _ID_GRUPO
        End Get
        Set(ByVal value As Integer)
            _ID_GRUPO = value
        End Set
    End Property

    Public ReadOnly Property mensajesError() As String
        Get
            mensajesError = _ERROR
        End Get
    End Property
    Public Sub insertar()
        If Not existeCliente(_NIT) Then
            _PERSONA.insertar()
            Dim datos = New datos()
            datos.tabla = "CLIENTE"
            datos.campoID = "ID_CLIENTE"
            datos.modoID = "auto"
            datos.agregarCampoValor("ID_PERSONA", _PERSONA.ID_PERSONA)
            datos.agregarCampoValor("NIT", _NIT)
            datos.agregarCampoValor("NOMBRE_FACTURA", _NOMBRE_FACTURA)
            datos.agregarCampoValor("REFERENCIA_EMPRESA", _REFERENCIA_EMPRESA)
            datos.agregarCampoValor("LIMITE_CREDITO", Decimal.Parse(_LIMITE_CREDITO, New Globalization.CultureInfo("en-US")))
            datos.agregarCampoValor("OBSERVACIONES", _OBSERVACIONES)
            datos.agregarCampoValor("CREDITO", CBool(_CREDITO))
            datos.agregarCampoValor("ID_GRUPO", _ID_GRUPO)

            datos.insertar()
            _ID_CLIENTE = datos.valorID
            _ID_PERSONA = _PERSONA.ID_PERSONA
            _ERROR = ""
        Else
            _ERROR = "El Nit/CI del Cliente que intenta Agregar ya existe en los registros del sistema"
        End If
        
    End Sub
    Public Sub modificar()
        If Not existeCliente(_NIT, _ID_CLIENTE) Then
            _PERSONA.modificar()
            Dim datos = New datos()
            datos.tabla = "CLIENTE"
            datos.campoID = "ID_CLIENTE"
            datos.valorID = _ID_CLIENTE
            datos.agregarCampoValor("NIT", _NIT)
            datos.agregarCampoValor("NOMBRE_FACTURA", _NOMBRE_FACTURA)
            datos.agregarCampoValor("REFERENCIA_EMPRESA", _REFERENCIA_EMPRESA)
            datos.agregarCampoValor("LIMITE_CREDITO", Decimal.Parse(_LIMITE_CREDITO, New Globalization.CultureInfo("en-US")))
            datos.agregarCampoValor("OBSERVACIONES", _OBSERVACIONES)
            datos.agregarCampoValor("CREDITO", CBool(_CREDITO))
            datos.agregarCampoValor("ID_GRUPO", _ID_GRUPO)
            datos.modificar()
            _ERROR = ""
        Else
            _ERROR = "El Nit/CI del Cliente que intenta Modificar ya existe en los registros del sistema"
        End If


        
    End Sub
    Public Sub eliminar()
        _PERSONA.ID_PERSONA = _ID_PERSONA
        Dim datos = New datos()
        datos.tabla = "CLIENTE"
        datos.campoID = "ID_CLIENTE"
        datos.valorID = _ID_CLIENTE
        datos.eliminar()
        _PERSONA.eliminar()
    End Sub
    Public Sub cargarDatos()
        Dim datos = New datos()
        Dim sSql As String
        sSql = "DECLARE @ID_CLIENTE AS INTEGER;"
        sSql += " SET @ID_CLIENTE = " + _ID_CLIENTE.ToString + ";"
        sSql += " SELECT  CL.ID_PERSONA,ISNULL(NIT,'') AS NIT,ISNULL(NOMBRE_FACTURA,'') AS NOMBRE_FACTURA,ISNULL(REFERENCIA_EMPRESA,'') AS REFERENCIA_EMPRESA,ISNULL(REFERENCIA_EMPRESA,'') AS NIT,"
        sSql += " ISNULL(OBSERVACIONES,'') AS OBSERVACIONES,ISNULL(CREDITO,0) AS CREDITO,ISNULL(CL.ID_GRUPO,1) AS ID_GRUPO,"
        sSql += " CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, ISNULL(TOTAL_CREDITO, 0) TOTAL_CREDITO , ISNULL(LIMITE_CREDITO,0) AS LIMITE, ISNULL(LIMITE_CREDITO,0) - ISNULL(TOTAL_CREDITO, 0) AS SALDO FROM CLIENTE CL"
        sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO  "
        sSql += " LEFT JOIN "
        sSql += " ("
        sSql += " SELECT SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,C.ID_CLIENTE    FROM CREDITO C"
        sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
        sSql += " WHERE ESTADO = 0"
        sSql += " GROUP BY C.ID_CLIENTE) C ON C.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " WHERE CL.ID_CLIENTE = @ID_CLIENTE"
        Dim tblConsulta = datos.ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            _ID_PERSONA = tblConsulta.Rows(0)("ID_PERSONA")
            _NIT = tblConsulta.Rows(0)("NIT")

            _NOMBRE_FACTURA = tblConsulta.Rows(0)("NOMBRE_FACTURA")
            _REFERENCIA_EMPRESA = tblConsulta.Rows(0)("REFERENCIA_EMPRESA")
            _LIMITE_CREDITO = tblConsulta.Rows(0)("SALDO")
            _OBSERVACIONES = tblConsulta.Rows(0)("OBSERVACIONES")
            _CREDITO = tblConsulta.Rows(0)("CREDITO")
            _ID_GRUPO = tblConsulta.Rows(0)("ID_GRUPO")

            _PERSONA.ID_PERSONA = _ID_PERSONA

            _PERSONA.cargarDatos()
        Else
            _ERROR = "No Existe Registro."
        End If
    End Sub
    Public Function mostrarCliente(Optional ByVal buscar As String = "", Optional ByVal tipo As Integer = 1) As DataTable
        Dim sSql As String = ""
        Dim datos = New datos()
        sSql += " SELECT * FROM CLIENTE C "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        If Trim(buscar) <> "" Then
            sSql += "WHERE (NOMBRE_APELLIDOS LIKE '%" + buscar + "%' OR NIT LIKE '%" & buscar & "%') AND ID_CLIENTE <> 1"
        Else
            sSql += "WHERE ID_CLIENTE <> 1"
        End If
        If tipo = 2 Then
            sSql += " AND CREDITO = 1"
        End If
        sSql += " ORDER BY  NOMBRE_APELLIDOS"
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Sub buscarClientePedido(ByVal nit As String)
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "DECLARE @NIT AS NVARCHAR(15);"
        sSql += " SET @NIT = '" & nit & "';"
        sSql += " SELECT * FROM CLIENTE WHERE NIT = @NIT"
        Dim tabla As DataTable = datos.ejecutarConsulta(sSql)
        If tabla.Rows.Count <> 0 Then
            _ID_CLIENTE = tabla.Rows(0)("ID_CLIENTE")          
            cargarDatos()          
        Else
            _error = "Cliente no valido"
        End If
    End Sub
    Private Function existeCliente(ByVal nit As String, Optional ByVal idCliente As Integer = -1) As Boolean
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql = "SELECT * FROM CLIENTE WHERE NIT = '" & nit & "' AND NIT <> '0'"
        If idCliente <> -1 Then
            sSql += " AND ID_CLIENTE <> " & idCliente
        End If
        Return datos.ejecutarConsulta(sSql).Rows.Count > 0
    End Function




End Class
