﻿Imports CAPA_DATOS
Imports System.Data
Public Class eVenta
    Private _ID_VENTA As Integer
    Private _FECHA As Date
    Private _NRO_VENTA As Integer
    Private _ID_CLIENTE As Integer
    Private _ID_PERSONA_CLIENTE As Integer
    Private _ID_USUARIO As Integer
    Private _ID_PERSONA As Integer
    Private _ID_TIPO_COMPROBANTE
    Private _ID_DOSIFICACION
    Private _CODIGO_CONTROL
    Private _CODIGO_QR
    Private _ID_CAJA
    Private _ID_BANCO As Integer
    Private _OBSERVACION As String
    Private _PAGADO As Decimal
    Private _TOTAL_PEDIDO As Decimal
    Private _DESCUENTO As Decimal
    Private _TOTAL_DESCUENTO As Decimal

    Private _EFECTIVO As Decimal
    Private _DOLARES As Decimal
    Private _TARJETA As Decimal
    Private _TC As Decimal
    Private _CREDITO As Boolean
    Private _ID_PACIENTE As String
    Private _ID_RECETA As String
    Private _TRATAMIENTO As String
    Private _VOUCHER As String
    Private _COTIZACION As Boolean = False


    Public Property ID_VENTA As Integer
        Get
            Return _ID_VENTA
        End Get
        Set(value As Integer)
            _ID_VENTA = value
        End Set
    End Property
    Public Property ID_CAJA As String
        Get
            Return _ID_CAJA
        End Get
        Set(value As String)
            _ID_CAJA = value
        End Set
    End Property

    Public Property ID_BANCO As Integer
        Get
            Return _ID_BANCO
        End Get
        Set(value As Integer)
            _ID_BANCO = value
        End Set
    End Property
    Public Property FECHA As Date
        Get
            Return _FECHA
        End Get
        Set(value As Date)
            _FECHA = value
        End Set
    End Property
    Public Property NRO_VENTA As Integer
        Get
            Return _NRO_VENTA
        End Get
        Set(value As Integer)
            _NRO_VENTA = value
        End Set
    End Property
    Public Property ID_CLIENTE As Integer
        Get
            Return _ID_CLIENTE
        End Get
        Set(value As Integer)
            _ID_CLIENTE = value
        End Set
    End Property
    Public Property ID_PERSONA_CLIENTE As Integer
        Get
            Return _ID_PERSONA_CLIENTE
        End Get
        Set(value As Integer)
            _ID_PERSONA_CLIENTE = value
        End Set
    End Property
    Public Property ID_USUARIO As Integer
        Get
            Return _ID_USUARIO
        End Get
        Set(value As Integer)
            _ID_USUARIO = value
        End Set
    End Property
    Public Property ID_PERSONA As Integer
        Get
            Return _ID_PERSONA
        End Get
        Set(value As Integer)
            _ID_PERSONA = value
        End Set
    End Property
    Public Property ID_TIPO_COMPROBANTE
        Get
            Return _ID_TIPO_COMPROBANTE
        End Get
        Set(value)
            _ID_TIPO_COMPROBANTE = value
        End Set
    End Property
    Public Property ID_DOSIFICACION
        Get
            Return _ID_DOSIFICACION
        End Get
        Set(value)
            _ID_DOSIFICACION = value
        End Set
    End Property
    Public Property CODIGO_CONTROL
        Get
            Return _CODIGO_CONTROL
        End Get
        Set(value)
            _CODIGO_CONTROL = value
        End Set
    End Property
    Public Property CODIGO_QR
        Get
            Return _CODIGO_QR
        End Get
        Set(value)
            _CODIGO_QR = value
        End Set
    End Property

    Public Property OBSERVACION As String
        Get
            Return _OBSERVACION
        End Get
        Set(value As String)
            _OBSERVACION = value
        End Set
    End Property
    Public Property PAGADO As Decimal
        Get
            Return _PAGADO
        End Get
        Set(value As Decimal)
            _PAGADO = value
        End Set
    End Property
    Public Property TOTAL_PEDIDO As Decimal
        Get
            Return _TOTAL_PEDIDO
        End Get
        Set(value As Decimal)
            _TOTAL_PEDIDO = value
        End Set
    End Property
    Public Property DESCUENTO As Decimal
        Get
            Return _DESCUENTO
        End Get
        Set(value As Decimal)
            _DESCUENTO = value
        End Set
    End Property
    Public Property TOTAL_DESCUENTO As Decimal
        Get
            Return _TOTAL_DESCUENTO
        End Get
        Set(value As Decimal)
            _TOTAL_DESCUENTO = value
        End Set
    End Property
    Public Property EFECTIVO As Decimal
        Get
            Return _EFECTIVO
        End Get
        Set(value As Decimal)
            _EFECTIVO = value
        End Set
    End Property

    Public Property DOLARES As Decimal
        Get
            Return _DOLARES
        End Get
        Set(value As Decimal)
            _DOLARES = value
        End Set
    End Property
    Public Property TARJETA As Decimal
        Get
            Return _TARJETA
        End Get
        Set(value As Decimal)
            _TARJETA = value
        End Set
    End Property
    Public Property TC As Decimal
        Get
            Return _TC
        End Get
        Set(value As Decimal)
            _TC = value
        End Set
    End Property

    Public Property CREDITO As Boolean
        Get
            Return _CREDITO
        End Get
        Set(value As Boolean)
            _CREDITO = value
        End Set
    End Property

    Public Property ID_PACIENTE As String
        Get
            Return _ID_PACIENTE
        End Get
        Set(value As String)
            _ID_PACIENTE = value
        End Set
    End Property
    Public Property VOUCHER As String
        Get
            Return _VOUCHER
        End Get
        Set(value As String)
            _VOUCHER = value
        End Set
    End Property

    Public Property ID_RECETA As String
        Get
            Return _ID_RECETA
        End Get
        Set(value As String)
            _ID_RECETA = value
        End Set
    End Property

    Public Property TRATAMIENTO As String
        Get
            Return _TRATAMIENTO
        End Get
        Set(value As String)
            _TRATAMIENTO = value
        End Set
    End Property

    Public Property COTIZACION As Boolean
        Get
            Return _COTIZACION
        End Get
        Set(value As Boolean)
            _COTIZACION = value
        End Set
    End Property

    Public Sub INSERTAR()
        Dim datos = New datos()
        datos.tabla = "VENTA"
        datos.campoID = "ID_VENTA"
        datos.modoID = "auto"
        datos.agregarCampoValor("FECHA", _FECHA)
        datos.agregarCampoValor("NRO_VENTA", _NRO_VENTA)
        datos.agregarCampoValor("ID_CLIENTE", _ID_CLIENTE)                

        datos.agregarCampoValor("ID_TIPO_COMPROBANTE", _ID_TIPO_COMPROBANTE)
        datos.agregarCampoValor("ID_DOSIFICACION", _ID_DOSIFICACION)
        datos.agregarCampoValor("CODIGO_CONTROL", _CODIGO_CONTROL)
        If _CODIGO_CONTROL <> "null" Then
            datos.agregarCampoValor("CODIGO_QR", _CODIGO_QR)
        End If
        'datos.agregarCampoValor("ID_CAJA", _ID_CAJA)
        datos.agregarCampoValor("ID_BANCO", _ID_BANCO)
        If Not _OBSERVACION Is Nothing Then
            datos.agregarCampoValor("OBSERVACION", _OBSERVACION)
        End If
        datos.agregarCampoValor("PAGADO", _PAGADO)
        datos.agregarCampoValor("FECHA_AUD", "serverDateTime")
        datos.agregarCampoValor("TOTAL_PEDIDO", _TOTAL_PEDIDO)
        datos.agregarCampoValor("DESCUENTO", _DESCUENTO)
        datos.agregarCampoValor("TOTAL_DESCUENTO", _TOTAL_DESCUENTO)
        datos.agregarCampoValor("EFECTIVO", _EFECTIVO)
        datos.agregarCampoValor("DOLARES", _DOLARES)
        datos.agregarCampoValor("TARJETA", _TARJETA)
        datos.agregarCampoValor("TC", _TC)
        datos.agregarCampoValor("CREDITO", _CREDITO)
        datos.agregarCampoValor("COTIZACION", _COTIZACION)
        If Not _ID_PACIENTE Is Nothing Then
            datos.agregarCampoValor("ID_PACIENTE", _ID_PACIENTE)
        End If
        If Not _TRATAMIENTO Is Nothing Then
            datos.agregarCampoValor("TRATAMIENTO", _TRATAMIENTO)
        End If
        If Not _ID_RECETA Is Nothing Then
            datos.agregarCampoValor("ID_RECETA", _ID_RECETA)
        End If
        If Not _VOUCHER Is Nothing Then
            datos.agregarCampoValor("NRO_VOUCHER", _VOUCHER)
        End If
        datos.insertar()
        _ID_VENTA = datos.valorID
    End Sub
    Public Sub MODIFICAR()
        Dim datos = New datos()
        datos.tabla = "VENTA"
        datos.campoID = "ID_VENTA"
        datos.valorID = _ID_VENTA
        'datos.agregarCampoValor("OBSERVACION", _OBSERVACION)
        datos.agregarCampoValor("ID_CLIENTE", _ID_CLIENTE)
        If _NRO_VENTA <> 0 Then
            datos.agregarCampoValor("NRO_VENTA", _NRO_VENTA)
        End If
        datos.agregarCampoValor("ID_TIPO_COMPROBANTE", _ID_TIPO_COMPROBANTE)
        datos.agregarCampoValor("ID_DOSIFICACION", _ID_DOSIFICACION)
        datos.agregarCampoValor("CODIGO_CONTROL", _CODIGO_CONTROL)
        If _CODIGO_CONTROL <> "null" Then
            datos.agregarCampoValor("CODIGO_QR", _CODIGO_QR)
        End If
        If Not _ID_CAJA Is Nothing Then
            datos.agregarCampoValor("ID_CAJA", _ID_CAJA)
        End If
        If Not _OBSERVACION Is Nothing Then
            datos.agregarCampoValor("OBSERVACION", _OBSERVACION)
        End If
        datos.agregarCampoValor("PAGADO", _PAGADO)
        datos.agregarCampoValor("FECHA_AUD", "serverDateTime")
        datos.agregarCampoValor("TOTAL_PEDIDO", _TOTAL_PEDIDO)
        datos.agregarCampoValor("DESCUENTO", _DESCUENTO)
        datos.agregarCampoValor("TOTAL_DESCUENTO", _TOTAL_DESCUENTO)
        datos.agregarCampoValor("EFECTIVO", _EFECTIVO)
        datos.agregarCampoValor("DOLARES", _DOLARES)
        datos.agregarCampoValor("TARJETA", _TARJETA)
        datos.agregarCampoValor("TC", _TC)
        datos.agregarCampoValor("CREDITO", _CREDITO)
        datos.agregarCampoValor("COTIZACION", _COTIZACION)
        datos.agregarCampoValor("ID_BANCO", _ID_BANCO)
        datos.modificar()
    End Sub
    Public Function mostrarVentas(ByVal desde As String, ByVal hasta As String, ByVal tipoMonto As Integer, ByVal monto As Decimal, ByVal tipoPago As Integer, ByVal cliente As String, ByVal banco As Integer, ByVal tipoMovimiento As Integer, ByVal nroComprobante As String) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""

        'sSql += " DECLARE @DESDE AS DATE,@HASTA AS DATE,@MONTO AS DECIMAL,@TIPOPAGO AS INTEGER, @CLIENTE AS NVARCHAR(150),@BANCO AS INTEGER,"
        'sSql += " @TIPOMOVIMIENTO AS INTEGER, @NROCOMPROBANTE AS NVARCHAR(10);"

        'sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        'sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        'sSql += " SET @MONTO =  " + monto.ToString + " ;"
        'sSql += " SET @TIPOPAGO = " + tipoPago.ToString + ";"
        'sSql += " SET @CLIENTE = '" + cliente.Trim + "';"
        'sSql += " SET @BANCO = " + banco.ToString + ";"
        'sSql += " SET @TIPOMOVIMIENTO = " + tipoMovimiento.ToString + ";"
        'sSql += " SET @NROCOMPROBANTE = '" + nroComprobante.Trim + "';"

        'sSql += " SELECT CONVERT(NVARCHAR(10),V.FECHA_AUD,103) + ' ' + CONVERT(NVARCHAR(5),V.FECHA_AUD,108) AS FECHA,V.ID_VENTA,V.NRO_VENTA, "
        'sSql += " P.NOMBRE_APELLIDOS AS NOMBRE_CLIENTE, C.NIT,"
        'sSql += " CASE WHEN V.CREDITO = 1 THEN UPPER(PC.NOMBRE_APELLIDOS) ELSE "
        'sSql += " CASE WHEN LTRIM(C.NIT) = '' THEN UPPER(P.NOMBRE_APELLIDOS) ELSE C.NIT + ' - ' + UPPER(P.NOMBRE_APELLIDOS) END END AS CLIENTE,"
        'sSql += " CASE WHEN CR.ID_CREDITO IS NULL THEN CASE WHEN V.TARJETA > 0 THEN 'TARJETA - (Nro.: ' + ISNULL(CAST(V.NRO_VOUCHER AS NVARCHAR(18)),'') + ')' ELSE 'EFECTIVO'END ELSE 'CREDITO' END AS TIPO_PAGO,"
        'sSql += " V.TOTAL_PEDIDO - V.TOTAL_DESCUENTO AS TOTAL, TC.DETALLE, TC.ID_TIPO_COMPROBANTE, "
        'sSql += " CASE WHEN V.ANULADA = 1 THEN 'Anulada' ELSE 'Valido' END AS ESTADO, V.ANULADA           "
        'sSql += " FROM VENTA V  "
        'sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE  "
        'sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE   "
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA  "
        'sSql += " LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA"
        'sSql += " LEFT JOIN CLIENTE CC ON CC.ID_CLIENTE = CR.ID_CLIENTE "
        'sSql += " LEFT JOIN PERSONA PC ON PC.ID_PERSONA = CC.ID_PERSONA  "
        'sSql += " WHERE CONVERT(DATE,FECHA_AUD,103) >= @DESDE AND CONVERT(DATE,FECHA_AUD,103) <= @HASTA"
        'sSql += " AND V.CREDITO = CASE WHEN @TIPOPAGO = -1 THEN V.CREDITO WHEN @TIPOPAGO = 1 THEN 0 WHEN @TIPOPAGO = 2 THEN 1 END "
        'sSql += " AND (P.NOMBRE_APELLIDOS LIKE '%' + @CLIENTE + '%' OR PC.NOMBRE_APELLIDOS LIKE '%' + @CLIENTE + '%')"
        'sSql += " AND V.ID_BANCO = CASE WHEN @BANCO = -1 THEN V.ID_BANCO ELSE @BANCO END"
        'sSql += " AND CAST(V.NRO_VENTA AS NVARCHAR(10)) LIKE '%' + @NROCOMPROBANTE  + '%'"


        sSql += " DECLARE @DESDE AS DATE,@HASTA AS DATE,@BANCO AS INTEGER,"
        sSql += " @TOTAL_VENTA AS DECIMAL(18,2), @TOTAL_MOVIMIENTO AS DECIMAL(18,2), @SALDO_ANTERIOR AS DECIMAL(18,2);"

        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SET @BANCO = " + banco.ToString + ";"


        sSql += " SELECT @TOTAL_VENTA = ISNULL(SUM (TOTAL_PEDIDO - TOTAL_DESCUENTO),0) FROM VENTA V "
        sSql += " LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
        sSql += " WHERE CONVERT(DATE,V.FECHA_AUD,103) < @DESDE AND V.ANULADA = 0 AND V.TARJETA = 0"
        sSql += " AND V.ID_BANCO = @BANCO AND CR.ID_VENTA IS NULL"

        sSql += " SELECT @TOTAL_MOVIMIENTO = ISNULL(SUM(IMPORTE),0)  FROM MOVIMIENTO_BANCO MB "
        sSql += " WHERE MB.ID_BANCO = @BANCO AND CONVERT(DATE,MB.FH_MOVIMIENTO,103) < @DESDE AND ANULADO = 0 "

        sSql += " SET @SALDO_ANTERIOR = @TOTAL_VENTA + @TOTAL_MOVIMIENTO;"

        sSql += " SELECT @SALDO_ANTERIOR AS SALDO_ANTERIOR,*,CASE WHEN TOTAL_INGRESOS > 0  THEN TOTAL_INGRESOS ELSE 0 END AS INGRESO,CASE WHEN TOTAL_INGRESOS < 0  THEN TOTAL_INGRESOS ELSE 0 END AS EGRESO	 ,SUM(INGRESOS) OVER (PARTITION BY ID_BANCO   "
        sSql += " ORDER BY FECHA_AUD   "
        sSql += " ROWS UNBOUNDED PRECEDING) + @SALDO_ANTERIOR  AS SUBTOTAL FROM ("
        sSql += " SELECT CONVERT(NVARCHAR(10),V.FECHA_AUD,103) + ' ' + CONVERT(NVARCHAR(5),V.FECHA_AUD,108) AS FECHA,V.ID_VENTA,V.NRO_VENTA, "
        sSql += " CASE WHEN V.ID_TIPO_COMPROBANTE =  2 THEN"
        sSql += " CASE WHEN V.CREDITO = 1 THEN UPPER(PC.NOMBRE_APELLIDOS) ELSE "
        sSql += " CASE WHEN LTRIM(C.NIT) = '' THEN UPPER(PF.NOMBRE_APELLIDOS) ELSE CF.NIT + ' - ' + UPPER(PF.NOMBRE_APELLIDOS) END END"
        sSql += " ELSE"
        sSql += " CASE WHEN V.CREDITO = 1 THEN UPPER(PC.NOMBRE_APELLIDOS) ELSE "
        sSql += " CASE WHEN LTRIM(C.NIT) = '' THEN UPPER(P.NOMBRE_APELLIDOS) ELSE C.NIT + ' - ' + UPPER(P.NOMBRE_APELLIDOS) END END"
        sSql += " END AS CLIENTE,"
        sSql += " CASE WHEN CR.ID_CREDITO IS NULL THEN CASE WHEN V.TARJETA > 0 THEN 'TARJETA - (Nro.: ' + ISNULL(CAST(V.NRO_VOUCHER AS NVARCHAR(18)),'') + ')' ELSE 'EFECTIVO'END ELSE 'CREDITO' END AS TIPO_PAGO,"
        sSql += " CASE WHEN V.ANULADA = 1 THEN 0 ELSE V.TOTAL_PEDIDO - V.TOTAL_DESCUENTO END AS TOTAL_INGRESOS,"
        sSql += " CASE WHEN V.ANULADA = 1 THEN 0 ELSE CASE WHEN V.CREDITO > 0 THEN 0 WHEN V.TARJETA > 0 THEN 0 ELSE  V.TOTAL_PEDIDO - V.TOTAL_DESCUENTO END END AS INGRESOS"
        sSql += " , TC.DETALLE, "
        sSql += " CASE WHEN V.ANULADA = 1 THEN 'Anulada' ELSE 'Valido' END AS ESTADO, V.ANULADA,V.ID_BANCO,V.FECHA_AUD,"
        sSql += " '1.' + CAST(V.ID_TIPO_COMPROBANTE AS NVARCHAR(2)) AS TIPO_COMPROBANTE"
        sSql += " FROM VENTA V  "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE  "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE   "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA  "
        sSql += " LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA"
        sSql += " LEFT JOIN CLIENTE CC ON CC.ID_CLIENTE = CR.ID_CLIENTE "
        sSql += " LEFT JOIN PERSONA PC ON PC.ID_PERSONA = CC.ID_PERSONA  "
        sSql += " LEFT JOIN FACTURA F ON F.ID_VENTA = V.ID_VENTA "
        sSql += " LEFT JOIN CLIENTE CF ON CF.ID_CLIENTE = F.ID_CLIENTE "
        sSql += " LEFT JOIN PERSONA PF ON PF.ID_PERSONA = CF.ID_PERSONA "
        sSql += " WHERE CONVERT(DATE,FECHA_AUD,103) >= @DESDE AND CONVERT(DATE,FECHA_AUD,103) <= @HASTA"
        sSql += " AND V.ID_BANCO = CASE WHEN @BANCO = -1 THEN V.ID_BANCO ELSE @BANCO END"

        sSql += " UNION ALL "

        sSql += " SELECT CONVERT(NVARCHAR(10),MB.FH_MOVIMIENTO ,103) + ' ' + CONVERT(NVARCHAR(5),MB.FH_MOVIMIENTO,108) AS FECHA,"
        sSql += " ID_MOVIMIENTO,MB.ID_MOVIMIENTO,MB.GLOSA,'EFECTIVO',CASE WHEN MB.ANULADO  = 1 THEN 0 ELSE MB.IMPORTE END AS IMPORTE,CASE WHEN MB.ANULADO  = 1 THEN 0 ELSE MB.IMPORTE END AS TOTAL_IMPORTE,TMC.DESCRIPCION,CASE WHEN MB.ANULADO  = 1 THEN 'Anulada' ELSE 'Valido' END AS ESTADO,"
        sSql += " MB.ANULADO,MB.ID_BANCO, MB.FH_MOVIMIENTO,'2.' + CAST(TMC.ID_TIPO_MOVIMIENTO AS NVARCHAR(2)) AS TIPO_COMPROBANTE  FROM MOVIMIENTO_BANCO MB"
        sSql += " INNER JOIN TIPO_MOVIMIENTO_CAJA TMC ON TMC.ID_TIPO_MOVIMIENTO = MB.ID_TIPO_MOVIMIENTO"
        sSql += " WHERE CONVERT(DATE,MB.FH_MOVIMIENTO,103) >= @DESDE AND CONVERT(DATE,MB.FH_MOVIMIENTO,103) <= @HASTA"
        sSql += " AND MB.ID_BANCO = CASE WHEN @BANCO = -1 THEN MB.ID_BANCO ELSE @BANCO END AND ANULADO = 0"

        sSql += " UNION ALL"

        sSql += " SELECT CONVERT(NVARCHAR(10),C.F_PAGO ,103) + ' ' + CONVERT(NVARCHAR(5),C.F_PAGO,108) AS FECHA,"
        sSql += " c.ID_CONSULTA,C.ID_CONSULTA, PC.NOMBRE_APELLIDOS AS CLIENTE,"
        sSql += " CASE WHEN C.PAGO_EFECTIVO  =  0 THEN 'TARJETA - (Nro.: ' + ISNULL(CAST(C.NRO_VOUCHER AS NVARCHAR(18)),'') + ')' ELSE 'EFECTIVO'END AS TIPO_PAGO,"
        sSql += " CASE WHEN C.ANULADO  = 1 THEN 0 ELSE C.TOTAL_CONSULTA  END AS IMPORTE,"
        sSql += " CASE WHEN C.ANULADO  = 1 THEN 0 ELSE CASE WHEN C.PAGO_EFECTIVO = 1 THEN C.TOTAL_CONSULTA ELSE 0 END END AS TOTAL_IMPORTE,"
        sSql += " CM.NOMBRE_CAMPANHA,CASE WHEN C.ANULADO  = 1 THEN 'Anulada' ELSE 'Valido' END AS ESTADO,C.ANULADO,"
        sSql += " C.ID_BANCO,C.F_PAGO,'3.1' AS TIPO_COMPROBANTE"
        sSql += " FROM CONSULTA C"
        sSql += " INNER JOIN CAMPANHA_MEDICA CM ON CM.ID_CAMPANHA = C.ID_CAMPANHA "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA PC ON PC.ID_PERSONA = CL.ID_PERSONA "
        sSql += " WHERE CONVERT(DATE,C.F_PAGO ,103) >= @DESDE AND CONVERT(DATE,C.F_PAGO,103) <= @HASTA"
        sSql += " AND C.ID_BANCO = CASE WHEN @BANCO = -1 THEN C.ID_BANCO ELSE @BANCO END "

        sSql += " ) AS V"

        sSql += " ORDER BY  V.FECHA_AUD  DESC,NRO_VENTA DESC"


        'sSql += " ORDER BY  V.FECHA_AUD  DESC,NRO_VENTA DESC"
        Dim tbl As DataTable = datos.ejecutarConsulta(sSql)
        Dim filtro As String = ""
        If monto > 0 Then
            Select Case tipoMonto
                Case 1
                    filtro += " TOTAL_INGRESOS < " + monto
                Case 2
                    filtro += " TOTAL_INGRESOS <= " + monto
                Case 3
                    filtro += " TOTAL_INGRESOS = " + monto
                Case 4
                    filtro += " TOTAL_INGRESOS >= " + monto
                Case 5
                    filtro += " TOTAL_INGRESOS > " + monto
            End Select
        End If
        If tipoPago <> -1 Then
            filtro += " AND TIPO_PAGO = '" + IIf(tipoPago = 1, "EFECTIVO", "CREDITO") + "'"
        End If

        If cliente.Trim <> "" Then
            filtro += " AND CLIENTE '%" + cliente.Trim + "%'"
        End If

        If nroComprobante.Trim <> "" Then
            filtro += " AND NRO_VENTA = " + nroComprobante.Trim
        End If
        'Dim tblFiltro = tbl.Select(filtro)

        ''DataTable dt = CreateTable(); //returns 3 rows
        ''  DataTable dtTemp = CreateTable().Clone(); //copy the schema structure
        ''  DataRow[] findRow = dt.Select(string.Format("LastName='{0}'",filterValue));

        ''  foreach (DataRow row in findRow) {
        ''      AddToTempTable(dtTemp, row);
        ''  }
        'Dim tblResultado = tbl.Clone
        'For Each item In tbl.Select(filtro)
        '    tblResultado.Rows.Add(item)
        'Next
        Return tbl
    End Function
    Public Function obtenerNroPedido(ByVal tipoComprobante As Integer, Optional idDosificacion As Integer = -1) As Integer
        Dim datos = New datos()
        Dim sSql As String = ""

        If idDosificacion > -1 Then
            sSql += "SELECT COUNT(*)  AS NUEVO_PEDIDO FROM FACTURA WHERE"
            sSql += " ID_DOSIFICACION = " & idDosificacion
        Else
            sSql += "SELECT COUNT(*) AS NUEVO_PEDIDO FROM VENTA WHERE ID_TIPO_COMPROBANTE = " & tipoComprobante.ToString
        End If
        Return CInt(datos.ejecutarConsulta(sSql).Rows(0)(0))
    End Function
    Public Function exportarLibroVenta(ByVal f_desde As String, ByVal f_hasta As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @F_DESDE AS DATE, @F_HASTA AS DATE;"
        sSql += " SET @F_DESDE = CONVERT(DATE,'" & f_desde & "',103);"
        sSql += " SET @F_HASTA = CONVERT(DATE,'" & f_hasta & "',103);"
        sSql += " SELECT V.FECHA, V.NRO_VENTA,D.NUMERO_AUTORIZACION,CASE WHEN V.ANULADA = 1 THEN 'A' ELSE 'V' END AS ESTADO,"
        sSql += " C.NIT,P.NOMBRE_APELLIDOS, SUM(R.CANTIDAD * R.PRECIO_UNITARIO ) AS IMPORTE,"
        sSql += " 0 AS ICE, 0 AS	EXPORTACION, 0 AS VTA, V.CODIGO_CONTROL "
        sSql += " FROM VENTA V"
        sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        sSql += " INNER JOIN DOSIFICACION D ON D.ID_DOSIFICACION = V.ID_DOSIFICACION "
        sSql += " WHERE CONVERT(DATE,V.FECHA,103) >= @F_DESDE AND CONVERT(DATE,V.FECHA,103) <= @F_HASTA AND V.ID_TIPO_COMPROBANTE = 1"
        sSql += " GROUP BY  V.FECHA, V.NRO_VENTA,D.NUMERO_AUTORIZACION,V.ANULADA,V.CODIGO_CONTROL,C.NIT,P.NOMBRE_APELLIDOS  "
        Dim datos = New datos()
        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function mostrarComboTipoComprobante(Optional ByVal tipo As Integer = 1) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT * FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE IN (1,2) "
        If tipo <> 1 Then
            sSql += " UNION SELECT -1, '[Todos]'"
        End If
        sSql += "ORDER BY ID_TIPO_COMPROBANTE "

        Return datos.ejecutarConsulta(sSql)
    End Function
    Public Function cargarDatosPedido(ByVal id_venta As String) As DataTable
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + id_venta + ";"


        sSql += " SELECT V.FECHA,C.ID_CLIENTE,V.ID_VENTA ,C.NIT , "
        sSql += " CASE WHEN V.CREDITO = 1 THEN PC.NOMBRE_APELLIDOS WHEN V.ID_TIPO_COMPROBANTE = 2 THEN PF.NOMBRE_APELLIDOS ELSE P.NOMBRE_APELLIDOS  END  AS NOMBRE_CLIENTE,"
        sSql += " ISNULL(U.USUARIO,'') AS USUARIO, CASE WHEN F.NRO_FACTURA IS NULL THEN TC.DETALLE ELSE 'FACTURA' END AS DETALLE, TC.ID_TIPO_COMPROBANTE, V.DESCUENTO,V.NRO_VENTA,V.CREDITO,"
        sSql += " V.TOTAL_DESCUENTO,V.DOLARES,V.TARJETA, V.EFECTIVO, V.TC, V.PAGADO, V.TOTAL_PEDIDO "
        sSql += " FROM  VENTA V "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE  "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        sSql += " LEFT JOIN CAJA CJ ON CJ.ID_CAJA = V.ID_CAJA "
        sSql += " LEFT JOIN USUARIO U ON U.ID_USUARIO = CJ.ID_USUARIO "
        sSql += " LEFT JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
        sSql += " LEFT JOIN CLIENTE CLR ON CLR.ID_CLIENTE = CR.ID_CLIENTE "
        sSql += " LEFT JOIN PERSONA PC ON PC.ID_PERSONA = CLR.ID_PERSONA "
        sSql += " LEFT JOIN FACTURA F ON F.ID_VENTA = V.ID_VENTA  "
        sSql += " LEFT JOIN CLIENTE CF ON CF.ID_CLIENTE = F.ID_CLIENTE "
        sSql += " LEFT JOIN PERSONA PF ON PF.ID_PERSONA = CF.ID_PERSONA "
        sSql += " WHERE V.ID_VENTA   =  @ID_VENTA "
        Return datos.ejecutarConsulta(sSql)
    End Function
End Class
