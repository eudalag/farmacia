/*
   lunes, 26 de septiembre de 201610:12:08
   Usuario: sa
   Servidor: 127.0.0.1
   Base de datos: SISTEMA_FARMACIA
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.VENTA ADD
	CREDITO bit NULL,
	ID_PACIENTE numeric(18, 0) NULL,
	ID_RECETA numeric(18, 0) NULL
GO
ALTER TABLE dbo.VENTA SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
