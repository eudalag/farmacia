﻿Imports CAPA_NEGOCIO

Imports ClosedXML.Excel
Imports System.Data
Public Class form_exportar_libro_venta

    Private Sub obtenerDatosVentas()

        Dim tVenta = New eVenta()
        Dim tblVenta As DataTable = tVenta.exportarLibroVenta(txtDesdeVenta.Text, txtHastaVenta.Text)
        Dim wb As New XLWorkbook()

        Dim ws = wb.Worksheets.Add("VENTAS")
        Dim i As Integer = 1
        Dim pos As String = "0"

        'pos = CStr(i)

        'ws.Cell("A" & pos).SetValue("NIT").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("B" & pos).SetValue("RAZON SOCIAL").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("C" & pos).SetValue("NUMERO DE FACTURA").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("D" & pos).SetValue("NUMERO DE AUTORIZACION").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("E" & pos).SetValue("FECHA").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("F" & pos).SetValue("IMPORTE TOTAL").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("G" & pos).SetValue("IMPORTE ICE").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("H" & pos).SetValue("IMPORTE EXCENTO").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("I" & pos).SetValue("IMPORTE NETO").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("J" & pos).SetValue("DEBITO FISCAL").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("K" & pos).SetValue("ESTADO").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        'ws.Cell("L" & pos).SetValue("CODIGO DE CONTROL").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)

        'Dim rngTable2 = ws.Range("A" & pos & ":L" & pos)
        'rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(119, 119, 119)).Font.SetBold(True)
        'rngTable2.SetAutoFilter()
        'i = i + 1

        For Each ventas In tblVenta.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(CDate(ventas("FECHA")).ToString("dd/MM/yyyy"))
            ws.Cell("B" & pos).SetValue(ventas("NRO_VENTA"))

            ws.Cell("C" & pos).SetValue(ventas("NUMERO_AUTORIZACION"))
            ws.Cell("D" & pos).SetValue(ventas("ESTADO"))

            ws.Cell("E" & pos).SetValue(ventas("NIT"))
            ws.Cell("F" & pos).SetValue(ventas("NOMBRE_APELLIDOS"))
            ws.Cell("G" & pos).SetValue(ventas("IMPORTE"))

            ws.Cell("H" & pos).SetValue(0)
            ws.Cell("I" & pos).SetValue(0)

            ws.Cell("J" & pos).SetValue(0)
            ws.Cell("K" & pos).SetValue(ventas("IMPORTE"))
            ws.Cell("L" & pos).SetValue(0)
            ws.Cell("M" & pos).SetValue(ventas("IMPORTE"))
            ws.Cell("N" & pos).SetValue(ventas("IMPORTE") * 0.13)
            ws.Cell("O" & pos).SetValue(ventas("CODIGO_CONTROL"))
            i = i + 1
        Next
        ws.Column("G").Style.NumberFormat.Format = "#0.00"
        ws.Column("H").Style.NumberFormat.Format = "#0.00"
        ws.Column("I").Style.NumberFormat.Format = "#0.00"
        ws.Column("J").Style.NumberFormat.Format = "#0.00"
        ws.Column("K").Style.NumberFormat.Format = "#0.00"
        ws.Column("L").Style.NumberFormat.Format = "#0.00"
        ws.Column("M").Style.NumberFormat.Format = "#0.00"
        ws.Column("N").Style.NumberFormat.Format = "#0.00"


        ws.ColumnsUsed.AdjustToContents()
        wb.SaveAs(txtDestino.Text & "VENTAS_" & CDate(txtHastaVenta.Text).ToString("MMyyyy") & "_" & form_sesion.cConfiguracion.NIT & ".xlsx")
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        If txtDestino.Text = "" Then
            Dim mensaje As String = "Escoja una ubicación valida."
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.Critical
            title = "Error"
            response = MsgBox(mensaje, style, title)
        Else            
            obtenerDatosVentas()
            Dim mensaje As String = "Archivo Exportado con exito."
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.OkOnly
            title = "Exportar Excel"
            response = MsgBox(mensaje, style, title)

        End If
    End Sub

    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        txtDestino.Text = ""
        Dim dlgDestino As New FolderBrowserDialog
        With dlgDestino
            .Description = "Seleccione el directorio de destino del libro de ventas a exportar. "
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim strDestino As String = .SelectedPath.ToString
                If Not strDestino.EndsWith("\") Then
                    strDestino = strDestino & "\"
                End If
                Me.txtDestino.Text = strDestino
            End If
        End With
    End Sub
End Class