﻿Public Class Alleged_RC4

    Public Function RC4(ByVal Expression As String, ByVal Password As String, Optional ByVal guion As Boolean = True) As String
        On Error Resume Next
        Dim RB(255) As Short
        Dim Y, X, Z As Integer
        Dim Key() As Byte
        Dim ByteArray() As Byte
        Dim Temp As Byte
        Dim aux As String = ""
        If Len(Password) = 0 Then
            Return ""
        End If
        If Len(Expression) = 0 Then
            Return ""
        End If
        If Len(Password) > 256 Then
            'UPGRADE_ISSUE: No se actualizó la constante vbFromUnicode. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2070"'
            'UPGRADE_TODO: El código se actualizó para usar System.Text.UnicodeEncoding.Unicode.GetBytes(), que podría no tener el mismo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1059"'
            'Key = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(VB.Left(Password, 256), vbFromUnicode))
            Key = System.Text.Encoding.GetEncoding(1252).GetBytes(Microsoft.VisualBasic.Left(Password, 256))
        Else
            'UPGRADE_ISSUE: No se actualizó la constante vbFromUnicode. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2070"'
            'UPGRADE_TODO: El código se actualizó para usar System.Text.UnicodeEncoding.Unicode.GetBytes(), que podría no tener el mismo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1059"'
            'Key = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(Password, vbFromUnicode))
            Key = System.Text.Encoding.GetEncoding(1252).GetBytes(Password)
        End If
        For X = 0 To 255
            RB(X) = X
        Next X
        X = 0
        Y = 0
        Z = 0
        For X = 0 To 255
            Y = (Y + RB(X) + Key(X Mod Len(Password))) Mod 256
            Temp = RB(X)
            RB(X) = RB(Y)
            RB(Y) = Temp
        Next X
        X = 0
        Y = 0
        Z = 0
        'UPGRADE_ISSUE: No se actualizó la constante vbFromUnicode. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2070"'
        'UPGRADE_TODO: El código se actualizó para usar System.Text.UnicodeEncoding.Unicode.GetBytes(), que podría no tener el mismo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1059"'
        'ByteArray = System.Text.UnicodeEncoding.Unicode.GetBytes(StrConv(Expression, vbFromUnicode))
        ByteArray = System.Text.Encoding.GetEncoding(1252).GetBytes(Expression)
        For X = 0 To Len(Expression)
            Y = (Y + 1) Mod 256
            Z = (Z + RB(Y)) Mod 256
            Temp = RB(Y)
            RB(Y) = RB(Z)
            RB(Z) = Temp
            ByteArray(X) = ByteArray(X) Xor (RB((RB(Y) + RB(Z)) Mod 256))
            aux = aux & Microsoft.VisualBasic.Right(New String("0", 2) & Hex(ByteArray(X)), 2)
            If guion And (X < Len(Expression) - 1) Then
                aux = aux & "-"
            End If
        Next X
        RC4 = aux 'StrConv(ByteArray, vbUnicode)

    End Function
End Class
