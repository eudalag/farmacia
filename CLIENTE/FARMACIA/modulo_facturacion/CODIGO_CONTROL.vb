﻿Public Class CODIGO_CONTROL
    Private _nro_autorizacion As String
    Private _nro_factura As String

    Private _nit As String
    Private _fecha As String
    Private _monto As String
    Private _llave As String
    Private _fecha_lim_emision As String
    Private _cliente As String
    Private _nit_empresa As String
    Private _empresa As String

    Private _qrFactura As String
    Private _qrAutorizacion As String
    Private _qrFecha As String
    Private _qrMonto As String
    Private _qrNit As String


    Dim digVerHoeff As String
    Dim cadenaRc4 As String
    Dim st As Integer = 0
    Dim sp1 As Integer = 0
    Dim sp2 As Integer = 0
    Dim sp3 As Integer = 0
    Dim sp4 As Integer = 0
    Dim sp5 As Integer = 0
    Dim sumTotal As Integer = 0
    Dim cadBase64 As String
    Dim codigoControl As String

    Dim sumHoeff As String

    Public Property nro_autorizacion
        Get
            Return _nro_autorizacion
        End Get
        Set(value)
            _nro_autorizacion = value
            _qrAutorizacion = value
        End Set
    End Property
    Public Property nro_factura
        Get
            Return _nro_factura
        End Get
        Set(value)
            _nro_factura = value
            _qrFactura = value
        End Set
    End Property
    Public Property nit
        Get
            Return _nit
        End Get
        Set(value)
            _nit = value
            _qrNit = value
        End Set
    End Property
    Public Property fecha
        Get
            Return _fecha
        End Get
        Set(value)
            _fecha = value
        End Set
    End Property
    Public Property qrFecha
        Get
            Return _qrFecha
        End Get
        Set(value)
            _qrFecha = value
        End Set
    End Property
    Public Property qrMonto
        Get
            Return _qrMonto
        End Get
        Set(value)
            _qrMonto = value
        End Set
    End Property

    Public Property monto
        Get
            Return _monto
        End Get
        Set(value)
            _monto = value

        End Set
    End Property
    Public Property llave
        Get
            Return _llave
        End Get
        Set(value)
            _llave = value
        End Set
    End Property
    Public Property fecha_lim_emision
        Get
            Return _fecha_lim_emision
        End Get
        Set(value)
            _fecha_lim_emision = value
        End Set
    End Property
    Public Property cliente
        Get
            Return _cliente
        End Get
        Set(value)
            _cliente = value
        End Set
    End Property
    Public Property nit_empresa
        Get
            Return _nit_empresa
        End Get
        Set(value)
            _nit_empresa = value
        End Set
    End Property
    Public Property empresa
        Get
            Return _empresa
        End Get
        Set(value)
            _empresa = value
        End Set
    End Property

    Public Sub New()
        st = 0
        sp1 = 0
        sp2 = 0
        sp3 = 0
        sp4 = 0
        sp5 = 0
        sumTotal = 0
    End Sub
    Private Sub paso1()
        Dim hoeff = New Verhoeff()
        For i As Integer = 1 To 2 Step 1
            _nro_factura = _nro_factura + hoeff.obtenerVerhoeff(_nro_factura)
            _nit = _nit + hoeff.obtenerVerhoeff(_nit)
            _fecha = _fecha + hoeff.obtenerVerhoeff(_fecha)
            _monto = _monto + hoeff.obtenerVerhoeff(_monto)
        Next
        Dim aux As Long
        aux = aux + Convert.ToInt64(_nro_factura)
        aux = aux + Convert.ToInt64(_nit)
        aux = aux + Convert.ToInt64(_fecha)
        aux = aux + Convert.ToInt64(_monto)
        sumHoeff = Convert.ToString(aux)
        digVerHoeff = sumHoeff
        For i As Integer = 1 To 5 Step 1
            digVerHoeff = digVerHoeff + hoeff.obtenerVerhoeff(digVerHoeff)
        Next
        digVerHoeff = Strings.Right(digVerHoeff, 5)
    End Sub
    Private Sub paso2()
        Dim cad1, cad2, cad3, cad4, cad5 As String
        Dim star As Integer = 1
        Dim dig = digVerHoeff.Substring(0, 1)
        cad1 = Mid(_llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1
        dig = digVerHoeff.Substring(1, 1)
        cad2 = Mid(_llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1
        dig = digVerHoeff.Substring(2, 1)
        cad3 = Mid(_llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1
        dig = digVerHoeff.Substring(3, 1)
        cad4 = Mid(_llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1
        dig = digVerHoeff.Substring(4, 1)
        cad5 = Mid(_llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1
        _nro_autorizacion = _nro_autorizacion + cad1
        _nro_factura = _nro_factura + cad2
        _nit = _nit + cad3
        _fecha = _fecha + cad4
        _monto = _monto + cad5
    End Sub
    Private Sub paso3()
        Dim cadConcatenada, llaveCifrado As String
        cadConcatenada = _nro_autorizacion + _nro_factura + _nit + _fecha + _monto
        llaveCifrado = _llave + digVerHoeff
        cadenaRc4 = New Alleged_RC4().RC4(cadConcatenada, llaveCifrado, False)        
    End Sub
    Private Sub paso4()
        Dim dig As Char
        Dim sw As Integer = 1
        For i = 1 To cadenaRc4.Length
            dig = Strings.GetChar(cadenaRc4, i)
            st += Asc(dig)
            Select Case sw
                Case 1
                    sp1 += Asc(dig)
                    sw = 2
                Case 2
                    sp2 += Asc(dig)
                    sw = 3
                Case 3
                    sp3 += Asc(dig)
                    sw = 4
                Case 4
                    sp4 += Asc(dig)
                    sw = 5
                Case 5
                    sp5 += Asc(dig)
                    sw = 1
            End Select
        Next        
    End Sub
    Private Sub paso5()
        Dim op1 As Integer = 0
        Dim op2 As Integer = 0
        Dim op3 As Integer = 0
        Dim op4 As Integer = 0
        Dim op5 As Integer = 0
        Dim dig As String
        dig = digVerHoeff.Substring(0, 1)
        op1 = Math.Truncate(st * sp1 / (CInt(dig) + 1))
        sumTotal += op1

        dig = digVerHoeff.Substring(1, 1)
        op2 = Math.Truncate(st * sp2 / (CInt(dig) + 1))
        sumTotal += op2

        dig = digVerHoeff.Substring(2, 1)
        op3 = Math.Truncate(st * sp3 / (CInt(dig) + 1))
        sumTotal += op3

        dig = digVerHoeff.Substring(3, 1)
        op4 = Math.Truncate(st * sp4 / (CInt(dig) + 1))
        sumTotal += op4

        dig = digVerHoeff.Substring(4, 1)
        op5 = Math.Truncate(st * sp5 / (CInt(dig) + 1))
        sumTotal += op5
        cadBase64 = New BASE64().BASE64(sumTotal)
    End Sub

    Private Sub paso6()
        Dim llaveCifrado As String
        llaveCifrado = _llave + digVerHoeff
        codigoControl = New Alleged_RC4().RC4(cadBase64, llaveCifrado, True)
    End Sub
    Public Function datosCodigoQr() As String
        Dim texto As String = ""
        texto += _nit_empresa + "|"
        'texto += _empresa + "|"
        texto += _qrFactura + "|"
        texto += _qrAutorizacion + "|"
        texto += _qrFecha + "|"
        texto += _qrMonto + "|"
        texto += _qrMonto + "|"
        texto += codigoControl + "|"
        texto += _qrNit + "|"
        texto += "0" + "|"
        texto += "0" + "|"
        texto += "0" + "|"
        texto += "0"
        'texto += _fecha_lim_emision + "|"
        'texto += "0" + "|"
        'texto += _cliente
        Return texto
    End Function
    Public Function getCodigoControl() As String
        paso1()
        paso2()
        paso3()
        paso4()
        paso5()
        paso6()
        Return codigoControl
    End Function
    
End Class
