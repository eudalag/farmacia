﻿Public Class Verhoeff
    Private Function swap(ByRef Numero As String) As String
        Dim i As Short
        Dim aux As String = ""

        For i = 1 To Len(Numero)
            aux = aux & Mid(Numero, Len(Numero) - i + 1, 1)
        Next i
        swap = aux
    End Function
    Public Function obtenerVerhoeff(ByRef Numero As String) As String
        Dim Mul As Object '(10, 10) As Integer
        Dim Per As Object '(8, 10) As Integer
        Dim Inv As Object '(10) As Integer
        'UPGRADE_WARNING: Array tiene un nuevo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Inv. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        Inv = New Object() {0, 4, 3, 2, 1, 5, 6, 7, 8, 9}
        Dim NumeroInvertido As String
        Dim Check As Short
        Dim i As Short
        Dim aux02, aux01, aux03 As Object
        'UPGRADE_WARNING: Array tiene un nuevo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Mul. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        Mul = New Object() {New Object() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, New Object() {1, 2, 3, 4, 0, 6, 7, 8, 9, 5}, New Object() {2, 3, 4, 0, 1, 7, 8, 9, 5, 6}, New Object() {3, 4, 0, 1, 2, 8, 9, 5, 6, 7}, New Object() {4, 0, 1, 2, 3, 9, 5, 6, 7, 8}, New Object() {5, 9, 8, 7, 6, 0, 4, 3, 2, 1}, New Object() {6, 5, 9, 8, 7, 1, 0, 4, 3, 2}, New Object() {7, 6, 5, 9, 8, 2, 1, 0, 4, 3}, New Object() {8, 7, 6, 5, 9, 3, 2, 1, 0, 4}, New Object() {9, 8, 7, 6, 5, 4, 3, 2, 1, 0}}
        'UPGRADE_WARNING: Array tiene un nuevo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Per. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        Per = New Object() {New Object() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, New Object() {1, 5, 7, 6, 2, 8, 3, 0, 9, 4}, New Object() {5, 8, 0, 3, 7, 9, 6, 1, 4, 2}, New Object() {8, 9, 1, 6, 0, 4, 3, 5, 2, 7}, New Object() {9, 4, 5, 3, 1, 2, 6, 8, 7, 0}, New Object() {4, 2, 8, 6, 5, 7, 3, 9, 0, 1}, New Object() {2, 7, 9, 3, 8, 0, 6, 4, 1, 5}, New Object() {7, 0, 4, 6, 9, 1, 3, 2, 5, 8}}

        Check = 0
        NumeroInvertido = swap(Numero)
        For i = 0 To Len(NumeroInvertido) - 1
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto aux01. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            aux01 = ((i + 1) Mod 8)
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto aux02. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            aux02 = Mid(NumeroInvertido, i + 1, 1)
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto aux02. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Per()(). Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto aux03. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            aux03 = Per(aux01)(CInt(aux02))
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Mul()(). Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            Check = Mul(Check)(aux03) 'Per(((i + 1) Mod 8), Mid(NumeroInvertido, i, 1)))
        Next
        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Inv(). Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        obtenerVerhoeff = Inv(Check)
    End Function
End Class
