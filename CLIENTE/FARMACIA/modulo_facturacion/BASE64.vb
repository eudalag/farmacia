﻿Public Class BASE64
    Public STOPNOW As Boolean
    Private Function TrimZeros(ByVal num As String) As String
        Dim a As Integer
        For a = 1 To Len(num)
            If CDbl(Mid(num, a, 1)) <> 0 Then GoTo YuckFu
        Next a
        TrimZeros = "0"
        Exit Function
YuckFu:
        TrimZeros = Mid(num, a, Len(num) - a + 1)
    End Function
    Private Function IntAddition(ByVal FirstNum As String, ByVal SecondNum As String) As String
        Dim a, DifLen As Integer
        Dim TempStr As String = ""
        Dim TempNum As Short
        Dim Num1, Num2 As String
        Dim TempNum1, TempNum2 As Short
        Dim CarryOver As Short
        Dim LeftOvers As Integer
        'Setup the numbers so that they are easier to handle.
        'I originally had about 10 nested if statements that this block
        'of code simplifies Dramatically.
        If Len(FirstNum) >= Len(SecondNum) Then
            Num1 = FirstNum
            Num2 = SecondNum
        Else
            Num2 = FirstNum
            Num1 = SecondNum
        End If

        'Just setup some of the variables that need an initial value
        DifLen = Len(Num1) - Len(Num2)
        CarryOver = 0
        LeftOvers = DifLen

        'Ok, now for the real math.  Looping from the end of the numbers
        'just like our preschool teachers taught us, we add numbers that
        'line up in the 'places' (I.E. ones, tens, hundreds, thousands, etc)
        For a = Len(Num2) To 1 Step -1
            TempNum1 = Int(CDbl(Mid(Num1, a + DifLen, 1)))
            TempNum2 = Int(CDbl(Mid(Num2, a, 1)))
            TempNum = TempNum1 + TempNum2 + CarryOver
            CarryOver = TempNum \ 10
            TempStr = (TempNum - (CarryOver * 10)) & TempStr
            System.Windows.Forms.Application.DoEvents()
            If STOPNOW = True Then GoTo StopAdd
        Next a

        'What do we do if there is a 1 or a 2 that carries over outside the
        'numbers that line up in the places, well, we do the following block of
        'code.  The do loop is used incase we get a situation like this:
        '
        '    199999  When you add 1 to a set of nines it continues to
        '    _+___1  Carry over until it hits the first digit
        '    200000
        Do Until CarryOver = 0 Or LeftOvers = 0
            TempNum = Int(CDbl(Mid(Num1, LeftOvers, 1))) + CarryOver
            CarryOver = TempNum \ 10
            TempStr = (TempNum - (CarryOver * 10)) & TempStr
            LeftOvers = LeftOvers - 1
        Loop

        'Since there are two possible ways of exiting the Loop above, we need
        'to test and apply the other variable and its associated values in the following
        'two if statements.
        'Handle a possible carryover that will drop off the front end creating a new place.
        If CarryOver > 0 Then TempStr = CarryOver & TempStr
        'add any of the numbers that are remaining on the left side of the longer string
        If LeftOvers > 0 Then TempStr = (Microsoft.VisualBasic.Left(Num1, LeftOvers)) & TempStr
        'and return the value
StopAdd:
        IntAddition = TrimZeros(TempStr)
    End Function
    Private Function IntSubtract(ByVal FirstNum As String, ByVal SecondNum As String) As String
        '***
        'DO NOT change the integers to bytes, there are negative values in this function
        '***
        Dim Num1, Num2 As String
        Dim a, DifLen As Integer
        Dim Neg As Boolean
        Dim TempStr As String = ""
        Dim TempNum1, TempNum2 As Short
        Dim TempNum As Short
        Dim Barrow As Byte
        'This function operates on a theory known as Two-Compliment.
        'If you want to know more, look for it at www.mathforum.com
        'This function works great now
        'This block of code arranges the numbers into the Num1 and Num2 based on
        'which number is larger.  This prevents a great number of errors if the numbers
        'dont line up, or if the larger number is taken from the smaller number.
        If Len(FirstNum) > Len(SecondNum) Then
            Num1 = FirstNum
            Num2 = SecondNum
            Neg = False
        ElseIf Len(FirstNum) < Len(SecondNum) Then
            Num1 = SecondNum
            Num2 = FirstNum
            Neg = True
        Else

            'In the case that the strings are of equal length we have this pretty little
            'set of code to find which number has the first larger digit.
            For a = 1 To Len(FirstNum)
                If Int(CDbl(Mid(FirstNum, a, 1))) > Int(CDbl(Mid(SecondNum, a, 1))) Then
                    Num1 = FirstNum
                    Num2 = SecondNum
                    Neg = False
                    GoTo ContinSubtraction

                ElseIf Int(CDbl(Mid(FirstNum, a, 1))) < Int(CDbl(Mid(SecondNum, a, 1))) Then
                    Num1 = SecondNum
                    Num2 = FirstNum
                    Neg = True
                    GoTo ContinSubtraction
                End If

                System.Windows.Forms.Application.DoEvents()

                'sentinel
                If STOPNOW = True Then GoTo ExitFunction
            Next a

            'In the case that no larger digit is found, then guess what, its a perfect
            'subtraction, so we don't need to do the function, just assign a 0 outside the end.
            GoTo ExitFunction
        End If

ContinSubtraction:
        'If we have a difference in length then ajust with 0's that will not affect the calculations.
        'This allows us to get all the digits into the final out number.
        DifLen = Len(Num1) - Len(Num2)
        Num2 = New String("0", DifLen) & Num2
        Barrow = 0
        'lets do some math
        For a = Len(Num2) To 1 Step -1

            'Pick out the individual digit from each number
            TempNum1 = Int(CDbl(Mid(Num1, a, 1))) - Barrow
            TempNum2 = Int(CDbl(Mid(Num2, a, 1)))
            Barrow = 0

            'Perform single digit subraction using the Two Compliment theory
            If TempNum1 >= TempNum2 Then
                TempNum = TempNum1 - TempNum2

            ElseIf TempNum1 < TempNum2 Then
                TempNum = (TempNum1 + 10) - TempNum2
                Barrow = 1
            End If

            'Assign new digit to the final string.
            TempStr = CStr(TempNum) & TempStr

            System.Windows.Forms.Application.DoEvents()

            'sentinel
            If STOPNOW = True Then GoTo ExitFunction
        Next a
        'now, since we are subtracting, we need to determine if the number being returned is a negative.
        'Just to note, the Trim is to remove unneccsary zero's at the head(left) of the return number.
        If Neg = True Then
            IntSubtract = "-" & TrimZeros(Trim(TempStr))
        Else
            IntSubtract = TrimZeros(Trim(TempStr))
        End If
        Exit Function
ExitFunction:
        IntSubtract = CStr(0)
    End Function
    Public Function IntDivide(ByVal FirstNum As String, ByVal SecondNum As String, ByRef Resto As String) As String
        'Before we even alocate memory for variables, test for some very important error values
        If Len(FirstNum) < Len(SecondNum) Or InStr(1, IntSubtract(FirstNum, SecondNum), "-") > 0 Then
            'MsgBox "Fault: Extra Long Division does not support dividing a shorter number by a longer number, as this requires decimals which are not currently handled."
            Resto = FirstNum
            IntDivide = "0"
            Exit Function

        ElseIf TrimZeros(SecondNum) = "" Then
            MsgBox("Fault: Cannot divide by Zero.")

        ElseIf TrimZeros(SecondNum) = "" Then
            GoTo EndFunc

        Else
            GoTo continDivide
        End If
        GoTo ExitDivide
        'After passing the error checking, lets get started with some division
continDivide:
        Dim DivTotal, Num1, DivMult As String
        Dim DifLen As Integer
        Dim DivSub, TempNum As String
        'Initiallize values
        Num1 = FirstNum
        DivTotal = "0"
        DifLen = (Len(Num1) - Len(SecondNum))
        DivMult = New String("0", DifLen)
        DivSub = SecondNum & DivMult

        'Lets do some division
        Do Until (Len(Num1) < Len(SecondNum) Or Num1 = "0" Or (InStr(1, IntSubtract(Num1, SecondNum), "-") > 0 And DivMult = "")) Or STOPNOW = True

            'The way this division works is it subtracts values from the divided number
            'until no more can be subtracted.  This sets up a the largest possible number
            'that can be subtracted from the number so that you remove larger chucks of
            'Numbers at a time and waste less CPU Cycles doing it.
            If DifLen >= 0 Then DivMult = New String("0", DifLen)
            DivSub = SecondNum & DivMult

            If InStr(1, IntSubtract(Num1, DivSub), "-") > 0 Then
                If DifLen > 0 Then
                    DivMult = New String("0", DifLen - 1)
                    DivSub = SecondNum & DivMult
                Else
                    Exit Do
                End If
            End If

            'Perform the accually math.  DivTotal adds up how many times the original
            'number has been subtracted from the divided number. Num1 is the working
            'number.
            DivTotal = IntAddition(DivTotal, "1" & DivMult)
            Num1 = IntSubtract(Num1, DivSub)
            DifLen = Len(Num1) - Len(SecondNum)

            System.Windows.Forms.Application.DoEvents()

            'sentinel
            If STOPNOW = True Then GoTo ExitDivide
        Loop

        'Since there are no decimals, we return return the devide results with a remainder.
        IntDivide = DivTotal '& "r" & Num1
        Resto = Num1

EndFunc:
        Exit Function
ExitDivide:
        IntDivide = "NaN"
    End Function
    Public Function BASE64(ByVal sNum As String) As String
        Dim Dic As Object        
        Dim Resto As Short
        Dim aux As String = ""
        Dim aux2 As String = ""
        Dim num As Short
        Dim sResto As String = ""
        Dim sResult As String
        Dim i As Short
        'UPGRADE_WARNING: Array tiene un nuevo comportamiento. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
        'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Dic. Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        Dic = New Object() {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "+", "/"}

        Do While sNum <> "0" 'Len(sNum) > 4
            sResult = IntDivide(sNum, "64", sResto)
            Resto = CShort(sResto) 'IntSubtract(sNum, IntMultiply(result, "64"))
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Dic(). Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            aux = aux & Dic(Resto)
            sNum = sResult
        Loop
        num = CShort(sNum)
        Do While num > 0
            Resto = num Mod 64
            'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Dic(). Haga clic aquí para obtener más información: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            aux = aux & Dic(Resto)
            num = num / 64
        Loop
        For i = 1 To Len(aux)
            aux2 = aux2 & Mid(aux, Len(aux) - i + 1, 1)
        Next i
        BASE64 = aux2
    End Function
End Class
