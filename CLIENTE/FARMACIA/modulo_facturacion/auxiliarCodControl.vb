﻿Public Class auxiliarCodControl
    Dim nro_autorizacion As String
    Dim nro_factura As String
    Dim nit As String
    Dim fecha As String
    Dim monto As String
    Dim llave As String

    Dim digVerHoeff As String
    Dim cadenaRc4 As String
    Dim st As Integer = 0
    Dim sp1 As Integer = 0
    Dim sp2 As Integer = 0
    Dim sp3 As Integer = 0
    Dim sp4 As Integer = 0
    Dim sp5 As Integer = 0
    Dim sumTotal As Integer = 0
    Dim cadBase64 As String
    Dim codigoControl As String

    Dim sumHoeff As String


    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        st = 0
        sp1 = 0
        sp2 = 0
        sp3 = 0
        sp4 = 0
        sp5 = 0
        sumTotal = 0
        nro_autorizacion = Trim(txtNoAutorizacion.Text)
        nit = Trim(txtNIT.Text)
        fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
        monto = Math.Round(CDec(Trim(txtMonto.Text)), 0)
        llave = Trim(txtLlave.Text)
        nro_factura = Trim(txtNoFactura.Text)

        Dim gen_cod_control = New CODIGO_CONTROL()
        gen_cod_control.nro_autorizacion = Trim(txtNoAutorizacion.Text)
        gen_cod_control.nit = Trim(txtNIT.Text)
        gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
        gen_cod_control.llave = Trim(txtLlave.Text)
        gen_cod_control.nro_factura = Trim(txtNoFactura.Text)
        gen_cod_control.monto = Math.Round(CDec(Trim(txtMonto.Text)), 0)

        txtCodigoControl.Text = gen_cod_control.getCodigoControl()

        paso1()
        paso2()
        paso3()
        paso4()
        paso5()
        'paso6()
        generarCodigoQr()
    End Sub
    Private Sub paso1()
        Dim hoeff = New Verhoeff()        
        For i As Integer = 1 To 2 Step 1
            nro_factura = nro_factura + hoeff.obtenerVerhoeff(nro_factura)
            nit = nit + hoeff.obtenerVerhoeff(nit)
            fecha = fecha + hoeff.obtenerVerhoeff(fecha)
            monto = monto + hoeff.obtenerVerhoeff(monto)
        Next
        Dim aux As Long
        aux = aux + Convert.ToInt64(nro_factura)
        aux = aux + Convert.ToInt64(nit)
        aux = aux + Convert.ToInt64(fecha)
        aux = aux + Convert.ToInt64(monto)
        sumHoeff = Convert.ToString(aux)
        digVerHoeff = sumHoeff
        For i As Integer = 1 To 5 Step 1
            digVerHoeff = digVerHoeff + hoeff.obtenerVerhoeff(digVerHoeff)
        Next
        digVerHoeff = Strings.Right(digVerHoeff, 5)

        txtFact1.Text = nro_factura
        txtNit1.Text = nit
        txtFecha1.Text = fecha
        txtMonto1.Text = monto

        txtSuma.Text = sumHoeff
        txt5dig.Text = digVerHoeff

        txt5Verhoeff.Text = digVerHoeff
    End Sub
    Private Sub paso2()
        Dim cad1, cad2, cad3, cad4, cad5 As String
        Dim star As Integer = 1

        Dim dig = digVerHoeff.Substring(0, 1)
        cad1 = Mid(llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1

        dig = digVerHoeff.Substring(1, 1)
        cad2 = Mid(llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1

        dig = digVerHoeff.Substring(2, 1)
        cad3 = Mid(llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1

        dig = digVerHoeff.Substring(3, 1)
        cad4 = Mid(llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1

        dig = digVerHoeff.Substring(4, 1)
        cad5 = Mid(llave, star, CInt(dig) + 1)
        star = star + CInt(dig) + 1


        txtCad1.Text = cad1
        txtCad2.Text = cad2
        txtCad3.Text = cad3
        txtCad4.Text = cad4
        txtCad5.Text = cad5

        nro_autorizacion = nro_autorizacion + cad1
        nro_factura = nro_factura + cad2
        nit = nit + cad3
        fecha = fecha + cad4
        monto = monto + cad5
    End Sub
    Private Sub paso3()
        Dim cadConcatenada, llaveCifrado As String
        cadConcatenada = nro_autorizacion + nro_factura + nit + fecha + monto
        llaveCifrado = llave + digVerHoeff
        cadenaRc4 = New Alleged_RC4().RC4(cadConcatenada, llaveCifrado, False)
        txtCadConcat.Text = cadConcatenada
        txtLlaveCifra.Text = llaveCifrado
        txtCadena.Text = cadenaRc4
    End Sub
    Private Sub paso4()
        Dim dig As Char        
        Dim sw As Integer = 1
        For i = 1 To cadenaRc4.Length
            dig = Strings.GetChar(cadenaRc4, i)
            st += Asc(dig)
            Select Case sw
                Case 1
                    sp1 += Asc(dig)
                    sw = 2
                Case 2
                    sp2 += Asc(dig)
                    sw = 3
                Case 3
                    sp3 += Asc(dig)
                    sw = 4
                Case 4
                    sp4 += Asc(dig)
                    sw = 5
                Case 5
                    sp5 += Asc(dig)
                    sw = 1
            End Select
        Next
        txtSumParc1.Text = sp1
        txtSumParc2.Text = sp2
        txtSumParc3.Text = sp3
        txtSumParc4.Text = sp4
        txtSumParc5.Text = sp5
        txtSumTotal.Text = st
    End Sub
    Private Sub paso5()
        Dim op1 As Integer = 0
        Dim op2 As Integer = 0
        Dim op3 As Integer = 0
        Dim op4 As Integer = 0
        Dim op5 As Integer = 0


        Dim dig As String

        dig = digVerHoeff.Substring(0, 1)
        op1 = Math.Truncate(st * sp1 / (CInt(dig) + 1))
        sumTotal += op1

        dig = digVerHoeff.Substring(1, 1)
        op2 = Math.Truncate(st * sp2 / (CInt(dig) + 1))
        sumTotal += op2

        dig = digVerHoeff.Substring(2, 1)
        op3 = Math.Truncate(st * sp3 / (CInt(dig) + 1))
        sumTotal += op3

        dig = digVerHoeff.Substring(3, 1)
        op4 = Math.Truncate(st * sp4 / (CInt(dig) + 1))
        sumTotal += op4

        dig = digVerHoeff.Substring(4, 1)
        op5 = Math.Truncate(st * sp5 / (CInt(dig) + 1))
        sumTotal += op5

        txtProd1.Text = op1
        txtProd2.Text = op2
        txtProd3.Text = op3
        txtProd4.Text = op4
        txtProd5.Text = op5
        txtProdTotal.Text = sumTotal

        txtSumatoriaProducto.Text = sumTotal

        cadBase64 = New BASE64().BASE64(sumTotal)

        txtBase64.Text = cadBase64
    End Sub
    
    Private Sub paso6()
        Dim llaveCifrado As String
        llaveCifrado = llave + digVerHoeff
        codigoControl = New Alleged_RC4().RC4(cadBase64, llaveCifrado, True)

        txtCodigoControl.Text = codigoControl
    End Sub
    Private Sub generarCodigoQr()
        Dim texto As String = ""
        texto += "2993027018" + "|"
        texto += "ALTA FARM" + "|"
        texto += txtNoFactura.Text + "|"
        texto += txtNoAutorizacion.Text + "|"
        texto += txtFecha.Text + "|"
        texto += txtMonto.Text + "|"
        texto += codigoControl + "|"
        texto += txtFechaLimiteEmision.Text + "|"
        texto += "0" + "|"
        texto += "0" + "|"
        texto += txtNIT.Text + "|"
        texto += txtCliente.Text
        cimgQR.Text = texto
    End Sub

    Private Sub auxiliarCodControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtFecha.Text = "02/07/2007"
    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True            
        End If
    End Sub

    Private Sub txtMonto_Click(sender As Object, e As EventArgs) Handles txtMonto.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtMonto_Leave(sender As Object, e As EventArgs) Handles txtMonto.Leave
        Dim txt As TextBox = CType(sender, TextBox)
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        txtNoAutorizacion.Text = ""
        txtNoFactura.Text = ""
        txtNIT.Text = ""
        txtFecha.Text = ""
        txtFechaLimiteEmision.Text = ""
        txtLlave.Text = ""
        txtCliente.Text = ""
        txtCodigoControl.Text = ""
    End Sub
End Class