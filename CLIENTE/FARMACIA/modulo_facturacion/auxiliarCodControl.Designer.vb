﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class auxiliarCodControl
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(auxiliarCodControl))
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtFechaLimiteEmision = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtLlave = New System.Windows.Forms.TextBox()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.txtNIT = New System.Windows.Forms.TextBox()
        Me.txtNoFactura = New System.Windows.Forms.TextBox()
        Me.txtNoAutorizacion = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cimgQR = New Gma.QrCodeNet.Encoding.Windows.Forms.QrCodeImgControl()
        Me.txtCodigoControl = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtBase64 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtSumatoriaProducto = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCadena = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt5Verhoeff = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txt5dig = New System.Windows.Forms.TextBox()
        Me.txtSuma = New System.Windows.Forms.TextBox()
        Me.txtMonto1 = New System.Windows.Forms.TextBox()
        Me.txtFecha1 = New System.Windows.Forms.TextBox()
        Me.txtNit1 = New System.Windows.Forms.TextBox()
        Me.txtFact1 = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtCad5 = New System.Windows.Forms.TextBox()
        Me.txtCad4 = New System.Windows.Forms.TextBox()
        Me.txtCad3 = New System.Windows.Forms.TextBox()
        Me.txtCad2 = New System.Windows.Forms.TextBox()
        Me.txtCad1 = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtLlaveCifra = New System.Windows.Forms.TextBox()
        Me.txtCadConcat = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtSumParc5 = New System.Windows.Forms.TextBox()
        Me.txtSumParc4 = New System.Windows.Forms.TextBox()
        Me.txtSumParc3 = New System.Windows.Forms.TextBox()
        Me.txtSumParc2 = New System.Windows.Forms.TextBox()
        Me.txtSumParc1 = New System.Windows.Forms.TextBox()
        Me.txtSumTotal = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtProdTotal = New System.Windows.Forms.TextBox()
        Me.txtProd5 = New System.Windows.Forms.TextBox()
        Me.txtProd4 = New System.Windows.Forms.TextBox()
        Me.txtProd3 = New System.Windows.Forms.TextBox()
        Me.txtProd2 = New System.Windows.Forms.TextBox()
        Me.txtProd1 = New System.Windows.Forms.TextBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.cimgQR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(326, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(321, 26)
        Me.Label8.TabIndex = 67
        Me.Label8.Text = "Control de Codigo de Control"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtFechaLimiteEmision)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtFecha)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtLlave)
        Me.GroupBox1.Controls.Add(Me.txtMonto)
        Me.GroupBox1.Controls.Add(Me.txtNIT)
        Me.GroupBox1.Controls.Add(Me.txtNoFactura)
        Me.GroupBox1.Controls.Add(Me.txtNoAutorizacion)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(418, 221)
        Me.GroupBox1.TabIndex = 69
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Basicos"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(354, 201)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(58, 13)
        Me.Label22.TabIndex = 92
        Me.Label22.Text = "(Factura)"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(226, 179)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 13)
        Me.Label21.TabIndex = 91
        Me.Label21.Text = "(Factura)"
        '
        'txtCliente
        '
        Me.txtCliente.Location = New System.Drawing.Point(136, 198)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(215, 20)
        Me.txtCliente.TabIndex = 90
        Me.txtCliente.Text = "Jorge Calvimontes"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 201)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(39, 13)
        Me.Label20.TabIndex = 89
        Me.Label20.Text = "Cliente"
        '
        'txtFechaLimiteEmision
        '
        Me.txtFechaLimiteEmision.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaLimiteEmision.Location = New System.Drawing.Point(136, 175)
        Me.txtFechaLimiteEmision.Name = "txtFechaLimiteEmision"
        Me.txtFechaLimiteEmision.Size = New System.Drawing.Size(84, 20)
        Me.txtFechaLimiteEmision.TabIndex = 88
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(12, 176)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(121, 13)
        Me.Label19.TabIndex = 87
        Me.Label19.Text = "Fecha Limite de Emision"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(210, 127)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 86
        Me.Label18.Text = "(Factura)"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(210, 99)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 13)
        Me.Label17.TabIndex = 85
        Me.Label17.Text = "(Factura)"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(286, 74)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 13)
        Me.Label16.TabIndex = 84
        Me.Label16.Text = "(Factura)"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(286, 48)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 83
        Me.Label15.Text = "(Factura)"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(341, 152)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 13)
        Me.Label14.TabIndex = 82
        Me.Label14.Text = "(Dosificación)"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(286, 22)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(71, 13)
        Me.Label13.TabIndex = 81
        Me.Label13.Text = "(Dosificación)"
        '
        'txtFecha
        '
        Me.txtFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFecha.Location = New System.Drawing.Point(120, 97)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(84, 20)
        Me.txtFecha.TabIndex = 80
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(109, 13)
        Me.Label6.TabIndex = 79
        Me.Label6.Text = "Llave de Dosificacion"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 126)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 78
        Me.Label5.Text = "Monto Total"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 77
        Me.Label4.Text = "Fecha Transaccion"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 76
        Me.Label3.Text = "NIT / CI del Cliente"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 75
        Me.Label2.Text = "No Factura"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 74
        Me.Label1.Text = "No Autorizacion"
        '
        'txtLlave
        '
        Me.txtLlave.Location = New System.Drawing.Point(120, 149)
        Me.txtLlave.Name = "txtLlave"
        Me.txtLlave.Size = New System.Drawing.Size(215, 20)
        Me.txtLlave.TabIndex = 73
        Me.txtLlave.Text = "9rCB7Sv4X29d)5k7N%3ab89p-3(5[A "
        '
        'txtMonto
        '
        Me.txtMonto.Location = New System.Drawing.Point(120, 123)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(84, 20)
        Me.txtMonto.TabIndex = 72
        Me.txtMonto.Text = "2500"
        '
        'txtNIT
        '
        Me.txtNIT.Location = New System.Drawing.Point(120, 71)
        Me.txtNIT.Name = "txtNIT"
        Me.txtNIT.Size = New System.Drawing.Size(160, 20)
        Me.txtNIT.TabIndex = 71
        Me.txtNIT.Text = "4189179011"
        '
        'txtNoFactura
        '
        Me.txtNoFactura.Location = New System.Drawing.Point(120, 45)
        Me.txtNoFactura.Name = "txtNoFactura"
        Me.txtNoFactura.Size = New System.Drawing.Size(160, 20)
        Me.txtNoFactura.TabIndex = 70
        Me.txtNoFactura.Text = "1503"
        '
        'txtNoAutorizacion
        '
        Me.txtNoAutorizacion.Location = New System.Drawing.Point(120, 19)
        Me.txtNoAutorizacion.Name = "txtNoAutorizacion"
        Me.txtNoAutorizacion.Size = New System.Drawing.Size(160, 20)
        Me.txtNoAutorizacion.TabIndex = 69
        Me.txtNoAutorizacion.Text = "29040011007"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cimgQR)
        Me.GroupBox2.Controls.Add(Me.txtCodigoControl)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.txtBase64)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtSumatoriaProducto)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtCadena)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txt5Verhoeff)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(436, 38)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(473, 189)
        Me.GroupBox2.TabIndex = 70
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Codigo de Control"
        '
        'cimgQR
        '
        Me.cimgQR.ErrorCorrectLevel = Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.M
        Me.cimgQR.Image = CType(resources.GetObject("cimgQR.Image"), System.Drawing.Image)
        Me.cimgQR.Location = New System.Drawing.Point(335, 51)
        Me.cimgQR.Name = "cimgQR"
        Me.cimgQR.QuietZoneModule = Gma.QrCodeNet.Encoding.Windows.Render.QuietZoneModules.Two
        Me.cimgQR.Size = New System.Drawing.Size(132, 132)
        Me.cimgQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cimgQR.TabIndex = 83
        Me.cimgQR.TabStop = False
        Me.cimgQR.Text = "QrCodeImgControl1"
        '
        'txtCodigoControl
        '
        Me.txtCodigoControl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoControl.Location = New System.Drawing.Point(193, 145)
        Me.txtCodigoControl.Name = "txtCodigoControl"
        Me.txtCodigoControl.ReadOnly = True
        Me.txtCodigoControl.Size = New System.Drawing.Size(119, 20)
        Me.txtCodigoControl.TabIndex = 82
        Me.txtCodigoControl.Text = "00-00-00-00"
        Me.txtCodigoControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(190, 126)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(139, 13)
        Me.Label12.TabIndex = 81
        Me.Label12.Text = "CODIGO DE CONTROL"
        '
        'txtBase64
        '
        Me.txtBase64.Location = New System.Drawing.Point(94, 146)
        Me.txtBase64.Name = "txtBase64"
        Me.txtBase64.ReadOnly = True
        Me.txtBase64.Size = New System.Drawing.Size(73, 20)
        Me.txtBase64.TabIndex = 80
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(15, 149)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "BASE 64:"
        '
        'txtSumatoriaProducto
        '
        Me.txtSumatoriaProducto.Location = New System.Drawing.Point(94, 120)
        Me.txtSumatoriaProducto.Name = "txtSumatoriaProducto"
        Me.txtSumatoriaProducto.ReadOnly = True
        Me.txtSumatoriaProducto.Size = New System.Drawing.Size(73, 20)
        Me.txtSumatoriaProducto.TabIndex = 78
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(74, 13)
        Me.Label10.TabIndex = 77
        Me.Label10.Text = "SUMATORIA:"
        '
        'txtCadena
        '
        Me.txtCadena.Location = New System.Drawing.Point(94, 52)
        Me.txtCadena.Multiline = True
        Me.txtCadena.Name = "txtCadena"
        Me.txtCadena.ReadOnly = True
        Me.txtCadena.Size = New System.Drawing.Size(235, 59)
        Me.txtCadena.TabIndex = 76
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 75
        Me.Label9.Text = "CADENA:"
        '
        'txt5Verhoeff
        '
        Me.txt5Verhoeff.Location = New System.Drawing.Point(94, 26)
        Me.txt5Verhoeff.Name = "txt5Verhoeff"
        Me.txt5Verhoeff.ReadOnly = True
        Me.txt5Verhoeff.Size = New System.Drawing.Size(73, 20)
        Me.txt5Verhoeff.TabIndex = 74
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(76, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "5 VERHOEFF:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txt5dig)
        Me.GroupBox3.Controls.Add(Me.txtSuma)
        Me.GroupBox3.Controls.Add(Me.txtMonto1)
        Me.GroupBox3.Controls.Add(Me.txtFecha1)
        Me.GroupBox3.Controls.Add(Me.txtNit1)
        Me.GroupBox3.Controls.Add(Me.txtFact1)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 265)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(226, 183)
        Me.GroupBox3.TabIndex = 71
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Paso 1:"
        '
        'txt5dig
        '
        Me.txt5dig.Location = New System.Drawing.Point(168, 71)
        Me.txt5dig.Name = "txt5dig"
        Me.txt5dig.ReadOnly = True
        Me.txt5dig.Size = New System.Drawing.Size(52, 20)
        Me.txt5dig.TabIndex = 78
        '
        'txtSuma
        '
        Me.txtSuma.Location = New System.Drawing.Point(15, 132)
        Me.txtSuma.Name = "txtSuma"
        Me.txtSuma.ReadOnly = True
        Me.txtSuma.Size = New System.Drawing.Size(131, 20)
        Me.txtSuma.TabIndex = 77
        '
        'txtMonto1
        '
        Me.txtMonto1.Location = New System.Drawing.Point(15, 97)
        Me.txtMonto1.Name = "txtMonto1"
        Me.txtMonto1.ReadOnly = True
        Me.txtMonto1.Size = New System.Drawing.Size(131, 20)
        Me.txtMonto1.TabIndex = 76
        '
        'txtFecha1
        '
        Me.txtFecha1.Location = New System.Drawing.Point(15, 71)
        Me.txtFecha1.Name = "txtFecha1"
        Me.txtFecha1.ReadOnly = True
        Me.txtFecha1.Size = New System.Drawing.Size(131, 20)
        Me.txtFecha1.TabIndex = 75
        '
        'txtNit1
        '
        Me.txtNit1.Location = New System.Drawing.Point(15, 45)
        Me.txtNit1.Name = "txtNit1"
        Me.txtNit1.ReadOnly = True
        Me.txtNit1.Size = New System.Drawing.Size(131, 20)
        Me.txtNit1.TabIndex = 74
        '
        'txtFact1
        '
        Me.txtFact1.Location = New System.Drawing.Point(15, 19)
        Me.txtFact1.Name = "txtFact1"
        Me.txtFact1.ReadOnly = True
        Me.txtFact1.Size = New System.Drawing.Size(131, 20)
        Me.txtFact1.TabIndex = 73
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtCad5)
        Me.GroupBox4.Controls.Add(Me.txtCad4)
        Me.GroupBox4.Controls.Add(Me.txtCad3)
        Me.GroupBox4.Controls.Add(Me.txtCad2)
        Me.GroupBox4.Controls.Add(Me.txtCad1)
        Me.GroupBox4.Location = New System.Drawing.Point(244, 265)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(166, 183)
        Me.GroupBox4.TabIndex = 72
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Paso 2:"
        '
        'txtCad5
        '
        Me.txtCad5.Location = New System.Drawing.Point(20, 123)
        Me.txtCad5.Name = "txtCad5"
        Me.txtCad5.ReadOnly = True
        Me.txtCad5.Size = New System.Drawing.Size(131, 20)
        Me.txtCad5.TabIndex = 82
        '
        'txtCad4
        '
        Me.txtCad4.Location = New System.Drawing.Point(20, 97)
        Me.txtCad4.Name = "txtCad4"
        Me.txtCad4.ReadOnly = True
        Me.txtCad4.Size = New System.Drawing.Size(131, 20)
        Me.txtCad4.TabIndex = 81
        '
        'txtCad3
        '
        Me.txtCad3.Location = New System.Drawing.Point(20, 71)
        Me.txtCad3.Name = "txtCad3"
        Me.txtCad3.ReadOnly = True
        Me.txtCad3.Size = New System.Drawing.Size(131, 20)
        Me.txtCad3.TabIndex = 80
        '
        'txtCad2
        '
        Me.txtCad2.Location = New System.Drawing.Point(20, 45)
        Me.txtCad2.Name = "txtCad2"
        Me.txtCad2.ReadOnly = True
        Me.txtCad2.Size = New System.Drawing.Size(131, 20)
        Me.txtCad2.TabIndex = 79
        '
        'txtCad1
        '
        Me.txtCad1.Location = New System.Drawing.Point(20, 19)
        Me.txtCad1.Name = "txtCad1"
        Me.txtCad1.ReadOnly = True
        Me.txtCad1.Size = New System.Drawing.Size(131, 20)
        Me.txtCad1.TabIndex = 78
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtLlaveCifra)
        Me.GroupBox5.Controls.Add(Me.txtCadConcat)
        Me.GroupBox5.Location = New System.Drawing.Point(416, 260)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(200, 183)
        Me.GroupBox5.TabIndex = 73
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Paso 3:"
        '
        'txtLlaveCifra
        '
        Me.txtLlaveCifra.Location = New System.Drawing.Point(6, 105)
        Me.txtLlaveCifra.Multiline = True
        Me.txtLlaveCifra.Name = "txtLlaveCifra"
        Me.txtLlaveCifra.ReadOnly = True
        Me.txtLlaveCifra.Size = New System.Drawing.Size(188, 64)
        Me.txtLlaveCifra.TabIndex = 78
        '
        'txtCadConcat
        '
        Me.txtCadConcat.Location = New System.Drawing.Point(6, 27)
        Me.txtCadConcat.Multiline = True
        Me.txtCadConcat.Name = "txtCadConcat"
        Me.txtCadConcat.ReadOnly = True
        Me.txtCadConcat.Size = New System.Drawing.Size(188, 72)
        Me.txtCadConcat.TabIndex = 77
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtSumParc5)
        Me.GroupBox6.Controls.Add(Me.txtSumParc4)
        Me.GroupBox6.Controls.Add(Me.txtSumParc3)
        Me.GroupBox6.Controls.Add(Me.txtSumParc2)
        Me.GroupBox6.Controls.Add(Me.txtSumParc1)
        Me.GroupBox6.Controls.Add(Me.txtSumTotal)
        Me.GroupBox6.Location = New System.Drawing.Point(622, 233)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(143, 183)
        Me.GroupBox6.TabIndex = 74
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Paso 4:"
        '
        'txtSumParc5
        '
        Me.txtSumParc5.Location = New System.Drawing.Point(22, 149)
        Me.txtSumParc5.Name = "txtSumParc5"
        Me.txtSumParc5.ReadOnly = True
        Me.txtSumParc5.Size = New System.Drawing.Size(101, 20)
        Me.txtSumParc5.TabIndex = 88
        '
        'txtSumParc4
        '
        Me.txtSumParc4.Location = New System.Drawing.Point(22, 123)
        Me.txtSumParc4.Name = "txtSumParc4"
        Me.txtSumParc4.ReadOnly = True
        Me.txtSumParc4.Size = New System.Drawing.Size(101, 20)
        Me.txtSumParc4.TabIndex = 87
        '
        'txtSumParc3
        '
        Me.txtSumParc3.Location = New System.Drawing.Point(22, 97)
        Me.txtSumParc3.Name = "txtSumParc3"
        Me.txtSumParc3.ReadOnly = True
        Me.txtSumParc3.Size = New System.Drawing.Size(101, 20)
        Me.txtSumParc3.TabIndex = 86
        '
        'txtSumParc2
        '
        Me.txtSumParc2.Location = New System.Drawing.Point(22, 71)
        Me.txtSumParc2.Name = "txtSumParc2"
        Me.txtSumParc2.ReadOnly = True
        Me.txtSumParc2.Size = New System.Drawing.Size(101, 20)
        Me.txtSumParc2.TabIndex = 85
        '
        'txtSumParc1
        '
        Me.txtSumParc1.Location = New System.Drawing.Point(22, 45)
        Me.txtSumParc1.Name = "txtSumParc1"
        Me.txtSumParc1.ReadOnly = True
        Me.txtSumParc1.Size = New System.Drawing.Size(101, 20)
        Me.txtSumParc1.TabIndex = 84
        '
        'txtSumTotal
        '
        Me.txtSumTotal.Location = New System.Drawing.Point(22, 19)
        Me.txtSumTotal.Name = "txtSumTotal"
        Me.txtSumTotal.ReadOnly = True
        Me.txtSumTotal.Size = New System.Drawing.Size(101, 20)
        Me.txtSumTotal.TabIndex = 83
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtProdTotal)
        Me.GroupBox7.Controls.Add(Me.txtProd5)
        Me.GroupBox7.Controls.Add(Me.txtProd4)
        Me.GroupBox7.Controls.Add(Me.txtProd3)
        Me.GroupBox7.Controls.Add(Me.txtProd2)
        Me.GroupBox7.Controls.Add(Me.txtProd1)
        Me.GroupBox7.Location = New System.Drawing.Point(771, 233)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(162, 183)
        Me.GroupBox7.TabIndex = 75
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Paso 5:"
        '
        'txtProdTotal
        '
        Me.txtProdTotal.Location = New System.Drawing.Point(31, 156)
        Me.txtProdTotal.Name = "txtProdTotal"
        Me.txtProdTotal.ReadOnly = True
        Me.txtProdTotal.Size = New System.Drawing.Size(101, 20)
        Me.txtProdTotal.TabIndex = 94
        '
        'txtProd5
        '
        Me.txtProd5.Location = New System.Drawing.Point(31, 120)
        Me.txtProd5.Name = "txtProd5"
        Me.txtProd5.ReadOnly = True
        Me.txtProd5.Size = New System.Drawing.Size(101, 20)
        Me.txtProd5.TabIndex = 93
        '
        'txtProd4
        '
        Me.txtProd4.Location = New System.Drawing.Point(31, 94)
        Me.txtProd4.Name = "txtProd4"
        Me.txtProd4.ReadOnly = True
        Me.txtProd4.Size = New System.Drawing.Size(101, 20)
        Me.txtProd4.TabIndex = 92
        '
        'txtProd3
        '
        Me.txtProd3.Location = New System.Drawing.Point(31, 68)
        Me.txtProd3.Name = "txtProd3"
        Me.txtProd3.ReadOnly = True
        Me.txtProd3.Size = New System.Drawing.Size(101, 20)
        Me.txtProd3.TabIndex = 91
        '
        'txtProd2
        '
        Me.txtProd2.Location = New System.Drawing.Point(31, 42)
        Me.txtProd2.Name = "txtProd2"
        Me.txtProd2.ReadOnly = True
        Me.txtProd2.Size = New System.Drawing.Size(101, 20)
        Me.txtProd2.TabIndex = 90
        '
        'txtProd1
        '
        Me.txtProd1.Location = New System.Drawing.Point(31, 16)
        Me.txtProd1.Name = "txtProd1"
        Me.txtProd1.ReadOnly = True
        Me.txtProd1.Size = New System.Drawing.Size(101, 20)
        Me.txtProd1.TabIndex = 89
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.Location = New System.Drawing.Point(696, 422)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 32)
        Me.btnAceptar.TabIndex = 86
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Image = CType(resources.GetObject("btnVolver.Image"), System.Drawing.Image)
        Me.btnVolver.Location = New System.Drawing.Point(858, 422)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 32)
        Me.btnVolver.TabIndex = 87
        Me.btnVolver.Text = "&Salir"
        Me.btnVolver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(777, 422)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 32)
        Me.Button1.TabIndex = 88
        Me.Button1.Text = "&Nuevo"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = True
        '
        'auxiliarCodControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(945, 460)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "auxiliarCodControl"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Activacion Codigo de Control"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.cimgQR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLlave As System.Windows.Forms.TextBox
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents txtNIT As System.Windows.Forms.TextBox
    Friend WithEvents txtNoFactura As System.Windows.Forms.TextBox
    Friend WithEvents txtNoAutorizacion As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCodigoControl As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtBase64 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSumatoriaProducto As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtCadena As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt5Verhoeff As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txt5dig As System.Windows.Forms.TextBox
    Friend WithEvents txtSuma As System.Windows.Forms.TextBox
    Friend WithEvents txtMonto1 As System.Windows.Forms.TextBox
    Friend WithEvents txtFecha1 As System.Windows.Forms.TextBox
    Friend WithEvents txtNit1 As System.Windows.Forms.TextBox
    Friend WithEvents txtFact1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCad5 As System.Windows.Forms.TextBox
    Friend WithEvents txtCad4 As System.Windows.Forms.TextBox
    Friend WithEvents txtCad3 As System.Windows.Forms.TextBox
    Friend WithEvents txtCad2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCad1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCadConcat As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSumParc5 As System.Windows.Forms.TextBox
    Friend WithEvents txtSumParc4 As System.Windows.Forms.TextBox
    Friend WithEvents txtSumParc3 As System.Windows.Forms.TextBox
    Friend WithEvents txtSumParc2 As System.Windows.Forms.TextBox
    Friend WithEvents txtSumParc1 As System.Windows.Forms.TextBox
    Friend WithEvents txtSumTotal As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents txtProdTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtProd5 As System.Windows.Forms.TextBox
    Friend WithEvents txtProd4 As System.Windows.Forms.TextBox
    Friend WithEvents txtProd3 As System.Windows.Forms.TextBox
    Friend WithEvents txtProd2 As System.Windows.Forms.TextBox
    Friend WithEvents txtProd1 As System.Windows.Forms.TextBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtLlaveCifra As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cimgQR As Gma.QrCodeNet.Encoding.Windows.Forms.QrCodeImgControl
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtFechaLimiteEmision As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
