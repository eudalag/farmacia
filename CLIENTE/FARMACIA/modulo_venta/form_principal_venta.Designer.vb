﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_principal_venta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_principal_venta))
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.panelContenedor = New System.Windows.Forms.Panel()
        Me.btnMovimiento = New System.Windows.Forms.Button()
        Me.btnCierreCaja = New System.Windows.Forms.Button()
        Me.btnApertura = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCotizar = New System.Windows.Forms.Button()
        Me.btnPedido = New System.Windows.Forms.Button()
        Me.lblIdCaja = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.InformesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VencimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistenciaPorLaboratorioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioGeneralPorLaboratorioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientoMedicamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasPorLaboratorioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnCredito = New System.Windows.Forms.Button()
        Me.btnPaciente = New System.Windows.Forms.Button()
        Me.btnRecetas = New System.Windows.Forms.Button()
        Me.btnDevolucion = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuario.Location = New System.Drawing.Point(267, 849)
        Me.lblUsuario.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(74, 24)
        Me.lblUsuario.TabIndex = 38
        Me.lblUsuario.Text = "Usuario"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(181, 849)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 24)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Usuario:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'panelContenedor
        '
        Me.panelContenedor.BackColor = System.Drawing.SystemColors.Control
        Me.panelContenedor.Location = New System.Drawing.Point(189, 34)
        Me.panelContenedor.Margin = New System.Windows.Forms.Padding(4)
        Me.panelContenedor.Name = "panelContenedor"
        Me.panelContenedor.Size = New System.Drawing.Size(1500, 800)
        Me.panelContenedor.TabIndex = 35
        '
        'btnMovimiento
        '
        Me.btnMovimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMovimiento.Location = New System.Drawing.Point(16, 556)
        Me.btnMovimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.btnMovimiento.Name = "btnMovimiento"
        Me.btnMovimiento.Size = New System.Drawing.Size(160, 55)
        Me.btnMovimiento.TabIndex = 47
        Me.btnMovimiento.Text = "&Ajustes a Capital"
        Me.btnMovimiento.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnMovimiento.UseVisualStyleBackColor = True
        '
        'btnCierreCaja
        '
        Me.btnCierreCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCierreCaja.Location = New System.Drawing.Point(573, 833)
        Me.btnCierreCaja.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCierreCaja.Name = "btnCierreCaja"
        Me.btnCierreCaja.Size = New System.Drawing.Size(160, 55)
        Me.btnCierreCaja.TabIndex = 46
        Me.btnCierreCaja.Text = "&Cierre de Caja"
        Me.btnCierreCaja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnCierreCaja.UseVisualStyleBackColor = True
        Me.btnCierreCaja.Visible = False
        '
        'btnApertura
        '
        Me.btnApertura.Enabled = False
        Me.btnApertura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApertura.Location = New System.Drawing.Point(405, 833)
        Me.btnApertura.Margin = New System.Windows.Forms.Padding(4)
        Me.btnApertura.Name = "btnApertura"
        Me.btnApertura.Size = New System.Drawing.Size(160, 55)
        Me.btnApertura.TabIndex = 45
        Me.btnApertura.Text = "&Apertura de Caja"
        Me.btnApertura.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnApertura.UseVisualStyleBackColor = True
        Me.btnApertura.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(16, 779)
        Me.btnSalir.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(160, 55)
        Me.btnSalir.TabIndex = 43
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInforme.Location = New System.Drawing.Point(16, 681)
        Me.btnInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(160, 55)
        Me.btnInforme.TabIndex = 44
        Me.btnInforme.Text = "&Informe Caja"
        Me.btnInforme.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCotizar
        '
        Me.btnCotizar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCotizar.Location = New System.Drawing.Point(16, 99)
        Me.btnCotizar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCotizar.Name = "btnCotizar"
        Me.btnCotizar.Size = New System.Drawing.Size(160, 55)
        Me.btnCotizar.TabIndex = 42
        Me.btnCotizar.Text = "&Cotizaciones (F2)"
        Me.btnCotizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnCotizar.UseVisualStyleBackColor = True
        '
        'btnPedido
        '
        Me.btnPedido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPedido.Location = New System.Drawing.Point(16, 33)
        Me.btnPedido.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPedido.Name = "btnPedido"
        Me.btnPedido.Size = New System.Drawing.Size(160, 58)
        Me.btnPedido.TabIndex = 41
        Me.btnPedido.Text = "&Nueva Venta (F1)"
        Me.btnPedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnPedido.UseVisualStyleBackColor = True
        '
        'lblIdCaja
        '
        Me.lblIdCaja.AutoSize = True
        Me.lblIdCaja.Location = New System.Drawing.Point(349, 849)
        Me.lblIdCaja.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblIdCaja.Name = "lblIdCaja"
        Me.lblIdCaja.Size = New System.Drawing.Size(21, 17)
        Me.lblIdCaja.TabIndex = 48
        Me.lblIdCaja.Text = "-1"
        Me.lblIdCaja.Visible = False
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.ForeColor = System.Drawing.Color.Red
        Me.lblEstado.Location = New System.Drawing.Point(1520, 849)
        Me.lblEstado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(76, 20)
        Me.lblEstado.TabIndex = 99
        Me.lblEstado.Text = "Cerrado"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1389, 849)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(117, 20)
        Me.Label6.TabIndex = 98
        Me.Label6.Text = "Estado Caja:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InformesToolStripMenuItem, Me.ClientesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1284, 28)
        Me.MenuStrip1.TabIndex = 100
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'InformesToolStripMenuItem
        '
        Me.InformesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VencimientosToolStripMenuItem, Me.ExistenciaPorLaboratorioToolStripMenuItem, Me.InventarioGeneralPorLaboratorioToolStripMenuItem, Me.InventarioGeneralToolStripMenuItem, Me.MovimientoMedicamentoToolStripMenuItem, Me.VentasPorLaboratorioToolStripMenuItem})
        Me.InformesToolStripMenuItem.Name = "InformesToolStripMenuItem"
        Me.InformesToolStripMenuItem.Size = New System.Drawing.Size(79, 24)
        Me.InformesToolStripMenuItem.Text = "Informes"
        '
        'VencimientosToolStripMenuItem
        '
        Me.VencimientosToolStripMenuItem.Name = "VencimientosToolStripMenuItem"
        Me.VencimientosToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.VencimientosToolStripMenuItem.Text = "Vencimientos"
        '
        'ExistenciaPorLaboratorioToolStripMenuItem
        '
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Name = "ExistenciaPorLaboratorioToolStripMenuItem"
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Text = "Existencia por Laboratorio"
        '
        'InventarioGeneralPorLaboratorioToolStripMenuItem
        '
        Me.InventarioGeneralPorLaboratorioToolStripMenuItem.Name = "InventarioGeneralPorLaboratorioToolStripMenuItem"
        Me.InventarioGeneralPorLaboratorioToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.InventarioGeneralPorLaboratorioToolStripMenuItem.Text = "Inventario General por Laboratorio"
        '
        'InventarioGeneralToolStripMenuItem
        '
        Me.InventarioGeneralToolStripMenuItem.Enabled = False
        Me.InventarioGeneralToolStripMenuItem.Name = "InventarioGeneralToolStripMenuItem"
        Me.InventarioGeneralToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.InventarioGeneralToolStripMenuItem.Text = "Inventario General"
        '
        'MovimientoMedicamentoToolStripMenuItem
        '
        Me.MovimientoMedicamentoToolStripMenuItem.Enabled = False
        Me.MovimientoMedicamentoToolStripMenuItem.Name = "MovimientoMedicamentoToolStripMenuItem"
        Me.MovimientoMedicamentoToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.MovimientoMedicamentoToolStripMenuItem.Text = "Movimiento Medicamento"
        '
        'VentasPorLaboratorioToolStripMenuItem
        '
        Me.VentasPorLaboratorioToolStripMenuItem.Enabled = False
        Me.VentasPorLaboratorioToolStripMenuItem.Name = "VentasPorLaboratorioToolStripMenuItem"
        Me.VentasPorLaboratorioToolStripMenuItem.Size = New System.Drawing.Size(314, 26)
        Me.VentasPorLaboratorioToolStripMenuItem.Text = "Ventas por Laboratorio"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'btnCredito
        '
        Me.btnCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCredito.Location = New System.Drawing.Point(16, 420)
        Me.btnCredito.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCredito.Name = "btnCredito"
        Me.btnCredito.Size = New System.Drawing.Size(160, 55)
        Me.btnCredito.TabIndex = 101
        Me.btnCredito.Text = "Cta. por Cobrar"
        Me.btnCredito.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnCredito.UseVisualStyleBackColor = True
        '
        'btnPaciente
        '
        Me.btnPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPaciente.Location = New System.Drawing.Point(16, 294)
        Me.btnPaciente.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPaciente.Name = "btnPaciente"
        Me.btnPaciente.Size = New System.Drawing.Size(160, 55)
        Me.btnPaciente.TabIndex = 102
        Me.btnPaciente.Text = "&Recetas Consultorio"
        Me.btnPaciente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnPaciente.UseVisualStyleBackColor = True
        '
        'btnRecetas
        '
        Me.btnRecetas.Enabled = False
        Me.btnRecetas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecetas.Location = New System.Drawing.Point(16, 357)
        Me.btnRecetas.Margin = New System.Windows.Forms.Padding(4)
        Me.btnRecetas.Name = "btnRecetas"
        Me.btnRecetas.Size = New System.Drawing.Size(160, 55)
        Me.btnRecetas.TabIndex = 103
        Me.btnRecetas.Text = "Recetas Controladas"
        Me.btnRecetas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRecetas.UseVisualStyleBackColor = True
        '
        'btnDevolucion
        '
        Me.btnDevolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDevolucion.Location = New System.Drawing.Point(741, 833)
        Me.btnDevolucion.Margin = New System.Windows.Forms.Padding(4)
        Me.btnDevolucion.Name = "btnDevolucion"
        Me.btnDevolucion.Size = New System.Drawing.Size(160, 55)
        Me.btnDevolucion.TabIndex = 103
        Me.btnDevolucion.Text = "Devoluciones"
        Me.btnDevolucion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnDevolucion.UseVisualStyleBackColor = True
        Me.btnDevolucion.Visible = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(16, 619)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(160, 55)
        Me.Button1.TabIndex = 104
        Me.Button1.Text = "&Transferencia Entre Cajas"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(16, 483)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(160, 55)
        Me.Button2.TabIndex = 105
        Me.Button2.Text = "Seguimiento a Clientes"
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(16, 162)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(160, 58)
        Me.Button3.TabIndex = 106
        Me.Button3.Text = "Consultorio (F3)"
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(16, 228)
        Me.Button4.Margin = New System.Windows.Forms.Padding(4)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(160, 58)
        Me.Button4.TabIndex = 107
        Me.Button4.Text = "Facturacion (F4)"
        Me.Button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button4.UseVisualStyleBackColor = True
        '
        'form_principal_venta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1284, 892)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnDevolucion)
        Me.Controls.Add(Me.btnApertura)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnPaciente)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblIdCaja)
        Me.Controls.Add(Me.btnRecetas)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCierreCaja)
        Me.Controls.Add(Me.btnCredito)
        Me.Controls.Add(Me.btnCotizar)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnPedido)
        Me.Controls.Add(Me.btnMovimiento)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.panelContenedor)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "form_principal_venta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sistema de Farmacia"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents panelContenedor As System.Windows.Forms.Panel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCotizar As System.Windows.Forms.Button
    Friend WithEvents btnPedido As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents btnApertura As System.Windows.Forms.Button
    Friend WithEvents btnCierreCaja As System.Windows.Forms.Button
    Friend WithEvents btnMovimiento As System.Windows.Forms.Button
    Friend WithEvents lblIdCaja As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents InformesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCredito As System.Windows.Forms.Button
    Friend WithEvents btnPaciente As System.Windows.Forms.Button
    Friend WithEvents btnRecetas As System.Windows.Forms.Button
    Friend WithEvents btnDevolucion As System.Windows.Forms.Button
    Friend WithEvents MovimientoMedicamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasPorLaboratorioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioGeneralPorLaboratorioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExistenciaPorLaboratorioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents VencimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class
