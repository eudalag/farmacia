﻿
Imports CAPA_DATOS
Public Class form_facturacion_manual

    Private mod_conf As modulo = New modulo()

    Public Shared dtSource As DataTable

    Protected Sub inicializarParametros()
        With (dgvGrilla)
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False

            Dim nroFactura As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(nroFactura)
            With nroFactura
                .Name = "nroFactura"
                .HeaderText = "Nro. Factura"
                .DataPropertyName = "NRO_FACTURA"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fecha)
            With fecha
                .Name = "fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "FECHA"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim nit As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(nit)
            With nit
                .Name = "nit"
                .HeaderText = "Nit"
                .DataPropertyName = "NIT"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim cliente As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(cliente)
            With cliente
                .Name = "cliente"
                .HeaderText = "Cliente"
                .DataPropertyName = "NOMBRE_APELLIDOS"
                .Width = 350
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim cod_control As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(cod_control)
            With cod_control
                .Name = "cod_control"
                .HeaderText = "Codigo de Control"
                .DataPropertyName = "CODIGO_CONTROL"
                .Width = 150
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim total As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(total)
            With total
                .Name = "total"
                .HeaderText = "Total"
                .DataPropertyName = "TOTAL_FACTURA"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim estado As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(estado)
            With estado
                .Name = "estado"
                .HeaderText = "Estado"
                .DataPropertyName = "ESTADO"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton
                .HeaderText = "Ver"
                .Name = "colButton"
                .Width = 60
                .Frozen = False
                .Text = "Ver"
                .UseColumnTextForButtonValue = True
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID_FACTURA"
            End With

            Dim tipoMovimiento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(tipoMovimiento)
            With tipoMovimiento
                .Visible = False
                .Name = "tipoMovimiento"
                .DataPropertyName = "FACTURA_MANUAL"
            End With

        End With


    End Sub

    Protected Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE, @NIT AS NVARCHAR(15), @CLIENTE AS NVARCHAR(100), @NRO_COMPROBANTE AS NVARCHAR(10);"
        sSql += " SET @DESDE = CONVERT(DATE,'" + txtFecha.Value + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + txtHasta.Value + "',103);"
        sSql += " SET @NIT = '" + txtBuscar.Text + "';"
        sSql += " SET @CLIENTE = '" + txtBuscar.Text + "';"
        sSql += " SET @NRO_COMPROBANTE = '';"
        sSql += " SELECT ID_FACTURA,F.NRO_FACTURA,F.FECHA,TOTAL_FACTURA,CASE WHEN ANULADO = 1 THEN '' ELSE CODIGO_CONTROL END AS CODIGO_CONTROL,P.NOMBRE_APELLIDOS	,CL.NIT,FACTURA_MANUAL,CASE WHEN ANULADO = 1 THEN 'Anulado' Else  'Vigente' END AS ESTADO FROM FACTURA F"
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = F.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " WHERE (CONVERT(DATE,FECHA,103) >= @DESDE AND CONVERT(DATE,FECHA,103) <= @HASTA) AND"
        sSql += " (CL.NIT LIKE '%' + @NIT + '%' OR P.NOMBRE_APELLIDOS LIKE '%' + @CLIENTE  + '%') AND NRO_FACTURA = CASE WHEN LEN(LTRIM(@NRO_COMPROBANTE)) > 0 THEN RTRIM(LTRIM(@NRO_COMPROBANTE)) ELSE NRO_FACTURA END"
        sSql += " ORDER BY NRO_FACTURA DESC"
        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        cargarDatos()
        dgvGrilla.DataSource = dtSource
    End Sub

    Private Sub form_facturacion_manual_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
        cargarDatos()
        dgvGrilla.DataSource = dtSource
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim formulario As form_nueva_factura = New form_nueva_factura()
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, formulario)
        Me.Close()
    End Sub
End Class