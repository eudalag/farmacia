﻿Imports CAPA_DATOS
Imports ClosedXML.Excel
Imports System.Data
Public Class exportar_excel
    Private Shared _idVenta As Integer
    Public Sub New(ByVal idVenta As Integer)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().   
        _idVenta = idVenta
    End Sub
    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        txtDestino.Text = ""
        Dim dlgDestino As New FolderBrowserDialog
        With dlgDestino
            .Description = "Seleccione el directorio de destino a exportar. "
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim strDestino As String = .SelectedPath.ToString
                If Not strDestino.EndsWith("\") Then
                    strDestino = strDestino & "\"
                End If
                Me.txtDestino.Text = strDestino
            End If
        End With
    End Sub

    Private Sub obtenerDatos()
        Dim sSql As String = " DECLARE @VENTA AS INTEGER;"
        sSql += " SET @VENTA =  " + _idVenta.ToString
        sSql += " SELECT ROW_NUMBER() OVER (ORDER BY M.MEDICAMENTO) AS NUMERAL, UPPER(M.MEDICAMENTO) AS MEDICAMENTO,"
        sSql += " VM.CANTIDAD, VM.PRECIO_UNITARIO  FROM VENTA V "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA"
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO"
        sSql += " WHERE V.ID_VENTA = @VENTA"

        Dim tVenta As DataTable = New datos().ejecutarConsulta(sSql)

        Dim wb As New XLWorkbook()

        Dim ws = wb.Worksheets.Add("COTIZACION")
        Dim i As Integer = 1
        Dim pos As String = "0"

        pos = CStr(i)

        ws.Cell("A" & pos).SetValue("NRO. ITEM").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        ws.Cell("B" & pos).SetValue("MEDICAMENTO").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        ws.Cell("C" & pos).SetValue("CANTIDAD").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        ws.Cell("D" & pos).SetValue("PRECIO UNITARIO").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
        ws.Cell("E" & pos).SetValue("SUB TOTAL").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)


        Dim rngTable2 = ws.Range("A" & pos & ":E" & pos)
        rngTable2.Style.Fill.SetBackgroundColor(XLColor.FromArgb(119, 119, 119)).Font.SetBold(True)
        rngTable2.SetAutoFilter()
        i = i + 1

        For Each ventas In tVenta.Rows
            pos = CStr(i)
            ws.Cell("A" & pos).SetValue(ventas("NUMERAL"))
            ws.Cell("B" & pos).SetValue(ventas("MEDICAMENTO"))

            ws.Cell("C" & pos).SetValue(ventas("CANTIDAD"))
            ws.Cell("D" & pos).SetValue(ventas("PRECIO_UNITARIO"))

            ws.Cell("E" & pos).SetFormulaA1("=C" + pos + " * D" + pos)
            i = i + 1
        Next
        ws.Column("C").Style.NumberFormat.Format = "#0.00"
        ws.Column("D").Style.NumberFormat.Format = "#0.00"
        ws.Column("E").Style.NumberFormat.Format = "#0.00"


        ws.ColumnsUsed.AdjustToContents()
        wb.SaveAs(txtDestino.Text & "COTIZACION.xlsx")
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        If txtDestino.Text = "" Then
            Dim mensaje As String = "Escoja una ubicación valida."
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.Critical
            title = "Error"
            response = MsgBox(mensaje, style, title)
        Else
            obtenerDatos()
            Dim mensaje As String = "Archivo Exportado con exito."
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.OkOnly
            title = "Exportar Excel"
            response = MsgBox(mensaje, style, title)

        End If
    End Sub
End Class