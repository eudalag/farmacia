﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class form_gestion_cotizacion
    Public Shared dtSource As DataTable
    Private Shared _idVenta As Integer
    Private tPedido As eVenta
    Private tReceta As eReceta

    Private mod_conf As modulo = New modulo()
    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "LABORATORIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_UNITARIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "TOTAL"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        dtSource = tblItems
    End Sub

    Public Shared Sub nuevoItem(ByVal idMedicamento As Integer, ByVal nombre As String, ByVal cantidad As Integer, ByVal precioUnitario As Decimal, ByVal laboratorio As String, ByVal nuevo As Integer, ByVal accion As Integer)
        Dim tblItems As DataTable = dtSource
        'Dim foundRows() As DataRow
        'foundRows = tblItems.Select("NRO_FILA = MAX(NRO_FILA) ")
        Dim dr As DataRow
        'If foundRows.Length = 0 Then
        dr = tblItems.NewRow

        dr("NRO_FILA") = IIf(idMedicamento = 0, 999, dtSource.Rows.Count)
        dr("ID_MEDICAMENTO") = IIf(idMedicamento = 0, DBNull.Value, idMedicamento)
        dr("NOMBRE") = nombre
        dr("LABORATORIO") = laboratorio
        dr("CANTIDAD") = IIf(idMedicamento = 0, DBNull.Value, cantidad)
        dr("PRECIO_UNITARIO") = IIf(idMedicamento = 0, DBNull.Value, precioUnitario)
        dr("TOTAL") = IIf(idMedicamento = 0, DBNull.Value, CDec(cantidad) * CDec(precioUnitario))
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblItems.Rows.Add(dr)
        tblItems.AcceptChanges()
    End Sub

    Protected Sub actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For cont = 0 To dgvGrilla.Rows.Count - 1
            For Each tabla In tblItems.Rows
                If CInt(tabla("NRO_FILA")) = CInt(dgvGrilla.Rows(cont).Cells(5).Value) And dgvGrilla.Rows(cont).Cells(0).Value.ToString <> "" Then
                    Dim nroFila As Integer = dgvGrilla.Rows(cont).Cells(5).Value
                    Dim idMedicamento As String = dgvGrilla.Rows(cont).Cells(0).Value
                    Dim nombre As String = dgvGrilla.Rows(cont).Cells(2).Value
                    Dim laboratorio As String = dgvGrilla.Rows(cont).Cells(3).Value
                    Dim cantidad As Decimal = dgvGrilla.Rows(cont).Cells(1).Value
                    Dim precioUnitario As Decimal = dgvGrilla.Rows(cont).Cells(4).Value

                    If tabla("ID_MEDICAMENTO").ToString <> idMedicamento Or tabla("CANTIDAD").ToString <> cantidad.ToString Or tabla("NOMBRE") <> nombre Or tabla("PRECIO_UNITARIO").ToString <> precioUnitario.ToString Then
                        tabla("ACCION") = 2
                        tabla("NRO_FILA") = IIf(nroFila = 999, dgvGrilla.Rows.Count, nroFila)
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("CANTIDAD") = cantidad
                        tabla("NOMBRE") = nombre
                        tabla("LABORATORIO") = laboratorio
                        tabla("PRECIO_UNITARIO") = precioUnitario
                        tabla("TOTAL") = cantidad * precioUnitario
                    End If
                End If
            Next
        Next
        dtSource = tblItems
    End Sub

    Protected Sub iniciarItems()
        nuevoItem(0, "", 0, 0, "", 1, 1)
    End Sub

    Private Sub LoadPage()
        Dim dataView As New DataView(dtSource.Select("ACCION <> 3").CopyToDataTable)
        dataView.Sort = "NRO_FILA ASC"
        dgvGrilla.DataSource = dataView.ToTable()
    End Sub




    Private Sub inicializarParametros()
        'Me.totalRegistro = dtSource.Rows.Count
        'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        'Me.paginaActual = 1
        'Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100            

            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 6
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            '.Columns(0).Name = "nro_item"
            '.Columns(0).HeaderText = "Nro. Item"
            '.Columns(0).DataPropertyName = "NRO_ITEM"
            '.Columns(0).Width = 100
            '.Columns(0).ReadOnly = True
            '.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(0).Name = "codigo"
            .Columns(0).HeaderText = "Código"
            .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(0).Width = 100
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            .Columns(1).Name = "cantidad"
            .Columns(1).HeaderText = "Cantidad"
            .Columns(1).DataPropertyName = "CANTIDAD"
            .Columns(1).Width = 100
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "nombre"
            .Columns(2).HeaderText = "Detalle"
            .Columns(2).DataPropertyName = "NOMBRE"
            .Columns(2).Width = 500
            .Columns(2).ReadOnly = True

            .Columns(3).Name = "laboratorio"
            .Columns(3).HeaderText = "Laboratorio"
            .Columns(3).DataPropertyName = "LABORATORIO"
            .Columns(3).Width = 200
            .Columns(3).ReadOnly = True


            .Columns(4).Name = "precio_unitario"
            .Columns(4).HeaderText = "P. Unitario"
            .Columns(4).DataPropertyName = "PRECIO_UNITARIO"
            .Columns(4).Width = 100
            .Columns(4).ReadOnly = True
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


            .Columns(5).Name = "nro_fila"
            .Columns(5).DataPropertyName = "NRO_FILA"
            .Columns(5).Visible = False

            Dim colTotal As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            colTotal.Name = "total"
            colTotal.HeaderText = "Total"
            colTotal.Width = 100
            colTotal.DataPropertyName = "TOTAL"
            colTotal.ReadOnly = True
            colTotal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns.Add(colTotal)
        End With
        'LoadPage()
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tPedido = New eVenta()
        tReceta = New eReceta()
        _idVenta = -1
        crearTabla()
    End Sub
    Public Sub New(ByVal idVenta As Integer)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        crearTabla()
        _idVenta = idVenta
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + _idVenta.ToString + ";"
             sSql += " SELECT M.ID_MEDICAMENTO, "
        sSql += " UPPER(M.NOMBRE) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, R.CANTIDAD, R.PRECIO_UNITARIO,V.OBSERVACION "
        sSql += " FROM MEDICAMENTO M "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS R ON R.ID_MEDICAMENTO = M.ID_MEDICAMENTO   "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = R.ID_VENTA "
        sSql += " WHERE V.ID_VENTA =  @ID_VENTA "
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        Dim total As Decimal = 0
        txtNombreReceta.Text = tblConsulta.Rows(0)("OBSERVACION")
        For Each item In tblConsulta.Rows
            nuevoItem(item("ID_MEDICAMENTO"), item("MEDICAMENTO"), item("CANTIDAD"), item("PRECIO_UNITARIO"), item("LABORATORIO"), 0, 1)
            total = total + (CDec(item("CANTIDAD")) * CDec(item("PRECIO_UNITARIO")))
        Next
        txtTotal.Text = total.ToString("#,##0.00")
    End Sub

    Private Sub form_cotizacion_Load(sender As Object, e As EventArgs) Handles Me.Load
        'dtSource = tReceta.mostrarReceta(_idVenta)
        inicializarParametros()        
        iniciarItems()
        LoadPage()
        txtNombreReceta.Focus()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        actualizarTabla()
        Dim tipo As Integer = 3
        Using nuevo_pedido = New form_buscar_medicamento(tipo)
            If DialogResult.OK = nuevo_pedido.ShowDialog() Then                
                LoadPage()
                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                Me.dgvGrilla.BeginEdit(True)
            End If
        End Using
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        'Dim rowsToDelete As EnumerableRowCollection(Of DataRow) = Nothing
        'rowsToDelete = From tbl In dtSource Where tbl!NRO_FILA = dgvGrilla.SelectedCells(4).Value And tbl!NRO_FILA <> 999
        'For Each row As DataRow In rowsToDelete
        '    row.Delete()
        'Next
        actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For Each row As DataRow In tblItems.Rows
            If CStr(row("NRO_FILA")) = dgvGrilla.SelectedCells(5).Value Then
                row("ACCION") = 3
                Exit For
            End If
        Next
        dtSource.AcceptChanges()        
        LoadPage()
        calcular_total()
    End Sub
    Private Sub calcular_total()
        Dim total As Decimal = 0
        For Each row In dtSource.Select("ACCION <> 3")
            If Not (IsDBNull(row("CANTIDAD"))) And Not (IsDBNull(row("PRECIO_UNITARIO"))) Then
                total = total + Decimal.Parse(row("CANTIDAD")) * Decimal.Parse(row("PRECIO_UNITARIO"))
            End If
        Next
        txtTotal.Text = String.Format("{0:#0.00}", total)        
    End Sub

    Private Sub dgvGrilla_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvGrilla.DataBindingComplete
        calcular_total()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        mod_conf.volver_venta(form_cotizacion)
    End Sub

    

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "codigo"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "cantidad"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
        End Select
    End Sub

    Private Sub dgvGrilla_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellEndEdit
        If dgvGrilla.Rows.Count > 0 Then
            Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
                Case "codigo"
                    If e.RowIndex >= 0 Then
                        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value) Then
                            Dim med = New eMedicamento(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value)
                            If med.mensajesError = "" Then
                                Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value = med.NOMBRE.ToString + " (" & med.TIPO_MEDICAMENTO.DESCRIPCION & ")"
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = 1
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value = med.PRECIO_UNITARIO
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                                actualizarTabla()
                                If filAnt = 999 Then
                                    nuevoItem(0, "", 0, 0, "", 1, 1)
                                End If

                                'Dim foundRows() As DataRow
                                'foundRows = form_pedido.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")
                                'Dim dr As DataRow
                                'If foundRows.Length = 0 Then
                                '    dr = dtSource.NewRow
                                '    dr("NRO_ITEM") = 1
                                '    dr("ID_VENTA") = 0
                                '    dr("ID_MEDICAMENTO") = med.ID_MEDICAMENTO
                                '    dr("CANTIDAD") = 1
                                '    dr("PRECIO_UNITARIO") = med.PRECIO_UNITARIO
                                '    dr("NOMBRE") = med.NOMBRE
                                '    dr("TOTAL") = med.PRECIO_UNITARIO
                                '    dtSource.Rows.Add(dr)
                                'Else
                                '    dr = dtSource.NewRow
                                '    dr("NRO_ITEM") = CInt(dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
                                '    dr("ID_VENTA") = 0
                                '    dr("ID_MEDICAMENTO") = med.ID_MEDICAMENTO
                                '    dr("CANTIDAD") = 1
                                '    dr("PRECIO_UNITARIO") = med.PRECIO_UNITARIO
                                '    dr("NOMBRE") = med.NOMBRE
                                '    dr("TOTAL") = med.PRECIO_UNITARIO
                                '    dtSource.Rows.Add(dr)
                                'End If
                                'dtSource.AcceptChanges()
                                'Me.totalRegistro = dtSource.Rows.Count
                                'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                                'Me.paginaActual = 1
                                'Me.regNro = 0

                                'dgvGrilla.Update()
                                'Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                                'Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                                'Me.dgvGrilla.BeginEdit(True)
                                Try
                                    LoadPage()
                                    Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                                    Me.dgvGrilla.BeginEdit(True)
                                Catch ex As Exception

                                End Try

                            Else
                                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                            End If
                        End If
                    End If
                Case "cantidad"
                    Try
                        If dgvGrilla.EndEdit Then
                            actualizarTabla()
                            LoadPage()
                        End If
                    Catch ex As Exception

                    End Try
            End Select
        End If
    End Sub

    Protected Sub guardar()
        Dim msg As String = ""
        Using scope = New TransactionScope
            Try
                tPedido.FECHA = Now.ToString("dd/MM/yyyy")
                tPedido.ID_CLIENTE = 1
                tPedido.ID_PERSONA_CLIENTE = 1
                tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO                
                tPedido.NRO_VENTA = CInt(tPedido.obtenerNroPedido(3))
                tPedido.ID_TIPO_COMPROBANTE = 3
                tPedido.ID_DOSIFICACION = "null"
                tPedido.CODIGO_CONTROL = "null"
                tPedido.CODIGO_QR = "null"
                tPedido.ID_CAJA = form_principal_venta.lblIdCaja.Text
                tPedido.OBSERVACION = txtNombreReceta.Text
                tPedido.INSERTAR()

                _idVenta = tPedido.ID_VENTA                
                For Each row In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
                    tReceta.ID_VENTA = tPedido.ID_VENTA
                    tReceta.ID_MEDICAMENTO = row("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = row("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = row("CANTIDAD")
                    tReceta.insertar()
                Next
                scope.Complete()
            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
        End Using
        'If msg = "" Then
        'Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        'mod_conf.volver_venta(formulario)
        'End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        'If validarNombre(txtNombreReceta, ErrorProvider1) And validarImporte(txtTotal, ErrorProvider1) Then
        '    If _idVenta = -1 Then
        '        guardar()
        '        btnVolver_Click(Nothing, Nothing)
        '        Me.Close()
        '    Else
        '        Using scope = New TransactionScope
        '            Dim datos = New datos()
        '            datos.tabla = "VENTA"
        '            datos.campoID = "ID_VENTA"
        '            datos.valorID = _idVenta
        '            datos.agregarCampoValor("OBSERVACION", txtNombreReceta.Text)
        '            datos.modificar()
        '            procesarInsumos()
        '            scope.Complete()
        '        End Using
        '        btnVolver_Click(Nothing, Nothing)
        '        Me.Close()
        '    End If
        'End If


        ' Dim idVenta As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(5), DataGridViewTextBoxCell).Value
        Dim pedido As form_pedido = New form_pedido(_idVenta, True)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, pedido)
        Me.Close()
    End Sub

    Private Sub form_gestion_cotizacion_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.F3
                btnNuevo_Click(Nothing, Nothing)
            Case Keys.F5
                Me.dgvGrilla.BeginEdit(True)
                'dgvGrilla.BeginEdit(True)
            Case Keys.F4
                btnEliminar_Click(Nothing, Nothing)            
        End Select
    End Sub
    Protected Sub procesarInsumos()
        Dim tblInsumo = New datos()
        Dim tblItems = dtSource
        Dim proceso As String = ""
        Dim idMedicamento As Integer = 0
        Dim sSql As String = ""
        Dim tblDatos = New datos()
        For Each item As DataRow In tblItems.Select("ACCION <> 0 AND ID_MEDICAMENTO <> ''", "NRO_FILA ASC, ACCION DESC")
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    tReceta = New eReceta()
                    tReceta.ID_VENTA = _idVenta.ToString
                    tReceta.ID_MEDICAMENTO = item("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = item("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = item("CANTIDAD")
                    tReceta.insertar()
                End If
            Else
                Select Case item("ACCION")
                    Case "1"
                        sSql = "UPDATE VENTA_MEDICAMENTOS SET CANTIDAD = " + item("CANTIDAD").ToString + " WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                    Case "2"
                        sSql = "UPDATE VENTA_MEDICAMENTOS SET CANTIDAD = " + item("CANTIDAD").ToString + " WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                    Case "3"
                        sSql = "DELETE FROM VENTA_MEDICAMENTOS WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                End Select
            End If
            tblInsumo.reset()
        Next
        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "A", "alert('" + proceso + "')", True)        
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If _idVenta = -1 Then
            guardar()
        Else
            Using scope = New TransactionScope
                Dim datos = New datos()
                datos.tabla = "VENTA"
                datos.campoID = "ID_VENTA"
                datos.valorID = _idVenta
                datos.agregarCampoValor("OBSERVACION", txtNombreReceta.Text)
                datos.modificar()
                procesarInsumos()
                scope.Complete()
            End Using
        End If
        Dim venta = New form_pedido(_idVenta)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, venta)
        Me.Close()
    End Sub

    Private Function validarNombre(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If inputText.Text.Length = 0 Then
            errorProvider.SetError(inputText, "Ingrese el nombre de la receta a cotizar")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function

    Private Function validarImporte(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If CDec(inputText.Text) = 0 Then
            errorProvider.SetError(inputText, "El importe de la receta no es valido.")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click        
        Using Form = New exportar_excel(_idVenta)
            Form.ShowDialog()
        End Using
    End Sub
End Class