﻿Imports CAPA_DATOS
Public Class form_cotizacion
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4
            .Columns(0).Name = "nro_venta"
            .Columns(0).HeaderText = "Nro. Pedido"
            .Columns(0).DataPropertyName = "NRO_VENTA"
            .Columns(0).Width = 200
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(1).Name = "receta"
            .Columns(1).HeaderText = "Cliente"
            .Columns(1).DataPropertyName = "OBSERVACION"
            .Columns(1).Width = 800
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            .Columns(2).Name = "total"
            .Columns(2).HeaderText = "Total"
            .Columns(2).DataPropertyName = "IMPORTE_TOTAL"
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(2).Width = 100

            .Columns(3).Visible = False
            .Columns(3).Name = "id_venta"
            .Columns(3).DataPropertyName = "ID_VENTA"
        End With
        LoadPage()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub form_cotizacion_Load(sender As Object, e As EventArgs) Handles Me.Load
        mostrarCotizacion()
        inicializarParametros()
    End Sub
    Private Sub mostrarCotizacion()
        Dim sSql As String = ""
        sSql += " DECLARE @FECHA AS DATE, @BUSCAR AS NVARCHAR(150);"
        sSql += " SET @FECHA = CONVERT (DATE,'" + txtFecha.Text + "',103);"
        sSql += " SET @BUSCAR = '" + txtBuscar.Text + "';"
        sSql += " SELECT V.ID_VENTA,NRO_VENTA,V.FECHA,V.OBSERVACION,TC.DETALLE,SUM(R.CANTIDAD * R.PRECIO_UNITARIO) AS IMPORTE_TOTAL  FROM VENTA V"
        sSql += " INNER JOIN VENTA_MEDICAMENTOS R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " WHERE TC.ID_TIPO_COMPROBANTE = 3 AND (V.FECHA = @FECHA OR NRO_VENTA  LIKE '%' + @BUSCAR + '%' OR V.OBSERVACION LIKE '%' + @BUSCAR + '%')"
        sSql += " GROUP BY NRO_VENTA,V.FECHA,TC.DETALLE,V.OBSERVACION,V.ID_VENTA"
        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub

    Private Sub txtFecha_ValueChanged(sender As Object, e As EventArgs) Handles txtFecha.ValueChanged
        mostrarCotizacion()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        mostrarCotizacion()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idVenta As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(3), DataGridViewTextBoxCell).Value
        Dim pedido As form_gestion_cotizacion = New form_gestion_cotizacion(idVenta)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, pedido)
        Me.Close()

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Dim pedido As form_gestion_cotizacion = New form_gestion_cotizacion()
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, pedido)
        Me.Close()
    End Sub
End Class