﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Public Class actualizarInsumo
    Private _medicamento As eMedicamento
    Private idMedicamento As Integer
    Private mod_conf As New modulo()
    Private cantidad As Integer

    Public Sub New(ByVal _idMedicamento As Integer, ByVal _cantidad As Integer)
        InitializeComponent()
        iniciarCombo()
        _medicamento = New eMedicamento(_idMedicamento)
        cantidad = _cantidad
        idMedicamento = _idMedicamento
        cargarDatos()
    End Sub
    Protected Sub iniciarCombo()
        iniciarComboFormaFarmaceutica()
        iniciarComboLaboratorio()
    End Sub

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_FORMA_FARMACEUTICA ,LTRIM(DESCRIPCION) AS DESCRIPCION FROM FORMA_FARMACEUTICA UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Presentacion]' ORDER BY DESCRIPCION"

        cmbTipoMedicamento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_FORMA_FARMACEUTICA"
    End Sub
    Private Sub cargarDatos()
        txtMedicamento.Text = _medicamento.NOMBRE
        cmbTipoMedicamento.SelectedValue = _medicamento.ID_TIPO_MEDICAMENTO
        cmbLaboratorio.SelectedValue = _medicamento.ID_LABORATORIO
        txtMarca.Text = _medicamento.MARCA
        txtComposicion.Text = _medicamento.COMPOSICION
        txtConcentracion.Text = _medicamento.CONCENTRACION
        chkControlado.Checked = _medicamento.CONTROLADO
        cargarCodigoBarra()
    End Sub
    Private Sub cargarCodigoBarra()
        Dim sSql As String = ""
        sSql += " SELECT CODIGO_BARRA FROM CODIGO_BARRA "
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento.ToString
        sSql += " ORDER BY ID_CODIGO_BARRA DESC"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            txtCodigoBarra.Text = tblConsulta.Rows(0)(0)
        End If
    End Sub
    Private Sub insertarCodigoBarra()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " IF NOT EXISTS(SELECT *  FROM  CODIGO_BARRA  WHERE ID_MEDICAMENTO = @MEDICAMENTO AND CODIGO_BARRA = @COD_BARRA )  "
        sSql += " BEGIN "
        sSql += " INSERT CODIGO_BARRA VALUES (@COD_BARRA,@MEDICAMENTO)"
        sSql += " END"
        Dim tbl = New datos()
        tbl.ejecutarSQL(sSql)
    End Sub
    Private Function existeCodBarraMedicamento() As String
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " SELECT TOP 1 M.NOMBRE FROM CODIGO_BARRA CB"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = CB.ID_MEDICAMENTO "
        sSql += " WHERE M.ID_MEDICAMENTO <> @MEDICAMENTO AND CB.CODIGO_BARRA = @COD_BARRA "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count = 0 Then
            Return ""
        Else
            Return tbl.Rows(0)(0)
        End If
    End Function

    Private Sub chkMedicamento_CheckedChanged(sender As Object, e As EventArgs) Handles chkMedicamento.CheckedChanged
        txtMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbTipoMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbLaboratorio.Enabled = CType(sender, CheckBox).Checked
        txtMarca.Enabled = CType(sender, CheckBox).Checked
        txtComposicion.Enabled = CType(sender, CheckBox).Checked
        txtConcentracion.Enabled = CType(sender, CheckBox).Checked
        chkControlado.Enabled = CType(sender, CheckBox).Checked
        txtCodigoBarra.Enabled = CType(sender, CheckBox).Checked
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chkLote.CheckedChanged
        txtLote.Enabled = chkLote.Checked
    End Sub

    Private Sub chkControlFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlFecha.CheckedChanged
        txtFechaVencimiento.Enabled = chkControlFecha.Checked
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim msg As String = ""
            If chkMedicamento.Checked Then
                Dim MedCodBarra = existeCodBarraMedicamento()
                If MedCodBarra = "" Then
                    actualizarMedicamento()
                Else
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error"
                    msg = "El codigo de barra que intenta agregar, esta registrado con el medicamento: " + MedCodBarra + ", revise e intente nuevamente."
                    response = MsgBox(msg, style, title)
                End If
            End If
            If msg = "" Then
                nuevoKardex()
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        End If
        
    End Sub
    Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If chkLote.Checked Then
            If txtLote.Text.Length = 0 Then
                mensaje = "Debe Ingresar un lote Valido"
                ErrorProvider1.SetError(txtLote, mensaje)
                Return False
            End If
        Else
            Select Case mod_conf.mensajeConfirmacion("Lote", "El medicamento no tiene un Lote Asignado, desea que el sistema agregue un Numero de Lote Automatico")
                Case MsgBoxResult.Yes
                    txtLote.Text = "L" + _medicamento.ID_MEDICAMENTO.ToString
                Case MsgBoxResult.No
            End Select
        End If

        If chkControlFecha.Checked Then
            If Not IsDate(CDate(txtFechaVencimiento.Text)) Then
                mensaje = "Debe Ingresar una Fecha Valida"
                ErrorProvider1.SetError(txtFechaVencimiento, mensaje)
                Return False
            End If
        End If
        Return valido
    End Function
    Protected Sub actualizarMedicamento()
        Dim tblConsulta = New datos()
        If chkMedicamento.Checked Then
            tblConsulta.tabla = "MEDICAMENTO"
            tblConsulta.valorID = _medicamento.ID_MEDICAMENTO
            tblConsulta.campoID = "ID_MEDICAMENTO"
                tblConsulta.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                tblConsulta.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
                tblConsulta.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                tblConsulta.agregarCampoValor("MARCA", txtMarca.Text)
                tblConsulta.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                tblConsulta.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                tblConsulta.agregarCampoValor("CONTROLADO", chkControlado.Checked)
            tblConsulta.modificar()
        End If
        If Trim(txtCodigoBarra.Text).Length > 0 Then
            insertarCodigoBarra()
        End If
    End Sub
    Private Function existeKardex(ByVal nroLote As String, ByVal idMedicamento As Integer) As Integer
        Dim sSql As String = ""
        sSql += " DECLARE @LOTE AS NVARCHAR(15) , @MEDICAMENTO AS INTEGER;"
        sSql += " SET @LOTE = '" + nroLote + "';"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT ID_KARDEX FROM KARDEX"
        sSql += " WHERE NRO_LOTE = @LOTE AND ID_MEDICAMENTO = @MEDICAMENTO  "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count = 0 Then
            Return -1
        Else
            Return tbl.Rows(0)("ID_KARDEX")
        End If
    End Function
    Protected Sub nuevoKardex()
        Dim idKardex As Integer
        Dim nroLote = txtLote.Text
        idKardex = existeKardex(nroLote, idMedicamento)
        If idKardex = -1 Then
            Dim tbl = New datos
            tbl.tabla = "KARDEX"
            tbl.campoID = "ID_KARDEX"
            tbl.agregarCampoValor("NRO_LOTE", nroLote)
            If chkControlFecha.Checked Then
                tbl.agregarCampoValor("F_VENCIMIENTO", txtFechaVencimiento.Text)
            End If
            tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
            tbl.agregarCampoValor("CERRADO", 0)            
            tbl.insertar()
            idKardex = tbl.valorID
        End If
        movimientoKardex(idKardex)
    End Sub
    Protected Sub movimientoKardex(ByVal idKardex As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
        tbl.agregarCampoValor("CANTIDAD", cantidad * (-1))
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub
End Class