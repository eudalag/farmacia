﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Imports System.Transactions
Public Class pedido_mayor
    Private tPedido As eVenta
    Private tReceta As eReceta
    Private tCliente As eCliente
    Private _fecha As Date
    Public Shared dtSource As DataTable
    Public _idVenta As Integer
    Private _idTipoComprobante As Integer
    Private _tipoComprobante As Integer
    Private conf As eConfiguracion
    Private tDosificacion As eDosificacion
    Public Shared _nit As String
    Public Shared _registroPaciente As Integer = -1
    Private mod_conf As modulo = New modulo()
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 6
    Private tVenta As DataTable
    Private ReadInvoice As Boolean

    Private tblKardex As DataTable

    Private InString As String
    Private PistolaEnviando As Boolean = False
    Public Shared idClienteCredito As Integer
    Public Shared _idReceta As Integer
    Public Shared doctor As String

    Public Shared medicamento As String
    Public Shared vencimientos As String
    Public Shared registros As String
    Public Shared cantidad As Integer


    Private Sub LoadPage()
        Dim dataView As New DataView(dtSource.Select("ACCION <> 3").CopyToDataTable)
        dataView.Sort = "NRO_FILA ASC"
        dgvGrilla.DataSource = dataView.ToTable()
    End Sub

    Private Sub inicializarParametros()
        With (dgvGrilla)
            .Width = 1100

            If _idVenta <> -1 Then
                .ReadOnly = True
            End If
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton
                .HeaderText = ""
                .Name = "colEliminar"
                .Width = 25
                '.DisplayIndex = 0                
                .Frozen = False
                .Text = "X"
                .DefaultCellStyle.ForeColor = Color.Red
                .UseColumnTextForButtonValue = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With


            Dim colCantidad As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colCantidad)
            With colCantidad
                .Name = "cantidad"
                .HeaderText = "Cant."
                .DataPropertyName = "CANTIDAD"
                .Width = 70
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim colMedicamento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colMedicamento)
            With colMedicamento
                .Name = "nombre"
                .HeaderText = "Detalle"
                .DataPropertyName = "NOMBRE"
                .Width = 470
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colGenerico As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colGenerico)
            With colGenerico
                .Name = "generico"
                .HeaderText = "Nombre Generico"
                .DataPropertyName = "NOMBRE_GENERICO"
                .Width = 310
                .Visible = False
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim lote As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(lote)
            With lote
                .Name = "lote"
                .HeaderText = "Lote - Vencimiento - (Cantidad)"
                .DataPropertyName = "LOTE_VENCIMIENTO"
                .Width = 370
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colPrecioUnitario As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colPrecioUnitario)
            With colPrecioUnitario
                .Name = "precio_unitario"
                .HeaderText = "P. Uni."
                .DataPropertyName = "PRECIO_UNITARIO"
                .Width = 80
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colTotal As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colTotal)
            With colTotal
                .Name = "total"
                .HeaderText = "Total"
                .Width = 80
                .DataPropertyName = "TOTAL"
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Name = "registro"
                .DataPropertyName = "ID_MEDICAMENTO"
                .Visible = False
            End With
            Dim colNroFila As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colNroFila)
            With colNroFila
                .Name = "nro_fila"
                .DataPropertyName = "NRO_FILA"
                .Visible = False
            End With

            Dim lote_vencimiento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(lote_vencimiento)
            With lote_vencimiento
                .Name = "lote_registro"
                .DataPropertyName = "LOTE_REGISTRO"
                .Visible = False
            End With
        End With
    End Sub

    
    Public Sub New()
        InitializeComponent()
        _idReceta = -1
        _fecha = Now.Date
        tPedido = New eVenta()
        tCliente = New eCliente()
        tReceta = New eReceta()
        crearTabla()
        _idVenta = -1
        inicializarParametros()
        tDosificacion = New eDosificacion()
        'iniciarCombo()
        tDosificacion.cargarDatosDosificacionActiva()
        If tDosificacion.mensajesError <> "" Then
            chkRecibo.Checked = True
            chkRecibo.Enabled = False
            'tPedido.NRO_VENTA = 0
        Else
            'tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL
        End If

        tCliente.ID_CLIENTE = 1
        tCliente.cargarDatos()

        If form_sesion.cusuario.DESCUENTO Then
            txtTotalDescuento.ReadOnly = False
            txtTotalDescuento.Text = "0"
        Else
            txtTotalDescuento.ReadOnly = True
            txtTotalDescuento.Text = "0"
        End If

        If variableSesion.administrador Then
            txtFecha.Enabled = True
        End If

    End Sub
    
    Private Sub form_pedido_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtFecha.Text = _fecha
        txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
        'dtSource = tReceta.mostrarReceta(_idVenta)        
        txtNit.Focus()
        lblTC.Text = form_sesion.cConfiguracion.TC_ME
        iniciarItems()
        LoadPage()
    End Sub


    Protected Sub actualizarGrilla()
        Dim cont As Integer = 0
        For Each row In dtSource.Rows
            cont += 1
            If cont < dgvGrilla.Rows.Count Then
                row("NRO_FILA") = cont
            End If

        Next
        dtSource.AcceptChanges()
    End Sub

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged

        tCliente.buscarClientePedido(txtNit.Text)
        Dim cliente_factura = ""
        If tCliente.NOMBRE_FACTURA = "" Then
            If tCliente.PERSONA.NOMBRE_APELLIDOS = "" Then
                txtCliente.Text = ""
            Else
                txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
            End If
        Else
            txtCliente.Text = tCliente.NOMBRE_FACTURA
        End If
    End Sub    

    Private Sub txtPagado_Leave(sender As Object, e As EventArgs) Handles txtPagado.Leave, txtTarjeta.Leave, txtDolaresCambio.Leave, txtImporteCredito.Leave, txtTotalDescuento.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtPagado_Click(sender As Object, e As EventArgs) Handles txtPagado.Click, txtTarjeta.Click, txtDolaresCambio.Click, txtImporteCredito.Click, txtTotalDescuento.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtPagado_TextChanged(sender As Object, e As EventArgs) Handles txtPagado.TextChanged, txtImporteCredito.TextChanged
        If Me.txtPagado.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            Me.txtPagado.Text = "" 'Limpia la caja de texto        
        End If
        calcularCambio()
    End Sub
    Private Sub calcular_total()
        Dim total As Decimal = 0
        For cont = 0 To dgvGrilla.Rows.Count - 1
            If Not IsDBNull(dgvGrilla.Rows(cont).Cells("total").Value) Then
                total = total + Decimal.Parse(dgvGrilla.Rows(cont).Cells("total").Value)
            End If
        Next
        txtTotal.Text = String.Format("{0:#0.00}", total)
        calcularCambio()
    End Sub
    Private Sub calcularCambio()
        Dim totalCompra As String = IIf(Trim(txtTotalPagar.Text.Length) = 0, "0.00", txtTotalPagar.Text)

        Dim totalPagado As String = IIf(Trim(txtPagado.Text.Length) = 0, "0.00", txtPagado.Text)
        Dim totalDolares As String = IIf(Trim(txtDolares.Text.Length) = 0, "0.00", txtDolares.Text)
        Dim totalTarjeta As String = IIf(Trim(txtTarjeta.Text.Length) = 0, "0.00", txtTarjeta.Text)
        Dim totalCredito As String = IIf(Trim(txtImporteCredito.Text.Length) = 0, "0.00", txtImporteCredito.Text)
        Dim totalCancelado As String = Decimal.Parse(totalDolares) + Decimal.Parse(totalTarjeta) + Decimal.Parse(totalPagado)
        txtTotalCancelado.Text = totalCancelado
        txtCambio.Text = (Decimal.Parse(Replace(totalCancelado, ",", "."), New Globalization.CultureInfo("en-US")) + Decimal.Parse(totalCredito)) - Decimal.Parse(Replace(totalCompra, ",", "."), New Globalization.CultureInfo("en-US"))
    End Sub

    Private Sub dgvGrilla_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvGrilla.DataBindingComplete
        calcular_total()
        Me.dgvGrilla.Rows(dgvGrilla.RowCount - 1).Cells("colEliminar").ReadOnly = True
        'Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
        'Me.dgvGrilla.BeginEdit(True)
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        mod_conf.volver_venta(form_ventas)
    End Sub
    Private Function validarCambio(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Decimal.Parse(txtCambio.Text) < 0 Then
            errorProvider.SetError(inputText, "Importe cancelado menor al pedido.")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function

    Protected Sub guardar()
        Dim msg As String = ""
        Using scope = New TransactionScope
            Try
                If variableSesion.administrador Then
                    tPedido.FECHA = txtFecha.Text
                Else
                    tPedido.FECHA = _fecha
                End If

                tPedido.ID_CLIENTE = tCliente.ID_CLIENTE
                tPedido.ID_PERSONA_CLIENTE = tCliente.ID_PERSONA
                tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO
                tPedido.CREDITO = chkCredito.Checked
                If chkRecibo.Checked = False Then
                    tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL + CInt(tPedido.obtenerNroPedido(2, tDosificacion.ID_DOSIFICACION))
                Else
                    tPedido.NRO_VENTA = CInt(tPedido.obtenerNroPedido(1)) + 1
                End If

                'tPedido.FECHA = txtFecha.Text
                tPedido.ID_TIPO_COMPROBANTE = IIf(chkRecibo.Checked, 1, 2)
                If chkRecibo.Checked = False Then
                    tPedido.ID_DOSIFICACION = tDosificacion.ID_DOSIFICACION

                    Dim gen_cod_control = New CODIGO_CONTROL()
                    gen_cod_control.nro_autorizacion = Trim(tDosificacion.NUMERO_AUTORIZACION)
                    gen_cod_control.nit = IIf(txtNit.Text.Length = 0, "0", Trim(txtNit.Text))
                    gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
                    gen_cod_control.qrFecha = txtFecha.Text
                    gen_cod_control.llave = Trim(tDosificacion.LLAVE_DOSIFICACION)
                    gen_cod_control.nro_factura = Trim(tPedido.NRO_VENTA)
                    gen_cod_control.monto = Math.Round(CDec(Trim(txtTotal.Text)), 0)
                    gen_cod_control.qrMonto = txtTotal.Text
                    tPedido.CODIGO_CONTROL = gen_cod_control.getCodigoControl()

                    gen_cod_control.empresa = form_sesion.cConfiguracion.NOMBRE
                    gen_cod_control.nit_empresa = form_sesion.cConfiguracion.NIT
                    gen_cod_control.fecha_lim_emision = tDosificacion.F_LIMITE_EMISION
                    gen_cod_control.cliente = txtCliente.Text

                    cimgQR.Text = gen_cod_control.datosCodigoQr()


                    Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

                    cimgQR.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

                    tPedido.CODIGO_QR = ms.GetBuffer()

                Else
                    tPedido.ID_DOSIFICACION = "null"
                    tPedido.CODIGO_CONTROL = "null"
                    tPedido.CODIGO_QR = "null"
                End If
                'If form_sesion.cusuario.ADMINISTRADOR = 1 Then
                If variableSesion.administrador Then
                    tPedido.ID_CAJA = "null"
                Else
                    tPedido.ID_CAJA = form_principal_venta.lblIdCaja.Text
                End If

                tPedido.PAGADO = txtTotalCancelado.Text
                tPedido.TOTAL_PEDIDO = txtTotal.Text
                tPedido.DESCUENTO = txtTotalDescuento.Text
                tPedido.TOTAL_DESCUENTO = txtTotalDescuento.Text
                tPedido.EFECTIVO = txtPagado.Text
                tPedido.DOLARES = txtDolaresCambio.Text
                tPedido.TC = lblTC.Text
                tPedido.TARJETA = txtTarjeta.Text
                            
                If _tipoComprobante = 3 Then
                    tPedido.ID_VENTA = _idVenta
                    tPedido.MODIFICAR()
                Else
                    tPedido.INSERTAR()
                End If

                _idVenta = tPedido.ID_VENTA
                If chkCredito.Checked Then
                    guardarCredito(_idVenta)
                Else
                    If variableSesion.administrador Then
                        procesarMovimientoBanco(_idVenta, tPedido.NRO_VENTA)
                    End If
                End If
                If _tipoComprobante = 3 Then
                    Dim tbl = New datos()
                    tbl.tabla = "VENTA_MEDICAMENTOS"
                    tbl.campoID = "ID_VENTA"
                    tbl.valorID = _idVenta
                    tbl.eliminar()
                End If
                For Each row In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
                    tReceta.ID_VENTA = tPedido.ID_VENTA
                    tReceta.ID_MEDICAMENTO = row("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = row("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = row("CANTIDAD")
                    tReceta.PRECIO_PAQUETE = row("PRECIO_UNITARIO")
                    tReceta.insertar()




                    procesarMovimiento(row("ID_MEDICAMENTO"), row("CANTIDAD"), row("LOTE_REGISTRO"), row("LOTE_VENCIMIENTO"))

                Next
                scope.Complete()
            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
        End Using
        If msg = "" Then
            'Me.Close()
            Dim formulario As form_ventas = New form_ventas()
            mod_conf.volver_venta(formulario)
        End If
    End Sub
    Protected Sub procesarMovimientoBanco(ByVal idVenta As Integer, ByVal nroVenta As Integer)
        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtTotalPagar.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("GLOSA", "Comprobante: " + IIf(chkRecibo.Checked, "RECIBO", "FACTURA") + " - NRO.: " + nroVenta.ToString)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
        tbl.insertar()
    End Sub    

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If (_idVenta = -1 Or _tipoComprobante = 3) Then
            If validarCambio(txtCambio, ErrorProvider1) And validarPedido(txtTotal, ErrorProvider1) And validarCliente(txtCliente, ErrorProvider1) And validarCredito(txtClienteCredito, ErrorProvider1) And validarImporteCredito(txtImporteCredito, ErrorProvider1) Then
                guardar()
                If Not chkRecibo.Checked Then
                    If chkImprimir.Checked Then
                        PrintReport()
                    End If
                End If                
                If chkCredito.Checked Then
                    If Not variableSesion.administrador Then
                        imprimirCredito()
                        imprimirCredito()
                    End If

                End If
                Dim form = New form_pedido()
                mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
            End If
        Else
            If _tipoComprobante = 2 Then
                conf = New eConfiguracion()
                PrintReport()
                Dim form = New form_pedido()
                mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
            Else
                If validarCambio(txtCambio, ErrorProvider1) And validarPedido(txtTotal, ErrorProvider1) And validarCliente(txtCliente, ErrorProvider1) And validarCredito(txtClienteCredito, ErrorProvider1) And validarImporteCredito(txtImporteCredito, ErrorProvider1) Then
                    Using scope = New TransactionScope
                        tPedido.FECHA = _fecha
                        tPedido.ID_CLIENTE = tCliente.ID_CLIENTE
                        tPedido.ID_PERSONA_CLIENTE = tCliente.ID_PERSONA
                        tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO
                        tPedido.CREDITO = chkCredito.Checked
                        If (IIf(chkRecibo.Checked, 1, 2) <> _tipoComprobante) Then
                            tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL + CInt(tPedido.obtenerNroPedido(2, tDosificacion.ID_DOSIFICACION))
                        End If

                        'tPedido.FECHA = txtFecha.Text
                        tPedido.ID_TIPO_COMPROBANTE = IIf(chkRecibo.Checked, 1, 2)
                        If chkRecibo.Checked = False Then
                            tPedido.ID_DOSIFICACION = tDosificacion.ID_DOSIFICACION

                            Dim gen_cod_control = New CODIGO_CONTROL()
                            gen_cod_control.nro_autorizacion = Trim(tDosificacion.NUMERO_AUTORIZACION)
                            gen_cod_control.nit = IIf(txtNit.Text.Length = 0, "0", Trim(txtNit.Text))
                            gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
                            gen_cod_control.qrFecha = txtFecha.Text
                            gen_cod_control.llave = Trim(tDosificacion.LLAVE_DOSIFICACION)
                            gen_cod_control.nro_factura = Trim(tPedido.NRO_VENTA)
                            gen_cod_control.monto = Math.Round(CDec(Trim(txtTotal.Text)), 0)
                            gen_cod_control.qrMonto = txtTotal.Text
                            tPedido.CODIGO_CONTROL = gen_cod_control.getCodigoControl()

                            gen_cod_control.empresa = form_sesion.cConfiguracion.NOMBRE
                            gen_cod_control.nit_empresa = form_sesion.cConfiguracion.NIT
                            gen_cod_control.fecha_lim_emision = tDosificacion.F_LIMITE_EMISION
                            gen_cod_control.cliente = txtCliente.Text

                            cimgQR.Text = gen_cod_control.datosCodigoQr()


                            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

                            cimgQR.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

                            tPedido.CODIGO_QR = ms.GetBuffer()

                        Else
                            tPedido.ID_DOSIFICACION = "null"
                            tPedido.CODIGO_CONTROL = "null"
                            tPedido.CODIGO_QR = "null"
                        End If
                        If form_sesion.cusuario.ADMINISTRADOR = 1 Then
                            tPedido.ID_CAJA = "null"
                        Else
                            tPedido.ID_CAJA = form_principal_venta.lblIdCaja.Text
                        End If
                        tPedido.PAGADO = txtTotalCancelado.Text
                        tPedido.TOTAL_PEDIDO = txtTotal.Text
                        tPedido.DESCUENTO = txtTotalDescuento.Text
                        tPedido.TOTAL_DESCUENTO = txtTotalDescuento.Text
                        tPedido.EFECTIVO = txtPagado.Text
                        tPedido.DOLARES = txtDolaresCambio.Text
                        tPedido.TC = lblTC.Text
                        tPedido.TARJETA = txtTarjeta.Text
                        tPedido.ID_VENTA = _idVenta
                        tPedido.MODIFICAR()
                        scope.Complete()
                    End Using
                    'Dim print As factura = New factura
                    'print.imprimirComprobante(tPedido.ID_VENTA)
                    'conf = New eConfiguracion()
                    'PrintReport()
                    'btnVolver_Click(Nothing, Nothing)
                End If
            End If
        End If
    End Sub
    Private Function validarPedido(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Decimal.Parse(inputText.Text) > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Debe realizar un pedido.")
            valido = False
        End If
        Return valido
    End Function
    Private Function validarCliente(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Trim(inputText.Text).Length > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Cliente invalido.")
            valido = False
        End If
        Return valido
    End Function

    Private Function validarCredito(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If chkCredito.Checked Then
            If Trim(inputText.Text).Length > 0 Then
                errorProvider.SetError(inputText, "")
            Else
                errorProvider.SetError(inputText, "Cliente invalido.")
                valido = False
            End If
        End If
        Return valido
    End Function

    Private Function validarImporteCredito(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If chkCredito.Checked Then
            If IsNumeric(inputText.Text) Then
                If Decimal.Parse(inputText.Text) > 0 Then
                    errorProvider.SetError(inputText, "")
                Else
                    errorProvider.SetError(inputText, "Importe incorrecto.")
                    valido = False
                End If
            Else
                errorProvider.SetError(inputText, "Importe incorrecto.")
                valido = False
            End If
        End If
        Return valido
    End Function

    Private Sub PrintReport()
        Try
            PrintDocument1.Print()            
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir(_idVenta)
        print.SetInvoiceHead(e.Graphics)
        print.SetOrderData(e.Graphics)
        print.SetInvoiceData(e.Graphics, e)
        ReadInvoice = True
    End Sub

    Private Sub imprimir()
        ReadInvoice = False
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub txtPagado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTarjeta.KeyPress, txtPagado.KeyPress, txtDolaresCambio.KeyPress, txtTotalDescuento.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtNit.Focus()
        End If
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente()
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
            End If
        End Using
    End Sub

    Private Sub form_pedido_KeyPress_1(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        'Dim carAsc As Integer
        'carAsc = Asc(e.KeyChar)
        'If carAsc = 169 Then
        '    InString = ""
        '    PistolaEnviando = True
        '    e.Handled = True
        'ElseIf PistolaEnviando = True Then
        '    If carAsc = 174 Then
        '        PistolaEnviando = False
        '        'dgvGrilla.SelectedCells(1).Value = InString
        '        Dim med As DataTable = obtenerMedicamento(InString, 2)
        '        If med.Rows.Count > 0 Then
        '            'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
        '            actualizarTabla()
        '            nuevoItem(med.Rows(0)("ID_MEDICAMENTO"), med.Rows(0)("MEDICAMENTO"), med.Rows(0)("COMPOSICION"), 1, med.Rows(0)("PRECIO_UNITARIO"), "", "", 1, 1)
        '            calcular_total()
        '            Try
        '                LoadPage()
        '                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
        '                Me.dgvGrilla.BeginEdit(True)
        '            Catch ex As Exception

        '            End Try
        '        End If

        '    Else
        '        InString &= e.KeyChar.ToString
        '    End If
        '    e.Handled = True
        'End If
    End Sub

    Private Sub form_pedido_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.F3
                'btnNuevo_Click(Nothing, Nothing)
            Case Keys.F5
                Me.dgvGrilla.BeginEdit(True)
                'dgvGrilla.BeginEdit(True)
                'Case Keys.F4
                '    btnEliminar_Click(Nothing, Nothing)
            Case Keys.F7
                btnBuscar_Click(Nothing, Nothing)
            Case Keys.F8
                'btnNuevo_Click(Nothing, Nothing)
            Case Keys.F9
                btnAceptar_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        Else
            e.Handled = True
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 8 Then
            e.Handled = False
        End If
    End Sub

    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "codigo"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "cantidad"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "nombre"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "generico"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "colEliminar"
                actualizarTabla()
                Dim numFila = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                Dim fila As String = ""

                Dim tblItems As DataTable = dtSource
                For Each row As DataRow In tblItems.Rows
                    fila = row("NRO_FILA")
                    If fila = CStr(numFila) Then
                        row("ACCION") = 3
                        Exit For
                    End If
                Next
                dtSource.AcceptChanges()
                LoadPage()
                calcular_total()
        End Select
    End Sub

    Function obtenerMedicamento(ByVal dato As String, tipo As Integer) As DataTable 'TIPO 1 = REGISTRO ; TIPO 2 = CODIGO_BARRA
        Dim sSql As String = ""
        If Microsoft.VisualBasic.Left(dato, 7) = form_sesion.cConfiguracion.CODIGO_BARRA Then
            dato = Microsoft.VisualBasic.Left(dato, 12)
        End If
        sSql += " DECLARE @BUSCAR AS NVARCHAR(25);"
        sSql += " SET @BUSCAR = '" + dato + "';"

        sSql += " SELECT M.ID_MEDICAMENTO, UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END + ' - ' + LABORATORIO AS MEDICAMENTO,"
        sSql += " LABORATORIO,  UPPER(FORMA_FARMACEUTICA) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION, "
        sSql += " M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, "
        sSql += " ISNULL(S.CANTIDAD,0) AS STOCK, ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, M.CODIGO_BARRA "
        sSql += " FROM VW_MEDICAMENTO M"
        sSql += " INNER JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        Select Case tipo
            Case 1
                sSql += " WHERE M.ID_MEDICAMENTO =  @BUSCAR"
            Case 2
                sSql += " WHERE M.CODIGO_BARRA =  @BUSCAR"
        End Select
        sSql += " ORDER BY MEDICAMENTO,COMPOSICION, LABORATORIO"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub dgvGrilla_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellEndEdit
        If dgvGrilla.Rows.Count > 0 Then
            Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
                'Case "codigo"
                '    If e.RowIndex >= 0 Then
                '        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value) Then
                '            Dim med As DataTable = obtenerMedicamento((CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value), 2)
                '            If med.Rows.Count > 0 Then
                '                'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
                '                Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value = med.Rows(0)("ID_MEDICAMENTO")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value = med.Rows(0)("CODIGO_BARRA")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("stock"), DataGridViewTextBoxCell).Value = med.Rows(0)("STOCK")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value = med.Rows(0)("MEDICAMENTO")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = 1
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_UNITARIO")
                '                ' CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_MAYOR")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("CANTIDAD_MAYOR")

                '                actualizarTabla()
                '                calcular_total()
                '                If filAnt = 999 Then
                '                    nuevoItem(0, "", "", "", "", 0, 0, 1, 1)
                '                End If
                '                Try
                '                    LoadPage()
                '                    Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                '                    Me.dgvGrilla.BeginEdit(True)
                '                Catch ex As Exception

                '                End Try

                '            Else
                '                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                '                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                '            End If
                '        End If
                '    End If
                'Case "registro"
                '    If e.RowIndex >= 0 Then
                '        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value) Then
                '            Dim med As DataTable = obtenerMedicamento((CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value), 1)
                '            If med.Rows.Count > 0 Then
                '                'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
                '                Dim nombre As String ' NOMBRE = MEDICAMENTO x CONCENTRACION - MARCA ( LABORATORIO )
                '                nombre = med.Rows(0)("MEDICAMENTO")
                '                nombre += " (" + med.Rows(0)("LABORATORIO") + ")"
                '                Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value = med.Rows(0)("ID_MEDICAMENTO")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value = med.Rows(0)("CODIGO_BARRA")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("stock"), DataGridViewTextBoxCell).Value = med.Rows(0)("STOCK")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value = nombre
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = 1
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_UNITARIO")
                '                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_MAYOR")
                '                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("CANTIDAD_MAYOR")

                '                actualizarTabla()
                '                calcular_total()
                '                If filAnt = 999 Then
                '                    nuevoItem(0, "", "", "", "", 0, 0, 1, 1)
                '                End If
                '                Try
                '                    LoadPage()
                '                    Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                '                    Me.dgvGrilla.BeginEdit(True)
                '                Catch ex As Exception

                '                End Try

                '            Else
                '                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                '                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                '            End If
                '        End If
                '    End If
                Case "cantidad"
                    Try
                        If dgvGrilla.EndEdit Then
                            actualizarTabla()
                            calcular_total()
                            LoadPage()
                        End If
                    Catch ex As Exception

                    End Try
                Case "nombre"
                    If CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value.ToString <> "" Then
                        actualizarTabla()
                        Dim tipo As Integer = 2
                        Using nuevo_pedido = New form_buscar_medicamento(10, CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value.ToString, False)

                            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
     
                            End If
                        End Using
                        Using lote_vencimiento = New lote_vencimiento(medicamento)
                            If DialogResult.OK = lote_vencimiento.ShowDialog Then
                                Dim med = obtenerMedicamento(medicamento, 1)
                                nuevoItem(med.Rows(0)("ID_MEDICAMENTO"), med.Rows(0)("MEDICAMENTO"), med.Rows(0)("COMPOSICION"), vencimientos, registros, cantidad, med.Rows(0)("PRECIO_UNITARIO"), 1, 1)
                            End If
                        End Using
                        Me.totalRegistro = dtSource.Rows.Count
                        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                        Me.paginaActual = 1
                        Me.regNro = 0
                        LoadPage()
                        Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                        Me.dgvGrilla.BeginEdit(True)
                        calcular_total()
                    End If
                Case "generico"
                    If CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("generico"), DataGridViewTextBoxCell).Value.ToString <> "" Then
                        actualizarTabla()
                        Dim tipo As Integer = 2
                        Using nuevo_pedido = New form_buscar_medicamento(2, CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("generico"), DataGridViewTextBoxCell).Value.ToString, True)
                            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                                Me.totalRegistro = dtSource.Rows.Count
                                Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                                Me.paginaActual = 1
                                Me.regNro = 0
                                LoadPage()
                                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                                Me.dgvGrilla.BeginEdit(True)
                                calcular_total()
                            End If
                        End Using
                    End If
            End Select
        End If
    End Sub

    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "LOTE_VENCIMIENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "LOTE_REGISTRO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE_GENERICO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_UNITARIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "TOTAL"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()

        

        dtSource = tblItems
    End Sub

    Public Shared Sub nuevoItem(ByVal idMedicamento As Integer, ByVal nombre As String, ByVal nombreGenerico As String, ByVal lote As String, ByVal lote_registro As String, ByVal cantidad As Integer, ByVal precioUnitario As Decimal, ByVal nuevo As Integer, _
                                ByVal accion As Integer)
        Dim tblItems As DataTable = dtSource
        Dim dr As DataRow
        dr = tblItems.NewRow

        dr("NRO_FILA") = IIf(idMedicamento = 0, 999, dtSource.Rows.Count)
        dr("ID_MEDICAMENTO") = IIf(idMedicamento = 0, DBNull.Value, idMedicamento)        
        dr("NOMBRE") = nombre
        dr("NOMBRE_GENERICO") = nombreGenerico
        dr("CANTIDAD") = IIf(idMedicamento = 0, DBNull.Value, cantidad)
        dr("PRECIO_UNITARIO") = IIf(idMedicamento = 0, DBNull.Value, precioUnitario)
        Dim total As Decimal = 0
        total = cantidad * precioUnitario
        dr("TOTAL") = IIf(idMedicamento = 0, DBNull.Value, total)

        dr("NUEVO") = nuevo
        dr("ACCION") = accion    
        dr("LOTE_VENCIMIENTO") = IIf(idMedicamento = 0, DBNull.Value, lote)
        dr("LOTE_REGISTRO") = IIf(idMedicamento = 0, DBNull.Value, lote_registro)
        tblItems.Rows.Add(dr)              
        tblItems.AcceptChanges()
    End Sub

    Protected Sub actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For cont = 0 To dgvGrilla.Rows.Count - 1
            For Each tabla In tblItems.Rows
                '.dgvGrilla.Rows(e.RowIndex).Cells("generico"),
                If CInt(tabla("NRO_FILA")) = CInt(dgvGrilla.Rows(cont).Cells("nro_fila").Value) And (dgvGrilla.Rows(cont).Cells("cantidad").Value.ToString <> "" Or dgvGrilla.Rows(cont).Cells("registro").Value.ToString <> "") Then
                    Dim nroFila As Integer = dgvGrilla.Rows(cont).Cells("nro_fila").Value
                    Dim idMedicamento As String = dgvGrilla.Rows(cont).Cells("registro").Value
                    Dim cantidad As Decimal = dgvGrilla.Rows(cont).Cells("cantidad").Value
                    Dim nombre As String = dgvGrilla.Rows(cont).Cells("nombre").Value
                    Dim nombreGenerico As String = dgvGrilla.Rows(cont).Cells("generico").Value
                    Dim precio_unitario As Decimal = dgvGrilla.Rows(cont).Cells("precio_unitario").Value                
                    Dim lote As String = dgvGrilla.Rows(cont).Cells("lote").Value
                    Dim lote_registro As String = dgvGrilla.Rows(cont).Cells("lote_registro").Value
                    If tabla("ID_MEDICAMENTO").ToString <> idMedicamento Or tabla("CANTIDAD").ToString <> cantidad.ToString Then
                        tabla("ACCION") = 2
                        tabla("NRO_FILA") = IIf(nroFila = 999, dgvGrilla.Rows.Count, nroFila)
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("LOTE_VENCIMIENTO") = lote
                        tabla("NOMBRE") = nombre
                        tabla("NOMBRE_GENERICO") = nombreGenerico
                        tabla("CANTIDAD") = cantidad
                        tabla("PRECIO_UNITARIO") = precio_unitario
                        Dim total As Decimal = 0                     
                        total = cantidad * precio_unitario
                        tabla("TOTAL") = total
                        tabla("LOTE_REGISTRO") = lote_registro
                    End If
                End If
            Next
        Next
        dtSource = tblItems
    End Sub

    Protected Sub iniciarItems()
        nuevoItem(0, "", "", "", "", 0, 0, 1, 1)
    End Sub

    Private Sub txtDescuento_Leave(sender As Object, e As EventArgs)
        Dim result As String = CType(sender, TextBox).Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        CType(sender, TextBox).Text = result
    End Sub


    Private Sub txtDescuento_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtPagado.Focus()
        End If
    End Sub

    Private Sub txtTotal_TextChanged(sender As Object, e As EventArgs) Handles txtTotal.TextChanged
        txtTotalDescuento_TextChanged(Nothing, Nothing)
    End Sub

    Private Sub txtDescuento_Click(sender As Object, e As EventArgs)
        If (Not String.IsNullOrEmpty(sender.Text)) Then
            sender.SelectionStart = 0
            sender.SelectionLength = sender.Text.Length
        End If
    End Sub

    Private Sub txtTarjeta_TextChanged(sender As Object, e As EventArgs) Handles txtTarjeta.TextChanged
        Dim txt As TextBox = sender
        If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            txt.Text = "" 'Limpia la caja de texto        
        End If
        calcularCambio()
    End Sub

    Private Sub txtDolares_TextChanged(sender As Object, e As EventArgs) Handles txtDolaresCambio.TextChanged
        Dim txt As TextBox = sender
        If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            txt.Text = "" 'Limpia la caja de texto  
            txtDolares.Text = "0.00"
        Else
            Dim totalDolares = IIf(Trim(txtDolaresCambio.Text.Length) = 0, "0.00", txtDolaresCambio.Text)
            txtDolares.Text = Math.Round(Decimal.Parse(totalDolares) * Decimal.Parse(form_sesion.cConfiguracion.TC_ME), 2)
        End If
        calcularCambio()
    End Sub


    Private Sub btnBuscarClienteCredito_Click(sender As Object, e As EventArgs) Handles btnBuscarClienteCredito.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente(2)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                Dim tClienteCredito = New eCliente()
                tClienteCredito.ID_CLIENTE = idClienteCredito
                tClienteCredito.cargarDatos()
                txtClienteCredito.Text = IIf(tClienteCredito.NOMBRE_FACTURA.ToString = "", tClienteCredito.PERSONA.NOMBRE_APELLIDOS, tClienteCredito.NOMBRE_FACTURA)
                chkRecibo.Checked = True
                txtNit.Text = tClienteCredito.NIT
            End If
        End Using
    End Sub

    Private Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        Dim form As form_gestionCliente = New form_gestionCliente(txtNit.Text, True)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
                Dim cliente_buscar = New eCliente()
                cliente_buscar.buscarClientePedido(txtNit.Text)
                If cliente_buscar.PERSONA.NOMBRE_APELLIDOS <> "" Then
                    tCliente = cliente_buscar
                    txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
                Else
                    txtCliente.Text = ""
                End If
            End If
        End Using
    End Sub


    Private Sub chkCredito_CheckedChanged(sender As Object, e As EventArgs) Handles chkCredito.CheckedChanged
        btnBuscarClienteCredito.Enabled = chkCredito.Checked
        txtImporteCredito.ReadOnly = Not chkCredito.Checked
    End Sub

    Private Sub guardarCredito(ByVal idVenta As Integer)
        Dim tblCredito = New datos()
        tblCredito.tabla = "CREDITO"
        tblCredito.campoID = "ID_CREDITO"
        tblCredito.modoID = "sql"
        tblCredito.agregarCampoValor("ID_CLIENTE", idClienteCredito)
        tblCredito.agregarCampoValor("ID_VENTA", idVenta)
        tblCredito.agregarCampoValor("IMPORTE_CREDITO", txtImporteCredito.Text)
        tblCredito.agregarCampoValor("ESTADO", 0) ' 0 = ACTIVO; 1 = PAGADO
        tblCredito.insertar()
    End Sub

    Protected Sub procesarMovimiento(ByVal idMedicamento As Integer, ByVal cantidad As Integer, ByVal registro As String, ByVal lote As String)
        Dim kardex = registro.Split("*")
        Dim venc = lote.Split("*")
        Dim pos As Integer = 0
        For Each k In kardex            
            Dim d = venc(pos).Split("-")
            Dim cant As Integer = CInt(d(2))
            movimientoKardex(k, cant, _idVenta)
            pos = pos + 1
        Next
    End Sub
    
    Protected Sub movimientoKardex(ByVal idKardex As Integer, ByVal cantidad As Integer, ByVal idVent As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)
        tbl.agregarCampoValor("ID_VENTA_MEDICAMENTOS", idVent)
        tbl.insertar()
    End Sub    

  
    Private Sub imprimirCredito()
        Try
            PrintDocument3.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    

    Private Sub PrintDocument3_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument3.PrintPage
        Dim print = New imprimir()
        print.imprimirVoucherCredito(e.Graphics, _idVenta)
    End Sub

    Private Sub txtTotalDescuento_TextChanged(sender As Object, e As EventArgs) Handles txtTotalDescuento.TextChanged
        'txtTotalDescuento.Text = Math.Round(pedido * (porcDesc / 100), 2).ToString
        Dim desc = Decimal.Parse(IIf(Trim(txtTotalDescuento.Text.Length) = 0, "0.00", txtTotalDescuento.Text))
        Dim pedido = Decimal.Parse(IIf(Trim(txtTotal.Text.Length) = 0, "0.00", txtTotal.Text))
        txtTotalPagar.Text = pedido - desc
        calcularCambio()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick

        Using Form = New modal_informacion_medicamento(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value)
            Form.ShowDialog()
        End Using
    End Sub
End Class