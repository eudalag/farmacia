﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class facturar_venta
    Public Shared _nit As String
    Private tCliente As eCliente
    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente(3)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
            End If
        End Using
    End Sub

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged

        tCliente.buscarClientePedido(txtNit.Text)
        Dim cliente_factura = ""
        If tCliente.NOMBRE_FACTURA = "" Then
            If tCliente.PERSONA.NOMBRE_APELLIDOS = "" Then
                txtCliente.Text = ""
            Else
                txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
            End If
        Else
            txtCliente.Text = tCliente.NOMBRE_FACTURA
        End If
    End Sub

    Private Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        Dim form As form_gestionCliente = New form_gestionCliente(txtNit.Text, True)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
                Dim cliente_buscar = New eCliente()
                cliente_buscar.buscarClientePedido(txtNit.Text)
                If cliente_buscar.PERSONA.NOMBRE_APELLIDOS <> "" Then
                    tCliente = cliente_buscar
                    txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
                Else
                    txtCliente.Text = ""
                End If
            End If
        End Using
    End Sub
End Class