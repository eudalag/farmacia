﻿Imports CAPA_DATOS
Public Class form_buscarReceta
    Private mod_conf As modulo = New modulo()
    Private dtSourcePrincipal As DataTable
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 13

    Private idPaciente As Integer = 0

    Public Sub New(ByVal _idPaciente As Integer)
        InitializeComponent()
        idPaciente = _idPaciente
        cargarDatos()
    End Sub
    Private Sub cargarDatos()
        Dim sSql As String = ""
        sSql += "SELECT NOMBRE_APELLIDOS FROM PACIENTE WHERE ID_PACIENTE = " + idPaciente.ToString
        Dim tbl = New datos().ejecutarConsulta(sSql)
        txtPaciente.Text = tbl.Rows(0)(0)
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 650
            .Height = 301
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4

            .Columns(0).Name = "registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "REGISTRO"
            .Columns(0).Width = 100

            .Columns(1).Name = "fecha"
            .Columns(1).HeaderText = "Fecha Cons."
            .Columns(1).DataPropertyName = "FECHA"
            .Columns(1).Width = 100

            .Columns(2).Name = "diagnostico"
            .Columns(2).HeaderText = "Diagnostico"
            .Columns(2).DataPropertyName = "CONCLUSIONES"
            .Columns(2).Width = 350

            .Columns(3).Name = "vigente"
            .Columns(3).HeaderText = "Vigente"
            .Columns(3).DataPropertyName = "VIGENTE"
            .Columns(3).Width = 100

        End With
        LoadPage()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub form_buscar_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtBuscar.Focus()
        cargarReceta()
        dtSource = buscarReceta()
        inicializarParametros()
    End Sub
    Private Sub cargarReceta()
        Dim sSql As String = ""
        sSql += " DECLARE @PACIENTE AS INTEGER;"
        sSql += " SET @PACIENTE = " + idPaciente.ToString + ";"
        sSql += " SELECT RIGHT('000000' + CAST(HC.ID_HISTORICO AS NVARCHAR(6)),6) AS REGISTRO, "
        sSql += " CONVERT(NVARCHAR(10),FH_HISTORICO,103) AS FECHA, HC.CONCLUSIONES, "
        sSql += " CASE WHEN R.VIGENTE = 1 THEN 'Si'  ELSE 'No' END AS VIGENTE  FROM RECETA R "
        sSql += " INNER JOIN HISTORICO_CLINICO HC ON HC.ID_HISTORICO = R.ID_HISTORICO"
        sSql += " WHERE HC.ID_PACIENTE = @PACIENTE"
        dtSourcePrincipal = New datos().ejecutarConsulta(sSql)
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        form_pedido._idReceta = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        form_pedido.doctor = CType(dgvGrilla.Rows(e.RowIndex).Cells(1), DataGridViewTextBoxCell).Value
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        dtSource = buscarReceta()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub
    Function buscarReceta() As DataTable
        Dim filtro As String = "(CONCLUSIONES LIKE '%" + txtBuscar.Text.ToString + "%' OR REGISTRO LIKE '% " + txtBuscar.Text.ToString + "%') "
        If chkSoloVigentes.Checked Then
            filtro += " AND VIGENTE = 'Si'"
        End If
        Dim tbl = dtSourcePrincipal.Select(filtro)
        If tbl.Count > 0 Then
            Return tbl.CopyToDataTable
        Else
            Return dtSourcePrincipal.Clone
        End If
    End Function


    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            dgvGrilla.Focus()
        End If
    End Sub

    Private Sub form_buscar_cliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Dim form As form_gestionReceta = New form_gestionReceta(idPaciente)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Using       
    End Sub

    Private Sub chkSoloVigentes_CheckedChanged(sender As Object, e As EventArgs) Handles chkSoloVigentes.CheckedChanged
        If Not dtSourcePrincipal Is Nothing Then
            dtSource = buscarReceta()
            Me.totalRegistro = dtSource.Rows.Count
            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            Me.paginaActual = 1
            Me.regNro = 0
            LoadPage()
        End If
        
    End Sub
End Class