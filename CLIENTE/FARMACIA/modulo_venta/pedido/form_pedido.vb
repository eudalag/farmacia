﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class form_pedido
    Private tPedido As eVenta
    Private tReceta As eReceta
    Private tCliente As eCliente
    Private _fecha As Date    
    Public Shared dtSource As DataTable    
    Public _idVenta As Integer
    Public _cotizacion As Boolean = False
    Private _idTipoComprobante As Integer
    Private _tipoComprobante As Integer
    Private conf As eConfiguracion
    Private tDosificacion As eDosificacion
    Public Shared _nit As String
    Public Shared _registroPaciente As Integer = -1
    Private mod_conf As modulo = New modulo()    
    'Private paginaActual As Integer
    'Private regNro As Integer
    'Private totalRegistro As Integer
    'Private totalPagina As Integer
    'Private paginacion As Integer = 6
    Private tVenta As DataTable
    Private ReadInvoice As Boolean

    Private tblKardex As DataTable

    'Private InString As String
    'Private PistolaEnviando As Boolean = False
    Public Shared idClienteCredito As Integer    
    Public Shared _idReceta As Integer
    Public Shared doctor As String
    Private limiteCredito As Decimal = 0

    Private Sub LoadPage()
        Dim dataView As New DataView(dtSource.Select("ACCION <> 3").CopyToDataTable)
        dataView.Sort = "NRO_FILA ASC"
        dgvGrilla.DataSource = dataView.ToTable()
    End Sub
    
    Private Sub inicializarParametros()
        With (dgvGrilla)
            .Width = 1100

            If _idVenta <> -1 And _cotizacion = False Then
                .ReadOnly = True
            End If
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False            
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton                
                .HeaderText = ""            
                .Name = "colEliminar"                
                .Width = 25
                '.DisplayIndex = 0                
                .Frozen = False
                .Text = "X"
                .DefaultCellStyle.ForeColor = Color.Red
                .UseColumnTextForButtonValue = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim colCodigo As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colCodigo)
            With colCodigo
                .Name = "codigo"
                .HeaderText = "Código Barra"
                .DataPropertyName = "CODIGO_BARRA"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft                
            End With

            Dim colStock As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colStock)
            With colStock
                .Name = "stock"
                .HeaderText = "Stock"
                .DataPropertyName = "STOCK"
                .Width = 50
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .ReadOnly = True
            End With

            Dim colCantidad As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colCantidad)
            With colCantidad
                .Name = "cantidad"
                .HeaderText = "Cant."
                .DataPropertyName = "CANTIDAD"
                .Width = 50
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter                
            End With

            Dim colMedicamento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colMedicamento)
            With colMedicamento
                .Name = "nombre"
                .HeaderText = "Detalle"
                .DataPropertyName = "NOMBRE"
                .Width = 300
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colGenerico As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colGenerico)
            With colGenerico
                .Name = "generico"
                .HeaderText = "Nombre Generico"
                .DataPropertyName = "NOMBRE_GENERICO"
                .Width = 305
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colPrecioUnitario As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colPrecioUnitario)
            With colPrecioUnitario
                .Name = "precio_unitario"
                .HeaderText = "P. Uni."
                .DataPropertyName = "PRECIO_UNITARIO"
                .Width = 80
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colPrecioPaquete As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colPrecioPaquete)
            With colPrecioPaquete
                .Name = "precio_mayor_vw"
                .HeaderText = "P. Paq."
                .DataPropertyName = "PRECIO_MAYOR_VW"
                .Width = 85
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colCantidadMayor As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colCantidadMayor)
            With colCantidadMayor
                .Name = "cantidad_mayor"                
                .DataPropertyName = "CANTIDAD_MAYOR"                
                .Visible = False                
            End With

            Dim colPrecioMayor As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colPrecioMayor)
            With colPrecioMayor
                .Name = "precio_mayor"
                .DataPropertyName = "PRECIO_MAYOR"
                .Visible = False
            End With

            Dim colNroFila As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colNroFila)
            With colNroFila
                .Name = "nro_fila"
                .DataPropertyName = "NRO_FILA"
                .Visible = False
            End With

            Dim colTotal As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colTotal)
            With colTotal
                .Name = "total"
                .HeaderText = "Total"
                .Width = 80
                .DataPropertyName = "TOTAL"
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight                
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Name = "registro"
                .DataPropertyName = "ID_MEDICAMENTO"
                .Visible = False
            End With

        End With
    End Sub

    Private Sub inicializarParametrosIndicaciones()
        With (dgvIndicaciones)
            .Width = 650
            .Height = 301
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 2

            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 250

            .Columns(1).Name = "indicaciones"
            .Columns(1).HeaderText = "Indicaciones"
            .Columns(1).DataPropertyName = "INDICACIONES"
            .Columns(1).Width = 400
        End With
    End Sub
    Public Sub New()
        InitializeComponent()
        _idReceta = -1
        _fecha = Now.Date
        tPedido = New eVenta()
        tCliente = New eCliente()
        tReceta = New eReceta()
        crearTabla()
        _idVenta = -1
        inicializarParametros()
        tDosificacion = New eDosificacion()
        'iniciarCombo()
        tDosificacion.cargarDatosDosificacionActiva()
        If tDosificacion.mensajesError <> "" Then
            chkRecibo.Checked = True
            chkRecibo.Enabled = False            
            'tPedido.NRO_VENTA = 0
        Else
            'tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL
        End If

        tCliente.ID_CLIENTE = 1
        tCliente.cargarDatos()

        If form_sesion.cusuario.DESCUENTO Then
            txtTotalDescuento.ReadOnly = False
            txtTotalDescuento.Text = "0.00"
        Else
            txtTotalDescuento.ReadOnly = True
            txtTotalDescuento.Text = "0.00"
        End If

        If variableSesion.administrador Then
            txtFecha.Enabled = True
        End If

    End Sub
    Public Sub New(ByVal idVenta As Integer, Optional ByVal cotizacion As Boolean = False)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        _idReceta = -1
        _cotizacion = cotizacion
        tPedido = New eVenta()
        tCliente = New eCliente()
        _idVenta = idVenta
        Dim tabla As DataTable = tPedido.cargarDatosPedido(_idVenta)
        tCliente.ID_CLIENTE = tabla.Rows(0)("ID_CLIENTE")
        tCliente.cargarDatos()
        txtNit.Text = tCliente.NIT
        If CInt(tabla.Rows(0)("ID_TIPO_COMPROBANTE")) = 1 Then
            _fecha = tabla.Rows(0)("FECHA")
        Else
            _fecha = Now.ToString("dd/MM/yyyy")
        End If

        tReceta = New eReceta()
        tDosificacion = New eDosificacion()
        'iniciarCombo()
        tDosificacion.cargarDatosDosificacionActiva()
        If tDosificacion.mensajesError <> "" Then
            chkRecibo.Checked = True
            chkRecibo.Enabled = False

            'tPedido.NRO_VENTA = 0
        Else
            'tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL
            chkRecibo.Checked = False
        End If
        txtTotal.Text = tabla.Rows(0)("TOTAL_PEDIDO")
        txtTotalCancelado.Text = tabla.Rows(0)("PAGADO")
        txtTarjeta.Text = tabla.Rows(0)("TARJETA")
        txtDolaresCambio.Text = tabla.Rows(0)("DOLARES")
        txtPagado.Text = tabla.Rows(0)("EFECTIVO")
        txtTotalDescuento.Text = tabla.Rows(0)("DESCUENTO")
        txtTotalDescuento.Text = tabla.Rows(0)("TOTAL_DESCUENTO")
        If CInt(tabla.Rows(0)("ID_TIPO_COMPROBANTE")) = 1 Then
            lblTC.Text = tabla.Rows(0)("TC")
        Else
            lblTC.Text = form_sesion.cConfiguracion.TC_ME
        End If

        'btnNuevo.Visible = False
        'btnEliminar.Visible = False
        txtPagado.Enabled = False
        txtTotalDescuento.Enabled = False
        txtDolaresCambio.Enabled = False
        txtTarjeta.Enabled = False
        _tipoComprobante = CInt(tabla.Rows(0)("ID_TIPO_COMPROBANTE"))
        chkControlada.Enabled = False
        chkCredito.Enabled = False
        btnBuscarReceta.Enabled = False
        txtReceta.Enabled = False
        If CInt(tabla.Rows(0)("ID_TIPO_COMPROBANTE")) = 2 Then
            txtNit.Enabled = False
            btnNuevoCliente.Visible = False
            chkRecibo.Checked = False
            chkRecibo.Enabled = False
            btnBuscarCliente.Visible = False
        End If

        crearTabla()
        inicializarParametros()
        _idVenta = idVenta
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + _idVenta.ToString + ";"

        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " LABORATORIO, MARCA, UPPER(FORMA_FARMACEUTICA ) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION, "
        sSql += " M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, "
        sSql += " ISNULL(S.CANTIDAD,0) AS STOCK,R.CANTIDAD,"
        sSql += " ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA"
        sSql += " FROM VW_MEDICAMENTO M "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS  R ON R.ID_MEDICAMENTO = M.ID_MEDICAMENTO   "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = R.ID_VENTA "
        sSql += " LEFT JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE V.ID_VENTA =  @ID_VENTA "
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        Dim total As Decimal = 0
        Dim nombre As String ' NOMBRE = MEDICAMENTO x CONCENTRACION - MARCA ( LABORATORIO )
        For Each item In tblConsulta.Rows
            nombre = item("MEDICAMENTO")
            nuevoItem(item("ID_MEDICAMENTO"), item("COMPOSICION"), nombre, item("CANTIDAD"), item("PRECIO_UNITARIO"), item("STOCK"), 0, 1, item("CANTIDAD_MAYOR"), item("PRECIO_MAYOR"), item("CODIGO_BARRA"))
            total = total + (CDec(item("CANTIDAD")) * CDec(item("PRECIO_UNITARIO")))
        Next
        txtTotal.Text = total.ToString("#,##0.00")
        If CInt(_tipoComprobante) = 3 Then
            txtPagado.Text = "0.00"
            txtDolaresCambio.Text = "0.00"
            txtTarjeta.Text = "0.00"
            txtPagado.Enabled = True
            txtTarjeta.Enabled = True
            txtDolaresCambio.Enabled = True
            txtTotalDescuento.Enabled = True
            txtTotalDescuento.Text = "0.00"
        End If
        'If CBool(tblConsulta.Rows(0)("ANULADA")) Then
        '    btnAceptar.Visible = False
        '    txtNit.Enabled = False
        '    btnNuevoClient.Visible = False
        '    cmbTipoComprobante.Enabled = False
        '    btnBuscar.Visible = False
        '    cmbTipoComprobante.SelectedValue = CInt(tblConsulta.Rows(0)("ID_TIPO_COMPROBANTE"))
        'End If
    End Sub

    Public Sub New(ByVal idReceta As Integer, ByVal tipo As Integer)
        InitializeComponent()
        _fecha = Now.Date
        tPedido = New eVenta()
        tCliente = New eCliente()
        tReceta = New eReceta()
        crearTabla()
        _idVenta = -1
        _idReceta = idReceta
        inicializarParametros()
        tDosificacion = New eDosificacion()
        'iniciarCombo()
        tDosificacion.cargarDatosDosificacionActiva()
        If tDosificacion.mensajesError <> "" Then
            chkRecibo.Checked = True
            chkRecibo.Enabled = False
            'tPedido.NRO_VENTA = 0
        Else
            'tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL
        End If

        tCliente.ID_CLIENTE = 1
        tCliente.cargarDatos()
        If form_sesion.cusuario.DESCUENTO Then
            txtTotalDescuento.ReadOnly = False
            txtTotalDescuento.Text = "0"
        Else
            txtTotalDescuento.ReadOnly = True
            txtTotalDescuento.Text = "0"
        End If

        Select Case tipo
            Case 1 'Receta
                txtReceta.Text = idReceta
                'buscarReceta(idReceta)
        End Select
    End Sub
    
    'Protected Sub iniciarCombo()        
    '    cmbTipoComprobante.DataSource = tPedido.mostrarComboTipoComprobante()
    '    cmbTipoComprobante.DisplayMember = "DETALLE"
    '    cmbTipoComprobante.ValueMember = "ID_TIPO_COMPROBANTE"
    'End Sub

    Private Sub form_pedido_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtFecha.Text = _fecha
        txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS        
        'dtSource = tReceta.mostrarReceta(_idVenta)        
        txtNit.Focus()
        lblTC.Text = form_sesion.cConfiguracion.TC_ME        
        iniciarItems()
        LoadPage()
    End Sub    

    'Private Sub btnNuevo_Click(sender As Object, e As EventArgs)
    '    actualizarTabla()
    '    Dim tipo As Integer = 2
    '    Using nuevo_pedido = New form_buscar_medicamento(tipo)
    '        If DialogResult.OK = nuevo_pedido.ShowDialog() Then
    '            Me.totalRegistro = dtSource.Rows.Count
    '            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
    '            Me.paginaActual = 1
    '            Me.regNro = 0
    '            LoadPage()
    '            Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
    '            Me.dgvGrilla.BeginEdit(True)
    '            calcular_total()
    '        End If
    '    End Using
    'End Sub


    Protected Sub actualizarGrilla()
        Dim cont As Integer = 0
        For Each row In dtSource.Rows
            cont += 1
            If cont < dgvGrilla.Rows.Count Then
                row("NRO_FILA") = cont
            End If

        Next
        dtSource.AcceptChanges()
    End Sub

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged

        tCliente.buscarClientePedido(txtNit.Text)
        Dim cliente_factura = ""
        If tCliente.NOMBRE_FACTURA = "" Then
            If tCliente.PERSONA.NOMBRE_APELLIDOS = "" Then
                txtCliente.Text = ""
            Else
                txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
            End If
        Else
            txtCliente.Text = tCliente.NOMBRE_FACTURA
        End If
    End Sub
    Private Sub buscarReceta(ByVal nroReceta As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @RECETA AS INTEGER;"
        sSql += " SET @RECETA  = " + nroReceta.ToString + ";"
        sSql += " SELECT R.ID_HISTORICO,HC.CONCLUSIONES,RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO, "
        sSql += " UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END AS MEDICAMENTO,RD.CANTIDAD AS CANTIDAD_RECETADA,"
        sSql += " M.PRECIO_UNITARIO, ISNULL(S.STOCK,0) AS STOCK,ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_PAQUETE , ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO) AS PRECIO_PAQUETE,INDICACIONES,ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA,M.COMPOSICION FROM RECETA R "
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = R.ID_PACIENTE "
        sSql += " INNER JOIN RECETA_DETALLE RD ON RD.ID_RECETA = R.ID_RECETA"
        sSql += " INNER JOIN HISTORICO_CLINICO HC ON HC.ID_HISTORICO = R.ID_HISTORICO"
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = RD.ID_MEDICAMENTO"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_MEDICAMENTO ,SUM(CANTIDAD) AS STOCK FROM MOVIMIENTO_KARDEX MK INNER JOIN KARDEX K ON K.ID_KARDEX= MK.ID_KARDEX  GROUP BY ID_MEDICAMENTO"
        sSql += " ) S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE R.ID_RECETA = @RECETA;"
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            'actualizarTabla()
            For Each item In tbl.Rows
                nuevoItem(item("ID_MEDICAMENTO"), item("MEDICAMENTO"), item("COMPOSICION"), item("CANTIDAD_RECETADA"), item("PRECIO_UNITARIO"), item("STOCK"), 1, 1, item("CANTIDAD_PAQUETE"), item("PRECIO_PAQUETE"), item("CODIGO_BARRA"))
            Next            
            txtTratamiento.Text = tbl.Rows(0)("CONCLUSIONES")
            inicializarParametrosIndicaciones()
            dgvIndicaciones.DataSource = tbl
            _idReceta = nroReceta
            calcular_total()
            Try
                LoadPage()
                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                Me.dgvGrilla.BeginEdit(True)
            Catch ex As Exception

            End Try
        Else
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Information
            title = "Receta"
            Dim msg = "No existe el numero de Receta Ingresado"
            response = MsgBox(msg, style, title)
            _idReceta = -1
        End If

    End Sub

    Private Sub txtPagado_Leave(sender As Object, e As EventArgs) Handles txtPagado.Leave, txtTarjeta.Leave, txtDolaresCambio.Leave, txtImporteCredito.Leave, txtTotalDescuento.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtPagado_Click(sender As Object, e As EventArgs) Handles txtPagado.Click, txtTarjeta.Click, txtDolaresCambio.Click, txtImporteCredito.Click, txtTotalDescuento.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtPagado_TextChanged(sender As Object, e As EventArgs) Handles txtPagado.TextChanged, txtImporteCredito.TextChanged
        If Me.txtPagado.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            Me.txtPagado.Text = "" 'Limpia la caja de texto        
        End If
        calcularCambio()
    End Sub
    Function calcularSubTotal()
        For cont = 0 To dgvGrilla.Rows.Count - 1
            If Not IsDBNull(dgvGrilla.Rows(cont).Cells("cantidad").Value) And Not IsDBNull(dgvGrilla.Rows(cont).Cells("precio_unitario").Value) Then
                dgvGrilla.Rows(cont).Cells("total").Value = CInt(dgvGrilla.Rows(cont).Cells("cantidad").Value) * Decimal.Parse(dgvGrilla.Rows(cont).Cells("precio_unitario").Value, New Globalization.CultureInfo("en-US"))
            End If
        Next
    End Function
    Private Sub calcular_total()
        Dim total As Decimal = 0
        For cont = 0 To dgvGrilla.Rows.Count - 1
            If Not IsDBNull(dgvGrilla.Rows(cont).Cells("total").Value) Then
                total = total + Decimal.Parse(dgvGrilla.Rows(cont).Cells("total").Value)
            End If
        Next
        txtTotal.Text = String.Format("{0:#0.00}", total)
        calcularCambio()
    End Sub
    Private Sub calcularCambio()
        Dim totalCompra As String = IIf(Trim(txtTotalPagar.Text.Length) = 0, "0.00", txtTotalPagar.Text)

        Dim totalPagado As String = IIf(Trim(txtPagado.Text.Length) = 0, "0.00", txtPagado.Text)
        Dim totalDolares As String = IIf(Trim(txtDolares.Text.Length) = 0, "0.00", txtDolares.Text)
        Dim totalTarjeta As String = IIf(Trim(txtTarjeta.Text.Length) = 0, "0.00", txtTarjeta.Text)
        Dim totalCredito As String = IIf(Trim(txtImporteCredito.Text.Length) = 0, "0.00", txtImporteCredito.Text)
        Dim totalCancelado As String = Decimal.Parse(totalDolares) + Decimal.Parse(totalTarjeta) + Decimal.Parse(totalPagado)
        txtTotalCancelado.Text = totalCancelado
        txtCambio.Text = (Decimal.Parse(Replace(totalCancelado, ",", "."), New Globalization.CultureInfo("en-US")) + Decimal.Parse(totalCredito)) - Decimal.Parse(Replace(totalCompra, ",", "."), New Globalization.CultureInfo("en-US"))
    End Sub

    Private Sub dgvGrilla_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvGrilla.DataBindingComplete
        calcular_total()
        Me.dgvGrilla.Rows(dgvGrilla.RowCount - 1).Cells("colEliminar").ReadOnly = True
        'Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
        'Me.dgvGrilla.BeginEdit(True)
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        mod_conf.volver_venta(form_ventas)
    End Sub
    Private Function validarCambio(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True

        If Decimal.Parse(txtCambio.Text) < 0 Then
            errorProvider.SetError(inputText, "Importe cancelado menor al pedido.")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function

    Protected Sub actualizarTotales()

    End Sub
    Protected Sub guardarFactura(ByVal idVenta As Integer)
        Dim nroFactura As Integer = tDosificacion.N_FACT_INICIAL + CInt(tPedido.obtenerNroPedido(2, tDosificacion.ID_DOSIFICACION))
        Dim CODIGO_CONTROL As String = ""

        Dim gen_cod_control = New CODIGO_CONTROL()
        gen_cod_control.nro_autorizacion = Trim(tDosificacion.NUMERO_AUTORIZACION)
        gen_cod_control.nit = IIf(txtNit.Text.Length = 0, "0", Trim(txtNit.Text))
        gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
        gen_cod_control.qrFecha = txtFecha.Text
        gen_cod_control.llave = Trim(tDosificacion.LLAVE_DOSIFICACION)
        gen_cod_control.nro_factura = Trim(tPedido.NRO_VENTA)
        gen_cod_control.monto = Math.Round(CDec(Trim(txtTotal.Text)), 0)
        gen_cod_control.qrMonto = txtTotal.Text

        CODIGO_CONTROL = gen_cod_control.getCodigoControl()

        gen_cod_control.empresa = form_sesion.cConfiguracion.NOMBRE
        gen_cod_control.nit_empresa = form_sesion.cConfiguracion.NIT
        gen_cod_control.fecha_lim_emision = tDosificacion.F_LIMITE_EMISION
        gen_cod_control.cliente = txtCliente.Text

        cimgQR.Text = gen_cod_control.datosCodigoQr()


        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

        cimgQR.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

        Dim CODIGO_QR = ms.GetBuffer()


        Dim tblDatos = New datos()
        tblDatos.tabla = "FACTURA"
        tblDatos.modoID = "auto"
        tblDatos.campoID = "ID_FACTURA"
        tblDatos.agregarCampoValor("NRO_FACTURA", nroFactura)
        tblDatos.agregarCampoValor("TOTAL_FACTURA", txtTotal.Text)
        tblDatos.agregarCampoValor("ID_CLIENTE", tCliente.ID_CLIENTE)
        tblDatos.agregarCampoValor("FECHA", "serverDateTime")
        tblDatos.agregarCampoValor("ID_DOSIFICACION", tDosificacion.ID_DOSIFICACION)
        tblDatos.agregarCampoValor("CODIGO_CONTROL", CODIGO_CONTROL)
        tblDatos.agregarCampoValor("CODIGO_QR", CODIGO_QR)
        tblDatos.agregarCampoValor("FACTURA_MANUAL", 0)
        tblDatos.agregarCampoValor("ID_VENTA", idVenta)
        tblDatos.agregarCampoValor("ANULADO", 0)
        tblDatos.insertar()

    End Sub

    Protected Sub guardar(ByRef msg As String)
        Using scope = New TransactionScope
            Try
                If variableSesion.administrador Then
                    tPedido.FECHA = txtFecha.Text
                Else
                    tPedido.FECHA = _fecha
                End If

                tPedido.ID_CLIENTE = tCliente.ID_CLIENTE
                tPedido.ID_PERSONA_CLIENTE = tCliente.ID_PERSONA
                tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO
                tPedido.CREDITO = chkCredito.Checked
                ''If chkRecibo.Checked = False Then

                ''Else
                tPedido.NRO_VENTA = CInt(tPedido.obtenerNroPedido(1)) + 1
                ''End If

                'tPedido.FECHA = txtFecha.Text
                tPedido.ID_TIPO_COMPROBANTE = IIf(chkRecibo.Checked, 1, 2)
                ''If chkRecibo.Checked = False Then


                ''Else
                tPedido.ID_DOSIFICACION = "null"
                tPedido.CODIGO_CONTROL = "null"
                tPedido.CODIGO_QR = "null"
                ''End If
                'If form_sesion.cusuario.ADMINISTRADOR = 1 Then
                'If variableSesion.administrador Then
                '    tPedido.ID_CAJA = "null"
                'Else
                '    tPedido.ID_CAJA = form_principal_venta.lblIdCaja.Text
                'End If
                tPedido.ID_BANCO = variableSesion.idBanco
                tPedido.PAGADO = txtTotalCancelado.Text
                tPedido.TOTAL_PEDIDO = txtTotal.Text
                tPedido.DESCUENTO = txtTotalDescuento.Text
                tPedido.TOTAL_DESCUENTO = txtTotalDescuento.Text
                tPedido.EFECTIVO = txtPagado.Text
                tPedido.DOLARES = txtDolaresCambio.Text
                tPedido.TC = lblTC.Text
                tPedido.TARJETA = txtTarjeta.Text
                tPedido.COTIZACION = _cotizacion
                If chkTarjeta.Checked Then
                    tPedido.VOUCHER = txtNroVoucher.Text
                End If
                If IsNumeric(txtRegistro.Text) Then
                    tPedido.ID_PACIENTE = txtRegistro.Text
                    tPedido.TRATAMIENTO = txtTratamiento.Text
                End If
                If _idReceta > 0 Then
                    tPedido.ID_RECETA = _idReceta
                    'guardarRecetaDetalle()
                End If
                If _tipoComprobante = 3 Then
                    tPedido.ID_VENTA = _idVenta
                    tPedido.ID_BANCO = variableSesion.idBanco
                    tPedido.MODIFICAR()
                Else
                    tPedido.INSERTAR()
                End If

                _idVenta = tPedido.ID_VENTA
                If chkCredito.Checked Then
                    guardarCredito(_idVenta)               
                End If
                If _tipoComprobante = 3 Then
                    Dim tbl = New datos()
                    tbl.tabla = "VENTA_MEDICAMENTOS"
                    tbl.campoID = "ID_VENTA"
                    tbl.valorID = _idVenta
                    tbl.eliminar()

                    tbl.reset()
                    tbl.tabla = "VENTA"
                    tbl.campoID = "ID_VENTA"
                    tbl.valorID = _idVenta
                    tbl.agregarCampoValor("FECHA", "serverDateTime")
                    tbl.modificar()
                End If

                Dim medicamentos As Integer = 0
                For Each row In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
                    tReceta.ID_VENTA = tPedido.ID_VENTA
                    tReceta.ID_MEDICAMENTO = row("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = row("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = row("CANTIDAD")
                    tReceta.PRECIO_PAQUETE = row("PRECIO_MAYOR")
                    tReceta.insertar()
                    medicamentos += 1
                    If row("STOCK") <= 0 Then
                        Using form = New actualizarInsumo(row("ID_MEDICAMENTO"), row("CANTIDAD"))
                            Dim dialog As Boolean = True
                            While dialog
                                If DialogResult.OK = form.ShowDialog() Then
                                    dialog = False
                                End If
                            End While
                        End Using
                    Else
                        cargarKardex(row("ID_MEDICAMENTO"))
                        procesarMovimiento(row("ID_MEDICAMENTO"), row("CANTIDAD"), row("CANTIDAD_MAYOR"))
                    End If
                Next

                If chkRecibo.Checked = False Then
                    guardarFactura(tPedido.ID_VENTA)
                End If

                If medicamentos > 0 Then
                    scope.Complete()
                Else
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Medicamentos"
                    msg = "Error al generar la lista de medicamentos, revise e intente nuevamente."
                    response = MsgBox(msg, style, title)
                End If

            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
        End Using
        If msg = "" Then
            'Me.Close()
            Dim formulario As form_ventas = New form_ventas()
            mod_conf.volver_venta(formulario)
        End If
    End Sub
    Protected Sub procesarMovimientoBanco(ByVal idVenta As Integer, ByVal nroVenta As Integer)
        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtTotalPagar.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("GLOSA", "Comprobante: " + IIf(chkRecibo.Checked, "RECIBO", "FACTURA") + " - NRO.: " + nroVenta.ToString)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
        tbl.insertar()
    End Sub
    'Protected Sub guardarRecetaDetalle()
    '    Dim tblDetalle = New datos()        
    '    For Each row In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
    '        tblDetalle.tabla = "RECETA_DETALLE"
    '        tblDetalle.campoID = "ID_RECETA_DETALLE"
    '        tblDetalle.modoID = "sql"
    '        tblDetalle.agregarCampoValor("ID_RECETA", idReceta)
    '        tblDetalle.agregarCampoValor("ID_MEDICAMENTO", row("ID_MEDICAMENTO"))
    '        tblDetalle.agregarCampoValor("CANTIDAD", row("CANTIDAD"))
    '        tblDetalle.insertar()
    '        tblDetalle.reset()
    '    Next
    'End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If (_idVenta = -1 Or _tipoComprobante = 3) Then
            If validarCambio(txtCambio, ErrorProvider1) And validarPedido(txtTotal, ErrorProvider1) And validarCliente(txtCliente, ErrorProvider1) And validarCredito(txtClienteCredito, ErrorProvider1) And validarImporteCredito(txtImporteCredito, ErrorProvider1) And validarMedicamentos(dgvGrilla, ErrorProvider1) And validarVoucher(txtTarjeta, ErrorProvider1) Then
                Dim msg As String = ""
                guardar(msg)
                If msg = "" Then
                    If Not chkRecibo.Checked Then
                        If chkImprimir.Checked Then
                            PrintReport()
                        End If
                    End If
                    If _idReceta > 0 Then
                        imprimirReceta()
                    End If
                    If chkCredito.Checked Then
                        If Not variableSesion.administrador Then
                            imprimirCredito()
                            imprimirCredito()
                        End If

                    End If
                    Dim form = New form_pedido()
                    mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
                End If               
            End If
        Else
            If _tipoComprobante = 2 Then
                conf = New eConfiguracion()
                PrintReport()
                Dim form = New form_pedido()
                mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
            Else
                If validarCambio(txtCambio, ErrorProvider1) And validarPedido(txtTotal, ErrorProvider1) And validarCliente(txtCliente, ErrorProvider1) And validarCredito(txtClienteCredito, ErrorProvider1) And validarImporteCredito(txtImporteCredito, ErrorProvider1) And validarMedicamentos(dgvGrilla, ErrorProvider1) And validarVoucher(txtTarjeta, ErrorProvider1) Then
                    Using scope = New TransactionScope
                        tPedido.FECHA = _fecha
                        tPedido.ID_CLIENTE = tCliente.ID_CLIENTE
                        tPedido.ID_PERSONA_CLIENTE = tCliente.ID_PERSONA
                        tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO
                        tPedido.CREDITO = chkCredito.Checked
                        tPedido.ID_BANCO = variableSesion.idBanco
                        If (IIf(chkRecibo.Checked, 1, 2) <> _tipoComprobante) Then
                            tPedido.NRO_VENTA = tDosificacion.N_FACT_INICIAL + CInt(tPedido.obtenerNroPedido(2, tDosificacion.ID_DOSIFICACION))
                        End If

                        'tPedido.FECHA = txtFecha.Text
                        tPedido.ID_TIPO_COMPROBANTE = IIf(chkRecibo.Checked, 1, 2)
                        tPedido.ID_DOSIFICACION = "null"
                        tPedido.CODIGO_CONTROL = "null"
                        tPedido.CODIGO_QR = "null"
                        If chkRecibo.Checked = False Then         
                            guardarFactura(tPedido.ID_VENTA)

                            'tPedido.ID_DOSIFICACION = tDosificacion.ID_DOSIFICACION

                            'Dim gen_cod_control = New CODIGO_CONTROL()
                            'gen_cod_control.nro_autorizacion = Trim(tDosificacion.NUMERO_AUTORIZACION)
                            'gen_cod_control.nit = IIf(txtNit.Text.Length = 0, "0", Trim(txtNit.Text))
                            'gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
                            'gen_cod_control.qrFecha = txtFecha.Text
                            'gen_cod_control.llave = Trim(tDosificacion.LLAVE_DOSIFICACION)
                            'gen_cod_control.nro_factura = Trim(tPedido.NRO_VENTA)
                            'gen_cod_control.monto = Math.Round(CDec(Trim(txtTotal.Text)), 0)
                            'gen_cod_control.qrMonto = txtTotal.Text
                            'tPedido.CODIGO_CONTROL = gen_cod_control.getCodigoControl()

                            'gen_cod_control.empresa = form_sesion.cConfiguracion.NOMBRE
                            'gen_cod_control.nit_empresa = form_sesion.cConfiguracion.NIT
                            'gen_cod_control.fecha_lim_emision = tDosificacion.F_LIMITE_EMISION
                            'gen_cod_control.cliente = txtCliente.Text

                            'cimgQR.Text = gen_cod_control.datosCodigoQr()


                            'Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

                            'cimgQR.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

                            'tPedido.CODIGO_QR = ms.GetBuffer()

                        End If
                        'If form_sesion.cusuario.ADMINISTRADOR = 1 Then
                        '    tPedido.ID_CAJA = "null"
                        'Else
                        '    tPedido.ID_CAJA = form_principal_venta.lblIdCaja.Text
                        'End If


                        tPedido.ID_BANCO = variableSesion.idBanco
                        tPedido.PAGADO = txtTotalCancelado.Text
                        tPedido.TOTAL_PEDIDO = txtTotal.Text
                        tPedido.DESCUENTO = txtTotalDescuento.Text
                        tPedido.TOTAL_DESCUENTO = txtTotalDescuento.Text
                        tPedido.EFECTIVO = txtPagado.Text
                        tPedido.DOLARES = txtDolaresCambio.Text
                        tPedido.TC = lblTC.Text
                        tPedido.TARJETA = txtTarjeta.Text
                        If chkTarjeta.Checked Then
                            tPedido.VOUCHER = txtNroVoucher.Text
                        End If
                        If IsNumeric(txtRegistro.Text) Then
                            tPedido.ID_PACIENTE = txtRegistro.Text
                            tPedido.TRATAMIENTO = txtTratamiento.Text
                        End If
                        If _idReceta > 0 Then
                            tPedido.ID_RECETA = _idReceta
                            'guardarRecetaDetalle()
                        End If
                        If _tipoComprobante = 3 Then
                            tPedido.ID_VENTA = _idVenta
                            tPedido.ID_BANCO = variableSesion.idBanco
                            tPedido.MODIFICAR()
                        Else
                            tPedido.INSERTAR()
                        End If

                        _idVenta = tPedido.ID_VENTA
                        If chkCredito.Checked Then
                            guardarCredito(_idVenta)
                        End If

                        tPedido.MODIFICAR()
                        scope.Complete()
                    End Using
                    'Dim print As factura = New factura
                    'print.imprimirComprobante(tPedido.ID_VENTA)
                    'conf = New eConfiguracion()
                    'PrintReport()
                    'btnVolver_Click(Nothing, Nothing)
                End If
            End If
        End If
    End Sub
    Private Function validarPedido(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Decimal.Parse(inputText.Text) > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Debe realizar un pedido.")
            valido = False
        End If
        Return valido
    End Function
    Private Function validarMedicamentos(ByVal lista As DataGridView, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True

        If Decimal.Parse(dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3").Count) > 0 Then
            errorProvider.SetError(lista, "")
        Else
            errorProvider.SetError(lista, "revise la lista de medicamento.")
            valido = False
        End If
        Return valido
    End Function

    Private Function validarVoucher(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If chkTarjeta.Checked Then
            If Decimal.Parse(inputText.Text) > 0 Then
                If Trim(txtNroVoucher.Text).Length > 0 Then
                    errorProvider.SetError(inputText, "")
                Else
                    errorProvider.SetError(txtNroVoucher, "Nro. Voucher Invalido.")
                    valido = False
                End If
            Else
                errorProvider.SetError(inputText, "Monto Incorrecto.")
                valido = False
            End If
        End If
       
        Return valido
    End Function
    Private Function validarCliente(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Trim(inputText.Text).Length > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Cliente invalido.")
            valido = False
        End If
        Return valido
    End Function

    Private Function validarCredito(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If chkCredito.Checked Then
            If Trim(inputText.Text).Length > 0 Then
                errorProvider.SetError(inputText, "")
                If limiteCredito < Decimal.Parse(txtTotalPagar.Text, New Globalization.CultureInfo("en-US")) Then
                    Dim msj = New modulo()
                    msj.mensajeError("LIMITE DE CREDITO", "El importe de la compra, Supero el limite de credito otorgado al Cliente.")
                    errorProvider.SetError(inputText, "Limite de Credito Excedido")
                    valido = False
                End If
            Else
                errorProvider.SetError(inputText, "Cliente invalido.")
                valido = False
            End If
        End If

        Return valido
    End Function

    Private Function validarImporteCredito(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If chkCredito.Checked Then
            If IsNumeric(inputText.Text) Then
                If Decimal.Parse(inputText.Text) > 0 Then
                    errorProvider.SetError(inputText, "")
                Else
                    errorProvider.SetError(inputText, "Importe incorrecto.")
                    valido = False
                End If
            Else
                errorProvider.SetError(inputText, "Importe incorrecto.")
                valido = False
            End If
        End If
        Return valido
    End Function

    Private Sub PrintReport()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir(_idVenta)
        print.SetInvoiceHead(e.Graphics)
        print.SetOrderData(e.Graphics)
        print.SetInvoiceData(e.Graphics, e)
        ReadInvoice = True
    End Sub

    Private Sub imprimir()
        ReadInvoice = False
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub txtPagado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTarjeta.KeyPress, txtPagado.KeyPress, txtDolaresCambio.KeyPress, txtTotalDescuento.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtNit.Focus()
        End If
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente()
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
            End If
        End Using
    End Sub

    'Private Sub form_pedido_KeyPress_1(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
    '    'Dim carAsc As Integer
    '    'carAsc = Asc(e.KeyChar)
    '    'If carAsc = 169 Then
    '    '    InString = ""
    '    '    PistolaEnviando = True
    '    '    e.Handled = True
    '    'ElseIf PistolaEnviando = True Then
    '    '    If carAsc = 174 Then
    '    '        PistolaEnviando = False
    '    '        'dgvGrilla.SelectedCells(1).Value = InString
    '    '        Dim med As DataTable = obtenerMedicamento(InString, 2)
    '    '        If med.Rows.Count > 0 Then
    '    '            'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
    '    '            actualizarTabla()
    '    '            nuevoItem(med.Rows(0)("ID_MEDICAMENTO"), med.Rows(0)("MEDICAMENTO"), med.Rows(0)("COMPOSICION"), 1, med.Rows(0)("PRECIO_UNITARIO"), med.Rows(0)("STOCK"), 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), med.Rows(0)("PRECIO_MAYOR"), med.Rows(0)("CODIGO_BARRA"))
    '    '            calcular_total()
    '    '            Try
    '    '                LoadPage()
    '    '                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
    '    '                Me.dgvGrilla.BeginEdit(True)
    '    '            Catch ex As Exception

    '    '            End Try
    '    '        End If

    '    '    Else
    '    '        InString &= e.KeyChar.ToString
    '    '    End If
    '    '    e.Handled = True
    '    'End If
    'End Sub

    Private Sub form_pedido_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.F3
                'btnNuevo_Click(Nothing, Nothing)
            Case Keys.F5
                Me.dgvGrilla.BeginEdit(True)
                'dgvGrilla.BeginEdit(True)
                'Case Keys.F4
                '    btnEliminar_Click(Nothing, Nothing)
            Case Keys.F7
                btnBuscar_Click(Nothing, Nothing)
            Case Keys.F8
                'btnNuevo_Click(Nothing, Nothing)
            Case Keys.F9
                btnAceptar_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        Else
            e.Handled = True
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 8 Then
            e.Handled = False
        End If
    End Sub

    

   

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick

        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "codigo"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "cantidad"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "nombre"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "generico"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "colEliminar"
                actualizarTabla()
                Dim numFila = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                Dim fila As String = ""

                Dim tblItems As DataTable = dtSource
                For Each row As DataRow In tblItems.Rows
                    fila = row("NRO_FILA")
                    If fila = CStr(numFila) Then
                        row("ACCION") = 3
                        Exit For
                    End If
                Next
                dtSource.AcceptChanges()
                LoadPage()
                calcular_total()
        End Select
    End Sub

    Function obtenerMedicamento(ByVal dato As String, tipo As Integer) As DataTable 'TIPO 1 = REGISTRO ; TIPO 2 = CODIGO_BARRA
        Dim sSql As String = ""
        If Microsoft.VisualBasic.Left(dato, 7) = form_sesion.cConfiguracion.CODIGO_BARRA Then
            dato = Microsoft.VisualBasic.Left(dato, 12)
        End If
        'sSql += " DECLARE @BUSCAR AS NVARCHAR(25);"
        'sSql += " SET @BUSCAR = '';"

        'sSql += " SELECT M.ID_MEDICAMENTO, UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END + ' - ' + LABORATORIO AS MEDICAMENTO,"
        'sSql += " LABORATORIO,  UPPER(FORMA_FARMACEUTICA) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION, "
        'sSql += " M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, "
        'sSql += " ISNULL(S.CANTIDAD,0) AS STOCK, ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, M.CODIGO_BARRA "
        'sSql += " FROM VW_MEDICAMENTO M"
        'sSql += " INNER JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO "

        'sSql += " ORDER BY MEDICAMENTO,COMPOSICION, LABORATORIO"


        sSql += " DECLARE @BUSCAR AS NVARCHAR(150);"
        sSql += " SET @BUSCAR = '" + dato + "';"


        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO, "
        sSql += " UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END  AS MEDICAMENTO, "
        sSql += " LABORATORIO, UPPER(FORMA_FARMACEUTICA) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION,  M.MINIMO,"
        sSql += " ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO,  "
        sSql += " ISNULL(S.CANTIDAD,0) AS STOCK, CASE WHEN M.VENTA_MAYOR = 1 THEN ISNULL(M.CANTIDAD_PAQUETE,1) ELSE 1 END AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA,"
        sSql += " ISNULL(CONVERT(NVARCHAR(10),KD.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO"
        sSql += " FROM VW_MEDICAMENTO M        "
        sSql += " LEFT JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " LEFT JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY K.ID_MEDICAMENTO ORDER BY K.ID_MEDICAMENTO, K.F_VENCIMIENTO) AS NRO_KARDEX, K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS STOCK FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.SALDOS > 0"
        sSql += " GROUP BY K.ID_MEDICAMENTO,K.ID_KARDEX, K.F_VENCIMIENTO) KD ON KD.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND KD.NRO_KARDEX = 1"
        Select Case tipo
            Case 1
                sSql += " WHERE M.ID_MEDICAMENTO =  @BUSCAR"
            Case 2
                sSql += " WHERE M.CODIGO_BARRA =  @BUSCAR"
        End Select
        sSql += " AND ISNULL(S.CANTIDAD,0) > 0"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub dgvGrilla_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellEndEdit
        If dgvGrilla.Rows.Count > 0 Then
            Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
                Case "codigo"
                    If e.RowIndex >= 0 Then
                        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value) Then
                            Dim med As DataTable = obtenerMedicamento((CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value), 2)
                            If med.Rows.Count > 0 Then
                                'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
                                'Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value = med.Rows(0)("ID_MEDICAMENTO")
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value =
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("stock"), DataGridViewTextBoxCell).Value =
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value = med.Rows(0)("MEDICAMENTO")
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = 1
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value = 
                                '' CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_mayor"), DataGridViewTextBoxCell).Value =
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad_mayor"), DataGridViewTextBoxCell).Value =
                                nuevoItem(med.Rows(0)("ID_MEDICAMENTO"), med.Rows(0)("MEDICAMENTO"), med.Rows(0)("COMPOSICION"), 1, med.Rows(0)("PRECIO_UNITARIO"), med.Rows(0)("STOCK"), 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), med.Rows(0)("PRECIO_MAYOR"), med.Rows(0)("CODIGO_BARRA"))

                                ' actualizarTabla()
                                'calcular_total()
                                'If filAnt = 999 Then
                                '    nuevoItem(0, "","", 0, 0, 0, 1, 1, 0, 0, "")
                                'End If
                                Try
                                    'Me.totalRegistro = dtSource.Rows.Count
                                    'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                                    'Me.paginaActual = 1
                                    'Me.regNro = 0
                                    LoadPage()
                                    Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                                    Me.dgvGrilla.BeginEdit(True)
                                    calcular_total()

                                Catch ex As Exception

                                End Try

                            Else
                                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                            End If
                        End If
                    End If
                Case "registro"
                    If e.RowIndex >= 0 Then
                        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value) Then
                            Dim med As DataTable = obtenerMedicamento((CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value), 1)
                            If med.Rows.Count > 0 Then
                                'nuevoItem(, , , , , 1, 1, med.Rows(0)("CANTIDAD_MAYOR"), )
                                Dim nombre As String ' NOMBRE = MEDICAMENTO x CONCENTRACION - MARCA ( LABORATORIO )
                                nombre = med.Rows(0)("MEDICAMENTO")
                                nombre += " (" + med.Rows(0)("LABORATORIO") + ")"
                                Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value = med.Rows(0)("ID_MEDICAMENTO")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("codigo"), DataGridViewTextBoxCell).Value = med.Rows(0)("CODIGO_BARRA")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("stock"), DataGridViewTextBoxCell).Value = med.Rows(0)("STOCK")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value = nombre
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = 1
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_UNITARIO")
                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("PRECIO_MAYOR")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad_mayor"), DataGridViewTextBoxCell).Value = med.Rows(0)("CANTIDAD_MAYOR")

                                actualizarTabla()
                                calcular_total()
                                If filAnt = 999 Then
                                    nuevoItem(0, "", "", 0, 0, 0, 1, 1, 0, 0, "")
                                End If
                                Try
                                    LoadPage()
                                    Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                                    Me.dgvGrilla.BeginEdit(True)
                                Catch ex As Exception

                                End Try

                            Else
                                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                            End If
                        End If
                    End If
                Case "cantidad"
                    Try
                        If dgvGrilla.EndEdit Then
                            actualizarTabla()
                            calcular_total()
                            LoadPage()
                        End If
                    Catch ex As Exception

                    End Try
                Case "nombre"
                    If CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value.ToString <> "" Then
                        actualizarTabla()
                        Dim tipo As Integer = 2
                        Using nuevo_pedido = New form_buscar_medicamento(2, CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nombre"), DataGridViewTextBoxCell).Value.ToString, False)
                            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                                'Me.totalRegistro = dtSource.Rows.Count
                                'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                                'Me.paginaActual = 1
                                'Me.regNro = 0
                                LoadPage()
                                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                                Me.dgvGrilla.BeginEdit(True)
                                calcular_total()
                            End If
                        End Using
                    End If
                Case "generico"
                    If CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("generico"), DataGridViewTextBoxCell).Value.ToString <> "" Then
                        actualizarTabla()
                        Dim tipo As Integer = 2
                        Using nuevo_pedido = New form_buscar_medicamento(2, CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("generico"), DataGridViewTextBoxCell).Value.ToString, True)
                            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                                'Me.totalRegistro = dtSource.Rows.Count
                                'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                                'Me.paginaActual = 1
                                'Me.regNro = 0
                                LoadPage()
                                Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                                Me.dgvGrilla.BeginEdit(True)
                                calcular_total()
                            End If
                        End Using
                    End If
            End Select
        End If
    End Sub

    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "CODIGO_BARRA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE_GENERICO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "STOCK"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_UNITARIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "TOTAL"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "CANTIDAD_MAYOR"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "PRECIO_MAYOR"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "PRECIO_MAYOR_VW"
        tblItems.Columns.Add(columna)

        dtSource = tblItems
    End Sub

    Public Shared Sub nuevoItem(ByVal idMedicamento As Integer, ByVal nombre As String, ByVal nombreGenerico As String, ByVal cantidad As Integer, ByVal precioUnitario As Decimal, ByVal stock As Integer, ByVal nuevo As Integer, _
                                ByVal accion As Integer, ByVal cant_mayor As Decimal, ByVal prec_mayor As Decimal, ByVal codBarra As String)
        Dim tblItems As DataTable = dtSource
        'Dim foundRows() As DataRow
        'foundRows = tblItems.Select("NRO_FILA = MAX(NRO_FILA) ")
        Dim dr As DataRow
        'If foundRows.Length = 0 Then
        dr = tblItems.NewRow

        dr("NRO_FILA") = IIf(idMedicamento = 0, 999, dtSource.Rows.Count)
        dr("ID_MEDICAMENTO") = IIf(idMedicamento = 0, DBNull.Value, idMedicamento)
        dr("STOCK") = IIf(idMedicamento = 0, DBNull.Value, stock)
        dr("NOMBRE") = nombre
        dr("NOMBRE_GENERICO") = nombreGenerico
        dr("CANTIDAD") = IIf(idMedicamento = 0, DBNull.Value, cantidad)
        dr("PRECIO_UNITARIO") = IIf(idMedicamento = 0, DBNull.Value, precioUnitario)
        Dim total As Decimal = 0

        If cant_mayor > 1 Then
            If cantidad >= cant_mayor Then
                Dim paq As Decimal = (cantidad \ cant_mayor)
                Dim unid As Decimal = cantidad - (cant_mayor * paq)
                total = (prec_mayor * paq) + (precioUnitario * unid)
            Else
                total = cantidad * precioUnitario
            End If
        Else
            total = cantidad * precioUnitario
        End If

        dr("TOTAL") = IIf(idMedicamento = 0, DBNull.Value, total)

        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        dr("CANTIDAD_MAYOR") = IIf(idMedicamento = 0, DBNull.Value, cant_mayor)
        dr("PRECIO_MAYOR") = IIf(idMedicamento = 0, DBNull.Value, prec_mayor)
        dr("PRECIO_MAYOR_VW") = IIf(idMedicamento = 0, DBNull.Value, cant_mayor.ToString + " x " + prec_mayor.ToString + " Bs.-")
        dr("CODIGO_BARRA") = IIf(idMedicamento = 0, DBNull.Value, codBarra)

        tblItems.Rows.Add(dr)
        'Else
        'dr = tblItems.NewRow
        'dr("NRO_FILA") = dtSource.Rows.Count  'IIf(idMedicamento = 0, 999, CInt(tblItems.Select("NRO_FILA = MAX(NRO_FILA)")(0)("NRO_FILA")) + 1)
        'dr("ID_MEDICAMENTO") = idMedicamento
        'dr("NOMBRE") = nombre
        'dr("CANTIDAD") = cantidad
        'dr("PRECIO_UNITARIO") = precioUnitario
        'dr("TOTAL") = CDec(cantidad) * CDec(precioUnitario)
        'dr("NUEVO") = nuevo
        'dr("ACCION") = accion
        'tblItems.Rows.Add(dr)
        'End If
        tblItems.AcceptChanges()
    End Sub

    Protected Sub actualizarTabla()
        For cont = 0 To dgvGrilla.Rows.Count - 1
            For Each tabla In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
                '.dgvGrilla.Rows(e.RowIndex).Cells("generico"),
                If CInt(tabla("NRO_FILA")) = CInt(dgvGrilla.Rows(cont).Cells("nro_fila").Value) And (dgvGrilla.Rows(cont).Cells("cantidad").Value.ToString <> "" Or dgvGrilla.Rows(cont).Cells("registro").Value.ToString <> "") Then
                    Dim nroFila As Integer = dgvGrilla.Rows(cont).Cells("nro_fila").Value
                    Dim idMedicamento As String = dgvGrilla.Rows(cont).Cells("registro").Value
                    Dim codBarra As String = dgvGrilla.Rows(cont).Cells("codigo").Value
                    Dim stock As Integer = dgvGrilla.Rows(cont).Cells("stock").Value
                    Dim cantidad As Decimal = dgvGrilla.Rows(cont).Cells("cantidad").Value

                    Dim nombre As String = dgvGrilla.Rows(cont).Cells("nombre").Value
                    Dim nombreGenerico As String = dgvGrilla.Rows(cont).Cells("generico").Value
                    Dim precio_unitario As Decimal = dgvGrilla.Rows(cont).Cells("precio_unitario").Value
                    Dim precioMayor As Decimal = dgvGrilla.Rows(cont).Cells("precio_mayor").Value
                    Dim cantidad_mayor As Decimal = dgvGrilla.Rows(cont).Cells("cantidad_mayor").Value

                    If tabla("ID_MEDICAMENTO").ToString <> idMedicamento Or tabla("CANTIDAD").ToString <> cantidad.ToString Then
                        tabla("ACCION") = 2
                        tabla("NRO_FILA") = IIf(nroFila = 999, dgvGrilla.Rows.Count, nroFila)
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("CODIGO_BARRA") = codBarra
                        tabla("NOMBRE") = nombre
                        tabla("NOMBRE_GENERICO") = nombreGenerico
                        tabla("STOCK") = stock
                        tabla("CANTIDAD") = cantidad
                        tabla("PRECIO_UNITARIO") = precio_unitario
                        tabla("CANTIDAD_MAYOR") = cantidad_mayor
                        tabla("PRECIO_MAYOR") = precioMayor
                        Dim total As Decimal = 0
                        If cantidad_mayor > 1 Then
                            If cantidad >= cantidad_mayor Then
                                Dim paq As Decimal = (cantidad \ cantidad_mayor)
                                Dim unid As Decimal = cantidad - (cantidad_mayor * paq)
                                total = (precioMayor * paq) + (precio_unitario * unid)
                            Else
                                total = cantidad * precio_unitario
                            End If
                        Else
                            total = cantidad * precio_unitario
                        End If

                        tabla("TOTAL") = total
                    End If
                End If
            Next
        Next        
    End Sub

    Protected Sub iniciarItems()
        nuevoItem(0, "", "", 0, 0, 0, 1, 1, 0, 0, "")
    End Sub

    Private Sub txtDescuento_Leave(sender As Object, e As EventArgs)
        Dim result As String = CType(sender, TextBox).Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        CType(sender, TextBox).Text = result
    End Sub


    Private Sub txtDescuento_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtPagado.Focus()
        End If
    End Sub

    Private Sub txtTotal_TextChanged(sender As Object, e As EventArgs) Handles txtTotal.TextChanged
        txtTotalDescuento_TextChanged(Nothing, Nothing)
    End Sub

    Private Sub txtDescuento_Click(sender As Object, e As EventArgs)
        If (Not String.IsNullOrEmpty(sender.Text)) Then
            sender.SelectionStart = 0
            sender.SelectionLength = sender.Text.Length
        End If
    End Sub

    Private Sub txtTarjeta_TextChanged(sender As Object, e As EventArgs) Handles txtTarjeta.TextChanged
        Dim txt As TextBox = sender
        If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            txt.Text = "" 'Limpia la caja de texto        
        End If
        calcularCambio()
    End Sub

    Private Sub txtDolares_TextChanged(sender As Object, e As EventArgs) Handles txtDolaresCambio.TextChanged
        Dim txt As TextBox = sender
        If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            txt.Text = "" 'Limpia la caja de texto  
            txtDolares.Text = "0.00"
        Else
            Dim totalDolares = IIf(Trim(txtDolaresCambio.Text.Length) = 0, "0.00", txtDolaresCambio.Text)
            txtDolares.Text = Math.Round(Decimal.Parse(totalDolares) * Decimal.Parse(form_sesion.cConfiguracion.TC_ME), 2)
        End If
        calcularCambio()
    End Sub

    Private Sub txtRegistro_TextChanged(sender As Object, e As EventArgs) Handles txtRegistro.TextChanged
        If _registroPaciente > 0 Then
            Dim sSql As String = ""
            sSql += "SELECT * FROM PACIENTE WHERE ID_PACIENTE = " + txtRegistro.Text.ToString
            Dim tbl = New datos().ejecutarConsulta(sSql)
            If tbl.Rows.Count() > 0 Then
                txtPaciente.Text = tbl.Rows(0)("NOMBRE_APELLIDOS")
            End If
        End If
    End Sub

    Private Sub btnBuscarClienteCredito_Click(sender As Object, e As EventArgs) Handles btnBuscarClienteCredito.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente(2)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                Dim tClienteCredito = New eCliente()
                tClienteCredito.ID_CLIENTE = idClienteCredito
                tClienteCredito.cargarDatos()                
                txtClienteCredito.Text = IIf(tClienteCredito.NOMBRE_FACTURA.ToString = "", tClienteCredito.PERSONA.NOMBRE_APELLIDOS, tClienteCredito.NOMBRE_FACTURA)
                chkRecibo.Checked = True
                txtNit.Text = tClienteCredito.NIT
                limiteCredito = tClienteCredito.LIMITE_CREDITO
            End If
        End Using
    End Sub

    Private Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        Dim form As form_gestionCliente = New form_gestionCliente(txtNit.Text, True)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
                Dim cliente_buscar = New eCliente()
                cliente_buscar.buscarClientePedido(txtNit.Text)
                If cliente_buscar.PERSONA.NOMBRE_APELLIDOS <> "" Then
                    tCliente = cliente_buscar
                    txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
                Else
                    txtCliente.Text = ""
                End If
            End If
        End Using
    End Sub

    Private Sub btnNuevoPaciente_Click(sender As Object, e As EventArgs) Handles btnNuevoPaciente.Click
        Dim form As form_gestionPaciente = New form_gestionPaciente(-1, True)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtRegistro.Text = _registroPaciente
            End If
        End Using
    End Sub

    Private Sub btnBuscarPaciente_Click(sender As Object, e As EventArgs) Handles btnBuscarPaciente.Click
        Dim form As form_buscar_pasciente = New form_buscar_pasciente()
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtRegistro.Text = _registroPaciente
            End If
        End Using
    End Sub

    Private Sub btnLimpiarPaciente_Click(sender As Object, e As EventArgs) Handles btnLimpiarPaciente.Click
        _registroPaciente = -1
        txtRegistro.Text = ""
        txtPaciente.Text = ""
        _idReceta = -1
        txtReceta.Text = ""
    End Sub
    

    Private Sub chkCredito_CheckedChanged(sender As Object, e As EventArgs) Handles chkCredito.CheckedChanged
        btnBuscarClienteCredito.Enabled = chkCredito.Checked
        txtImporteCredito.ReadOnly = Not chkCredito.Checked

        chkTarjeta.Checked = False
        chkTarjeta.Enabled = Not chkCredito.Checked
        txtTarjeta.Text = "0.00"
        txtNroVoucher.Text = ""
        txtTarjeta.Enabled = Not chkCredito.Checked
        txtNroVoucher.Enabled = Not chkCredito.Checked

        txtPagado.Enabled = Not chkCredito.Checked
        txtPagado.Text = "0.00"
    End Sub

    Private Sub guardarCredito(ByVal idVenta As Integer)
        Dim tblCredito = New datos()
        tblCredito.tabla = "CREDITO"
        tblCredito.campoID = "ID_CREDITO"
        tblCredito.modoID = "sql"
        tblCredito.agregarCampoValor("ID_CLIENTE", idClienteCredito)
        tblCredito.agregarCampoValor("ID_VENTA", idVenta)
        tblCredito.agregarCampoValor("IMPORTE_CREDITO", txtImporteCredito.Text)        
        tblCredito.agregarCampoValor("ESTADO", 0) ' 0 = ACTIVO; 1 = PAGADO
        tblCredito.insertar()
    End Sub

    Protected Sub procesarMovimiento(ByVal idMedicamento As Integer, ByVal cantidad As Integer, ByVal cantidad_mayor As Integer)
        Dim paq As Decimal = 0
        Dim unid As Decimal = 0

        If cantidad_mayor > 1 Then
            paq = (cantidad \ cantidad_mayor)
            unid = cantidad - (cantidad_mayor * paq)
        Else
            unid = cantidad
        End If

        Dim unidadAfectadas As Decimal = 0
        While unid > 0
            For Each kardex In tblKardex.Rows
                Dim auxStock = CInt(kardex("STOCK"))
                If CInt(kardex("STOCK")) >= unid Then
                    movimientoKardex(kardex("ID_KARDEX"), unid * (-1), _idVenta)
                    unid = 0
                    kardex("STOCK") = kardex("STOCK") - unid
                    Exit For
                Else
                    unidadAfectadas = kardex("STOCK")
                    unid = unid - unidadAfectadas
                    If unidadAfectadas > 0 Then
                        movimientoKardex(kardex("ID_KARDEX"), unidadAfectadas * (-1), _idVenta)
                        kardex("STOCK") = 0
                    End If
                End If
            Next
        End While
        While paq > 0
            For Each kardex In tblKardex.Rows
                If CInt(kardex("STOCK")) >= cantidad_mayor Then
                    movimientoKardex(kardex("ID_KARDEX"), cantidad_mayor * (-1), _idVenta)
                    paq = paq - 1
                    kardex("STOCK") = kardex("STOCK") - cantidad_mayor
                    tblKardex.AcceptChanges()
                    Exit For
                Else

                End If
            Next
        End While

    End Sub
    Protected Sub nuevoKardex(ByVal nroLote As String, ByVal f_vencimiento As String, ByVal idMedicamento As Integer, ByVal idComprobanteDetalle As Integer, ByVal cantidad As Integer)
        Dim idKardex As Integer
        idKardex = existeKardex(nroLote, idMedicamento)
        If idKardex = -1 Then
            Dim tbl = New datos
            tbl.tabla = "KARDEX"
            tbl.campoID = "ID_KARDEX"
            tbl.agregarCampoValor("NRO_LOTE", nroLote)
            tbl.agregarCampoValor("F_VENCIMIENTO", f_vencimiento)
            tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
            tbl.agregarCampoValor("CERRADO", 0)
            tbl.agregarCampoValor("ID_COMPROBANTE_DETALLE", idComprobanteDetalle)
            tbl.insertar()
            idKardex = tbl.valorID
        End If

        ' movimientoKardex(idKardex, cantidad)
    End Sub
    Protected Sub movimientoKardex(ByVal idKardex As Integer, ByVal cantidad As Integer, ByVal idVent As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.agregarCampoValor("ID_VENTA_MEDICAMENTOS", idVent)
        tbl.insertar()
    End Sub
    Private Function existeKardex(ByVal nroLote As String, ByVal idMedicamento As Integer) As Integer
        Dim sSql As String = ""
        sSql += " DECLARE @LOTE AS NVARCHAR(15) , @MEDICAMENTO AS INTEGER;"
        sSql += " SET @LOTE = '" + nroLote + "';"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT ID_KARDEX FROM KARDEX"
        sSql += " WHERE NRO_LOTE = @LOTE AND ID_MEDICAMENTO = @MEDICAMENTO  "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count = 0 Then
            Return -1
        Else
            Return tbl.Rows(0)("ID_KARDEX")
        End If
    End Function
    Protected Sub cargarKardex(ByVal idMedicamento As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT K.ID_KARDEX ,K.ID_MEDICAMENTO,M.NOMBRE , K.NRO_LOTE,K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS STOCK  FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = K.ID_MEDICAMENTO  "
        sSql += " WHERE ISNULL(K.CERRADO,0) = 0 And K.ID_MEDICAMENTO = @MEDICAMENTO"
        sSql += " GROUP BY K.ID_MEDICAMENTO, K.NRO_LOTE, K.F_VENCIMIENTO,M.NOMBRE, K.ID_KARDEX"
        sSql += " HAVING SUM(MK.CANTIDAD) > 0"
        sSql += " ORDER BY K.F_VENCIMIENTO"
        tblKardex = New datos().ejecutarConsulta(sSql)
    End Sub

    'Private Sub dgvGrilla_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs)
    '    'If e.ColumnIndex >= 0 And e.RowIndex >= 0 Then
    '    '    Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
    '    '        Case "total"
    '    '            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
    '    '            Dim celTexto As DataGridViewTextBoxCell = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("total"), DataGridViewTextBoxCell)
    '    '            Dim a = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell)
    '    '            If Not (CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value) Is Nothing And Not (CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value) Is Nothing Then
    '    '                If (CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value <> "" And CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value <> "") Then
    '    '                    celTexto.Value = Decimal.Parse(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value) * Decimal.Parse(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value)
    '    '                End If


    '    '            End If

    '    '            e.Handled = True
    '    '    End Select

    '    'End If
    'End Sub

    'Private Sub dgvGrilla_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs)
    'If dgvGrilla.Rows.Count > 0 Then
    '    Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
    '        Case "cantidad"
    '            If dgvGrilla.EndEdit Then
    '                actualizarTabla()
    '                LoadPage()
    '            End If

    '            'If e.RowIndex >= 0 Then
    '            '    Dim cantidad = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value
    '            '    Dim stock = 0 'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("stock"), DataGridViewTextBoxCell).Value
    '            '    Dim row As DataRow
    '            '    row = (From tbl In dtSource Where tbl!NRO_FILA = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value)(0)
    '            '    'If cantidad <= stock Or Not form_sesion.cConfiguracion.CONTROL_STOCK Then
    '            '    row("CANTIDAD") = cantidad
    '            '    dtSource.AcceptChanges()
    '            '    Dim celTexto As DataGridViewTextBoxCell = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("total"), DataGridViewTextBoxCell)
    '            '    celTexto.Value = Decimal.Parse(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value) * Decimal.Parse(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("precio_unitario"), DataGridViewTextBoxCell).Value)
    '            'End If
    '            'Else
    '            '   mod_conf.mensajeError("Stock Minimo", "La farmacia no cuenta con la cantidad solicitada por favor informe al administrador.")
    '            '  CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad"), DataGridViewTextBoxCell).Value = row("CANTIDAD")
    '            'End If                
    '    End Select
    '    calcular_total()
    'End If
    'End Sub

    Private Sub chkControlada_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlada.CheckedChanged
        'btnReceta.Enabled = chkControlada.Checked
        'btnBuscarReceta.Enabled = chkControlada.Checked
    End Sub

    'Private Sub btnReceta_Click(sender As Object, e As EventArgs) Handles btnReceta.Click
    '    If _registroPaciente > 0 Then
    '        Dim form As form_gestionReceta = New form_gestionReceta(_registroPaciente)
    '        Using Form
    '            If DialogResult.OK = Form.ShowDialog() Then
    '                txtReceta.Text = idReceta
    '            End If
    '        End Using
    '    Else
    '        mod_conf.mensajeError("Error Paciente", "Para Agregar una Receta debe seleccionar el paciente.")
    '    End If
    'End Sub

    Private Sub btnBuscarReceta_Click(sender As Object, e As EventArgs) Handles btnBuscarReceta.Click
        If _registroPaciente > 0 Then
            Dim form As form_buscarReceta = New form_buscarReceta(_registroPaciente)
            Using form
                If DialogResult.OK = form.ShowDialog() Then
                    txtReceta.Text = _idReceta
                End If
            End Using
        Else
            mod_conf.mensajeError("Error Paciente", "No se encuentra paciente seleccionado")
        End If
        
    End Sub

    Private Sub txtReceta_TextChanged(sender As Object, e As EventArgs) Handles txtReceta.TextChanged
        buscarReceta(txtReceta.Text)
    End Sub
    Private Sub imprimirReceta()
        Try
            PrintDocument2.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub imprimirCredito()
        Try
            PrintDocument3.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub PrintDocument2_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim print = New imprimir()
        print.imprimirRecetaMedica(e.Graphics, _idReceta)
    End Sub

    Private Sub PrintDocument3_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument3.PrintPage
        Dim print = New imprimir()
        print.imprimirVoucherCredito(e.Graphics, _idVenta)
    End Sub

    Private Sub txtTotalDescuento_TextChanged(sender As Object, e As EventArgs) Handles txtTotalDescuento.TextChanged
        'txtTotalDescuento.Text = Math.Round(pedido * (porcDesc / 100), 2).ToString
        Dim desc = Decimal.Parse(IIf(Trim(txtTotalDescuento.Text.Length) = 0, "0.00", txtTotalDescuento.Text))
        Dim pedido = Decimal.Parse(IIf(Trim(txtTotal.Text.Length) = 0, "0.00", txtTotal.Text))
        txtTotalPagar.Text = pedido - desc
        calcularCambio()
    End Sub

    'Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick

    '    Using Form = New modal_informacion_medicamento(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value)
    '        Form.ShowDialog()
    '    End Using
    'End Sub
    Protected Sub guardarCotizacion()
        Dim msg As String = ""
        Using scope = New TransactionScope
            Try
                tPedido.FECHA = Now.ToString("dd/MM/yyyy")
                tPedido.ID_CLIENTE = tCliente.ID_CLIENTE
                tPedido.ID_PERSONA_CLIENTE = tCliente.ID_PERSONA                
                tPedido.ID_USUARIO = form_sesion.cusuario.ID_USUARIO
                tPedido.NRO_VENTA = CInt(tPedido.obtenerNroPedido(3))
                tPedido.ID_TIPO_COMPROBANTE = 3
                tPedido.ID_DOSIFICACION = "null"
                tPedido.CODIGO_CONTROL = "null"
                tPedido.CODIGO_QR = "null"                
                tPedido.OBSERVACION = txtCliente.Text
                tPedido.COTIZACION = 1
                tPedido.INSERTAR()
                _idVenta = tPedido.ID_VENTA
                For Each row In dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
                    tReceta.ID_VENTA = tPedido.ID_VENTA
                    tReceta.ID_MEDICAMENTO = row("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = row("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = row("CANTIDAD")
                    tReceta.insertar()
                Next
                scope.Complete()
            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
        End Using
        'If msg = "" Then
        'Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        'mod_conf.volver_venta(formulario)
        'End If
    End Sub
    Private Function validarNombre(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If inputText.Text.Length = 0 Then
            errorProvider.SetError(inputText, "Ingrese el nombre de la receta a cotizar")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function
    Private Function validarImporte(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If CDec(inputText.Text) = 0 Then
            errorProvider.SetError(inputText, "El importe de la receta no es valido.")
            valido = False
        Else
            errorProvider.SetError(inputText, "")
        End If
        Return valido
    End Function

    Protected Sub procesarInsumos()
        Dim tblInsumo = New datos()
        Dim tblItems = dtSource
        Dim proceso As String = ""
        Dim idMedicamento As Integer = 0
        Dim sSql As String = ""
        Dim tblDatos = New datos()
        For Each item As DataRow In tblItems.Select("ACCION <> 0 AND ID_MEDICAMENTO <> ''", "NRO_FILA ASC, ACCION DESC")
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    tReceta = New eReceta()
                    tReceta.ID_VENTA = _idVenta.ToString
                    tReceta.ID_MEDICAMENTO = item("ID_MEDICAMENTO")
                    tReceta.PRECIO_UNITARIO = item("PRECIO_UNITARIO")
                    tReceta.CANTIDAD = item("CANTIDAD")
                    tReceta.insertar()
                End If
            Else
                Select Case item("ACCION")
                    Case "1"
                        sSql = "UPDATE VENTA_MEDICAMENTOS SET CANTIDAD = " + item("CANTIDAD").ToString + " WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                    Case "2"
                        sSql = "UPDATE VENTA_MEDICAMENTOS SET CANTIDAD = " + item("CANTIDAD").ToString + " WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                    Case "3"
                        sSql = "DELETE FROM VENTA_MEDICAMENTOS WHERE ID_MEDICAMENTO = " + item("ID_MEDICAMENTO").ToString + " AND ID_VENTA = " + _idVenta.ToString
                        tblDatos.ejecutarSQL(sSql)
                End Select
            End If
            tblInsumo.reset()
        Next
        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "A", "alert('" + proceso + "')", True)        
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'For cont = 0 To dgvGrilla.Rows.Count - 1
        '    If Not IsDBNull(dgvGrilla.Rows(cont).Cells("total").Value) Then
        '        Dim cantidad = dgvGrilla.Rows(cont).Cells("cantidad").Value
        '        Dim precio = dgvGrilla.Rows(cont).Cells("precio_unitario").Value
        '        Dim subTotal = dgvGrilla.Rows(cont).Cells("total").Value


        '    End If
        'Next

        If validarNombre(txtCliente, ErrorProvider1) And validarImporte(txtTotal, ErrorProvider1) Then
            If _idVenta = -1 Then
                guardarCotizacion()
                btnVolver_Click(Nothing, Nothing)
                Me.Close()
            Else
                Using scope = New TransactionScope
                    Dim datos = New datos()
                    datos.tabla = "VENTA"
                    datos.campoID = "ID_VENTA"
                    datos.valorID = _idVenta
                    datos.agregarCampoValor("OBSERVACION", txtCliente.Text)

                    datos.modificar()
                    procesarInsumos()
                    scope.Complete()
                End Using
                btnVolver_Click(Nothing, Nothing)
                Me.Close()
            End If
        End If
    End Sub

    'Private Sub dgvGrilla_SelectionChanged(sender As Object, e As EventArgs) Handles dgvGrilla.SelectionChanged
    '    calcular_total()

    'End Sub

    Private Sub dgvGrilla_SelectionChanged(sender As Object, e As EventArgs) Handles dgvGrilla.SelectionChanged
        calcularSubTotal()
        calcular_total()        
    End Sub

    'Private Sub dgvGrilla_CellStateChanged(sender As Object, e As DataGridViewCellStateChangedEventArgs) Handles dgvGrilla.CellStateChanged
    '    actualizarTabla()
    '    calcular_total()

    'End Sub


    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chkTarjeta.CheckedChanged
        txtTarjeta.Enabled = chkTarjeta.Checked
        txtNroVoucher.Enabled = chkTarjeta.Checked
        txtPagado.Text = "0.00"
        txtPagado.Enabled = Not chkTarjeta.Checked

        txtImporteCredito.Text = "0.00"
        chkCredito.Checked = False
        chkCredito.Enabled = Not chkTarjeta.Checked

    End Sub

    Private Sub txtNroVoucher_Click(sender As Object, e As EventArgs) Handles txtNroVoucher.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtNroVoucher_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNroVoucher.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub chkDescuento_CheckedChanged(sender As Object, e As EventArgs) Handles chkDescuento.CheckedChanged

        txtTotalDescuento.Text = "0.00"        
        txtTotalDescuento.Enabled = chkDescuento.Checked
    End Sub

    Private Sub dgvGrilla_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellValueChanged

    End Sub
End Class
