﻿Imports CAPA_DATOS
Public Class lote_vencimiento
    Private dtSource As DataTable
    Private id_medicamento
    Public Sub New(ByVal _id_medicamento)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        id_medicamento = _id_medicamento
    End Sub
    Protected Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER;"
        sSql += " SET @MEDICAMENTO = " + id_medicamento.ToString + ";"
        sSql += " SELECT MEDICAMENTO, FORMA_FARMACEUTICA, LABORATORIO FROM VW_MEDICAMENTO"
        sSql += " WHERE ID_MEDICAMENTO = @MEDICAMENTO"
        sSql += " SELECT K.ID_KARDEX,K.NRO_LOTE, K.F_VENCIMIENTO, SUM(MK.CANTIDAD) AS SALDO, 0 AS CANT_SOL  FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.ID_MEDICAMENTO = @MEDICAMENTO"
        sSql += " GROUP BY K.ID_KARDEX,K.NRO_LOTE, K.F_VENCIMIENTO"
        sSql += " HAVING  SUM(MK.CANTIDAD) > 0"
        Dim ds = New datos().obtenerDataSet(sSql)
        dtSource = ds.Tables(1)
        txtMedicamento.Text = ds.Tables(0).Rows(0)("MEDICAMENTO")
        txtTipo.Text = ds.Tables(0).Rows(0)("FORMA_FARMACEUTICA")
        txtLaboratorio.Text = ds.Tables(0).Rows(0)("LABORATORIO")
        inicializarParametros()
    End Sub

    Private Sub inicializarParametros()        
        With (dgvGrilla)

            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim lote As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(0, lote)
            With lote
                .Name = "lote"
                .HeaderText = "Nro. Lote"
                .DataPropertyName = "NRO_LOTE"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .ReadOnly = True
            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(1, fecha)
            With fecha
                .Name = "vencimiento"
                .HeaderText = "Fecha Vencimiento"
                .DataPropertyName = "F_VENCIMIENTO"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .ReadOnly = True
            End With

           
            Dim cantidad As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(2, cantidad)
            With cantidad
                .Name = "cantidad"
                .HeaderText = "Stock Actual"
                .DataPropertyName = "SALDO"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .ReadOnly = False
            End With


            Dim total As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(3, total)
            With total
                .Name = "total"
                .HeaderText = "Cant. Sol."
                .DataPropertyName = "CANT_SOL"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .ReadOnly = False
            End With

             Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Insert(4, colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID_KARDEX"
            End With

            .Refresh()

        End With
        dgvGrilla.DataSource = dtSource        
    End Sub

    Private Sub lote_vencimiento_Load(sender As Object, e As EventArgs) Handles Me.Load
        cargarDatos()
    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "total"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If

                'Case "cantidad"
                '    If dgvGrilla.EndEdit Then
                '        dgvGrilla.BeginEdit(True)
                '    End If
        End Select
    End Sub

    Private Sub dgvGrilla_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellEndEdit
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "total"
                calcular_total()
        End Select

    End Sub
    Private Sub calcular_total()
        Dim total As Decimal = 0
        For cont = 0 To dgvGrilla.Rows.Count - 1
            If Not IsDBNull(dgvGrilla.Rows(cont).Cells("total").Value) Then
                total = total + Decimal.Parse(dgvGrilla.Rows(cont).Cells("total").Value)
            End If
        Next
        txtCantidad.Text = String.Format("{0:#0.00}", total)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim lote As String = ""
        Dim registro As String = ""
        For cont = 0 To dgvGrilla.Rows.Count - 1
            If CInt(dgvGrilla.Rows(cont).Cells("total").Value) > 0 Then
                lote += dgvGrilla.Rows(cont).Cells("lote").Value + " - " + dgvGrilla.Rows(cont).Cells("vencimiento").Value + " - (" + dgvGrilla.Rows(cont).Cells("total").Value.ToString + ")*"
                registro += dgvGrilla.Rows(cont).Cells("registro").Value.ToString + "*"
            End If
        Next

        pedido_mayor.cantidad = CInt(txtCantidad.Text)
        pedido_mayor.vencimientos = Microsoft.VisualBasic.Left(lote, lote.Length - 1)
        pedido_mayor.registros = Microsoft.VisualBasic.Left(registro, registro.Length - 1)
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class