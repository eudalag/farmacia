﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Public Class form_buscar_pasciente
    Private mod_conf As modulo = New modulo()
    Private dtSourcePrincipal As DataTable
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 13
    Private tipo As Integer

    Public Sub New(Optional _tipo As Integer = 1) ' Tipo 1: Pedido; Tipo 2: Gestion Receta 
        InitializeComponent()
        tipo = _tipo
    End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 485
            .Height = 301
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 3

            .Columns(0).Name = "registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_PACIENTE"
            .Columns(0).Width = 100

            .Columns(1).Name = "ci"
            .Columns(1).HeaderText = "Doc. de Identidad"
            .Columns(1).DataPropertyName = "CI"
            .Columns(1).Width = 135

            .Columns(2).Name = "nombre"
            .Columns(2).HeaderText = "Nombre Apellidos"
            .Columns(2).DataPropertyName = "NOMBRE_APELLIDOS"
            .Columns(2).Width = 250
        End With
        LoadPage()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub form_buscar_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtBuscar.Focus()
        cargarPasciente()
        dtSource = buscar()
        inicializarParametros()
    End Sub
    Private Sub cargarPasciente()
        Dim sSql As String = ""
        sSql += " SELECT RIGHT('000000' + CAST(ID_PACIENTE AS NVARCHAR(6)),6) AS ID_PACIENTE, NOMBRE_APELLIDOS, "
        sSql += " CI FROM PACIENTE"
        sSql += " ORDER BY NOMBRE_APELLIDOS"
        dtSourcePrincipal = New datos().ejecutarConsulta(sSql)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs)
        dtSource = buscar()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        If tipo = 1 Then
            form_pedido._registroPaciente = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            form_gestionReceta.idPaciente = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
            form_gestionReceta.paciente = CType(dgvGrilla.Rows(e.RowIndex).Cells(2), DataGridViewTextBoxCell).Value
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
       
    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        dtSource = buscar()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub
    Function buscar() As DataTable
        Dim tbl = dtSourcePrincipal.Select("NOMBRE_APELLIDOS LIKE '%" + txtBuscar.Text.ToString + "%'")
        If tbl.Count > 0 Then
            Return tbl.CopyToDataTable
        Else
            Return dtSourcePrincipal.Clone
        End If
    End Function


    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            dgvGrilla.Focus()
        End If
    End Sub

    Private Sub form_buscar_cliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub
End Class