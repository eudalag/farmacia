﻿Imports CAPA_DATOS
Public Class pacientes
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer

    Private dtSourcePaciente As DataTable
    Private paginaActualPaciente As Integer
    Private regNroPaciente As Integer
    Private totalRegistroPaciente As Integer
    Private totalPaginaPaciente As Integer
    Private paginacion As Integer = 10

    Private Function obtenerTabla()
        Dim sSql As String = ""
        sSql += " SELECT HC.ID_HISTORICO,CONVERT(NVARCHAR(10),FH_HISTORICO, 103) AS F_HISTORICO, P.NOMBRE_APELLIDOS, HC.CONCLUSIONES "
        sSql += " FROM HISTORICO_CLINICO HC "
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = HC.ID_PACIENTE "
        sSql += " ORDER BY ID_HISTORICO DESC"

        Return New datos().ejecutarConsulta(sSql)
    End Function

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1100
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Historico"
            .Columns(0).DataPropertyName = "ID_HISTORICO"
            .Columns(0).Width = 100

            .Columns(1).Name = "Fecha"
            .Columns(1).HeaderText = "Fecha"
            .Columns(1).DataPropertyName = "F_HISTORICO"
            .Columns(1).Width = 100


            .Columns(2).Name = "paciente"
            .Columns(2).HeaderText = "Nombre(s) y Apellido(s)"
            .Columns(2).DataPropertyName = "NOMBRE_APELLIDOS"
            .Columns(2).Width = 400

            .Columns(3).Name = "Conclusion"
            .Columns(3).HeaderText = "Evaluacion Medica"
            .Columns(3).DataPropertyName = "CONCLUSIONES"
            .Columns(3).Width = 500

        End With
        LoadPage()
    End Sub

    Private Sub LoadPagePaciente()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSourcePaciente.Clone
        If dtSourcePaciente.Rows.Count > 0 Then
            If Me.paginaActualPaciente = Me.totalPaginaPaciente Then
                endRec = dtSourcePaciente.Rows.Count
            Else
                endRec = Me.paginaActualPaciente * (Me.paginacion)
            End If
            startRec = regNroPaciente
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSourcePaciente.Rows(i))
                regNroPaciente += 1
            Next
        End If
        dgvGrillaPaciente.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguientePaciente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguientePaciente.Click
        Me.paginaActualPaciente += 1
        If (Me.paginaActualPaciente > (Me.totalPaginaPaciente)) Then
            Me.paginaActualPaciente = Me.totalPaginaPaciente
        End If
        regNro = (paginaActualPaciente - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnteriorPaciente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbnAnteriorPaciente.Click
        Me.paginaActualPaciente -= 1
        If (Me.paginaActualPaciente = 0) Then
            Me.paginaActualPaciente = 1
        End If
        regNro = (paginaActualPaciente - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinalPaciente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoPaciente.Click
        Me.paginaActualPaciente = Me.totalPaginaPaciente
        regNro = (paginaActualPaciente - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimeroPaciente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalPaciente.Click
        Me.paginaActualPaciente = 1
        regNro = (paginaActualPaciente - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametrosPaciente()       
        With (dgvGrillaPaciente)
            '831| 360
            .Width = 1100
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 6
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 400


            .Columns(1).Name = "presentacion"
            .Columns(1).HeaderText = "Presentacion"
            .Columns(1).DataPropertyName = "FORMA_FARMACEUTICA"
            .Columns(1).Width = 100

            .Columns(2).Name = "cantidad"
            .Columns(2).HeaderText = "Cant. Rec."
            .Columns(2).DataPropertyName = "CANTIDAD"
            .Columns(2).Width = 80
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns(3).Name = "precioUnitario"
            .Columns(3).HeaderText = "Precio"
            .Columns(3).DataPropertyName = "PRECIO_UNITARIO"
            .Columns(3).Width = 80
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns(4).Name = "total"
            .Columns(4).HeaderText = "Total"
            .Columns(4).DataPropertyName = "TOTAL"
            .Columns(4).Width = 80
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns(5).Name = "indicaciones"
            .Columns(5).HeaderText = "Indicaciones"
            .Columns(5).DataPropertyName = "INDICACIONES"
            .Columns(5).Width = 460

        End With
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        TabControl1.SelectedIndex = 1
    End Sub

    Private Sub form_receta_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        dtSource = obtenerTabla()
        inicializarParametros()
        inicializarParametrosPaciente()
        txtBuscar.Focus()
    End Sub
    Private Sub cargarDatosPaciente(ByVal idHistorico As String)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_HISTORICO AS INTEGER "
        sSql += " SET @ID_HISTORICO  = " + idHistorico + ";"
        sSql += " SELECT M.MEDICAMENTO +' (' + M.LABORATORIO + ')' AS MEDICAMENTO , M.FORMA_FARMACEUTICA, M.PRECIO_UNITARIO,RD.CANTIDAD, M.PRECIO_UNITARIO * RD.CANTIDAD AS TOTAL,RD.INDICACIONES FROM RECETA R "
        sSql += " INNER JOIN RECETA_DETALLE RD ON R.ID_RECETA = RD.ID_RECETA "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = RD.ID_MEDICAMENTO "
        sSql += " WHERE R.ID_HISTORICO = @ID_HISTORICO "
        dtSourcePaciente = New datos().ejecutarConsulta(sSql)
    End Sub
    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim tbcontrol = CType(sender, TabControl)
        If tbcontrol.SelectedIndex = 1 Then
            If dgvGrilla.RowCount > 0 Then
                Dim idHistorico = dgvGrilla.SelectedCells(0).Value
                txtPaciente.Text = dgvGrilla.SelectedCells(2).Value
                txtHistorico.Text = dgvGrilla.SelectedCells(0).Value
                txtFecha.Text = dgvGrilla.SelectedCells(1).Value
                cargarDatosPaciente(idHistorico)
                Me.totalRegistroPaciente = dtSourcePaciente.Rows.Count
                Me.totalPaginaPaciente = Math.Ceiling(Me.totalRegistroPaciente / Me.paginacion)
                Me.paginaActualPaciente = 1
                Me.regNroPaciente = 0
                LoadPagePaciente()
            End If
        End If
    End Sub

    Private Sub dgvGrillaPaciente_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrillaPaciente.CellDoubleClick
        'Dim idVenta = CType(dgvGrillaPaciente.Rows(e.RowIndex).Cells(3), DataGridViewTextBoxCell).Value
        'Dim formDetalle As detalle_tratamiento = New detalle_tratamiento(idVenta)
        'formDetalle.ShowDialog()
    End Sub


    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        imprimirReceta()
    End Sub

    Private Sub btnVenta_Click(sender As Object, e As EventArgs) Handles btnVenta.Click
        Dim venta = New form_pedido(dgvGrilla.SelectedCells(0).Value, 1)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, venta)
        Me.Close()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirRecetaMedica(e.Graphics, dgvGrilla.SelectedCells(0).Value)
    End Sub

    Private Sub imprimirReceta()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
  
End Class