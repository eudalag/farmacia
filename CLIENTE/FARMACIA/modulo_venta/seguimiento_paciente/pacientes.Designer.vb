﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pacientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(pacientes))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtDisplayPageNo = New System.Windows.Forms.TextBox()
        Me.btnFinal = New System.Windows.Forms.Button()
        Me.btnSiguiente = New System.Windows.Forms.Button()
        Me.btnAnterior = New System.Windows.Forms.Button()
        Me.btnPrimero = New System.Windows.Forms.Button()
        Me.dgvGrilla = New System.Windows.Forms.DataGridView()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnVenta = New System.Windows.Forms.Button()
        Me.txtFecha = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.txtDisplayPageNoPaciente = New System.Windows.Forms.TextBox()
        Me.btnUltimoPaciente = New System.Windows.Forms.Button()
        Me.btnSiguientePaciente = New System.Windows.Forms.Button()
        Me.tbnAnteriorPaciente = New System.Windows.Forms.Button()
        Me.btnFinalPaciente = New System.Windows.Forms.Button()
        Me.dgvGrillaPaciente = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtHistorico = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.Panel2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvGrilla, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvGrillaPaciente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Green
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1127, 31)
        Me.Panel2.TabIndex = 122
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(2, 2)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(309, 26)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Seguimientos de Pacientes:"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(0, 32)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1140, 682)
        Me.TabControl1.TabIndex = 123
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtDisplayPageNo)
        Me.TabPage1.Controls.Add(Me.btnFinal)
        Me.TabPage1.Controls.Add(Me.btnSiguiente)
        Me.TabPage1.Controls.Add(Me.btnAnterior)
        Me.TabPage1.Controls.Add(Me.btnPrimero)
        Me.TabPage1.Controls.Add(Me.dgvGrilla)
        Me.TabPage1.Controls.Add(Me.txtBuscar)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1132, 656)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Historico Clinico"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtDisplayPageNo
        '
        Me.txtDisplayPageNo.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtDisplayPageNo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDisplayPageNo.Location = New System.Drawing.Point(493, 572)
        Me.txtDisplayPageNo.Name = "txtDisplayPageNo"
        Me.txtDisplayPageNo.ReadOnly = True
        Me.txtDisplayPageNo.Size = New System.Drawing.Size(140, 13)
        Me.txtDisplayPageNo.TabIndex = 137
        Me.txtDisplayPageNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnFinal
        '
        Me.btnFinal.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinal.Image = CType(resources.GetObject("btnFinal.Image"), System.Drawing.Image)
        Me.btnFinal.Location = New System.Drawing.Point(666, 566)
        Me.btnFinal.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFinal.Name = "btnFinal"
        Me.btnFinal.Size = New System.Drawing.Size(25, 25)
        Me.btnFinal.TabIndex = 136
        Me.btnFinal.UseVisualStyleBackColor = True
        '
        'btnSiguiente
        '
        Me.btnSiguiente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSiguiente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSiguiente.Image = CType(resources.GetObject("btnSiguiente.Image"), System.Drawing.Image)
        Me.btnSiguiente.Location = New System.Drawing.Point(636, 566)
        Me.btnSiguiente.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSiguiente.Name = "btnSiguiente"
        Me.btnSiguiente.Size = New System.Drawing.Size(25, 25)
        Me.btnSiguiente.TabIndex = 135
        Me.btnSiguiente.UseVisualStyleBackColor = True
        '
        'btnAnterior
        '
        Me.btnAnterior.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAnterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnterior.Image = CType(resources.GetObject("btnAnterior.Image"), System.Drawing.Image)
        Me.btnAnterior.Location = New System.Drawing.Point(462, 566)
        Me.btnAnterior.Margin = New System.Windows.Forms.Padding(0)
        Me.btnAnterior.Name = "btnAnterior"
        Me.btnAnterior.Size = New System.Drawing.Size(25, 25)
        Me.btnAnterior.TabIndex = 134
        Me.btnAnterior.UseVisualStyleBackColor = True
        '
        'btnPrimero
        '
        Me.btnPrimero.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnPrimero.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPrimero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrimero.Image = CType(resources.GetObject("btnPrimero.Image"), System.Drawing.Image)
        Me.btnPrimero.Location = New System.Drawing.Point(435, 566)
        Me.btnPrimero.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPrimero.Name = "btnPrimero"
        Me.btnPrimero.Size = New System.Drawing.Size(25, 25)
        Me.btnPrimero.TabIndex = 133
        Me.btnPrimero.UseVisualStyleBackColor = True
        '
        'dgvGrilla
        '
        Me.dgvGrilla.AllowUserToAddRows = False
        Me.dgvGrilla.AllowUserToDeleteRows = False
        Me.dgvGrilla.AllowUserToResizeColumns = False
        Me.dgvGrilla.AllowUserToResizeRows = False
        Me.dgvGrilla.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvGrilla.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGrilla.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Green
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGrilla.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvGrilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGrilla.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvGrilla.EnableHeadersVisualStyles = False
        Me.dgvGrilla.Location = New System.Drawing.Point(11, 46)
        Me.dgvGrilla.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.dgvGrilla.MultiSelect = False
        Me.dgvGrilla.Name = "dgvGrilla"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Green
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGrilla.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvGrilla.RowHeadersVisible = False
        Me.dgvGrilla.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvGrilla.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGrilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGrilla.ShowCellErrors = False
        Me.dgvGrilla.ShowCellToolTips = False
        Me.dgvGrilla.ShowEditingIcon = False
        Me.dgvGrilla.ShowRowErrors = False
        Me.dgvGrilla.Size = New System.Drawing.Size(1100, 500)
        Me.dgvGrilla.StandardTab = True
        Me.dgvGrilla.TabIndex = 132
        Me.dgvGrilla.TabStop = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.Location = New System.Drawing.Point(58, 14)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(253, 20)
        Me.txtBuscar.TabIndex = 130
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 16)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 131
        Me.Label6.Text = "Buscar:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnVenta)
        Me.TabPage2.Controls.Add(Me.txtFecha)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.btnImprimir)
        Me.TabPage2.Controls.Add(Me.txtDisplayPageNoPaciente)
        Me.TabPage2.Controls.Add(Me.btnUltimoPaciente)
        Me.TabPage2.Controls.Add(Me.btnSiguientePaciente)
        Me.TabPage2.Controls.Add(Me.tbnAnteriorPaciente)
        Me.TabPage2.Controls.Add(Me.btnFinalPaciente)
        Me.TabPage2.Controls.Add(Me.dgvGrillaPaciente)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.txtHistorico)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.txtPaciente)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1132, 656)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Recetas por Paciente"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnVenta
        '
        Me.btnVenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVenta.Image = Global.FARMACIA.My.Resources.Resources.Apertura
        Me.btnVenta.Location = New System.Drawing.Point(844, 56)
        Me.btnVenta.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnVenta.Name = "btnVenta"
        Me.btnVenta.Size = New System.Drawing.Size(130, 37)
        Me.btnVenta.TabIndex = 159
        Me.btnVenta.Text = "Venta de Receta"
        Me.btnVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVenta.UseVisualStyleBackColor = True
        '
        'txtFecha
        '
        Me.txtFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFecha.Location = New System.Drawing.Point(271, 39)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(82, 20)
        Me.txtFecha.TabIndex = 158
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(184, 41)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 157
        Me.Label4.Text = "Fecha Historico:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Image = Global.FARMACIA.My.Resources.Resources.printer
        Me.btnImprimir.Location = New System.Drawing.Point(992, 56)
        Me.btnImprimir.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(116, 38)
        Me.btnImprimir.TabIndex = 156
        Me.btnImprimir.Text = "Imprimir Receta"
        Me.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'txtDisplayPageNoPaciente
        '
        Me.txtDisplayPageNoPaciente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtDisplayPageNoPaciente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDisplayPageNoPaciente.Location = New System.Drawing.Point(352, 632)
        Me.txtDisplayPageNoPaciente.Name = "txtDisplayPageNoPaciente"
        Me.txtDisplayPageNoPaciente.ReadOnly = True
        Me.txtDisplayPageNoPaciente.Size = New System.Drawing.Size(140, 13)
        Me.txtDisplayPageNoPaciente.TabIndex = 142
        Me.txtDisplayPageNoPaciente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnUltimoPaciente
        '
        Me.btnUltimoPaciente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnUltimoPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUltimoPaciente.Image = CType(resources.GetObject("btnUltimoPaciente.Image"), System.Drawing.Image)
        Me.btnUltimoPaciente.Location = New System.Drawing.Point(525, 626)
        Me.btnUltimoPaciente.Margin = New System.Windows.Forms.Padding(0)
        Me.btnUltimoPaciente.Name = "btnUltimoPaciente"
        Me.btnUltimoPaciente.Size = New System.Drawing.Size(25, 25)
        Me.btnUltimoPaciente.TabIndex = 141
        Me.btnUltimoPaciente.UseVisualStyleBackColor = True
        '
        'btnSiguientePaciente
        '
        Me.btnSiguientePaciente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSiguientePaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSiguientePaciente.Image = CType(resources.GetObject("btnSiguientePaciente.Image"), System.Drawing.Image)
        Me.btnSiguientePaciente.Location = New System.Drawing.Point(495, 626)
        Me.btnSiguientePaciente.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSiguientePaciente.Name = "btnSiguientePaciente"
        Me.btnSiguientePaciente.Size = New System.Drawing.Size(25, 25)
        Me.btnSiguientePaciente.TabIndex = 140
        Me.btnSiguientePaciente.UseVisualStyleBackColor = True
        '
        'tbnAnteriorPaciente
        '
        Me.tbnAnteriorPaciente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tbnAnteriorPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnAnteriorPaciente.Image = CType(resources.GetObject("tbnAnteriorPaciente.Image"), System.Drawing.Image)
        Me.tbnAnteriorPaciente.Location = New System.Drawing.Point(321, 626)
        Me.tbnAnteriorPaciente.Margin = New System.Windows.Forms.Padding(0)
        Me.tbnAnteriorPaciente.Name = "tbnAnteriorPaciente"
        Me.tbnAnteriorPaciente.Size = New System.Drawing.Size(25, 25)
        Me.tbnAnteriorPaciente.TabIndex = 139
        Me.tbnAnteriorPaciente.UseVisualStyleBackColor = True
        '
        'btnFinalPaciente
        '
        Me.btnFinalPaciente.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFinalPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnFinalPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalPaciente.Image = CType(resources.GetObject("btnFinalPaciente.Image"), System.Drawing.Image)
        Me.btnFinalPaciente.Location = New System.Drawing.Point(294, 626)
        Me.btnFinalPaciente.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFinalPaciente.Name = "btnFinalPaciente"
        Me.btnFinalPaciente.Size = New System.Drawing.Size(25, 25)
        Me.btnFinalPaciente.TabIndex = 138
        Me.btnFinalPaciente.UseVisualStyleBackColor = True
        '
        'dgvGrillaPaciente
        '
        Me.dgvGrillaPaciente.AllowUserToAddRows = False
        Me.dgvGrillaPaciente.AllowUserToDeleteRows = False
        Me.dgvGrillaPaciente.AllowUserToResizeColumns = False
        Me.dgvGrillaPaciente.AllowUserToResizeRows = False
        Me.dgvGrillaPaciente.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvGrillaPaciente.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvGrillaPaciente.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Green
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGrillaPaciente.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvGrillaPaciente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGrillaPaciente.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvGrillaPaciente.EnableHeadersVisualStyles = False
        Me.dgvGrillaPaciente.Location = New System.Drawing.Point(7, 101)
        Me.dgvGrillaPaciente.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.dgvGrillaPaciente.MultiSelect = False
        Me.dgvGrillaPaciente.Name = "dgvGrillaPaciente"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Green
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Padding = New System.Windows.Forms.Padding(3)
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGrillaPaciente.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvGrillaPaciente.RowHeadersVisible = False
        Me.dgvGrillaPaciente.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvGrillaPaciente.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvGrillaPaciente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGrillaPaciente.ShowCellErrors = False
        Me.dgvGrillaPaciente.ShowCellToolTips = False
        Me.dgvGrillaPaciente.ShowEditingIcon = False
        Me.dgvGrillaPaciente.ShowRowErrors = False
        Me.dgvGrillaPaciente.Size = New System.Drawing.Size(1100, 478)
        Me.dgvGrillaPaciente.StandardTab = True
        Me.dgvGrillaPaciente.TabIndex = 137
        Me.dgvGrillaPaciente.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 79)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(107, 13)
        Me.Label3.TabIndex = 136
        Me.Label3.Text = "Detalle de Consultas:"
        '
        'txtHistorico
        '
        Me.txtHistorico.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHistorico.Location = New System.Drawing.Point(84, 39)
        Me.txtHistorico.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtHistorico.Name = "txtHistorico"
        Me.txtHistorico.Size = New System.Drawing.Size(82, 20)
        Me.txtHistorico.TabIndex = 134
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 43)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 135
        Me.Label2.Text = "Nro. Historico:"
        '
        'txtPaciente
        '
        Me.txtPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaciente.Location = New System.Drawing.Point(62, 14)
        Me.txtPaciente.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.Size = New System.Drawing.Size(430, 20)
        Me.txtPaciente.TabIndex = 132
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 133
        Me.Label1.Text = "Paciente:"
        '
        'PrintDocument1
        '
        '
        'pacientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1124, 651)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Panel2)
        Me.Name = "pacientes"
        Me.Text = "pacientes"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvGrilla, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvGrillaPaciente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtDisplayPageNo As System.Windows.Forms.TextBox
    Friend WithEvents btnFinal As System.Windows.Forms.Button
    Friend WithEvents btnSiguiente As System.Windows.Forms.Button
    Friend WithEvents btnAnterior As System.Windows.Forms.Button
    Friend WithEvents btnPrimero As System.Windows.Forms.Button
    Friend WithEvents dgvGrilla As System.Windows.Forms.DataGridView
    Friend WithEvents txtBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtHistorico As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvGrillaPaciente As System.Windows.Forms.DataGridView
    Friend WithEvents txtDisplayPageNoPaciente As System.Windows.Forms.TextBox
    Friend WithEvents btnUltimoPaciente As System.Windows.Forms.Button
    Friend WithEvents btnSiguientePaciente As System.Windows.Forms.Button
    Friend WithEvents tbnAnteriorPaciente As System.Windows.Forms.Button
    Friend WithEvents btnFinalPaciente As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents txtFecha As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnVenta As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
