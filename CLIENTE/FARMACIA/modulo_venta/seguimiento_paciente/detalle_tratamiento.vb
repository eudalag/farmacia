﻿Imports CAPA_DATOS
Public Class detalle_tratamiento
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private tblCabecera As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private idReceta As Integer = -1

    Public Sub New(ByVal idVenta As Integer)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        idReceta = idVenta
    End Sub
    Private Sub obtenerTabla()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;
        sSql += " SET @ID_VENTA = " + idReceta.ToString + ";"

        sSql += " SELECT NOMBRE_APELLIDOS,CELULAR FROM PACIENTE P "
        sSql += " INNER JOIN VENTA V ON V.ID_PACIENTE = P.ID_PACIENTE AND V.ID_VENTA = @ID_VENTA"

        sSql += " SELECT  M.NOMBRE, FF.DESCRIPCION,SUM(VM.CANTIDAD) AS CANTIDAD  FROM VENTA V "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE V.ID_VENTA = @ID_VENTA"
        sSql += " GROUP BY M.NOMBRE, FF.DESCRIPCION"
        Dim ds = New datos().obtenerDataSet(sSql)

        tblCabecera = ds.Tables(0)
        dtSource = ds.Tables(1)
    End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 480
            .Height = 290
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 3
            .Columns(0).Name = "nombre"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "NOMBRE"
            .Columns(0).Width = 200

            .Columns(1).Name = "forma_farmaceutica"
            .Columns(1).HeaderText = "Forma Farmaceutica"
            .Columns(1).DataPropertyName = "DESCRIPCION"
            .Columns(1).Width = 160

            .Columns(2).Name = "cantidad"
            .Columns(2).HeaderText = "Cantidad"
            .Columns(2).DataPropertyName = "CANTIDAD"
            .Columns(2).Width = 120

        End With
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idPersona = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Dim formPago As pago_credito = New pago_credito(idPersona)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, formPago)
    End Sub
    Protected Sub iniciarCabecera()
        txtpaciente.Text = tblCabecera.Rows(0)("NOMBRE_APELLIDOS")
    End Sub

    Private Sub form_receta_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        obtenerTabla()
        inicializarParametros()
        iniciarCabecera()
        txtpaciente.Focus()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class