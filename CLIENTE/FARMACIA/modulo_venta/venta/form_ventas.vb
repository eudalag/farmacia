﻿Imports System.Transactions
Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Public Class form_ventas
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    'Private paginaActual As Integer
    'Private regNro As Integer
    'Private totalRegistro As Integer
    'Private totalPagina As Integer
    'Private paginacion As Integer = 20

    Dim tCompra As eVenta = New eVenta()
    'Private Sub DisplayPageInfo()
    '    txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    'End Sub
    'Private Sub LoadPage()
    '    Dim i As Integer
    '    Dim startRec As Integer
    '    Dim endRec As Integer
    '    Dim dtTemp As DataTable
    '    'Duplicate or clone the source table to create the temporary table.
    '    dtTemp = dtSource.Clone
    '    If dtSource.Rows.Count > 0 Then
    '        If Me.paginaActual = Me.totalPagina Then
    '            endRec = dtSource.Rows.Count
    '        Else
    '            endRec = Me.paginaActual * (Me.paginacion)
    '        End If
    '        startRec = regNro
    '        'Copy the rows from the source table to fill the temporary table.
    '        For i = startRec To endRec - 1
    '            dtTemp.ImportRow(dtSource.Rows(i))
    '            regNro += 1
    '        Next
    '    End If
    '    dgvGrilla.DataSource = dtTemp
    '    DisplayPageInfo()
    'End Sub
    'Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
    '    Me.paginaActual += 1
    '    If (Me.paginaActual > (Me.totalPagina)) Then
    '        Me.paginaActual = Me.totalPagina
    '    End If
    '    regNro = (paginaActual - 1) * paginacion
    '    LoadPage()
    'End Sub

    'Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
    '    Me.paginaActual -= 1
    '    If (Me.paginaActual = 0) Then
    '        Me.paginaActual = 1
    '    End If
    '    regNro = (paginaActual - 1) * paginacion
    '    LoadPage()
    'End Sub

    'Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
    '    Me.paginaActual = Me.totalPagina
    '    regNro = (paginaActual - 1) * paginacion
    '    LoadPage()
    'End Sub

    'Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
    '    Me.paginaActual = 1
    '    regNro = (paginaActual - 1) * paginacion
    '    LoadPage()
    'End Sub

    Private Sub inicializarParametros()
        'Me.totalRegistro = dtSource.Rows.Count
        'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        'Me.paginaActual = 1
        'Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100            
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False            
            .ColumnCount = 7
            .Columns(0).Name = "nro_venta"
            .Columns(0).HeaderText = "Nro. Comp."
            .Columns(0).DataPropertyName = "NRO_VENTA"
            .Columns(0).Width = 110
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(1).Name = "cliente"
            .Columns(1).HeaderText = "Descripcion"
            .Columns(1).DataPropertyName = "CLIENTE"
            .Columns(1).Width = 310
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            .Columns(2).Name = "pago"
            .Columns(2).HeaderText = "Pago"
            .Columns(2).DataPropertyName = "TIPO_PAGO"
            .Columns(2).Width = 150
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            .Columns(3).Name = "total"
            .Columns(3).HeaderText = "Sub Total"
            .Columns(3).DataPropertyName = "SUBTOTAL"
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(3).Width = 100


            .Columns(4).Visible = False
            .Columns(4).Name = "tipo_comprobante"
            .Columns(4).DataPropertyName = "TIPO_COMPROBANTE"

            .Columns(5).Visible = False
            .Columns(5).Name = "id_venta"
            .Columns(5).DataPropertyName = "ID_VENTA"

            .Columns(6).Name = "estado"
            .Columns(6).HeaderText = "Estado"
            .Columns(6).DataPropertyName = "ESTADO"
            .Columns(6).Width = 80
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Insert(7, colButton)
            With colButton
                'Titulo de la columna.
                .HeaderText = "Ver"
                'Nombre para acceder al contenido por codigo.
                .Name = "colButton"
                'Ancho
                .Width = 50
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 7
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False

                .Text = "Ver"
                .UseColumnTextForButtonValue = True

            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fecha)
            With fecha
                .Name = "Fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "FECHA"
                .Width = 120
                .DisplayIndex = 1
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim ingreso As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(ingreso)
            With ingreso
                .Name = "ingreso"
                .HeaderText = "Ingreso"
                .DataPropertyName = "INGRESO"
                .Width = 80
                .DisplayIndex = 4
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim egreso As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(egreso)
            With egreso
                .Name = "egreso"
                .HeaderText = "Egreso"
                .DataPropertyName = "EGRESO"
                .Width = 80
                .DisplayIndex = 5
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With
        End With
        'LoadPage()
    End Sub

    Private Sub form_ventas_Load(sender As Object, e As EventArgs) Handles Me.Load
        'iniciarCombo()        
        cmbMonto.SelectedIndex = 0
        cmbPago.SelectedIndex = 0
        cmbTipoMovimiento.SelectedIndex = 0
        iniciarCombo()
        cmbCaja.SelectedValue = variableSesion.idBanco
        If variableSesion.administrador Then
            cmbCaja.Enabled = True
        Else
            cmbCaja.Enabled = False
        End If
        dtSource = tCompra.mostrarVentas(txtFecha.Text, txtHasta.Text, -1, 0, -1, "", cmbCaja.SelectedValue, -1, "")
        If dtSource.Rows.Count > 0 Then
            txtSaldoAnterior.Text = dtSource.Rows(0)("SALDO_ANTERIOR")
        Else
            txtSaldoAnterior.Text = "0.00"
        End If

        inicializarParametros()
        dgvGrilla.DataSource = dtSource
        form_principal_venta._fecha = txtFecha.Text
        'form_principal_venta.obtenerTotalCaja()

        
    End Sub


    Protected Sub iniciarCombo()
        Dim tPedido As eVenta = New eVenta()
        Dim sSql As String = ""
        sSql += " SELECT B.ID_BANCO, B.ENTIDAD_FINANCIERA + ' [' + U.USUARIO + ']' AS CAJA FROM USUARIO U"
        sSql += " INNER JOIN BANCO B ON B.ID_USUARIO = U.ID_USUARIO"
        sSql += " INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += " WHERE TB.ID_TIPO_BANCO = 1"
        sSql += " UNION ALL"
        sSql += " SELECT -1, '[Todas las Cajas]'"
        sSql += " ORDER BY CAJA"
        Dim tbl = New datos().ejecutarConsulta(sSql)
        cmbCaja.DataSource = tbl
        cmbCaja.DisplayMember = "CAJA"
        cmbCaja.ValueMember = "ID_BANCO"
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim tipoMonto As Integer = -1
        Select Case cmbMonto.SelectedItem
            Case "Menor"
                tipoMonto = 1
            Case "Menor Igual"
                tipoMonto = 2
            Case "Igual"
                tipoMonto = 3
            Case "Mayor Igual"
                tipoMonto = 4
            Case "Mayor"
                tipoMonto = 5
        End Select

        Dim tipoPago As Integer = -1
        Select Case cmbPago.SelectedItem
            Case "Contado"
                tipoPago = 1
            Case "Credito"
                tipoPago = 2
        End Select

        Dim tipoMovimiento As Integer = -1
        Select Case cmbPago.SelectedItem
            Case "Ventas"
                tipoMovimiento = 1
            Case "Transferencias"
                tipoMovimiento = 2
            Case "Otros Movimientos"
                tipoMovimiento = 3
        End Select

        dtSource = tCompra.mostrarVentas(txtFecha.Text, txtHasta.Text, tipoMonto, IIf(IsNumeric(txtMonto.Text), txtMonto.Text, 0), tipoPago, txtBuscar.Text, cmbCaja.SelectedValue, tipoMovimiento, txtNroComprobante.Text)
        'Me.totalRegistro = dtSource.Rows.Count
        'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        'Me.paginaActual = 1
        'Me.regNro = 0
        'LoadPage()


        If dtSource.Rows.Count > 0 Then
            txtSaldoAnterior.Text = dtSource.Rows(0)("SALDO_ANTERIOR")
        Else
            txtSaldoAnterior.Text = "0.00"
        End If
        dgvGrilla.DataSource = dtSource        
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    'Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
    '    Dim idVenta As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(5), DataGridViewTextBoxCell).Value
    '    Dim pedido As form_pedido = New form_pedido(idVenta)
    '    mod_conf.cambiarForm(form_principal_venta.panelContenedor, pedido)
    '    Me.Close()
    'End Sub
    Private Sub actualizarKardex(ByVal idVenta As Integer)
        Dim sSql = ""
        'sSql += " INSERT INTO [dbo].[MOVIMIENTO_KARDEX]"
        'sSql += " ([ID_KARDEX],[ID_TIPO_MOVIMIENTO],[CANTIDAD],[FECHA],[ID_USUARIO],"
        'sSql += " [ID_VENTA_MEDICAMENTOS])"

        sSql += " SELECT ID_KARDEX ,  CANTIDAD * (-1) AS CANTIDAD, ID_VENTA_MEDICAMENTOS FROM MOVIMIENTO_KARDEX WHERE ID_VENTA_MEDICAMENTOS = " + idVenta.ToString
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count() > 0 Then
            Dim mov = New datos()

            For Each item In tbl.Rows
                mov.tabla = "MOVIMIENTO_KARDEX"
                mov.campoID = "ID_MOVIMIENTO_KARDEX"
                mov.modoID = "sql"
                mov.agregarCampoValor("ID_KARDEX", item("ID_KARDEX"))
                mov.agregarCampoValor("ID_TIPO_MOVIMIENTO", 8)
                mov.agregarCampoValor("CANTIDAD", item("CANTIDAD"))
                mov.agregarCampoValor("FECHA", "serverDateTime")
                mov.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO.ToString)
                mov.agregarCampoValor("ID_VENTA_MEDICAMENTOS", item("ID_VENTA_MEDICAMENTOS"))
                mov.insertar()
                mov.reset()
            Next
        End If
        Dim tblConsulta = New datos()
        tblConsulta.ejecutarSQL(sSql)
    End Sub

    Private Sub insertarMovimiento(ByVal idVenta As Integer, ByVal nroRecibo As Integer)
        Dim sSql = ""
        sSql += " DECLARE @TOTAL_PEDIDO AS NUMERIC(18,2), @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + idVenta.ToString + ";"
        sSql += " SELECT @TOTAL_PEDIDO = TOTAL_PEDIDO FROM VENTA WHERE ID_VENTA = @ID_VENTA; "
        sSql += " INSERT INTO [dbo].[MOVIMIENTO] ([ID_CAJA],[ID_TIPO_MOVIMIENTO],[IMPORTE],[GLOSA])"
        sSql += " VALUES("
        sSql += form_principal_venta.lblIdCaja.Text
        sSql += " ,2"
        sSql += " ,@TOTAL_PEDIDO * -1"
        sSql += " ,'Por devolucion de medicamentos recibo / factura nro: " + nroRecibo.ToString + "')"
        Dim tblCons = New datos()
        tblCons.ejecutarSQL(sSql)
    End Sub
    Private Sub anularComprobante(ByVal idVenta As Integer)
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "VENTA"
        tblConsulta.campoID = "ID_VENTA"
        tblConsulta.valorID = idVenta
        tblConsulta.agregarCampoValor("ANULADA", 1)
        tblConsulta.modificar()

    End Sub
    Private Sub anularCredito(ByVal idVenta As Integer)
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "CREDITO"
        tblConsulta.campoID = "ID_VENTA"
        tblConsulta.valorID = idVenta
        tblConsulta.agregarCampoValor("ESTADO", 1)
        tblConsulta.modificar()
    End Sub
    Private Sub anularConsulta(ByVal idConsulta)
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "CONSULTA"
        tblConsulta.campoID = "ID_CONSULTA"
        tblConsulta.valorID = idConsulta
        tblConsulta.agregarCampoValor("ANULADO", 1)
        tblConsulta.modificar()
    End Sub

    Private Sub anularMovimiento(ByVal idMovimiento)
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "MOVIMIENTO_BANCO"
        tblConsulta.campoID = "ID_MOVIMIENTO"
        tblConsulta.valorID = idMovimiento
        tblConsulta.agregarCampoValor("ANULADO", 1)
        tblConsulta.modificar()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim idVenta = dgvGrilla.SelectedCells(5).Value
        If idVenta <> -1 Then
            Using confirmacion = New formConfirmacion()
                If DialogResult.OK = confirmacion.ShowDialog() Then
                    Using scope As New TransactionScope
                        Try
                            Dim tipoComprobante = CStr(dgvGrilla.SelectedCells(4).Value).Split(".")
                            Select Case tipoComprobante(0)
                                Case 1 'Ventas Efectivo y Ventas al Credito
                                    actualizarKardex(idVenta)
                                    insertarMovimiento(idVenta, dgvGrilla.SelectedCells(0).Value)
                                    anularComprobante(idVenta)
                                    Dim tipoPago = dgvGrilla.SelectedCells(2).Value
                                    If tipoPago = "CREDITO" Then
                                        anularCredito(idVenta)
                                    End If
                                Case 2 ' Movimiento Banco
                                    anularMovimiento(idVenta)
                                Case 3 ' consulta
                                    anularConsulta(idVenta)
                            End Select                                                       
                            mod_conf.mensaje("Anulacion", "El recibo / Factura fue anulado satisfactoriamente.")
                            btnBuscar_Click(Nothing, Nothing)
                            scope.Complete()
                        Catch ex As Exception

                        End Try
                    End Using

                    
                Else
                    mod_conf.mensajeError("Usuario No Autorizado", "El codigo ingresado no es valido.")
                End If
            End Using
        End If

    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "colButton"
                Dim idVenta = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("id_venta"), DataGridViewTextBoxCell).Value
                Using Form = New comprobante_venta(idVenta)
                    Form.ShowDialog()
                End Using
        End Select
    End Sub

    Private Sub txtMonto_Click(sender As Object, e As EventArgs) Handles txtMonto.Click, txtNroComprobante.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtMonto_Leave(sender As Object, e As EventArgs) Handles txtMonto.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress, txtNroComprobante.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub
End Class