﻿Imports CAPA_NEGOCIO
Imports System.Transactions
Imports CAPA_DATOS
Public Class consultorio
    Private tCliente As eCliente
    Private mod_conf As modulo = New modulo()
    Public Shared _nit As String
    Private idPaciente As Integer = -1
    Private idConsulta As Integer = -1
    Public Sub New()
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tCliente = New eCliente()
    End Sub

    Protected Sub iniciarCombo()
        Dim tPedido As eVenta = New eVenta()
        Dim sSql As String = ""
        sSql += " SELECT ID_CAMPANHA,NOMBRE_CAMPANHA FROM CAMPANHA_MEDICA
        sSql += " WHERE ESTADO = 1 AND CONVERT(DATE,GETDATE(),103) <= CONVERT(DATE,F_HASTA,103)"
        Dim tbl = New datos().ejecutarConsulta(sSql)
        cmbCampanha.DataSource = tbl
        cmbCampanha.DisplayMember = "NOMBRE_CAMPANHA"
        cmbCampanha.ValueMember = "ID_CAMPANHA"
    End Sub
    Private Sub consultorio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        iniciarCombo()
    End Sub

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged

        tCliente.buscarClientePedido(txtNit.Text)
        Dim cliente_factura = ""
        If tCliente.NOMBRE_FACTURA = "" Then
            If tCliente.PERSONA.NOMBRE_APELLIDOS = "" Then
                txtCliente.Text = ""
            Else
                txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
            End If
        Else
            txtCliente.Text = tCliente.NOMBRE_FACTURA
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        mod_conf.volver_venta(form_ventas)
    End Sub

    Private Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        Dim form As form_buscar_cliente = New form_buscar_cliente(2)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
            End If
        End Using
    End Sub

    Private Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        Dim form As form_gestionCliente = New form_gestionCliente(txtNit.Text, True, 3)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
                Dim cliente_buscar = New eCliente()
                cliente_buscar.buscarClientePedido(txtNit.Text)
                If cliente_buscar.PERSONA.NOMBRE_APELLIDOS <> "" Then
                    tCliente = cliente_buscar
                    txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
                Else
                    txtCliente.Text = ""
                End If
            End If
        End Using
    End Sub

    Protected Sub cargarDatosPaciente(ByVal nroCi As String)

        If Len(Trim(nroCi)) > 0 Then
            Dim sSql As String = ""
            sSql += " DECLARE @CI AS NVARCHAR(15);"
            sSql += " SET @CI = '" + Trim(nroCi) + "';"

            sSql += " SELECT * FROM PACIENTE "
            sSql += " WHERE CI = @CI"

            Dim consulta = New datos().ejecutarConsulta(sSql)

            If consulta.Rows.Count > 0 Then
                txtPaciente.Text = consulta.Rows(0)("NOMBRE_APELLIDOS")
                idPaciente = consulta.Rows(0)("ID_PACIENTE")
            Else
                txtPaciente.Text = ""
                idPaciente = -1
            End If
        Else
            txtPaciente.Text = ""
            idPaciente = -1
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtCiPaciente.TextChanged
        cargarDatosPaciente(txtCiPaciente.Text)
    End Sub

    Private Sub chkTarjeta_CheckedChanged(sender As Object, e As EventArgs) Handles chkTarjeta.CheckedChanged
        txtTarjeta.Enabled = chkTarjeta.Checked
        txtNroVoucher.Enabled = chkTarjeta.Checked
        txtPagado.Text = "0.00"
        txtPagado.Enabled = Not chkTarjeta.Checked
    End Sub

    Private Sub txtTarjeta_TextChanged(sender As Object, e As EventArgs) Handles txtTarjeta.TextChanged, txtPagado.TextChanged
        Dim txt As TextBox = sender
        If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
            txt.Text = "" 'Limpia la caja de texto        
        End If
    End Sub

    Private Sub txtTarjeta_Click(sender As Object, e As EventArgs) Handles txtTarjeta.Click, txtPagado.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtTarjeta_Leave(sender As Object, e As EventArgs) Handles txtTarjeta.Leave, txtPagado.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtTarjeta_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTarjeta.KeyPress, txtPagado.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub txtNroVoucher_Click(sender As Object, e As EventArgs) Handles txtNroVoucher.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtNroVoucher_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNroVoucher.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub
    Private Function validarCliente(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Trim(inputText.Text).Length > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Cliente invalido.")
            valido = False
        End If
        Return valido
    End Function

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarCliente(txtCliente, ErrorProvider1) Then
            Dim msg As String = ""
            Using scope = New TransactionScope
                Try
                    If idPaciente = -1 And Trim(txtPaciente.Text).Length > 0 Then
                        Dim tblPaciente = New datos()
                        tblPaciente.tabla = "PACIENTE"
                        tblPaciente.campoID = "ID_PACIENTE"
                        tblPaciente.modoID = "sql"
                        tblPaciente.agregarCampoValor("NOMBRE_APELLIDOS", txtPaciente.Text)
                        tblPaciente.agregarCampoValor("CI", txtCiPaciente.Text)
                        tblPaciente.insertar()
                        idPaciente = tblPaciente.valorID
                    End If
                    Dim tblCampanha = New datos()
                    tblCampanha.tabla = "CONSULTA"
                    tblCampanha.campoID = "ID_CONSULTA"
                    tblCampanha.modoID = "auto"
                    tblCampanha.agregarCampoValor("ID_CLIENTE", tCliente.ID_CLIENTE)
                    tblCampanha.agregarCampoValor("F_PAGO", "serverDateTime")
                    If idPaciente <> -1 Then
                        tblCampanha.agregarCampoValor("ID_PACIENTE", idPaciente)
                    End If

                    tblCampanha.agregarCampoValor("ID_CAMPANHA", cmbCampanha.SelectedValue)
                    If chkTarjeta.Checked Then
                        tblCampanha.agregarCampoValor("TOTAL_CONSULTA", txtTarjeta.Text)
                        tblCampanha.agregarCampoValor("PAGO_EFECTIVO", 0)
                        tblCampanha.agregarCampoValor("NRO_VOUCHER", txtNroVoucher.Text)
                    Else
                        tblCampanha.agregarCampoValor("TOTAL_CONSULTA", txtPagado.Text)
                        tblCampanha.agregarCampoValor("PAGO_EFECTIVO", 1)
                    End If



                    tblCampanha.agregarCampoValor("ANULADO", 0)
                    tblCampanha.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
                    tblCampanha.insertar()
                    idConsulta = tblCampanha.valorID
                    scope.Complete()



                Catch ex As Exception
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Inconsistencia en la base de datos"
                    msg = ex.Message.ToString
                    response = MsgBox(msg, style, title)
                End Try
            End Using
            If msg = "" Then
                'Me.Close()
                PrintReport()
                Dim formulario As form_ventas = New form_ventas()
                mod_conf.volver_venta(formulario)
            End If

        End If
    End Sub
    Private Sub PrintReport()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirVoucherConsultaCampanha(e.Graphics, idConsulta)       
    End Sub
End Class