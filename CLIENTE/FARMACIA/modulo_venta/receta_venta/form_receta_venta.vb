﻿Imports CAPA_DATOS
Public Class form_receta_venta
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private Function obtenerTabla()
        Dim sSql As String = ""
        sSql += " SELECT R.ID_RECETA, P.NOMBRE_APELLIDOS, M.NOMBRE,M.ESPECIALIDAD,CASE WHEN R.VIGENTE = 1 THEN 'Vigente' ELSE 'Concluido' END AS ESTADO,R.OBSERVACION FROM RECETA R"
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = R.ID_PACIENTE "
        sSql += " INNER JOIN MEDICO M ON M.ID_MEDICO = R.ID_MEDICO "
        sSql += " ORDER BY VIGENTE DESC,ID_RECETA"
        Return New datos().ejecutarConsulta(sSql)
    End Function

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1190
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 5
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_RECETA"
            .Columns(0).Width = 100

            .Columns(1).Name = "Paciente"
            .Columns(1).HeaderText = "Paciente"
            .Columns(1).DataPropertyName = "NOMBRE_APELLIDOS"
            .Columns(1).Width = 385


            .Columns(2).Name = "Medico"
            .Columns(2).HeaderText = "Medico"
            .Columns(2).DataPropertyName = "NOMBRE"
            .Columns(2).Width = 385

            .Columns(3).Name = "Especialidad"
            .Columns(3).HeaderText = "Especialidad"
            .Columns(3).DataPropertyName = "ESPECIALIDAD"
            .Columns(3).Width = 200

            .Columns(4).Name = "Estado"
            .Columns(4).HeaderText = "Estado"
            .Columns(4).DataPropertyName = "ESTADO"
            .Columns(4).Width = 120

        End With
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idReceta = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Dim formPago As receta_detalle = New receta_detalle(idReceta)
        formPago.ShowDialog()
    End Sub

    Private Sub form_receta_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        dtSource = obtenerTabla()
        inicializarParametros()
        txtBuscar.Focus()
    End Sub
End Class