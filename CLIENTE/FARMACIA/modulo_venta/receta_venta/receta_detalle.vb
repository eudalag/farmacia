﻿Imports CAPA_DATOS
Public Class receta_detalle
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private tblCabecera As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private idReceta As Integer = -1

    Public Sub New(ByVal _idReceta As Integer)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        idReceta = _idReceta
    End Sub
    Private Sub obtenerTabla()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_RECETA AS INTEGER;"
        sSql += " SET @ID_RECETA = " + idReceta.ToString

        sSql += " SELECT R.ID_RECETA, P.NOMBRE_APELLIDOS AS PACIENTE, M.NOMBRE AS MEDICO,M.ESPECIALIDAD,R.VIGENTE,ISNULL(R.OBSERVACION,'') AS TRATAMIENTO FROM RECETA R"
        sSql += " INNER JOIN PACIENTE P ON P.ID_PACIENTE = R.ID_PACIENTE "
        sSql += " INNER JOIN MEDICO M ON M.ID_MEDICO = R.ID_MEDICO "
        sSql += " WHERE R.ID_RECETA  = @ID_RECETA "

        sSql += " SELECT M.NOMBRE, FF.DESCRIPCION,SUM(RD.CANTIDAD) AS CANTIDAD FROM RECETA R "
        sSql += " INNER JOIN RECETA_DETALLE RD ON RD.ID_RECETA = R.ID_RECETA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = RD.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE R.ID_RECETA  = @ID_RECETA"
        sSql += " GROUP BY M.NOMBRE, FF.DESCRIPCION"
        Dim ds = New datos().obtenerDataSet(sSql)

        tblCabecera = ds.Tables(0)
        dtSource = ds.Tables(1)
    End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 480
            .Height = 290
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 3
            .Columns(0).Name = "nombre"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "NOMBRE"
            .Columns(0).Width = 200

            .Columns(1).Name = "forma_farmaceutica"
            .Columns(1).HeaderText = "Forma Farmaceutica"
            .Columns(1).DataPropertyName = "DESCRIPCION"
            .Columns(1).Width = 160

            .Columns(2).Name = "cantidad"
            .Columns(2).HeaderText = "Cantidad"
            .Columns(2).DataPropertyName = "CANTIDAD"
            .Columns(2).Width = 120

        End With
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idPersona = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Dim formPago As pago_credito = New pago_credito(idPersona)
        mod_conf.cambiarForm(form_principal_venta.panelContenedor, formPago)
    End Sub
    Protected Sub iniciarCabecera()
        txtpaciente.Text = tblCabecera.Rows(0)("PACIENTE")
        txtDoctor.Text = tblCabecera.Rows(0)("MEDICO")
        txtEspecialidad.Text = tblCabecera.Rows(0)("ESPECIALIDAD")
        txtTratamiento.Text = tblCabecera.Rows(0)("TRATAMIENTO")
        Dim valor = tblCabecera.Rows(0)("VIGENTE")
        chkVigente.Checked = CBool(tblCabecera.Rows(0)("VIGENTE"))
    End Sub

    Private Sub form_receta_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        obtenerTabla()
        inicializarParametros()
        iniciarCabecera()
        txtpaciente.Focus()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class