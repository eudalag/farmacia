﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Public Class form_principal_venta
    Private mod_conf As modulo = New modulo()
    Public Shared _fecha As Date
    Public Shared idVentaDevolucion As Integer
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Dispose()
        form_sesion.txtContrasenha.Text = ""
        form_sesion.txtUsuario.Text = ""
        form_sesion.cusuario = New CAPA_NEGOCIO.eUsuario()
        form_sesion.Show()
    End Sub

    Private Sub form_principal_venta_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        form_sesion.Dispose()
        form_sesion.Close()
    End Sub


    Private Sub form_principal_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        
        Dim fVentas As form_ventas = New form_ventas()
        mod_conf.cambiarForm(panelContenedor, fVentas)        
        Dim gen = New generico()
        gen.apertura(form_sesion.cusuario.ID_USUARIO, lblIdCaja.Text, variableSesion.idBanco)
        variableSesion.idCaja = lblIdCaja.Text
        obtenerEstado()        
    End Sub


    Private Sub btnPedido_Click(sender As Object, e As EventArgs) Handles btnPedido.Click
        'If CInt(lblIdCaja.Text) <> -1 Then
        form_ventas.Close()
        Dim pedido As form_pedido = New form_pedido()
        mod_conf.cambiarForm(Me.panelContenedor, pedido)
        'Else
        'Dim modMensaje = New modulo()
        'modMensaje.mensajeError("Error", "No existe una apertura de caja")
        'End If        
    End Sub

    Private Sub btnCliente_Click(sender As Object, e As EventArgs) Handles btnInforme.Click
        form_ventas.Close()
        Dim cliente As form_informe_venta = New form_informe_venta()
        mod_conf.cambiarForm(Me.panelContenedor, cliente)
    End Sub

    Private Sub btnInforme_Click(sender As Object, e As EventArgs) Handles btnCotizar.Click
        form_ventas.Close()
        Dim cotizacion As form_cotizacion = New form_cotizacion()
        mod_conf.cambiarForm(Me.panelContenedor, cotizacion)
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnApertura.Click
        If CInt(lblIdCaja.Text) = -1 Then
            Using f_nuevo_pedido = New form_apertura()
                If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then
                    'imprimirApertura()
                    obtenerEstado()                    
                End If
            End Using
        Else
            If MessageBox.Show("Usted ya realizo una apertura de caja ¿desea modificarla?", "Aperturar Caja", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
                Using f_nuevo_pedido = New form_apertura(CInt(lblIdCaja.Text))
                    If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then
                        'imprimirApertura()                        
                    End If
                End Using
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnMovimiento.Click
        If CInt(lblIdCaja.Text) <> -1 Then
            Using f_nuevo_pedido = New form_movimiento()
                If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then                    
                End If
            End Using
        Else
            Dim modMensaje = New modulo()
            modMensaje.mensajeError("Error", "No existe una apertura de caja")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCierreCaja.Click
        If CInt(lblIdCaja.Text) <> -1 Then
            Using f_nuevo_pedido = New form_cierre()
                If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then
                    imprimirCierre()
                    lblIdCaja.Text = -1
                    obtenerEstado()

                End If
            End Using
        Else
            Dim modMensaje = New modulo()
            modMensaje.mensajeError("Error", "No existe una apertura de caja")
        End If
    End Sub
    Private Sub imprimirApertura()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub imprimirCierre()
        Try
            PrintDocument2.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirApertura(e.Graphics, lblIdCaja.Text)
    End Sub

    Private Sub PrintDocument2_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim print = New imprimir()
        print.imprimirCierre(e.Graphics, lblIdCaja.Text)
    End Sub

    Private Sub form_principal_venta_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.F1            
                  form_ventas.Close()
                Dim pedido As form_pedido = New form_pedido()
                mod_conf.cambiarForm(Me.panelContenedor, pedido)
                'Case Keys.F2
                '    If CInt(lblIdCaja.Text) <> -1 Then
                '        form_ventas.Close()
                '        Dim cotiza As form_gestion_cotizacion = New form_gestion_cotizacion()
                '        mod_conf.cambiarForm(panelContenedor, cotiza)
                '    Else
                '        Dim modMensaje = New modulo()
                '        modMensaje.mensajeError("Error", "No existe una apertura de caja")
                '    End If
            Case Keys.F3
                form_ventas.Close()
                Dim consulta As consultorio = New consultorio()
                mod_conf.cambiarForm(Me.panelContenedor, consulta)
            Case Keys.Escape
                mod_conf.cambiarForm(panelContenedor, form_ventas)
        End Select

    End Sub

    Public Sub obtenerEstado()
        Dim sSql As String = "SELECT COUNT(*) AS TOTAL FROM CAJA WHERE F_CIERRE IS NULL AND ID_USUARIO = " + form_sesion.cusuario.ID_USUARIO.ToString
        Dim tbl = New datos()
        variableSesion.estadoCaja = IIf(CInt(tbl.ejecutarConsulta(sSql).Rows(0.0)(0)) = 0, 0, 1)
        lblEstado.Text = IIf(CInt(tbl.ejecutarConsulta(sSql).Rows(0.0)(0)) = 0, "Cerrado", "Abierto")
    End Sub

    Public Sub obtenerTotalCaja()
        Dim sSql As String = ""

        sSql += " DECLARE @ID_CAJA AS INTEGER;"
        sSql += " SET @ID_CAJA = " & lblIdCaja.Text & ";"
        sSql += " SELECT C.IMPORTE_APERTURA_MN  + ISNULL(SUM(M.TOTAL),0) + ISNULL((V.TOTAL),0) FROM CAJA C"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, CASE WHEN ID_TIPO_MOVIMIENTO = 2 THEN  IMPORTE * -1 ELSE IMPORTE  END AS TOTAL FROM MOVIMIENTO "
        sSql += " WHERE ID_CAJA = @ID_CAJA ) M ON M.ID_CAJA = C.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT V.ID_CAJA,"
        sSql += " SUM(CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,0) > 1 THEN"
        sSql += " CAST((R.CANTIDAD / CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,0) = 0 THEN 1 ELSE M.CANTIDAD_PAQUETE END) "
        sSql += " AS INTEGER) * R.PRECIO_PAQUETE ELSE 0 END +"
        sSql += " (R.CANTIDAD - CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,0) > 1 THEN "
        sSql += " CAST((R.CANTIDAD / CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,0) = 0 THEN 1 ELSE M.CANTIDAD_PAQUETE END) AS INTEGER) ELSE 0 END * ISNULL(M.CANTIDAD_PAQUETE,0)) * R.PRECIO_UNITARIO)"
        sSql += " AS TOTAL  FROM VENTA V "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO  "
        sSql += " WHERE V.ID_CAJA = @ID_CAJA AND ID_TIPO_COMPROBANTE <> 3 AND V.ANULADA = 0 GROUP BY V.ID_CAJA "
        sSql += " ) V ON V.ID_CAJA = C.ID_CAJA WHERE C.ID_CAJA = @ID_CAJA  "
        sSql += " GROUP BY C.IMPORTE_APERTURA_MN,V.TOTAL "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            'txtTotalCaja.Text = Decimal.Parse(tbl.Rows(0)(0))
        Else
            'txtTotalCaja.Text = "0.00"
        End If

    End Sub

    Private Sub btnCredito_Click(sender As Object, e As EventArgs) Handles btnCredito.Click
        form_ventas.Close()
        Dim cotizacion As cuentas_x_cobrar = New cuentas_x_cobrar()
        mod_conf.cambiarForm(Me.panelContenedor, cotizacion)
    End Sub

    Private Sub btnRecetas_Click(sender As Object, e As EventArgs) Handles btnRecetas.Click
        form_ventas.Close()
        Dim cotizacion As form_receta_venta = New form_receta_venta()
        mod_conf.cambiarForm(Me.panelContenedor, cotizacion)
    End Sub

    Private Sub btnPaciente_Click(sender As Object, e As EventArgs) Handles btnPaciente.Click
        form_ventas.Close()
        Dim cotizacion As pacientes = New pacientes()
        mod_conf.cambiarForm(Me.panelContenedor, cotizacion)
    End Sub

    Private Sub btnDevolucion_Click(sender As Object, e As EventArgs) Handles btnDevolucion.Click
        Dim devolucionNro = New devolucion_nro_comprobante()
        If devolucionNro.ShowDialog = Windows.Forms.DialogResult.OK Then
            form_ventas.Close()
            Dim devolucion = New devolucion_detalle(idVentaDevolucion)
        End If

    End Sub

    Private Sub MovimientoMedicamentoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MovimientoMedicamentoToolStripMenuItem.Click
        form_ventas.Close()
        Dim cotizacion As movimiento_viewer = New movimiento_viewer()
        mod_conf.cambiarForm(Me.panelContenedor, cotizacion)
    End Sub

    Private Sub VentasPorLaboratorioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VentasPorLaboratorioToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As ventaLaboratorio_reporte = New ventaLaboratorio_reporte()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub

    Private Sub InventarioGeneralPorLaboratorioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioGeneralPorLaboratorioToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As inventario_general_laboratorio_reporte = New inventario_general_laboratorio_reporte()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub

    Private Sub ExistenciaPorLaboratorioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExistenciaPorLaboratorioToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As inf_existencia = New inf_existencia()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        'If CInt(lblIdCaja.Text) <> -1 Then
        '    Using f_nuevo_pedido = New form_transferencia()
        '        If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then
        '            'imprimirCierre()

        '        End If
        '    End Using
        'Else
        '    Dim modMensaje = New modulo()
        '    modMensaje.mensajeError("Error", "No existe una apertura de caja")
        'End If
        Using f_nuevo_pedido = New form_transferencia()
            If DialogResult.OK = f_nuevo_pedido.ShowDialog() Then
                'imprimirCierre()

            End If
        End Using


    End Sub

    Private Sub VencimientosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VencimientosToolStripMenuItem.Click
        Dim form = New form_baja_medicamento()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        Dim form = New form_cliente()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        'If CInt(lblIdCaja.Text) <> -1 Then
        form_ventas.Close()
        Dim consulta As consultorio = New consultorio()
        mod_conf.cambiarForm(Me.panelContenedor, consulta)
        'Else
        'Dim modMensaje = New modulo()
        'modMensaje.mensajeError("Error", "No existe una apertura de caja")
        'End If       
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        form_ventas.Close()
        Dim formulario As form_facturacion_manual = New form_facturacion_manual()
        mod_conf.cambiarForm(Me.panelContenedor, formulario)
    End Sub
End Class