﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class comprobante_pago
    Private _idPago As Integer
    Private dtSource As DataTable
    Public Sub New(ByVal idPago As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().



        _idPago = idPago
        cargarDatos()
    End Sub

    Private Sub inicializarParametros()

        With (dgvGrilla)
            .Width = 525
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = False

            '.Columns(0).Name = "registro"
            '.Columns(0).HeaderText = "Registro"
            '.Columns(0).DataPropertyName = "ID_CREDITO"
            '.Columns(0).Width = 80
            '.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            'Dim ColCheckBox As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
            '.Columns.Insert(0, ColCheckBox)
            'With ColCheckBox
            '    'Titulo de la columna.
            '    .HeaderText = "Pagar"
            '    'Nombre para acceder al contenido por codigo.
            '    .Name = "colChecked"
            '    'Ancho
            '    .Width = 50
            '    'Valor a devolver cuando este marcada
            '    .TrueValue = 1
            '    'Valor a devolver cuando este desmarcada
            '    .FalseValue = 0
            '    'Color de fondo de esta columna
            '    '.DefaultCellStyle.BackColor = Color.LightSteelBlue
            '    'Posicion en la que se visualizará
            '    .DisplayIndex = 0
            '    'La columna es fija, o se desplaza con el resto.
            '    .Frozen = False
            '    .ReadOnly = False
            'End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fecha)
            With fecha
                .Name = "Fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "FECHA"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim Detalle As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(Detalle)
            With Detalle
                .Name = "Recibo"
                .HeaderText = "Detalle"
                .DataPropertyName = "NRO_COMPROBANTE"
                .Width = 250
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim importe As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(importe)
            With importe
                .Name = "importe"
                .HeaderText = "Importe"
                .DataPropertyName = "IMPORTE"
                .Width = 110
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton
                'Titulo de la columna.
                .HeaderText = "Ver"
                'Nombre para acceder al contenido por codigo.
                .Name = "colButton"
                'Ancho
                .Width = 50
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 3
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False

                .Text = "Ver"
                .UseColumnTextForButtonValue = True

            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID_VENTA"

            End With
            .Refresh()
        End With
        dgvGrilla.DataSource = dtSource
    End Sub
    Protected Function obtenerCabecera() As DataTable
        Dim sSql As String = ""

        Return New CAPA_DATOS.datos().ejecutarConsulta(sSql)
    End Function
    Protected Sub cargarDatos()
        Dim gen = New generico()
        Dim tbls As DataSet = gen.obtenerDetallePago(_idPago)
        Dim tblPago = tbls.Tables(1)
        dtSource = tbls.Tables(0)
        txtNro.Text = tblPago.Rows(0)("NRO_PAGO").ToString
        txtCliente.Text = tblPago.Rows(0)("NOMBRE_APELLIDOS").ToString
        txtGrupoCliente.Text = tblPago.Rows(0)("GRUPO_PERSONAS").ToString
        txtTipo.Text = tblPago.Rows(0)("DESC_TIPO_PAGO").ToString
        txtFecha.Text = tblPago.Rows(0)("FECHA").ToString
        txtUsuario.Text = tblPago.Rows(0)("USUARIO").ToString

        txtTotal.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblPago.Rows(0)("TOTAL_PAGO"))
        txtCancelado.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblPago.Rows(0)("IMPORTE_PAGO"))
    End Sub

    Private Sub comprobante_pago_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name         
            Case "colButton"
                Dim idVenta = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value
                Using Form = New comprobante_venta(idVenta)
                    Form.ShowDialog()
                End Using
        End Select
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirVoucherPagoCredito(e.Graphics, _idPago)
    End Sub
    Private Sub imprimirVoucher()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        imprimirVoucher()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Using confirmacion = New formConfirmacion(1)
            If DialogResult.OK = confirmacion.ShowDialog() Then
                Using scope As New TransactionScope
                    Try

                        Dim tblUpdate = New datos()

                        tblUpdate.tabla = "MOVIMIENTO_BANCO"
                        tblUpdate.campoID = "ID_MOVIMIENTO"
                        tblUpdate.modoID = "auto"
                        tblUpdate.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
                        tblUpdate.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
                        tblUpdate.agregarCampoValor("IMPORTE", Decimal.Parse(txtCancelado.Text, New Globalization.CultureInfo("en-US")))
                        tblUpdate.agregarCampoValor("GLOSA", "ANULACION DE PAGO CREDITO CLIENTE " + txtCliente.Text + " - COMP. NRO(S): " + txtNro.Text)
                        tblUpdate.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                        tblUpdate.insertar()

                        tblUpdate.tabla = "PAGO_CREDITO"
                        tblUpdate.campoID = "ID_PAGO"
                        tblUpdate.valorID = _idPago
                        tblUpdate.eliminar()
                        tblUpdate.reset()
                        tblUpdate.tabla = "PAGOS"
                        tblUpdate.campoID = "ID_PAGO"
                        tblUpdate.valorID = _idPago
                        tblUpdate.eliminar()
                        tblUpdate.reset()

                        scope.Complete()

                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Information
                        title = "Anulacion de Pago"
                        Dim msg = "El pago de Credito Fue Anulado."
                        response = MsgBox(msg, style, title)
                        Me.Close()
                    Catch ex As Exception
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                        title = "Error Inconsistencia en la base de datos"
                        Dim msg = ex.Message.ToString
                        response = MsgBox(msg, style, title)
                    End Try
                End Using
            End If
            

            'Dim tblPago = New CAPA_DATOS.datos()

            'tblPago.tabla = "PAGOS"
            'tblPago.campoID = "ID_PAGO"
            'tblPago.agregarCampoValor("ANULADO", 1)
            'tblPago.modificar()

            'tblPago.reset()
            'tblPago.tabla = "PAGO_CREDITO"
            'tblPago.campoID = "ID_PAGO"
            'tblPago.agregarCampoValor("ANULADO", 1)
            'tblPago.modificar()

        End Using
    End Sub
End Class