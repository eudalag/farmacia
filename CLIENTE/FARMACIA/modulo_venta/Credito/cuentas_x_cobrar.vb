﻿Imports CAPA_DATOS
Public Class cuentas_x_cobrar
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private dtSourceGeneral As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 20
    Private Function obtenerDeuda()
        Dim sSql As String = ""
        sSql += " SELECT * FROM ("
        sSql += " SELECT CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,GC.GRUPO_PERSONAS   FROM CREDITO C"
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
        sSql += " WHERE ESTADO = 0"
        sSql += " GROUP BY CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, GC.GRUPO_PERSONAS ) C"
        'sSql += " C WHERE C.TOTAL_CREDITO > 0"
        sSql += " ORDER BY NOMBRE_APELLIDOS "
        Return New datos().ejecutarConsulta(sSql)
    End Function

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1100

            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 5
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_CLIENTE"
            .Columns(0).Width = 100

            .Columns(1).Name = "Nombre"
            .Columns(1).HeaderText = "Nombre"
            .Columns(1).DataPropertyName = "NOMBRE_APELLIDOS"
            .Columns(1).Width = 550
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "Celular"
            .Columns(2).HeaderText = "Celular"
            .Columns(2).DataPropertyName = "CELULAR"
            .Columns(2).Width = 150
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(3).Name = "grupo"
            .Columns(3).HeaderText = "Tipo Cliente"
            .Columns(3).DataPropertyName = "GRUPO_PERSONAS"
            .Columns(3).Width = 150
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft

            .Columns(4).Name = "Deuda"
            .Columns(4).HeaderText = "Cta. por Cobrar"
            .Columns(4).DataPropertyName = "TOTAL_CREDITO"
            .Columns(4).Width = 150
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        End With
        LoadPage()
    End Sub
    Protected Sub buscarCliente()
        Dim buscar = txtBuscar.Text
        buscar = buscar.Replace("'", "")
        buscar = buscar.Replace("--", "")


        Dim dtSourceAux As DataTable = dtSourceGeneral

        Dim tbl = dtSourceAux.Select("(NOMBRE_APELLIDOS like '%" + buscar + "%')")
        If tbl.Count > 0 Then
            dtSource = tbl.CopyToDataTable
        Else
            dtSource = dtSourceGeneral.Clone
        End If
    End Sub

    Private Sub cuentas_x_cobrar_Load(sender As Object, e As EventArgs) Handles Me.Load
        dtSourceGeneral = obtenerDeuda()
        buscarCliente()
        inicializarParametros()
        txtBuscar.Focus()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idCliente = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Dim formPago As pago_credito = New pago_credito(idCliente)
        If variableSesion.administrador Then
            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, formPago)
        Else
            mod_conf.cambiarForm(form_principal_venta.panelContenedor, formPago)
        End If

    End Sub

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        buscarCliente()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub
End Class