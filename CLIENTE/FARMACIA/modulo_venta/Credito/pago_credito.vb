﻿Imports CAPA_DATOS
Imports System.Transactions
Public Class pago_credito
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private dtSourcePago As DataTable
    Private idCliente As Integer
    Shared idPago As Integer

    Public Sub New(ByVal _idCliente As Integer)
        InitializeComponent()
        idCliente = _idCliente
        'If form_sesion.cusuario.DESCUENTO Then
        '    txtDescuento.ReadOnly = False
        '    txtDescuento.Text = "0"
        'Else
        '    txtDescuento.ReadOnly = True
        '    txtDescuento.Text = "0"
        'End If
    End Sub

    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        sSql += "  SELECT ID_TIPO_PAGO, DESC_TIPO_PAGO FROM TIPO_PAGO"
        If Not variableSesion.administrador Then
            sSql += " WHERE ID_TIPO_PAGO = 1"
        End If
        sSql += " UNION ALL SELECT -1, '[Seleccione Tipo de Pago]'"        
        sSql += " ORDER BY DESC_TIPO_PAGO "

        cmbTipoPago.DataSource = New datos().ejecutarConsulta(sSql)
        cmbTipoPago.DisplayMember = "DESC_TIPO_PAGO"
        cmbTipoPago.ValueMember = "ID_TIPO_PAGO"
    End Sub

    Protected Sub iniciarComboBanco()
        Dim sSql As String = ""
        sSql += "  SELECT B.ID_BANCO, TB.DESC_TIPO_BANCO + ' - ' + B.ENTIDAD_FINANCIERA + ' (' + ISNULL(B.NRO_CUENTA,'') + ')' AS BANCO  FROM BANCO B"
        sSql += "  INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += "  WHERE NOT B.ID_TIPO_BANCO IN (1,2)"
        sSql += "  UNION ALL SELECT -1, '[Seleccione Banco / Nro. de Cuenta]'"

        cmbCuentaBanco.DataSource = New datos().ejecutarConsulta(sSql)
        cmbCuentaBanco.DisplayMember = "BANCO"
        cmbCuentaBanco.ValueMember = "ID_BANCO"
        If Not variableSesion.administrador Then
            cmbCuentaBanco.Enabled = False
        End If
    End Sub

    Private Function obtenerDeuda()
        Dim sSql As String = ""
        sSql += " DECLARE @CLIENTE AS INTEGER;"
        sSql += " SET @CLIENTE = " + idCliente.ToString + ";"
        sSql += " SELECT C.ID_CREDITO,ISNULL(V.ID_VENTA,-1) AS ID_VENTA  ,ISNULL(CONVERT(NVARCHAR(10),V.FECHA,103),'18/03/2019') AS FECHA, "
        sSql += " CASE WHEN NOT TC.DETALLE IS NULL THEN TC.DETALLE + ' - ' ELSE '' END "
        sSql += " + ISNULL(CAST(V.NRO_VENTA AS NVARCHAR(50)),C.OBSERVACION) AS NRO_VENTA, C.IMPORTE_CREDITO,"
        sSql += " C.IMPORTE_CREDITO - ISNULL(PC.PAGO,0)  AS SALDO   FROM CREDITO C"
        sSql += " LEFT JOIN VENTA V ON V.ID_VENTA = C.ID_VENTA "
        sSql += " LEFT JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " LEFT JOIN (SELECT ID_CREDITO, SUM(IMPORTE) AS PAGO FROM PAGO_CREDITO GROUP BY ID_CREDITO) AS PC ON PC.ID_CREDITO = C.ID_CREDITO "
        sSql += " WHERE C.ESTADO = 0 AND C.ID_CLIENTE  = @CLIENTE AND C.IMPORTE_CREDITO - ISNULL(PC.PAGO,0) > 0"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Function obtenerPagos()
        Dim sSql As String = ""
        sSql += " DECLARE @CLIENTE AS INTEGER;"
        sSql += " SET @CLIENTE = " + idCliente.ToString + ";"
        sSql += " SELECT DISTINCT P.ID_PAGO, CONVERT(NVARCHAR(10),P.FECHA,103) AS F_PAGO, P.IMPORTE_PAGO, U.USUARIO  FROM PAGOS P"
        sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        sSql += " INNER JOIN CREDITO C ON C.ID_CREDITO = PC.ID_CREDITO "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA PR ON PR.ID_PERSONA = CL.ID_PERSONA "
        sSql += " INNER JOIN BANCO B ON B.ID_BANCO = P.ID_BANCO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = B.ID_USUARIO "
        sSql += " WHERE CL.ID_CLIENTE = @CLIENTE"
        sSql += " ORDER BY F_PAGO DESC"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @CLIENTE AS INTEGER;"
        sSql += " SET @CLIENTE =  " + idCliente.ToString + " ;"
        sSql += " SELECT CL.ID_CLIENTE,NOMBRE_APELLIDOS, ISNULL(DIRECCION,'') AS DIRECCION, ISNULL(CELULAR,'') AS CELULAR"
        sSql += "  ,GC.GRUPO_PERSONAS, SUM(TOTAL_PEDIDO)  -  (ISNULL(PAGO,0)) AS TOTAL_CREDITO FROM("
        sSql += " SELECT CL.ID_CLIENTE ,SUM(V.TOTAL_PEDIDO) AS TOTAL_PEDIDO"
        sSql += " FROM VENTA V "
        sSql += " INNER JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " WHERE V.CREDITO = 1 AND ANULADA = 0  AND CL.ID_CLIENTE = @CLIENTE"
        sSql += " GROUP BY CL.ID_CLIENTE"
        sSql += " UNION ALL"
        sSql += " SELECT CL.ID_CLIENTE  ,SUM(CR.IMPORTE_CREDITO) FROM CREDITO CR"
        sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " WHERE ID_VENTA IS NULL AND CL.ID_CLIENTE = @CLIENTE"
        sSql += " GROUP BY CL.ID_CLIENTE"
        sSql += " ) AS CR"
        sSql += " LEFT JOIN ("
        sSql += " SELECT C.ID_CLIENTE, SUM(PC.IMPORTE) AS PAGO  FROM PAGO_CREDITO PC"
        sSql += " INNER JOIN CREDITO C ON C.ID_CREDITO = PC.ID_CREDITO "
        sSql += " WHERE C.ID_CLIENTE = @CLIENTE"
        sSql += " GROUP BY ID_CLIENTE"
        sSql += " ) P ON P.ID_CLIENTE = CR.ID_CLIENTE "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE =  CR.ID_CLIENTE "
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " INNER JOIN PERSONA PS ON PS.ID_PERSONA = CL.ID_PERSONA"

        sSql += " GROUP BY CL.ID_CLIENTE,P.PAGO,NOMBRE_APELLIDOS, DIRECCION, CELULAR,GC.GRUPO_PERSONAS"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        txtNombre.Text = tblConsulta.Rows(0)("NOMBRE_APELLIDOS").ToString
        txtDireccion.Text = tblConsulta.Rows(0)("DIRECCION")
        txtCelular.Text = tblConsulta.Rows(0)("CELULAR")
        txtGrupo.Text = tblConsulta.Rows(0)("GRUPO_PERSONAS")
        txtTotal.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblConsulta.Rows(0)("TOTAL_CREDITO"))
    End Sub

    Private Sub inicializarParametros()

        With (dgvGrilla)
            .Width = 525
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = False

            '.Columns(0).Name = "registro"
            '.Columns(0).HeaderText = "Registro"
            '.Columns(0).DataPropertyName = "ID_CREDITO"
            '.Columns(0).Width = 80
            '.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim ColCheckBox As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
            .Columns.Insert(0, ColCheckBox)
            With ColCheckBox
                'Titulo de la columna.
                .HeaderText = "Pagar"
                'Nombre para acceder al contenido por codigo.
                .Name = "colChecked"
                'Ancho
                .Width = 50
                'Valor a devolver cuando este marcada
                .TrueValue = 1
                'Valor a devolver cuando este desmarcada
                .FalseValue = 0
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 0
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False
                .ReadOnly = False
            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(1, fecha)
            With fecha
                .Name = "Fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "FECHA"
                .Width = 70
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim Detalle As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(2, Detalle)
            With Detalle
                .Name = "Recibo"
                .HeaderText = "Detalle"
                .DataPropertyName = "NRO_VENTA"
                .Width = 230
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim importe As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(3, importe)
            With importe
                .Name = "importe"
                .HeaderText = "Importe"
                .DataPropertyName = "SALDO"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With            
            
            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Insert(4, colButton)
            With colButton
                'Titulo de la columna.
                .HeaderText = "Ver"
                'Nombre para acceder al contenido por codigo.
                .Name = "colButton"
                'Ancho
                .Width = 50
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 4
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False

                .Text = "Ver"
                .UseColumnTextForButtonValue = True

            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Insert(5, colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID_CREDITO"

            End With
            .Refresh()
        End With
        dgvGrilla.DataSource = dtSource
    End Sub

    Private Sub inicializarParametrosPagos()

        With (dgvPagos)
            .Width = 525
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = False

            '.Columns(0).Name = "registro"
            '.Columns(0).HeaderText = "Registro"
            '.Columns(0).DataPropertyName = "ID_CREDITO"
            '.Columns(0).Width = 80
            '.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim ColCheckBox As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
            .Columns.Insert(0, ColCheckBox)
            With ColCheckBox
                'Titulo de la columna.
                .HeaderText = "Anular"
                'Nombre para acceder al contenido por codigo.
                .Name = "colChecked"
                'Ancho
                .Width = 50
                'Valor a devolver cuando este marcada
                .TrueValue = 1
                'Valor a devolver cuando este desmarcada
                .FalseValue = 0
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 0
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False
                .ReadOnly = False
            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(1, fecha)
            With fecha
                .Name = "Fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "F_PAGO"
                .Width = 70
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim Detalle As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(2, Detalle)
            With Detalle
                .Name = "idPago"
                .HeaderText = "Nro. Pago"
                .DataPropertyName = "ID_PAGO"
                .Width = 230
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim importe As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Insert(3, importe)
            With importe
                .Name = "importe"
                .HeaderText = "Importe"
                .DataPropertyName = "IMPORTE_PAGO"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Insert(4, colButton)
            With colButton
                'Titulo de la columna.
                .HeaderText = "Ver"
                'Nombre para acceder al contenido por codigo.
                .Name = "colButton"
                'Ancho
                .Width = 50
                'Color de fondo de esta columna
                '.DefaultCellStyle.BackColor = Color.LightSteelBlue
                'Posicion en la que se visualizará
                .DisplayIndex = 4
                'La columna es fija, o se desplaza con el resto.
                .Frozen = False

                .Text = "Ver"
                .UseColumnTextForButtonValue = True

            End With

            .Refresh()
        End With
        dgvPagos.DataSource = dtSourcePago
    End Sub

    Private Sub pago_credito_Load(sender As Object, e As EventArgs) Handles Me.Load
        cargarDatos()
        dtSource = obtenerDeuda()
        dtSourcePago = obtenerPagos()
        inicializarParametros()
        inicializarParametrosPagos()
        iniciarCombo()
        iniciarComboBanco()
        'obtenerTotalDeuda()
        'calcularSaldo()
    End Sub

    Protected Function obtenerIdVenta(ByVal idCredito As Integer) As Integer
        Dim sSql As String = ""

        sSql += " DECLARE @CREDITO AS INTEGER;"
        sSql += " SET @CREDITO = " + idCredito.ToString + ";"
        sSql += " SELECT ID_VENTA  FROM CREDITO WHERE ID_CREDITO = @CREDITO "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            Return tbl.Rows(0)(0)
        Else
            Return -1
        End If
    End Function

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick

        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "colChecked"
                Dim chek As DataGridViewCheckBoxCell = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("colChecked"), DataGridViewCheckBoxCell)
                chek.Value = Not chek.Value
                calcularTotalPagar()
            Case "colButton"
                Dim idCredito = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value

                Using Form = New comprobante_venta(obtenerIdVenta(idCredito))
                    Form.ShowDialog()
                End Using
        End Select
    End Sub
    'Private Sub obtenerTotalDeuda()
    '    Dim total As Decimal = 0
    '    For Each item In dtSource.Rows
    '        total += CDec(item("IMPORTE_CREDITO"))
    '    Next
    '    txtTotalCredito.Text = total.ToString("#,##0.00")
    'End Sub
    Private Sub calcularTotalPagar()
        Dim total As Decimal = 0
        For Each cell In dgvGrilla.Rows
            If CBool(CType(cell.Cells("colChecked"), DataGridViewCheckBoxCell).Value) Then
                total += Decimal.Parse(CType(cell.Cells("importe"), DataGridViewTextBoxCell).Value, New Globalization.CultureInfo("en-US"))
            End If
        Next
        txtPagar.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", total)
        ' calcularCambio()
    End Sub

    'Private Sub calcularSaldo()
    '    If IsNumeric(txtTotalCredito.Text) And IsNumeric(txtTotalCancelado.Text) Then
    '        txtSaldo.Text = CDec(txtTotalCredito.Text) - CDec(txtTotalPagar.Text)
    '    End If
    'End Sub

    'Private Sub calcularCambio()
    '    Dim totalCompra As String = IIf(Trim(txtTotalPagar.Text.Length) = 0, "0.00", txtTotalPagar.Text)
    '    Dim totalPagado As String = IIf(Trim(txtPagado.Text.Length) = 0, "0.00", txtPagado.Text)
    '    Dim totalDolares As String = IIf(Trim(txtDolares.Text.Length) = 0, "0.00", txtDolares.Text)
    '    Dim totalTarjeta As String = IIf(Trim(txtTarjeta.Text.Length) = 0, "0.00", txtTarjeta.Text)
    '    Dim totalCancelado As String = Decimal.Parse(totalDolares) + Decimal.Parse(totalTarjeta) + Decimal.Parse(totalPagado)
    '    txtTotalCancelado.Text = totalCancelado
    '    txtCambio.Text = (Decimal.Parse(Replace(totalCancelado, ",", "."), New Globalization.CultureInfo("en-US"))) - Decimal.Parse(Replace(totalCompra, ",", "."), New Globalization.CultureInfo("en-US"))
    '    calcularSaldo()
    'End Sub

    'Private Sub txtPagado_Leave(sender As Object, e As EventArgs)
    '    Dim txt As TextBox = sender
    '    Dim result As String = txt.Text.ToString
    '    Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
    '    If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
    '        If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
    '            result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
    '        End If
    '    Else
    '        result = result + ".00"
    '    End If
    '    txt.Text = result
    'End Sub

    'Private Sub txtPagado_Click(sender As Object, e As EventArgs)
    '    Dim txt As TextBox = sender
    '    If (Not String.IsNullOrEmpty(txt.Text)) Then
    '        txt.SelectionStart = 0
    '        txt.SelectionLength = txt.Text.Length
    '    End If
    'End Sub

    'Private Sub txtPagado_TextChanged(sender As Object, e As EventArgs)
    '    If Me.txtPagado.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
    '        Me.txtPagado.Text = "" 'Limpia la caja de texto        
    '    End If
    '    calcularCambio()
    'End Sub
    'Private Sub txtDescuento_Leave(sender As Object, e As EventArgs)
    '    Dim result As String = CType(sender, TextBox).Text.ToString
    '    Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
    '    If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
    '        If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
    '            result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
    '        End If
    '    Else
    '        result = result + ".00"
    '    End If
    '    CType(sender, TextBox).Text = result
    'End Sub

    'Private Sub txtDescuento_TextChanged(sender As Object, e As EventArgs)
    '    Dim pedido As Decimal = Decimal.Parse(IIf(Trim(txtTotal.Text.Length) = 0, "0.00", txtTotal.Text))
    '    Dim porcDesc As Decimal = Decimal.Parse(IIf(Trim(txtDescuento.Text.Length) = 0, "0.00", txtDescuento.Text))
    '    txtTotalDescuento.Text = Math.Round(pedido * (porcDesc / 100), 2).ToString
    '    txtTotalPagar.Text = Math.Round(pedido - (pedido * (porcDesc / 100)), 2).ToString
    '    calcularCambio()
    'End Sub

    'Private Sub txtDescuento_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    Dim ch As Char = e.KeyChar
    '    Dim tb As TextBox = sender

    '    If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
    '        e.Handled = True
    '    End If

    '    If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
    '        e.Handled = True
    '    End If

    '    If Char.IsDigit(ch) Or ch = "." Then
    '        Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
    '                               + e.KeyChar _
    '                               + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

    '        Dim parts() As String = result.Split(".")
    '        If parts.Length > 1 Then
    '            If parts(1).Length > 2 Then
    '                e.Handled = True
    '            End If
    '        End If
    '    End If

    '    If Asc(e.KeyChar) = 13 Then
    '        e.Handled = True
    '        txtPagado.Focus()
    '    End If
    'End Sub

    'Private Sub txtTotal_TextChanged(sender As Object, e As EventArgs)
    '    txtDescuento_TextChanged(Nothing, Nothing)
    'End Sub

    'Private Sub txtDescuento_Click(sender As Object, e As EventArgs)
    '    If (Not String.IsNullOrEmpty(sender.Text)) Then
    '        sender.SelectionStart = 0
    '        sender.SelectionLength = sender.Text.Length
    '    End If
    'End Sub

    'Private Sub txtTarjeta_TextChanged(sender As Object, e As EventArgs)
    '    Dim txt As TextBox = sender
    '    If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
    '        txt.Text = "" 'Limpia la caja de texto        
    '    End If
    '    calcularCambio()
    'End Sub

    'Private Sub txtDolares_TextChanged(sender As Object, e As EventArgs)
    '    Dim txt As TextBox = sender
    '    If txt.Text.IndexOf(".") = 0 Then ' Si el primer caracter inggresado es punto (".")
    '        txt.Text = "" 'Limpia la caja de texto  
    '        txtDolares.Text = "0.00"
    '    Else
    '        Dim totalDolares = IIf(Trim(txtDolaresCambio.Text.Length) = 0, "0.00", txtDolaresCambio.Text)
    '        txtDolares.Text = Math.Round(Decimal.Parse(totalDolares) * Decimal.Parse(form_sesion.cConfiguracion.TC_ME), 2)
    '    End If
    '    calcularCambio()
    'End Sub

    Private Function validarFormulario() As Boolean
        Dim valido As Boolean = True
        Dim mensaje As String = ""
        If IsNumeric(txtCancelado.Text) Then
            If Not CDec(txtCancelado.Text) > 0 Then
                mod_conf.mensajeError("Error Importe", "Importe de Credito no valido.")
                valido = False
            End If
        Else
            mod_conf.mensajeError("Error Importe", "Importe de Credito no valido.")
            valido = False
        End If
        If cmbTipoPago.SelectedValue = -1 Then
            mensaje = "Seleccione un Usuario valido."
            ErrorProvider1.SetError(cmbTipoPago, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarFormulario() Then

            Dim idCredito As Integer
            Dim tblUpdate = New datos()
            Dim Totalcancelado As Decimal = Decimal.Parse(txtCancelado.Text, New Globalization.CultureInfo("en-US"))
            Dim pagar As Decimal = Decimal.Parse(txtPagar.Text, New Globalization.CultureInfo("en-US"))
            If Totalcancelado > pagar Then
                Totalcancelado = pagar
            End If
            Using scope = New TransactionScope
                Try
                    tblUpdate.tabla = "PAGOS"
                    tblUpdate.campoID = "ID_PAGO"
                    tblUpdate.modoID = "sql"
                    tblUpdate.agregarCampoValor("IMPORTE_PAGO", Totalcancelado)
                    tblUpdate.agregarCampoValor("OBSERVACIONES", txtObservaciones.Text)
                    tblUpdate.agregarCampoValor("FECHA", txtFechaPago.Value)
                    tblUpdate.agregarCampoValor("ID_TIPO_PAGO", cmbTipoPago.SelectedValue)
                    tblUpdate.agregarCampoValor("TOTAL_PAGO", pagar)
                    If cmbTipoPago.SelectedValue = 1 Then
                        tblUpdate.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
                    Else
                        tblUpdate.agregarCampoValor("ID_BANCO", cmbCuentaBanco.SelectedValue)
                    End If
                    tblUpdate.insertar()
                    idPago = tblUpdate.valorID
                    tblUpdate.reset()

                    Dim saldo As Decimal = Totalcancelado
                    Dim cancelado As Decimal = 0
                    Dim rec_cobrado As String = ""
                    While saldo > 0
                        For Each cell In dgvGrilla.Rows
                            If CBool(CType(cell.Cells("colChecked"), DataGridViewCheckBoxCell).Value) Then
                                Dim credito = Decimal.Parse(CType(cell.Cells("importe"), DataGridViewTextBoxCell).Value, New Globalization.CultureInfo("en-US"))
                                idCredito = CType(cell.Cells("registro"), DataGridViewTextBoxCell).Value
                                tblUpdate.tabla = "PAGO_CREDITO"
                                tblUpdate.modoID = "manual"
                                tblUpdate.agregarCampoValor("ID_CREDITO", idCredito)
                                tblUpdate.agregarCampoValor("ID_PAGO", idPago)
                                If saldo >= credito Then
                                    tblUpdate.agregarCampoValor("IMPORTE", credito)
                                    cancelado = cancelado + credito
                                    saldo = saldo - credito
                                Else
                                    tblUpdate.agregarCampoValor("IMPORTE", saldo)
                                    cancelado = cancelado + saldo
                                    saldo = saldo - saldo
                                End If
                                rec_cobrado = rec_cobrado + idCredito.ToString + ", "
                                tblUpdate.insertar()

                                tblUpdate.reset()
                            End If
                        Next

                    End While
                    rec_cobrado = Microsoft.VisualBasic.Left(rec_cobrado, rec_cobrado.Length - 2)
                    'If Not variableSesion.administrador Then
                    '    tblUpdate.tabla = "MOVIMIENTO"
                    '    tblUpdate.campoID = "ID_MOVIMIENTO"
                    '    tblUpdate.modoID = "sql"
                    '    tblUpdate.agregarCampoValor("ID_CAJA", form_principal_venta.lblIdCaja.Text)
                    '    tblUpdate.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
                    '    tblUpdate.agregarCampoValor("IMPORTE", Decimal.Parse(txtPagar.Text, New Globalization.CultureInfo("en-US")))
                    '    tblUpdate.agregarCampoValor("GLOSA", "CREDITO CLIENTE " + txtNombre.Text + " - COMP. NRO: " + rec_cobrado)
                    '    tblUpdate.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                    '    tblUpdate.insertar()
                    'Else
                    tblUpdate.tabla = "MOVIMIENTO_BANCO"
                    tblUpdate.campoID = "ID_MOVIMIENTO"
                    tblUpdate.modoID = "auto"
                    tblUpdate.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
                    tblUpdate.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
                    tblUpdate.agregarCampoValor("IMPORTE", Totalcancelado)
                    tblUpdate.agregarCampoValor("GLOSA", "CREDITO CLIENTE " + txtNombre.Text + " - COMP. NRO: " + rec_cobrado)
                    tblUpdate.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                    tblUpdate.insertar()
                    'End If

                    tblUpdate.reset()
                    scope.Complete()

                Catch ex As Exception
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Inconsistencia en la base de datos"
                    Dim msg = ex.Message.ToString
                    response = MsgBox(msg, style, title)
                End Try
            End Using
            imprimirVoucher()

            Me.Close()
            'Dim formulario As form_ventas = New form_ventas()
            Dim formPago As cuentas_x_cobrar = New cuentas_x_cobrar()
            If variableSesion.administrador Then
                mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, formPago)
            Else
                mod_conf.cambiarForm(form_principal_venta.panelContenedor, formPago)
            End If
            'Dim form = New visualizador_reporte(8, idPago)
            'form.Show()

        End If
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPagar.KeyPress, txtCancelado.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox2_Leave(sender As Object, e As EventArgs) Handles txtPagar.Leave, txtCancelado.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub TextBox2_Click(sender As Object, e As EventArgs) Handles txtPagar.Click, txtCancelado.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    'Protected Sub calcularCambio()
    '    Dim cancelado = Decimal.Parse(IIf(txtCancelado.Text = "", 0, txtCancelado.Text), New Globalization.CultureInfo("en-US"))
    '    Dim pagar = Decimal.Parse(IIf(IsNumeric(txtPagar.Text), txtPagar.Text, 0), New Globalization.CultureInfo("en-US"))
    '    Dim cambio = cancelado - pagar
    '    txtCambio.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", cambio)
    '    calcularSaldo()
    'End Sub
    'Protected Sub calcularSaldo()
    '    Dim cancelado = Decimal.Parse(txtCancelado.Text, New Globalization.CultureInfo("en-US"))
    '    Dim total = Decimal.Parse(txtTotal.Text, New Globalization.CultureInfo("en-US"))
    '    Dim saldo = total - cancelado
    '    txtSaldo.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", saldo)
    'End Sub

    'Private Sub txtPagar_TextChanged(sender As Object, e As EventArgs) Handles txtPagar.TextChanged

    '    calcularCambio()
    'End Sub

    'Private Sub txtCancelado_TextChanged(sender As Object, e As EventArgs) Handles txtCancelado.TextChanged
    '    calcularCambio()
    'End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        'Dim formulario As form_ventas = New form_ventas()
        Dim formPago As cuentas_x_cobrar = New cuentas_x_cobrar()
        If variableSesion.administrador Then
            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, formPago)
        Else
            mod_conf.cambiarForm(form_principal_venta.panelContenedor, formPago)
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirVoucherPagoCredito(e.Graphics, idPago)
    End Sub

    Private Sub imprimirVoucher()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click        
        imprimirVoucher()
    End Sub


    Private Sub dgvPagos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPagos.CellClick

        Select Case Me.dgvPagos.Columns(e.ColumnIndex).Name
            Case "colChecked"
                Dim chek As DataGridViewCheckBoxCell = CType(Me.dgvPagos.Rows(e.RowIndex).Cells("colChecked"), DataGridViewCheckBoxCell)
                chek.Value = Not chek.Value
            Case "colButton"
                Dim pago = CType(Me.dgvPagos.Rows(e.RowIndex).Cells("idPago"), DataGridViewTextBoxCell).Value
                Using Form = New comprobante_pago(pago)
                    Form.ShowDialog()
                    dtSourcePago = obtenerPagos()
                    dtSource = obtenerDeuda()
                    dgvPagos.DataSource = dtSourcePago
                    dgvGrilla.DataSource = dtSource
                End Using
        End Select
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using confirmacion = New formConfirmacion()
            If DialogResult.OK = confirmacion.ShowDialog() Then
                Dim pagos As String = ""
                Dim importe As Decimal = 0
                For Each cell In dgvPagos.Rows
                    If CBool(CType(cell.Cells("colChecked"), DataGridViewCheckBoxCell).Value) Then
                        pagos += Decimal.Parse(CType(cell.Cells("idPago"), DataGridViewTextBoxCell).Value, New Globalization.CultureInfo("en-US")).ToString + ","
                        importe += Decimal.Parse(CType(cell.Cells("importe"), DataGridViewTextBoxCell).Value, New Globalization.CultureInfo("en-US"))
                    End If
                Next
                If pagos <> "" Then
                    pagos = Microsoft.VisualBasic.Left(pagos, pagos.Length - 1)
                End If
                Using scope As New TransactionScope
                    Try
                        Dim tblUpdate = New datos()

                        tblUpdate.tabla = "MOVIMIENTO_BANCO"
                        tblUpdate.campoID = "ID_MOVIMIENTO"
                        tblUpdate.modoID = "auto"
                        tblUpdate.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
                        tblUpdate.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
                        tblUpdate.agregarCampoValor("IMPORTE", importe)
                        tblUpdate.agregarCampoValor("GLOSA", "ANULACION DE PAGO CREDITO CLIENTE " + txtNombre.Text + " - COMP. NRO(S): " + pagos)
                        tblUpdate.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
                        tblUpdate.insertar()

                        tblUpdate.reset()

                        For Each pago In pagos.Split(",")
                            tblUpdate.tabla = "PAGO_CREDITO"
                            tblUpdate.campoID = "ID_PAGO"
                            tblUpdate.valorID = pago
                            tblUpdate.eliminar()
                            tblUpdate.reset()
                            tblUpdate.tabla = "PAGOS"
                            tblUpdate.campoID = "ID_PAGO"
                            tblUpdate.valorID = pago
                            tblUpdate.eliminar()
                            tblUpdate.reset()
                        Next
                        scope.Complete()


                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Information
                        title = "Anulacion de Pago"
                        Dim msg = "El pago de Credito Fue Anulado."
                        response = MsgBox(msg, style, title)
                        
                    Catch ex As Exception
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                        title = "Error Inconsistencia en la base de datos"
                        Dim msg = ex.Message.ToString
                        response = MsgBox(msg, style, title)
                    End Try
                    
                End Using
                dtSourcePago = obtenerPagos()
                dtSource = obtenerDeuda()
                dgvPagos.DataSource = dtSourcePago
                dgvGrilla.DataSource = dtSource
            End If
        End Using        
    End Sub
End Class