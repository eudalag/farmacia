﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_informe_venta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_informe_venta))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFechaVta = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbtVta = New System.Windows.Forms.RadioButton()
        Me.rbtProducto = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFechaProducto = New System.Windows.Forms.DateTimePicker()
        Me.rbtGnral = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFechaDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFechaHasta = New System.Windows.Forms.DateTimePicker()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.rbtCaja = New System.Windows.Forms.RadioButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtInformeCaja = New System.Windows.Forms.DateTimePicker()
        Me.cmbCajas = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(62, 133)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "Fecha:"
        Me.Label1.Visible = False
        '
        'txtFechaVta
        '
        Me.txtFechaVta.Enabled = False
        Me.txtFechaVta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaVta.Location = New System.Drawing.Point(107, 131)
        Me.txtFechaVta.Name = "txtFechaVta"
        Me.txtFechaVta.Size = New System.Drawing.Size(101, 20)
        Me.txtFechaVta.TabIndex = 71
        Me.txtFechaVta.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Green
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(854, 57)
        Me.Panel1.TabIndex = 73
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 26)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Informes:"
        '
        'rbtVta
        '
        Me.rbtVta.AutoSize = True
        Me.rbtVta.Location = New System.Drawing.Point(12, 110)
        Me.rbtVta.Name = "rbtVta"
        Me.rbtVta.Size = New System.Drawing.Size(152, 17)
        Me.rbtVta.TabIndex = 75
        Me.rbtVta.TabStop = True
        Me.rbtVta.Text = "Ventas con Detalle por Dia"
        Me.rbtVta.UseVisualStyleBackColor = True
        Me.rbtVta.Visible = False
        '
        'rbtProducto
        '
        Me.rbtProducto.AutoSize = True
        Me.rbtProducto.Location = New System.Drawing.Point(12, 152)
        Me.rbtProducto.Name = "rbtProducto"
        Me.rbtProducto.Size = New System.Drawing.Size(127, 17)
        Me.rbtProducto.TabIndex = 76
        Me.rbtProducto.TabStop = True
        Me.rbtProducto.Text = "Ventas por Productos"
        Me.rbtProducto.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(62, 174)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 78
        Me.Label3.Text = "Fecha:"
        '
        'txtFechaProducto
        '
        Me.txtFechaProducto.Enabled = False
        Me.txtFechaProducto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaProducto.Location = New System.Drawing.Point(107, 172)
        Me.txtFechaProducto.Name = "txtFechaProducto"
        Me.txtFechaProducto.Size = New System.Drawing.Size(101, 20)
        Me.txtFechaProducto.TabIndex = 77
        '
        'rbtGnral
        '
        Me.rbtGnral.AutoSize = True
        Me.rbtGnral.Location = New System.Drawing.Point(12, 253)
        Me.rbtGnral.Name = "rbtGnral"
        Me.rbtGnral.Size = New System.Drawing.Size(109, 17)
        Me.rbtGnral.TabIndex = 79
        Me.rbtGnral.TabStop = True
        Me.rbtGnral.Text = "Ventas Generales"
        Me.rbtGnral.UseVisualStyleBackColor = True
        Me.rbtGnral.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(61, 276)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "Fecha Desde:"
        Me.Label4.Visible = False
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.Enabled = False
        Me.txtFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaDesde.Location = New System.Drawing.Point(139, 271)
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.Size = New System.Drawing.Size(101, 20)
        Me.txtFechaDesde.TabIndex = 80
        Me.txtFechaDesde.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(275, 276)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "Fecha Hasta:"
        Me.Label5.Visible = False
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.Enabled = False
        Me.txtFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtFechaHasta.Location = New System.Drawing.Point(352, 272)
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.Size = New System.Drawing.Size(101, 20)
        Me.txtFechaHasta.TabIndex = 82
        Me.txtFechaHasta.Visible = False
        '
        'rbtCaja
        '
        Me.rbtCaja.AutoSize = True
        Me.rbtCaja.Location = New System.Drawing.Point(12, 198)
        Me.rbtCaja.Name = "rbtCaja"
        Me.rbtCaja.Size = New System.Drawing.Size(154, 17)
        Me.rbtCaja.TabIndex = 84
        Me.rbtCaja.TabStop = True
        Me.rbtCaja.Text = "Ventas por Gestión de Caja"
        Me.rbtCaja.UseVisualStyleBackColor = True
        Me.rbtCaja.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(61, 221)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "Fecha:"
        Me.Label6.Visible = False
        '
        'txtInformeCaja
        '
        Me.txtInformeCaja.Enabled = False
        Me.txtInformeCaja.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtInformeCaja.Location = New System.Drawing.Point(107, 218)
        Me.txtInformeCaja.Name = "txtInformeCaja"
        Me.txtInformeCaja.Size = New System.Drawing.Size(101, 20)
        Me.txtInformeCaja.TabIndex = 86
        Me.txtInformeCaja.Visible = False
        '
        'cmbCajas
        '
        Me.cmbCajas.FormattingEnabled = True
        Me.cmbCajas.Location = New System.Drawing.Point(309, 217)
        Me.cmbCajas.Name = "cmbCajas"
        Me.cmbCajas.Size = New System.Drawing.Size(121, 21)
        Me.cmbCajas.TabIndex = 87
        Me.cmbCajas.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(230, 221)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 13)
        Me.Label7.TabIndex = 88
        Me.Label7.Text = "Nro. Apertura:"
        Me.Label7.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.Location = New System.Drawing.Point(767, 514)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 32)
        Me.btnAceptar.TabIndex = 74
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'form_informe_venta
        '
        Me.AcceptButton = Me.btnAceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 558)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmbCajas)
        Me.Controls.Add(Me.txtInformeCaja)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.rbtCaja)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtFechaHasta)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtFechaDesde)
        Me.Controls.Add(Me.rbtGnral)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtFechaProducto)
        Me.Controls.Add(Me.rbtProducto)
        Me.Controls.Add(Me.rbtVta)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtFechaVta)
        Me.Name = "form_informe_venta"
        Me.Text = "form_informe_venta"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFechaVta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents rbtVta As System.Windows.Forms.RadioButton
    Friend WithEvents rbtProducto As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFechaProducto As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbtGnral As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFechaDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFechaHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents rbtCaja As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtInformeCaja As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbCajas As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
