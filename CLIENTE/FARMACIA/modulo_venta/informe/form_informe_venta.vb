﻿
Imports CAPA_NEGOCIO
Public Class form_informe_venta
   
    Private Sub rbtVta_CheckedChanged(sender As Object, e As EventArgs) Handles rbtVta.CheckedChanged
        txtFechaVta.Enabled = CType(sender, RadioButton).Checked
        txtFechaProducto.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaDesde.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaHasta.Enabled = Not CType(sender, RadioButton).Checked
    End Sub

    Private Sub rbtProducto_CheckedChanged(sender As Object, e As EventArgs) Handles rbtProducto.CheckedChanged
        txtFechaVta.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaProducto.Enabled = CType(sender, RadioButton).Checked
        txtFechaDesde.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaHasta.Enabled = Not CType(sender, RadioButton).Checked
    End Sub

    Private Sub rbtGnral_CheckedChanged(sender As Object, e As EventArgs) Handles rbtGnral.CheckedChanged
        txtFechaVta.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaProducto.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaDesde.Enabled = CType(sender, RadioButton).Checked
        txtFechaHasta.Enabled = CType(sender, RadioButton).Checked
    End Sub
    
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If rbtGnral.Checked Then

        End If
        If rbtProducto.Checked Then
            Dim parametros = form_sesion.cusuario.ID_USUARIO.ToString + ";"
            parametros += txtFechaProducto.Text
            Dim form = New visualizador_reporte(4, parametros)
            form.Show()
        End If
        If rbtVta.Checked Then
            Dim form_reporte As form_reporte_ventas = New form_reporte_ventas(txtFechaVta.Text, txtFechaVta.Text)
            form_reporte.ShowDialog()
        End If
        If rbtCaja.Checked Then
            Dim form_reporte As form_reporte_caja = New form_reporte_caja(cmbCajas.SelectedValue)
            form_reporte.ShowDialog()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles rbtCaja.CheckedChanged
        txtFechaVta.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaProducto.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaDesde.Enabled = Not CType(sender, RadioButton).Checked
        txtFechaHasta.Enabled = Not CType(sender, RadioButton).Checked
        txtInformeCaja.Enabled = CType(sender, RadioButton).Checked
        cmbCajas.Enabled = CType(sender, RadioButton).Checked
        iniciarCombo(txtInformeCaja.Text)
    End Sub
    Protected Sub iniciarCombo(ByVal fecha As String)

        Dim tablaConsulta As generico = New generico()        
        cmbCajas.DataSource = tablaConsulta.obtenerCajasInforme(fecha)
        cmbCajas.DisplayMember = "ESTADO"
        cmbCajas.ValueMember = "ID_CAJA"
        cmbCajas.Refresh()
        If cmbCajas.Items.Count = 0 Then
            cmbCajas.ResetText()
        End If

    End Sub
    Private Sub txtInformeCaja_ValueChanged(sender As Object, e As EventArgs) Handles txtInformeCaja.ValueChanged
        iniciarCombo(txtInformeCaja.Text)
    End Sub
End Class
