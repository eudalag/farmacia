﻿Imports CAPA_DATOS
Public Class inf_existencia
    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        sSql += " SELECT L.ID_LABORATORIO,LTRIM(L.DESCRIPCION) AS DESCRIPCION  FROM LABORATORIO L"       
        sSql += " UNION SELECT  -1 , '[Todos Los Laboratorios]'"
        sSql += " ORDER BY DESCRIPCION "

        cmbLaboratorio.DataSource = New datos().ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
        cmbLaboratorio.Refresh()
        If cmbLaboratorio.Items.Count = 0 Then
            cmbLaboratorio.ResetText()
        End If

    End Sub

    Private Sub inf_existencia_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarCombo()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimireExistenciaLaboratorio(e.Graphics, cmbLaboratorio.Text, cmbLaboratorio.SelectedValue, chkCantidad.Checked, chkNegativo.Checked, chkDetalle.Checked)
    End Sub
    Private Sub imprimir()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        imprimir()
    End Sub
End Class