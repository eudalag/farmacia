﻿Imports System.Transactions
Imports CAPA_NEGOCIO
Imports CAPA_DATOS

Public Class form_cierre    
    Dim totalMovimientos As Decimal
    Dim totalPedidos As Decimal

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql += " SELECT B.ID_BANCO , NOMBRE_APELLIDOS + ' - ' + TB.DESC_TIPO_BANCO AS NOMBRE_APELLIDOS  FROM USUARIO U"
        sSql += " INNER JOIN BANCO B ON B.ID_USUARIO = U.ID_USUARIO "
        sSql += " INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += " WHERE U.ID_USUARIO <> " + form_sesion.cusuario.ID_USUARIO.ToString
        sSql += " UNION ALL SELECT -1 , '[Seleccione Un Usuario]'"
        sSql += " ORDER BY NOMBRE_APELLIDOS"


        cmbUsuario.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        cmbUsuario.ValueMember = "ID_BANCO"
    End Sub

    Private Sub form_cierre_Load(sender As Object, e As EventArgs) Handles MyBase.Load        
        iniciarComboLaboratorio()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Function numeroValido()
        Dim valido As Boolean = True
        If Not IsNumeric(txtBolivianos.Text) Then
            valido = False
            Return valido
        End If

        Return valido
    End Function

    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True

        If cmbUsuario.SelectedValue = -1 Then
            mensaje = "Seleccione un Usuario valido."
            ErrorProvider1.SetError(cmbUsuario, mensaje)
            valido = False
        End If        
        Return valido
    End Function
    Function obtenerUsuarioRecepcion(ByVal idBanco As Integer)
        Dim sSql As String = ""
        sSql += " SELECT ID_USUARIO FROM BANCO WHERE ID_BANCO = " + idBanco.ToString
        Return New datos().ejecutarConsulta(sSql).Rows(0)(0)
    End Function
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If numeroValido() Then
            If validarformulario() Then
                Using scope As New TransactionScope()
                    Try
                        Dim gen = New generico()
                        Dim total As Decimal
                        total = Decimal.Parse(txtSobranteCaja.Text, New Globalization.CultureInfo("en-US"))
                        procesarTransferenciaBanco()

                        totalMovimientos = gen.movimientos(form_principal_venta.lblIdCaja.Text)
                        totalPedidos = gen.pedidos(form_sesion.cusuario.ID_USUARIO, form_principal_venta.lblIdCaja.Text)
                        Dim excedente As Decimal = total - (totalPedidos + totalMovimientos)
                        If excedente <> 0 Then
                            Dim motivo As String = ""
                            If excedente > 0 Then
                                motivo = "Sobrante al cierre de Caja"
                            Else
                                motivo = "Faltante al cierre de Caja"
                            End If
                            gen.insertarMovimientoEconomico(form_principal_venta.lblIdCaja.Text, 3, excedente, motivo)                           
                        End If
                        gen.cerrarCaja(form_principal_venta.lblIdCaja.Text, total)

                        Dim tbl = New datos()
                        tbl.tabla = "CIERRE"
                        tbl.campoID = "ID_CIERRE"
                        tbl.modoID = "sql"
                        tbl.agregarCampoValor("BOLIVIANOS", total)

                        tbl.agregarCampoValor("EXCEDENTE", excedente)
                        tbl.agregarCampoValor("TC", form_sesion.cConfiguracion.TC_ME)
                        tbl.agregarCampoValor("ID_CAJA", form_principal_venta.lblIdCaja.Text)
                        tbl.agregarCampoValor("ID_USUARIO_REC", obtenerUsuarioRecepcion(cmbUsuario.SelectedValue))
                        tbl.insertar()
                        registrarMovimientoBancoCaja(total, 1)
                        If excedente <> 0 Then
                            registrarMovimientoBancoCaja(excedente, 3)
                        End If
                        scope.Complete()
                        Me.DialogResult = Windows.Forms.DialogResult.OK
                    Catch ex As Exception

                    End Try
                End Using

                

                

                
                'Dim form = New visualizador_reporte(6, form_principal_venta.lblIdCaja.Text)
                'form.Show()

            End If
        Else
            Dim mensaje = New modulo()
            mensaje.mensajeError("Error", "Los importes ingresados no son correctos, revise e intente nuevamente.")
        End If
    End Sub

    Private Sub txtExcedente_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBolivianos.KeyPress, txtSobranteCaja.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub txtExcedente_Click(sender As Object, e As EventArgs) Handles txtBolivianos.Click, txtSobranteCaja.Click
        Dim txt = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    'Private Sub txtExcedente_TextChanged(sender As Object, e As EventArgs) Handles txtExcedente.TextChanged
    '    calcularTotal()
    'End Sub

    Private Sub form_cierre_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.Escape
                btnVolver_Click(Nothing, Nothing)
        End Select
    End Sub
    Protected Sub procesarTransferenciaBanco()

        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "sql"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")) * -1)
        tbl.agregarCampoValor("GLOSA", "Transferencia de Cierre de Caja")
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_CAJA", form_principal_venta.lblIdCaja.Text)
        tbl.insertar()
        tbl.reset()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("GLOSA", "Transferencia de Cierre de Caja")
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", cmbUsuario.SelectedValue)
        tbl.insertar()

    End Sub

    Protected Sub registrarMovimientoBancoCaja(ByVal importe As Decimal, ByVal idTipoMovimiento As Integer)
        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", idTipoMovimiento)
        tbl.agregarCampoValor("IMPORTE", importe)
        If idTipoMovimiento = 1 Then
            tbl.agregarCampoValor("GLOSA", "Movimiento de Cierre de Caja")
        Else
            tbl.agregarCampoValor("GLOSA", IIf(importe < 0, "Faltante de Caja", "Sobrante de Caja"))
        End If
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
        tbl.insertar()
    End Sub
    Protected Sub calcularTotalCierre()
        Dim transferencia As Decimal = Decimal.Parse(IIf(Not IsNumeric(txtBolivianos.Text), 0, txtBolivianos.Text), New Globalization.CultureInfo("en-US"))
        Dim sobranteCaja As Decimal = Decimal.Parse(IIf(Not IsNumeric(txtSobranteCaja.Text), 0, txtSobranteCaja.Text), New Globalization.CultureInfo("en-US"))
        If Not IsNumeric(transferencia) Then
            transferencia = 0
        End If
        If Not IsNumeric(sobranteCaja) Then
            sobranteCaja = 0
        End If
        txtEfectivo.Text = transferencia + sobranteCaja
    End Sub

    Private Sub txtBolivianos_TextChanged(sender As Object, e As EventArgs) Handles txtBolivianos.TextChanged
        calcularTotalCierre()
    End Sub

    Private Sub txtSobranteCaja_TextChanged(sender As Object, e As EventArgs) Handles txtSobranteCaja.TextChanged
        calcularTotalCierre()
    End Sub
End Class