﻿Imports CAPA_NEGOCIO
Public Class form_apertura
    Dim idCaja As Integer = -1
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Public Sub New(ByVal _idCaja As Integer)
        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        idCaja = _idCaja
    End Sub
    Private Function numeroValido()
        Dim valor As Boolean = True
        valor = txtImporte.Text.Length > 0
        Return valor
    End Function
    Private Sub txtImporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImporte.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If numeroValido() Then
            Dim gen = New generico()
            If CInt(idCaja) <> -1 Then
                gen.modificarCaja(txtImporte.Text, idCaja)
            Else
                gen.aperturarCaja(form_sesion.cusuario.ID_USUARIO, txtImporte.Text, form_principal_venta.lblIdCaja.Text, variableSesion.idBanco)
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Dim mensaje = New modulo()
            mensaje.mensajeError("Error", "El importe  que desea introducir no es valido revise e intente nuevamente.")
        End If
    End Sub

    Private Sub txtImporte_Click(sender As Object, e As EventArgs) Handles txtImporte.Click
        Dim txt = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtImporte_Leave(sender As Object, e As EventArgs) Handles txtImporte.Leave
        Dim txt = CType(sender, TextBox)
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txtImporte.Text = result
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub form_apertura_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.Escape
                btnVolver_Click(Nothing, Nothing)
        End Select
    End Sub

End Class