﻿Imports System.Transactions
Imports CAPA_DATOS
Public Class form_transferencia

    Private Sub txtBolivianos_Click(sender As Object, e As EventArgs) Handles txtBolivianos.Click
        Dim txt = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub
    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql += " DECLARE @USUARIO AS INTEGER;"
        sSql += " SET @USUARIO = " + variableSesion.idUsuario.ToString
        sSql += " SELECT B.ID_BANCO , NOMBRE_APELLIDOS + ' - ' + TB.DESC_TIPO_BANCO AS NOMBRE_APELLIDOS  FROM USUARIO U"
        sSql += " INNER JOIN BANCO B ON B.ID_USUARIO = U.ID_USUARIO "
        sSql += " INNER JOIN TIPO_BANCO TB ON TB.ID_TIPO_BANCO = B.ID_TIPO_BANCO "
        sSql += " WHERE U.ID_USUARIO <> @USUARIO"
        sSql += " UNION ALL SELECT -1 , '[Seleccione Un Usuario]'"
        sSql += " ORDER BY NOMBRE_APELLIDOS"


        cmbUsuario.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        cmbUsuario.ValueMember = "ID_BANCO"
    End Sub
    Private Sub txtBolivianos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBolivianos.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True

        If cmbUsuario.SelectedValue = -1 Then
            mensaje = "Seleccione un Usuario valido."
            ErrorProvider1.SetError(cmbUsuario, mensaje)
            valido = False
        End If
        If txtConcepto.Text.Trim = "" Then
            mensaje = "Ingrese una descripcion de la transferencia a realizar"
            ErrorProvider1.SetError(txtConcepto, mensaje)
            valido = False
        End If
        Return valido
    End Function
    Function numeroValido()
        Dim valido As Boolean = True
        If Not IsNumeric(txtBolivianos.Text) Then
            valido = False
            Return valido
        End If

        Return valido
    End Function
    Protected Sub procesarTransferenciaBanco()

        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")) * -1)
        tbl.agregarCampoValor("GLOSA", "Transferencia a " + cmbUsuario.Text + "; " + txtConcepto.Text)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", variableSesion.idBanco)
        tbl.insertar()
        tbl.reset()
        tbl.tabla = "MOVIMIENTO_BANCO"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "auto"
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 5)
        tbl.agregarCampoValor("IMPORTE", Decimal.Parse(txtBolivianos.Text, New Globalization.CultureInfo("en-US")))
        tbl.agregarCampoValor("GLOSA", "Transferencia de " + variableSesion.Usuario + "; " + txtConcepto.Text)
        tbl.agregarCampoValor("FH_MOVIMIENTO", "serverDateTime")
        tbl.agregarCampoValor("ID_BANCO", cmbUsuario.SelectedValue)
        tbl.insertar()

    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If numeroValido() Then
            If validarformulario() Then
                Using scope As New TransactionScope()
                    Try
                        Using confirmacion = New formConfirmacion(2)
                            If DialogResult.OK = confirmacion.ShowDialog() Then
                                procesarTransferenciaBanco()
                                scope.Complete()
                                Me.DialogResult = Windows.Forms.DialogResult.OK
                            End If
                        End Using
                        
                        
                    Catch ex As Exception

                    End Try
                End Using                
            End If
        End If
       
    End Sub



    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub form_transferencia_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarComboLaboratorio()
    End Sub
End Class