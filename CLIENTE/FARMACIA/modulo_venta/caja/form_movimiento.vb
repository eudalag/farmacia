﻿Imports CAPA_NEGOCIO
Public Class form_movimiento

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub txtImporte_Click(sender As Object, e As EventArgs) Handles txtImporte.Click
        Dim txt = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtImporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImporte.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If numeroValido() Then
            Dim gen = New generico()
            If RadioButton1.Checked Then
                gen.insertarMovimientoEconomico(form_principal_venta.lblIdCaja.Text, 2, txtImporte.Text, txtConcepto.Text)
            End If
            If RadioButton2.Checked Then
                gen.insertarMovimientoEconomico(form_principal_venta.lblIdCaja.Text, 1, txtImporte.Text, txtConcepto.Text)
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Dim mensaje = New modulo()
            mensaje.mensajeError("Error", "Revise el concepto o la cantidad a registrar.")
        End If
    End Sub

    Private Sub form_movimiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Private Function numeroValido()
        Dim valor As Boolean = True
        valor = txtImporte.Text.Length > 0
        If valor Then
            valor = CInt(txtImporte.Text) > 0
        End If
        Return valor And txtConcepto.Text.Length > 0
    End Function

    Private Sub form_movimiento_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.Escape
                btnVolver_Click(Nothing, Nothing)
        End Select
    End Sub
End Class