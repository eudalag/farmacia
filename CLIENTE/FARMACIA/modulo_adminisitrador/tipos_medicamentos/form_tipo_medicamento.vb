﻿Imports CAPA_NEGOCIO
Imports System.Data
Public Class form_tipo_medicamento
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1103
            .Height = 360
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 3
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_TIPO_MEDICAMENTO"
            .Columns(0).Width = 90

            .Columns(1).Name = "descripcion"
            .Columns(1).HeaderText = "Descripción"
            .Columns(1).DataPropertyName = "DESCRIPCION"
            .Columns(1).Width = 902

            .Columns(2).Name = "Activo"
            .Columns(2).HeaderText = "Activo"
            .Columns(2).DataPropertyName = "ACTIVO"
            .Columns(2).Width = 110

        End With
        LoadPage()

    End Sub

    Private Sub form_tipo_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tTipoMedicamento As eTipoMedicamento = New eTipoMedicamento()
        dtSource = tTipoMedicamento.mostrarTipoMedicamento()
        inicializarParametros()
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New form_gestionTipo_medicamento()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idTipoMedicamento As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Me.Close()
        Dim form_gestion As form_gestionTipo_medicamento = New form_gestionTipo_medicamento(idTipoMedicamento)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim idTipoMedicamento As Integer = 0
        idTipoMedicamento = dgvGrilla.SelectedCells(0).Value
        Dim tTipoMedicamento As eTipoMedicamento = New eTipoMedicamento()
        tTipoMedicamento.ID_TIPO_MEDICAMENTO = idTipoMedicamento

        tTipoMedicamento.eliminar()
        If tTipoMedicamento.mensajesError = "" Then
            dtSource = tTipoMedicamento.mostrarTipoMedicamento()
            Me.totalRegistro = dtSource.Rows.Count
            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            Me.paginaActual = 1
            Me.regNro = 0
            LoadPage()
        Else
            mod_conf.mensajeError("Error", tTipoMedicamento.mensajesError)
        End If
    End Sub
End Class