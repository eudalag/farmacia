﻿Imports CAPA_NEGOCIO

Public Class form_gestionTipo_medicamento
    Private tTipoMedicamento As eTipoMedicamento
    Private mod_conf As modulo = New modulo()
    Public Sub New(ByVal idTipoMedicamento As Integer)
        InitializeComponent()
        tTipoMedicamento = New eTipoMedicamento(idTipoMedicamento)
        cargarDatos()
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tTipoMedicamento = New eTipoMedicamento()
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            tTipoMedicamento.DESCRIPCION = txtDescripcion.Text
            tTipoMedicamento.ACTIVO = chkActivo.Checked
            If tTipoMedicamento.ID_TIPO_MEDICAMENTO = 0 Then
                tTipoMedicamento.insertar()
            Else
                tTipoMedicamento.modificar()
            End If
            Me.Close()
            mod_conf.volver(New form_tipo_medicamento())
        End If
    End Sub
    Private Sub cargarDatos()
        txtDescripcion.Text = tTipoMedicamento.DESCRIPCION
        chkActivo.Checked = tTipoMedicamento.ACTIVO
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtDescripcion.Text).Length > 0 Then
            ErrorProvider1.SetError(txtDescripcion, "")
        Else
            mensaje = "Complete el campo Descripción."
            ErrorProvider1.SetError(txtDescripcion, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        mod_conf.volver(New form_tipo_medicamento())
    End Sub

    Private Sub form_gestionTipo_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtDescripcion.Focus()
    End Sub
End Class