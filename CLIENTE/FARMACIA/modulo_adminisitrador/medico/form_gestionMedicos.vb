﻿Imports CAPA_DATOS
Public Class form_gestionMedicos
    Private _idMedico As Integer = 0
    Private mod_conf As modulo = New modulo()

    Public Sub New(ByVal idMedico As Integer)
        InitializeComponent()
        If idMedico > 0 Then
            _idMedico = idMedico
            Dim sSql As String = "SELECT * FROM MEDICO WHERE ID_MEDICO = " + idMedico.ToString
            cargarDatos(New datos().ejecutarConsulta(sSql))
        End If
    End Sub
    Private Sub cargarDatos(ByVal tbl As DataTable)
        txtNombre.Text = tbl.Rows(0)("NOMBRE")
        txtEspecialidad.Text = tbl.Rows(0)("ESPECIALIDAD")
        txtMatricula.Text = tbl.Rows(0)("MATRICULA")
        txtTelefono.Text = tbl.Rows(0)("TELEFONO")
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub


    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If Modal Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            volver()
        End If      
    End Sub
    Private Sub volver()
        Me.Close()
        mod_conf.volver(New form_medico())
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim tMedico = New datos()
            tMedico.tabla = "MEDICO"
            tMedico.campoID = "ID_MEDICO"
            tMedico.modoID = "sql"
            tMedico.agregarCampoValor("NOMBRE", txtNombre.Text)
            tMedico.agregarCampoValor("ESPECIALIDAD", txtEspecialidad.Text)
            tMedico.agregarCampoValor("MATRICULA", txtMatricula.Text)
            tMedico.agregarCampoValor("TELEFONO", txtTelefono.Text)
            
            If _idMedico = 0 Then
                tMedico.insertar()
            Else
                tMedico.valorID = _idMedico
                tMedico.modificar()
            End If
            If Modal Then
                form_buscarMedico.buscar = txtNombre.Text
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                volver()
            End If

        End If

    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre del Proveedor."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub form_gestionCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtEspecialidad.Focus()
        End If
    End Sub
    Private Sub form_gestionCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub
End Class