﻿Imports CAPA_DATOS
Public Class form_gestionPaciente

    Private idPaciente As Integer = 0
    Private mod_conf As modulo = New modulo()
    Private modal_cliente As Boolean = False

    Public Sub New(ByVal id As Integer, ByVal modal As Boolean)
        InitializeComponent()
        idPaciente = id
        cargarDatos()
        modal_cliente = modal
    End Sub
   
    Private Sub cargarDatos()
        If idPaciente <> -1 Then
            Dim sSql = "SELECT * FROM PACIENTE WHERE ID_PACIENTE = " + idPaciente.ToString
            Dim tblConsulta = New datos().ejecutarConsulta(sSql)
            txtNombre.Text = tblConsulta.Rows(0)("NOMBRE_APELLIDOS")
            txtCI.Text = tblConsulta.Rows(0)("CI")
            txtCelular.Text = tblConsulta.Rows(0)("CELULAR")
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If modal_cliente Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            volver()
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim tblConsulta = New datos()
            tblConsulta.tabla = "PACIENTE"
            tblConsulta.campoID = "ID_PACIENTE"
            tblConsulta.modoID = "sql"
            tblConsulta.valorID = IIf(idPaciente = -1, "", idPaciente)
            tblConsulta.agregarCampoValor("NOMBRE_APELLIDOS", txtNombre.Text)
            tblConsulta.agregarCampoValor("CI", txtCI.Text)
            tblConsulta.agregarCampoValor("CELULAR", txtCelular.Text)
            If idPaciente = -1 Then
                tblConsulta.insertar()
            Else
                tblConsulta.modificar()
            End If
            form_pedido._registroPaciente = tblConsulta.valorID
            Me.DialogResult = Windows.Forms.DialogResult.OK

        End If
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre y Apellido."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If
        Return valido
    End Function
    Private Sub volver()
        If form_sesion.cusuario.ADMINISTRADOR Then
            Me.Close()
            mod_conf.volver(New form_cliente())
        Else
            Me.Close()
            mod_conf.volver_venta(New form_pedido())
        End If
    End Sub

    Private Sub form_gestionCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtCI.Focus()
        End If
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCI.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        Else
            e.Handled = True
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 8 Then
            e.Handled = False
        End If
    End Sub

    Private Sub form_gestionCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub
End Class