﻿Imports CAPA_NEGOCIO
Public Class form_informacion_basica

    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub form_informacion_basica_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim infor = New eInformacion()
        lblFechaEmision.Text = infor.obtenerFechaEmision
        If lblFechaEmision.Text <> "" Then
            Dim fEmision As Date = CDate(lblFechaEmision.Text)
            If fEmision <= Now.Date Then
                lblFechaEmision.ForeColor = Color.Red
            ElseIf fEmision.AddDays(-15) <= Now.Date Then
                lblFechaEmision.ForeColor = Color.Blue
            End If
        End If
        lblStock.Text = infor.obtenerCantProdStock.Rows.Count
        lblProductosPorVencerse.Text = infor.obtenerCantProdVencerse
        lblProductosVencidos.Text = infor.obtenerCantProdVencidos
    End Sub
End Class