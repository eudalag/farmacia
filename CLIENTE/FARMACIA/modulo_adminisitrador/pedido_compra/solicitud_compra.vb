﻿Imports CAPA_DATOS
Public Class solicitud_compra
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 20
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Protected Sub iniciarComboEstado()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT * FROM ESTADO_PEDIDO ORDER BY ESTADO"

        cmbEstado.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbEstado.DisplayMember = "ESTADO"
        cmbEstado.ValueMember = "ID_ESTADO"
    End Sub
    Protected Sub iniciarComboProveedores()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_PROVEEDOR, NOMBRE FROM PROVEEDORES WHERE ID_PROVEEDOR <> 10004 UNION ALL "
        sSql += " SELECT -1 , '[Todos los Proveedores]' ORDER BY NOMBRE"

        cmbProveedor.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbProveedor.DisplayMember = "NOMBRE"
        cmbProveedor.ValueMember = "ID_PROVEEDOR"
    End Sub

    Private Sub solicitud_compra_Load(sender As Object, e As EventArgs) Handles Me.Load
        dtSource = mostrarSolicitudes(-1, 0)
        inicializarParametros()
        iniciarComboEstado()
        cmbEstado.SelectedValue = 1
        iniciarComboProveedores()
        txtNroSolicitud.Focus()
    End Sub
    Function mostrarSolicitudes(ByVal estado As Integer, ByVal proveedor As Integer) As DataTable
        Dim tbl As New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ESTADO AS INTEGER, @PROVEEDOR AS INTEGER, @NRO_SOLICITUD AS NVARCHAR (15);"
        sSql += " SET @ESTADO = " + estado.ToString + ";"
        sSql += " SET @PROVEEDOR = " + proveedor.ToString + ";"

        sSql += " SET @NRO_SOLICITUD = '" + txtNroSolicitud.Text + "';"
        sSql += " SELECT NRO_ORDEN,CONVERT(NVARCHAR(10),FECHA,103) AS FECHA, P.NOMBRE AS PROVEEDOR, ESTADO,OC.ID_ORDEN_COMPRA   FROM ORDEN_COMPRA OC"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = OC.ID_PROVEEDOR "
        sSql += " INNER JOIN ESTADO_PEDIDO EP ON EP.ID_ESTADO = OC.ID_ESTADO"
        sSql += " WHERE EP.ID_ESTADO = @ESTADO AND P.ID_PROVEEDOR = CASE WHEN @PROVEEDOR = -1 THEN P.ID_PROVEEDOR ELSE @PROVEEDOR END "
        sSql += " AND NRO_ORDEN LIKE '%' + @NRO_SOLICITUD + '%'"
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1120
            .Height = 480
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 5
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Nro. de Solicitud"
            .Columns(0).DataPropertyName = "NRO_ORDEN"
            .Columns(0).Width = 140

            .Columns(1).Name = "fecha"
            .Columns(1).HeaderText = "Fecha"
            .Columns(1).DataPropertyName = "FECHA"
            .Columns(1).Width = 80


            .Columns(2).Name = "proveedor"
            .Columns(2).HeaderText = "Proveedor"
            .Columns(2).DataPropertyName = "PROVEEDOR"
            .Columns(2).Width = 750

            .Columns(3).Name = "estado"
            .Columns(3).HeaderText = "Estado"
            .Columns(3).DataPropertyName = "ESTADO"
            .Columns(3).Width = 150

            .Columns(4).Name = "ID"
            .Columns(4).DataPropertyName = "ID_ORDEN_COMPRA"
            .Columns(4).Visible = False
        End With
        LoadPage()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs)
        Dim form = New form_gestion_pedido()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idOrdenCompra As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(4), DataGridViewTextBoxCell).Value
        Dim form = New detalle_solicitud_compra(idOrdenCompra)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        
        dtSource = mostrarSolicitudes(cmbEstado.SelectedValue, cmbProveedor.SelectedValue)
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub cmbEstado_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbEstado.SelectedValueChanged
        If IsNumeric(cmbEstado.SelectedValue) Then
            dtSource = mostrarSolicitudes(cmbEstado.SelectedValue, cmbProveedor.SelectedValue)
            Me.totalRegistro = dtSource.Rows.Count
            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            Me.paginaActual = 1
            Me.regNro = 0
            LoadPage()
        End If
       
    End Sub

    Private Sub cmbProveedor_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbProveedor.SelectedValueChanged
        If IsNumeric(cmbProveedor.SelectedValue) Then
            dtSource = mostrarSolicitudes(cmbEstado.SelectedValue, cmbProveedor.SelectedValue)
            Me.totalRegistro = dtSource.Rows.Count
            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            Me.paginaActual = 1
            Me.regNro = 0
            LoadPage()
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim idOrdenCompra As String = dgvGrilla.SelectedCells(4).Value
        Dim form = New visualizador_reporte(3, idOrdenCompra)
        form.Show()
    End Sub
End Class