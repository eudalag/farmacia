﻿Imports CAPA_DATOS
Public Class verDetalleSolicitud
    Private idOrdenCompra As Integer
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Public Sub New(ByVal _idOrdenCompra As Integer)
        InitializeComponent()
        idOrdenCompra = _idOrdenCompra
        cargarDatosCabecera(_idOrdenCompra)
        cargarDatosMovimientos(_idOrdenCompra)
    End Sub
    Protected Sub cargarDatosCabecera(ByVal idSolicitud As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_COMPRA AS INTEGER;"
        sSql += " SET @ID_COMPRA = " + idSolicitud.ToString + ";"
        sSql += " SELECT RIGHT('000000' + CAST(NRO_ORDEN AS NVARCHAR(6)),6) AS NRO_SOLICITUD,P.NIT, P.NOMBRE AS PROVEEDOR,"
        sSql += " OC.OBSERVACION FROM ORDEN_COMPRA OC"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = OC.ID_PROVEEDOR "
        sSql += " WHERE OC.ID_ORDEN_COMPRA = @ID_COMPRA "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtNroSolicitud.Text = tbl.Rows(0)("NRO_SOLICITUD")
            txtNit.Text = tbl.Rows(0)("NIT")
            txtProveedor.Text = tbl.Rows(0)("PROVEEDOR")
            txtObservacion.Text = tbl.Rows(0)("OBSERVACION")
        End If
    End Sub
    Protected Sub cargarDatosMovimientos(ByVal idSolicitud As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_ORDEN AS INTEGER;"
        sSql += " SET @ID_ORDEN = " + idSolicitud.ToString + ";"
        sSql += " SELECT M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO, FF.DESCRIPCION AS FORMA_FARMACEUTICA, L.DESCRIPCION AS LABORATORIO,I.CANTIDAD_SOLICITADA FROM INSUMOS I "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO  "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE I.ID_ORDEN_COMPRA = @ID_ORDEN"

        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 610
            .Height = 250
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 200

            .Columns(1).Name = "forma_farmaceutica"
            .Columns(1).HeaderText = "Forma Farmaceutica"
            .Columns(1).DataPropertyName = "FORMA_FARMACEUTICA"
            .Columns(1).Width = 150
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "laboratorio"
            .Columns(2).HeaderText = "Laboratorio"
            .Columns(2).DataPropertyName = "LABORATORIO"
            .Columns(2).Width = 150

            .Columns(3).Name = "cantidad"
            .Columns(3).HeaderText = "Cantidad"
            .Columns(3).DataPropertyName = "CANTIDAD_SOLICITADA"
            .Columns(3).Width = 100
        End With
        LoadPage()
    End Sub

    Private Sub verDetalleSolicitud_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class