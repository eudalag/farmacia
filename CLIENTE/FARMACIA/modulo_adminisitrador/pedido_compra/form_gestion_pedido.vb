﻿Imports CAPA_DATOS
Public Class form_gestion_pedido
    Private mod_conf As modulo = New modulo()

    Public Shared dtSource As DataTable

    Public Sub New()
        InitializeComponent()
        iniciarComboProveedores()
        crearTabla()
        txtFecha.Text = Now().ToString("dd/MM/yyyy")
    End Sub

    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "MEDICAMENTO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "COMPOSICION"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "LABORATORIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "FORMA_FARMACEUTICA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Decimal")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        dtSource = tblItems
    End Sub

    Private Sub LoadPage()
        Dim dataView As New DataView(dtSource.Select("ACCION <> 3").CopyToDataTable)
        dataView.Sort = "NRO_FILA ASC"
        dgvGrilla.DataSource = dataView.ToTable()
    End Sub
    
    Protected Sub iniciarComboProveedores()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_PROVEEDOR, NOMBRE FROM PROVEEDORES WHERE ID_PROVEEDOR <> 10004 UNION ALL "
        sSql += " SELECT -1 , '[Seleccione un Proveedor]' ORDER BY NOMBRE"

        cmbProveedor.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbProveedor.DisplayMember = "NOMBRE"
        cmbProveedor.ValueMember = "ID_PROVEEDOR"
    End Sub
    

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged
        Dim sSql As String = ""
        sSql += " SELECT NOMBRE,ID_PROVEEDOR FROM PROVEEDORES WHERE NIT = '" + txtNit.Text.ToString + "'"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            cmbProveedor.SelectedValue = tblConsulta.Rows(0)("ID_PROVEEDOR")
        Else
            cmbProveedor.SelectedValue = -1
        End If
    End Sub

    Private Sub cmbProveedor_TextChanged(sender As Object, e As EventArgs) Handles cmbProveedor.TextChanged
        Dim sSql As String = ""
        Try
            sSql += " SELECT NIT,ID_PROVEEDOR FROM PROVEEDORES WHERE ID_PROVEEDOR = '" + cmbProveedor.SelectedValue.ToString + "'"
            Dim tblConsulta = New datos().ejecutarConsulta(sSql)
            If tblConsulta.Rows.Count > 0 Then
                txtNit.Text = tblConsulta.Rows(0)("NIT")
            Else
                cmbProveedor.SelectedValue = -1
            End If
        Catch ex As Exception

        End Try
       
    End Sub

    Private Sub inicializarParametrosCompra()
        With (dgvGrilla)
            .Width = 1120
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 7
            .Columns(0).Name = "registro"
            .Columns(0).HeaderText = "registro"
            .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(0).Width = 100

            .Columns(1).Name = "medicamento"
            .Columns(1).HeaderText = "Medicamento"
            .Columns(1).DataPropertyName = "MEDICAMENTO"
            .Columns(1).Width = 320
            .Columns(1).ReadOnly = True

            .Columns(2).Name = "composicion"
            .Columns(2).HeaderText = "Composicion"
            .Columns(2).DataPropertyName = "COMPOSICION"
            .Columns(2).Width = 300
            .Columns(2).ReadOnly = True

            .Columns(3).Name = "laboratorio"
            .Columns(3).HeaderText = "Laboratorio"
            .Columns(3).DataPropertyName = "LABORATORIO"
            .Columns(3).Width = 150
            .Columns(3).ReadOnly = True

            .Columns(4).Name = "forma_farmaceutica"
            .Columns(4).HeaderText = "F. Farmaceutica"
            .Columns(4).DataPropertyName = "FORMA_FARMACEUTICA"
            .Columns(4).Width = 150
            .Columns(4).ReadOnly = True

            .Columns(5).Name = "cantidad"
            .Columns(5).HeaderText = "Cantidad"
            .Columns(5).DataPropertyName = "CANTIDAD"
            .Columns(5).Width = 100

            .Columns(6).Name = "nro_fila"
            .Columns(6).DataPropertyName = "NRO_FILA"
            .Columns(6).Visible = False
        End With
    End Sub

    Protected Sub actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For cont = 0 To dgvGrilla.Rows.Count - 1
            For Each tabla In tblItems.Rows
                If CInt(tabla("NRO_FILA")) = CInt(dgvGrilla.Rows(cont).Cells(6).Value) And dgvGrilla.Rows(cont).Cells(0).Value.ToString <> "" Then
                    Dim nroFila As Integer = dgvGrilla.Rows(cont).Cells(6).Value
                    Dim idMedicamento As String = dgvGrilla.Rows(cont).Cells(0).Value
                    Dim nombre As String = dgvGrilla.Rows(cont).Cells(1).Value
                    Dim composicion As String = mod_conf.esNulo(dgvGrilla.Rows(cont).Cells(2).Value)
                    Dim laboratorio As String = dgvGrilla.Rows(cont).Cells(3).Value
                    Dim formaFarmaceutica As String = dgvGrilla.Rows(cont).Cells(4).Value
                    Dim cantidad As Decimal = dgvGrilla.Rows(cont).Cells(5).Value


                    If tabla("ID_MEDICAMENTO").ToString <> idMedicamento Or tabla("CANTIDAD").ToString <> cantidad.ToString Or tabla("MEDICAMENTO") <> nombre Or _
                        tabla("COMPOSICION").ToString <> composicion Or tabla("LABORATORIO").ToString <> laboratorio Or tabla("FORMA_FARMACEUTICA").ToString <> formaFarmaceutica Then
                        tabla("ACCION") = 2
                        tabla("NRO_FILA") = IIf(nroFila = 999, dgvGrilla.Rows.Count, nroFila)
                        tabla("ID_MEDICAMENTO") = idMedicamento
                        tabla("MEDICAMENTO") = nombre
                        tabla("COMPOSICION") = composicion
                        tabla("LABORATORIO") = laboratorio
                        tabla("FORMA_FARMACEUTICA") = formaFarmaceutica
                        tabla("CANTIDAD") = cantidad
                    End If
                End If
            Next
        Next
        dtSource = tblItems
    End Sub

    
    Public Shared Sub nuevoItem(ByVal idMedicamento As String, ByVal nombre As String, ByVal composicion As String, ByVal laboratorio As String, _
                                ByVal forma_farmaceutica As String, ByVal minimo As Integer, ByVal maximo As Integer, ByVal cantidad As Integer, _
                                ByVal nuevo As Integer, ByVal accion As Integer)
        Dim tblItems As DataTable = dtSource
        Dim dr As DataRow
        dr = tblItems.NewRow

        dr("NRO_FILA") = IIf(idMedicamento = "0", 999, dtSource.Rows.Count)
        dr("ID_MEDICAMENTO") = IIf(idMedicamento = "0", DBNull.Value, idMedicamento)
        dr("MEDICAMENTO") = nombre
        dr("COMPOSICION") = IIf(idMedicamento = "0", DBNull.Value, composicion)
        dr("LABORATORIO") = IIf(idMedicamento = "0", DBNull.Value, laboratorio)
        dr("FORMA_FARMACEUTICA") = IIf(idMedicamento = 0, DBNull.Value, forma_farmaceutica)
        dr("CANTIDAD") = IIf(idMedicamento = "0", DBNull.Value, cantidad)
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblItems.Rows.Add(dr)
        tblItems.AcceptChanges()
    End Sub
    Private Sub eliminar()
        actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For Each row As DataRow In tblItems.Rows
            If CStr(row("NRO_FILA")) = dgvGrilla.SelectedCells(6).Value Then
                row("ACCION") = 3
                Exit For
            End If
        Next
        dtSource.AcceptChanges()
        LoadPage()
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs)
        eliminar()
    End Sub

    Private Sub form_gestion_pedido_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametrosCompra()
        iniciarItems()
        LoadPage()
    End Sub
    'Protected Sub cargarGrilla()
    '    Dim sSql As String = ""
    '    sSql += " SELECT ROW_NUMBER() OVER (ORDER BY M.ID_MEDICAMENTO) AS NRO_ITEM,RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO, "
    '    sSql += " M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO , COMPOSICION, L.DESCRIPCION AS LABORATORIO, "
    '    sSql += " TM.DESCRIPCION AS FORMA_FARMACEUTICA,SUM(I.CANTIDAD_SOLICITADA ) AS CANTIDAD  FROM INSUMOS I"
    '    sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
    '    sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_MEDICAMENTO "
    '    sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
    '    sSql += " WHERE I.ID_ORDEN_COMPRA = " + lblIdSolicitud.Text
    '    sSql += " GROUP BY M.ID_MEDICAMENTO, M.NOMBRE,M.CONCENTRACION,M.COMPOSICION,L.DESCRIPCION,TM.DESCRIPCION"
    '    Dim tbl = New datos().ejecutarConsulta(sSql)
    '    For Each item In tbl.Rows
    '        nuevoItem(item("ID_MEDICAMENTO"), item("MEDICAMENTO"), item("COMPOSICION"), item("LABORATORIO"), item("FORMA_FARMACEUTICA"), item("CANTIDAD"), 0, 1)
    '    Next
    'End Sub
    Protected Sub iniciarItems()
        nuevoItem("0", "", "", "", "", 0, 0, 0, 1, 1)
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        actualizarTabla()
        Dim tipo As Integer = 2
        If cmbProveedor.SelectedValue <> -1 Then
            Using nuevo_pedido = New form_buscar_medicamento(7, CInt(cmbProveedor.SelectedValue))
                If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                    LoadPage()
                    Me.dgvGrilla.Rows(dgvGrilla.RowCount - 2).Cells("cantidad").Selected = True
                    Me.dgvGrilla.BeginEdit(True)
                End If
            End Using
        Else
            mod_conf.mensajeError("Proveedor", "Error: Debe Seleccionar un proveedor de la lista.")
        End If

    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs)
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "registro"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
            Case "cantidad"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
        End Select
    End Sub

    Private Function obtenerMedicamento(ByVal codigo As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(50);"
        sSql += " SET @BUSCAR = '" + codigo + "';"

        sSql += " SELECT TOP 1 RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, MARCA, TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO, M.COMPOSICION,CB.CODIGO_BARRA "
        sSql += " FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE (RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) = RIGHT('000000' + @BUSCAR,6) OR CB.CODIGO_BARRA = @BUSCAR)"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub dgvGrilla_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs)
        If dgvGrilla.Rows.Count > 0 Then
            Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
                Case "registro"
                    If e.RowIndex >= 0 Then
                        If Not IsDBNull(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value) Then
                            Dim med = obtenerMedicamento(CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value)
                            If med.Rows.Count > 0 Then
                                Dim filAnt = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells(6), DataGridViewTextBoxCell).Value
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("REGISTRO"), DataGridViewTextBoxCell).Value = med.Rows(0)("ID_MEDICAMENTO")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("MEDICAMENTO"), DataGridViewTextBoxCell).Value = med.Rows(0)("MEDICAMENTO")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("COMPOSICION"), DataGridViewTextBoxCell).Value = med.Rows(0)("COMPOSICION")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("LABORATORIO"), DataGridViewTextBoxCell).Value = med.Rows(0)("LABORATORIO")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("forma_farmaceutica"), DataGridViewTextBoxCell).Value = med.Rows(0)("DESC_TIPO_MEDICAMENTO")
                                CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("CANTIDAD"), DataGridViewTextBoxCell).Value = 1


                                'CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("nro_fila"), DataGridViewTextBoxCell).Value = dgvGrilla.Rows.Count
                                actualizarTabla()
                                If filAnt = 999 Then
                                    nuevoItem("0", "", "", "", "", 0, 0, 0, 1, 1)
                                End If
                                Try
                                    LoadPage()
                                    Me.dgvGrilla.Rows(e.RowIndex).Cells("cantidad").Selected = True
                                    Me.dgvGrilla.BeginEdit(True)
                                Catch ex As Exception

                                End Try

                            Else
                                mod_conf.mensajeError("ERROR DE CODIGO", "El codigo ingresado no existe")
                                'dgvGrilla.Rows.Remove(Me.dgvGrilla.Rows(e.RowIndex))
                            End If
                        End If
                    End If
                Case "cantidad"
                    Try
                        If dgvGrilla.EndEdit Then
                            actualizarTabla()
                            LoadPage()
                        End If
                    Catch ex As Exception

                    End Try
            End Select
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()

        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, solicitud_compra)
    End Sub

    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        actualizarTabla()
        Dim tblItems As DataTable = dtSource
        For Each row As DataRow In tblItems.Rows
            If CStr(row("NRO_FILA")) = dgvGrilla.SelectedCells(6).Value Then
                row("ACCION") = 3
                Exit For
            End If
        Next
        dtSource.AcceptChanges()
        LoadPage()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        actualizarTabla()
        If validarPedido(txtNit, ErrorProvider1) And validarCombo() And validarLista() Then
            If CInt(lblIdSolicitud.Text) <> -1 Then
                modificarOrdenCompra()
            Else
                lblIdSolicitud.Text = insertarOrdenCompra()
            End If
            insertarInsumo(lblIdSolicitud.Text)
            Select Case mod_conf.mensajeConfirmacion("Ordenes de Compra", "Desea Imprimir la nota de Orden de Compra?")
                Case MsgBoxResult.Yes
                    Dim form = New visualizador_reporte(3, lblIdSolicitud.Text)
                    form.Show()
            End Select
            Me.Close()

            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, solicitud_compra)
        End If
    End Sub

    Private Function validarPedido(ByVal inputText As TextBox, ByVal errorProvider As ErrorProvider) As Boolean
        Dim valido As Boolean = True
        If Trim(inputText.Text).Length > 0 Then
            errorProvider.SetError(inputText, "")
        Else
            errorProvider.SetError(inputText, "Debe ingresar un valor.")
            valido = False
        End If
        Return valido
    End Function
    Private Function validarCombo()
        Dim valido As Boolean = True
        If Decimal.Parse(cmbProveedor.SelectedValue) > 0 Then
            ErrorProvider1.SetError(cmbProveedor, "")
        Else
            ErrorProvider1.SetError(cmbProveedor, "Debe ingresar un valor.")
            valido = False
        End If
        Return valido
    End Function
    Private Function validarLista()
        Dim valido As Boolean = True

        Dim tbl = dtSource.Select("NRO_FILA <> 999 AND ACCION <> 3")
        If CInt(tbl.Count) > 0 Then
            ErrorProvider1.SetError(dgvGrilla, "")
        Else
            ErrorProvider1.SetError(dgvGrilla, "Debe ingresar un valor.")
            mod_conf.mensajeError("Error Falta Medicamento", "No Agrego ningun Medicamento a la lista de pedido.")
            valido = False
        End If
        Return valido
    End Function
    Protected Function insertarOrdenCompra() As Integer
        Dim tblDatos = New datos()
        tblDatos.tabla = "ORDEN_COMPRA"
        tblDatos.modoID = "sql"
        tblDatos.campoID = "ID_ORDEN_COMPRA"
        tblDatos.agregarCampoValor("FECHA", CDate(txtFecha.Text))
        tblDatos.agregarCampoValor("OBSERVACION", txtDetalle.Text)
        tblDatos.agregarCampoValor("ID_PROVEEDOR", cmbProveedor.SelectedValue)
        tblDatos.agregarCampoValor("NRO_ORDEN", obtenerNroOrden())
        tblDatos.agregarCampoValor("ID_ESTADO", 1)
        tblDatos.insertar()
        Return tblDatos.valorID()
    End Function
    Protected Sub modificarOrdenCompra()
        Dim tblDatos = New datos()
        tblDatos.tabla = "ORDEN_COMPRA"
        tblDatos.modoID = "sql"
        tblDatos.campoID = "ID_ORDEN_COMPRA"
        tblDatos.valorID = lblIdSolicitud.Text
        tblDatos.agregarCampoValor("FECHA", CDate(txtFecha.Text))
        tblDatos.agregarCampoValor("OBSERVACION", txtDetalle.Text)
        tblDatos.agregarCampoValor("ID_PROVEEDOR", cmbProveedor.SelectedValue)
        tblDatos.modificar()
    End Sub

    Protected Sub insertarInsumo(ByVal idOrdenCompra As Integer)
        Dim tbl = New datos()
        For Each dr In dtSource.Select("ACCION <> 0 AND NRO_FILA <> 999", "ACCION DESC")
            If dr("NUEVO") Then
                If dr("ACCION") <> 3 Then
                    tbl.campoID = "ID_INSUMO"
                    tbl.tabla = "INSUMOS"
                    tbl.modoID = "sql"
                    tbl.agregarCampoValor("ID_MEDICAMENTO", dr("ID_MEDICAMENTO"))
                    tbl.agregarCampoValor("CANTIDAD_SOLICITADA", dr("CANTIDAD"))
                    tbl.agregarCampoValor("ID_ORDEN_COMPRA", idOrdenCompra)
                    tbl.insertar()
                End If
            Else
                Select Case dr("ACCION")
                    Case "2"
                        tbl.campoID = "ID_INSUMO"
                        tbl.tabla = "INSUMOS"
                        tbl.modoID = "sql"
                        tbl.agregarCampoValor("ID_MEDICAMENTO", dr("ID_MEDICAMENTO"))
                        tbl.agregarCampoValor("CANTIDAD_SOLICITADA", dr("CANTIDAD"))
                        tbl.agregarCampoValor("ID_ORDEN_COMPRA", idOrdenCompra)
                        tbl.modificar()
                    Case "3"
                        Dim sSql = "DELETE FROM INSUMOS WHERE ID_MEDICAMENTO = " + dr("ID_MEDICAMENTO") + " AND ID_ORDEN_COMPRA = " + idOrdenCompra
                        tbl.ejecutarSQL(sSql)
                End Select
            End If
            tbl.reset()
        Next
    End Sub
    Function obtenerNroOrden() As String
        Dim sSql = ""
        sSql = "SELECT  RIGHT('000000' + CAST(COUNT(*) + 1 AS NVARCHAR (6)),6) AS NRO_ORDEN FROM ORDEN_COMPRA"
        Return New datos().ejecutarConsulta(sSql).Rows(0)(0)
    End Function

    Private Sub dgvGrilla_CellClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "cantidad"
                If dgvGrilla.EndEdit Then
                    dgvGrilla.BeginEdit(True)
                End If
        End Select
    End Sub
End Class