﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Public Class capturarMedicamentoCompra
    Private idOrdenCompra As Integer
    Private idComprobante As Integer
    Public Shared idMedicamento As Integer
    Private idProveedor As Integer
    Private nroItem As Integer
    Private tipo As Integer
    Public Shared idInsumo As Integer
    ' Tipo 1 = gestion Facturas (Orden Compra)
    ' Tipo 2 = gestion Compra (Ingreso de Factura)
    Private InString As String
    Private PistolaEnviando As Boolean = False
    Private mod_conf = New modulo()
    Public Sub New(ByVal _idOrdenCompra As Integer, ByVal _idComprobante As Integer, ByVal _idMedicamento As Integer, ByVal _nroItem As Integer, ByVal _idProveedor As Integer, Optional _tipo As Integer = 1)
        InitializeComponent()
        idOrdenCompra = _idOrdenCompra
        idComprobante = _idComprobante
        idMedicamento = _idMedicamento
        idProveedor = _idProveedor
        nroItem = _nroItem
        iniciarCombo()
        cargarDatos(nroItem)
        tipo = _tipo
    End Sub

    Protected Sub iniciarCombo()
        iniciarComboFormaFarmaceutica()
        iniciarComboLaboratorio()
    End Sub

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_TIPO_MEDICAMENTO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM TIPO_MEDICAMENTO WHERE ACTIVO = 1 UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Forma Farmaceutica]' ORDER BY DESCRIPCION"

        cmbTipoMedicamento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_TIPO_MEDICAMENTO"
    End Sub
    Private Sub cargarDatos(ByVal nroItem As Integer)
        If idMedicamento <> -1 Then
            Dim sSql As String = ""
            sSql += " DECLARE @ORDEN_COMPRA AS INTEGER, @ID_COMPROBANTE AS INTEGER, @ID_MEDICAMENTO AS INTEGER;"
            sSql += " SET @ORDEN_COMPRA = " + idOrdenCompra.ToString + ";"
            sSql += " SET @ID_COMPROBANTE = " + idComprobante.ToString + ";"
            sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
            sSql += " SELECT ISNULL(CB.CODIGO_BARRA,'') AS CODIGO_BARRA, M.NOMBRE AS MEDICAMENTO, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, "
            sSql += " M.ID_TIPO_MEDICAMENTO, M.ID_LABORATORIO,MINIMO,ISNULL(MAXIMO,0) AS MAXIMO,"
            sSql += " FF.DESCRIPCION AS FORMA_FARMACEUTICA,L.DESCRIPCION AS LABORATORIO, M.MARCA, M.COMPOSICION, M.CONTROLADO, "
            sSql += " ISNULL(I.CANTIDAD_SOLICITADA,0)  AS CANTIDAD_SOLICITADA,ISNULL(CD.CONTROL_FECHA,1) AS CONTROL_FECHA,"
            sSql += " M.PRECIO_UNITARIO, ISNULL(M.PORC_UNITARIO,0) AS PORC_UNITARIO, ISNULL(M.PRECIO_PAQUETE,0) AS PRECIO_PAQUETE,"
            sSql += " ISNULL(M.PORC_PAQUETE,0) AS PORC_PAQUETE, ISNULL(M.CANTIDAD_PAQUETE,0) AS CANTIDAD_PAQUETE,"
            sSql += " ISNULL(CD.CANTIDAD,ISNULL(I.CANTIDAD_SOLICITADA,0)) AS CANTIDAD_RECEPCIONADA, ISNULL(CD.PRECIO_UNITARIO,0) AS PRECIO_COMPRA, ISNULL(CD.LOTE,'') AS LOTE, "
            sSql += " ISNULL(CONVERT(NVARCHAR(10),CD.FECHA_VENCIMIENTO,103),'')  AS F_VENCIMIENTO  FROM MEDICAMENTO M "
            sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO"
            sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
            sSql += " LEFT JOIN INSUMOS I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND I.ID_ORDEN_COMPRA = @ORDEN_COMPRA"
            sSql += " LEFT JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
            sSql += " LEFT JOIN COMPROBANTE_DETALLE CD ON CD.ID_INSUMO = I.ID_INSUMO AND CD.ID_COMPROBANTE = @ID_COMPROBANTE "
            sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO  "

            Dim tbl = New datos().ejecutarConsulta(sSql)
            txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
            cmbTipoMedicamento.SelectedValue = tbl.Rows(0)("ID_TIPO_MEDICAMENTO")
            txtPrecio.Text = tbl.Rows(0)("PRECIO_UNITARIO")
            cmbLaboratorio.SelectedValue = tbl.Rows(0)("ID_LABORATORIO")
            txtMarca.Text = tbl.Rows(0)("MARCA")
            txtComposicion.Text = tbl.Rows(0)("COMPOSICION")
            txtConcentracion.Text = tbl.Rows(0)("CONCENTRACION")
            chkControlado.Checked = tbl.Rows(0)("CONTROLADO")
            txtPrecioPaquete.Text = tbl.Rows(0)("PRECIO_PAQUETE")
            txtPorcPaquete.Text = tbl.Rows(0)("PORC_PAQUETE")
            txtPrecioPorcentaje.Text = tbl.Rows(0)("PORC_UNITARIO")
            txtCantidadPaquete.Text = tbl.Rows(0)("CANTIDAD_PAQUETE")
            txtCodigoBarra.Text = tbl.Rows(0)("CODIGO_BARRA")
            txtCantidadSolicitada.Text = tbl.Rows(0)("CANTIDAD_SOLICITADA")
            txtMinimo.Text = tbl.Rows(0)("MINIMO")
            txtMaximo.Text = tbl.Rows(0)("MAXIMO")
            If nroItem <> -1 Then
                Dim row As DataRow
                row = (From item In gestionFactura.dtSource Where item!NRO_ITEM = nroItem)(0)
                txtPrecioCompra.Text = CDec(row("PRECIO_UNITARIO")).ToString("##0.00")
                txtCantidad.Text = row("CANTIDAD")
                txtLote.Text = row("LOTE")
                chkControlFecha.Checked = row("CONTROL_FECHA")
                If chkControlFecha.Checked Then
                    txtFechaVencimiento.Text = row("F_VENCIMIENTO")
                    txtFechaVencimiento.Enabled = True
                End If
            End If
            cargarCodigoBarra()
        End If
    End Sub
    Private Sub insertarCodigoBarra()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " IF NOT EXISTS(SELECT *  FROM  CODIGO_BARRA  WHERE ID_MEDICAMENTO = @MEDICAMENTO AND CODIGO_BARRA = @COD_BARRA )  "
        sSql += " BEGIN "
        sSql += " INSERT CODIGO_BARRA VALUES (@COD_BARRA,@MEDICAMENTO)"
        sSql += " END"
        Dim tbl = New datos()
        tbl.ejecutarSQL(sSql)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If tipo = 1 Then
            Using nuevo_Laboratorio = New form_buscar_medicamento(8, idProveedor)
                If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
                    cargarDatos(-1)
                End If
            End Using
        Else
            Using nuevo_Laboratorio = New form_buscar_medicamento(9, idProveedor)
                If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
                    cargarDatos(-1)
                End If
            End Using
        End If
        
    End Sub

    Private Sub chkMedicamento_CheckedChanged(sender As Object, e As EventArgs) Handles chkMedicamento.CheckedChanged
        txtMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbTipoMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbLaboratorio.Enabled = CType(sender, CheckBox).Checked
        txtMarca.Enabled = CType(sender, CheckBox).Checked
        txtComposicion.Enabled = CType(sender, CheckBox).Checked
        txtConcentracion.Enabled = CType(sender, CheckBox).Checked
        chkControlado.Enabled = CType(sender, CheckBox).Checked
        txtMinimo.Enabled = CType(sender, CheckBox).Checked
        txtMaximo.Enabled = CType(sender, CheckBox).Checked
        txtCodigoBarra.Enabled = CType(sender, CheckBox).Checked
    End Sub
    Private Sub cargarCodigoBarra()
        Dim sSql As String = ""
        sSql += " SELECT CODIGO_BARRA FROM CODIGO_BARRA "
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento.ToString
        sSql += " ORDER BY ID_CODIGO_BARRA DESC"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            txtCodigoBarra.Text = tblConsulta.Rows(0)(0)
        End If
    End Sub
    'Private Sub txtCodigoBarra_TextChanged(sender As Object, e As EventArgs)
    '    idMedicamento = obtenerIdeMedicamento(sender.text)
    '    cargarDatos(-1)
    'End Sub
    Private Function obtenerIdeMedicamento(ByVal codBarra As String) As Integer
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(20), @ORDEN_COMPRA AS INTEGER;"
        sSql += " SET @BUSCAR = '" + codBarra + "';"
        If tipo = 1 Then
            sSql += " SET @ORDEN_COMPRA = " + idOrdenCompra.ToString + ";"
            sSql += " SELECT M.ID_MEDICAMENTO "
            sSql += " FROM MEDICAMENTO M"
            sSql += " INNER JOIN INSUMOS I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND I.ID_ORDEN_COMPRA = @ORDEN_COMPRA"
            sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
            sSql += " LEFT JOIN COMPROBANTE_DETALLE CD ON CD.ID_INSUMO = I.ID_INSUMO "
            sSql += " WHERE   I.CANTIDAD_SOLICITADA - ISNULL(CD.CANTIDAD,0)  > 0 AND CB.CODIGO_BARRA = @BUSCAR"
        Else
            sSql += " SELECT M.ID_MEDICAMENTO "
            sSql += " FROM MEDICAMENTO M"
            sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
            sSql += " WHERE CB.CODIGO_BARRA = @BUSCAR"
        End If
        
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            Return tbl.Rows(0)(0)
        Else
            Return -1
        End If
    End Function
    'Private Function validarformulario() As Boolean
    '    Dim mensaje As String
    '    Dim valido As Boolean = True
    '    If Trim(txtCantidad.Text).Length > 0 Then
    '        If CInt(txtCantidad.Text) > 0 Then
    '            ErrorProvider1.SetError(txtCantidad, "")
    '        Else
    '            mensaje = "La Cantidad no puede ser 0"
    '            ErrorProvider1.SetError(txtCantidad, mensaje)
    '            valido = False
    '        End If
    '    Else
    '        mensaje = "Complete el campo Cantidad."
    '        ErrorProvider1.SetError(txtCantidad, mensaje)
    '        valido = False
    '    End If


    '    If txtPrecioCompra.Text.Length > 0 Then
    '        If CDec(txtPrecioCompra.Text) > 0 Then
    '            ErrorProvider2.SetError(txtPrecioCompra, "")
    '        Else
    '            mensaje = "Debe ingresar un precio de compra valido"
    '            ErrorProvider2.SetError(txtPrecioCompra, "")
    '            valido = False
    '        End If
    '    Else
    '        mensaje = "Debe ingresar un precio de compra valido"
    '        ErrorProvider2.SetError(txtPrecioCompra, "")
    '        valido = False
    '    End If

    '    If txtLote.Text.Length > 0 Then
    '        ErrorProvider3.SetError(txtLote, "")
    '    Else
    '        mensaje = "Debe ingresar un lote valido"
    '        ErrorProvider3.SetError(txtLote, "")
    '        valido = False
    '    End If
    '    Return valido
    'End Function
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtCantidad.Text).Length > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "Complete el campo Cantidad."
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If

        If CInt(txtCantidad.Text) > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "La Cantidad no puede ser 0"
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If
        If txtPrecioCompra.Text.Length > 0 Then
            If CDec(txtPrecioCompra.Text) > 0 Then
                ErrorProvider1.SetError(txtPrecioCompra, "")
            Else
                mensaje = "Debe ingresar un precio de compra valido"
                ErrorProvider1.SetError(txtPrecioCompra, "")
                Return False
            End If
        Else
            mensaje = "Debe ingresar un precio de compra valido"
            ErrorProvider1.SetError(txtPrecioCompra, "")
            Return False
        End If
        If txtLote.Text.Length > 0 Then
            ErrorProvider1.SetError(txtLote, "")
        Else
            Select Case mod_conf.mensajeConfirmacion("Lote", "El medicamento no tiene un Lote Asignado, desea que el sistema agregue un Numero de Lote Automatico")
                Case MsgBoxResult.Yes
                    txtLote.Text = "L" + idMedicamento.ToString
                Case MsgBoxResult.No
                    mensaje = "Debe Ingresar un lote Valido"
                    ErrorProvider1.SetError(txtLote, mensaje)
                    Return False
            End Select

        End If
        Return valido
    End Function
    Private Function existeMedicamento(ByVal idMedicamento As Integer, ByVal lote As String)
        Dim foundRows() As DataRow
        Select Case tipo
            Case 1
                foundRows = gestionFactura.dtSource.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND LOTE = '" & lote & "' AND NRO_ITEM <> " & nroItem)
            Case 2
                foundRows = gestion_compra.dtSource.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND LOTE = '" & lote & "' AND NRO_ITEM <> " & nroItem)
            Case 3
                foundRows = gestionFactura.dtSource.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND LOTE = '" & lote & "' AND NRO_ITEM <> " & nroItem)
        End Select
        
        Return foundRows.Count > 0
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            If Not existeMedicamento(idMedicamento, txtLote.Text) Then
                Dim tblConsulta = New datos()
                tblConsulta.tabla = "MEDICAMENTO"
                tblConsulta.valorID = idMedicamento
                tblConsulta.campoID = "ID_MEDICAMENTO"
                If chkMedicamento.Checked Then
                    tblConsulta.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                    tblConsulta.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
                    tblConsulta.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                    tblConsulta.agregarCampoValor("MARCA", txtMarca.Text)
                    tblConsulta.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                    tblConsulta.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                    tblConsulta.agregarCampoValor("CONTROLADO", chkControlado.Checked)
                    tblConsulta.agregarCampoValor("MINIMO", txtMinimo.Text)
                    tblConsulta.agregarCampoValor("MAXIMO", txtMaximo.Text)
                End If

                tblConsulta.agregarCampoValor("PRECIO_UNITARIO", txtPrecio.Text)
                tblConsulta.agregarCampoValor("PRECIO_PAQUETE", txtPrecioPaquete.Text)
                tblConsulta.agregarCampoValor("PORC_UNITARIO", txtPrecioPorcentaje.Text)
                tblConsulta.agregarCampoValor("PORC_PAQUETE", txtPorcPaquete.Text)
                tblConsulta.agregarCampoValor("CANTIDAD_PAQUETE", txtCantidadPaquete.Text)
                tblConsulta.modificar()
                If nroItem <> -1 Then
                    modificar(nroItem)
                Else
                    guardar()
                End If
                If Trim(txtCodigoBarra.Text).Length > 0 Then
                    insertarCodigoBarra()
                End If
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                mod_conf.mensaje()
            End If
        End If
    End Sub
    Private Sub guardar()
        Dim foundRows() As DataRow
        Dim dr As DataRow
        Select Case tipo
            Case 1
                foundRows = gestionFactura.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")
                dr = gestionFactura.dtSource.NewRow
            Case 2
                foundRows = gestion_compra.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")
                dr = gestion_compra.dtSource.NewRow
            Case 3
                foundRows = gestionFactura.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")
                dr = gestionFactura.dtSource.NewRow
        End Select
        If foundRows.Length = 0 Then          
            dr("NRO_ITEM") = 1           
        Else
            Select Case tipo
                Case 1
                    dr("NRO_ITEM") = CInt(gestionFactura.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
                Case 2
                    dr("NRO_ITEM") = CInt(gestion_compra.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
                Case 3
                    dr("NRO_ITEM") = CInt(gestionFactura.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
            End Select
        End If
        dr("MEDICAMENTO") = txtMedicamento.Text
        dr("LABORATORIO") = cmbLaboratorio.Text
        dr("FORMA_FARMACEUTICA") = cmbTipoMedicamento.Text
        dr("CANTIDAD_SOLICITADA") = txtCantidadSolicitada.Text
        dr("CANTIDAD") = txtCantidad.Text
        dr("PRECIO_UNITARIO") = txtPrecioCompra.Text
        dr("TOTAL") = CDec(txtPrecioCompra.Text) * CDec(txtCantidad.Text) - CDec(txtDescuento.Text)
        dr("NUEVO") = 1
        dr("ACCION") = 1
        dr("LOTE") = txtLote.Text
        dr("CONTROL_FECHA") = chkControlFecha.Checked
        If chkControlFecha.Checked Then
            dr("FECHA_VENCIMIENTO") = txtFechaVencimiento.Text
        Else
            dr("FECHA_VENCIMIENTO") = DBNull.Value
        End If
        dr("ID_MEDICAMENTO") = idMedicamento
        dr("DESCUENTO") = txtDescuento.Text
        Select Case tipo
            Case 1
                dr("ID_INSUMO") = idInsumo
                gestionFactura.dtSource.Rows.Add(dr)
                gestionFactura.dtSource.AcceptChanges()
            Case 2
                dr("ID_COMPROBANTE_DETALLE") = -1
                gestion_compra.dtSource.Rows.Add(dr)
                gestion_compra.dtSource.AcceptChanges()
            Case 3
                dr("ID_INSUMO") = -1
                gestionFactura.dtSource.Rows.Add(dr)
                gestionFactura.dtSource.AcceptChanges()
        End Select
    End Sub
    Private Sub modificar(ByVal nro_item As Integer)
        Dim row As DataRow
        If tipo = 1 Then
            row = (From tbl In gestionFactura.dtSource Where tbl!NRO_ITEM = nro_item)(0)
        Else
            row = (From tbl In gestion_compra.dtSource Where tbl!NRO_ITEM = nro_item)(0)
        End If

        row("MEDICAMENTO") = txtMedicamento.Text
        row("LABORATORIO") = cmbLaboratorio.Text
        row("FORMA_FARMACEUTICA") = cmbTipoMedicamento.Text
        row("CANTIDAD_SOLICITADA") = txtCantidadSolicitada.Text
        row("CANTIDAD") = txtCantidad.Text
        row("PRECIO_UNITARIO") = txtPrecioCompra.Text
        row("TOTAL") = CDec(txtPrecioCompra.Text) * CDec(txtCantidad.Text)
        row("NUEVO") = 1
        row("ACCION") = 1
        row("LOTE") = txtLote.Text
        row("CONTROL_FECHA") = chkControlFecha.Checked
        If chkControlFecha.Checked Then
            row("F_VENCIMIENTO") = txtFechaVencimiento.Text
        End If
        row("DESCUENTO") = txtDescuento.Text
        row("ID_MEDICAMENTO") = idMedicamento
        If tipo = 1 Then
            row("ID_INSUMO") = idInsumo
            gestionFactura.dtSource.AcceptChanges()
        Else
            gestion_compra.dtSource.AcceptChanges()
        End If

    End Sub

    Private Sub chkPrecioUnidad_Click(sender As Object, e As EventArgs) Handles chkPrecioUnidad.Click
        txtPrecio.Enabled = chkPrecioUnidad.Checked
        If chkPorcUnidad.Checked And chkPrecioUnidad.Checked Then
            chkPorcUnidad.Checked = Not chkPrecioUnidad.Checked
            txtPrecioPorcentaje.Enabled = Not chkPrecioUnidad.Checked
        End If

    End Sub

    Private Sub chkPorcUnidad_Click(sender As Object, e As EventArgs) Handles chkPorcUnidad.Click
        txtPrecioPorcentaje.Enabled = chkPorcUnidad.Checked
        If chkPrecioUnidad.Checked And chkPorcUnidad.Checked Then
            chkPrecioUnidad.Checked = Not chkPorcUnidad.Checked
            txtPrecio.Enabled = Not chkPorcUnidad.Checked
        End If
    End Sub

    Private Sub chkPrecioPaquete_Click(sender As Object, e As EventArgs) Handles chkPrecioPaquete.Click
        txtPrecioPaquete.Enabled = chkPrecioPaquete.Checked
        If chkPorcPaquete.Checked And chkPrecioPaquete.Checked Then
            chkPorcPaquete.Checked = Not chkPrecioPaquete.Checked
            txtPorcPaquete.Enabled = Not chkPrecioPaquete.Checked
        End If
    End Sub

    Private Sub chkPorcPaquete_Click(sender As Object, e As EventArgs) Handles chkPorcPaquete.Click
        txtPorcPaquete.Enabled = chkPorcPaquete.Checked
        If chkPrecioPaquete.Checked And chkPorcPaquete.Checked Then
            chkPrecioPaquete.Checked = Not chkPorcPaquete.Checked
            txtPrecioPaquete.Enabled = Not chkPorcPaquete.Checked
        End If
    End Sub

    Private Sub txtPrecioCompra_TextChanged(sender As Object, e As EventArgs) Handles txtPrecioCompra.TextChanged
        Dim precioVenta As Decimal = 0
        If txtPrecio.Text.Length > 0 Then
            precioVenta = CDec(txtPrecio.Text)
        End If
        Dim precioCompra As Decimal = 0
        If txtPrecioCompra.Text.Length > 0 Then
            precioCompra = CDec(txtPrecioCompra.Text)
        End If
        If precioCompra > 0 Then
            txtPrecioPorcentaje.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:n}", ((precioVenta * 100 / precioCompra) - 100))
            txtPrecioPaquete.Text = txtPrecio.Text
            txtPorcPaquete.Text = txtPrecioPorcentaje.Text
        Else
            txtPrecioPorcentaje.Text = "0"
            txtPrecioPaquete.Text = "0"
            txtPorcPaquete.Text = "0"
        End If

    End Sub

    Private Sub txtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged
        If chkPrecioUnidad.Checked Then
            Dim precioVenta As Decimal = 0
            If txtPrecio.Text.Length > 0 Then
                precioVenta = CDec(txtPrecio.Text)
            End If
            Dim precioCompra As Decimal = 0
            If txtPrecioCompra.Text.Length > 0 Then
                precioCompra = CDec(txtPrecioCompra.Text)
            End If
            If precioCompra > 0 Then
                txtPrecioPorcentaje.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:n}", ((precioVenta * 100 / precioCompra) - 100))
            Else
                txtPrecioPorcentaje.Text = "0"
            End If
        End If
    End Sub

    Private Sub txtPrecioPorcentaje_TextChanged(sender As Object, e As EventArgs) Handles txtPrecioPorcentaje.TextChanged
        If chkPorcUnidad.Checked Then
            Dim precioCompra As Decimal = CDec(txtPrecioCompra.Text)
            Dim porcentaje As Decimal = CDec(txtPrecioPorcentaje.Text)
            txtPrecio.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:n}", (precioCompra * porcentaje / 100 + precioCompra))
        End If
    End Sub

    Private Sub txtPrecioPaquete_TextChanged(sender As Object, e As EventArgs) Handles txtPrecioPaquete.TextChanged
        If chkPrecioPaquete.Checked Then
            Dim precioVenta As Decimal = 0
            If txtPrecioPaquete.Text.Length > 0 Then
                precioVenta = CDec(txtPrecioPaquete.Text)
            End If
            Dim precioCompra As Decimal = 0
            If txtPrecioCompra.Text.Length > 0 Then
                precioCompra = CDec(txtPrecioCompra.Text)
            End If
            Dim cantidad As Decimal = 1
            If IsNumeric(txtCantidadPaquete.Text) Then
                If CDec(txtCantidadPaquete.Text) > 0 Then
                    cantidad = CDec(txtCantidadPaquete.Text)
                Else
                    cantidad = 1
                End If
            End If
            If precioCompra > 0 Then
                txtPorcPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:n}", (((precioVenta / cantidad) * 100 / precioCompra) - 100))
            Else
                txtPrecioPorcentaje.Text = "0"
            End If
        End If
    End Sub

    Private Sub txtPorcPaquete_TextChanged(sender As Object, e As EventArgs) Handles txtPorcPaquete.TextChanged
        If chkPorcPaquete.Checked Then
            Dim precioCompra As Decimal = CDec(txtPrecioCompra.Text)
            Dim porcentaje As Decimal = CDec(txtPorcPaquete.Text)
            txtPrecioPaquete.Text = String.Format(New Globalization.CultureInfo("en-US"), "{0:n}", (precioCompra * porcentaje / 100 + precioCompra))
        End If
    End Sub

    Private Sub chkControlFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlFecha.CheckedChanged
        txtFechaVencimiento.Enabled = sender.checked
    End Sub
    Private Function existeCodigoBarraBD(ByVal codBarra As String) As Integer
        Dim sSql As String = ""
        sSql += "SELECT DISTINCT M.ID_MEDICAMENTO FROM MEDICAMENTO M "
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE CB.CODIGO_BARRA = '" + codBarra + "'"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count = 0 Then
            Return -1
        Else
            Return tblConsulta.Rows(0)(0)
        End If
    End Function

    Private Sub capturarMedicamentoCompra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                idMedicamento = existeCodigoBarraBD(InString)

                If idMedicamento <> -1 Then

                    cargarDatos(-1)
                Else
                    Dim mens = New modulo()
                    mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya existe en la lista de codigos del Medicamento o esta asignado a otro medicamento.")
                End If

            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidadPaquete_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadPaquete.TextChanged
        If IsNumeric(txtCantidadPaquete.Text) Then
            If CDec(txtCantidadPaquete.Text) > 0 Then
                chkPorcPaquete.Enabled = True
                chkPrecioPaquete.Enabled = True
            Else
                chkPorcPaquete.Enabled = False
                chkPrecioPaquete.Enabled = False
            End If
        Else
            chkPorcPaquete.Enabled = False
            chkPrecioPaquete.Enabled = False
        End If
    End Sub
End Class