﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Public Class form_add_med_solicitud
    Private _medicamento As eMedicamento
    Private _nroItem As Integer
    Private _tipo As Integer
    Private compra As Boolean
    Private idCompra As Integer
    Private idInventario As Integer

    Public Shared idMedicamento As Integer
    Private idOrdenCompra As Integer

    Private InString As String
    Private PistolaEnviando As Boolean = False


    Public Sub New(ByVal _idMedicamento As Integer, ByVal _idOrdenCompra As Integer, ByVal _rowItem As Integer)
        InitializeComponent()
        iniciarCombo()
        idOrdenCompra = _idOrdenCompra
        _nroItem = _rowItem
        _medicamento = New eMedicamento(_idMedicamento)
    End Sub

    Protected Sub iniciarCombo()
        iniciarComboFormaFarmaceutica()
        iniciarComboLaboratorio()
    End Sub

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_TIPO_MEDICAMENTO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM TIPO_MEDICAMENTO WHERE ACTIVO = 1 UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Forma Farmaceutica]' ORDER BY DESCRIPCION"

        cmbTipoMedicamento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_TIPO_MEDICAMENTO"
    End Sub


    Private Sub form_compra_inventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCantidad.Focus()
    End Sub
    Private Sub cargarDatos(ByVal nroItem As Integer)
        txtMedicamento.Text = _medicamento.NOMBRE
        cmbTipoMedicamento.SelectedValue = _medicamento.ID_TIPO_MEDICAMENTO
        cmbLaboratorio.SelectedValue = _medicamento.ID_LABORATORIO
        txtMarca.Text = _medicamento.MARCA
        txtComposicion.Text = _medicamento.COMPOSICION
        txtConcentracion.Text = _medicamento.CONCENTRACION
        chkControlado.Checked = _medicamento.CONTROLADO
        If nroItem <> -1 Then
            Dim row As DataRow
            row = (From tbl In form_gestion_pedido.dtSourceCompra Where tbl!NRO_ITEM = nroItem)(0)
            txtCantidad.Text = row("CANTIDAD")
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") = -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidad_Click(sender As Object, e As EventArgs) Handles txtCantidad.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            If _tipo <> 3 Then
                If Not existeMedicamento(_medicamento.ID_MEDICAMENTO) Then
                    Dim tblConsulta = New datos()
                    If chkMedicamento.Checked Then
                        tblConsulta.tabla = "MEDICAMENTO"
                        tblConsulta.valorID = _medicamento.ID_MEDICAMENTO
                        tblConsulta.campoID = "ID_MEDICAMENTO"
                        If chkMedicamento.Checked Then
                            tblConsulta.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                            tblConsulta.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
                            tblConsulta.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                            tblConsulta.agregarCampoValor("MARCA", txtMarca.Text)
                            tblConsulta.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                            tblConsulta.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                            tblConsulta.agregarCampoValor("CONTROLADO", chkControlado.Checked)

                        End If
                        tblConsulta.modificar()
                    End If
                    If _nroItem <> -1 Then
                        modificar(_nroItem)
                    Else
                        guardar()
                    End If
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                Else
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error"
                    Dim msg As String
                    If chkControlFecha.Checked Then
                        msg = "La fecha de vencimiento del medicamento seleccionado ya existe en la compra."
                    Else
                        msg = "El medicamento seleccionado ya existe en la compra."
                    End If

                    response = MsgBox(msg, style, title)
                End If
            Else
                Dim tblConsulta = New datos()
                If chkMedicamento.Checked Or chkPrecioUnidad.Checked Or chkPrecioUnidad.Checked Then
                    tblConsulta.tabla = "MEDICAMENTO"
                    tblConsulta.valorID = idMedicamento
                    tblConsulta.campoID = "ID_MEDICAMENTO"
                    If chkMedicamento.Checked Then
                        tblConsulta.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                        tblConsulta.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
                        tblConsulta.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                        tblConsulta.agregarCampoValor("MARCA", txtMarca.Text)
                        tblConsulta.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                    End If

                    tblConsulta.agregarCampoValor("PRECIO_UNITARIO", txtPrecio.Text)
                    tblConsulta.agregarCampoValor("PRECIO_PAQUETE", txtPrecioPaquete.Text)
                    tblConsulta.agregarCampoValor("PORC_UNITARIO", txtPrecioPorcentaje.Text)
                    tblConsulta.agregarCampoValor("PORC_PAQUETE", txtPorcPaquete.Text)
                    tblConsulta.agregarCampoValor("CANTIDAD_PAQUETE", txtCantidadPaquete.Text)

                    tblConsulta.modificar()
                    tblConsulta.modificar()
                End If
                Dim sSql As String = ""
                sSql += " DECLARE @INVENTARIO AS INTEGER, @COMPRA AS INTEGER,@CANTIDAD AS NUMERIC(18,2),@PRECIO AS NUMERIC(18,2),@VENCIMIENTO AS DATE;"
                sSql += " SET @INVENTARIO = " & idInventario & ";"
                sSql += " SET @COMPRA = " & idCompra & ";"
                sSql += " SET @CANTIDAD = " & txtCantidad.Text & ";"
                sSql += " SET @PRECIO = " & txtPrecioCompra.Text & ";"
                sSql += " SET @VENCIMIENTO = CONVERT(DATE,'" & txtFechaVencimiento.Text & "',103);"
                sSql += " UPDATE INVENTARIO  SET CANTIDAD = @CANTIDAD, PRECIO_UNITARIO = @PRECIO, F_VENCIMIENTO = @VENCIMIENTO "
                sSql += " WHERE ID_INVENTARIO = @INVENTARIO AND ID_COMPRA = @COMPRA"
                tblConsulta.ejecutarSQL(sSql)
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        End If
    End Sub

    Private Sub guardar()
        Dim foundRows() As DataRow
        If _tipo = 1 Then
            foundRows = form_gestionCompra.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")
        Else
            foundRows = form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")
        End If

        Dim dr As DataRow
        If foundRows.Length = 0 Then
            If _tipo = 1 Then
                dr = form_gestionCompra.dtSourceCompra.NewRow
            Else
                dr = form_inventario_inicial.dtSourceCompra.NewRow
            End If

            dr("NRO_ITEM") = 1
            dr("NOMBRE") = txtMedicamento.Text + "(" + cmbTipoMedicamento.Text + ")"
            If chkControlFecha.Checked Then
                dr("F_VENCIMIENTO") = txtFechaVencimiento.Text
            End If
            dr("LABORATORIO") = cmbLaboratorio.Text
        Else
            If _tipo = 1 Then
                dr = form_gestionCompra.dtSourceCompra.NewRow
                dr("NRO_ITEM") = CInt(form_gestionCompra.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
            Else
                dr = form_inventario_inicial.dtSourceCompra.NewRow
                dr("NRO_ITEM") = CInt(form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
                dr("PRECIO") = _medicamento.PRECIO_UNITARIO
            End If

            dr("LABORATORIO") = cmbLaboratorio.Text
            dr("NOMBRE") = txtMedicamento.Text + "(" + cmbTipoMedicamento.Text + ")"
            If chkControlFecha.Checked Then
                dr("F_VENCIMIENTO") = txtFechaVencimiento.Text
            End If

        End If
        dr("CANTIDAD") = txtCantidad.Text
        'If _tipo = 1 Then
        dr("PRECIO_UNITARIO") = txtPrecioCompra.Text
        'dr("TOTAL") = Decimal.Parse(txtPrecioCompra.Text) * Decimal.Parse(txtCantidad.Text)
        'Else
        dr("PRECIO") = txtPrecio.Text
        ' End If
        dr("ID_MEDICAMENTO") = _medicamento.ID_MEDICAMENTO
        dr("CONTROL_FECHA") = chkControlFecha.Checked
        dr("LOTE") = txtLote.Text

        If _tipo = 1 Then
            form_gestionCompra.dtSourceCompra.Rows.Add(dr)
            form_gestionCompra.dtSourceCompra.AcceptChanges()
        Else
            form_inventario_inicial.dtSourceCompra.Rows.Add(dr)
            form_inventario_inicial.dtSourceCompra.AcceptChanges()
        End If

    End Sub
    Private Function existeMedicamento(ByVal idMedicamento As Integer)
        Dim foundRows() As DataRow        
        foundRows = form_gestion_pedido.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & "AND NRO_ITEM <> " & _nroItem)         
        Return foundRows.Count > 0
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub modificar(ByVal nro_item As Integer)
        Dim row As DataRow
        If _tipo = 1 Then
            row = (From tbl In form_gestionCompra.dtSourceCompra Where tbl!NRO_ITEM = nro_item)(0)
        Else
            row = (From tbl In form_inventario_inicial.dtSourceCompra Where tbl!NRO_ITEM = nro_item)(0)
        End If

        If chkControlFecha.Checked Then
            row("F_VENCIMIENTO") = txtFechaVencimiento.Text
        End If
        row("NOMBRE") = txtMedicamento.Text + "(" + cmbTipoMedicamento.Text + ")"
        row("LABORATORIO") = cmbLaboratorio.SelectedText
        row("CANTIDAD") = txtCantidad.Text
        row("CONTROL_FECHA") = chkControlFecha.Checked
        row("PRECIO_UNITARIO") = txtPrecioCompra.Text
        If _tipo = 1 Then
            form_gestionCompra.dtSourceCompra.AcceptChanges()
        Else
            form_inventario_inicial.dtSourceCompra.AcceptChanges()
        End If

    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtCantidad.Text).Length > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "Complete el campo Cantidad."
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If

        If CInt(txtCantidad.Text) > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "La Cantidad no puede ser 0"
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If
        Return valido
    End Function


    Private Sub chkMedicamento_CheckedChanged(sender As Object, e As EventArgs) Handles chkMedicamento.CheckedChanged
        txtMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbTipoMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbLaboratorio.Enabled = CType(sender, CheckBox).Checked
        txtMarca.Enabled = CType(sender, CheckBox).Checked
        txtComposicion.Enabled = CType(sender, CheckBox).Checked
        txtConcentracion.Enabled = CType(sender, CheckBox).Checked
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Using nuevo_Laboratorio = New form_buscar_medicamento(6, idProveedor)
            If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
                _medicamento = New eMedicamento(idMedicamento)
                cargarDatos(-1)
            End If
        End Using
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using nuevo_Laboratorio = New form_gestionMedicamento(True)
            If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
            End If
        End Using
    End Sub

    Private Sub form_compra_inventario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                idMedicamento = existeCodigoBarraBD(InString)

                If idMedicamento <> -1 Then
                    _medicamento = New eMedicamento(idMedicamento)
                    cargarDatos(-1)
                Else
                    Dim mens = New modulo()
                    mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya existe en la lista de codigos del Medicamento o esta asignado a otro medicamento.")
                End If

            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub
    Private Function existeCodigoBarraBD(ByVal codBarra As String) As Integer
        Dim sSql As String = ""
        sSql += "SELECT DISTINCT M.ID_MEDICAMENTO FROM MEDICAMENTO M "
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE CB.CODIGO_BARRA = '" + codBarra + "'"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count = 0 Then
            Return -1
        Else
            Return tblConsulta.Rows(0)(0)
        End If
    End Function
End Class