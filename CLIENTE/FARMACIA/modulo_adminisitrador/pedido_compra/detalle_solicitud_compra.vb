﻿Imports CAPA_DATOS
Public Class detalle_solicitud_compra
    Private idOrdenCompra As Integer
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private mod_conf As New modulo()
    Public Sub New(ByVal _idOrdenCompra As Integer)
        InitializeComponent()
        idOrdenCompra = _idOrdenCompra
        cargarDatosCabecera(_idOrdenCompra)
        cargarDatosMovimientos(_idOrdenCompra)
    End Sub
    Protected Sub cargarDatosCabecera(ByVal idSolicitud As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_COMPRA AS INTEGER;"
        sSql += " SET @ID_COMPRA = " + idSolicitud.ToString + ";"
        sSql += " SELECT RIGHT('000000' + CAST(NRO_ORDEN AS NVARCHAR(6)),6) AS NRO_SOLICITUD,P.NIT, P.NOMBRE AS PROVEEDOR,"
        sSql += " OC.OBSERVACION FROM ORDEN_COMPRA OC"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = OC.ID_PROVEEDOR "
        sSql += " WHERE OC.ID_ORDEN_COMPRA = @ID_COMPRA "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtNroSolicitud.Text = tbl.Rows(0)("NRO_SOLICITUD")
            txtNit.Text = tbl.Rows(0)("NIT")
            txtProveedor.Text = tbl.Rows(0)("PROVEEDOR")
            txtObservacion.Text = tbl.Rows(0)("OBSERVACION")
        End If
    End Sub
    Protected Sub cargarDatosMovimientos(ByVal idSolicitud As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_COMPRA AS INTEGER;"
        sSql += " SET @ID_COMPRA = " + idSolicitud.ToString + ";"
        sSql += " SELECT C.FECHA, C.NRO_COMPROBANTE,TC.DETALLE AS TIPO_COMPROBANTE,"
        sSql += " C.NRO_AUTORIZACION, C.CODIGO_CONTROL, C.TOTAL, C.DESCUENTO, EC.DESCRIPCION AS ESTADO,C.ID_COMPROBANTE  FROM COMPROBANTE C "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE"
        sSql += " INNER JOIN ESTADO_COMPRA EC ON EC.ID_ESTADO = C.ID_ESTADO "
        sSql += " INNER JOIN ORDEN_COMPRA OC ON OC.ID_ORDEN_COMPRA = C.ID_ORDEN_COMPRA"
        sSql += " WHERE OC.ID_ORDEN_COMPRA = @ID_COMPRA"
        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtsource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1130
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 9
            .Columns(0).Name = "Fecha"
            .Columns(0).HeaderText = "Fecha"
            .Columns(0).DataPropertyName = "FECHA"
            .Columns(0).Width = 100

            .Columns(1).Name = "nro_comprobante"
            .Columns(1).HeaderText = "Nro. Comprobante"
            .Columns(1).DataPropertyName = "NRO_COMPROBANTE"
            .Columns(1).Width = 150
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "tipo"
            .Columns(2).HeaderText = "Tipo Comprobante"
            .Columns(2).DataPropertyName = "TIPO_COMPROBANTE"
            .Columns(2).Width = 140

            .Columns(3).Name = "autorizacion"
            .Columns(3).HeaderText = "Nro. Autorizacion"
            .Columns(3).DataPropertyName = "NRO_AUTORIZACION"
            .Columns(3).Width = 140

            .Columns(4).Name = "codigo_control"
            .Columns(4).HeaderText = "Codigo Control"
            .Columns(4).DataPropertyName = "CODIGO_CONTROL"
            .Columns(4).Width = 150

            .Columns(5).Name = "total"
            .Columns(5).HeaderText = "Total"
            .Columns(5).DataPropertyName = "TOTAL"
            .Columns(5).Width = 150

            .Columns(6).Name = "descuento"
            .Columns(6).HeaderText = "Descuento"
            .Columns(6).DataPropertyName = "DESCUENTO"
            .Columns(6).Width = 150

            .Columns(7).Name = "estado"
            .Columns(7).HeaderText = "Estado"
            .Columns(7).DataPropertyName = "ESTADO"
            .Columns(7).Width = 150

            .Columns(8).Name = "idComprobante"

            .Columns(8).DataPropertyName = "ID_COMPROBANTE"
            .Columns(8).Visible = False

        End With
        LoadPage()
    End Sub


    Private Sub detalle_solicitud_compra_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, solicitud_compra)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim form As verDetalleSolicitud = New verDetalleSolicitud(idOrdenCompra)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                
            End If
        End Using
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim form = New gestionFactura(idOrdenCompra, -1)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub btnConcluir_Click(sender As Object, e As EventArgs) Handles btnConcluir.Click
        Dim tbl = New datos()
        tbl.tabla = "ORDEN_COMPRA"
        tbl.campoID = "ID_ORDEN_COMPRA"
        tbl.valorID = idOrdenCompra
        tbl.agregarCampoValor("OBSERVACION", txtObservacion.Text)
        tbl.agregarCampoValor("ID_ESTADO", 4)
        tbl.modificar()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim tbl = New datos()
        tbl.tabla = "ORDEN_COMPRA"
        tbl.campoID = "ID_ORDEN_COMPRA"
        tbl.valorID = idOrdenCompra
        tbl.agregarCampoValor("ID_ESTADO", 3)
        tbl.agregarCampoValor("OBSERVACION", txtObservacion.Text)
        tbl.modificar()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        'Dim idComprobante As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(8), DataGridViewTextBoxCell).Value
        'Dim form = New gestionFactura(idOrdenCompra, idComprobante)
        'mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub
End Class