﻿Imports CAPA_DATOS
Imports System.Transactions
Public Class gestionFactura
    Public Shared idOrdenCompra As Integer
    Private idComprobante As Integer
    Public Shared dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private mod_conf As New modulo()
    Public Sub New(ByVal _idOrdenCompra As Integer, ByVal _idComprobante As Integer)
        InitializeComponent()
        idOrdenCompra = _idOrdenCompra
        idComprobante = _idComprobante
        iniciarComboTiposComprobantes()
        iniciarComboMoneda()
        cargarDatosCabecera(_idOrdenCompra, idComprobante)
        cargarDatosMovimientos(_idOrdenCompra, idComprobante)
        calcularTotal()
        If idComprobante = -1 Then
            btnAnular.Visible = False
        End If
    End Sub
    Protected Sub iniciarComboTiposComprobantes()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_TIPO_COMPROBANTE, DETALLE FROM TIPO_COMPROBANTE WHERE ID_TIPO_COMPROBANTE IN (1,2) UNION ALL "
        sSql += " SELECT -1 , '[Seleccione un Tipo de Comprobante]' ORDER BY DETALLE"

        cmbTipoComprobante.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoComprobante.DisplayMember = "DETALLE"
        cmbTipoComprobante.ValueMember = "ID_TIPO_COMPROBANTE"
    End Sub
    Protected Sub iniciarComboMoneda()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_TIPO_MONEDA, MONEDA FROM TIPO_MONEDA UNION ALL "
        sSql += " SELECT -1 , '[Seleccione una moneda]' ORDER BY ID_TIPO_MONEDA"

        cmbTipoMoneda.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMoneda.DisplayMember = "MONEDA"
        cmbTipoMoneda.ValueMember = "ID_TIPO_MONEDA"
    End Sub
    Protected Sub cargarDatosCabecera(ByVal idSolicitud As Integer, ByVal idComprobante As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_ORDEN AS INTEGER, @ID_COMPROBANTE AS INTEGER;"
        sSql += " SET @ID_ORDEN = " + idSolicitud.ToString + ";"
        sSql += " SET @ID_COMPROBANTE = " + idComprobante.ToString + ";"
        sSql += " SELECT P.NIT,P.NOMBRE AS PROVEEDOR, P.ID_PROVEEDOR, ISNULL(C.ID_TIPO_COMPROBANTE,-1) AS TIPO_COMPROBANTE,"
        sSql += " RIGHT('000000'+ CAST(OC.NRO_ORDEN AS NVARCHAR(10)),6) AS NRO_ORDEN,ISNULL(EC.DESCRIPCION,'NUEVO COMP.') AS ESTADO,"
        sSql += " ISNULL(C.NRO_COMPROBANTE,'') AS NRO_COMPROBANTE,ISNULL(C.NRO_AUTORIZACION,'') AS NRO_AUTORIZACION,ISNULL(C.DESCUENTO,0) AS DESCUENTO,"
        sSql += " ISNULL(C.CODIGO_CONTROL,'') AS CODIGO_CONTROL, ISNULL(C.ID_MONEDA,-1) AS MONEDA, OC.OBSERVACION,ISNULL(C.ID_ESTADO,-1) AS ID_ESTADO "
        sSql += " FROM ORDEN_COMPRA OC"
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = OC.ID_PROVEEDOR "
        sSql += " LEFT JOIN COMPROBANTE C ON C.ID_ORDEN_COMPRA = OC.ID_ORDEN_COMPRA AND C.ID_COMPROBANTE = @ID_COMPROBANTE "
        sSql += " LEFT JOIN ESTADO_COMPRA EC ON EC.ID_ESTADO = C.ID_ESTADO "
        sSql += " WHERE OC.ID_ORDEN_COMPRA = @ID_ORDEN; "
        sSql += " SELECT TC_ME FROM CONFIGURACION "
        Dim ds = New datos().obtenerDataSet(sSql)
        Dim tbl = ds.Tables(0)
        txtFecha.Text = Now().ToString("dd/MM/yyyy")
        txtNit.Text = tbl.Rows(0)("NIT")
        txtNroOrden.Text = tbl.Rows(0)("NRO_ORDEN")
        txtProveedor.Text = tbl.Rows(0)("PROVEEDOR")
        lblIdProveedor.Text = tbl.Rows(0)("ID_PROVEEDOR")
        cmbTipoComprobante.SelectedValue = tbl.Rows(0)("TIPO_COMPROBANTE")
        txtDetalle.Text = tbl.Rows(0)("OBSERVACION")
        txtNroComprobante.Text = tbl.Rows(0)("NRO_COMPROBANTE")
        txtNroAutorizacion.Text = tbl.Rows(0)("NRO_AUTORIZACION")
        txtCodigoControl.Text = tbl.Rows(0)("CODIGO_CONTROL")
        cmbTipoMoneda.SelectedValue = tbl.Rows(0)("MONEDA")
        lblEstado.Text = tbl.Rows(0)("ESTADO")
        lblIdEstado.Text = tbl.Rows(0)("ID_ESTADO")
        txtTC.Text = ds.Tables(1).Rows(0)(0)
        txtDescuentoComprobante.Text = tbl.Rows(0)("DESCUENTO")
    End Sub
    Protected Sub cargarDatosMovimientos(ByVal idSolicitud As Integer, ByVal idComprobante As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @ID_ORDEN AS INTEGER, @ID_COMPROBANTE AS INTEGER;"
        sSql += " SET @ID_ORDEN = " + idSolicitud.ToString + ";"
        sSql += " SET @ID_COMPROBANTE = " + idComprobante.ToString + ";"
        sSql += " SELECT ROW_NUMBER() OVER (ORDER BY I.ID_MEDICAMENTO) AS NRO_ITEM,  M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO,TM.DESCRIPCION AS FORMA_FARMACEUTICA, I.CANTIDAD_SOLICITADA,CD.CANTIDAD,CD.PRECIO_UNITARIO,CD.CANTIDAD * CD.PRECIO_UNITARIO AS TOTAL,CD.DESCUENTO, 0  AS NUEVO, 0 AS ACCION, "
        sSql += " CD.LOTE,CD.CONTROL_FECHA,CD.FECHA_VENCIMIENTO,M.ID_MEDICAMENTO, I.ID_INSUMO "
        sSql += " FROM COMPROBANTE_DETALLE CD "
        sSql += " LEFT JOIN INSUMOS I ON I.ID_INSUMO = CD.ID_INSUMO  "
        sSql += " INNER JOIN MEDICAMENTO M ON CD.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE CD.ID_COMPROBANTE = @ID_COMPROBANTE "
        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1130
            .Height = 280
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 10
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 310

            .Columns(1).Name = "laboratorio"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 150
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "forma_farmaceutica"
            .Columns(2).HeaderText = "Forma Farmaceutica"
            .Columns(2).DataPropertyName = "FORMA_FARMACEUTICA"
            .Columns(2).Width = 130

            .Columns(3).Name = "cantidad_solicitada"
            .Columns(3).HeaderText = "Cant. O/C"
            .Columns(3).DataPropertyName = "CANTIDAD_SOLICITADA"
            .Columns(3).Width = 90

            .Columns(4).Name = "lote"
            .Columns(4).HeaderText = "Lote"
            .Columns(4).DataPropertyName = "LOTE"
            .Columns(4).Width = 90

            .Columns(5).Name = "cantidad_solicitada"
            .Columns(5).HeaderText = "Cantidad"
            .Columns(5).DataPropertyName = "CANTIDAD"
            .Columns(5).Width = 90

            .Columns(6).Name = "precio_unitario"
            .Columns(6).HeaderText = "Precio Unit."
            .Columns(6).DataPropertyName = "PRECIO_UNITARIO"
            .Columns(6).Width = 90

            .Columns(7).Name = "descuento"
            .Columns(7).HeaderText = "Descuento"
            .Columns(7).DataPropertyName = "DESCUENTO"
            .Columns(7).Width = 90

            .Columns(8).Name = "total"
            .Columns(8).HeaderText = "SubTotal"
            .Columns(8).DataPropertyName = "TOTAL"
            .Columns(8).Width = 90

            .Columns(9).Name = "nro_fila"
            .Columns(9).DataPropertyName = "NRO_ITEM"
            .Columns(9).Visible = False
        End With
        LoadPage()
    End Sub

    Private Sub gestionFactura_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Using captura = New capturarMedicamentoCompra(idOrdenCompra, idComprobante, -1, -1, lblIdProveedor.Text)
            If DialogResult.OK = captura.ShowDialog() Then
                Me.totalRegistro = dtSource.Rows.Count
                Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                Me.paginaActual = 1
                Me.regNro = 0
                LoadPage()
                calcularTotal()
                If txtDescuento.Text.Length > 0 Then
                    txtTotalComprobante.Text = CDec(txtSubtotal.Text) - CDec(txtDescuento.Text)
                End If
            End If
        End Using
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim form As verDetalleSolicitud = New verDetalleSolicitud(idOrdenCompra)
        Using form
            If DialogResult.OK = form.ShowDialog() Then

            End If
        End Using
    End Sub
    Private Sub calcularTotal()
        Dim total As Decimal = 0
        Dim descuento As Decimal = 0
        For Each item In dtSource.Select("ACCION <> 3")
            total += CDec(item("TOTAL"))
            descuento += CDec(item("DESCUENTO"))
        Next
        txtSubtotal.Text = total + descuento
        txtDescuento.Text = descuento
        txtTotalComprobante.Text = CDec(txtSubtotal.Text) - CDec(txtDescuento.Text) - CDec(txtDescuentoComprobante.Text)
    End Sub

    Private Sub txtDescuento_TextChanged(sender As Object, e As EventArgs) Handles txtDescuento.TextChanged
        If txtDescuento.Text.Length > 0 Then
            txtTotalComprobante.Text = CDec(txtSubtotal.Text) - CDec(txtDescuento.Text)
        End If
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        'guardar(1)
        'Dim form = New detalle_solicitud_compra(idOrdenCompra)
        'mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
        If formularioValido() Then
            Dim msg As String = ""
            Using scope As New TransactionScope()
                Try
                    If idComprobante > 0 Then
                        modificar(2)
                    Else
                        guardar(2)
                    End If
                    guardarComprobanteDetalle(idComprobante)
                    Dim form = New detalle_solicitud_compra(idOrdenCompra)
                    mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
                    scope.Complete()
                Catch ex As Exception
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Inconsistencia en la base de datos"
                    msg = ex.Message.ToString
                    response = MsgBox(msg, style, title)
                End Try
            End Using
        End If
    End Sub
    Protected Function formularioValido() As Boolean
        Dim valido As Boolean = True
        If CInt(lblIdProveedor.Text) = -1 Then
            valido = False
            mod_conf.mensajeError("Error", "Seleccione un Proveedor Valido")
            Return False
        End If
        If cmbTipoComprobante.SelectedValue = -1 Then
            valido = False
            mod_conf.mensajeError("Error", "Seleccione un Tipo de Comprobante Valido")
            Return False
        End If
        If txtNroComprobante.Text.Length = 0 Then
            valido = False
            mod_conf.mensajeError("Error", "Debe Ingresar un numero de Comprobante Valido")
            Return False
        End If
        If cmbTipoMoneda.SelectedValue = -1 Then
            valido = False
            mod_conf.mensajeError("Error", "Seleccione un Tipo de Comprobante Valido")
            Return False
        End If
        Return valido
    End Function
    Protected Sub guardarComprobanteDetalle(ByVal idComprobante As Integer)
        Dim tbl = New datos()
        Dim fVencimiento As String = ""
        For Each item As DataRow In dtSource.Select("ACCION <> 0", "NRO_ITEM ASC, ACCION DESC")
            fVencimiento = ""
            If item("NUEVO") = 1 Then
                If item("ACCION") <> 3 Then
                    tbl.tabla = "COMPROBANTE_DETALLE"
                    tbl.campoID = "ID_COMPROBANTE_DETALLE"
                    tbl.agregarCampoValor("ID_COMPROBANTE", idComprobante)
                    tbl.agregarCampoValor("CANTIDAD", item("CANTIDAD"))
                    tbl.agregarCampoValor("PRECIO_UNITARIO", item("PRECIO_UNITARIO"))
                    tbl.agregarCampoValor("LOTE", item("LOTE"))
                    If CBool(item("CONTROL_FECHA")) Then
                        tbl.agregarCampoValor("FECHA_VENCIMIENTO", item("FECHA_VENCIMIENTO"))
                    End If
                    tbl.agregarCampoValor("ID_INSUMO", "null")
                    tbl.agregarCampoValor("CONTROL_FECHA", item("CONTROL_FECHA"))
                    tbl.agregarCampoValor("ID_MEDICAMENTO", item("ID_MEDICAMENTO"))
                    tbl.agregarCampoValor("DESCUENTO", item("DESCUENTO"))
                    tbl.insertar()
                    If Not IsDBNull(item("FECHA_VENCIMIENTO")) Then
                        fVencimiento = item("FECHA_VENCIMIENTO")
                    End If
                    nuevoKardex(item("LOTE"), fVencimiento, item("ID_MEDICAMENTO"), tbl.valorID, item("CANTIDAD"))
                    insertarInventario(tbl.valorID, item("ID_MEDICAMENTO"), item("CANTIDAD"), item("PRECIO_UNITARIO"), item("CONTROL_FECHA"), fVencimiento, item("LOTE"))
                End If
            Else
                If item("ACCION") = 3 Then
                    tbl.tabla = "MOVIMIENTO_KARDEX"
                    tbl.campoID = "ID_KARDEX"
                    tbl.valorID = item("ID_KARDEX")
                    tbl.eliminar()

                    tbl.tabla = "KARDEX"
                    tbl.campoID = "ID_KARDEX"
                    tbl.valorID = item("ID_KARDEX")
                    tbl.eliminar()

                    tbl.tabla = "INVENTARIO"
                    tbl.campoID = "ID_COMPRA"
                    tbl.valorID = item("ID_COMPROBANTE_DETALLE")
                    tbl.eliminar()

                    tbl.tabla = "COMPROBANTE_DETALLE"
                    tbl.campoID = "ID_COMPROBANTE_DETALLE"
                    tbl.valorID = item("ID_COMPROBANTE_DETALLE")
                    tbl.eliminar()

                End If
            End If
            tbl.reset()
        Next
    End Sub
    Protected Sub nuevoKardex(ByVal nroLote As String, ByVal f_vencimiento As String, ByVal idMedicamento As Integer, ByVal idComprobanteDetalle As Integer, ByVal cantidad As Integer)
        Dim tbl = New datos
        tbl.tabla = "KARDEX"
        tbl.campoID = "ID_KARDEX"
        tbl.agregarCampoValor("NRO_LOTE", nroLote)
        tbl.agregarCampoValor("F_VENCIMIENTO", f_vencimiento)
        tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
        tbl.agregarCampoValor("CERRADO", 0)
        tbl.agregarCampoValor("ID_COMPROBANTE_DETALLE", idComprobanteDetalle)
        tbl.insertar()
        movimientoKardex(tbl.valorID, cantidad)
    End Sub
    Protected Sub movimientoKardex(ByVal idKardex As Integer, ByVal cantidad As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 1)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub
    Protected Sub insertarInventario(ByVal idComprobante As Integer, ByVal idMedicamento As Integer, ByVal cantidad As Integer, ByVal precioUnitario As Decimal, ByVal controlFecha As Boolean, ByVal f_vencimiento As String, ByVal lote As String)
        Dim datos = New datos()
        datos.tabla = "INVENTARIO"
        datos.campoID = "ID_INVENTARIO"
        datos.modoID = "auto"
        datos.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
        datos.agregarCampoValor("ID_COMPRA", idComprobante)
        datos.agregarCampoValor("CANTIDAD", cantidad)
        datos.agregarCampoValor("PRECIO_UNITARIO", precioUnitario)
        datos.agregarCampoValor("CONTROL_FECHA", controlFecha)
        datos.agregarCampoValor("LOTE", lote)
        If controlFecha Then
            datos.agregarCampoValor("F_VENCIMIENTO", f_vencimiento)
        End If
        datos.agregarCampoValor("FECHA", "serverDateTime")
        datos.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        datos.insertar()

    End Sub

    Private Sub guardar(ByVal estado As Integer)
        Dim tbl = New datos()
        tbl.tabla = "COMPROBANTE"
        tbl.campoID = "ID_COMPROBANTE"
        tbl.agregarCampoValor("ID_ORDEN_COMPRA", idOrdenCompra)
        tbl.agregarCampoValor("ID_MONEDA", cmbTipoMoneda.SelectedValue)
        tbl.agregarCampoValor("ID_ESTADO", estado)
        tbl.agregarCampoValor("ID_TIPO_COMPROBANTE", cmbTipoComprobante.SelectedValue)
        tbl.agregarCampoValor("TC", txtTC.Text)
        tbl.agregarCampoValor("ID_PROVEEDOR", lblIdProveedor.Text)
        tbl.agregarCampoValor("FECHA", txtFecha.Text)
        tbl.agregarCampoValor("NRO_AUTORIZACION", txtNroAutorizacion.Text)
        tbl.agregarCampoValor("CODIGO_CONTROL", txtCodigoControl.Text)
        tbl.agregarCampoValor("TOTAL", txtSubtotal.Text)
        tbl.agregarCampoValor("NRO_COMPROBANTE", txtNroComprobante.Text)
        tbl.agregarCampoValor("DESCUENTO", txtDescuentoComprobante.Text)
        tbl.insertar()
        idComprobante = tbl.valorID
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim form = New detalle_solicitud_compra(idOrdenCompra)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click

    End Sub

    Private Sub btnConcluir_Click(sender As Object, e As EventArgs) Handles btnConcluir.Click
        If formularioValido() Then
            Dim msg As String = ""
            Using scope As New TransactionScope()
                Try
                    If idComprobante > 0 Then
                        modificar(2)
                    Else
                        guardar(2)
                    End If
                    guardarComprobanteDetalle(idComprobante)
                    Dim form = New detalle_solicitud_compra(idOrdenCompra)
                    mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
                    scope.Complete()
                Catch ex As Exception
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Inconsistencia en la base de datos"
                    msg = ex.Message.ToString
                    response = MsgBox(msg, style, title)
                End Try
            End Using
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using captura = New capturarMedicamentoCompra(idOrdenCompra, idComprobante, -1, -1, lblIdProveedor.Text, 3)
            If DialogResult.OK = captura.ShowDialog() Then
                Me.totalRegistro = dtSource.Rows.Count
                Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                Me.paginaActual = 1
                Me.regNro = 0
                LoadPage()
                calcularTotal()
                If txtDescuento.Text.Length > 0 Then
                    txtTotalComprobante.Text = CDec(txtSubtotal.Text) - CDec(txtDescuento.Text)
                End If
            End If
        End Using
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Select Case mod_conf.mensajeConfirmacion("Eliminar Medicamento de la lista", "Esta seguro de Eliminar el Medicamento Seleccionado?")
            Case MsgBoxResult.Yes
                Dim NRO_ITEM = dgvGrilla.SelectedCells(8).Value
                For Each row As DataRow In dtSource.Rows
                    If CStr(row("NRO_ITEM")) = NRO_ITEM Then
                        row("ACCION") = 3
                        Exit For
                    End If
                Next
                LoadPage()
                calcularTotal()
        End Select
    End Sub

    Private Sub txtDescuentoComprobante_TextChanged(sender As Object, e As EventArgs) Handles txtDescuentoComprobante.TextChanged
        If txtDescuentoComprobante.Text.Length > 0 Then
            txtTotalComprobante.Text = CDec(txtSubtotal.Text) - CDec(txtDescuento.Text) - CDec(txtDescuentoComprobante.Text)
        End If
    End Sub

    Private Sub modificar(ByVal estado As Integer)
        Dim tbl = New datos()
        tbl.tabla = "COMPROBANTE"
        tbl.campoID = "ID_COMPROBANTE"
        tbl.valorID = idComprobante
        tbl.agregarCampoValor("ID_ORDEN_COMPRA", "null")
        tbl.agregarCampoValor("ID_MONEDA", cmbTipoMoneda.SelectedValue)
        tbl.agregarCampoValor("ID_ESTADO", estado)
        tbl.agregarCampoValor("ID_TIPO_COMPROBANTE", cmbTipoComprobante.SelectedValue)
        tbl.agregarCampoValor("TC", txtTC.Text)
        tbl.agregarCampoValor("ID_PROVEEDOR", lblIdProveedor.Text)
        tbl.agregarCampoValor("FECHA", txtFecha.Text)
        tbl.agregarCampoValor("NRO_AUTORIZACION", txtNroAutorizacion.Text)
        tbl.agregarCampoValor("CODIGO_CONTROL", txtCodigoControl.Text)
        tbl.agregarCampoValor("TOTAL", txtSubtotal.Text)
        tbl.agregarCampoValor("NRO_COMPROBANTE", txtNroComprobante.Text)
        tbl.agregarCampoValor("DESCUENTO", txtDescuentoComprobante.Text)
        tbl.modificar()
    End Sub
End Class