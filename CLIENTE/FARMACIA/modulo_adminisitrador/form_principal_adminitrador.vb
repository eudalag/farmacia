﻿Public Class form_principal_adminitrador
    Private mod_conf As modulo = New modulo()    
    Private Sub form_principal_adminitrador_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        form_sesion.Dispose()
        form_sesion.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblHora.Text = TimeString
    End Sub

    Private Sub form_principal_adminitrador_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Timer1.Enabled = True
        lblHora.Text = TimeString
        Dim form = New form_informacion_basica()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    'Private Sub btnCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim form = New form_cliente()
    '    mod_conf.cambiarForm(panelContenedor, form)
    'End Sub

   

    'Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Dispose()
    '    form_sesion.txtContrasenha.Text = ""
    '    form_sesion.txtUsuario.Text = ""
    '    form_sesion.cusuario = New CAPA_NEGOCIO.eUsuario()
    '    form_sesion.Show()
    'End Sub

    'Private Sub Button3_Click(sender As Object, e As EventArgs)
    '    Dim tipo As Integer = 2
    '    Using nuevo_pedido = New auxiliarCodControl()
    '        If DialogResult.OK = nuevo_pedido.ShowDialog() Then
    '        End If
    '    End Using
    'End Sub

  
    Private Sub ListaDeUsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDeUsuariosToolStripMenuItem.Click
        Dim form = New form_usuarios()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevoToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem2.Click
        Dim form = New form_gestion_usuario()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ListaDeClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDeClientesToolStripMenuItem.Click
        Dim form = New form_cliente()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevoToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem1.Click
        Dim form = New form_gestionCliente()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ListaDeProveedoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDeProveedoresToolStripMenuItem.Click
        Dim form = New form_proveedor()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevoProveedorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoProveedorToolStripMenuItem.Click
        Dim form = New form_gestionProveedor()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ListaDeProductosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDeProductosToolStripMenuItem.Click
        Dim form = New form_medicamentos()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        Dim form = New form_gestionMedicamento()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ListaDeMedicoToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub NuevoMedicoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim form = New form_gestionMedicos()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ConfiguracionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfiguracionToolStripMenuItem.Click
        Dim form = New form_configuracion()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ComprasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ComprasToolStripMenuItem.Click
        Dim form = New form_gestion_pedido()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub FormaFarmaceuticaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FormaFarmaceuticaToolStripMenuItem.Click
        Dim form = New form_tipo_medicamento()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub RecepcionDeCompraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecepcionDeCompraToolStripMenuItem.Click
        Dim form = New solicitud_compra()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub InventarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioToolStripMenuItem.Click
        Dim form = New form_inventario()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevaVentaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaVentaToolStripMenuItem.Click
        Dim form = New form_pedido()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub


    Private Sub AperturaDePaqCajaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AperturaDePaqCajaToolStripMenuItem.Click
        Dim form = New form_gestion_apertura()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    'Private Sub InventarioInicialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioInicialToolStripMenuItem.Click
    '    Dim form = New form_inventario_inicial()
    '    mod_conf.cambiarForm(panelContenedor, form)
    'End Sub

    Private Sub VencimientosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VencimientosToolStripMenuItem.Click
        Dim form = New form_baja_medicamento()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub


    Private Sub SolicitudToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SolicitudToolStripMenuItem.Click

    End Sub

    Private Sub RecepcionDeMedicamentosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecepcionDeMedicamentosToolStripMenuItem.Click
        Dim form = New form_compra()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub



    Private Sub InventarioGeneralToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventarioGeneralToolStripMenuItem.Click
        Dim parametros As String = ""
        Dim form = New visualizador_reporte(2, parametros)
        form.Show()
    End Sub

    Private Sub MovimientosPorLoteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MovimientosPorLoteToolStripMenuItem.Click
        Dim Form = New form_medicamentos(2)
        mod_conf.cambiarForm(panelContenedor, Form)
    End Sub

    Private Sub VentasPsicotropicoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VentasPsicotropicoToolStripMenuItem.Click
        Dim Form = New form_venta_general()
        mod_conf.cambiarForm(panelContenedor, Form)
    End Sub

    Private Sub CerrarSesionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarSesionToolStripMenuItem.Click
        Me.Dispose()
        form_sesion.txtContrasenha.Text = ""
        form_sesion.txtUsuario.Text = ""
        form_sesion.cusuario = New CAPA_NEGOCIO.eUsuario()
        form_sesion.Show()
    End Sub

    Private Sub SalirDelSistemaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirDelSistemaToolStripMenuItem.Click
        Me.Close()
        form_sesion.Close()
    End Sub

    Private Sub CambiarContraseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarContraseToolStripMenuItem.Click

        Using Form = New form_cambiar_contrasenha()
            Form.ShowDialog()
        End Using

    End Sub

    Private Sub ConsultaDeCierresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaDeCierresToolStripMenuItem.Click
        Dim Form = New form_cierre_caja()
        mod_conf.cambiarForm(panelContenedor, Form)
    End Sub

    Private Sub MedicoToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Dim Form = New form_medico()
        mod_conf.cambiarForm(panelContenedor, Form)
    End Sub

    Private Sub ActivarFacturacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ActivarFacturacionToolStripMenuItem.Click
        Dim form = New auxiliarCodControl()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub DosificacionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DosificacionToolStripMenuItem.Click
        Dim form = New dosificacion_factura()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ExcepcionesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ExcepcionesToolStripMenuItem1.Click

    End Sub

    Private Sub GestionDeCobranzaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionDeCobranzaToolStripMenuItem.Click
        Dim form = New cuentas_x_cobrar()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub InformeCuentaPorCobrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InformeCuentaPorCobrarToolStripMenuItem.Click
        Dim form = New cuentas_cobrar()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub VentasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VentasToolStripMenuItem.Click
        Dim form = New form_ventas()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub NuevaVentaDistribuidorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaVentaDistribuidorToolStripMenuItem.Click
        Dim form = New pedido_mayor()
        mod_conf.cambiarForm(panelContenedor, form)
    End Sub

    Private Sub ExistenciaPorLaboratorioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExistenciaPorLaboratorioToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As inf_existencia = New inf_existencia()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub

    Private Sub InformeDeVentaPorLaboratorioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InformeDeVentaPorLaboratorioToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As inventario_general_laboratorio_reporte = New inventario_general_laboratorio_reporte()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub

    Private Sub EstadoDeResultadoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EstadoDeResultadoToolStripMenuItem.Click
        form_ventas.Close()
        Dim informe As informeEstadoResultado = New informeEstadoResultado()
        mod_conf.cambiarForm(Me.panelContenedor, informe)
    End Sub
End Class