﻿Imports CAPA_NEGOCIO
Public Class form_informe_administrador

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If rbtMedicamento.Checked Then
            Dim form_reporte As form_reporte_medicamento = New form_reporte_medicamento()
            form_reporte.ShowDialog()
        End If
        If rbtStock.Checked Then
            Dim form_reporte As form_reporte_medicamento_minimo = New form_reporte_medicamento_minimo()
            form_reporte.ShowDialog()
        End If
        If RbtVencido.Checked Then
            Dim mes As Integer = obtenerMes(ddlMes.SelectedItem)
            If mes = -1 Or txtAnio.Text.Length < 4 Then
                Dim conf As modulo = New modulo()
                conf.mensajeError("Dato Incorrecto", "El mes o año ingresado no es correcto")
            Else
                Dim form_reporte As form_reporte_medicamento_vencidos = New form_reporte_medicamento_vencidos(mes, txtAnio.Text)
                form_reporte.ShowDialog()
            End If

        End If
        If rbtCompra.Checked Then
            'Dim form_reporte As form_reporte_compra = New form_reporte_compra(txtDesdeCompras.Text, txtHastaCompras.Text)
            'form_reporte.ShowDialog()
        End If
        If rbtnVenta.Checked Then
            Dim form_reporte As form_reporte_ventas = New form_reporte_ventas(txtDesdeVentas.Text, txtHastaVentas.Text, ddlUsuario.SelectedValue)
            form_reporte.ShowDialog()
        End If
        If rbtnVtasProd.Checked Then
            Dim form_reporte As form_reporte_venta_producto = New form_reporte_venta_producto(txtDesdeVtaProd.Text, txtHastaVtaProd.Text)
            form_reporte.ShowDialog()
        End If
    End Sub

    Private Sub RbtVencido_CheckedChanged(sender As Object, e As EventArgs) Handles RbtVencido.CheckedChanged
        deshabilitarComponentes()
        txtAnio.Enabled = True
        ddlMes.Enabled = True
        If CType(sender, RadioButton).Checked Then
            txtAnio.Text = Year(Now())
            Dim mes = Now().ToString("MMMM").ToUpper
            ddlMes.SelectedItem = mes
        End If
        'txtFechaMedVenc.Enabled = True
    End Sub

    Private Sub rbtStock_CheckedChanged(sender As Object, e As EventArgs) Handles rbtStock.CheckedChanged
        deshabilitarComponentes()
    End Sub

    Private Sub rbtMedicamento_CheckedChanged(sender As Object, e As EventArgs) Handles rbtMedicamento.CheckedChanged
        deshabilitarComponentes()
    End Sub


    Private Sub form_informe_administrador_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        deshabilitarComponentes()
        iniciarCombo()
    End Sub
    Protected Sub iniciarCombo()
        Dim tUsuario As eUsuario = New eUsuario()
        ddlUsuario.DataSource = tUsuario.mostrarComboUsuario()
        ddlUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        ddlUsuario.ValueMember = "ID_USUARIO"
    End Sub

    Private Sub rbtCompra_CheckedChanged(sender As Object, e As EventArgs) Handles rbtCompra.CheckedChanged
        deshabilitarComponentes()
        txtDesdeCompras.Enabled = True
        txtHastaCompras.Enabled = True
    End Sub

    Private Sub rbtnVenta_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnVenta.CheckedChanged
        deshabilitarComponentes()
        txtDesdeVentas.Enabled = True
        txtHastaVentas.Enabled = True
        ddlUsuario.Enabled = True
    End Sub
    Private Sub deshabilitarComponentes()
        txtDesdeCompras.Enabled = False
        txtHastaCompras.Enabled = False
        txtDesdeVentas.Enabled = False
        txtHastaVentas.Enabled = False
        ddlUsuario.Enabled = False
        'txtFechaMedVenc.Enabled = False
        txtDesdeVtaProd.Enabled = False
        txtHastaVtaProd.Enabled = False
        txtAnio.Enabled = False
        ddlMes.Enabled = False
    End Sub

    Private Sub rbtnVtasProd_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnVtasProd.CheckedChanged
        deshabilitarComponentes()
        txtDesdeVtaProd.Enabled = True
        txtHastaVtaProd.Enabled = True
    End Sub

    Private Sub rbtnInventario_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnInventario.CheckedChanged
        deshabilitarComponentes()
    End Sub
    Private Function obtenerMes(ByVal mes As String) As Integer
        If Not mes Is Nothing Then
            Select Case mes.ToUpper
                Case "ENERO"
                    Return 1
                Case "FEBRERO"
                    Return 2
                Case "MARZO"
                    Return 3
                Case "ABRIL"
                    Return 4
                Case "MAYO"
                    Return 5
                Case "JUNIO"
                    Return 6
                Case "JULIO"
                    Return 7
                Case "AGOSTO"
                    Return 8
                Case "SEPTIEMBRE"
                    Return 9
                Case "OCTUBRE"
                    Return 10
                Case "NOVIEMBRE"
                    Return 11
                Case "DICIEMBRE"
                    Return 12
                Case Else
                    Return -1
            End Select
        Else
            Return -1
        End If
        
    End Function

    Private Sub txtAnio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAnio.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtHastaCompras_ValueChanged(sender As Object, e As EventArgs) Handles txtHastaCompras.ValueChanged

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click

    End Sub

    Private Sub txtDesdeCompras_ValueChanged(sender As Object, e As EventArgs) Handles txtDesdeCompras.ValueChanged

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class