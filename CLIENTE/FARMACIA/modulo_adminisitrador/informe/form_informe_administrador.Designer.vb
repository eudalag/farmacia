﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_informe_administrador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_informe_administrador))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbtMedicamento = New System.Windows.Forms.RadioButton()
        Me.rbtCompra = New System.Windows.Forms.RadioButton()
        Me.RbtVencido = New System.Windows.Forms.RadioButton()
        Me.rbtStock = New System.Windows.Forms.RadioButton()
        Me.rbtnVenta = New System.Windows.Forms.RadioButton()
        Me.txtDesdeVentas = New System.Windows.Forms.DateTimePicker()
        Me.txtHastaVentas = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtHastaCompras = New System.Windows.Forms.DateTimePicker()
        Me.txtDesdeCompras = New System.Windows.Forms.DateTimePicker()
        Me.ddlUsuario = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtHastaVtaProd = New System.Windows.Forms.DateTimePicker()
        Me.txtDesdeVtaProd = New System.Windows.Forms.DateTimePicker()
        Me.rbtnVtasProd = New System.Windows.Forms.RadioButton()
        Me.rbtnInventario = New System.Windows.Forms.RadioButton()
        Me.ddlMes = New System.Windows.Forms.ComboBox()
        Me.txtAnio = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(550, 57)
        Me.Panel1.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 26)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Informes:"
        '
        'rbtMedicamento
        '
        Me.rbtMedicamento.AutoSize = True
        Me.rbtMedicamento.Location = New System.Drawing.Point(12, 101)
        Me.rbtMedicamento.Name = "rbtMedicamento"
        Me.rbtMedicamento.Size = New System.Drawing.Size(94, 17)
        Me.rbtMedicamento.TabIndex = 76
        Me.rbtMedicamento.TabStop = True
        Me.rbtMedicamento.Text = "Medicamentos"
        Me.rbtMedicamento.UseVisualStyleBackColor = True
        '
        'rbtCompra
        '
        Me.rbtCompra.AutoSize = True
        Me.rbtCompra.Location = New System.Drawing.Point(12, 78)
        Me.rbtCompra.Name = "rbtCompra"
        Me.rbtCompra.Size = New System.Drawing.Size(66, 17)
        Me.rbtCompra.TabIndex = 79
        Me.rbtCompra.TabStop = True
        Me.rbtCompra.Text = "Compras"
        Me.rbtCompra.UseVisualStyleBackColor = True
        Me.rbtCompra.Visible = False
        '
        'RbtVencido
        '
        Me.RbtVencido.AutoSize = True
        Me.RbtVencido.Location = New System.Drawing.Point(12, 153)
        Me.RbtVencido.Name = "RbtVencido"
        Me.RbtVencido.Size = New System.Drawing.Size(194, 17)
        Me.RbtVencido.TabIndex = 80
        Me.RbtVencido.TabStop = True
        Me.RbtVencido.Text = "Medicamentos Vencidos al Periodo:"
        Me.RbtVencido.UseVisualStyleBackColor = True
        '
        'rbtStock
        '
        Me.rbtStock.AutoSize = True
        Me.rbtStock.Location = New System.Drawing.Point(12, 127)
        Me.rbtStock.Name = "rbtStock"
        Me.rbtStock.Size = New System.Drawing.Size(179, 17)
        Me.rbtStock.TabIndex = 81
        Me.rbtStock.TabStop = True
        Me.rbtStock.Text = "Medicamentos con stock minimo"
        Me.rbtStock.UseVisualStyleBackColor = True
        '
        'rbtnVenta
        '
        Me.rbtnVenta.AutoSize = True
        Me.rbtnVenta.Location = New System.Drawing.Point(12, 205)
        Me.rbtnVenta.Name = "rbtnVenta"
        Me.rbtnVenta.Size = New System.Drawing.Size(61, 17)
        Me.rbtnVenta.TabIndex = 82
        Me.rbtnVenta.TabStop = True
        Me.rbtnVenta.Text = "Ventas:"
        Me.rbtnVenta.UseVisualStyleBackColor = True
        '
        'txtDesdeVentas
        '
        Me.txtDesdeVentas.Enabled = False
        Me.txtDesdeVentas.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesdeVentas.Location = New System.Drawing.Point(146, 205)
        Me.txtDesdeVentas.Name = "txtDesdeVentas"
        Me.txtDesdeVentas.Size = New System.Drawing.Size(95, 20)
        Me.txtDesdeVentas.TabIndex = 84
        '
        'txtHastaVentas
        '
        Me.txtHastaVentas.Enabled = False
        Me.txtHastaVentas.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHastaVentas.Location = New System.Drawing.Point(291, 205)
        Me.txtHastaVentas.Name = "txtHastaVentas"
        Me.txtHastaVentas.Size = New System.Drawing.Size(95, 20)
        Me.txtHastaVentas.TabIndex = 85
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(99, 207)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 86
        Me.Label1.Text = "Desde:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(247, 207)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 87
        Me.Label3.Text = "Hasta:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.Location = New System.Drawing.Point(464, 362)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 32)
        Me.btnAceptar.TabIndex = 75
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(247, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 91
        Me.Label4.Text = "Hasta:"
        Me.Label4.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(99, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Desde:"
        Me.Label5.Visible = False
        '
        'txtHastaCompras
        '
        Me.txtHastaCompras.Enabled = False
        Me.txtHastaCompras.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHastaCompras.Location = New System.Drawing.Point(291, 78)
        Me.txtHastaCompras.Name = "txtHastaCompras"
        Me.txtHastaCompras.Size = New System.Drawing.Size(95, 20)
        Me.txtHastaCompras.TabIndex = 89
        Me.txtHastaCompras.Visible = False
        '
        'txtDesdeCompras
        '
        Me.txtDesdeCompras.Enabled = False
        Me.txtDesdeCompras.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesdeCompras.Location = New System.Drawing.Point(146, 78)
        Me.txtDesdeCompras.Name = "txtDesdeCompras"
        Me.txtDesdeCompras.Size = New System.Drawing.Size(95, 20)
        Me.txtDesdeCompras.TabIndex = 88
        Me.txtDesdeCompras.Visible = False
        '
        'ddlUsuario
        '
        Me.ddlUsuario.FormattingEnabled = True
        Me.ddlUsuario.Location = New System.Drawing.Point(392, 204)
        Me.ddlUsuario.Name = "ddlUsuario"
        Me.ddlUsuario.Size = New System.Drawing.Size(147, 21)
        Me.ddlUsuario.TabIndex = 92
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(291, 233)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 97
        Me.Label6.Text = "Hasta:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(143, 233)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 96
        Me.Label7.Text = "Desde:"
        '
        'txtHastaVtaProd
        '
        Me.txtHastaVtaProd.Enabled = False
        Me.txtHastaVtaProd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHastaVtaProd.Location = New System.Drawing.Point(335, 231)
        Me.txtHastaVtaProd.Name = "txtHastaVtaProd"
        Me.txtHastaVtaProd.Size = New System.Drawing.Size(95, 20)
        Me.txtHastaVtaProd.TabIndex = 95
        '
        'txtDesdeVtaProd
        '
        Me.txtDesdeVtaProd.Enabled = False
        Me.txtDesdeVtaProd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesdeVtaProd.Location = New System.Drawing.Point(190, 231)
        Me.txtDesdeVtaProd.Name = "txtDesdeVtaProd"
        Me.txtDesdeVtaProd.Size = New System.Drawing.Size(95, 20)
        Me.txtDesdeVtaProd.TabIndex = 94
        '
        'rbtnVtasProd
        '
        Me.rbtnVtasProd.AutoSize = True
        Me.rbtnVtasProd.Location = New System.Drawing.Point(12, 231)
        Me.rbtnVtasProd.Name = "rbtnVtasProd"
        Me.rbtnVtasProd.Size = New System.Drawing.Size(129, 17)
        Me.rbtnVtasProd.TabIndex = 93
        Me.rbtnVtasProd.TabStop = True
        Me.rbtnVtasProd.Text = "Ventas por productos:"
        Me.rbtnVtasProd.UseVisualStyleBackColor = True
        '
        'rbtnInventario
        '
        Me.rbtnInventario.AutoSize = True
        Me.rbtnInventario.Location = New System.Drawing.Point(12, 254)
        Me.rbtnInventario.Name = "rbtnInventario"
        Me.rbtnInventario.Size = New System.Drawing.Size(112, 17)
        Me.rbtnInventario.TabIndex = 98
        Me.rbtnInventario.TabStop = True
        Me.rbtnInventario.Text = "Inventario General"
        Me.rbtnInventario.UseVisualStyleBackColor = True
        '
        'ddlMes
        '
        Me.ddlMes.FormattingEnabled = True
        Me.ddlMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO ", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE ", "NOVIEMBRE", "DICIEMBRE"})
        Me.ddlMes.Location = New System.Drawing.Point(224, 152)
        Me.ddlMes.Name = "ddlMes"
        Me.ddlMes.Size = New System.Drawing.Size(105, 21)
        Me.ddlMes.TabIndex = 99
        '
        'txtAnio
        '
        Me.txtAnio.Location = New System.Drawing.Point(335, 153)
        Me.txtAnio.MaxLength = 4
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.Size = New System.Drawing.Size(51, 20)
        Me.txtAnio.TabIndex = 100
        Me.txtAnio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'form_informe_administrador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 406)
        Me.Controls.Add(Me.txtAnio)
        Me.Controls.Add(Me.ddlMes)
        Me.Controls.Add(Me.rbtnInventario)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtHastaVtaProd)
        Me.Controls.Add(Me.txtDesdeVtaProd)
        Me.Controls.Add(Me.rbtnVtasProd)
        Me.Controls.Add(Me.ddlUsuario)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtHastaCompras)
        Me.Controls.Add(Me.txtDesdeCompras)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtHastaVentas)
        Me.Controls.Add(Me.txtDesdeVentas)
        Me.Controls.Add(Me.rbtnVenta)
        Me.Controls.Add(Me.rbtStock)
        Me.Controls.Add(Me.RbtVencido)
        Me.Controls.Add(Me.rbtCompra)
        Me.Controls.Add(Me.rbtMedicamento)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "form_informe_administrador"
        Me.Text = "form_informe_administrador"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents rbtMedicamento As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCompra As System.Windows.Forms.RadioButton
    Friend WithEvents RbtVencido As System.Windows.Forms.RadioButton
    Friend WithEvents rbtStock As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnVenta As System.Windows.Forms.RadioButton
    Friend WithEvents txtDesdeVentas As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtHastaVentas As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtHastaCompras As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtDesdeCompras As System.Windows.Forms.DateTimePicker
    Friend WithEvents ddlUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtHastaVtaProd As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtDesdeVtaProd As System.Windows.Forms.DateTimePicker
    Friend WithEvents rbtnVtasProd As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnInventario As System.Windows.Forms.RadioButton
    Friend WithEvents ddlMes As System.Windows.Forms.ComboBox
    Friend WithEvents txtAnio As System.Windows.Forms.TextBox
End Class
