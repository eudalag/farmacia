﻿Imports CAPA_DATOS
Public Class cuentas_cobrar
    Protected Sub iniciarComboGrupoEconomico()
        Dim sSql As String = ""
        sSql += " SELECT [ID_GRUPO], [GRUPO_PERSONAS] FROM [GRUPO_CLIENTES]"
        sSql += " UNION ALL SELECT -1, '[SELECCIONE GRUPO ECONOMICO]'"
        sSql += " ORDER BY [GRUPO_PERSONAS]"

        cmbGrupoEconomico.DataSource = New datos().ejecutarConsulta(sSql)
        cmbGrupoEconomico.DisplayMember = "GRUPO_PERSONAS"
        cmbGrupoEconomico.ValueMember = "ID_GRUPO"
    End Sub

    Private Sub cuentas_cobrar_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarComboGrupoEconomico()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim parametros As String = cmbGrupoEconomico.SelectedValue.ToString + ";" + txtDesdeCompras.Text + ";" + txtHastaCompras.Text
        Dim form = New visualizador_reporte(12, parametros)
        form.Show()
    End Sub
End Class