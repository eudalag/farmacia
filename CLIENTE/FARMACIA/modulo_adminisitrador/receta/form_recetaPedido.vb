﻿Public Class form_recetaPedido
    Private idReceta As Integer
    Private _registroPaciente As Integer
    Private mod_conf As modulo = New modulo()
    Private doctor As String = ""

    Private Sub btnLimpiarReceta_Click(sender As Object, e As EventArgs) Handles btnLimpiarReceta.Click

        idReceta = -1
        txtRegistroReceta.Text = ""
        txtDoctor.Text = ""

    End Sub

    Private Sub btnBuscarReceta_Click(sender As Object, e As EventArgs) Handles btnBuscarReceta.Click
        If _registroPaciente > 0 Then
            Dim form As form_buscarReceta = New form_buscarReceta(_registroPaciente)
            Using form
                If DialogResult.OK = form.ShowDialog() Then
                    txtDoctor.Text = doctor
                    txtRegistroReceta.Text = Microsoft.VisualBasic.Right(New String("0", 6) + idReceta.ToString, 6)
                End If
            End Using
        Else
            mod_conf.mensajeError("Agregar Receta", "Error, Para Registrar o buscar una receta necesita tener seleccionado el nombre del paciente.")
        End If
    End Sub

    Private Sub btnGestionReceta_Click(sender As Object, e As EventArgs) Handles btnGestionReceta.Click
        If _registroPaciente > 0 Then
            Dim form As form_gestionReceta = New form_gestionReceta(_registroPaciente)
            Using form
                If DialogResult.OK = form.ShowDialog() Then
                    txtDoctor.Text = doctor
                    txtRegistroReceta.Text = Microsoft.VisualBasic.Right(New String("0", 6) + idReceta.ToString, 6)
                End If
            End Using
        Else
            mod_conf.mensajeError("Agregar Receta", "Error, Para Registrar una receta necesita tener seleccionado el nombre del paciente.")
        End If
    End Sub
End Class