﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_recetaPedido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_recetaPedido))
        Me.btnGestionReceta = New System.Windows.Forms.Button()
        Me.txtDoctor = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtRegistroReceta = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.btnLimpiarReceta = New System.Windows.Forms.Button()
        Me.btnBuscarReceta = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGestionReceta
        '
        Me.btnGestionReceta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGestionReceta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGestionReceta.Location = New System.Drawing.Point(525, 90)
        Me.btnGestionReceta.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGestionReceta.Name = "btnGestionReceta"
        Me.btnGestionReceta.Size = New System.Drawing.Size(188, 27)
        Me.btnGestionReceta.TabIndex = 127
        Me.btnGestionReceta.Text = "Registrar Receta"
        Me.btnGestionReceta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGestionReceta.UseVisualStyleBackColor = True
        '
        'txtDoctor
        '
        Me.txtDoctor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDoctor.Location = New System.Drawing.Point(68, 94)
        Me.txtDoctor.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDoctor.Name = "txtDoctor"
        Me.txtDoctor.ReadOnly = True
        Me.txtDoctor.Size = New System.Drawing.Size(451, 20)
        Me.txtDoctor.TabIndex = 125
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(17, 97)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(42, 13)
        Me.Label29.TabIndex = 126
        Me.Label29.Text = "Doctor:"
        '
        'txtRegistroReceta
        '
        Me.txtRegistroReceta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRegistroReceta.Location = New System.Drawing.Point(68, 68)
        Me.txtRegistroReceta.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtRegistroReceta.Name = "txtRegistroReceta"
        Me.txtRegistroReceta.ReadOnly = True
        Me.txtRegistroReceta.Size = New System.Drawing.Size(68, 20)
        Me.txtRegistroReceta.TabIndex = 121
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(10, 71)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(49, 13)
        Me.Label28.TabIndex = 122
        Me.Label28.Text = "Registro:"
        '
        'btnLimpiarReceta
        '
        Me.btnLimpiarReceta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiarReceta.Image = CType(resources.GetObject("btnLimpiarReceta.Image"), System.Drawing.Image)
        Me.btnLimpiarReceta.Location = New System.Drawing.Point(189, 65)
        Me.btnLimpiarReceta.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnLimpiarReceta.Name = "btnLimpiarReceta"
        Me.btnLimpiarReceta.Size = New System.Drawing.Size(35, 25)
        Me.btnLimpiarReceta.TabIndex = 124
        Me.btnLimpiarReceta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLimpiarReceta.UseVisualStyleBackColor = True
        '
        'btnBuscarReceta
        '
        Me.btnBuscarReceta.Image = CType(resources.GetObject("btnBuscarReceta.Image"), System.Drawing.Image)
        Me.btnBuscarReceta.Location = New System.Drawing.Point(148, 65)
        Me.btnBuscarReceta.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnBuscarReceta.Name = "btnBuscarReceta"
        Me.btnBuscarReceta.Size = New System.Drawing.Size(35, 25)
        Me.btnBuscarReceta.TabIndex = 123
        Me.btnBuscarReceta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnBuscarReceta.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Green
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(751, 45)
        Me.Panel1.TabIndex = 128
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(8, 7)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(183, 26)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Gestión Receta:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.Location = New System.Drawing.Point(531, 190)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(100, 39)
        Me.btnAceptar.TabIndex = 129
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Image = CType(resources.GetObject("btnVolver.Image"), System.Drawing.Image)
        Me.btnVolver.Location = New System.Drawing.Point(639, 190)
        Me.btnVolver.Margin = New System.Windows.Forms.Padding(4)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(100, 39)
        Me.btnVolver.TabIndex = 130
        Me.btnVolver.Text = "&Volver"
        Me.btnVolver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'form_recetaPedido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(752, 242)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnGestionReceta)
        Me.Controls.Add(Me.txtDoctor)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.btnLimpiarReceta)
        Me.Controls.Add(Me.btnBuscarReceta)
        Me.Controls.Add(Me.txtRegistroReceta)
        Me.Controls.Add(Me.Label28)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "form_recetaPedido"
        Me.Text = "Receta - Pedido"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGestionReceta As System.Windows.Forms.Button
    Friend WithEvents txtDoctor As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents btnLimpiarReceta As System.Windows.Forms.Button
    Friend WithEvents btnBuscarReceta As System.Windows.Forms.Button
    Friend WithEvents txtRegistroReceta As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnVolver As System.Windows.Forms.Button
End Class
