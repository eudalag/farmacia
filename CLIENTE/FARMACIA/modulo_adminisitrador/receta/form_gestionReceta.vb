﻿Imports CAPA_DATOS

Public Class form_gestionReceta
    Private idReceta As Integer = 0
    Private tipo As Integer
    Private mod_conf As modulo = New modulo()

    Public Shared medico As String
    Public Shared idMedico As String
    Public Shared paciente As String
    Public Shared idPaciente As String

    Public Sub New(ByVal _idPaciente As Integer, Optional _tipo As Integer = 1)
        InitializeComponent()
        If _idPaciente > 0 Then
            tipo = _tipo
            idPaciente = _idPaciente
            paciente = New datos().ejecutarConsulta("SELECT NOMBRE_APELLIDOS FROM PACIENTE WHERE ID_PACIENTE = " + idPaciente.ToString)(0)(0)
            txtPaciente.Text = paciente
            btnBuscar.Visible = False
        End If
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnBuscarMedico.Click
        Dim formMedico As form_buscarMedico = New form_buscarMedico()
        Using formMedico
            If formMedico.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtMedico.Text = medico
            End If
        End Using
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim formPaciente As form_buscar_pasciente = New form_buscar_pasciente()
        Using formPaciente
            If formPaciente.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtPaciente.Text = medico
            End If
        End Using
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If modal Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            volver()
        End If

    End Sub
    Private Sub volver()
        Me.Close()
        'mod_conf.volver(New form_receta())
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim tblReceta = New datos()
            tblReceta.tabla = "RECETA"
            tblReceta.campoID = "ID_RECETA"
            tblReceta.modoID = "sql"
            tblReceta.agregarCampoValor("ID_PACIENTE", idPaciente)
            tblReceta.agregarCampoValor("ID_MEDICO", idMedico)
            tblReceta.agregarCampoValor("FECHA", txtFechaReceta.Text)
            tblReceta.agregarCampoValor("VIGENTE", chkFinalizada.Checked)
            tblReceta.agregarCampoValor("OBSERVACION", txtObservacion.Text)
            If idReceta = 0 Then
                tblReceta.insertar()
                idReceta = tblReceta.valorID
            Else
                tblReceta.valorID = idReceta
                tblReceta.modificar()
            End If

            If modal Then
                form_pedido._idReceta = idReceta
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                volver()
            End If
        
        End If
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtPaciente.Text).Length > 0 Then
            ErrorProvider1.SetError(txtPaciente, "")
        Else
            mensaje = "Seleccione el Nombre del paciente."
            ErrorProvider1.SetError(txtPaciente, mensaje)
            valido = False
        End If

        If Trim(txtMedico.Text).Length > 0 Then
            ErrorProvider1.SetError(txtMedico, "")
        Else
            mensaje = "Seleccione el Nombre del medico."
            ErrorProvider1.SetError(txtMedico, mensaje)
            valido = False
        End If
        Return valido
    End Function
End Class