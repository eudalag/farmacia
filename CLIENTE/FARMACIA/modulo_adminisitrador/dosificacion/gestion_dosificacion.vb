﻿Imports CAPA_NEGOCIO

Public Class gestion_dosificacion

    Private mod_conf As modulo = New modulo()
    Private tDosificacion As eDosificacion
    Private _idDosificacion As Integer = -1
    Public Sub New(ByVal idDosificacion As Integer)
        InitializeComponent()
        _idDosificacion = idDosificacion
        tDosificacion = New eDosificacion(idDosificacion)
        cargarDatos()
    End Sub    
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tDosificacion = New eDosificacion()
    End Sub
    Private Sub cargarDatos()
        txtNoAutorizacion.Text = tDosificacion.NUMERO_AUTORIZACION
        txtFechaLimiteEmision.Text = tDosificacion.F_LIMITE_EMISION
        txtNoFactura.Text = tDosificacion.N_FACT_INICIAL
        txtLlave.Text = tDosificacion.LLAVE_DOSIFICACION
        chkActivo.Checked = tDosificacion.ACTIVO
    End Sub
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        mod_conf.volver(New dosificacion_factura())
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        tDosificacion.NUMERO_AUTORIZACION = txtNoAutorizacion.Text
        tDosificacion.F_LIMITE_EMISION = txtFechaLimiteEmision.Text
        tDosificacion.N_FACT_INICIAL = txtNoFactura.Text
        tDosificacion.LLAVE_DOSIFICACION = txtLlave.Text
        tDosificacion.ACTIVO = chkActivo.Checked
        If _idDosificacion = -1 Then
            tDosificacion.insertar()
        Else
            tDosificacion.modificar()
        End If
        If tDosificacion.mensajesError <> "" Then
            mod_conf.mensajeError("Error", tDosificacion.mensajesError)
        Else
            Me.Close()
            mod_conf.volver(New dosificacion_factura())
        End If
    End Sub
End Class