﻿Imports CAPA_NEGOCIO
Public Class dosificacion_factura
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100
            .Height = 240
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 5
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_DOSIFICACION"
            .Columns(0).Width = 80

            .Columns(1).Name = "nro_autorizacion"
            .Columns(1).HeaderText = "Número de Autorizacion"
            .Columns(1).DataPropertyName = "NUMERO_AUTORIZACION"
            .Columns(1).Width = 550
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "fecha"
            .Columns(2).HeaderText = "F. Lim. Emisión"
            .Columns(2).DataPropertyName = "F_LIMITE_EMISION"
            .Columns(2).Width = 170
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(3).Name = "nro_factura"
            .Columns(3).HeaderText = "Fact. Inicial"
            .Columns(3).DataPropertyName = "N_FACT_INICIAL"
            .Columns(3).Width = 150
            .Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(4).Name = "activo"
            .Columns(4).HeaderText = "Activo"
            .Columns(4).DataPropertyName = "ACTIVO_M"
            .Columns(4).Width = 150
            .Columns(4).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
        End With
        LoadPage()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New gestion_dosificacion()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)        
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Dim id_dosificacion As Integer = 0
        id_dosificacion = dgvGrilla.SelectedCells(0).Value

        Dim tDosificacion = New eDosificacion()
        tDosificacion.ID_DOSIFICACION = id_dosificacion
        tDosificacion.eliminar()        
        If tDosificacion.mensajesError = "" Then
            dtSource = tDosificacion.mostrarDosificacion()
            Me.totalRegistro = dtSource.Rows.Count
            Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            Me.paginaActual = 1
            Me.regNro = 0
            LoadPage()
        Else
            mod_conf.mensajeError("Error", tDosificacion.mensajesError)
        End If
    End Sub

    Private Sub dosificacion_factura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tDosificacion = New eDosificacion()
        dtSource = tDosificacion.mostrarDosificacion()
        inicializarParametros()        
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim id_dosificacion As Integer = 0
        id_dosificacion = dgvGrilla.SelectedCells(0).Value
        Me.Close()
        Dim form_gestion = New gestion_dosificacion(id_dosificacion)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub
End Class