﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data

Public Class form_buscar_medicamento
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    'Private dtSourceVademecum As DataTable
    Private dtSourceGeneral As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 20
    Private _tipo As Integer
    Private generico As Boolean
    Private _buscar As String = ""
    Private tMedicamento As eMedicamento = New eMedicamento()
    Private idProveedor As Integer
    Dim controlStock As Integer
    Public Sub New(Optional ByVal tipo As Integer = 1)
        InitializeComponent()
        _tipo = tipo
        idProveedor = -1
    End Sub
    Public Sub New(ByVal tipo As Integer, ByVal _idProveedor As Integer)
        InitializeComponent()
        _tipo = tipo
        idProveedor = _idProveedor

    End Sub

    Public Sub New(ByVal tipo As Integer, ByVal buscar As String, ByVal _generico As Boolean)
        InitializeComponent()
        _tipo = tipo
        _buscar = buscar
        generico = _generico
    End Sub

    'Private Sub iniciarCombo()
    '    Dim tTipoMedicamento As eTipoMedicamento = New eTipoMedicamento()
    '    cmbTipoMedicamento.DataSource = tTipoMedicamento.mostrarComboTipoMedicamento()
    '    cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
    '    cmbTipoMedicamento.ValueMember = "ID_TIPO_MEDICAMENTO"
    'End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    'Protected Sub iniciarComboLaboratorio()
    '    Dim sSql As String = ""
    '    Dim tblCombo = New datos()

    '    sSql = "SELECT L.ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO L"
    '    If idProveedor <> -1 Then
    '        sSql += " INNER JOIN PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO  = L.ID_LABORATORIO "
    '        sSql += " WHERE PL.ID_PROVEEDOR = " + idProveedor.ToString
    '        sSql += " UNION ALL SELECT -1 , '[Todos los laboratorios]' ORDER BY DESCRIPCION"
    '    Else
    '        sSql += " UNION ALL SELECT -1 , '[Todos los laboratorios]' ORDER BY DESCRIPCION"
    '    End If


    '    cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
    '    cmbLaboratorio.DisplayMember = "DESCRIPCION"
    '    cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    'End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            Select Case _tipo
                Case 2
                    .ColumnCount = 10
                    .Columns(0).Name = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Visible = False

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 450

                    .Columns(2).Name = "composicion"
                    .Columns(2).HeaderText = "Nombre Generico"
                    .Columns(2).DataPropertyName = "COMPOSICION"
                    .Columns(2).Width = 320

                    .Columns(3).Name = "laboratorio"
                    .Columns(3).HeaderText = "Laboratorio"
                    .Columns(3).DataPropertyName = "LABORATORIO"
                    .Columns(3).Width = 150

                    .Columns(4).Name = "precio"
                    .Columns(4).HeaderText = "Prec."
                    .Columns(4).DataPropertyName = "PRECIO_UNITARIO"
                    .Columns(4).Width = 50

                    .Columns(5).Name = "stock"
                    .Columns(5).HeaderText = "Stock"
                    .Columns(5).DataPropertyName = "STOCK"
                    .Columns(5).Width = 50
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    .Columns(6).Name = "vencimiento"
                    .Columns(6).HeaderText = "Vencimiento"
                    .Columns(6).DataPropertyName = "F_VENCIMIENTO"
                    .Columns(6).Width = 80
                    .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    .Columns(7).Name = "cant_mayor"
                    .Columns(7).DataPropertyName = "CANTIDAD_MAYOR"
                    .Columns(7).Visible = False

                    .Columns(8).Name = "precio_mayor"
                    .Columns(8).DataPropertyName = "PRECIO_MAYOR"
                    .Columns(8).Visible = False

                    .Columns(9).Name = "cod_barra"
                    .Columns(9).DataPropertyName = "CODIGO_BARRA"
                    .Columns(9).Visible = False

                Case 7
                    .ColumnCount = 8
                    .Columns(0).Name = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Visible = False

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 350

                    .Columns(2).Name = "Laboratorio"
                    .Columns(2).HeaderText = "Laboratorio"
                    .Columns(2).DataPropertyName = "LABORATORIO"
                    .Columns(2).Width = 150

                    .Columns(3).Name = "tipo_medicamento"
                    .Columns(3).HeaderText = "Form. Farm."
                    .Columns(3).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
                    .Columns(3).Width = 150

                    .Columns(4).Name = "minimo"
                    .Columns(4).HeaderText = "Minimo"
                    .Columns(4).DataPropertyName = "MINIMO"
                    .Columns(4).Width = 65
                    .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(5).Name = "maximo"
                    .Columns(5).HeaderText = "Maximo"
                    .Columns(5).DataPropertyName = "MAXIMO"
                    .Columns(5).Width = 65
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(6).Name = "stock"
                    .Columns(6).HeaderText = "Stock"
                    .Columns(6).DataPropertyName = "STOCK"
                    .Columns(6).Width = 70
                    .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(7).Name = "composicion"
                    .Columns(7).DataPropertyName = "COMPOSICION"
                    .Columns(7).Visible = False
                Case 8
                    .ColumnCount = 9
                    .Columns(0).Name = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Visible = False

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 350

                    .Columns(2).Name = "Laboratorio"
                    .Columns(2).HeaderText = "Laboratorio"
                    .Columns(2).DataPropertyName = "LABORATORIO"
                    .Columns(2).Width = 150

                    .Columns(3).Name = "tipo_medicamento"
                    .Columns(3).HeaderText = "Form. Farm."
                    .Columns(3).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
                    .Columns(3).Width = 150

                    .Columns(4).Name = "minimo"
                    .Columns(4).HeaderText = "Minimo"
                    .Columns(4).DataPropertyName = "MINIMO"
                    .Columns(4).Width = 65
                    .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(5).Name = "maximo"
                    .Columns(5).HeaderText = "Maximo"
                    .Columns(5).DataPropertyName = "MAXIMO"
                    .Columns(5).Width = 65
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(6).Name = "stock"
                    .Columns(6).HeaderText = "Stock"
                    .Columns(6).DataPropertyName = "STOCK"
                    .Columns(6).Width = 70
                    .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(7).Name = "composicion"
                    .Columns(7).DataPropertyName = "COMPOSICION"
                    .Columns(7).Visible = False

                    .Columns(8).Name = "insumo"
                    .Columns(8).DataPropertyName = "ID_INSUMO"
                    .Columns(8).Visible = False
                Case Else
                    .ColumnCount = 7
                    .Columns(0).Name = "Registro"
                    .Columns(0).HeaderText = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Visible = True
                    .Columns(1).Width = 100

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 250

                    .Columns(2).Name = "Laboratorio"
                    .Columns(2).HeaderText = "Laboratorio"
                    .Columns(2).DataPropertyName = "LABORATORIO"
                    .Columns(2).Width = 150

                    .Columns(3).Name = "descripcion"
                    .Columns(3).HeaderText = "Composición"
                    .Columns(3).DataPropertyName = "COMPOSICION"
                    .Columns(3).Width = 150

                    .Columns(4).Name = "tipo_medicamento"
                    .Columns(4).HeaderText = "Form. Farm."
                    .Columns(4).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
                    .Columns(4).Width = 100
                    If _tipo = 1 Then
                        .Columns(5).Name = "minimo"
                        .Columns(5).HeaderText = "Stock Min."
                        .Columns(5).DataPropertyName = "MINIMO"
                        .Columns(5).Width = 50
                    Else
                        .Columns(5).Name = "cantidad"
                        .Columns(5).HeaderText = "Prec."
                        .Columns(5).DataPropertyName = "PRECIO_UNITARIO"
                        .Columns(5).Width = 50
                    End If
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                    .Columns(6).Name = "stock"
                    .Columns(6).DataPropertyName = "STOCK"
                    .Columns(6).Width = 50
                    .Columns(6).Visible = True
                    .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            End Select

        End With
        LoadPage()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        seleccionarItem()
    End Sub
    Private Function obtenerTablaMedicamentoOrdenCompra(ByVal buscar As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(150),@CONTROLADO AS BIT,@TIPO_MEDICAMENTO AS INTEGER, @ORDEN_COMPRA AS INTEGER;"
        sSql += " SET @BUSCAR = '" + buscar + "';"
        sSql += " SET @CONTROLADO =  0;"
        sSql += " SET @TIPO_MEDICAMENTO = -1;"
        sSql += " SET @ORDEN_COMPRA = " + gestionFactura.idOrdenCompra.ToString + ";"

        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, MARCA, TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO, M.COMPOSICION, M.MINIMO,"
        sSql += " ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, I.ID_INSUMO,"
        sSql += " ISNULL(IA.TOTAL_INVENTARIO,0) + ISNULL(COMP.TOTAL_COMPRA,0) + ISNULL(VENT.TOTAL_VENTA,0) AS STOCK"
        sSql += " FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN INSUMOS I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO AND I.ID_ORDEN_COMPRA = @ORDEN_COMPRA"
        sSql += " LEFT JOIN COMPROBANTE_DETALLE CD ON CD.ID_INSUMO = I.ID_INSUMO "
        sSql += " LEFT JOIN (SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS TOTAL_INVENTARIO FROM INVENTARIO I "
        sSql += " WHERE ID_COMPRA IS NULL AND ID_VENTA IS NULL"
        sSql += " GROUP BY ID_MEDICAMENTO) IA ON IA.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN (SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS TOTAL_COMPRA FROM INVENTARIO I "
        sSql += " WHERE NOT ID_COMPRA IS NULL AND ID_VENTA IS NULL"
        sSql += " GROUP BY ID_MEDICAMENTO"
        sSql += " ) COMP ON COMP.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN (SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS TOTAL_VENTA FROM INVENTARIO I "
        sSql += " WHERE ID_COMPRA IS NULL AND NOT ID_VENTA IS NULL"
        sSql += " GROUP BY ID_MEDICAMENTO) VENT ON VENT.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE ((M.NOMBRE LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (L.DESCRIPCION LIKE '%' + @BUSCAR + '%') OR (TM.DESCRIPCION  LIKE '%' + @BUSCAR + '%')) "
        sSql += " AND (M.CONTROLADO = CASE WHEN @CONTROLADO = 1 THEN @CONTROLADO ELSE M.CONTROLADO END)"
        sSql += " AND (M.ID_TIPO_MEDICAMENTO = CASE WHEN @TIPO_MEDICAMENTO = -1 THEN M.ID_TIPO_MEDICAMENTO ELSE @TIPO_MEDICAMENTO END)"
        sSql += " AND I.CANTIDAD_SOLICITADA - ISNULL(CD.CANTIDAD,0)  > 0 "
        sSql += " ORDER BY MEDICAMENTO, LABORATORIO,MARCA"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Function obtenerTablaMedicamentoFactura(ByVal buscar As String, ByVal idProveedor As Integer) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(150),@CONTROLADO AS BIT,@TIPO_MEDICAMENTO AS INTEGER, @ID_PROVEEDOR AS INTEGER;"
        sSql += " SET @BUSCAR = '" + buscar + "';"
        sSql += " SET @CONTROLADO =  0;"
        sSql += " SET @TIPO_MEDICAMENTO = -1;"
        sSql += " SET @ID_PROVEEDOR = " + idProveedor.ToString + ";"
        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL THEN ' x ' + M.CONCENTRACION ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, MARCA, TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO, M.COMPOSICION, M.MINIMO,"
        sSql += " ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO,ISNULL(K.STOCK,0) AS STOCK "
        sSql += " FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO = L.ID_LABORATORIO "
        sSql += " LEFT JOIN (SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS STOCK FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX KM ON KM.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE K.CERRADO = 0"
        sSql += " GROUP BY ID_MEDICAMENTO) K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO     "
        sSql += " WHERE ((M.NOMBRE LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (L.DESCRIPCION LIKE '%' + @BUSCAR + '%') OR (TM.DESCRIPCION  LIKE '%' + @BUSCAR + '%')) "
        sSql += " AND (M.CONTROLADO = CASE WHEN @CONTROLADO = 1 THEN @CONTROLADO ELSE M.CONTROLADO END)"
        sSql += " AND (M.ID_TIPO_MEDICAMENTO = CASE WHEN @TIPO_MEDICAMENTO = -1 THEN M.ID_TIPO_MEDICAMENTO ELSE @TIPO_MEDICAMENTO END) "
        sSql += " AND PL.ID_PROVEEDOR = CASE WHEN @ID_PROVEEDOR = 10004 THEN PL.ID_PROVEEDOR ELSE @ID_PROVEEDOR END  "
        sSql += " ORDER BY MEDICAMENTO, LABORATORIO,MARCA"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Function obtenerVademecum() As DataTable
        Dim sSql As String = ""
        sSql += " SELECT RIGHT('000000' + CAST(ID_VADEMECUM AS nvarchar(6)),6) AS ID_MEDICAMENTO, NOMBRE AS MEDICAMENTO, LABORATORIO AS LABORATORIO, "
        sSql += " PRESENTACION AS DESC_TIPO_MEDICAMENTO,'0.00' AS PRECIO_UNITARIO,0.00 AS STOCK,0.00 AS CANTIDAD_MAYOR, 0.00 AS PRECIO_MAYOR, 1 AS VADEMECUM"
        sSql += " FROM VADEMECUM ORDER BY MEDICAMENTO, LABORATORIO, DESC_TIPO_MEDICAMENTO"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub form_buscar_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        controlStock = IIf(form_sesion.cConfiguracion.CONTROL_STOCK, 2, 1)
        txtBuscar.Focus()
        dtSourceGeneral = tMedicamento.mostrarMedicamento(_buscar, False)
        buscar()
        inicializarParametros()
        txtBuscar.Text = IIf(generico, "*", "") + _buscar
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs)
        buscar()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()

    End Sub
    Private Sub buscar()
        Select Case _tipo
            Case 6
                dtSource = tMedicamento.mostrarMedicamento(txtBuscar.Text, -1, False)
            Case 7
                'dtSource = tMedicamento.mostrarMedicamento(txtBuscar.Text, -1, 0, idProveedor, cmbLaboratorio.SelectedValue)
                Dim buscar = txtBuscar.Text
                buscar = buscar.Replace("'", "")
                buscar = buscar.Replace("--", "")
                Dim parametros = buscar.Split("*")

                'Dim tbl = (From m In dtSource Where m!MEDICAMENTO.Contains("/" + parametros(0) + "/") Select m).AsEnumerable
                'If tbl.Count > 0 Then
                '    dtSource = tbl.CopyToDataTable
                'End If
                Dim cadenaFiltro = ""
                If parametros.Length > 1 Then
                    cadenaFiltro += " AND COMPOSICION LIKE '%" + parametros(1) + "%'"
                End If
                If parametros.Length > 2 Then
                    cadenaFiltro += " AND DESC_TIPO_MEDICAMENTO LIKE '%" + parametros(2) + "%'"
                End If
                If parametros.Length > 3 Then
                    cadenaFiltro += " AND LABORATORIO LIKE '%" + parametros(3) + "%'"
                End If

                Dim dtaux = dtSourceGeneral
                Dim dtSourceAux As DataTable = dtSourceGeneral
                dtSourceGeneral = dtaux
                Dim tbl = dtSourceGeneral.Select("(MEDICAMENTO like '%" + parametros(0) + "%')" + cadenaFiltro + IIf(Not chkStock.Checked, " AND STOCK > 0", ""))
                If tbl.Count > 0 Then
                    dtSource = tbl.CopyToDataTable
                Else
                    dtSource = dtSourceGeneral.Clone
                End If

                'If chkVademecum.Checked Then
                '    Dim tbl2 = dtSourceVademecum.Select("MEDICAMENTO like '%" + parametros(0) + "%'" + cadenaFiltro)
                '    If tbl2.Count > 0 Then
                '        dtSource.Merge(tbl2.CopyToDataTable)
                '    Else
                '        If dtSource.Rows.Count = 0 Then
                '            dtSource = dtSource.Clone
                '        End If
                '    End If
                'End If
            Case 8
                dtSource = obtenerTablaMedicamentoOrdenCompra(txtBuscar.Text)
            Case 9
                dtSource = obtenerTablaMedicamentoFactura(txtBuscar.Text, idProveedor)
            Case Else
                Dim buscar = txtBuscar.Text
                buscar = buscar.Replace("'", "")
                buscar = buscar.Replace("--", "")
                Dim parametros = buscar.Split("*")

                Dim cadenaFiltro = ""
                If parametros.Length > 1 Then
                    cadenaFiltro += " AND COMPOSICION LIKE '%" + parametros(1) + "%'"
                End If
                If parametros.Length > 2 Then
                    cadenaFiltro += " AND DESC_TIPO_MEDICAMENTO LIKE '%" + parametros(2) + "%'"
                End If
                If parametros.Length > 3 Then
                    cadenaFiltro += " AND LABORATORIO LIKE '%" + parametros(3) + "%'"
                End If
                Dim dtaux = dtSourceGeneral
                Dim dtSourceAux As DataTable = dtSourceGeneral
                dtSourceGeneral = dtaux
                Dim tbl = dtSourceGeneral.Select("(MEDICAMENTO like '%" + parametros(0) + "%')" + cadenaFiltro)
                If tbl.Count > 0 Then
                    dtSource = tbl.CopyToDataTable
                Else
                    dtSource = dtSourceGeneral.Clone
                End If

                'If chkVademecum.Checked Then
                '    Dim tbl2 = dtSourceVademecum.Select("MEDICAMENTO like '%" + parametros(0) + "%'" + cadenaFiltro)
                '    If tbl2.Count > 0 Then
                '        dtSource.Merge(tbl2.CopyToDataTable)
                '    Else
                '        If dtSource.Rows.Count = 0 Then
                '            dtSource = dtSource.Clone
                '        End If
                '    End If
                'End If
        End Select
    End Sub


    Private Sub guardar()
        '    Dim foundRows() As DataRow
        '    foundRows = form_pedido.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")
        '    Dim dr As DataRow
        '    If foundRows.Length = 0 Then
        '        dr = form_pedido.dtSource.NewRow
        '        dr("NRO_ITEM") = 1
        '        dr("ID_VENTA") = 0
        '        dr("ID_MEDICAMENTO") =
        '        dr("CANTIDAD") = 1
        '        dr("PRECIO_UNITARIO") = dgvGrilla.SelectedCells(5).Value
        '        dr("NOMBRE") =
        '        dr("TOTAL") = dgvGrilla.SelectedCells(5).Value
        '        form_pedido.dtSource.Rows.Add(dr)
        '    Else
        '        dr = form_pedido.dtSource.NewRow
        '        dr("NRO_ITEM") = CInt(form_pedido.dtSource.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
        '        dr("ID_VENTA") = 0
        '        dr("ID_MEDICAMENTO") = dgvGrilla.SelectedCells(0).Value
        '        dr("CANTIDAD") = 1
        '        dr("PRECIO_UNITARIO") = dgvGrilla.SelectedCells(5).Value
        '        dr("NOMBRE") = dgvGrilla.SelectedCells(1).Value
        '        dr("TOTAL") = dgvGrilla.SelectedCells(5).Value
        '        form_pedido.dtSource.Rows.Add(dr)
        '    End If
        '    form_pedido.dtSource.AcceptChanges()
    End Sub
    Private Function existeInsumo(ByVal idItem As Integer)
        Select Case _tipo
            Case 2
                Dim foundRows() As DataRow
                foundRows = form_pedido.dtSource.Select("ID_MEDICAMENTO = " & idItem.ToString & " AND ACCION <> 3")
                Return foundRows.Count > 0
            Case 3
                Dim foundRows() As DataRow
                foundRows = form_gestion_cotizacion.dtSource.Select("ID_MEDICAMENTO = " & idItem.ToString)
                Return foundRows.Count > 0
            Case Else
                Return False
        End Select
    End Function

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs)
        If _tipo = 6 Then
            dtSource = tMedicamento.mostrarMedicamento(txtBuscar.Text, -1, False)
        Else
            dtSource = tMedicamento.mostrarMedicamento(txtBuscar.Text, -1, False)
        End If
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            dgvGrilla.Focus()
        End If
    End Sub

    Private Sub dgvGrilla_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvGrilla.KeyDown
        If e.KeyValue = 13 Then
            seleccionarItem()
        End If
    End Sub


    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        seleccionarItem()
    End Sub

    Private Sub txtBuscar_TextChanged_1(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        btnBuscar_Click(Nothing, Nothing)
    End Sub
    Private Sub seleccionarItem()
        Dim msg As String = ""
        If dgvGrilla.Rows.Count = 0 Then
            msg = "Debe seleccionar por lo menos 1 insumo."
        ElseIf existeInsumo(dgvGrilla.SelectedCells(0).Value) Then
            msg = "El Item seleccionado ya existe en la solicitud."
        End If
        Dim nombre As String ' NOMBRE = MEDICAMENTO x CONCENTRACION - MARCA ( LABORATORIO )
        nombre = dgvGrilla.SelectedCells(1).Value
        'nombre += " (" + dgvGrilla.SelectedCells(3).Value + ")"
        If msg = "" Then
            Dim continuar As Boolean = True
            Select Case _tipo
                Case 1
                    Dim idMedicamento As Integer = CType(dgvGrilla.SelectedCells(0), DataGridViewTextBoxCell).Value
                    'form_gestionCompra.tMedicamento.cargarDatos(idMedicamento)
                Case 2
                    'guardar()                       
                    
                        If dgvGrilla.SelectedCells(5).Value <= 0 Then
                            continuar = mod_conf.mensajeConfirmacion("Stock", "El producto que selecciono no cuenta con Stock, Desea forzar la venta del medicamento seleccionado?") = MsgBoxResult.Yes
                        End If
                        If continuar Then
                        form_pedido.nuevoItem(dgvGrilla.SelectedCells(0).Value, nombre, dgvGrilla.SelectedCells(2).Value, 1, dgvGrilla.SelectedCells(4).Value, dgvGrilla.SelectedCells(5).Value, 1, 1, _
                                    dgvGrilla.SelectedCells(7).Value, dgvGrilla.SelectedCells(8).Value, dgvGrilla.SelectedCells(9).Value)
                        End If

                Case 3
                    form_gestion_cotizacion.nuevoItem(dgvGrilla.SelectedCells(0).Value, nombre, 1, dgvGrilla.SelectedCells(5).Value, dgvGrilla.SelectedCells(2).Value, 1, 1)
                Case 4
                    form_gestion_apertura.idMedicamentoPaq = dgvGrilla.SelectedCells(0).Value
                Case 5
                    form_gestion_apertura.idMedicamentoUnid = dgvGrilla.SelectedCells(0).Value
                Case 6
                    form_add_med_inventario_inicial.idMedicamento = dgvGrilla.SelectedCells(0).Value
                Case 7
                    form_gestion_pedido.nuevoItem(dgvGrilla.SelectedCells(0).Value, dgvGrilla.SelectedCells(1).Value, dgvGrilla.SelectedCells(7).Value, _
                                                  dgvGrilla.SelectedCells(2).Value, dgvGrilla.SelectedCells(3).Value, dgvGrilla.SelectedCells(4).Value, dgvGrilla.SelectedCells(5).Value, 1, 1, 1)
                Case 8
                    capturarMedicamentoCompra.idMedicamento = dgvGrilla.SelectedCells(0).Value
                    capturarMedicamentoCompra.idInsumo = dgvGrilla.SelectedCells(8).Value
                Case 9
                    capturarMedicamentoCompra.idMedicamento = dgvGrilla.SelectedCells(0).Value
                Case 10
                    pedido_mayor.medicamento = dgvGrilla.SelectedCells(0).Value
            End Select
            If continuar Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End If
        Else
            mod_conf.mensajeError("Error", msg)
        End If
    End Sub

    Private Sub chkStock_CheckedChanged(sender As Object, e As EventArgs) Handles chkStock.CheckedChanged
        If Not dtSourceGeneral Is Nothing Then
            dtSourceGeneral = tMedicamento.mostrarMedicamento("", True)
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    'Private Sub chkVademecum_CheckedChanged(sender As Object, e As EventArgs)

    '    If chkVademecum.Checked() Then
    '        dtSourceVademecum = obtenerVademecum()
    '    End If
    '    If Not dtSourceGeneral Is Nothing Or Not dtSourceVademecum Is Nothing Then
    '        btnBuscar_Click(Nothing, Nothing)
    '    End If

    'End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If dgvGrilla.Rows.Count > 0 Then
            Using Form = New modal_informacion_medicamento(dgvGrilla.SelectedCells(0).Value)
                Form.ShowDialog()
            End Using
        End If
    End Sub

    Private Sub form_buscar_medicamento_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.F1
                Button1_Click(Nothing, Nothing)
            Case Keys.F5
                Dim func = New modulo()
                If func.mensajeConfirmacion("Venta Perdida", "Desea Registrar el Medicamento " + dgvGrilla.SelectedCells(1).Value.ToString + " como venta perdida?") = MsgBoxResult.Yes Then
                    func.registrarVentaPerdida(dgvGrilla.SelectedCells(0).Value, dgvGrilla.SelectedCells(8).Value)
                End If
        End Select

    End Sub
End Class