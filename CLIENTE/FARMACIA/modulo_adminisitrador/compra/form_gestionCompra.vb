﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Public Class form_gestionCompra
    Private dtSource As DataTable
    Public Shared dtSourceCompra As DataTable
    Private mod_conf As modulo = New modulo()    
    Private tCompra As eCompraMedicamento
    Private _id_inventario As Integer = -1
    Dim tMedicamentoCompra As eMedicamento = New eMedicamento()
    Dim tInventario As eInventario
    Public Sub New(ByVal idCompra As Integer)
        InitializeComponent()
        tCompra = New eCompraMedicamento(idCompra)
        'tMedicamento = New eMedicamento(tCompra.ID_MEDICAMENTO)
        cargarDatos()
        iniciarCombo()
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tCompra = New eCompraMedicamento()
        tInventario = New eInventario()
        iniciarCombo()
    End Sub
    Private Sub cargarDatos()
        txtFecha.Text = tCompra.FECHA_COMPRA
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If dtSourceCompra.Rows.Count = 0 Then
            mensaje = "No tiene Productos Seleccionado."
            ErrorProvider1.SetError(txtFecha, mensaje)
        Else
            ErrorProvider1.SetError(dgvGrillaCompra, "")
        End If
        Return valido
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
        mod_conf.volver(New form_compra())
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            tCompra.FECHA_COMPRA = txtFecha.Text
            tCompra.DETALLE = txtDetalle.Text
            tCompra.insertar()
            For Each item In dtSourceCompra.Rows                
                tInventario.ID_MEDICAMENTO = item("ID_MEDICAMENTO")
                tInventario.ID_COMPRA = tCompra.ID_COMPRA
                tInventario.ID_VENTA = "null"
                tInventario.CANTIDAD = item("CANTIDAD")
                tInventario.CONTROL_FECHA = item("CONTROL_FECHA")
                If CBool(item("CONTROL_FECHA")) Then
                    tInventario.F_VENCIMIENTO = item("F_VENCIMIENTO")
                End If
                tInventario.insertar()
            Next
            Me.Close()
            mod_conf.volver(New form_compra())
        End If
    End Sub




    Private Sub form_gestionCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtSourceCompra = tInventario.mostrarInventarioCompra(_id_inventario)
        'inicializarParametrosMedicamento()
        inicializarParametrosCompra()
        iniciarCombo()
        'txtBuscar.Focus()
    End Sub

    'Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs)
    '    If (Asc(e.KeyChar) = 13) Then
    '        e.Handled = True
    '        btnBuscar_Click_1(Nothing, Nothing)
    '        dgvGrillaMedicamento.Focus()
    '    End If
    'End Sub

    'Private Sub btnBuscar_Click_1(sender As Object, e As EventArgs)
    '    If Trim(txtBuscar.Text).Length > 0 Then
    '        dtSource = tMedicamentoCompra.mostrarMedicamento(txtBuscar.Text)
    '        dgvGrillaMedicamento.DataSource = dtSource
    '        dgvGrillaMedicamento.Focus()
    '    End If
    'End Sub
    'Private Sub inicializarParametrosMedicamento()
    '    With (dgvGrillaMedicamento)
    '        .Width = 530
    '        .Height = 350
    '        .MultiSelect = False
    '        .SelectionMode = DataGridViewSelectionMode.FullRowSelect
    '        .AutoGenerateColumns = False
    '        .ColumnCount = 4
    '        .Columns(0).Name = "medicamento"
    '        .Columns(0).HeaderText = "Medicamento"
    '        .Columns(0).DataPropertyName = "NOMBRE"
    '        .Columns(0).Width = 250

    '        .Columns(1).Name = "laboratorio"
    '        .Columns(1).HeaderText = "Laboratorio"
    '        .Columns(1).DataPropertyName = "LABORATORIO"
    '        .Columns(1).Width = 150

    '        .Columns(2).Name = "tipo_medicamento"
    '        .Columns(2).HeaderText = "Tipo de Med."
    '        .Columns(2).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
    '        .Columns(2).Width = 130

    '        .Columns(3).Name = "registro"
    '        .Columns(3).DataPropertyName = "ID_MEDICAMENTO"
    '        .Columns(3).Visible = False
    '    End With
    'End Sub

    'Private Sub dgvGrillaMedicamento_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
    '    Dim idMedicamento As Integer = CType(dgvGrillaMedicamento.Rows(e.RowIndex).Cells(1), DataGridViewTextBoxCell).Value
    '    Using fInventario = New form_compra_inventario(idMedicamento)
    '        If DialogResult.OK = fInventario.ShowDialog() Then
    '            dgvGrillaCompra.DataSource = dtSourceCompra
    '        End If
    '    End Using
    'End Sub
    Private Sub inicializarParametrosCompra()
        With (dgvGrillaCompra)
            .Width = 1120
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 8
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "NOMBRE"
            .Columns(0).Width = 700

            .Columns(1).Name = "vencimiento"
            .Columns(1).HeaderText = "F. Venc."
            .Columns(1).DataPropertyName = "F_VENCIMIENTO"
            .Columns(1).Width = 110

            .Columns(2).Name = "cantidad"
            .Columns(2).HeaderText = "Cant."
            .Columns(2).DataPropertyName = "CANTIDAD"
            .Columns(2).Width = 70

            .Columns(3).Name = "precio_unitario"
            .Columns(3).HeaderText = "Precio Compra"
            .Columns(3).DataPropertyName = "PRECIO_COMPRA"
            .Columns(3).Width = 120

            .Columns(4).Name = "total"
            .Columns(4).HeaderText = "Total"
            .Columns(4).DataPropertyName = "TOTAL"
            .Columns(4).Width = 120


            .Columns(5).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(5).Visible = False

            .Columns(6).DataPropertyName = "CONTROL_FECHA"
            .Columns(6).Visible = False

            .Columns(7).DataPropertyName = "NRO_ITEM"
            .Columns(7).Visible = False
        End With
    End Sub
    Private Sub eliminar(ByVal nrItem As Integer)
        Dim rowsToDelete As EnumerableRowCollection(Of DataRow) = Nothing
        rowsToDelete = From tbl In dtSourceCompra Where tbl!NRO_ITEM = nrItem
        For Each row As DataRow In rowsToDelete
            row.Delete()
        Next
        dtSourceCompra.AcceptChanges()
        dgvGrillaCompra.DataSource = dtSourceCompra
    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs)
        eliminar(dgvGrillaCompra.SelectedCells(5).Value)
        actualizarGrilla()
    End Sub

    Private Sub btnAnterior_Click(sender As Object, e As EventArgs) Handles btnAnterior.Click
        eliminar(dgvGrillaCompra.SelectedCells(5).Value)
        actualizarGrilla()
    End Sub

    'Private Sub btnSiguiente_Click(sender As Object, e As EventArgs) Handles btnSiguiente.Click
    '    Dim idMedicamento As Integer = dgvGrillaMedicamento.SelectedCells(3).Value
    '    Using fInventario = New form_compra_inventario(idMedicamento)
    '        If DialogResult.OK = fInventario.ShowDialog() Then
    '            dgvGrillaCompra.DataSource = dtSourceCompra
    '        End If
    '    End Using
    'End Sub

    Private Sub dgvGrillaCompra_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        Dim idMedicamento As Integer = CType(dgvGrillaCompra.Rows(e.RowIndex).Cells(3), DataGridViewTextBoxCell).Value
        Dim nroItem As Integer = CType(dgvGrillaCompra.Rows(e.RowIndex).Cells(5), DataGridViewTextBoxCell).Value
        Using fInventario = New form_compra_inventario(idMedicamento, nroItem)
            If DialogResult.OK = fInventario.ShowDialog() Then
                dgvGrillaCompra.DataSource = dtSourceCompra
            End If
        End Using
    End Sub
    Protected Sub actualizarGrilla()
        Dim cont As Integer = 0
        For Each row In dtSourceCompra.Rows
            cont += 1
            row("NRO_ITEM") = cont
        Next
        dtSource.AcceptChanges()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        If CInt(lblIdProveedor.Text) = -1 And txtNit.Text.Length > 0 Then
            mod_conf.mensajeError("Proveedor Invalido", "Necesita Ingresar los datos de un proveedor valido para ingresar el detalle de la compra")
        Else
            Using nuevo_FormaFarmaceutica = New form_compra_inventario(CInt(lblIdProveedor.Text), 1, True)
                If DialogResult.OK = nuevo_FormaFarmaceutica.ShowDialog() Then
                    dgvGrillaCompra.DataSource = dtSourceCompra
                End If
            End Using
        End If
        
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged
        Dim sSql As String = ""
        sSql += " SELECT NOMBRE,ID_PROVEEDOR FROM PROVEEDORES WHERE NIT = '" + txtNit.Text.ToString + "'"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            txtProveedor.Text = tblConsulta.Rows(0)("NOMBRE")
            lblIdProveedor.Text = tblConsulta.Rows(0)("ID_PROVEEDOR")
        Else
            txtProveedor.Text = ""
            lblIdProveedor.Text = "-1"
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using nuevo_Laboratorio = New modal_laboratorio()
            If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
                txtNit.Focus()
            End If
        End Using
    End Sub
    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT * FROM TIPO_COMPROBANTE UNION ALL"
        sSql += " SELECT -1 , '[Seleccione Tipo Comprobante]' ORDER BY ID_COMPROBANTE"

        cmbTipoComprobante.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoComprobante.DisplayMember = "DETALLE"
        cmbTipoComprobante.ValueMember = "ID_COMPROBANTE"


        sSql = "SELECT * FROM TIPO_MONEDA UNION ALL"
        sSql += " SELECT -1 , '[Seleccione Moneda]' ORDER BY ID_TIPO_MONEDA"

        cmbTipoMoneda.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMoneda.DisplayMember = "ID_TIPO_MENEDA"
        cmbTipoMoneda.ValueMember = "MONEDA"
    End Sub
End Class