﻿Imports CAPA_DATOS
Public Class comprobanteCompra
    Protected id_compra As Integer
    Protected dtSource As DataTable

    Public Sub New(ByVal _idCompra As Integer)
        InitializeComponent()
        id_compra = _idCompra
        cargarCabecera()
        inicializarParametrosPagos()
    End Sub
    Protected Sub cargarCabecera()
        Dim sSql As String = ""
        sSql += " DECLARE @ID AS INTEGER;"
        sSql += " SET @ID = " + id_compra.ToString + ";"
        sSql += " SELECT P.NIT, P.NOMBRE,"
        sSql += " TC.DETALLE, TM.MONEDA, C.FECHA, C.NRO_COMPROBANTE, C.NRO_AUTORIZACION, C.CODIGO_CONTROL,"
        sSql += " CASE WHEN CC.ID_COMPRA_CREDITO IS NULL THEN 'Efectivo' ELSE 'Credito' END AS ID_TIPO_PAGO, "
        sSql += " ISNULL(CC.F_VENCIMIENTO,'') AS F_VENCIMIENTO, EC.DESCRIPCION,"
        sSql += " C.TOTAL AS SUBTOTAL,C.DESCUENTO_GENERAL + C.DESCUENTO_GENERAL2 AS TOTAL_DESCUENTO, TOTAL - (C.DESCUENTO_GENERAL + C.DESCUENTO_GENERAL2) AS TOTAL FROM COMPRA C "
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR "
        sSql += " INNER JOIN TIPO_COMPROBANTE  TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE "
        sSql += " INNER JOIN TIPO_MONEDA TM ON TM.ID_TIPO_MONEDA = C.ID_MONEDA "
        sSql += " LEFT JOIN COMPRA_CREDITO CC ON CC.ID_COMPRA = C.ID_COMPRA "
        sSql += " INNER JOIN ESTADO_COMPRA EC ON EC.ID_ESTADO = C.ID_ESTADO "
        sSql += " WHERE C.ID_COMPRA = @ID;"

        sSql += " SELECT MEDICAMENTO +  ' (' + LABORATORIO + ') x ' + CAST(ISNULL(CANTIDAD_PAQUETE,1) AS NVARCHAR(10)) + ' ' + FORMA_FARMACEUTICA AS MEDICAMENTO,"
        sSql += " CM.PAQUETES,CM.UNIDADES,CM.PRECIO_PAQUETE,CM.PRECIO_UNIDAD,DESCUENTO ,DESCUENTO2 ,CM.APLICA_DESC_UNITARIO  FROM COMPRA_MEDICAMENTOS CM"
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = CM.ID_MEDICAMENTO "
        sSql += " WHERE ID_COMPRA = @ID"
        Dim dsConsulta = New datos().obtenerDataSet(sSql)
        dtSource = dsConsulta.Tables(1)
        Dim tbl = dsConsulta.Tables(0)
        txtNit.Text = tbl.Rows(0)("NIT")
        txtRazonSocial.Text = tbl.Rows(0)("NOMBRE")
        txtTipo.Text = tbl.Rows(0)("DETALLE")
        txtMoneda.Text = tbl.Rows(0)("MONEDA")
        txtFecha.Text = tbl.Rows(0)("FECHA")
        txtNroComprobante.Text = tbl.Rows(0)("NRO_COMPROBANTE")
        txtAutorizacion.Text = tbl.Rows(0)("NRO_AUTORIZACION")
        txtCodigoControl.Text = tbl.Rows(0)("CODIGO_CONTROL")
        txtTipoPago.Text = tbl.Rows(0)("ID_TIPO_PAGO")
        txtFechaLimite.Text = tbl.Rows(0)("F_VENCIMIENTO")
        txtEstado.Text = tbl.Rows(0)("DESCRIPCION")
        txtSubtotal.Text = tbl.Rows(0)("SUBTOTAL")
        txtTotalDescuento.Text = tbl.Rows(0)("TOTAL_DESCUENTO")
        txtTotal.Text = tbl.Rows(0)("TOTAL")
    End Sub
    Private Sub inicializarParametrosPagos()

        With (dgvGrilla)
            .Width = 525
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = False


            Dim medicamento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(medicamento)
            With medicamento
                .Name = "medicamento"
                .HeaderText = "Medicamento"
                .DataPropertyName = "MEDICAMENTO"
                .Width = 200
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim paquete As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(paquete)
            With paquete
                .Name = "paquetes"
                .HeaderText = "Cant. Paquete"
                .DataPropertyName = "PAQUETES"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim unidad As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(unidad)
            With unidad
                .Name = "unidad"
                .HeaderText = "Cant. Unidades"
                .DataPropertyName = "UNIDADES"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            .Refresh()
        End With
        dgvGrilla.DataSource = dtSource
    End Sub
End Class