﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Public Class form_compra
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private mod_conf As New modulo()
    Public Sub New()
        InitializeComponent()
        cargarDatosMovimientos()
    End Sub

    Protected Sub cargarDatosMovimientos()
        Dim sSql As String = ""
        sSql += " SELECT C.FECHA,P.NOMBRE AS PROVEEDOR, C.NRO_COMPROBANTE,TC.DETALLE AS TIPO_COMPROBANTE,"
        sSql += " C.NRO_AUTORIZACION, C.CODIGO_CONTROL, C.TOTAL, C.DESCUENTO, EC.DESCRIPCION AS ESTADO,C.ID_COMPROBANTE  FROM COMPROBANTE C "
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = C.ID_PROVEEDOR  "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = C.ID_TIPO_COMPROBANTE"
        sSql += " INNER JOIN ESTADO_COMPRA EC ON EC.ID_ESTADO = C.ID_ESTADO "
        sSql += " WHERE C.ID_ORDEN_COMPRA IS NULL"
        dtSource = New datos().ejecutarConsulta(sSql)
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1130
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 10
            .Columns(0).Name = "Fecha"
            .Columns(0).HeaderText = "Fecha"
            .Columns(0).DataPropertyName = "FECHA"
            .Columns(0).Width = 90

            .Columns(1).Name = "proveedor"
            .Columns(1).HeaderText = "Proveedor"
            .Columns(1).DataPropertyName = "PROVEEDOR"
            .Columns(1).Width = 200

            .Columns(2).Name = "nro_comprobante"
            .Columns(2).HeaderText = "Nro. Comprobante"
            .Columns(2).DataPropertyName = "NRO_COMPROBANTE"
            .Columns(2).Width = 140
            .Columns(2).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(3).Name = "tipo"
            .Columns(3).HeaderText = "Tipo Comprobante"
            .Columns(3).DataPropertyName = "TIPO_COMPROBANTE"
            .Columns(3).Width = 140

            .Columns(4).Name = "autorizacion"
            .Columns(4).HeaderText = "Nro. Autorizacion"
            .Columns(4).DataPropertyName = "NRO_AUTORIZACION"
            .Columns(4).Width = 130

            .Columns(5).Name = "codigo_control"
            .Columns(5).HeaderText = "Codigo Control"
            .Columns(5).DataPropertyName = "CODIGO_CONTROL"
            .Columns(5).Width = 120

            .Columns(6).Name = "total"
            .Columns(6).HeaderText = "Total"
            .Columns(6).DataPropertyName = "TOTAL"
            .Columns(6).Width = 100

            .Columns(7).Name = "descuento"
            .Columns(7).HeaderText = "Descuento"
            .Columns(7).DataPropertyName = "DESCUENTO"
            .Columns(7).Width = 100

            .Columns(8).Name = "estado"
            .Columns(8).HeaderText = "Estado"
            .Columns(8).DataPropertyName = "ESTADO"
            .Columns(8).Width = 100

            .Columns(9).Name = "id"
            .Columns(9).DataPropertyName = "ID_COMPROBANTE"
            .Columns(9).Visible = False

        End With
        LoadPage()
    End Sub




    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Dim form = New gestion_compra(-1)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Select Case mod_conf.mensajeConfirmacion("Anular Factura", "Esta Seguro que desea Anular la factura?")
            Case MsgBoxResult.Yes
                Dim idCompra = dgvGrilla.SelectedCells(9).Value
                Dim tbl = New datos()
                tbl.tabla = "COMPROBANTE"
                tbl.valorID = idCompra
                tbl.campoID = "ID_COMPROBANTE"
                tbl.agregarCampoValor("ID_ESTADO", 3)
                tbl.modificar()

                cargarDatosMovimientos()
                Me.totalRegistro = dtSource.Rows.Count
                Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                Me.paginaActual = 1
                Me.regNro = 0
                LoadPage()
            Case MsgBoxResult.No

        End Select
       
    End Sub

    Private Sub form_compra_Load(sender As Object, e As EventArgs) Handles Me.Load
        inicializarParametros()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idCompra = dgvGrilla.SelectedCells(9).Value
        Dim form = New gestion_compra(idCompra)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub
End Class