﻿Imports CAPA_NEGOCIO
Public Class form_configuracion
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().        
        cargarDatos()
    End Sub
    Private Sub cargarDatos()
        Dim tConfiguracion As eConfiguracion = New eConfiguracion()
        chkImprimir.Checked = tConfiguracion.IMPRIMIR_RECIBO        
        txtTitulo.Text = tConfiguracion.NOMBRE
        txtDireccion.Text = tConfiguracion.DIRECCION
        txtTelefono.Text = tConfiguracion.TELEFONO
        chkControlStock.Checked = tConfiguracion.CONTROL_STOCK
        txtNit.Text = tConfiguracion.NIT
        txtTC.Text = tConfiguracion.TC_ME
        txtCodigoBarra.Text = tConfiguracion.CODIGO_BARRA
    End Sub



    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim tconfiguracion As eConfiguracion = New eConfiguracion()        
        tconfiguracion.NOMBRE = txtTitulo.Text
        tconfiguracion.DIRECCION = txtDireccion.Text
        tconfiguracion.TELEFONO = txtTelefono.Text
        tconfiguracion.IMPRIMIR_RECIBO = chkImprimir.Checked
        tconfiguracion.CONTROL_STOCK = chkControlStock.Checked
        tconfiguracion.NIT = txtNit.Text
        tconfiguracion.TC_ME = txtTC.Text
        tconfiguracion.CODIGO_BARRA = txtCodigoBarra.Text
        tconfiguracion.modificar()

        'Me.Close() 
        'If form_principal_adminitrador.panelContenedor.Controls.Count > 0 Then
        'form_principal_adminitrador.panelContenedor.Controls.RemoveAt(0)
        'End If

        Dim mensaje As String = "Datos Actualizados con exito."
        Dim style = MsgBoxStyle.OkOnly Or _
           MsgBoxStyle.OkOnly
        Dim title = "Copia de Seguridad"
        Dim response = MsgBox(mensaje, style, title)
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTC.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox2_Click(sender As Object, e As EventArgs) Handles txtTC.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub TextBox2_Leave(sender As Object, e As EventArgs) Handles txtTC.Leave
        Dim txt As TextBox = CType(sender, TextBox)
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNit.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub txtNit_Click(sender As Object, e As EventArgs) Handles txtNit.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub
End Class