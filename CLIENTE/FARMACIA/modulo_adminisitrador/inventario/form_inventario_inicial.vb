﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class form_inventario_inicial
    Private dtSource As DataTable
    Private tInventario As eInventario = New eInventario()
    Private tMedicamentoCompra As eMedicamento = New eMedicamento()
    Private mod_conf As New modulo()
    Public Shared dtSourceCompra As DataTable

    Public Sub New(Optional compra As Boolean = False)
        InitializeComponent()
        mostrarMedicamentos()
        inicializarParametrosMedicamento()

        dtSourceCompra = tInventario.mostrarInventarioInicial(True)
        txtBuscar.Focus()
    End Sub
    
    Private Sub form_inventario_inicial_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        inicializarParametrosCompra()

    End Sub
    Private Sub eliminar(ByVal nrItem As Integer)
        Dim rowsToDelete As EnumerableRowCollection(Of DataRow) = Nothing
        rowsToDelete = From tbl In dtSourceCompra Where tbl!NRO_ITEM = nrItem
        For Each row As DataRow In rowsToDelete
            row.Delete()
        Next
        dtSourceCompra.AcceptChanges()
        dgvGrillaCompra.DataSource = dtSourceCompra
    End Sub
    Protected Sub actualizarGrilla()
        Dim cont As Integer = 0
        For Each row In dtSourceCompra.Rows
            cont += 1
            row("NRO_ITEM") = cont
        Next
        dtSource.AcceptChanges()
    End Sub
    Private Sub inicializarParametrosMedicamento()
        With (dgvGrillaMedicamento)
            .Width = 530
            .Height = 470
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 290

            .Columns(1).Name = "laboratorio"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 100

            .Columns(2).Name = "tipo_medicamento"
            .Columns(2).HeaderText = "Form. Farm."
            .Columns(2).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
            .Columns(2).Width = 140

            .Columns(3).Name = "registro"
            .Columns(3).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(3).Visible = False
        End With
    End Sub
    Private Sub inicializarParametrosCompra()
        With (dgvGrillaCompra)
            .Width = 520
            .Height = 434
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 10
            .Columns(0).Name = "medicamento"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "NOMBRE"
            .Columns(0).Width = 230

            .Columns(1).Name = "laboratorio"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 100
            .Columns(1).Visible = True

            .Columns(2).Name = "vencimiento"
            .Columns(2).HeaderText = "F. Venc."
            .Columns(2).DataPropertyName = "F_VENCIMIENTO"
            .Columns(2).Width = 70

            .Columns(3).Name = "cantidad"
            .Columns(3).HeaderText = "Cant."
            .Columns(3).DataPropertyName = "CANTIDAD"
            .Columns(3).Width = 35

            .Columns(4).Name = "precio_unitario"
            .Columns(4).HeaderText = "Precio Vta."
            .Columns(4).DataPropertyName = "PRECIO"
            .Columns(4).Width = 85

            .Columns(5).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(5).Visible = False

            .Columns(6).DataPropertyName = "CONTROL_FECHA"
            .Columns(6).Visible = False

            .Columns(7).DataPropertyName = "NRO_ITEM"
            .Columns(7).Visible = False

            .Columns(8).DataPropertyName = "LOTE"
            .Columns(8).Visible = False

            .Columns(9).Name = "precio_unitario"
            .Columns(9).HeaderText = "Prec. Compra"
            .Columns(9).DataPropertyName = "PRECIO_UNITARIO"
            .Columns(9).Width = 100
            .Columns(9).Visible = 0
        End With
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If Me.Modal Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            Dim form = New form_inventario()
            Dim mod_conf As modulo = New modulo()
            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
        End If

    End Sub
    Private Sub mostrarMedicamentos()
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(150),@CONTROLADO AS BIT,@TIPO_MEDICAMENTO AS INTEGER;"
        sSql += " SET @BUSCAR = '" + txtBuscar.Text + "';"
        sSql += " SET @CONTROLADO = 0;"
        sSql += " SET @TIPO_MEDICAMENTO = -1;"

        sSql += " SELECT RIGHT('000000' + CAST(ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL "
        sSql += " AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + M.CONCENTRACION ELSE '' END + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN ' (' + M.MARCA + ')' ELSE '' END  AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, MARCA, TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO, M.COMPOSICION, M.MINIMO,ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO  FROM MEDICAMENTO M"
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " WHERE ((M.NOMBRE LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (L.DESCRIPCION LIKE '%' + @BUSCAR + '%') OR (TM.DESCRIPCION  LIKE '%' + @BUSCAR + '%') OR (M.MARCA LIKE '%' + @BUSCAR + '%')) "
        sSql += " AND (M.CONTROLADO = CASE WHEN @CONTROLADO = 1 THEN @CONTROLADO ELSE M.CONTROLADO END)"
        sSql += " AND (M.ID_TIPO_MEDICAMENTO = CASE WHEN @TIPO_MEDICAMENTO = -1 THEN M.ID_TIPO_MEDICAMENTO ELSE @TIPO_MEDICAMENTO END)"
        sSql += " ORDER BY MEDICAMENTO, LABORATORIO,MARCA"
        dtSource = New datos().ejecutarConsulta(sSql)

    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        If Trim(txtBuscar.Text).Length > 0 Then
            mostrarMedicamentos()
            dgvGrillaMedicamento.DataSource = dtSource
        End If
    End Sub

    Private Sub dgvGrillaMedicamento_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrillaMedicamento.CellDoubleClick
        Dim idMedicamento As Integer = CType(dgvGrillaMedicamento.Rows(e.RowIndex).Cells(3), DataGridViewTextBoxCell).Value
        Using fInventario = New form_add_med_inventario_inicial(idMedicamento, -1)
            If DialogResult.OK = fInventario.ShowDialog() Then
                dgvGrillaCompra.DataSource = dtSourceCompra
            End If
        End Using
    End Sub

    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If dtSourceCompra.Rows.Count = 0 Then
            mensaje = "No tiene Productos Seleccionado."
            ErrorProvider1.SetError(txtBuscar, mensaje)
        Else
            ErrorProvider1.SetError(dgvGrillaCompra, "")
        End If
        Return valido
    End Function
    Protected Sub nuevoKardex(ByVal nroLote As String, ByVal f_vencimiento As String, ByVal idMedicamento As Integer, ByVal cantidad As Integer)
        Dim idKardex As Integer
        idKardex = existeKardex(nroLote, idMedicamento)
        If idKardex = -1 Then
            Dim tbl = New datos
            tbl.tabla = "KARDEX"
            tbl.campoID = "ID_KARDEX"
            tbl.agregarCampoValor("NRO_LOTE", nroLote)
            tbl.agregarCampoValor("F_VENCIMIENTO", f_vencimiento)
            tbl.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
            tbl.agregarCampoValor("CERRADO", 0)
            tbl.agregarCampoValor("ID_COMPROBANTE_DETALLE", "null")
            tbl.insertar()
            idKardex = tbl.valorID
        End If
        movimientoKardex(idKardex, cantidad)
    End Sub
    Protected Sub movimientoKardex(ByVal idKardex As Integer, ByVal cantidad As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 4)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Try
                Using scope As New TransactionScope()
                    Dim idCompra = "null"
                    
                    For Each item In dtSourceCompra.Rows
                        tInventario.ID_MEDICAMENTO = item("ID_MEDICAMENTO")
                        tInventario.ID_COMPRA = idCompra
                        tInventario.ID_VENTA = "null"
                        tInventario.CANTIDAD = item("CANTIDAD")
                        tInventario.CONTROL_FECHA = item("CONTROL_FECHA")
                        Dim fVencimiento = "null"
                        If CBool(item("CONTROL_FECHA")) Then
                            tInventario.F_VENCIMIENTO = item("F_VENCIMIENTO")
                            fVencimiento = item("F_VENCIMIENTO")
                        End If
                            tInventario.PRECIO = item("PRECIO")
                        If Not IsDBNull(tInventario.LOTE) Then
                            tInventario.LOTE = item("LOTE")
                        End If
                        tInventario.ID_USUARIO = form_sesion.cusuario.ID_USUARIO                        
                        tInventario.insertar()
                        nuevoKardex(item("LOTE"), fVencimiento, item("ID_MEDICAMENTO"), item("CANTIDAD"))
                    Next
                    scope.Complete()
                    If Me.Modal Then
                        Me.DialogResult = Windows.Forms.DialogResult.OK
                    Else
                        Dim form = New form_inventario()
                        Dim mod_conf As modulo = New modulo()
                        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
                    End If
                End Using
            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                Dim msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
            
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Using fMedicamento = New form_gestionMedicamento(True)
            If DialogResult.OK = fMedicamento.ShowDialog() Then
                dtSource = tMedicamentoCompra.mostrarMedicamento(txtBuscar.Text)
                dgvGrillaMedicamento.DataSource = dtSource
            End If
        End Using
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If (Asc(e.KeyChar) = 13) Then
            e.Handled = True
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnSiguiente_Click(sender As Object, e As EventArgs) Handles btnSiguiente.Click
        Dim idMedicamento As Integer = dgvGrillaMedicamento.SelectedCells(3).Value
        Using fInventario = New form_add_med_inventario_inicial(idMedicamento, -1)
            If DialogResult.OK = fInventario.ShowDialog() Then
                dgvGrillaCompra.DataSource = dtSourceCompra
            End If
        End Using
    End Sub

    Private Sub btnAnterior_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click, btnAnterior.Click
        eliminar(dgvGrillaCompra.SelectedCells(7).Value)
        actualizarGrilla()
    End Sub

    Private Sub dgvGrillaCompra_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrillaCompra.CellDoubleClick
        Dim idMedicamento As Integer = CType(dgvGrillaCompra.Rows(e.RowIndex).Cells(5), DataGridViewTextBoxCell).Value
        Dim nroItem As Integer = CType(dgvGrillaCompra.Rows(e.RowIndex).Cells(7), DataGridViewTextBoxCell).Value
        Using fInventario = New form_add_med_inventario_inicial(idMedicamento, nroItem)
            If DialogResult.OK = fInventario.ShowDialog() Then
                dgvGrillaCompra.DataSource = dtSourceCompra
            End If
        End Using
    End Sub
    Private Function existeKardex(ByVal nroLote As String, ByVal idMedicamento As Integer) As Integer
        Dim sSql As String = ""
        sSql += " DECLARE @LOTE AS NVARCHAR(15) , @MEDICAMENTO AS INTEGER;"
        sSql += " SET @LOTE = '" + nroLote + "';"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT ID_KARDEX FROM KARDEX"
        sSql += " WHERE NRO_LOTE = @LOTE AND ID_MEDICAMENTO = @MEDICAMENTO  "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count = 0 Then
            Return -1
        Else
            Return tbl.Rows(0)("ID_KARDEX")
        End If
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If dgvGrillaCompra.RowCount > 0 Then
            Dim formCant = New solicitud_cantidad(-1, dgvGrillaCompra.SelectedCells(0).Value.ToString, dgvGrillaCompra.SelectedCells(5).Value.ToString, dgvGrillaCompra.SelectedCells(3).Value.ToString)
            Using formCant
                formCant.ShowDialog()
            End Using
        Else
            mod_conf.mensajeError("NO EXISTE CODIGO DE BARRA SELECCIONADO", "Error, Debe seleccionar por lo menos 1 Medicamento")
        End If
    End Sub
End Class