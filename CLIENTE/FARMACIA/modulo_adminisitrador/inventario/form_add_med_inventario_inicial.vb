﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Imports System.Transactions
Public Class form_add_med_inventario_inicial
    Private _medicamento As eMedicamento
    Private _nroItem As Integer
    Public Shared idMedicamento As Integer
    Private idInventarioInicial As Integer
    Private cantidadAnterior As Integer
    Private idKardex As Integer
    Private InString As String
    Private PistolaEnviando As Boolean = False
    Private mod_conf As New modulo()

    Public Sub New(ByVal _idMedicamento As Integer, Optional ByVal nroItem As Integer = -1, Optional _idInventario As Integer = -1)
        InitializeComponent()
        iniciarCombo()
        _medicamento = New eMedicamento(_idMedicamento)
        idMedicamento = _idMedicamento
        cargarDatos(nroItem)
        _nroItem = nroItem
        idInventarioInicial = _idInventario
        If idInventarioInicial <> -1 Then
            cargarDatosInventario()
        End If
    End Sub
    
    Protected Sub iniciarCombo()
        iniciarComboFormaFarmaceutica()
        iniciarComboLaboratorio()
    End Sub

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_TIPO_MEDICAMENTO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM TIPO_MEDICAMENTO WHERE ACTIVO = 1 UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Forma Farmaceutica]' ORDER BY DESCRIPCION"

        cmbTipoMedicamento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_TIPO_MEDICAMENTO"
    End Sub


    Private Sub form_compra_inventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCantidad.Focus()
    End Sub
    Private Sub cargarDatosInventario()
        Dim sSql As String = ""
        sSql += " SELECT CANTIDAD,LOTE, F_VENCIMIENTO FROM INVENTARIO"
        sSql += " WHERE ID_INVENTARIO = " + idInventarioInicial.ToString
        Dim tbl = New datos().ejecutarConsulta(sSql)
        txtCantidad.Text = tbl.Rows(0)(0)
        cantidadAnterior = tbl.Rows(0)(0)
        txtLote.Text = tbl.Rows(0)(1)
        If Not IsDBNull(tbl.Rows(0)(2)) Then
            chkControlFecha.Checked = True
            txtFechaVencimiento.Text = tbl.Rows(0)(2)
            idKardex = obtenerIdKardex(idMedicamento, tbl.Rows(0)(1))
        End If
    End Sub
    Private Sub cargarDatos(ByVal nroItem As Integer)
        txtMedicamento.Text = _medicamento.NOMBRE
        cmbTipoMedicamento.SelectedValue = _medicamento.ID_TIPO_MEDICAMENTO
        txtPrecio.Text = _medicamento.PRECIO_UNITARIO
        cmbLaboratorio.SelectedValue = _medicamento.ID_LABORATORIO
        txtMarca.Text = _medicamento.MARCA
        txtComposicion.Text = _medicamento.COMPOSICION
        txtConcentracion.Text = _medicamento.CONCENTRACION
        chkControlado.Checked = _medicamento.CONTROLADO
        txtPrecioPaquete.Text = _medicamento.PRECIO_PAQUETE
        txtCantidadPaquete.Text = _medicamento.CANTIDAD_PAQUETE
        If nroItem <> -1 Then
            Dim row As DataRow    
            row = (From tbl In form_inventario_inicial.dtSourceCompra Where tbl!NRO_ITEM = nroItem)(0)
            txtCantidad.Text = row("CANTIDAD")
            chkControlFecha.Checked = row("CONTROL_FECHA")
            txtLote.Text = row("LOTE")
            If chkControlFecha.Checked Then
                txtFechaVencimiento.Text = row("F_VENCIMIENTO")
                txtFechaVencimiento.Enabled = True
            End If
        End If
        cargarCodigoBarra()
    End Sub
    Private Sub cargarCodigoBarra()
        Dim sSql As String = ""
        sSql += " SELECT CODIGO_BARRA FROM CODIGO_BARRA "
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento.ToString
        sSql += " ORDER BY ID_CODIGO_BARRA DESC"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count > 0 Then
            txtCodigoBarra.Text = tblConsulta.Rows(0)(0)
        End If
    End Sub
    Private Sub insertarCodigoBarra()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " IF NOT EXISTS(SELECT *  FROM  CODIGO_BARRA  WHERE ID_MEDICAMENTO = @MEDICAMENTO AND CODIGO_BARRA = @COD_BARRA )  "
        sSql += " BEGIN "
        sSql += " INSERT CODIGO_BARRA VALUES (@COD_BARRA,@MEDICAMENTO)"
        sSql += " END"
        Dim tbl = New datos()
        tbl.ejecutarSQL(sSql)
    End Sub
    Private Function existeCodBarraMedicamento() As String
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " SELECT TOP 1 M.NOMBRE FROM CODIGO_BARRA CB"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = CB.ID_MEDICAMENTO "
        sSql += " WHERE M.ID_MEDICAMENTO <> @MEDICAMENTO AND CB.CODIGO_BARRA = @COD_BARRA "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count = 0 Then
            Return ""
        Else
            Return tbl.Rows(0)(0)
        End If
    End Function

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") = -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidad_Click(sender As Object, e As EventArgs) Handles txtCantidad.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub chkActualizaPrecio_CheckedChanged(sender As Object, e As EventArgs) Handles chkPrecioUnidad.CheckedChanged
        txtPrecio.Enabled = chkPrecioUnidad.Checked
    End Sub

    Private Sub chkControlFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlFecha.CheckedChanged
        txtFechaVencimiento.Enabled = chkControlFecha.Checked
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPrecio_Click(sender As Object, e As EventArgs) Handles txtPrecio.Click
        If chkPrecioUnidad.Checked Then
            Dim txt As TextBox = CType(sender, TextBox)
            If (Not String.IsNullOrEmpty(txt.Text)) Then
                txt.SelectionStart = 0
                txt.SelectionLength = txt.Text.Length
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim MedCodBarra = existeCodBarraMedicamento()
            If MedCodBarra = "" Then
                If idInventarioInicial = -1 Then
                    If Not existeMedicamento(_medicamento.ID_MEDICAMENTO, txtFechaVencimiento.Text, chkControlFecha.Checked) Then
                        actualizarMedicamento()
                        If _nroItem <> -1 Then
                            modificar(_nroItem)
                        Else
                            guardar()
                        End If

                        Me.DialogResult = Windows.Forms.DialogResult.OK
                    Else
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                        title = "Error"
                        Dim msg As String
                        If chkControlFecha.Checked Then
                            msg = "La fecha de vencimiento del medicamento seleccionado ya existe en la compra."
                        Else
                            msg = "El medicamento seleccionado ya existe en la compra."
                        End If

                        response = MsgBox(msg, style, title)
                    End If
                Else
                    Try
                        Using scope As New TransactionScope()
                            actualizarMedicamento()
                            actualizarInventario()
                            scope.Complete()
                            Me.DialogResult = Windows.Forms.DialogResult.OK
                        End Using
                    Catch ex As Exception
                        Dim title As String
                        Dim style As MsgBoxStyle
                        Dim response As MsgBoxResult
                        style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                        title = "Error Inconsistencia en la base de datos"
                        Dim msg = ex.Message.ToString
                        response = MsgBox(msg, style, title)
                    End Try


                End If
            Else
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error"
                Dim msg As String
                msg = "El codigo de barra que intenta agregar, esta registrado con el medicamento: " + MedCodBarra + ", revise e intente nuevamente."
                response = MsgBox(msg, style, title)
            End If
        End If
    End Sub
    Protected Sub actualizarMedicamento()
        Dim tblConsulta = New datos()
        If chkMedicamento.Checked Or chkPrecioUnidad.Checked Or chkPrecioPaquete.Checked Then
            tblConsulta.tabla = "MEDICAMENTO"
            tblConsulta.valorID = _medicamento.ID_MEDICAMENTO
            tblConsulta.campoID = "ID_MEDICAMENTO"
            If chkMedicamento.Checked Then
                tblConsulta.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                tblConsulta.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
                tblConsulta.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                tblConsulta.agregarCampoValor("MARCA", txtMarca.Text)
                tblConsulta.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                tblConsulta.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                tblConsulta.agregarCampoValor("CONTROLADO", chkControlado.Checked)
            End If
            tblConsulta.agregarCampoValor("PRECIO_UNITARIO", txtPrecio.Text)
            If CDec(txtPrecioPaquete.Text) = 0 Then
                tblConsulta.agregarCampoValor("PRECIO_PAQUETE", "null")
            Else
                tblConsulta.agregarCampoValor("PRECIO_PAQUETE", txtPrecioPaquete.Text)
            End If
            If CDec(txtCantidadPaquete.Text) <= 1 Then
                tblConsulta.agregarCampoValor("CANTIDAD_PAQUETE", "null")
            Else
                tblConsulta.agregarCampoValor("CANTIDAD_PAQUETE", txtCantidadPaquete.Text)
            End If
            tblConsulta.modificar()
        End If
        If Trim(txtCodigoBarra.Text).Length > 0 Then
            insertarCodigoBarra()
        End If
    End Sub
    Private Function obtenerIdKardex(ByVal idMedicamento As String, nroLote As String) As Integer
        Dim sSql As String = ""
        sSql += " SELECT ID_KARDEX  FROM KARDEX "
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento + " AND NRO_LOTE = '" + nroLote + "'"
        Return New datos().ejecutarConsulta(sSql).Rows(0)(0)
    End Function
    Protected Sub actualizarInventario()
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "INVENTARIO"
        tblConsulta.valorID = idInventarioInicial
        tblConsulta.campoID = "ID_INVENTARIO"
        tblConsulta.agregarCampoValor("PRECIO_UNITARIO", txtPrecio.Text)
        tblConsulta.agregarCampoValor("CANTIDAD", txtCantidad.Text)
        tblConsulta.agregarCampoValor("LOTE", txtLote.Text)
        If chkControlFecha.Checked Then
            tblConsulta.agregarCampoValor("F_VENCIMIENTO", txtFechaVencimiento.Text)
        End If
        tblConsulta.modificar()
        actualizarKardex()
        If CInt(txtCantidad.Text) <> cantidadAnterior Then
            actualizarMovimientoKardex(cantidadAnterior * (-1))
            actualizarMovimientoKardex(txtCantidad.Text)
        End If
    End Sub
    Protected Sub actualizarKardex()
        Dim tblConsulta = New datos()
        tblConsulta.tabla = "KARDEX"
        tblConsulta.valorID = idKardex
        tblConsulta.campoID = "ID_KARDEX"
        tblConsulta.agregarCampoValor("NRO_LOTE", txtLote.Text)
        If chkControlFecha.Checked Then
            tblConsulta.agregarCampoValor("F_VENCIMIENTO", txtFechaVencimiento.Text)
        End If
        tblConsulta.modificar()
    End Sub
    Protected Sub actualizarMovimientoKardex(ByVal cantidad As Integer)
        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "sql"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub
    Private Sub guardar()
        Dim foundRows() As DataRow
        foundRows = form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")
        Dim dr As DataRow
        dr = form_inventario_inicial.dtSourceCompra.NewRow
        If foundRows.Length = 0 Then
            dr("NRO_ITEM") = 1
        Else
            dr("NRO_ITEM") = CInt(form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
        End If
        Dim nombreMedicamento As String = ""
        nombreMedicamento += txtMedicamento.Text
        If Trim(txtConcentracion.Text).Length > 0 Then
            nombreMedicamento += " x " + txtConcentracion.Text
        End If
        nombreMedicamento += " (" + cmbTipoMedicamento.Text + ")"
        dr("NOMBRE") = nombreMedicamento
        dr("LABORATORIO") = cmbLaboratorio.Text
        dr("CANTIDAD") = txtCantidad.Text
        dr("PRECIO") = txtPrecio.Text
        dr("ID_MEDICAMENTO") = _medicamento.ID_MEDICAMENTO
        dr("CONTROL_FECHA") = chkControlFecha.Checked
        If chkControlFecha.Checked Then
            dr("F_VENCIMIENTO") = txtFechaVencimiento.Text
        End If
        dr("LOTE") = txtLote.Text
        form_inventario_inicial.dtSourceCompra.Rows.Add(dr)
        form_inventario_inicial.dtSourceCompra.AcceptChanges()
    End Sub
    Private Function existeMedicamento(ByVal idMedicamento As Integer, ByVal f_medicamento As String, ByVal control_fecha As Boolean)
        Dim foundRows() As DataRow
        If control_fecha Then
            foundRows = form_inventario_inicial.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND F_VENCIMIENTO = '" & f_medicamento & "'AND NRO_ITEM <> " & _nroItem)
        Else
            foundRows = form_inventario_inicial.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & "AND NRO_ITEM <> " & _nroItem)
        End If
        Return foundRows.Count > 0
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub modificar(ByVal nro_item As Integer)
        Dim row As DataRow
            row = (From tbl In form_inventario_inicial.dtSourceCompra Where tbl!NRO_ITEM = nro_item)(0)
        Dim nombreMedicamento As String = ""
        nombreMedicamento += txtMedicamento.Text
        If Trim(txtConcentracion.Text).Length > 0 Then
            nombreMedicamento += " x " + txtConcentracion.Text
        End If
        nombreMedicamento += " (" + cmbTipoMedicamento.Text + ")"
        row("NOMBRE") = nombreMedicamento
        row("LABORATORIO") = cmbLaboratorio.Text
        row("CANTIDAD") = txtCantidad.Text
        row("PRECIO") = txtPrecio.Text
        row("ID_MEDICAMENTO") = _medicamento.ID_MEDICAMENTO
        row("CONTROL_FECHA") = chkControlFecha.Checked
        If chkControlFecha.Checked Then
            row("F_VENCIMIENTO") = txtFechaVencimiento.Text
        End If
        row("LOTE") = txtLote.Text
            form_inventario_inicial.dtSourceCompra.AcceptChanges()
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtCantidad.Text).Length > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "Complete el campo Cantidad."
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If

        If CInt(txtCantidad.Text) > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "La Cantidad no puede ser 0"
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If
        If txtLote.Text.Length > 0 Then
            ErrorProvider1.SetError(txtLote, "")
        Else
            Select Case mod_conf.mensajeConfirmacion("Lote", "El medicamento no tiene un Lote Asignado, desea que el sistema agregue un Numero de Lote Automatico")
                Case MsgBoxResult.Yes
                    txtLote.Text = "L" + _medicamento.ID_MEDICAMENTO.ToString
                Case MsgBoxResult.No
                    mensaje = "Debe Ingresar un lote Valido"
                    ErrorProvider1.SetError(txtLote, mensaje)
                    Return False
            End Select
        End If
        Return valido
    End Function

   

    Private Sub chkMedicamento_CheckedChanged(sender As Object, e As EventArgs) Handles chkMedicamento.CheckedChanged
        txtMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbTipoMedicamento.Enabled = CType(sender, CheckBox).Checked
        cmbLaboratorio.Enabled = CType(sender, CheckBox).Checked
        txtMarca.Enabled = CType(sender, CheckBox).Checked
        txtComposicion.Enabled = CType(sender, CheckBox).Checked
        txtConcentracion.Enabled = CType(sender, CheckBox).Checked
        chkControlado.Enabled = CType(sender, CheckBox).Checked
        txtCodigoBarra.Enabled = CType(sender, CheckBox).Checked
    End Sub

    Private Sub form_compra_inventario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                idMedicamento = existeCodigoBarraBD(InString)

                If idMedicamento <> -1 Then
                    _medicamento = New eMedicamento(idMedicamento)
                    cargarDatos(-1)
                Else
                    Dim mens = New modulo()
                    mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya existe en la lista de codigos del Medicamento o esta asignado a otro medicamento.")
                End If

            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub
    Private Function existeCodigoBarraBD(ByVal codBarra As String) As Integer
        Dim sSql As String = ""
        sSql += "SELECT DISTINCT M.ID_MEDICAMENTO FROM MEDICAMENTO M "
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE CB.CODIGO_BARRA = '" + codBarra + "'"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If tblConsulta.Rows.Count = 0 Then
            Return -1
        Else
            Return tblConsulta.Rows(0)(0)
        End If
    End Function
    Private Sub chkPrecioPaquete_CheckedChanged(sender As Object, e As EventArgs) Handles chkPrecioPaquete.CheckedChanged
        txtPrecioPaquete.Enabled = chkPrecioPaquete.Checked
    End Sub

End Class