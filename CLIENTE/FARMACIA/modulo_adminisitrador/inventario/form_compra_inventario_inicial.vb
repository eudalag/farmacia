﻿Imports CAPA_NEGOCIO
Public Class form_compra_inventario_inicial
    Private _medicamento As eMedicamento
    Private _nroItem As Integer
    Private _tipo As Integer
    Public Sub New(ByVal id As Integer, Optional ByVal nroItem As Integer = -1, Optional ByVal tipo As Integer = 1)
        InitializeComponent()
        _medicamento = New eMedicamento(id)
        cargarDatos(nroItem)
        _nroItem = nroItem
        _tipo = tipo
    End Sub


    Private Sub form_compra_inventario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCantidad.Focus()
    End Sub
    Private Sub cargarDatos(ByVal nroItem As Integer)
        txtMedicamento.Text = _medicamento.NOMBRE
        txtTipo.Text = _medicamento.TIPO_MEDICAMENTO.DESCRIPCION
        txtPrecio.Text = _medicamento.PRECIO_UNITARIO
        If nroItem <> -1 Then
            Dim row As DataRow
            'row = (From tbl In form_gestionCompra.dtSourceCompra Where tbl!NRO_ITEM = nroItem)(0)
            txtCantidad.Text = row("CANTIDAD")
            chkControlFecha.Checked = row("CONTROL_FECHA")
            If chkControlFecha.Checked Then
                txtFechaVencimiento.Text = row("F_VENCIMIENTO")
                txtFechaVencimiento.Enabled = True
            End If
        End If
    End Sub

    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") = -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidad_Click(sender As Object, e As EventArgs) Handles txtCantidad.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub chkActualizaPrecio_CheckedChanged(sender As Object, e As EventArgs) Handles chkActualizaPrecio.CheckedChanged
        txtPrecio.Enabled = chkActualizaPrecio.Checked
    End Sub

    Private Sub chkControlFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlFecha.CheckedChanged
        txtFechaVencimiento.Enabled = chkControlFecha.Checked
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtPrecio_Click(sender As Object, e As EventArgs) Handles txtPrecio.Click
        If chkActualizaPrecio.Checked Then
            Dim txt As TextBox = CType(sender, TextBox)
            If (Not String.IsNullOrEmpty(txt.Text)) Then
                txt.SelectionStart = 0
                txt.SelectionLength = txt.Text.Length
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            If Not existeMedicamento(_medicamento.ID_MEDICAMENTO, txtFechaVencimiento.Text, chkControlFecha.Checked) Then
                If _nroItem <> -1 Then
                    modificar(_nroItem)
                Else
                    guardar()
                End If

                If chkActualizaPrecio.Checked Then
                    _medicamento.PRECIO_UNITARIO = txtPrecio.Text
                    _medicamento.modificar()
                End If
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error"
                Dim msg As String
                If chkControlFecha.Checked Then
                    msg = "La fecha de vencimiento del medicamento seleccionado ya existe en la compra."
                Else
                    msg = "El medicamento seleccionado ya existe en la compra."
                End If

                response = MsgBox(msg, style, title)
            End If
        End If
    End Sub

    Private Sub guardar()
        Dim foundRows() As DataRow
        If _tipo = 1 Then
            'foundRows = form_gestionCompra.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")
        Else
            foundRows = form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")
        End If

        Dim dr As DataRow
        If foundRows.Length = 0 Then
            If _tipo = 1 Then
                'dr = form_gestionCompra.dtSourceCompra.NewRow
            Else
                dr = form_inventario_inicial.dtSourceCompra.NewRow
            End If

            dr("NRO_ITEM") = 1
            dr("NOMBRE") = txtMedicamento.Text + "(" + txtTipo.Text + ")"
            If chkControlFecha.Checked Then
                dr("F_VENCIMIENTO") = txtFechaVencimiento.Text
            End If
            dr("CANTIDAD") = txtCantidad.Text
            dr("ID_MEDICAMENTO") = _medicamento.ID_MEDICAMENTO
            dr("CONTROL_FECHA") = chkControlFecha.Checked
        Else
            If _tipo = 1 Then
                'dr = form_gestionCompra.dtSourceCompra.NewRow
                'dr("NRO_ITEM") = CInt(form_gestionCompra.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
            Else
                dr = form_inventario_inicial.dtSourceCompra.NewRow
                dr("NRO_ITEM") = CInt(form_inventario_inicial.dtSourceCompra.Select("NRO_ITEM = MAX(NRO_ITEM)")(0)("NRO_ITEM")) + 1
                dr("PRECIO_UNITARIO") = _medicamento.PRECIO_UNITARIO
            End If


            dr("NOMBRE") = txtMedicamento.Text + "(" + txtTipo.Text + ")"
            If chkControlFecha.Checked Then
                dr("F_VENCIMIENTO") = txtFechaVencimiento.Text
            End If
            dr("CANTIDAD") = txtCantidad.Text
            dr("ID_MEDICAMENTO") = _medicamento.ID_MEDICAMENTO
            dr("CONTROL_FECHA") = chkControlFecha.Checked
        End If
        If _tipo = 1 Then
            'form_gestionCompra.dtSourceCompra.Rows.Add(dr)
            'form_gestionCompra.dtSourceCompra.AcceptChanges()
        Else
            form_inventario_inicial.dtSourceCompra.Rows.Add(dr)
            form_inventario_inicial.dtSourceCompra.AcceptChanges()
        End If

    End Sub
    Private Function existeMedicamento(ByVal idMedicamento As Integer, ByVal f_medicamento As String, ByVal control_fecha As Boolean)
        Dim foundRows() As DataRow
        If control_fecha Then
            If _tipo = 1 Then
                'foundRows = form_gestionCompra.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND F_VENCIMIENTO = '" & f_medicamento & "'AND NRO_ITEM <> " & _nroItem)
            Else
                foundRows = form_inventario_inicial.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & " AND F_VENCIMIENTO = '" & f_medicamento & "'AND NRO_ITEM <> " & _nroItem)
            End If
        Else
            If _tipo = 1 Then
                'foundRows = form_gestionCompra.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & "AND NRO_ITEM <> " & _nroItem)
            Else
                foundRows = form_inventario_inicial.dtSourceCompra.Select("ID_MEDICAMENTO = " & idMedicamento.ToString & "AND NRO_ITEM <> " & _nroItem)
            End If
        End If
        Return foundRows.Count > 0
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub modificar(ByVal nro_item As Integer)
        Dim row As DataRow
        If _tipo = 1 Then
            'row = (From tbl In form_gestionCompra.dtSourceCompra Where tbl!NRO_ITEM = nro_item)(0)
        Else
            row = (From tbl In form_inventario_inicial.dtSourceCompra Where tbl!NRO_ITEM = nro_item)(0)
        End If

        If chkControlFecha.Checked Then
            row("F_VENCIMIENTO") = txtFechaVencimiento.Text
        End If
        row("CANTIDAD") = txtCantidad.Text
        row("CONTROL_FECHA") = chkControlFecha.Checked
        If _tipo = 1 Then
            'form_gestionCompra.dtSourceCompra.AcceptChanges()
        Else
            form_inventario_inicial.dtSourceCompra.AcceptChanges()
        End If

    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtCantidad.Text).Length > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "Complete el campo Cantidad."
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If

        If CInt(txtCantidad.Text) > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "La Cantidad no puede ser 0"
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If
        Return valido
    End Function
End Class