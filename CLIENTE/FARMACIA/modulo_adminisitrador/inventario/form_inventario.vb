﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Imports System.Transactions

Public Class form_inventario
    Private mod_conf As modulo = New modulo()
    Public Shared dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15
    Public Shared codigoBarra As String
    Private InString As String
    Private PistolaEnviando As Boolean = False
    Function crearTabla() As DataTable
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "NOMBRE"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "FORM_FARM"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "CODIGO_BARRA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "CANTIDAD"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ID_MEDICAMENTO"
        tblItems.Columns.Add(columna)

        tblItems.AcceptChanges()
        Return tblItems
    End Function
    Public Shared Sub nuevoItem(ByRef tblItems As DataTable, ByVal nombre As String, ByVal form_farm As String, ByVal codigoBarra As String, ByVal cantidad As Integer, ByVal idMedicamento As Integer)
        Dim dr As DataRow
        dr = tblItems.NewRow
        dr("NOMBRE") = nombre
        dr("FORM_FARM") = form_farm
        dr("CODIGO_BARRA") = codigoBarra
        dr("CANTIDAD") = cantidad
        dr("ID_MEDICAMENTO") = idMedicamento
        tblItems.Rows.Add(dr)
        tblItems.AcceptChanges()
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100
            .Height = 370
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 9

            .Columns(0).Name = "nombre"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 300
            .Columns(0).ReadOnly = True

            .Columns(1).Name = "laboratorio"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 200
            .Columns(1).ReadOnly = True

            .Columns(2).Name = "tipo_medicamento"
            .Columns(2).HeaderText = "Forma Farmaceutica"
            .Columns(2).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
            .Columns(2).Width = 200
            .Columns(2).ReadOnly = True

            .Columns(3).Name = "lote"
            .Columns(3).HeaderText = "Lote"
            .Columns(3).DataPropertyName = "LOTE"
            .Columns(3).Width = 120
            .Columns(3).ReadOnly = True

            .Columns(4).Name = "vencimiento"
            .Columns(4).HeaderText = "F. Vencimiento"
            .Columns(4).DataPropertyName = "F_VENCIMIENTO"
            .Columns(4).Width = 120
            .Columns(4).ReadOnly = True

            .Columns(5).Name = "cantidad"
            .Columns(5).HeaderText = "Cantidad"
            .Columns(5).DataPropertyName = "CANTIDAD"
            .Columns(5).Width = 110
            .Columns(5).ReadOnly = True

            .Columns(6).DataPropertyName = "ID_INVENTARIO"
            .Columns(6).Visible = False

            .Columns(7).Name = "ID_MEDICAMENTO"
            .Columns(7).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(7).Visible = False

            Dim checkBoxColumn As New DataGridViewCheckBoxColumn()
            checkBoxColumn.Name = "IMPRIMIR"
            checkBoxColumn.HeaderText = "Impr."
            checkBoxColumn.Width = 50
            checkBoxColumn.ReadOnly = False
            .Columns.Insert(8, checkBoxColumn)

        End With
        LoadPage()
    End Sub


    Private Function cargarDatos() As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @TIPO AS INTEGER, @BUSCAR AS NVARCHAR(100);"
        sSql += " SET @TIPO =  " + cmbTipoMedicamento.SelectedValue.ToString + " ;"
        sSql += " SET @BUSCAR = '" + txtBuscar.Text + "';"

        sSql += " SELECT I.ID_INVENTARIO, M.ID_MEDICAMENTO,M.NOMBRE + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(LTRIM(M.CONCENTRACION)) > 0 THEN ' x ' + M.CONCENTRACION ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN ' (' + M.MARCA + ')' ELSE '' END AS MEDICAMENTO,"
        sSql += " TM.DESCRIPCION AS DESC_TIPO_MEDICAMENTO,L.DESCRIPCION AS LABORATORIO , I.CANTIDAD, I.LOTE, ISNULL(CONVERT(NVARCHAR(10),I.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO,"
        sSql += " CONVERT(NVARCHAR(10),I.FECHA,103) AS F_REGISTRO"
        sSql += " FROM INVENTARIO I "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " WHERE (I.ID_COMPRA IS NULL AND I.ID_VENTA IS NULL) AND M.ID_TIPO_MEDICAMENTO = CASE WHEN @TIPO = -1 THEN M.ID_TIPO_MEDICAMENTO ELSE @TIPO END"
        sSql += " AND "
        sSql += " ((M.NOMBRE LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (TM.DESCRIPCION  LIKE '%' + @BUSCAR + '%') OR (TM.DESCRIPCION  LIKE '%' + @BUSCAR + '%'))"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub form_inventario_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarCombo()
        dtSource = cargarDatos()
        inicializarParametros()
        txtBuscar.Focus()
    End Sub
    Private Sub iniciarCombo()
        Dim tTipoMedicamento As eTipoMedicamento = New eTipoMedicamento()
        cmbTipoMedicamento.DataSource = tTipoMedicamento.mostrarComboTipoMedicamento()
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_TIPO_MEDICAMENTO"
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click        

        dtSource = cargarDatos()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Using form_inventario_inicial = New form_inventario_inicial()
            If DialogResult.OK = form_inventario_inicial.ShowDialog() Then
                'Dim tinventario = New eInventario()
                'dtSource = tinventario.mostrarInventarioTotal(cmbTipoMedicamento.SelectedValue, txtBuscar.Text)
                'dgvGrilla.DataSource = dtSource
                'LoadPage()
                btnBuscar_Click(Nothing, Nothing)
            End If
        End Using
    End Sub

    
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Using form_ajustes = New (True)
        '    If DialogResult.OK = form_inventario_inicial.ShowDialog() Then
        '        Dim tinventario = New eInventario()
        '        'dtSource = tinventario.mostrarInventarioTotal(cmbTipoMedicamento.SelectedValue, txtBuscar.Text)
        '        'dgvGrilla.DataSource = dtSource
        '        'LoadPage()
        '        btnBuscar_Click(Nothing, Nothing)
        '    End If
        'End Using

    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idMedicamento As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(7), DataGridViewTextBoxCell).Value
        Dim idInventario = CType(dgvGrilla.Rows(e.RowIndex).Cells(6), DataGridViewTextBoxCell).Value

        Using fInventario = New form_add_med_inventario_inicial(idMedicamento, -1, idInventario)
            If DialogResult.OK = fInventario.ShowDialog() Then
                dgvGrilla.DataSource = cargarDatos()
            End If
        End Using
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
    
        Dim idInventario = dgvGrilla.SelectedCells(6).Value
        Dim idMedicamento As String = dgvGrilla.SelectedCells(7).Value
        Dim nroLote As String = dgvGrilla.SelectedCells(3).Value
        Dim idKardex = obtenerIdKardex(idMedicamento, nroLote)

        Select Case mod_conf.mensajeConfirmacion("Eliminar Medicamento", "Esta seguro que desea eliminar el medicamento: " + dgvGrilla.SelectedCells(0).Value.ToString + "?")
            Case MsgBoxResult.Yes
                Try
                    Using scope As New TransactionScope()
                        movimientoKardex(idKardex, CInt(dgvGrilla.SelectedCells(5).Value) * -1)
                        Dim tbl = New datos()
                        tbl.tabla = "INVENTARIO"
                        tbl.campoID = "ID_INVENTARIO"
                        tbl.valorID = idInventario
                        tbl.eliminar()
                        scope.Complete()
                    End Using
                Catch ex As Exception
                    Dim title As String
                    Dim style As MsgBoxStyle
                    Dim response As MsgBoxResult
                    style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                    title = "Error Inconsistencia en la base de datos"
                    Dim msg = ex.Message.ToString
                    response = MsgBox(msg, style, title)
                End Try
                dtSource = cargarDatos()
                Me.totalRegistro = dtSource.Rows.Count
                Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                Me.paginaActual = 1
                Me.regNro = 0
                LoadPage()
            Case MsgBoxResult.No

        End Select

        

    End Sub
    Protected Sub movimientoKardex(ByVal idKardex As Integer, ByVal cantidad As Integer)
        Dim tbl = New datos
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", 3)
        tbl.agregarCampoValor("CANTIDAD", cantidad)
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub
    Private Function obtenerIdKardex(ByVal idMedicamento As String, nroLote As String) As Integer
        Dim sSql As String = ""
        sSql += " SELECT ID_KARDEX  FROM KARDEX "
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento + " AND NRO_LOTE = '" + nroLote + "'"
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            Return tbl.Rows(0)(0)
        Else
            Return -1
        End If
    End Function

    Function obtenerCodigoBarra(ByVal Nombre As String, ByVal idMedicamento As Integer) As String
        Dim sSql As String = ""
        sSql += "SELECT CODIGO_BARRA FROM CODIGO_BARRA WHERE ID_MEDICAMENTO = " + idMedicamento.ToString
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 1 Then
            Dim ingreso = False
            codigoBarra = ""
            Do
                Using fMedicamento = New seleccionarCodigoBarra(Nombre, idMedicamento)
                    If DialogResult.OK = fMedicamento.ShowDialog() Then
                        ingreso = True
                    End If
                End Using
            Loop Until ingreso
            Return codigoBarra
        Else
            Return tbl.Rows(0)(0)
        End If
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim tblItems = crearTabla()
        For Each row As DataGridViewRow In dgvGrilla.Rows
            Dim isSelected As Boolean = Convert.ToBoolean(row.Cells("IMPRIMIR").Value)
            If isSelected Then
                nuevoItem(tblItems, row.Cells("nombre").Value, row.Cells("tipo_medicamento").Value, obtenerCodigoBarra(row.Cells("nombre").Value, row.Cells("ID_MEDICAMENTO").Value), row.Cells("cantidad").Value, row.Cells("ID_MEDICAMENTO").Value)
            End If
        Next
        If tblItems.Rows.Count > 0 Then

            Dim formCant = New listaCodigoBarra(tblItems)
            Using formCant
                formCant.ShowDialog()
            End Using
        Else
            mod_conf.mensajeError("NO EXISTE CODIGO DE BARRA SELECCIONADO", "Error, Debe seleccionar por lo menos 1 codigo de Barra")
        End If




    End Sub

    Private Sub form_inventario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                'dgvGrilla.SelectedCells(1).Value = InString
                Dim med As DataTable = mod_conf.obtenerMedicamento(InString, 2)
                txtBuscar.Text = med.Rows(0)("NOMBRE")
                cmbTipoMedicamento.SelectedValue = med.Rows(0)("ID_TIPO_MEDICAMENTO")
                btnBuscar_Click(Nothing, Nothing)
            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub
End Class