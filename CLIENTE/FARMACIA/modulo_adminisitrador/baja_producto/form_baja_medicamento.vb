﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Public Class form_baja_medicamento
    Private mod_conf As modulo = New modulo()
    Public Shared dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15
    Private Sub obtenerMedicamentos()
        'Dim sSql As String = ""
        'sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE;"
        'sSql += " SET @DESDE = CONVERT(DATE,'" + txtDesde.Text.ToString + "',103);"
        'sSql += " SET @HASTA = CONVERT(DATE,'" + txtHasta.Text.ToString + "',103);"
        'sSql += " SELECT M.ID_MEDICAMENTO,M.MEDICAMENTO,M.LABORATORIO,M.FORMA_FARMACEUTICA ,K.ID_KARDEX, NRO_LOTE, F_VENCIMIENTO, TOTAL_STOCK FROM KARDEX K "
        'sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = K.ID_MEDICAMENTO "
        'sSql += " INNER JOIN ("
        'sSql += " SELECT ID_KARDEX, SUM(CANTIDAD) AS TOTAL_STOCK FROM MOVIMIENTO_KARDEX MK "
        'sSql += " GROUP BY ID_KARDEX "
        'sSql += " HAVING SUM(CANTIDAD) > 0 ) MK ON MK.ID_KARDEX = K.ID_KARDEX "
        'sSql += " WHERE CONVERT(DATE,K.F_VENCIMIENTO,103) >= @DESDE AND CONVERT(DATE,K.F_VENCIMIENTO,103) <= @HASTA"
        'sSql += " ORDER BY F_VENCIMIENTO, MEDICAMENTO "

        dtSource = New generico().obtenerMedicamentosVencidos(txtDesde.Text, txtHasta.Text)
    End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1100
            .Height = 370
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 8

            .Columns(0).Name = "nombre"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "MEDICAMENTO"
            .Columns(0).Width = 360

            .Columns(1).Name = "laboratorio"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 200

            .Columns(2).Name = "tipo_medicamento"
            .Columns(2).HeaderText = "Tipo Medicamento"
            .Columns(2).DataPropertyName = "FORMA_FARMACEUTICA"
            .Columns(2).Width = 220

            .Columns(3).Name = "lote"
            .Columns(3).HeaderText = "Lote"
            .Columns(3).DataPropertyName = "NRO_LOTE"
            .Columns(3).Width = 120

            .Columns(4).Name = "vencimiento"
            .Columns(4).HeaderText = "Fecha Venc."
            .Columns(4).DataPropertyName = "F_VENCIMIENTO"
            .Columns(4).Width = 120
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(5).Name = "total"
            .Columns(5).HeaderText = "Stock"
            .Columns(5).DataPropertyName = "TOTAL_STOCK"
            .Columns(5).Width = 80
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns(6).Visible = False
            .Columns(6).Name = "id_medicamento"
            .Columns(6).DataPropertyName = "ID_MEDICAMENTO"

            .Columns(7).Visible = False
            .Columns(7).Name = "kardex"
            .Columns(7).DataPropertyName = "ID_KARDEX"
        End With
    End Sub
    Private Sub cargarTabla()
        obtenerMedicamentos()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub
   
    Function PrimerDiaDelMes(ByVal dtmFecha As Date) As Date
        Return DateSerial(Year(dtmFecha), Month(dtmFecha), 1)
    End Function




    Private Sub form_baja_medicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtDesde.Text = PrimerDiaDelMes(Now).ToString("dd/MM/yyyy")
        txtHasta.Text = Now.ToString("dd/MM/yyyy")
        obtenerMedicamentos()
        inicializarParametros()
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idMedicamento As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(6), DataGridViewTextBoxCell).Value
        Dim idKardex As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(7), DataGridViewTextBoxCell).Value
        Using baja = New form_gestion_baja(idMedicamento, idKardex)
            If DialogResult.OK = baja.ShowDialog() Then
                cargarTabla()
            End If
        End Using
    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        cargarTabla()
    End Sub
    Private Sub imprimir()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirVencimientos(e.Graphics, txtDesde.Text, txtHasta.Text)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        imprimir()
    End Sub
End Class