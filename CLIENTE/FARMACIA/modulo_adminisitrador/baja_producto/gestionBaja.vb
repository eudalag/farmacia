﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Imports System.Transactions
Public Class form_gestion_baja
    Private idMedicamento As Integer
    Private idKardex As Integer
    Public Sub New(ByVal _idMedicamento As Integer, ByVal _idKardex As Integer)
        InitializeComponent()
        idMedicamento = _idMedicamento
        idKardex = _idKardex
        cargarDatos()
    End Sub
    Private Sub cargarDatos()
        'txtMedicamento.Text = _medicamento.NOMBRE
        'txtTipo.Text = _medicamento.TIPO_MEDICAMENTO.DESCRIPCION
        'txtPrecio.Text = _medicamento.PRECIO_UNITARIO
        'txtLaboratorio.Text = _medicamento.LABORATORIO
        'If nroItem <> -1 Then
        '    Dim row As DataRow
        '    If _tipo = 1 Then
        '        row = (From tbl In form_gestionCompra.dtSourceCompra Where tbl!NRO_ITEM = nroItem)(0)
        '    Else
        '        row = (From tbl In form_inventario_inicial.dtSourceCompra Where tbl!NRO_ITEM = nroItem)(0)
        '    End If

        '    txtCantidad.Text = row("CANTIDAD")
        '    chkControlFecha.Checked = row("CONTROL_FECHA")
        '    If chkControlFecha.Checked Then
        '        txtFechaVencimiento.Text = row("F_VENCIMIENTO")
        '        txtFechaVencimiento.Enabled = True
        '    End If
        'End If
        Dim sSql As String = ""
        sSql += " DECLARE @ID_KARDEX AS INTEGER, @ID_MEDICAMENTO AS INTEGER"
        sSql += " SET @ID_KARDEX = " + idKardex.ToString + ";"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SELECT M.ID_MEDICAMENTO, UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END +"
        sSql += " CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " L.DESCRIPCION AS LABORATORIO, FORMA_FARMACEUTICA,K.TOTAL_STOCK,"
        sSql += " K.F_VENCIMIENTO,K.NRO_LOTE,k.ID_KARDEX FROM VW_MEDICAMENTO M "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN ("
        sSql += " SELECT k.ID_KARDEX ,K.ID_MEDICAMENTO , ISNULL(K.NRO_LOTE,'S/Lote') AS NRO_LOTE,ISNULL(CAST(K.F_VENCIMIENTO AS NVARCHAR(10)),'S/Venc.') AS F_VENCIMIENTO ,ISNULL(SUM(MK.CANTIDAD),0) AS TOTAL_STOCK FROM KARDEX K  "
        sSql += " LEFT JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE  ISNULL(K.CERRADO,0) = 0 AND K.ID_KARDEX = @ID_KARDEX "
        sSql += " GROUP BY K.ID_MEDICAMENTO ,K.NRO_LOTE,K.F_VENCIMIENTO, k.ID_KARDEX "
        sSql += " ) K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO  "

        Dim tblConsulta = New datos().ejecutarConsulta(sSql)

        txtMedicamento.Text = tblConsulta.Rows(0)("MEDICAMENTO")
        txtTipo.Text = tblConsulta.Rows(0)("FORMA_FARMACEUTICA")
        txtLaboratorio.Text = tblConsulta.Rows(0)("LABORATORIO")
        txtCantidad.Text = tblConsulta.Rows(0)("TOTAL_STOCK")
        txtFechaVencimiento.Text = tblConsulta.Rows(0)("F_VENCIMIENTO")
        txtLote.Text = tblConsulta.Rows(0)("NRO_LOTE")
        txtCantidad.Text = (tblConsulta.Rows(0)("TOTAL_STOCK") * -1).ToString
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Try
            If validarformulario() Then
                Using scope As New TransactionScope()
                    'guardarInventario()
                    guardarMovimiento()
                    scope.Complete()
                End Using
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
            title = "Error Inconsistencia en la base de datos"
            Dim msg = ex.Message.ToString
            response = MsgBox(msg, style, title)
        End Try
      
      

    End Sub
    'Protected Sub guardarInventario()
    '    Dim tInv = New datos()
    '    tInv.tabla = "INVENTARIO"
    '    tInv.campoID = "ID_INVENTARIO"
    '    tInv.modoID = "auto"
    '    tInv.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
    '    tInv.agregarCampoValor("CANTIDAD", CInt(txtCantidad.Text) * -1)
    '    tInv.agregarCampoValor("F_VENCIMIENTO", CDate(txtFechaVencimiento.Text).ToString("yyyyMMdd"))
    '    tInv.agregarCampoValor("CONTROL_FECHA", 1)
    '    tInv.agregarCampoValor("AJUSTES", 1)
    '    tInv.agregarCampoValor("F_AJUSTE", "serverDateTime")
    '    tInv.agregarCampoValor("OBSERVACION", txtObservacion.Text)
    '    tInv.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
    '    tInv.agregarCampoValor("LOTE", txtLote.Text)
    '    tInv.agregarCampoValor("FECHA", "serverDateTime")
    '    tInv.insertar()
    'End Sub
    Protected Sub guardarMovimiento()
        Dim tbl = New datos()
        tbl.tabla = "MOVIMIENTO_KARDEX"
        tbl.campoID = "ID_MOVIMIENTO"
        tbl.modoID = "sql"
        tbl.agregarCampoValor("ID_KARDEX", idKardex)
        tbl.agregarCampoValor("ID_TIPO_MOVIMIENTO", IIf(CInt(txtCantidad.Text) > 0, 4, 5))
        tbl.agregarCampoValor("CANTIDAD", CInt(txtCantidad.Text))
        tbl.agregarCampoValor("FECHA", "serverDateTime")
        tbl.agregarCampoValor("OBSERVACION", txtObservacion.Text)
        tbl.agregarCampoValor("ID_USUARIO", form_sesion.cusuario.ID_USUARIO)        
        tbl.insertar()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        If Trim(txtCantidad.Text).Length > 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "Complete el campo Cantidad."
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If

        If CInt(txtCantidad.Text) <> 0 Then
            ErrorProvider1.SetError(txtCantidad, "")
        Else
            mensaje = "La Cantidad no puede ser 0"
            ErrorProvider1.SetError(txtCantidad, mensaje)
            Return False
        End If
        Return True
    End Function
End Class