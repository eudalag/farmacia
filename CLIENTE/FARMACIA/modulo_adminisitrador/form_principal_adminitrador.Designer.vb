﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_principal_adminitrador
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_principal_adminitrador))
        Me.panelContenedor = New System.Windows.Forms.Panel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblHora = New System.Windows.Forms.Label()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.UsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeUsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoProveedorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VencimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistenciaPorLaboratorioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventarioGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AperturaDePaqCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormaFarmaceuticaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KardexToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosPorLoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeStockDeMedicamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcepcionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FinanzaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDeCobranzaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeCuentaPorCobrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepcionDeCompraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepcionDeMedicamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeDeVentaPorLaboratorioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasPsicotropicoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasRecibosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaVentaDistribuidorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TraspasosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolicitudToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepcionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepcionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeCierresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguracionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarContraseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActivarFacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DosificacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirDelSistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeResultadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelContenedor
        '
        Me.panelContenedor.Location = New System.Drawing.Point(0, 25)
        Me.panelContenedor.Margin = New System.Windows.Forms.Padding(4)
        Me.panelContenedor.Name = "panelContenedor"
        Me.panelContenedor.Size = New System.Drawing.Size(1350, 700)
        Me.panelContenedor.TabIndex = 20
        '
        'Timer1
        '
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHora.Location = New System.Drawing.Point(1449, 830)
        Me.lblHora.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(64, 17)
        Me.lblHora.TabIndex = 44
        Me.lblHora.Text = "00:00:00"
        Me.lblHora.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuario.Location = New System.Drawing.Point(101, 830)
        Me.lblUsuario.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(57, 17)
        Me.lblUsuario.TabIndex = 43
        Me.lblUsuario.Text = "Usuario"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(1380, 830)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 17)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Hora:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 830)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 17)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Usuario:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuarioToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.ProveedoresToolStripMenuItem, Me.ProductosToolStripMenuItem, Me.KardexToolStripMenuItem1, Me.FinanzaToolStripMenuItem, Me.CompraToolStripMenuItem, Me.ComprasToolStripMenuItem1, Me.VentaToolStripMenuItem, Me.TraspasosToolStripMenuItem, Me.CajasToolStripMenuItem, Me.SistemaToolStripMenuItem, Me.CerrarSesionToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1284, 24)
        Me.MenuStrip1.TabIndex = 51
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'UsuarioToolStripMenuItem
        '
        Me.UsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListaDeUsuariosToolStripMenuItem, Me.NuevoToolStripMenuItem2})
        Me.UsuarioToolStripMenuItem.Enabled = False
        Me.UsuarioToolStripMenuItem.Name = "UsuarioToolStripMenuItem"
        Me.UsuarioToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.UsuarioToolStripMenuItem.Text = "Usuarios"
        '
        'ListaDeUsuariosToolStripMenuItem
        '
        Me.ListaDeUsuariosToolStripMenuItem.Name = "ListaDeUsuariosToolStripMenuItem"
        Me.ListaDeUsuariosToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.ListaDeUsuariosToolStripMenuItem.Text = "Lista de Usuarios"
        '
        'NuevoToolStripMenuItem2
        '
        Me.NuevoToolStripMenuItem2.Name = "NuevoToolStripMenuItem2"
        Me.NuevoToolStripMenuItem2.Size = New System.Drawing.Size(169, 22)
        Me.NuevoToolStripMenuItem2.Text = "Nuevo"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListaDeClientesToolStripMenuItem, Me.NuevoToolStripMenuItem1})
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'ListaDeClientesToolStripMenuItem
        '
        Me.ListaDeClientesToolStripMenuItem.Name = "ListaDeClientesToolStripMenuItem"
        Me.ListaDeClientesToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.ListaDeClientesToolStripMenuItem.Text = "Lista de Clientes"
        '
        'NuevoToolStripMenuItem1
        '
        Me.NuevoToolStripMenuItem1.Name = "NuevoToolStripMenuItem1"
        Me.NuevoToolStripMenuItem1.Size = New System.Drawing.Size(164, 22)
        Me.NuevoToolStripMenuItem1.Text = "Nuevo"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListaDeProveedoresToolStripMenuItem, Me.NuevoProveedorToolStripMenuItem})
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(91, 20)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores "
        '
        'ListaDeProveedoresToolStripMenuItem
        '
        Me.ListaDeProveedoresToolStripMenuItem.Name = "ListaDeProveedoresToolStripMenuItem"
        Me.ListaDeProveedoresToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ListaDeProveedoresToolStripMenuItem.Text = "Lista de Proveedores"
        '
        'NuevoProveedorToolStripMenuItem
        '
        Me.NuevoProveedorToolStripMenuItem.Name = "NuevoProveedorToolStripMenuItem"
        Me.NuevoProveedorToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.NuevoProveedorToolStripMenuItem.Text = "Nuevo Proveedor"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListaDeProductosToolStripMenuItem, Me.VencimientosToolStripMenuItem, Me.ExistenciaPorLaboratorioToolStripMenuItem, Me.NuevoToolStripMenuItem, Me.InventarioToolStripMenuItem, Me.InventarioGeneralToolStripMenuItem, Me.AperturaDePaqCajaToolStripMenuItem, Me.FormaFarmaceuticaToolStripMenuItem})
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(74, 20)
        Me.ProductosToolStripMenuItem.Text = "Productos"
        '
        'ListaDeProductosToolStripMenuItem
        '
        Me.ListaDeProductosToolStripMenuItem.Name = "ListaDeProductosToolStripMenuItem"
        Me.ListaDeProductosToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.ListaDeProductosToolStripMenuItem.Text = "Lista de Productos"
        '
        'VencimientosToolStripMenuItem
        '
        Me.VencimientosToolStripMenuItem.Name = "VencimientosToolStripMenuItem"
        Me.VencimientosToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.VencimientosToolStripMenuItem.Text = "Vencimientos"
        '
        'ExistenciaPorLaboratorioToolStripMenuItem
        '
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Name = "ExistenciaPorLaboratorioToolStripMenuItem"
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.ExistenciaPorLaboratorioToolStripMenuItem.Text = "Existencia por Laboratorio"
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Enabled = False
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'InventarioToolStripMenuItem
        '
        Me.InventarioToolStripMenuItem.Enabled = False
        Me.InventarioToolStripMenuItem.Name = "InventarioToolStripMenuItem"
        Me.InventarioToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.InventarioToolStripMenuItem.Text = "Inventario Inicial "
        '
        'InventarioGeneralToolStripMenuItem
        '
        Me.InventarioGeneralToolStripMenuItem.Name = "InventarioGeneralToolStripMenuItem"
        Me.InventarioGeneralToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.InventarioGeneralToolStripMenuItem.Text = "Inventario General"
        '
        'AperturaDePaqCajaToolStripMenuItem
        '
        Me.AperturaDePaqCajaToolStripMenuItem.Enabled = False
        Me.AperturaDePaqCajaToolStripMenuItem.Name = "AperturaDePaqCajaToolStripMenuItem"
        Me.AperturaDePaqCajaToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.AperturaDePaqCajaToolStripMenuItem.Text = "Apertura de Paq. / Caja"
        '
        'FormaFarmaceuticaToolStripMenuItem
        '
        Me.FormaFarmaceuticaToolStripMenuItem.Enabled = False
        Me.FormaFarmaceuticaToolStripMenuItem.Name = "FormaFarmaceuticaToolStripMenuItem"
        Me.FormaFarmaceuticaToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.FormaFarmaceuticaToolStripMenuItem.Text = "Forma Farmaceutica"
        '
        'KardexToolStripMenuItem1
        '
        Me.KardexToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MovimientosPorLoteToolStripMenuItem, Me.InformeStockDeMedicamentosToolStripMenuItem, Me.ExcepcionesToolStripMenuItem1})
        Me.KardexToolStripMenuItem1.Name = "KardexToolStripMenuItem1"
        Me.KardexToolStripMenuItem1.Size = New System.Drawing.Size(58, 20)
        Me.KardexToolStripMenuItem1.Text = "Kardex"
        '
        'MovimientosPorLoteToolStripMenuItem
        '
        Me.MovimientosPorLoteToolStripMenuItem.Name = "MovimientosPorLoteToolStripMenuItem"
        Me.MovimientosPorLoteToolStripMenuItem.Size = New System.Drawing.Size(251, 22)
        Me.MovimientosPorLoteToolStripMenuItem.Text = "Movimientos por Lote"
        '
        'InformeStockDeMedicamentosToolStripMenuItem
        '
        Me.InformeStockDeMedicamentosToolStripMenuItem.Name = "InformeStockDeMedicamentosToolStripMenuItem"
        Me.InformeStockDeMedicamentosToolStripMenuItem.Size = New System.Drawing.Size(251, 22)
        Me.InformeStockDeMedicamentosToolStripMenuItem.Text = "Informe Stock de Medicamentos"
        '
        'ExcepcionesToolStripMenuItem1
        '
        Me.ExcepcionesToolStripMenuItem1.Enabled = False
        Me.ExcepcionesToolStripMenuItem1.Name = "ExcepcionesToolStripMenuItem1"
        Me.ExcepcionesToolStripMenuItem1.Size = New System.Drawing.Size(251, 22)
        Me.ExcepcionesToolStripMenuItem1.Text = "Excepciones"
        '
        'FinanzaToolStripMenuItem
        '
        Me.FinanzaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDeCobranzaToolStripMenuItem, Me.InformeCuentaPorCobrarToolStripMenuItem})
        Me.FinanzaToolStripMenuItem.Name = "FinanzaToolStripMenuItem"
        Me.FinanzaToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.FinanzaToolStripMenuItem.Text = "Creditos"
        '
        'GestionDeCobranzaToolStripMenuItem
        '
        Me.GestionDeCobranzaToolStripMenuItem.Name = "GestionDeCobranzaToolStripMenuItem"
        Me.GestionDeCobranzaToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.GestionDeCobranzaToolStripMenuItem.Text = "Gestion de Cobranza"
        '
        'InformeCuentaPorCobrarToolStripMenuItem
        '
        Me.InformeCuentaPorCobrarToolStripMenuItem.Name = "InformeCuentaPorCobrarToolStripMenuItem"
        Me.InformeCuentaPorCobrarToolStripMenuItem.Size = New System.Drawing.Size(219, 22)
        Me.InformeCuentaPorCobrarToolStripMenuItem.Text = "Informe Cuenta por Cobrar"
        '
        'CompraToolStripMenuItem
        '
        Me.CompraToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ComprasToolStripMenuItem, Me.RecepcionDeCompraToolStripMenuItem})
        Me.CompraToolStripMenuItem.Enabled = False
        Me.CompraToolStripMenuItem.Name = "CompraToolStripMenuItem"
        Me.CompraToolStripMenuItem.Size = New System.Drawing.Size(139, 20)
        Me.CompraToolStripMenuItem.Text = "Ordenes de  Compras"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.ComprasToolStripMenuItem.Text = "Nuevo"
        '
        'RecepcionDeCompraToolStripMenuItem
        '
        Me.RecepcionDeCompraToolStripMenuItem.Name = "RecepcionDeCompraToolStripMenuItem"
        Me.RecepcionDeCompraToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.RecepcionDeCompraToolStripMenuItem.Text = "Recepcion"
        '
        'ComprasToolStripMenuItem1
        '
        Me.ComprasToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RecepcionDeMedicamentosToolStripMenuItem, Me.FacturasToolStripMenuItem})
        Me.ComprasToolStripMenuItem1.Enabled = False
        Me.ComprasToolStripMenuItem1.Name = "ComprasToolStripMenuItem1"
        Me.ComprasToolStripMenuItem1.Size = New System.Drawing.Size(69, 20)
        Me.ComprasToolStripMenuItem1.Text = "Compras"
        '
        'RecepcionDeMedicamentosToolStripMenuItem
        '
        Me.RecepcionDeMedicamentosToolStripMenuItem.Name = "RecepcionDeMedicamentosToolStripMenuItem"
        Me.RecepcionDeMedicamentosToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.RecepcionDeMedicamentosToolStripMenuItem.Text = "Recepcion de Medicamentos"
        '
        'FacturasToolStripMenuItem
        '
        Me.FacturasToolStripMenuItem.Name = "FacturasToolStripMenuItem"
        Me.FacturasToolStripMenuItem.Size = New System.Drawing.Size(235, 22)
        Me.FacturasToolStripMenuItem.Text = "Facturas / Recibos"
        '
        'VentaToolStripMenuItem
        '
        Me.VentaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaVentaToolStripMenuItem, Me.VentasToolStripMenuItem, Me.InformeDeVentaPorLaboratorioToolStripMenuItem, Me.VentasPsicotropicoToolStripMenuItem, Me.FacturasRecibosToolStripMenuItem, Me.NuevaVentaDistribuidorToolStripMenuItem, Me.EstadoDeResultadoToolStripMenuItem})
        Me.VentaToolStripMenuItem.Name = "VentaToolStripMenuItem"
        Me.VentaToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.VentaToolStripMenuItem.Text = "Ventas"
        '
        'NuevaVentaToolStripMenuItem
        '
        Me.NuevaVentaToolStripMenuItem.Name = "NuevaVentaToolStripMenuItem"
        Me.NuevaVentaToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.NuevaVentaToolStripMenuItem.Text = "Nueva Venta"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'InformeDeVentaPorLaboratorioToolStripMenuItem
        '
        Me.InformeDeVentaPorLaboratorioToolStripMenuItem.Name = "InformeDeVentaPorLaboratorioToolStripMenuItem"
        Me.InformeDeVentaPorLaboratorioToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.InformeDeVentaPorLaboratorioToolStripMenuItem.Text = "Informe de Venta por Laboratorio"
        '
        'VentasPsicotropicoToolStripMenuItem
        '
        Me.VentasPsicotropicoToolStripMenuItem.Enabled = False
        Me.VentasPsicotropicoToolStripMenuItem.Name = "VentasPsicotropicoToolStripMenuItem"
        Me.VentasPsicotropicoToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.VentasPsicotropicoToolStripMenuItem.Text = "Informe de Venta"
        '
        'FacturasRecibosToolStripMenuItem
        '
        Me.FacturasRecibosToolStripMenuItem.Enabled = False
        Me.FacturasRecibosToolStripMenuItem.Name = "FacturasRecibosToolStripMenuItem"
        Me.FacturasRecibosToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.FacturasRecibosToolStripMenuItem.Text = "facturas /  recibos"
        '
        'NuevaVentaDistribuidorToolStripMenuItem
        '
        Me.NuevaVentaDistribuidorToolStripMenuItem.Name = "NuevaVentaDistribuidorToolStripMenuItem"
        Me.NuevaVentaDistribuidorToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.NuevaVentaDistribuidorToolStripMenuItem.Text = "Nueva Venta Distribuidor"
        '
        'TraspasosToolStripMenuItem
        '
        Me.TraspasosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SolicitudToolStripMenuItem, Me.RecepcionToolStripMenuItem, Me.RecepcionToolStripMenuItem1})
        Me.TraspasosToolStripMenuItem.Enabled = False
        Me.TraspasosToolStripMenuItem.Name = "TraspasosToolStripMenuItem"
        Me.TraspasosToolStripMenuItem.Size = New System.Drawing.Size(76, 20)
        Me.TraspasosToolStripMenuItem.Text = "Traspasos"
        '
        'SolicitudToolStripMenuItem
        '
        Me.SolicitudToolStripMenuItem.Name = "SolicitudToolStripMenuItem"
        Me.SolicitudToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.SolicitudToolStripMenuItem.Text = "Solicitudes Entrantes"
        '
        'RecepcionToolStripMenuItem
        '
        Me.RecepcionToolStripMenuItem.Name = "RecepcionToolStripMenuItem"
        Me.RecepcionToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.RecepcionToolStripMenuItem.Text = "Solicitudes Salientes"
        '
        'RecepcionToolStripMenuItem1
        '
        Me.RecepcionToolStripMenuItem1.Name = "RecepcionToolStripMenuItem1"
        Me.RecepcionToolStripMenuItem1.Size = New System.Drawing.Size(189, 22)
        Me.RecepcionToolStripMenuItem1.Text = "Recepcion"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConsultaDeCierresToolStripMenuItem})
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'ConsultaDeCierresToolStripMenuItem
        '
        Me.ConsultaDeCierresToolStripMenuItem.Name = "ConsultaDeCierresToolStripMenuItem"
        Me.ConsultaDeCierresToolStripMenuItem.Size = New System.Drawing.Size(181, 22)
        Me.ConsultaDeCierresToolStripMenuItem.Text = "Consulta de Cierres"
        '
        'SistemaToolStripMenuItem
        '
        Me.SistemaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfiguracionToolStripMenuItem, Me.CambiarContraseToolStripMenuItem, Me.ActivarFacturacionToolStripMenuItem, Me.DosificacionToolStripMenuItem})
        Me.SistemaToolStripMenuItem.Name = "SistemaToolStripMenuItem"
        Me.SistemaToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.SistemaToolStripMenuItem.Text = "Sistema"
        '
        'ConfiguracionToolStripMenuItem
        '
        Me.ConfiguracionToolStripMenuItem.Name = "ConfiguracionToolStripMenuItem"
        Me.ConfiguracionToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.ConfiguracionToolStripMenuItem.Text = "Configuracion"
        '
        'CambiarContraseToolStripMenuItem
        '
        Me.CambiarContraseToolStripMenuItem.Name = "CambiarContraseToolStripMenuItem"
        Me.CambiarContraseToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.CambiarContraseToolStripMenuItem.Text = "Cambiar Contrasena"
        '
        'ActivarFacturacionToolStripMenuItem
        '
        Me.ActivarFacturacionToolStripMenuItem.Enabled = False
        Me.ActivarFacturacionToolStripMenuItem.Name = "ActivarFacturacionToolStripMenuItem"
        Me.ActivarFacturacionToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.ActivarFacturacionToolStripMenuItem.Text = "Activar Facturacion"
        '
        'DosificacionToolStripMenuItem
        '
        Me.DosificacionToolStripMenuItem.Name = "DosificacionToolStripMenuItem"
        Me.DosificacionToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.DosificacionToolStripMenuItem.Text = "Dosificacion"
        '
        'CerrarSesionToolStripMenuItem1
        '
        Me.CerrarSesionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CerrarSesionToolStripMenuItem, Me.SalirDelSistemaToolStripMenuItem})
        Me.CerrarSesionToolStripMenuItem1.Name = "CerrarSesionToolStripMenuItem1"
        Me.CerrarSesionToolStripMenuItem1.Size = New System.Drawing.Size(94, 20)
        Me.CerrarSesionToolStripMenuItem1.Text = "Cerrar Sesion"
        '
        'CerrarSesionToolStripMenuItem
        '
        Me.CerrarSesionToolStripMenuItem.Name = "CerrarSesionToolStripMenuItem"
        Me.CerrarSesionToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.CerrarSesionToolStripMenuItem.Text = "Cerrar Sesion"
        '
        'SalirDelSistemaToolStripMenuItem
        '
        Me.SalirDelSistemaToolStripMenuItem.Name = "SalirDelSistemaToolStripMenuItem"
        Me.SalirDelSistemaToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.SalirDelSistemaToolStripMenuItem.Text = "Salir del sistema"
        '
        'EstadoDeResultadoToolStripMenuItem
        '
        Me.EstadoDeResultadoToolStripMenuItem.Name = "EstadoDeResultadoToolStripMenuItem"
        Me.EstadoDeResultadoToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.EstadoDeResultadoToolStripMenuItem.Text = "Estado de Resultado"
        '
        'form_principal_adminitrador
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1284, 721)
        Me.Controls.Add(Me.lblHora)
        Me.Controls.Add(Me.panelContenedor)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "form_principal_adminitrador"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sistema de Administracion - Farmacia"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblHora As System.Windows.Forms.Label
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents UsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeUsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VencimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AperturaDePaqCajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormaFarmaceuticaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevaVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasPsicotropicoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepcionDeCompraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevoProveedorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguracionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents panelContenedor As System.Windows.Forms.Panel
    Friend WithEvents CerrarSesionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KardexToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosPorLoteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcepcionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TraspasosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SolicitudToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepcionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepcionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarContraseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepcionDeMedicamentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventarioGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturasRecibosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CerrarSesionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirDelSistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeCierresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActivarFacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DosificacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeStockDeMedicamentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FinanzaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GestionDeCobranzaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeCuentaPorCobrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NuevaVentaDistribuidorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExistenciaPorLaboratorioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeDeVentaPorLaboratorioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeResultadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
