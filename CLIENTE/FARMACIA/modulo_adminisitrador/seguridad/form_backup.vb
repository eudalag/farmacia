﻿Imports CAPA_DATOS
Public Class form_backup
    Private mod_conf As modulo = New modulo()
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        If txtDestino.Text = "" Then
            Dim mensaje As String = "Escoja la ubicación para guardar la copia de seguridad"
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.Critical
            title = "Error"
            response = MsgBox(mensaje, style, title)
        Else
            Dim cDatos As datos = New datos()
            cDatos.crearBackup(txtDestino.Text)
            Dim mensaje As String = "Copia de seguridad generada con exito."
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.OkOnly
            title = "Copia de Seguridad"
            response = MsgBox(mensaje, style, title)
            volver()
        End If
    End Sub
    Private Sub volver()
        Me.Close()
        If form_principal_adminitrador.panelContenedor.Controls.Count > 0 Then
            form_principal_adminitrador.panelContenedor.Controls.RemoveAt(0)
        End If
    End Sub

    Private Sub btnExaminar_Click(sender As Object, e As EventArgs) Handles btnExaminar.Click
        txtOrigen.Text = ""
        Dim dlgDestino As New FolderBrowserDialog
        With dlgDestino
            .Description = "Seleccione el directorio de destino, trate de no escojer uno en donde se encuentre el sistema operativo:"
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim strDestino As String = .SelectedPath.ToString
                If Not strDestino.EndsWith("") Then
                    strDestino = strDestino & ""
                End If
                Me.txtDestino.Text = strDestino
            End If
        End With
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        volver()
    End Sub

End Class