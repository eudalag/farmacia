﻿Imports CAPA_DATOS
Public Class formConfirmacion
    Private tipo As Integer

    Public Sub New(Optional ByVal _tipo As Integer = 1)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        tipo = _tipo
        Select Case tipo
            Case 1
                Label1.Visible = True
                Label2.Visible = False
            Case 2
                Label1.Visible = False
                Label2.Visible = True
        End Select
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim sSql As String = ""
        sSql += " DECLARE @PSW AS NVARCHAR(100);"
        sSql += " SET @PSW = '" + New modulo().GenerarHash(txtCodigo.Text) + "';"
        sSql += " SELECT COUNT(*) FROM USUARIO "
        sSql += " WHERE CONTRASENHA = @PSW "
        Select Case tipo
            Case 1
                sSql += " AND ADMINISTRADOR = 1"
            Case 2
                sSql += " AND ID_USUARIO = " + variableSesion.idUsuario.ToString
        End Select
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        If CInt(tblConsulta.Rows(0)(0)) > 0 Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub formConfirmacion_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtCodigo.Focus()
    End Sub
End Class