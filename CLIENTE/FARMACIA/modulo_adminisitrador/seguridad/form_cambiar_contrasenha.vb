﻿Public Class form_cambiar_contrasenha

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarFormulario() Then
            form_sesion.cusuario.CONTRASENHA = New modulo().GenerarHash(txtContraseña2.Text)
            form_sesion.cusuario.actualizarContrasenha()            
                Me.DialogResult = Windows.Forms.DialogResult.OK            
        End If
    End Sub
    Private Function validarFormulario() As Boolean
        Dim valido As Boolean = True
        Dim mensaje As String = ""
        If Trim(txtContraseñaAnterior.Text).Length > 0 Then
            ErrorProvider1.SetError(txtContraseñaAnterior, "")
        Else
            mensaje = "Complete el campo Contraseña."
            ErrorProvider1.SetError(txtContraseñaAnterior, mensaje)
            valido = False
        End If
        If Trim(txtContraseña.Text).Length > 0 Then
            ErrorProvider1.SetError(txtContraseña, "")
        Else
            mensaje = "Complete el campo Nueva Contraseña."
            ErrorProvider1.SetError(txtContraseña, mensaje)
            valido = False
        End If
        If Trim(txtContraseña2.Text).Length > 0 Then
            ErrorProvider1.SetError(txtContraseña2, "")
        Else
            mensaje = "Complete el campo Repita Contraseña."
            ErrorProvider1.SetError(txtContraseña2, mensaje)
            valido = False
        End If
        If valido Then
            If txtContraseña.Text = txtContraseña2.Text Then
                ErrorProvider1.SetError(txtContraseña, "")
                ErrorProvider1.SetError(txtContraseña2, "")
            Else
                mensaje = "Los campos contraseñas no coinciden."
                ErrorProvider1.SetError(txtContraseña, mensaje)
                ErrorProvider1.SetError(txtContraseña2, mensaje)
                valido = False
            End If
        End If
        Dim anteriorPsw
        If valido Then
            anteriorPsw = New modulo().GenerarHash(txtContraseñaAnterior.Text)
            If anteriorPsw = form_sesion.cusuario.CONTRASENHA Then
                ErrorProvider1.SetError(txtContraseñaAnterior, "")
            Else
                mensaje = "La contraseña anterior no coincide."
                ErrorProvider1.SetError(txtContraseñaAnterior, mensaje)
                valido = False
            End If
        End If
        If valido Then
            If txtContraseñaAnterior.Text = txtContraseña.Text And txtContraseñaAnterior.Text = txtContraseña2.Text Then
                mensaje = "La nueva contraseña, tiene que ser distinta a la anterior"
                ErrorProvider1.SetError(txtContraseña, mensaje)
                valido = False
            Else
                ErrorProvider1.SetError(txtContraseña, "")
            End If
        End If

        Return valido
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class