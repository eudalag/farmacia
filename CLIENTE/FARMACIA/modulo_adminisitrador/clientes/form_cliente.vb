﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Public Class form_cliente
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable


    Protected Sub iniciarComboGrupo()
        Dim tPedido As eVenta = New eVenta()
        Dim sSql As String = ""
        sSql += " SELECT * FROM GRUPO_CLIENTES 
        sSql += " UNION ALL SELECT -1, '[TODOS LOS GRUPOS]'"
        sSql += " ORDER BY GRUPO_PERSONAS  "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        cmbCaja.DataSource = tbl
        cmbCaja.DisplayMember = "CAJA"
        cmbCaja.ValueMember = "ID_BANCO"
    End Sub
   
    

    Private Sub inicializarParametros()
        'With (dgvGrilla)
        '    '831| 360
        '    '.Width = 1120
        '    '.Height = 400
        '    .MultiSelect = False
        '    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
        '    .AutoGenerateColumns = False
        '    .ColumnCount = 3
        '    .Columns(0).Name = "Registro"
        '    .Columns(0).HeaderText = "Registro"
        '    .Columns(0).DataPropertyName = "ID_CLIENTE"
        '    .Columns(0).Width = 100

        '    .Columns(1).Name = "Nombre"
        '    .Columns(1).HeaderText = "Nombre Apellido o Empresa/Institución"
        '    .Columns(1).DataPropertyName = "NOMBRE_APELLIDOS"
        '    .Columns(1).Width = 900
        '    .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

        '    .Columns(2).Name = "nit"
        '    .Columns(2).HeaderText = "NIT / CI"
        '    .Columns(2).DataPropertyName = "NIT"
        '    .Columns(2).Width = 200

        'End With
        'LoadPage()
    End Sub

    Private Sub form_cliente_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tblCliente As eCliente = New eCliente()
        dtSource = tblCliente.mostrarCliente()
        inicializarParametros()
        If Not form_sesion.cusuario.ADMINISTRADOR Then            
            btnEliminar.Visible = False
        End If
        txtBuscar.Focus()
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New form_gestionCliente()
        If form_sesion.cusuario.ADMINISTRADOR Then
            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
        Else
            mod_conf.cambiarForm(form_principal_venta.panelContenedor, form_gestion)
        End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim tblCliente = New eCliente()
        dtSource = tblCliente.mostrarCliente(txtBuscar.Text)
        'Me.totalRegistro = dtSource.Rows.Count
        'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        'Me.paginaActual = 1
        'Me.regNro = 0
        'LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idCliente As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Me.Close()
        Dim form_gestion As form_gestionCliente = New form_gestionCliente(idCliente)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim id_cliente As Integer = 0
        id_cliente = dgvGrilla.SelectedCells(0).Value
        Dim tCliente As eCliente = New eCliente()
        tCliente.ID_CLIENTE = id_cliente
        tCliente.eliminar()
        If tCliente.mensajesError = "" Then
            dtSource = tCliente.mostrarCliente(txtBuscar.Text)
            'Me.totalRegistro = dtSource.Rows.Count
            'Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
            'Me.paginaActual = 1
            'Me.regNro = 0
            'LoadPage()
        Else
            mod_conf.mensajeError("Error", tCliente.mensajesError)
        End If
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If Not CBool(form_sesion.cusuario.ADMINISTRADOR) Then
            Me.Close()
            Dim formulario As form_ventas = New form_ventas()
            mod_conf.volver_venta(formulario)
        End If        
    End Sub

    Private Sub form_cliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.Escape
                btnVolver_Click(Nothing, Nothing)
        End Select
    End Sub
End Class