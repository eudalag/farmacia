﻿Imports CAPA_NEGOCIO

Public Class form_gestionCliente
    Private _id_cliente As Integer = 0
    Private tcliente As eCliente = New eCliente()
    Private mod_conf As modulo = New modulo()
    Private modal_cliente As Boolean = False
    Private modo As Integer

    Public Sub New(ByVal idCliente As Integer)
        InitializeComponent()
        _id_cliente = idCliente
        tcliente.ID_CLIENTE = idCliente
        tcliente.cargarDatos()
        cargarDatos()
    End Sub
    Public Sub New(ByVal nit As String, ByVal modal As Boolean, Optional ByVal _modo As Integer = 1)
        InitializeComponent()
        txtNit.Text = nit
        modal_cliente = modal
        modo = _modo
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub
    Private Sub cargarDatos()
        txtNombre.Text = tcliente.PERSONA.NOMBRE_APELLIDOS
        txtNit.Text = tcliente.NIT
        txtDireccion.Text = tcliente.PERSONA.DIRECCION
        txtCelular.Text = tcliente.PERSONA.CELULAR
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If modal_cliente Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            volver()
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            tcliente.PERSONA.NOMBRE_APELLIDOS = txtNombre.Text
            tcliente.PERSONA.DIRECCION = txtDireccion.Text
            tcliente.PERSONA.CELULAR = txtCelular.Text
            tcliente.PERSONA.CORREO = ""
            tcliente.PERSONA.NRO_CI = ""            
            tcliente.NIT = txtNit.Text
            tcliente.NOMBRE_FACTURA = txtNombre.Text
            tcliente.LIMITE_CREDITO = 0
            tcliente.CREDITO = 0
            tcliente.OBSERVACIONES = ""
            tcliente.ID_GRUPO = 6
            tcliente.REFERENCIA_EMPRESA = ""

            If _id_cliente = 0 Then
                tcliente.insertar()
            Else
                tcliente.modificar()
            End If
            If tcliente.mensajesError = "" Then
                If modal_cliente Then
                    Select Case modo
                        Case 1
                            form_pedido._nit = txtNit.Text
                        Case 2
                            emitirFactura._nit = txtNit.Text
                        Case 3
                            consultorio._nit = txtNit.Text
                    End Select

                    Me.DialogResult = Windows.Forms.DialogResult.OK
                Else
                    volver()
                End If
            Else
                mod_conf.mensajeError("Error Gestión Cliente", tcliente.mensajesError)
            End If
        End If
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Cliente/Empresa."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If

        If Trim(txtNit.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNit, "")
        Else
            mensaje = "Complete el campo Nit / CI."
            ErrorProvider1.SetError(txtNit, mensaje)
            valido = False
        End If
        Return valido
    End Function
    Private Sub volver()
        If form_sesion.cusuario.ADMINISTRADOR Then
            Me.Close()
            mod_conf.volver(New form_cliente())
        Else
            Me.Close()
            mod_conf.volver_venta(New form_cliente())
        End If
    End Sub

    Private Sub form_gestionCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtNit.Focus()
        End If
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNit.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        Else
            e.Handled = True
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 8 Then
            e.Handled = False
        End If
    End Sub

    Private Sub form_gestionCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub
End Class