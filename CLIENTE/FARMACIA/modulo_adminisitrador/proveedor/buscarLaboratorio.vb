﻿Imports CAPA_DATOS
Public Class buscarLaboratorio
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 14

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 365

            .Height = 330
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 2
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_LABORATORIO"
            .Columns(0).Width = 100

            .Columns(1).Name = "Nombre"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "DESCRIPCION"
            .Columns(1).Width = 270
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
        LoadPage()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim idLaboratorio As Integer = dgvGrilla.SelectedCells(0).Value
        Dim laboratorio As String = dgvGrilla.SelectedCells(1).Value
        Me.Close()
        form_gestionProveedor.nuevoItem(laboratorio, 1, 1, idLaboratorio)
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub buscarLaboratorio_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tbl = New datos()
        Dim sSql As String = "SELECT * FROM PROVEEDORES ORDER BY NOMBRE, NIT "

        dtSource = laboratorios()
        inicializarParametros()
        txtBuscar.Focus()
    End Sub
    Private Function laboratorios() As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(50);"
        sSql += " SET @BUSCAR = '" + txtBuscar.Text + "';"
        sSql += " SELECT * FROM LABORATORIO"
        sSql += " WHERE LABORATORIO.DESCRIPCION LIKE '%' +  @BUSCAR + '%' "
        Return New datos().ejecutarConsulta(sSql)
    End Function

    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        
        dtSource = laboratorios()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()

    End Sub

    Private Sub dgvGrilla_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idLaboratorio As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Dim laboratorio As String = CType(dgvGrilla.Rows(e.RowIndex).Cells(1), DataGridViewTextBoxCell).Value
        Me.Close()
        form_gestionProveedor.nuevoItem(laboratorio, 1, 1, idLaboratorio)
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class