﻿Imports CAPA_DATOS
Imports System.Transactions
Public Class form_gestionProveedor
    Private _idProveedor As Integer = 0
    Private mod_conf As modulo = New modulo()
    Private modal_cliente As Boolean = False

    Public Shared tblLaboratorio As DataTable

    Public Sub New(ByVal idProveedor As Integer)
        InitializeComponent()
        _idProveedor = idProveedor
        Dim sSql As String = "SELECT * FROM PROVEEDORES WHERE ID_PROVEEDOR = " + idProveedor.ToString
        cargarDatos(New datos().ejecutarConsulta(sSql))
        crearTabla()
        inicializarParametrosLaboratorio()
        cargarLaboratorios(idProveedor)
        LoadPage()
    End Sub
    Private Sub cargarLaboratorios(ByVal idProveedor As String)
        Dim sSql As String = ""
        sSql += " SELECT DESCRIPCION , PL.ID_LABORATORIO  FROM PROVEEDORES_LABORATORIO PL"
        sSql += " INNER JOIN LABORATORIO L ON PL.ID_LABORATORIO = L.ID_LABORATORIO  "
        sSql += " WHERE PL.ID_PROVEEDOR  = " + idProveedor

        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        For Each row In tblConsulta.Rows
            nuevoItem(row("DESCRIPCION"), 0, 0, row("ID_LABORATORIO"))
        Next
    End Sub
    Private Sub LoadPage()
        Dim aux = tblLaboratorio.Select("ACCION <> 3")
        If aux.Count > 0 Then
            Dim dataView As New DataView(aux.CopyToDataTable)
            dataView.Sort = "NRO_FILA ASC"
            dgvGrilla.DataSource = dataView.ToTable()
        Else
            dgvGrilla.DataSource = aux
        End If
    End Sub
    Private Sub inicializarParametrosLaboratorio()
        With (dgvGrilla)
            .Width = 295
            .Height = 286
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 3
            .Columns(0).Name = "NRO_ITEM"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "NRO_FILA"
            .Columns(0).Width = 95

            .Columns(1).Name = "LABORATORIO"
            .Columns(1).HeaderText = "Laboratorio"
            .Columns(1).DataPropertyName = "LABORATORIO"
            .Columns(1).Width = 200

            .Columns(2).DataPropertyName = "ID_LABORATORIO"
            .Columns(2).Visible = False
        End With
    End Sub
    Protected Sub crearTabla()
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NRO_FILA"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "ID_LABORATORIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "LABORATORIO"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "NUEVO"
        tblItems.Columns.Add(columna)
        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "ACCION"
        tblItems.Columns.Add(columna)
        tblItems.AcceptChanges()
        tblLaboratorio = tblItems
    End Sub
    Private Sub cargarLaboratorio(ByVal idProveedor As String)
        Dim sSql As String = ""
        sSql += " DECLARE @PROVEEDOR AS INTEGER;"
        sSql += " SET @PROVEEDOR = " + idProveedor + ";"
        sSql += " SELECT L.ID_LABORATORIO,DESCRIPCION AS LABORATORIO FROM LABORATORIO L"
        sSql += " INNER JOIN PROVEEDORES_LABORATORIO PL ON PL.ID_LABORATORIO = L.ID_LABORATORIO "
        sSql += " WHERE  PL.ID_PROVEEDOR = @PROVEEDOR"
        Dim tblConsulta = New datos().ejecutarConsulta(sSql)
        For Each row In tblConsulta.Rows
            nuevoItem(row("LABORATORIO"), 0, 0, row("ID_LABORATORIO"))
        Next
    End Sub

    Public Shared Sub nuevoItem(ByVal LABORATORIO As String, ByVal nuevo As Integer, ByVal accion As Integer, ByVal ID_LABORATORIO As String)
        Dim tblItems As DataTable = tblLaboratorio
        Dim dr As DataRow
        dr = tblItems.NewRow
        dr("NRO_FILA") = tblLaboratorio.Rows.Count + 1
        dr("ID_LABORATORIO") = IIf(ID_LABORATORIO = "", DBNull.Value, ID_LABORATORIO)
        dr("LABORATORIO") = LABORATORIO
        dr("NUEVO") = nuevo
        dr("ACCION") = accion
        tblItems.Rows.Add(dr)
        tblItems.AcceptChanges()
        tblLaboratorio = tblItems
        tblLaboratorio.AcceptChanges()
    End Sub
    Private Sub eliminarItem(ByVal NRO_FILA As Integer)
        Dim tblItems As DataTable = tblLaboratorio
        For Each row As DataRow In tblItems.Rows
            If CStr(row("NRO_FILA")) = NRO_FILA Then
                row("ACCION") = 3
                Exit For
            End If
        Next
        tblItems.AcceptChanges()
        LoadPage()
    End Sub

    Protected Sub guardarLaboratorio(ByVal idProveedor As Integer)
        Dim msg As String = ""
        Dim sSql As String = ""
        Using scope = New TransactionScope
            Try
                Dim tblConsulta = New datos()
                tblConsulta.modoID = "sql"
                For Each row In tblLaboratorio.Select("ACCION <> 3 AND NUEVO = 1")
                    tblConsulta.tabla = "PROVEEDORES_LABORATORIO"
                    tblConsulta.modoID = "manual"
                    tblConsulta.agregarCampoValor("ID_PROVEEDOR", idProveedor)
                    tblConsulta.agregarCampoValor("ID_LABORATORIO", row("ID_LABORATORIO"))
                    tblConsulta.insertar()
                    tblConsulta.reset()
                Next

                For Each row In tblLaboratorio.Select("ACCION = 3 AND NUEVO = 0")
                    sSql += " DELETE FROM PROVEEDORES_LABORATORIO WHERE"
                    sSql += " ID_LABORATORIO = " + row("ID_LABORATORIO") + " AND ID_PROVEEDOR = " + idProveedor.ToString
                    tblConsulta.ejecutarSQL(sSql)
                    tblConsulta.reset()
                Next
                scope.Complete()
            Catch ex As Exception
                Dim title As String
                Dim style As MsgBoxStyle
                Dim response As MsgBoxResult
                style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
                title = "Error Inconsistencia en la base de datos"
                msg = ex.Message.ToString
                response = MsgBox(msg, style, title)
            End Try
        End Using
    End Sub
    Private Function existeCodigoBarraGrilla(ByVal laboratorio As String) As Boolean
        Dim foundRows() As DataRow
        foundRows = tblLaboratorio.Select("ID_LABORATORIO = '" + laboratorio + "' AND ACCION <> 3")
        Return foundRows.Count > 0
    End Function
    Private Sub cargarDatos(ByVal tbl As DataTable)
        txtNombre.Text = tbl.Rows(0)("NOMBRE")
        txtNit.Text = tbl.Rows(0)("NIT")
        txtDireccion.Text = tbl.Rows(0)("DIRECCION")
        txtCelular.Text = tbl.Rows(0)("CELULAR")
        txtFax.Text = tbl.Rows(0)("FAX")
        txtCorreo.Text = tbl.Rows(0)("CORREO")
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        crearTabla()
        inicializarParametrosLaboratorio()
        LoadPage()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub
    

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Dim form = New form_proveedor()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim tProveedor = New datos()
            tProveedor.tabla = "PROVEEDORES"
            tProveedor.campoID = "ID_PROVEEDOR"
            tProveedor.modoID = "sql"
            tProveedor.agregarCampoValor("NOMBRE", txtNombre.Text)
            tProveedor.agregarCampoValor("NIT", txtNit.Text)
            tProveedor.agregarCampoValor("DIRECCION", txtDireccion.Text)
            tProveedor.agregarCampoValor("CELULAR", txtCelular.Text)
            tProveedor.agregarCampoValor("FAX", txtFax.Text)
            tProveedor.agregarCampoValor("CORREO", txtCorreo.Text)
            If _idProveedor = 0 Then
                tProveedor.insertar()
                _idProveedor = tProveedor.valorID
            Else
                tProveedor.valorID = _idProveedor
                tProveedor.modificar()
            End If
            guardarLaboratorio(_idProveedor)
            btnVolver_Click(Nothing, Nothing)            
        End If

    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre del Proveedor."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If

        If Trim(txtNit.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNit, "")
        Else
            mensaje = "Complete el campo Nit / CI."
            ErrorProvider1.SetError(txtNit, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub form_gestionCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            txtNit.Focus()
        End If
    End Sub

    Private Sub txtNit_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNit.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If Char.IsDigit(ch) Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

        Else
            e.Handled = True
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 8 Then
            e.Handled = False
        End If
    End Sub

    Private Sub form_gestionCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Using nuevo_pedido = New buscarLaboratorio()
            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                LoadPage()
            End If
        End Using
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        eliminarItem(dgvGrilla.SelectedCells(0).Value)
    End Sub
End Class