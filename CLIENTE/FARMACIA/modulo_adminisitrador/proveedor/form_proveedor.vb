﻿Imports CAPA_DATOS
Imports System.Data

Public Class form_proveedor

    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10
    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            '831| 360
            .Width = 1120

            .Height = 450
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 4
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_PROVEEDOR"
            .Columns(0).Width = 100

            .Columns(1).Name = "Nombre"
            .Columns(1).HeaderText = "Empresa / Proveedor"
            .Columns(1).DataPropertyName = "NOMBRE"
            .Columns(1).Width = 700
            .Columns(1).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns(2).Name = "nit"
            .Columns(2).HeaderText = "NIT / CI"
            .Columns(2).DataPropertyName = "NIT"
            .Columns(2).Width = 200

            .Columns(3).Name = "telefono"
            .Columns(3).HeaderText = "TELEFONO"
            .Columns(3).DataPropertyName = "TELEFONO"
            .Columns(3).Width = 120
        End With
        LoadPage()
    End Sub

    Private Sub form_cliente_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tblCliente = New datos()
        Dim sSql As String = "SELECT * FROM PROVEEDORES WHERE ID_PROVEEDOR <> 10004 ORDER BY NOMBRE, NIT "

        dtSource = tblCliente.ejecutarConsulta(sSql)
        inicializarParametros()
        If Not form_sesion.cusuario.ADMINISTRADOR Then
            btnEliminar.Visible = False
        End If
        txtBuscar.Focus()
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New form_gestionProveedor()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim tblCliente = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @BUSCAR AS NVARCHAR(150);"
        sSql += " SET @BUSCAR = '" + IIf(txtBuscar.Text.Trim.Length = 0, "%", txtBuscar.Text.Trim) + "';"
        sSql += " SELECT * FROM PROVEEDORES "
        sSql += " WHERE NOMBRE LIKE '%' + @BUSCAR + '%' OR NIT LIKE '%' + @BUSCAR + '%'"
        sSql += " ORDER BY NOMBRE, NIT "
        dtSource = tblCliente.ejecutarConsulta(sSql)
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idProveedor As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Me.Close()
        Dim form_gestion As form_gestionProveedor = New form_gestionProveedor(idProveedor)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim idProveedor As Integer = 0
        idProveedor = dgvGrilla.SelectedCells(0).Value
        Dim tProveedor = New datos()
        tProveedor.tabla = "PROVEEDORES"
        tProveedor.campoID = "ID_PROVEEDOR"
        tProveedor.valorID = idProveedor
        tProveedor.eliminar()
        btnBuscar_Click(Nothing, Nothing)
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If Not CBool(form_sesion.cusuario.ADMINISTRADOR) Then
            Me.Close()
            Dim formulario As form_ventas = New form_ventas()
            mod_conf.volver_venta(formulario)
        End If
    End Sub

    Private Sub form_cliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Dim tecla
        tecla = e.KeyCode
        Select Case e.KeyCode
            Case Keys.Escape
                btnVolver_Click(Nothing, Nothing)
        End Select
    End Sub

End Class