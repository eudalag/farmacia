﻿Imports CAPA_DATOS

Public Class form_gestion_apertura
    Public idMedicamentoPaq As Integer
    Public idMedicamentoUnid As Integer
    Private Function medicamento(ByVal idMedicamento As Integer) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " & idMedicamento.ToString & ";"
                sSql += " SELECT M.ID_MEDICAMENTO,M.NOMBRE,TM.DESCRIPCION AS FORMA_FARMACEUTICA,M.MARCA,M.LABORATORIO, 
        sSql += " M.DESCRIPCION AS COMPOSICION,M.PRECIO_UNITARIO, ISNULL(SUM (TOTAL),0) + ISNULL(V.TOTAL_VENTA,0) AS STOCK  "
        sSql += " FROM MEDICAMENTO M INNER JOIN TIPO_MEDICAMENTO TM "
        sSql += " ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO LEFT JOIN "
        sSql += " ( SELECT ID_MEDICAMENTO,CAST(MONTH(F_VENCIMIENTO) AS NVARCHAR(2)) + '/' + CAST(YEAR(F_VENCIMIENTO) AS NVARCHAR(4)) "
        sSql += " AS PERIODO_VENCIMIENTO, F_VENCIMIENTO, SUM(CANTIDAD) AS TOTAL FROM INVENTARIO  "
        sSql += " WHERE ID_COMPRA IS NULL AND ID_VENTA IS NULL"
        sSql += " GROUP BY ID_MEDICAMENTO, MONTH(F_VENCIMIENTO),YEAR(F_VENCIMIENTO),F_VENCIMIENTO  UNION ALL  "
        sSql += " SELECT ID_MEDICAMENTO, CAST(MONTH(F_VENCIMIENTO) AS NVARCHAR(2)) + '/' + CAST(YEAR(F_VENCIMIENTO) AS NVARCHAR(4)) "
        sSql += " AS PERIODO_VENCIMIENTO, I.F_VENCIMIENTO, SUM(CANTIDAD) AS TOTAL  FROM COMPRA_MEDICAMENTO CM "
        sSql += " INNER JOIN INVENTARIO I ON I.ID_COMPRA = CM.ID_COMPRA "
        sSql += " GROUP BY ID_MEDICAMENTO, FECHA_COMPRA, MONTH(F_VENCIMIENTO),YEAR(F_VENCIMIENTO),F_VENCIMIENTO  )  TBL "
        sSql += " ON TBL.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " LEFT JOIN ( SELECT ID_MEDICAMENTO, SUM (CANTIDAD) AS TOTAL_VENTA FROM VENTA V INNER JOIN INVENTARIO I "
        sSql += " ON I.ID_VENTA = V.ID_VENTA GROUP BY ID_MEDICAMENTO ) V ON V.ID_MEDICAMENTO = TBL.ID_MEDICAMENTO  "
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO  "
        sSql += " GROUP BY M.ID_MEDICAMENTO,V.TOTAL_VENTA,M.NOMBRE,TM.DESCRIPCION,M.MARCA,M.LABORATORIO,M.PRECIO_UNITARIO,M.DESCRIPCION"
        Return New datos().ejecutarConsulta(sSql)
    End Function

  
    Private Sub btnBuscarMedPaq_Click(sender As Object, e As EventArgs) Handles btnBuscarMedPaq.Click
        Using nuevo_pedido = New form_buscar_medicamento(4)
            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                Dim tblConsulta = medicamento(idMedicamentoPaq)
                txtMedicamento.Text = tblConsulta.Rows(0)("NOMBRE")
                txtFormaFarmaceutica.Text = tblConsulta.Rows(0)("FORMA_FARMACEUTICA")
                txtLaboratorio.Text = tblConsulta.Rows(0)("LABORATORIO")
                txtMarca.Text = tblConsulta.Rows(0)("MARCA")
                txtComposicion.Text = tblConsulta.Rows(0)("COMPOSICION")
                txtCantidad.Text = tblConsulta.Rows(0)("STOCK")
                txtPrecio.Text = tblConsulta.Rows(0)("PRECIO_UNITARIO")
                btnAperturar.Enabled = True
            End If
        End Using
    End Sub

    Private Sub btnBuscarMedUnid_Click(sender As Object, e As EventArgs) Handles btnBuscarMedUnid.Click
        Using nuevo_pedido = New form_buscar_medicamento(5)
            If DialogResult.OK = nuevo_pedido.ShowDialog() Then
                Dim tblConsulta = medicamento(idMedicamentoUnid)
                txtMedicamentoUnid.Text = tblConsulta.Rows(0)("NOMBRE")
                txtFormaFarmUnid.Text = tblConsulta.Rows(0)("FORMA_FARMACEUTICA")
                txtLaboratorioUnid.Text = tblConsulta.Rows(0)("LABORATORIO")
                txtMarcaUnid.Text = tblConsulta.Rows(0)("MARCA")
                txtComposicionUnid.Text = tblConsulta.Rows(0)("COMPOSICION")
                txtCantidadUnid.Text = tblConsulta.Rows(0)("STOCK")
                btnAceptar.Enabled = True
            End If
        End Using
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAperturar.Click
        If validarCantidadPaquetes() Then
            Using apertura = New form_aperturar()
                If DialogResult.OK = apertura.ShowDialog() Then
                    btnAperturar.Enabled = False
                End If
            End Using

        End If
    End Sub

    Function validarCantidadPaquetes()
        Dim valido As Boolean = True
        If Decimal.Parse(txtCantidad.Text) <= 0 Then
            ErrorProvider1.SetError(txtCantidad, "La cantidad de paquetes en stock es 0")
            valido = False
        Else
            ErrorProvider1.SetError(txtCantidad, "")
        End If
        Return valido
    End Function
    Function validarCantidadUnidades()
        Dim valido As Boolean = True
        If Decimal.Parse(txtCantidadAper.Text) <= 0 Then
            ErrorProvider1.SetError(txtCantidadAper, "La cantidad de unidades aperturadas no puede ser 0")
            valido = False
        Else
            ErrorProvider1.SetError(txtCantidadAper, "")
        End If
        Return valido
    End Function
    Function validarApertura()
        Dim valido As Boolean = True
        If Decimal.Parse(lblCantidadAperturado.Text) = 0 Then
            ErrorProvider1.SetError(txtCantidad, "La cantidad de paquetes aperturados no puede ser 0")
            valido = False
        Else
            ErrorProvider1.SetError(txtCantidad, "")
        End If
        Return valido
    End Function

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarApertura() And validarCantidadUnidades() And validarMedicamento() Then
            Dim tInv = New datos()
            tInv.tabla = "INVENTARIO"
            tInv.campoID = "ID_INVENTARIO"
            tInv.modoID = "auto"
            tInv.agregarCampoValor("ID_MEDICAMENTO", idMedicamentoPaq)
            tInv.agregarCampoValor("CANTIDAD", CInt(lblCantidadAperturado.Text) * (-1))
            tInv.agregarCampoValor("F_VENCIMIENTO", txt_fecha_vencimiento.Text)
            tInv.agregarCampoValor("CONTROL_FECHA", 1)
            tInv.agregarCampoValor("AJUSTES", 1)
            tInv.agregarCampoValor("F_AJUSTE", "serverDateTime")
            tInv.agregarCampoValor("OBSERVACION", "Apertura de Paquete")
            tInv.insertar()
            tInv.reset()
            tInv.tabla = "INVENTARIO"
            tInv.campoID = "ID_INVENTARIO"
            tInv.modoID = "auto"
            tInv.agregarCampoValor("ID_MEDICAMENTO", idMedicamentoUnid)
            tInv.agregarCampoValor("CANTIDAD", CInt(txtCantidadUnid.Text))
            tInv.agregarCampoValor("F_VENCIMIENTO", txt_fecha_vencimiento.Text)
            tInv.agregarCampoValor("CONTROL_FECHA", 1)
            tInv.agregarCampoValor("AJUSTES", 1)
            tInv.agregarCampoValor("F_AJUSTE", "serverDateTime")
            tInv.agregarCampoValor("OBSERVACION", "Apertura de Paquete")
            tInv.insertar()
            lblCantidadAperturado.Text = "0"
            txtCantidadUnid.Text = "0"

            Dim tblConsulta = medicamento(idMedicamentoPaq)
            txtMedicamento.Text = tblConsulta.Rows(0)("NOMBRE")
            txtFormaFarmaceutica.Text = tblConsulta.Rows(0)("FORMA_FARMACEUTICA")
            txtLaboratorio.Text = tblConsulta.Rows(0)("LABORATORIO")
            txtMarca.Text = tblConsulta.Rows(0)("MARCA")
            txtComposicion.Text = tblConsulta.Rows(0)("COMPOSICION")
            txtCantidad.Text = tblConsulta.Rows(0)("STOCK")
            txtPrecio.Text = tblConsulta.Rows(0)("PRECIO_UNITARIO")
            btnAperturar.Enabled = True

            tblConsulta = medicamento(idMedicamentoUnid)
            txtMedicamentoUnid.Text = tblConsulta.Rows(0)("NOMBRE")
            txtFormaFarmUnid.Text = tblConsulta.Rows(0)("FORMA_FARMACEUTICA")
            txtLaboratorioUnid.Text = tblConsulta.Rows(0)("LABORATORIO")
            txtMarcaUnid.Text = tblConsulta.Rows(0)("MARCA")
            txtComposicionUnid.Text = tblConsulta.Rows(0)("COMPOSICION")
            txtCantidadUnid.Text = tblConsulta.Rows(0)("STOCK")
            txtCantidadAper.Text = "0"
            Dim conf = New modulo()
            conf.mensaje("Apertura", "Se realizo la apertura con exito.!!")
        End If
    End Sub
    Function validarMedicamento()
        Dim valido As Boolean = True
        If txtMedicamentoUnid.Text.Length = 0 Then
            ErrorProvider1.SetError(txtMedicamentoUnid, "No existe Medicamento Seleccionado")
            valido = False
        Else
            ErrorProvider1.SetError(txtMedicamento, "")
        End If
        Return valido
    End Function
End Class