﻿Public Class form_aperturar

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click        
        form_gestion_apertura.txtCantidadAper.Text = txtCantUnid.Text
        form_gestion_apertura.lblCantidadAperturado.Text = txtCantAper.Text
        form_gestion_apertura.txt_fecha_vencimiento.Text = txtFechaVencimiento.Text
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub chkControlFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkControlFecha.CheckedChanged
        txtFechaVencimiento.Enabled = CType(sender, CheckBox).Checked
    End Sub
End Class