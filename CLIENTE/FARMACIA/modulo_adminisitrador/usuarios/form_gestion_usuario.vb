﻿Imports CAPA_NEGOCIO

Public Class form_gestion_usuario
    Private _idUsuario As Integer = 0
    Private mod_conf As modulo = New modulo()
    Private tusuario As eUsuario = New eUsuario()
    Public Sub New(ByVal idUsuario As Integer)
        InitializeComponent()
        _idUsuario = idUsuario        
        tUsuario.ID_USUARIO = idUsuario
        tUsuario.cargarDatos()
        cargarDatos()
        btnHabilitarBoton.Visible = True
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub
    Private Sub cargarDatos()
        txtNombre.Text = tusuario.NOMBRE_APELLIDOS
        txtUsuario.Text = tUsuario.USUARIO
        chkActivo.Checked = CBool(tUsuario.HABILITADO)
        chkUsuarioAdmin.Checked = CBool(tUsuario.ADMINISTRADOR)
        chkObligaCambio.Checked = CBool(tusuario.CAMBIO_CONTRASENHA)
        txtContrasenha.Text = tusuario.CONTRASENHA
        txtContrasenha.ReadOnly = True
        txtComision.Text = tusuario.COMISION
        txtContrasenha2.Text = tusuario.CONTRASENHA
        txtContrasenha2.ReadOnly = True
        chkDescuento.Checked = CBool(tusuario.DESCUENTO)
    End Sub

    Private Sub btnVolver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVolver.Click
        Me.Close()
        mod_conf.volver(New form_usuarios())
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            tusuario.NOMBRE_APELLIDOS = txtNombre.Text
            tusuario.USUARIO = txtUsuario.Text
            tusuario.CONTRASENHA = txtContrasenha2.Text
            tusuario.ADMINISTRADOR = chkUsuarioAdmin.Checked
            tusuario.CAMBIO_CONTRASENHA = chkObligaCambio.Checked
            tusuario.HABILITADO = chkActivo.Checked
            tusuario.COMISION = txtComision.Text
            tusuario.DESCUENTO = chkDescuento.Checked
            If _idUsuario = 0 Then
                tusuario.insertar()
            Else
                tusuario.ID_USUARIO = _idUsuario
                tusuario.modificar()
            End If
            If tusuario.mensajesError = "" Then
                Me.Close()
                mod_conf.volver(New form_usuarios())
            Else
                mod_conf.mensajeError("Error Usuario", tusuario.mensajesError)
            End If
        End If
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If

        If Trim(txtUsuario.Text).Length > 0 Then
            ErrorProvider1.SetError(txtUsuario, "")
        Else
            mensaje = "Complete el campo Usuario."
            ErrorProvider1.SetError(txtUsuario, mensaje)
            valido = False
        End If

        If Trim(txtContrasenha.Text).Length > 0 Then
            ErrorProvider1.SetError(txtContrasenha, "")
        Else
            mensaje = "Complete el campo Contraseña."
            ErrorProvider1.SetError(txtContrasenha, mensaje)
            valido = False
        End If

        If txtContrasenha.Text <> txtContrasenha2.Text Then
            mensaje = "Las contraseñas ingresadas no coinciden."
            ErrorProvider1.SetError(txtContrasenha2, mensaje)
            valido = False
        Else
            ErrorProvider1.SetError(txtContrasenha2, "")
        End If
        If Not (txtComision.Text.Length > 0 And IsNumeric(txtComision.Text)) Then
            mensaje = "Ingrese comision valida."
            ErrorProvider1.SetError(txtComision, mensaje)
            valido = False
        Else
            ErrorProvider1.SetError(txtComision, "")
        End If
        Return valido
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnHabilitarBoton.Click
        txtContrasenha.ReadOnly = False
        txtContrasenha2.ReadOnly = False
        txtContrasenha.Text = ""
        txtContrasenha2.Text = ""
    End Sub

    Private Sub form_gestion_usuario_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtNombre.Focus()
    End Sub
End Class