﻿Imports CAPA_NEGOCIO
Imports System.Data
Public Class form_usuarios
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 10

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1320
            .Height = 450
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 7
            .Columns(0).Name = "Registro"
            .Columns(0).HeaderText = "Registro"
            .Columns(0).DataPropertyName = "ID_USUARIO"
            .Columns(0).Width = 90

            .Columns(1).Name = "usuario"
            .Columns(1).HeaderText = "Usuario"
            .Columns(1).DataPropertyName = "USUARIO"
            .Columns(1).Width = 200

            .Columns(2).Name = "Nombre"
            .Columns(2).HeaderText = "Nombre"
            .Columns(2).DataPropertyName = "NOMBRE_APELLIDOS"
            .Columns(2).Width = 500

            .Columns(3).Name = "Celular"
            .Columns(3).HeaderText = "Nro. Celular"
            .Columns(3).DataPropertyName = "CELULAR"
            .Columns(3).Width = 150

            .Columns(4).Name = "Tipo_Usuario"
            .Columns(4).HeaderText = "Tipo de Usuario"
            .Columns(4).DataPropertyName = "TIPO_USUARIO"
            .Columns(4).Width = 130

            .Columns(5).Name = "Comision"
            .Columns(5).HeaderText = "Comision"
            .Columns(5).DataPropertyName = "COMISION"
            .Columns(5).Width = 130

            .Columns(6).Name = "Descuento"
            .Columns(6).HeaderText = "Aplica Descuento"
            .Columns(6).DataPropertyName = "APLICA_DESCUENTO"
            .Columns(6).Width = 120
        End With
        LoadPage()
    End Sub

    Private Sub form_usuarios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tblUsuarios As eUsuario = New eUsuario()
        dtSource = tblUsuarios.mostrarUsuario()
        inicializarParametros()
        txtBuscar.Focus()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New form_gestion_usuario()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim tblUsuarios = New eUsuario()
        dtSource = tblUsuarios.mostrarUsuario(txtBuscar.Text)
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick
        Dim idUsuario As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        Me.Close()
        Dim form_gestion As form_gestion_usuario = New form_gestion_usuario(idUsuario)
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If dtSource.Rows.Count > 0 Then
            Dim id_usuario As Integer = 0
            id_usuario = dgvGrilla.SelectedCells(0).Value
            If id_usuario <> form_sesion.cusuario.ID_USUARIO Then
                Dim tUsuario As eUsuario = New eUsuario()
                tUsuario.ID_USUARIO = id_usuario
                tUsuario.eliminar()
                If tUsuario.mensajesError = "" Then
                    dtSource = tUsuario.mostrarUsuario(txtBuscar.Text)
                    Me.totalRegistro = dtSource.Rows.Count
                    Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
                    Me.paginaActual = 1
                    Me.regNro = 0
                    LoadPage()
                Else
                    mod_conf.mensajeError("Error", tUsuario.mensajesError)
                End If

            Else
                mod_conf.mensajeError("Error", "No se puede eliminar el usuario que inicio sesion.")
            End If
        Else
            mod_conf.mensajeError("Error", "No existen registros para eliminar.")
        End If
        
    End Sub

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnBuscar_Click(Nothing, Nothing)
        End If
    End Sub
End Class