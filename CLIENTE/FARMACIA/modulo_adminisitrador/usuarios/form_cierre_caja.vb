﻿Imports CAPA_DATOS
Public Class form_cierre_caja
    Private mod_conf As modulo = New modulo()
    Private dtSource As DataTable
    Private paginaActual As Integer
    Private regNro As Integer
    Private totalRegistro As Integer
    Private totalPagina As Integer
    Private paginacion As Integer = 15

    Private idCaja As Integer

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql += " SELECT  U.ID_USUARIO ,NOMBRE_APELLIDOS FROM USUARIO U 
        sSql += " INNER JOIN BANCO B ON B.ID_USUARIO = U.ID_USUARIO "
        sSql += " WHERE B.ID_TIPO_BANCO = 1"
        sSql += " UNION ALL SELECT -1, '[Todos los Usuarios]'"
        sSql += " ORDER BY NOMBRE_APELLIDOS "


        cmbUsuario.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        cmbUsuario.ValueMember = "ID_USUARIO"
    End Sub

    Private Sub DisplayPageInfo()
        txtDisplayPageNo.Text = "Pagina " & Me.paginaActual.ToString & "/ " & Me.totalPagina.ToString
    End Sub
    Private Sub LoadPage()
        Dim i As Integer
        Dim startRec As Integer
        Dim endRec As Integer
        Dim dtTemp As DataTable
        'Duplicate or clone the source table to create the temporary table.
        dtTemp = dtSource.Clone
        If dtSource.Rows.Count > 0 Then
            If Me.paginaActual = Me.totalPagina Then
                endRec = dtSource.Rows.Count
            Else
                endRec = Me.paginaActual * (Me.paginacion)
            End If
            startRec = regNro
            'Copy the rows from the source table to fill the temporary table.
            For i = startRec To endRec - 1
                dtTemp.ImportRow(dtSource.Rows(i))
                regNro += 1
            Next
        End If
        dgvGrilla.DataSource = dtTemp
        DisplayPageInfo()
    End Sub
    Private Sub inicializarParametros()
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0
        With (dgvGrilla)
            .Width = 1200

            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False

            Dim nombre_apellidos As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(nombre_apellidos)
            With nombre_apellidos
                .Name = "nombre_apellidos"
                .HeaderText = "Usuario"
                .DataPropertyName = "NOMBRE_APELLIDOS"
                .Width = 180
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fecha)
            With fecha
                .Name = "F_APERTURA"
                .HeaderText = "F. Apertura"
                .DataPropertyName = "F_APERTURA"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim fechaCierre As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fechaCierre)
            With fechaCierre
                .Name = "F_CIERRE"
                .HeaderText = "F. Cierre"
                .DataPropertyName = "F_CIERRE"
                .Width = 90
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim credito As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(credito)
            With credito
                .Name = "credito"
                .HeaderText = "Ventas al Credito"
                .DataPropertyName = "TOTAL_CREDITO"
                .Width = 130
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim efectivo As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(efectivo)
            With efectivo
                .Name = "efectivo"
                .HeaderText = "Ventas en Efectivo"
                .DataPropertyName = "TOTAL_EFECTIVO"
                .Width = 130
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            

            Dim apertura As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(apertura)
            With apertura
                .Name = "IMPORTE_APERTURA"
                .HeaderText = "Total Apertura"
                .DataPropertyName = "IMPORTE_APERTURA_MN"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim movimiento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(movimiento)
            With movimiento
                .Name = "otros_ingresos"
                .HeaderText = "Cuentas por Cobrar"
                .DataPropertyName = "TOTAL_CTA_X_COBRAR"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim transferencia As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(transferencia)
            With transferencia
                .Name = "transferencia"
                .HeaderText = "Transferencias"
                .DataPropertyName = "TOTAL_TRANSFERENCIA"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim cierre As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(cierre)
            With cierre
                .Name = "IMPORTE_CIERRE"
                .HeaderText = "Total Cierre"
                .DataPropertyName = "IMPORTE_CIERRE_MN"
                .Width = 120
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton                
                .HeaderText = ""                
                .Name = "print"            '
                .Width = 100
                .Frozen = False
                .Text = "Imprimir"
                .UseColumnTextForButtonValue = True
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID_CAJA"
            End With
           
        End With
        LoadPage()
    End Sub

    Private Sub form_cierre_caja_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarComboLaboratorio()
        dtSource = mostrarDatos(cmbUsuario.SelectedValue)
        inicializarParametros()

    End Sub
    Function mostrarDatos(ByVal idUsuario As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @USUARIO AS INTEGER "
        sSql += " SET @USUARIO = " + idUsuario.ToString + ";"
        sSql += " SELECT CJ.ID_CAJA,U.NOMBRE_APELLIDOS,CONVERT(NVARCHAR(10),CJ.F_APERTURA,103) AS F_APERTURA,"
        sSql += " ISNULL(CONVERT(NVARCHAR(10),CJ.F_CIERRE,103),'')AS F_CIERRE,"
        sSql += " CJ.IMPORTE_APERTURA_MN,ISNULL(CJ.IMPORTE_CIERRE_MN,0) AS IMPORTE_CIERRE_MN, ISNULL(VE.TOTAL_EFECTIVO,0) AS TOTAL_EFECTIVO,"
        sSql += " ISNULL(VC.TOTAL_CREDITO,0) AS TOTAL_CREDITO,"
        sSql += " ISNULL(MT.TOTAL_TRANSFERENCIA  ,0) AS TOTAL_TRANSFERENCIA,"
        sSql += " ISNULL(CC.TOTAL_CTA_X_COBRAR    ,0) AS TOTAL_CTA_X_COBRAR,ISNULL(UR.NOMBRE_APELLIDOS,'') AS USUARIO_RECEPCION "
        sSql += " FROM CAJA CJ "
        sSql += " INNER JOIN BANCO B ON B.ID_BANCO = CJ.ID_BANCO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = CJ.ID_USUARIO "
        sSql += " LEFT  JOIN CIERRE CR ON CR.ID_CAJA = CJ.ID_CAJA"
        sSql += " LEFT JOIN USUARIO UR ON UR.ID_USUARIO = CR.ID_USUARIO_REC"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA,SUM(V.TOTAL_PEDIDO) AS TOTAL_EFECTIVO FROM VENTA V "
        sSql += " WHERE V.CREDITO = 0 AND ANULADA = 0"
        sSql += " GROUP BY ID_CAJA"
        sSql += " ) VE ON VE.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA,SUM(V.TOTAL_PEDIDO) AS TOTAL_CREDITO FROM VENTA V "
        sSql += " WHERE V.CREDITO = 1 AND ANULADA = 0 "
        sSql += " GROUP BY ID_CAJA"
        sSql += " ) VC ON VC.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(IMPORTE) AS TOTAL_TRANSFERENCIA FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO = 5 "
        sSql += " GROUP BY ID_CAJA"
        sSql += " )MT ON MT.ID_CAJA = CJ.ID_CAJA "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_CAJA, SUM(TOTAL_PAGO) AS TOTAL_CTA_X_COBRAR FROM PAGOS"
        sSql += " GROUP BY ID_CAJA"
        sSql += " )CC ON CC.ID_CAJA = CJ.ID_CAJA "
        sSql += " WHERE CJ.ID_USUARIO = CASE WHEN @USUARIO = -1 THEN CJ.ID_USUARIO ELSE @USUARIO END"
        sSql += " ORDER BY F_APERTURA DESC"

        Return New datos().ejecutarConsulta(sSql)
    End Function

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Dim form = New visualizador_reporte(6, dgvGrilla.SelectedCells(4).Value.ToString)
        form.Show()
    End Sub

    Private Sub btnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguiente.Click
        Me.paginaActual += 1
        If (Me.paginaActual > (Me.totalPagina)) Then
            Me.paginaActual = Me.totalPagina
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnterior.Click
        Me.paginaActual -= 1
        If (Me.paginaActual = 0) Then
            Me.paginaActual = 1
        End If
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnFinal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinal.Click
        Me.paginaActual = Me.totalPagina
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        Me.paginaActual = 1
        regNro = (paginaActual - 1) * paginacion
        LoadPage()
    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick

        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name

            Case "print"
                idCaja = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value
                imprimirCierre()
        End Select
    End Sub
    Private Sub imprimirCierre()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir()
        print.imprimirCierre(e.Graphics, idCaja)
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        dtSource = mostrarDatos(cmbUsuario.SelectedValue)
        Me.totalRegistro = dtSource.Rows.Count
        Me.totalPagina = Math.Ceiling(Me.totalRegistro / Me.paginacion)
        Me.paginaActual = 1
        Me.regNro = 0

        LoadPage()
    End Sub
End Class