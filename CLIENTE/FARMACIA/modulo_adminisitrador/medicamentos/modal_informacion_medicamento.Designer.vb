﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modal_informacion_medicamento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(modal_informacion_medicamento))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPrecioVenta = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtControlado = New System.Windows.Forms.TextBox()
        Me.txtMinimo = New System.Windows.Forms.TextBox()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.txtComposicion = New System.Windows.Forms.TextBox()
        Me.txtLaboratorio = New System.Windows.Forms.TextBox()
        Me.txtFormaFarmaceutica = New System.Windows.Forms.TextBox()
        Me.txtConcentracion = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMedicamento = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCodigoBarra = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtRegistro = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtRegSistema = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtAccion = New System.Windows.Forms.TextBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtPrecioPaquete = New System.Windows.Forms.TextBox()
        Me.txtCantidadPaquete = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Green
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(634, 31)
        Me.Panel1.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(152, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(286, 26)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Informacion Medicamento"
        '
        'txtPrecioVenta
        '
        Me.txtPrecioVenta.Location = New System.Drawing.Point(121, 285)
        Me.txtPrecioVenta.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPrecioVenta.Name = "txtPrecioVenta"
        Me.txtPrecioVenta.ReadOnly = True
        Me.txtPrecioVenta.Size = New System.Drawing.Size(58, 20)
        Me.txtPrecioVenta.TabIndex = 143
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(11, 289)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 13)
        Me.Label12.TabIndex = 142
        Me.Label12.Text = "Precio de Venta:"
        '
        'txtControlado
        '
        Me.txtControlado.Location = New System.Drawing.Point(443, 287)
        Me.txtControlado.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtControlado.Name = "txtControlado"
        Me.txtControlado.ReadOnly = True
        Me.txtControlado.Size = New System.Drawing.Size(34, 20)
        Me.txtControlado.TabIndex = 141
        '
        'txtMinimo
        '
        Me.txtMinimo.Location = New System.Drawing.Point(404, 261)
        Me.txtMinimo.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMinimo.Name = "txtMinimo"
        Me.txtMinimo.ReadOnly = True
        Me.txtMinimo.Size = New System.Drawing.Size(107, 20)
        Me.txtMinimo.TabIndex = 140
        '
        'txtMarca
        '
        Me.txtMarca.Location = New System.Drawing.Point(121, 262)
        Me.txtMarca.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.ReadOnly = True
        Me.txtMarca.Size = New System.Drawing.Size(197, 20)
        Me.txtMarca.TabIndex = 139
        '
        'txtComposicion
        '
        Me.txtComposicion.Location = New System.Drawing.Point(121, 183)
        Me.txtComposicion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtComposicion.Name = "txtComposicion"
        Me.txtComposicion.ReadOnly = True
        Me.txtComposicion.Size = New System.Drawing.Size(498, 20)
        Me.txtComposicion.TabIndex = 138
        '
        'txtLaboratorio
        '
        Me.txtLaboratorio.Location = New System.Drawing.Point(121, 159)
        Me.txtLaboratorio.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtLaboratorio.Name = "txtLaboratorio"
        Me.txtLaboratorio.ReadOnly = True
        Me.txtLaboratorio.Size = New System.Drawing.Size(254, 20)
        Me.txtLaboratorio.TabIndex = 137
        '
        'txtFormaFarmaceutica
        '
        Me.txtFormaFarmaceutica.Location = New System.Drawing.Point(121, 136)
        Me.txtFormaFarmaceutica.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtFormaFarmaceutica.Name = "txtFormaFarmaceutica"
        Me.txtFormaFarmaceutica.ReadOnly = True
        Me.txtFormaFarmaceutica.Size = New System.Drawing.Size(254, 20)
        Me.txtFormaFarmaceutica.TabIndex = 136
        '
        'txtConcentracion
        '
        Me.txtConcentracion.Location = New System.Drawing.Point(121, 112)
        Me.txtConcentracion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtConcentracion.Name = "txtConcentracion"
        Me.txtConcentracion.ReadOnly = True
        Me.txtConcentracion.Size = New System.Drawing.Size(254, 20)
        Me.txtConcentracion.TabIndex = 135
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(331, 289)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(107, 13)
        Me.Label11.TabIndex = 134
        Me.Label11.Text = "Producto Controlado:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(330, 265)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 13)
        Me.Label9.TabIndex = 133
        Me.Label9.Text = "Stock Minimo:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(11, 265)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 132
        Me.Label8.Text = "Marca:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 185)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 13)
        Me.Label7.TabIndex = 131
        Me.Label7.Text = "Composicion:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 161)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 130
        Me.Label5.Text = "Laboratorio:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 138)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 13)
        Me.Label4.TabIndex = 129
        Me.Label4.Text = "Presentacion:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 114)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 128
        Me.Label3.Text = "Concentracion:"
        '
        'txtMedicamento
        '
        Me.txtMedicamento.Location = New System.Drawing.Point(121, 87)
        Me.txtMedicamento.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMedicamento.Name = "txtMedicamento"
        Me.txtMedicamento.ReadOnly = True
        Me.txtMedicamento.Size = New System.Drawing.Size(390, 20)
        Me.txtMedicamento.TabIndex = 127
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 90)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 126
        Me.Label1.Text = "Medicamento:"
        '
        'txtCodigoBarra
        '
        Me.txtCodigoBarra.Location = New System.Drawing.Point(322, 63)
        Me.txtCodigoBarra.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtCodigoBarra.Name = "txtCodigoBarra"
        Me.txtCodigoBarra.ReadOnly = True
        Me.txtCodigoBarra.Size = New System.Drawing.Size(297, 20)
        Me.txtCodigoBarra.TabIndex = 125
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(231, 65)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 124
        Me.Label6.Text = "Codigo de Barra:"
        '
        'txtRegistro
        '
        Me.txtRegistro.Location = New System.Drawing.Point(121, 63)
        Me.txtRegistro.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.ReadOnly = True
        Me.txtRegistro.Size = New System.Drawing.Size(106, 20)
        Me.txtRegistro.TabIndex = 122
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 65)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 123
        Me.Label10.Text = "Registro:"
        '
        'txtRegSistema
        '
        Me.txtRegSistema.Location = New System.Drawing.Point(121, 37)
        Me.txtRegSistema.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtRegSistema.Name = "txtRegSistema"
        Me.txtRegSistema.ReadOnly = True
        Me.txtRegSistema.Size = New System.Drawing.Size(58, 20)
        Me.txtRegSistema.TabIndex = 145
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 39)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 13)
        Me.Label13.TabIndex = 144
        Me.Label13.Text = "Reg. en Sistema"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(11, 213)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 148
        Me.Label15.Text = "Accion:"
        '
        'txtAccion
        '
        Me.txtAccion.Location = New System.Drawing.Point(121, 209)
        Me.txtAccion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtAccion.Multiline = True
        Me.txtAccion.Name = "txtAccion"
        Me.txtAccion.ReadOnly = True
        Me.txtAccion.Size = New System.Drawing.Size(498, 46)
        Me.txtAccion.TabIndex = 149
        '
        'btnVolver
        '
        Me.btnVolver.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnVolver.Image = CType(resources.GetObject("btnVolver.Image"), System.Drawing.Image)
        Me.btnVolver.Location = New System.Drawing.Point(547, 332)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 32)
        Me.btnVolver.TabIndex = 151
        Me.btnVolver.Text = "&Volver"
        Me.btnVolver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(11, 314)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(98, 13)
        Me.Label16.TabIndex = 153
        Me.Label16.Text = "Precio de Paquete:"
        '
        'txtPrecioPaquete
        '
        Me.txtPrecioPaquete.Location = New System.Drawing.Point(121, 311)
        Me.txtPrecioPaquete.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPrecioPaquete.Name = "txtPrecioPaquete"
        Me.txtPrecioPaquete.ReadOnly = True
        Me.txtPrecioPaquete.Size = New System.Drawing.Size(58, 20)
        Me.txtPrecioPaquete.TabIndex = 154
        '
        'txtCantidadPaquete
        '
        Me.txtCantidadPaquete.Location = New System.Drawing.Point(285, 311)
        Me.txtCantidadPaquete.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtCantidadPaquete.Name = "txtCantidadPaquete"
        Me.txtCantidadPaquete.ReadOnly = True
        Me.txtCantidadPaquete.Size = New System.Drawing.Size(32, 20)
        Me.txtCantidadPaquete.TabIndex = 155
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(186, 314)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(95, 13)
        Me.Label17.TabIndex = 156
        Me.Label17.Text = "Cantidad Paquete:"
        '
        'modal_informacion_medicamento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(634, 376)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtCantidadPaquete)
        Me.Controls.Add(Me.txtPrecioPaquete)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.txtAccion)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtRegSistema)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtPrecioVenta)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtControlado)
        Me.Controls.Add(Me.txtMinimo)
        Me.Controls.Add(Me.txtMarca)
        Me.Controls.Add(Me.txtComposicion)
        Me.Controls.Add(Me.txtLaboratorio)
        Me.Controls.Add(Me.txtFormaFarmaceutica)
        Me.Controls.Add(Me.txtConcentracion)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtMedicamento)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCodigoBarra)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtRegistro)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "modal_informacion_medicamento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medicamento"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtControlado As System.Windows.Forms.TextBox
    Friend WithEvents txtMinimo As System.Windows.Forms.TextBox
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents txtComposicion As System.Windows.Forms.TextBox
    Friend WithEvents txtLaboratorio As System.Windows.Forms.TextBox
    Friend WithEvents txtFormaFarmaceutica As System.Windows.Forms.TextBox
    Friend WithEvents txtConcentracion As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMedicamento As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtRegistro As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtRegSistema As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtAccion As System.Windows.Forms.TextBox
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioPaquete As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadPaquete As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
End Class
