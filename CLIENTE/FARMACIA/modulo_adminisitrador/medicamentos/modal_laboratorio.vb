﻿Imports CAPA_DATOS
Public Class modal_laboratorio
    Private _idLaboratorio As Integer = 0
    Private mod_conf As modulo = New modulo()
    Private tipo As Integer

    Public Sub New(ByVal idLaboratorio As Integer)
        InitializeComponent()
        _idLaboratorio = idLaboratorio
        Dim sSql As String = "SELECT * FROM LABORATORIO WHERE ID_LABORATORIO = " + idLaboratorio.ToString
        cargarDatos(New datos().ejecutarConsulta(sSql))
    End Sub
    Public Sub New(ByVal laboratorio As String, Optional _tipo As Integer = 1)
        InitializeComponent()
        txtNombre.Text = laboratorio
        tipo = _tipo
    End Sub
    Private Sub cargarDatos(ByVal tbl As DataTable)
        txtNombre.Text = tbl.Rows(0)("NOMBRE")
    End Sub
    Public Sub New()
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub


    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
         Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
            Dim tProveedor = New datos()
            tProveedor.tabla = "LABORATORIO"
            tProveedor.campoID = "ID_LABORATORIO"
            tProveedor.modoID = "auto"
            tProveedor.agregarCampoValor("DESCRIPCION", txtNombre.Text)
            If _idLaboratorio = 0 Then
                tProveedor.insertar()
                _idLaboratorio = tProveedor.valorID
            Else
                tProveedor.valorID = _idLaboratorio
                tProveedor.modificar()
            End If
            If tipo = 2 Then
                form_relacionVademecum.idLaboratorio = _idLaboratorio
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If

    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub form_gestionCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnAceptar_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub form_gestionCliente_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If Keys.Escape = e.KeyCode Then
            btnVolver_Click(Nothing, Nothing)
        End If
    End Sub
End Class