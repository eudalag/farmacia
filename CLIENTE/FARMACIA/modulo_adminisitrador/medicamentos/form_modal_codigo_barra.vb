﻿Imports CAPA_DATOS

Public Class form_modal_codigo_barra

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarFormulario() Then
            form_gestionMedicamento.nuevoItem(txtCodigoBarra.Text, 1, 1)
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub
    Function validarFormulario() As Boolean
        Dim mens = New modulo()
        If Trim(txtCodigoBarra.Text.Length) = 0 Then
            mens.mensaje("ERROR", "Debe ingresar un codigo de barra valido.")
            Return False
        End If
        If existeCodigoBarraGrilla(txtCodigoBarra.Text) Then
            mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya existe en la lista de codigos del Medicamento.")
            Return False
        End If
        If existeCodigoBarraBD(txtCodigoBarra.Text) Then
            mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya se encuentra asignado a otro Medicamento.")
            Return False
        End If
        Return True
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Private Function existeCodigoBarraGrilla(ByVal codBarra As String) As Boolean
        Dim foundRows() As DataRow
        foundRows = form_gestionMedicamento.tblCodigoBarra.Select("CODIGO_BARRA = '" + codBarra + "' AND ACCION <> 3")
        Return foundRows.Count > 0
    End Function

    Private Function existeCodigoBarraBD(ByVal codBarra As String) As Boolean
        Dim sSql As String = ""
        sSql += "SELECT COUNT(*) AS TOTAL FROM MEDICAMENTO M"
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " WHERE CB.CODIGO_BARRA = '" + codBarra + "'"
        Dim tblConsulta = New datos()
        Return CInt(tblConsulta.ejecutarConsulta(sSql).Rows(0)(0)) > 0
    End Function

    Private Sub form_modal_codigo_barra_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtCodigoBarra.Focus()
    End Sub
End Class