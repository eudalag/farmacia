﻿Imports CAPA_DATOS
Imports System.Transactions

Public Class form_relacionVademecum
    Private idMedicamentoVademecum As Integer
    Public Shared idMedicamento As Integer

    Public Shared idFormaFarmaceutica As Integer
    Public Shared idLaboratorio As Integer

    Private InString As String
    Private PistolaEnviando As Boolean = False


    Public Sub New(ByVal _idVademecum As Integer)
        InitializeComponent()
        idMedicamentoVademecum = _idVademecum
        iniciarComboLaboratorio()
        iniciarComboFormaFarmaceutica()
        CargarDatosMedicamentoVademecum()
    End Sub

    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_TIPO_MEDICAMENTO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM TIPO_MEDICAMENTO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Forma Farmaceutica]' ORDER BY DESCRIPCION"

        cmbFormaFarmaceutica.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbFormaFarmaceutica.DisplayMember = "DESCRIPCION"
        cmbFormaFarmaceutica.ValueMember = "ID_TIPO_MEDICAMENTO"
    End Sub
    Protected Sub CargarDatosMedicamentoVademecum()
        Dim tblDatos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamentoVademecum.ToString + ";"
        sSql += " SELECT  NOMBRE,'' AS CONCENTRACION, PRESENTACION AS FORMA_FARMACEUTICA, LABORATORIO, ACCION, COMPOSICION, '' AS MARCA"
        sSql += " FROM VADEMECUM WHERE ID_VADEMECUM = @ID_MEDICAMENTO"

        Dim tbl = tblDatos.ejecutarConsulta(sSql)
        txtMedicamentoVademecum.Text = tbl.Rows(0)("NOMBRE")
        txtConcentracionVademecum.Text = tbl.Rows(0)("CONCENTRACION")
        txtFormaFarmaceuticaVademecum.Text = tbl.Rows(0)("FORMA_FARMACEUTICA")
        txtLaboratorioVademecum.Text = tbl.Rows(0)("LABORATORIO")
        txtComposicionVademecum.Text = tbl.Rows(0)("COMPOSICION")
        txtMarcaVademecum.Text = tbl.Rows(0)("MARCA")
        txtAccionVademecum.Text = tbl.Rows(0)("ACCION")
    End Sub
    Protected Sub CargarDatosMedicamento()
        Dim tblDatos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + CInt(txtRegistro.Text).ToString + ";"

        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS NVARCHAR(10)),6) AS REGISTRO,ISNULL(CB.CODIGO_BARRA,'') AS COD_BARRA,"
        sSql += " M.NOMBRE, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, ID_TIPO_MEDICAMENTO,ID_LABORATORIO, ISNULL(ACCION,'') AS ACCION,"
        sSql += " M.COMPOSICION, M.MARCA, M.MINIMO,  M.CONTROLADO , M.PRECIO_UNITARIO,"
        sSql += " ISNULL(CAST (M.PRECIO_PAQUETE AS NVARCHAR(10)),'S/P') AS PRECIO_PAQUETE,ISNULL(CAST(M.CANTIDAD_PAQUETE AS NVARCHAR(4)),'S/C') AS CANTIDAD_PAQUETE  FROM MEDICAMENTO M"

        sSql += " LEFT JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO  "
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO;"

        Dim tbl = tblDatos.ejecutarConsulta(sSql)
        txtRegistro.Text = tbl.Rows(0)("REGISTRO")
        txtCodigoBarra.Text = tbl.Rows(0)("COD_BARRA")
        txtMedicamento.Text = tbl.Rows(0)("NOMBRE")
        txtConcentracion.Text = tbl.Rows(0)("CONCENTRACION")
        cmbFormaFarmaceutica.Text = tbl.Rows(0)("ID_TIPO_MEDICAMENTO")
        cmbLaboratorio.Text = tbl.Rows(0)("ID_LABORATORIO")
        txtComposicion.Text = tbl.Rows(0)("COMPOSICION")
        txtMarca.Text = tbl.Rows(0)("MARCA")
        txtStockMinimo.Text = tbl.Rows(0)("MINIMO")
        chkControlado.Checked = CBool(tbl.Rows(0)("CONTROLADO"))
        txtPrecioVenta.Text = tbl.Rows(0)("PRECIO_UNITARIO")
        txtAccion.Text = tbl.Rows(0)("ACCION")
        txtCantidadPaquete.Text = tbl.Rows(0)("CANTIDAD_PAQUETE")
        txtPrecioPaquete.Text = tbl.Rows(0)("PRECIO_PAQUETE")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'Cargar Informacion Vademecum -> Medicamento
        txtMedicamento.Text = txtMedicamentoVademecum.Text
        txtConcentracion.Text = txtConcentracionVademecum.Text
        txtComposicion.Text = txtComposicionVademecum.Text
        txtMarca.Text = txtMarcaVademecum.Text
        txtAccion.Text = txtAccionVademecum.Text
        lblFormaFarmaceutica.Text = txtFormaFarmaceuticaVademecum.Text
        lblLaboratorio.Text = txtLaboratorioVademecum.Text
        lblEstado.Text = "Nuevo" '
        cmbFormaFarmaceutica.SelectedText = txtFormaFarmaceuticaVademecum.Text
        cmbLaboratorio.SelectedText = txtLaboratorioVademecum.Text
    End Sub

    Private Function validarFormulario() As Boolean
        Dim valido As Boolean = True
        Dim mensaje As String = ""
        If Trim(txtMedicamento.Text).Length > 0 Then
            ErrorProvider1.SetError(txtMedicamento, "")
        Else
            mensaje = "Complete el campo Nombre del Medicamento."
            ErrorProvider1.SetError(txtMedicamento, mensaje)
            valido = False
        End If
        If cmbFormaFarmaceutica.SelectedValue > -1 Then
            ErrorProvider1.SetError(cmbFormaFarmaceutica, "")
        Else
            mensaje = "Seleccione una Forma Farmaceutica."
            ErrorProvider1.SetError(cmbFormaFarmaceutica, mensaje)
            valido = False
        End If

        If cmbLaboratorio.SelectedValue > -1 Then
            ErrorProvider1.SetError(cmbLaboratorio, "")
        Else
            mensaje = "Seleccione un laboratorio."
            ErrorProvider1.SetError(cmbLaboratorio, mensaje)
            valido = False
        End If
        If IsNumeric(txtPrecioVenta.Text) Then
            If Decimal.Parse(txtPrecioVenta.Text) > 0 Then
                ErrorProvider1.SetError(txtPrecioVenta, "")
            Else
                mensaje = "El importe de Venta no puede ser 0."
                ErrorProvider1.SetError(txtPrecioVenta, mensaje)
                valido = False
            End If
        Else
            mensaje = "Importe Invalido."
            ErrorProvider1.SetError(txtPrecioVenta, mensaje)
            valido = False
        End If
        Return valido
    End Function
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarFormulario() Then
            Using scope = New TransactionScope
                Dim tblDatos = New datos()
                tblDatos.tabla = "MEDICAMENTO"
                tblDatos.campoID = "ID_MEDICAMENTO"
                tblDatos.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                tblDatos.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                tblDatos.agregarCampoValor("MINIMO", 0)
                tblDatos.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbFormaFarmaceutica.SelectedValue)
                tblDatos.agregarCampoValor("ACTIVO", 1)
                tblDatos.agregarCampoValor("PRECIO_UNITARIO", txtPrecioVenta.Text)
                tblDatos.agregarCampoValor("OBSERVACION", "")
                tblDatos.agregarCampoValor("MARCA", txtMarca.Text)
                tblDatos.agregarCampoValor("CONTROLADO", chkControlado.Checked)
                tblDatos.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
                tblDatos.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                tblDatos.agregarCampoValor("PRECIO_PAQUETE", txtPrecioPaquete.Text)
                tblDatos.agregarCampoValor("PORC_UNITARIO", calcularPorcentajeUnitario(txtPrecioVenta.Text, txtPrecioPaquete.Text))
                tblDatos.agregarCampoValor("PORC_PAQUETE", calcularPorcentajeMayor(txtPrecioVenta.Text, txtPrecioPaquete.Text, txtCantidadPaquete.Text))
                tblDatos.agregarCampoValor("CANTIDAD_PAQUETE", txtCantidadPaquete.Text)
                tblDatos.agregarCampoValor("MAXIMO", 0)
                tblDatos.agregarCampoValor("ACCION", txtAccion.Text)
                tblDatos.insertar()
                idMedicamento = tblDatos.valorID

                tblDatos.reset()
                tblDatos.tabla = "VADEMECUM"
                tblDatos.campoID = "ID_VADEMECUM"
                tblDatos.valorID = idMedicamentoVademecum
                tblDatos.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
                tblDatos.modificar()
                If txtCodigoBarra.Text.Length > 0 Then
                    insertarCodigoBarra()
                End If
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End Using
        End If        
    End Sub
    Function calcularPorcentajeUnitario(ByVal precioVenta As Decimal, ByVal precioCompra As Decimal) As Decimal
        Dim porcentajeUnitario As Decimal = 0
        If precioCompra > 0 Then
            porcentajeUnitario = ((precioVenta * 100 / precioCompra) - 100)
        End If
        Return porcentajeUnitario
    End Function

    Function calcularPorcentajeMayor(ByVal precioVenta As Decimal, ByVal precioCompra As Decimal, ByVal cantidad As Integer) As Decimal
        Dim porcentajeMayor As Decimal = 0
        If precioCompra > 0 Then
            porcentajeMayor = (((precioVenta / cantidad) * 100 / precioCompra) - 100)
        End If
        Return porcentajeMayor
    End Function

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Using formFormaFarmaceutica = New modal_formaFarmaceutica(lblFormaFarmaceutica.Text, 1)
            If formFormaFarmaceutica.ShowDialog = Windows.Forms.DialogResult.OK Then
                iniciarComboFormaFarmaceutica()
                cmbFormaFarmaceutica.SelectedValue = idFormaFarmaceutica
            End If
        End Using
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Using formFormaFarmaceutica = New modal_laboratorio(lblLaboratorio.Text, 2)
            If formFormaFarmaceutica.ShowDialog = Windows.Forms.DialogResult.OK Then
                iniciarComboLaboratorio()
                cmbLaboratorio.SelectedValue = idLaboratorio
            End If
        End Using
    End Sub

    Private Sub form_relacionVademecum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                txtCodigoBarra.Text = InString
            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub
    Private Sub insertarCodigoBarra()
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @COD_BARRA AS NVARCHAR(25);"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @COD_BARRA = '" + txtCodigoBarra.Text + "';"
        sSql += " IF NOT EXISTS(SELECT *  FROM  CODIGO_BARRA  WHERE ID_MEDICAMENTO = @MEDICAMENTO AND CODIGO_BARRA = @COD_BARRA )  "
        sSql += " BEGIN "
        sSql += " INSERT CODIGO_BARRA VALUES (@COD_BARRA,@MEDICAMENTO)"
        sSql += " END"
        Dim tbl = New datos()
        tbl.ejecutarSQL(sSql)
    End Sub

    Private Sub txtPrecioCompra_Click(sender As Object, e As EventArgs) Handles txtPrecioCompra.Click, txtPrecioVenta.Click, txtPrecioPaquete.Click, txtCantidadPaquete.Click
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtPrecioCompra_Leave(sender As Object, e As EventArgs) Handles txtPrecioCompra.Leave, txtPrecioVenta.Leave, txtPrecioPaquete.Leave
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtPrecioCompra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioCompra.KeyPress, txtPrecioVenta.KeyPress, txtPrecioPaquete.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub txtCantidadPaquete_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidadPaquete.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If
    End Sub
End Class