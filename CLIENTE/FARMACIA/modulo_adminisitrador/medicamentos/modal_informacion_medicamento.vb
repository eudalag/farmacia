﻿Imports CAPA_DATOS
Public Class modal_informacion_medicamento
    Private idMedicamento As Integer

    Public Sub New(ByVal _idMedicamento As Integer)
        InitializeComponent()
        idMedicamento = _idMedicamento

        CargarDatosMedicamento()
    End Sub
    
    Protected Sub CargarDatosMedicamento()
        Dim tblDatos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"

        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS NVARCHAR(10)),6) AS REGISTRO,ISNULL(M.CODIGO_BARRA,'') AS COD_BARRA,"
        sSql += " M.MEDICAMENTO, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, FORMA_FARMACEUTICA, LABORATORIO, ISNULL(ACCION_TERAPEUTICA,'') AS ACCION,"
        sSql += " M.COMPOSICION, ISNULL(M.MARCA,'') AS MARCA, M.MINIMO, CASE WHEN M.CONTROLADO = 0 THEN 'No' ELSE 'Si' END AS CONTROLADO, M.PRECIO_UNITARIO,"
        sSql += " ISNULL(CAST (M.PRECIO_PAQUETE AS NVARCHAR(10)),'S/P') AS PRECIO_PAQUETE,ISNULL(CAST(M.CANTIDAD_PAQUETE AS NVARCHAR(4)),'S/C') AS CANTIDAD_PAQUETE  "
        sSql += " FROM VW_MEDICAMENTO M"
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO;"
        txtRegSistema.Text = "Si"

        Dim tbl = tblDatos.ejecutarConsulta(sSql)
        txtRegistro.Text = tbl.Rows(0)("REGISTRO")
        txtCodigoBarra.Text = tbl.Rows(0)("COD_BARRA")
        txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
        txtConcentracion.Text = tbl.Rows(0)("CONCENTRACION")
        txtFormaFarmaceutica.Text = tbl.Rows(0)("FORMA_FARMACEUTICA")
        txtLaboratorio.Text = tbl.Rows(0)("LABORATORIO")
        txtComposicion.Text = tbl.Rows(0)("COMPOSICION")
        txtMarca.Text = tbl.Rows(0)("MARCA")
        txtMinimo.Text = tbl.Rows(0)("MINIMO")
        txtControlado.Text = tbl.Rows(0)("CONTROLADO")
        txtPrecioVenta.Text = tbl.Rows(0)("PRECIO_UNITARIO")
        txtAccion.Text = tbl.Rows(0)("ACCION")
        txtCantidadPaquete.Text = tbl.Rows(0)("CANTIDAD_PAQUETE")
        txtPrecioPaquete.Text = tbl.Rows(0)("PRECIO_PAQUETE")
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        'Dim proc = New modulo()
        'proc.registrarVentaPerdida(idMedicamento)
        'Me.Close()
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.Close()
    End Sub
End Class