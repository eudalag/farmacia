﻿Imports CAPA_DATOS
Imports System.Transactions

Public Class form_kardex

    Private dtsource As DataTable
    Private idMedicamento As Integer
    Private idKardex As Integer
    
    Public Sub New(ByVal _idMedicamento As Integer)
        InitializeComponent()
        idMedicamento = _idMedicamento
        inicializarParametros()
        txtDesde.Text = "01/" + Now.ToString("MM/yyyy")
        txtHasta.Text = Now.ToString("dd/MM/yyyy")
        iniciarCombo()
        iniciarComboTipoMovimiento()
        iniciarComboPresentacion()
        CargarDatosCabecera(_idMedicamento.ToString)


        'cargarDatosMovimientos("01/" + Now.ToString("MM/yyyy"), Now.ToString("dd/MM/yyyy"), -1, "")

        'dgvGrilla.DataSource = dtsource

    End Sub
    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        Dim tblCombo = New datos()
        sSql = " DECLARE @ID_MEDICAMENTO AS INTEGER ;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"

        sSql += " SELECT ISNULL(NRO_LOTE,'Sin Lote') + CASE WHEN CERRADO = 1 THEN ' (Cerrado)' ELSE ISNULL(' - Venc. ' + CONVERT(NVARCHAR(10),F_VENCIMIENTO,103),' S/Venc.')  END AS LOTE, "
        sSql += " ID_KARDEX FROM KARDEX WHERE ID_MEDICAMENTO = @ID_MEDICAMENTO"
        sSql += " UNION ALL SELECT '[Todos los Kardex]', - 1 ORDER BY LOTE"

        cmbKardex.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbKardex.DisplayMember = "LOTE"
        cmbKardex.ValueMember = "ID_KARDEX"
    End Sub

    Protected Sub iniciarComboPresentacion()
        Dim sSql As String = ""
        Dim tblCombo = New datos()
        sSql = " SELECT ID_FORMA_FARMACEUTICA, DESCRIPCION FROM FORMA_FARMACEUTICA"     

        cmbPresentacion.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbPresentacion.DisplayMember = "DESCRIPCION"
        cmbPresentacion.ValueMember = "ID_FORMA_FARMACEUTICA"
    End Sub

    Protected Sub iniciarComboTipoMovimiento()
        Dim sSql As String = ""
        Dim tblCombo = New datos()
        sSql = ""

        sSql += " SELECT ID_TIPO_MOVIMIENTO, DESCRIPCION FROM TIPO_MOVIMIENTO"
        sSql += " UNION ALL"
        sSql += " SELECT -1, '[Todos los Movimientos]'"
        sSql += " ORDER BY ID_TIPO_MOVIMIENTO "

        
        cmbTipoMovimiento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMovimiento.DisplayMember = "DESCRIPCION"
        cmbTipoMovimiento.ValueMember = "ID_TIPO_MOVIMIENTO"
    End Sub
    Protected Sub CargarDatosCabecera(ByVal idMedicamento As String)
        Dim tblDatos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento + ";"
        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS NVARCHAR(10)),6) AS REGISTRO,ISNULL(M.CODIGO_BARRA,'') AS COD_BARRA,"
        sSql += " M.MEDICAMENTO, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, M.ID_FORMA_FARMACEUTICA, LABORATORIO,"
        sSql += " M.COMPOSICION, M.MARCA, M.MINIMO, CASE WHEN M.CONTROLADO = 0 THEN 'No' ELSE 'Si' END AS CONTROLADO, M.PRECIO_UNITARIO  FROM VW_MEDICAMENTO M"
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO;"
        sSql += " SELECT ID_KARDEX FROM KARDEX K"
        sSql += " WHERE ID_MEDICAMENTO = @ID_MEDICAMENTO AND CERRADO = 0"
        sSql += " ORDER BY F_VENCIMIENTO "
        Dim ds = tblDatos.obtenerDataSet(sSql)
        Dim tbl = ds.Tables(0)
        Dim m = New modulo()
        txtRegistro.Text = tbl.Rows(0)("REGISTRO")
        txtCodigoBarra.Text = tbl.Rows(0)("COD_BARRA")
        txtMedicamento.Text = tbl.Rows(0)("MEDICAMENTO")
        txtConcentracion.Text = tbl.Rows(0)("CONCENTRACION")

        cmbPresentacion.SelectedValue = tbl.Rows(0)("ID_FORMA_FARMACEUTICA")
        txtLaboratorio.Text = tbl.Rows(0)("LABORATORIO")
        txtComposicion.Text = tbl.Rows(0)("COMPOSICION")
        txtMarca.Text = m.esNulo(tbl.Rows(0)("MARCA"))
        txtMinimo.Text = tbl.Rows(0)("MINIMO")
        txtControlado.Text = tbl.Rows(0)("CONTROLADO")
        txtPrecioVenta.Text = tbl.Rows(0)("PRECIO_UNITARIO")
        If ds.Tables(1).Rows.Count > 0 Then
            idKardex = ds.Tables(1).Rows(0)(0)
            cmbKardex.SelectedValue = idKardex
        End If
        cargarDatosKardex()
    End Sub
    Protected Sub cargarDatosMovimientos(ByVal desde As String, ByVal hasta As String, ByVal tipo As Integer, ByVal nroComp As String)
        Dim sSql As String = ""

        sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE, @TIPO_MOV AS INTEGER, @NRO_COMPROBANTE AS NVARCHAR(18), @ID_MEDICAMENTO AS INTEGER,@ID_KARDEX AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @ID_KARDEX = " + idKardex.ToString + ";"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SET @TIPO_MOV = " + tipo.ToString + ";"
        sSql += " SET @NRO_COMPROBANTE = '" + nroComp + "';"

        sSql += " SELECT MK.FECHA, TM.DESCRIPCION AS MOVIMIENTO,"
        sSql += " CASE "
        sSql += " WHEN NOT MK.ID_COMPRA_MEDICAMENTOS IS NULL AND MK.ID_VENTA_MEDICAMENTOS IS NULL THEN CAST(C.NRO_COMPROBANTE AS NVARCHAR(18))"
        sSql += " WHEN  MK.ID_COMPRA_MEDICAMENTOS IS NULL AND NOT MK.ID_VENTA_MEDICAMENTOS IS NULL THEN CAST(V.NRO_VENTA  AS NVARCHAR(18))"
        sSql += " WHEN MK.ID_VENTA_MEDICAMENTOS IS NULL AND MK.ID_COMPRA_MEDICAMENTOS IS NULL THEN CAST(MK.ID_MOVIMIENTO AS NVARCHAR(18))"
        sSql += " END AS NRO_COMP,"
        sSql += " CASE WHEN (MK.CANTIDAD > 0) THEN MK.CANTIDAD ELSE 0 END AS ENTRADA, "
        sSql += " CASE WHEN (MK.CANTIDAD < 0) THEN MK.CANTIDAD ELSE 0 END AS SALIDA,"
        sSql += " CASE "
        sSql += " WHEN NOT MK.ID_COMPRA_MEDICAMENTOS IS NULL AND MK.ID_VENTA_MEDICAMENTOS IS NULL THEN 1"
        sSql += " WHEN  MK.ID_COMPRA_MEDICAMENTOS IS NULL AND NOT MK.ID_VENTA_MEDICAMENTOS IS NULL THEN 2"
        sSql += " WHEN MK.ID_VENTA_MEDICAMENTOS IS NULL AND MK.ID_COMPRA_MEDICAMENTOS IS NULL THEN 3"
        sSql += " END AS TIPO,"
        sSql += " CASE "
        sSql += " WHEN NOT MK.ID_COMPRA_MEDICAMENTOS IS NULL AND MK.ID_VENTA_MEDICAMENTOS IS NULL THEN MK.ID_COMPRA_MEDICAMENTOS  "
        sSql += " WHEN  MK.ID_COMPRA_MEDICAMENTOS IS NULL AND NOT MK.ID_VENTA_MEDICAMENTOS IS NULL THEN MK.ID_VENTA_MEDICAMENTOS "
        sSql += " WHEN MK.ID_VENTA_MEDICAMENTOS IS NULL AND MK.ID_COMPRA_MEDICAMENTOS IS NULL THEN MK.ID_MOVIMIENTO "
        sSql += " END AS ID,U.USUARIO,"
        sSql += " K.ID_MEDICAMENTO"
        sSql += " FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " INNER JOIN TIPO_MOVIMIENTO TM ON TM.ID_TIPO_MOVIMIENTO = MK.ID_TIPO_MOVIMIENTO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = MK.ID_USUARIO "
        sSql += " LEFT JOIN VENTA V ON V.ID_VENTA = MK.ID_VENTA_MEDICAMENTOS "
        sSql += " LEFT JOIN COMPRA C ON C.ID_COMPRA = MK.ID_COMPRA_MEDICAMENTOS "
        sSql += " WHERE MK.CANTIDAD <> 0"
        sSql += " AND K.ID_MEDICAMENTO = @ID_MEDICAMENTO "
        sSql += " AND K.ID_KARDEX = CASE WHEN @ID_KARDEX = -1 THEN K.ID_KARDEX ELSE @ID_KARDEX END"
        If chkRangoFecha.Checked Then
            sSql += " AND MK.FECHA >= @DESDE AND MK.FECHA <= @HASTA "
        End If
        sSql += " AND MK.ID_TIPO_MOVIMIENTO =  CASE WHEN @TIPO_MOV = -1 THEN MK.ID_TIPO_MOVIMIENTO ELSE @TIPO_MOV END"
        sSql += " AND CASE "
        sSql += " WHEN NOT MK.ID_COMPRA_MEDICAMENTOS IS NULL AND MK.ID_VENTA_MEDICAMENTOS IS NULL THEN CAST(C.NRO_COMPROBANTE AS NVARCHAR(18))"
        sSql += " WHEN  MK.ID_COMPRA_MEDICAMENTOS IS NULL AND NOT MK.ID_VENTA_MEDICAMENTOS IS NULL THEN CAST(V.NRO_VENTA  AS NVARCHAR(18))"
        sSql += " WHEN MK.ID_VENTA_MEDICAMENTOS IS NULL AND MK.ID_COMPRA_MEDICAMENTOS IS NULL THEN CAST(MK.ID_MOVIMIENTO AS NVARCHAR(18))"
        sSql += " END LIKE '%' + @NRO_COMPROBANTE  + '%'"
        dtsource = New datos().ejecutarConsulta(sSql)
    End Sub
    Protected Sub cargarDatosKardex()
        Dim tblDatos = New datos()
        Dim sSql As String = ""
        If idKardex <> -1 Then
            sSql += " DECLARE @ID_KARDEX AS INTEGER;"
            sSql += " SET @ID_KARDEX = " + idKardex.ToString + ";"
            sSql += " SELECT ISNULL(K.F_VENCIMIENTO,NULL) AS F_VENCIMIENTO, ISNULL(K.NRO_LOTE,'SIN LOTE') AS NRO_LOTE ,SUM(CANTIDAD) AS STOCK FROM MOVIMIENTO_KARDEX MK"
            sSql += " INNER JOIN KARDEX K ON K.ID_KARDEX= MK.ID_KARDEX "
            sSql += " WHERE MK.ID_KARDEX = @ID_KARDEX"
            sSql += " GROUP BY K.F_VENCIMIENTO, K.NRO_LOTE "
            Dim tbl = tblDatos.ejecutarConsulta(sSql)
            If tbl.Rows.Count > 0 Then
                If Not IsDBNull(tbl.Rows(0)(0)) Then
                    txtFechaVencimiento.Text = CDate(tbl.Rows(0)(0))                    
                Else
                    txtFechaVencimiento.Text = ""                    
                End If

                If Not IsDBNull(tbl.Rows(0)(1)) Then
                    txtNroLote.Text = tbl.Rows(0)(1)
                Else
                    txtNroLote.Text = ""
                End If
                txtStock.Text = tbl.Rows(0)(1)
                chkActualizarKardex.Enabled = True
            Else
                txtFechaVencimiento.Text = ""
                txtNroLote.Text = ""
                txtStock.Text = 0
                chkActualizarKardex.Enabled = False
                chkActualizar.Checked = False
            End If

        Else
            sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER;"
            sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
            sSql += " SELECT isnull(SUM(CANTIDAD),0) AS STOCK FROM MOVIMIENTO_KARDEX MK"
            sSql += " INNER JOIN KARDEX K ON K.ID_KARDEX= MK.ID_KARDEX "
            sSql += " WHERE K.ID_MEDICAMENTO  = @ID_MEDICAMENTO "
            Dim tbl = tblDatos.ejecutarConsulta(sSql)
            txtFechaVencimiento.Text = ""
            txtFechaVencimiento.Enabled = False
            txtNroLote.ReadOnly = True
            txtNroLote.Text = ""
            If tbl.Rows.Count > 0 Then
                txtStock.Text = tbl.Rows(0)(0)
            Else
                txtStock.Text = 0
            End If
            chkActualizarKardex.Enabled = False
            chkActualizar.Checked = False            
        End If
    End Sub
    
    
    
    Private Sub inicializarParametros()
        
        With (dgvGrilla)                        
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False

            Dim fecha As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(fecha)
            With fecha
                .Name = "Fecha"
                .HeaderText = "Fecha"
                .DataPropertyName = "FECHA"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim Tipo As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(Tipo)
            With Tipo
                .Name = "Tipo"
                .HeaderText = "Tipo"
                .DataPropertyName = "MOVIMIENTO"
                .Width = 230
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim comp As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(comp)
            With comp
                .Name = "comp"
                .HeaderText = "Nro. Comprobante"
                .DataPropertyName = "NRO_COMP"
                .Width = 200
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim Entrada As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(Entrada)
            With Entrada
                .Name = "Entrada"
                .HeaderText = "Ingreso"
                .DataPropertyName = "ENTRADA"
                .Width = 130
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim Salida As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(Salida)
            With Salida
                .Name = "Salida"
                .HeaderText = "Salida"
                .DataPropertyName = "SALIDA"
                .Width = 130
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim Usuario As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn
            .Columns.Add(Usuario)
            With Usuario
                .Name = "Usuario"
                .HeaderText = "Usuario"
                .DataPropertyName = "USUARIO"
                .Width = 150
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colButton As DataGridViewButtonColumn = New DataGridViewButtonColumn()
            .Columns.Add(colButton)
            With colButton
                .HeaderText = "Ver"
                .Name = "colButton"
                .Width = 50
                .Frozen = False
                .Text = "Ver"
                .UseColumnTextForButtonValue = True
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Visible = False
                .Name = "registro"
                .DataPropertyName = "ID"
            End With

            Dim tipoMovimiento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(tipoMovimiento)
            With tipoMovimiento
                .Visible = False
                .Name = "tipoMovimiento"
                .DataPropertyName = "TIPO"
            End With
        End With
    End Sub

    Private Sub form_kardex_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsNumeric(cmbKardex.SelectedValue) Then
            idKardex = cmbKardex.SelectedValue                        
            calcularTotal()            
        End If
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
    Protected Sub calcularTotal()
        Dim total As Decimal = 0
        If Not dtsource Is Nothing Then
            For Each item In dtsource.Rows
                total += CDec(item("ENTRADA")) + CDec(item("SALIDA"))
            Next
        End If
      
        txtStock.Text = total
    End Sub

    Private Sub cmbKardex_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbKardex.SelectedIndexChanged
        If IsNumeric(cmbKardex.SelectedValue) Then
            idKardex = cmbKardex.SelectedValue
            cargarDatosMovimientos(txtDesde.Text, txtHasta.Text, cmbTipoMovimiento.SelectedValue, txtNroComprobante.Text)
            dgvGrilla.DataSource = dtsource
            cargarDatosKardex()
            calcularTotal()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Using form = New visualizadorKardexMedicamento()
        Dim parametros As String = idMedicamento.ToString + ";" + cmbKardex.SelectedValue.ToString
        Dim form = New visualizador_reporte(1, parametros)
        form.Show()
        'If DialogResult.OK = form.ShowDialog() Then

        '            End If
        'End Using
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click 
        Using form = New form_gestion_baja(idMedicamento, idKardex)
            If DialogResult.OK = form.ShowDialog() Then
                cargarDatosMovimientos(txtDesde.Text, txtHasta.Text, cmbTipoMovimiento.SelectedValue, txtNroComprobante.Text)
                dgvGrilla.DataSource = dtsource
                cargarDatosKardex()
                calcularTotal()                
            End If
        End Using
    End Sub

    Private Sub chkActualizar_CheckedChanged(sender As Object, e As EventArgs) Handles chkActualizar.CheckedChanged
        txtPrecioVenta.Enabled = chkActualizar.Checked
        txtPrecioVenta.ReadOnly = Not chkActualizar.Checked
        btnActualizar.Enabled = chkActualizar.Checked

        txtCodigoBarra.ReadOnly = Not chkActualizar.Checked
        txtMedicamento.ReadOnly = Not chkActualizar.Checked
        txtConcentracion.ReadOnly = Not chkActualizar.Checked
        cmbPresentacion.Enabled = chkActualizar.Checked
        txtComposicion.ReadOnly = Not chkActualizar.Checked
        txtMarca.ReadOnly = Not chkActualizar.Checked
        txtMinimo.ReadOnly = Not chkActualizar.Checked
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Try
            Using scope = New TransactionScope
                Dim tblMedicamento = New datos()
                tblMedicamento.tabla = "MEDICAMENTO"
                tblMedicamento.campoID = "ID_MEDICAMENTO"
                tblMedicamento.valorID = idMedicamento
                tblMedicamento.agregarCampoValor("PRECIO_UNITARIO", CDec(txtPrecioVenta.Text))
                tblMedicamento.agregarCampoValor("CODIGO_BARRA", txtCodigoBarra.Text)
                tblMedicamento.agregarCampoValor("NOMBRE", txtMedicamento.Text)
                tblMedicamento.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
                tblMedicamento.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbPresentacion.SelectedValue)
                tblMedicamento.agregarCampoValor("COMPOSICION", txtComposicion.Text)
                tblMedicamento.agregarCampoValor("MARCA", txtMarca.Text)
                tblMedicamento.agregarCampoValor("MINIMO", txtMinimo.Text)
                tblMedicamento.modificar()
                scope.Complete()
            End Using
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Information
            title = "Cambios Guardados"
            Dim msg = "Los Datos del Medicamento Fueron Actualizados."
            response = MsgBox(msg, style, title)
        Catch ex As Exception
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
            title = "Error Inconsistencia en la base de datos"
            Dim msg = ex.Message.ToString
            response = MsgBox(msg, style, title)
        End Try

    End Sub

    Private Sub txtPrecioVenta_Click(sender As Object, e As EventArgs)
        Dim txt As TextBox = sender
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtPrecioVenta_Leave(sender As Object, e As EventArgs)
        Dim txt As TextBox = sender
        Dim result As String = txt.Text.ToString
        Dim parts() As String = result.Split(".") 'Declarar un arreglo y llenar            
        If parts.Length > 1 Then  'Verificar que el arreglo tenga mas de un elemento.
            If parts(1).Length < 2 Then 'Validar Cantidad de Decimales.
                result = parts(0) + "." + Mid(parts(1) + "00", 1, 2)
            End If
        Else
            result = result + ".00"
        End If
        txt.Text = result
    End Sub

    Private Sub txtPrecioVenta_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub dgvGrilla_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGrilla.CellClick
        Select Case Me.dgvGrilla.Columns(e.ColumnIndex).Name
            Case "colButton"
                Dim tipoMovimiento = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("tipoMovimiento"), DataGridViewTextBoxCell).Value
                Dim idVenta = CType(Me.dgvGrilla.Rows(e.RowIndex).Cells("registro"), DataGridViewTextBoxCell).Value
                Select Case tipoMovimiento
                    Case 1
                        Using Form = New comprobanteCompra(idVenta)
                            Form.ShowDialog()
                        End Using
                    Case 2
                        Using Form = New comprobante_venta(idVenta)
                            Form.ShowDialog()
                        End Using
                    Case 3
                       
                End Select
                
        End Select
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        idKardex = cmbKardex.SelectedValue
        cargarDatosMovimientos(txtDesde.Text, txtHasta.Text, cmbTipoMovimiento.SelectedValue, txtNroComprobante.Text)
        dgvGrilla.DataSource = dtsource
        cargarDatosKardex()
        calcularTotal()
    End Sub

    Private Sub chkActualizarKardex_CheckedChanged(sender As Object, e As EventArgs) Handles chkActualizarKardex.CheckedChanged
        If Not chkActualizarKardex.Checked Then
            txtFechaVencimiento.Enabled = False
            txtNroLote.ReadOnly = True
            btnActualizarKardex.Enabled = False
        Else
            txtFechaVencimiento.Enabled = True
            txtNroLote.ReadOnly = False
            btnActualizarKardex.Enabled = True
        End If
    End Sub

    Private Sub btnActualizarKardex_Click(sender As Object, e As EventArgs) Handles btnActualizarKardex.Click
        If cmbKardex.SelectedValue <> -1 Then
            Dim tblDatos = New datos()
            tblDatos.tabla = "KARDEX"
            tblDatos.valorID = cmbKardex.SelectedValue
            tblDatos.campoID = "ID_KARDEX"
            tblDatos.agregarCampoValor("F_VENCIMIENTO", txtFechaVencimiento.Text)
            tblDatos.agregarCampoValor("NRO_LOTE", txtNroLote.Text)
            tblDatos.modificar()
            Dim idKardex = cmbKardex.SelectedValue

            iniciarCombo()
            cmbKardex.SelectedValue = idKardex
            chkActualizarKardex.Checked = False
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        
        Dim Form = New modalKardex(idMedicamento)
        If DialogResult.OK = Form.ShowDialog() Then
            iniciarCombo()
        End If
    End Sub

    Private Sub chkRangoFecha_CheckedChanged(sender As Object, e As EventArgs) Handles chkRangoFecha.CheckedChanged
            txtDesde.Enabled = chkRangoFecha.Checked
            txtHasta.Enabled = chkRangoFecha.Checked
    End Sub


End Class