﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Transactions
Public Class form_gestionMedicamento
    Private mod_conf As modulo = New modulo()
    Private tMedicamento As eMedicamento
    Private _modal As Boolean
    Public idMedicamento As Integer

    Private InString As String
    Private PistolaEnviando As Boolean = False

    Public Sub New(Optional modal As Boolean = False)
        _modal = modal
        InitializeComponent()
        tMedicamento = New eMedicamento()
        iniciarComboLaboratorio()
        iniciarComboFormaFarmaceutica()       
    End Sub
    Public Sub New(ByVal _idMedicamento As Integer)
        InitializeComponent()
        iniciarComboLaboratorio()
        iniciarComboFormaFarmaceutica()
        tMedicamento = New eMedicamento(_idMedicamento)
        idMedicamento = _idMedicamento
        cargarDatos()
        _modal = False        
       
    End Sub
    
   
    Private Sub cargarDatos()
        txtNombre.Text = tMedicamento.NOMBRE
        cmbTipoMedicamento.SelectedValue = tMedicamento.ID_TIPO_MEDICAMENTO
        cmbLaboratorio.SelectedValue = tMedicamento.ID_LABORATORIO
        txtComposicion.Text = mod_conf.esNulo(tMedicamento.COMPOSICION)
        txtConcentracion.Text = tMedicamento.CONCENTRACION
        txtMarca.Text = tMedicamento.MARCA
        txtMinimo.Text = mod_conf.esNulo(tMedicamento.MINIMO)
        txtObservacion.Text = tMedicamento.OBSERVACION
        chkControlado.Checked = tMedicamento.CONTROLADO
        chkEstado.Checked = tMedicamento.ACTIVO
        chkMayor.Checked = tMedicamento.VENTA_MAYOR
    End Sub
    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    Protected Sub iniciarComboFormaFarmaceutica()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT ID_FORMA_FARMACEUTICA,LTRIM(DESCRIPCION) AS DESCRIPCION FROM FORMA_FARMACEUTICA  UNION ALL"
        sSql += " SELECT -1 , '[Seleccione una Forma Farmaceutica]' ORDER BY DESCRIPCION"

        cmbTipoMedicamento.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbTipoMedicamento.DisplayMember = "DESCRIPCION"
        cmbTipoMedicamento.ValueMember = "ID_FORMA_FARMACEUTICA"
    End Sub
    Private Function validarformulario() As Boolean
        Dim mensaje As String
        Dim valido As Boolean = True
        If Trim(txtNombre.Text).Length > 0 Then
            ErrorProvider1.SetError(txtNombre, "")
        Else
            mensaje = "Complete el campo Nombre."
            ErrorProvider1.SetError(txtNombre, mensaje)
            valido = False
        End If
        If Trim(txtMinimo.Text).Length > 0 Then
            ErrorProvider1.SetError(txtMinimo, "")
        Else
            mensaje = "Complete el campo Minimo."
            ErrorProvider1.SetError(txtMinimo, mensaje)
            valido = False
        End If
        If cmbLaboratorio.SelectedValue = -1 Then
            mensaje = "Seleccione un laboratorio valido."
            ErrorProvider1.SetError(cmbLaboratorio, mensaje)
            valido = False
        End If
        If cmbTipoMedicamento.SelectedValue = -1 Then
            mensaje = "Seleccione un laboratorio valido."
            ErrorProvider1.SetError(cmbTipoMedicamento, mensaje)
            valido = False
        End If
        Return valido
    End Function

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        If _modal Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            Me.Close()
            mod_conf.volver(New form_medicamentos())
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If validarformulario() Then
             guardarMedicamento(False)
            If _modal Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Else
                Me.Close()
                mod_conf.volver(New form_medicamentos())
            End If            
        End If
    End Sub
    Protected Sub guardarMedicamento(ByVal generarCodigo As Boolean)
        Dim tblMedicamento = New datos()
        tblMedicamento.tabla = "MEDICAMENTO"
        tblMedicamento.modoID = "sql"
        tblMedicamento.campoID = "ID_MEDICAMENTO"

        tblMedicamento.agregarCampoValor("NOMBRE", txtNombre.Text)
        tblMedicamento.agregarCampoValor("COMPOSICION", txtComposicion.Text)
        tblMedicamento.agregarCampoValor("MINIMO", CInt(txtMinimo.Text))
        tblMedicamento.agregarCampoValor("MAXIMO", CInt(txtMaximo.Text))

        tblMedicamento.agregarCampoValor("ID_TIPO_MEDICAMENTO", cmbTipoMedicamento.SelectedValue)
        tblMedicamento.agregarCampoValor("ACTIVO", chkEstado.Checked)
        tblMedicamento.agregarCampoValor("OBSERVACION", txtObservacion.Text)
        tblMedicamento.agregarCampoValor("MARCA", txtMarca.Text)
        tblMedicamento.agregarCampoValor("CONTROLADO", chkControlado.Checked)


        tblMedicamento.agregarCampoValor("ID_LABORATORIO", cmbLaboratorio.SelectedValue)
        tblMedicamento.agregarCampoValor("CONCENTRACION", txtConcentracion.Text)
        tblMedicamento.agregarCampoValor("VENTA_MAYOR", chkMayor.Checked)
        If tMedicamento.ID_MEDICAMENTO = 0 Then
            tblMedicamento.insertar()
        Else
            tblMedicamento.valorID = idMedicamento
            tblMedicamento.modificar()
        End If
      

    End Sub

    Private Sub txtPrecioUnitario_KeyPress(sender As Object, e As KeyPressEventArgs)
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") <> -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub txtMinimo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMinimo.KeyPress
        Dim ch As Char = e.KeyChar
        Dim tb As TextBox = sender

        If ch = Chr(46) And tb.Text.IndexOf(".") = -1 Then
            e.Handled = True
        End If

        If Not Char.IsDigit(ch) And (ch <> Chr(8)) And (ch <> Chr(46)) Then
            e.Handled = True
        End If

        If Char.IsDigit(ch) Or ch = "." Then
            Dim result As String = tb.Text.Substring(0, tb.SelectionStart) _
                                   + e.KeyChar _
                                   + tb.Text.Substring(tb.SelectionStart + tb.SelectionLength)

            Dim parts() As String = result.Split(".")
            If parts.Length > 1 Then
                If parts(1).Length > 2 Then
                    e.Handled = True
                End If
            End If
        End If

        If Asc(e.KeyChar) = 13 Then
            e.Handled = True
            btnAceptar.Focus()
        End If
    End Sub

    Private Sub form_gestionMedicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtNombre.Focus()
    End Sub

    Private Sub txtMinimo_Click(sender As Object, e As EventArgs) Handles txtMinimo.Click
        Dim txt As TextBox = CType(sender, TextBox)
        If (Not String.IsNullOrEmpty(txt.Text)) Then
            txt.SelectionStart = 0
            txt.SelectionLength = txt.Text.Length
        End If
    End Sub

    Private Sub txtNombre_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNombre.KeyPress
        If (Asc(e.KeyChar) = 13) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Using nuevo_FormaFarmaceutica = New modal_formaFarmaceutica()
            If DialogResult.OK = nuevo_FormaFarmaceutica.ShowDialog() Then
                iniciarComboFormaFarmaceutica()
            End If
        End Using
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Using nuevo_Laboratorio = New modal_laboratorio()
            If DialogResult.OK = nuevo_Laboratorio.ShowDialog() Then
                iniciarComboLaboratorio()
            End If
        End Using
    End Sub

    Private tiempo As Integer = 0
    Dim ant As String = ""
    'Private Sub form_gestionMedicamento_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
    '    Dim carAsc As Integer
    '    carAsc = Asc(e.KeyChar)
    '    If carAsc = 169 Then
    '        InString = ""
    '        PistolaEnviando = True
    '        e.Handled = True
    '    ElseIf PistolaEnviando = True Then
    '        If carAsc = 174 Then
    '            PistolaEnviando = False
    '            If Not existeCodigoBarraBD(InString) And Not existeCodigoBarraGrilla(InString) Then
    '                nuevoItem(InString, 1, 1)
    '                LoadPage()
    '            Else
    '                Dim mens = New modulo()
    '                mens.mensaje("ERROR", "El Codigo de Barra que intenta ingresar ya existe en la lista de codigos del Medicamento o esta asignado a otro medicamento.")
    '            End If

    '        Else
    '            InString &= e.KeyChar.ToString
    '        End If
    '        e.Handled = True
    '    End If
    'End Sub
   

End Class