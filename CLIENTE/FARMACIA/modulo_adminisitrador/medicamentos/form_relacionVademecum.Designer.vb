﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_relacionVademecum
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_relacionVademecum))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtAccionVademecum = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtMarcaVademecum = New System.Windows.Forms.TextBox()
        Me.txtComposicionVademecum = New System.Windows.Forms.TextBox()
        Me.txtLaboratorioVademecum = New System.Windows.Forms.TextBox()
        Me.txtFormaFarmaceuticaVademecum = New System.Windows.Forms.TextBox()
        Me.txtConcentracionVademecum = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMedicamentoVademecum = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblLaboratorio = New System.Windows.Forms.Label()
        Me.lblFormaFarmaceutica = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.chkActualizar = New System.Windows.Forms.CheckBox()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.cmbFormaFarmaceutica = New System.Windows.Forms.ComboBox()
        Me.txtPrecioCompra = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmbLaboratorio = New System.Windows.Forms.ComboBox()
        Me.chkControlado = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCantidadPaquete = New System.Windows.Forms.TextBox()
        Me.txtPrecioPaquete = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAccion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtPrecioVenta = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtStockMinimo = New System.Windows.Forms.TextBox()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.txtComposicion = New System.Windows.Forms.TextBox()
        Me.txtConcentracion = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtMedicamento = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtCodigoBarra = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtRegistro = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Green
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1117, 67)
        Me.Panel1.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(374, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(422, 52)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Cargar Informacion de Vademecum  a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Sistema de Registro de Medicamentos"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.txtAccionVademecum)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtMarcaVademecum)
        Me.GroupBox1.Controls.Add(Me.txtComposicionVademecum)
        Me.GroupBox1.Controls.Add(Me.txtLaboratorioVademecum)
        Me.GroupBox1.Controls.Add(Me.txtFormaFarmaceuticaVademecum)
        Me.GroupBox1.Controls.Add(Me.txtConcentracionVademecum)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtMedicamentoVademecum)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 80)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(545, 390)
        Me.GroupBox1.TabIndex = 61
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sistema Vademecum"
        '
        'Button2
        '
        Me.Button2.Image = Global.FARMACIA.My.Resources.Resources.Last
        Me.Button2.Location = New System.Drawing.Point(379, 336)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(160, 32)
        Me.Button2.TabIndex = 185
        Me.Button2.Text = "Cargar Informacion"
        Me.Button2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Button2.UseVisualStyleBackColor = True
        '
        'txtAccionVademecum
        '
        Me.txtAccionVademecum.Location = New System.Drawing.Point(115, 203)
        Me.txtAccionVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtAccionVademecum.Multiline = True
        Me.txtAccionVademecum.Name = "txtAccionVademecum"
        Me.txtAccionVademecum.ReadOnly = True
        Me.txtAccionVademecum.Size = New System.Drawing.Size(390, 46)
        Me.txtAccionVademecum.TabIndex = 180
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(5, 207)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 179
        Me.Label15.Text = "Accion:"
        '
        'txtMarcaVademecum
        '
        Me.txtMarcaVademecum.Location = New System.Drawing.Point(115, 256)
        Me.txtMarcaVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMarcaVademecum.Name = "txtMarcaVademecum"
        Me.txtMarcaVademecum.ReadOnly = True
        Me.txtMarcaVademecum.Size = New System.Drawing.Size(197, 20)
        Me.txtMarcaVademecum.TabIndex = 174
        '
        'txtComposicionVademecum
        '
        Me.txtComposicionVademecum.Location = New System.Drawing.Point(115, 177)
        Me.txtComposicionVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtComposicionVademecum.Name = "txtComposicionVademecum"
        Me.txtComposicionVademecum.ReadOnly = True
        Me.txtComposicionVademecum.Size = New System.Drawing.Size(390, 20)
        Me.txtComposicionVademecum.TabIndex = 173
        '
        'txtLaboratorioVademecum
        '
        Me.txtLaboratorioVademecum.Location = New System.Drawing.Point(115, 153)
        Me.txtLaboratorioVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtLaboratorioVademecum.Name = "txtLaboratorioVademecum"
        Me.txtLaboratorioVademecum.ReadOnly = True
        Me.txtLaboratorioVademecum.Size = New System.Drawing.Size(254, 20)
        Me.txtLaboratorioVademecum.TabIndex = 172
        '
        'txtFormaFarmaceuticaVademecum
        '
        Me.txtFormaFarmaceuticaVademecum.Location = New System.Drawing.Point(115, 130)
        Me.txtFormaFarmaceuticaVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtFormaFarmaceuticaVademecum.Name = "txtFormaFarmaceuticaVademecum"
        Me.txtFormaFarmaceuticaVademecum.ReadOnly = True
        Me.txtFormaFarmaceuticaVademecum.Size = New System.Drawing.Size(254, 20)
        Me.txtFormaFarmaceuticaVademecum.TabIndex = 171
        '
        'txtConcentracionVademecum
        '
        Me.txtConcentracionVademecum.Location = New System.Drawing.Point(115, 106)
        Me.txtConcentracionVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtConcentracionVademecum.Name = "txtConcentracionVademecum"
        Me.txtConcentracionVademecum.ReadOnly = True
        Me.txtConcentracionVademecum.Size = New System.Drawing.Size(254, 20)
        Me.txtConcentracionVademecum.TabIndex = 170
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(5, 259)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 167
        Me.Label8.Text = "Marca:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(5, 179)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 13)
        Me.Label7.TabIndex = 166
        Me.Label7.Text = "Composicion:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 155)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 165
        Me.Label5.Text = "Laboratorio:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 132)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 13)
        Me.Label4.TabIndex = 164
        Me.Label4.Text = "Forma Farmaceutica:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 108)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 163
        Me.Label3.Text = "Concentracion:"
        '
        'txtMedicamentoVademecum
        '
        Me.txtMedicamentoVademecum.Location = New System.Drawing.Point(115, 81)
        Me.txtMedicamentoVademecum.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMedicamentoVademecum.Name = "txtMedicamentoVademecum"
        Me.txtMedicamentoVademecum.ReadOnly = True
        Me.txtMedicamentoVademecum.Size = New System.Drawing.Size(390, 20)
        Me.txtMedicamentoVademecum.TabIndex = 162
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 84)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 161
        Me.Label1.Text = "Medicamento:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblLaboratorio)
        Me.GroupBox2.Controls.Add(Me.lblFormaFarmaceutica)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.btnEliminar)
        Me.GroupBox2.Controls.Add(Me.btnNuevo)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.chkActualizar)
        Me.GroupBox2.Controls.Add(Me.lblEstado)
        Me.GroupBox2.Controls.Add(Me.cmbFormaFarmaceutica)
        Me.GroupBox2.Controls.Add(Me.txtPrecioCompra)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.btnAceptar)
        Me.GroupBox2.Controls.Add(Me.btnVolver)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.cmbLaboratorio)
        Me.GroupBox2.Controls.Add(Me.chkControlado)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtCantidadPaquete)
        Me.GroupBox2.Controls.Add(Me.txtPrecioPaquete)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtAccion)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txtPrecioVenta)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.txtStockMinimo)
        Me.GroupBox2.Controls.Add(Me.txtMarca)
        Me.GroupBox2.Controls.Add(Me.txtComposicion)
        Me.GroupBox2.Controls.Add(Me.txtConcentracion)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.txtMedicamento)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.txtCodigoBarra)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.txtRegistro)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Location = New System.Drawing.Point(563, 80)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(610, 390)
        Me.GroupBox2.TabIndex = 62
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sistema Registro de Medicamento"
        '
        'lblLaboratorio
        '
        Me.lblLaboratorio.AutoSize = True
        Me.lblLaboratorio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLaboratorio.ForeColor = System.Drawing.Color.Black
        Me.lblLaboratorio.Location = New System.Drawing.Point(404, 155)
        Me.lblLaboratorio.Name = "lblLaboratorio"
        Me.lblLaboratorio.Size = New System.Drawing.Size(0, 13)
        Me.lblLaboratorio.TabIndex = 200
        '
        'lblFormaFarmaceutica
        '
        Me.lblFormaFarmaceutica.AutoSize = True
        Me.lblFormaFarmaceutica.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormaFarmaceutica.ForeColor = System.Drawing.Color.Black
        Me.lblFormaFarmaceutica.Location = New System.Drawing.Point(404, 133)
        Me.lblFormaFarmaceutica.Name = "lblFormaFarmaceutica"
        Me.lblFormaFarmaceutica.Size = New System.Drawing.Size(0, 13)
        Me.lblFormaFarmaceutica.TabIndex = 199
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Image = Global.FARMACIA.My.Resources.Resources.add
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(374, 153)
        Me.Button3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(24, 22)
        Me.Button3.TabIndex = 198
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.Location = New System.Drawing.Point(228, 19)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(103, 32)
        Me.btnEliminar.TabIndex = 197
        Me.btnEliminar.Text = "Limpiar Datos"
        Me.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Image = Global.FARMACIA.My.Resources.Resources.add
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(374, 128)
        Me.btnNuevo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(24, 22)
        Me.btnNuevo.TabIndex = 196
        Me.btnNuevo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(187, 285)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 17)
        Me.CheckBox1.TabIndex = 195
        Me.CheckBox1.Text = "Actualizar"
        Me.CheckBox1.UseVisualStyleBackColor = True
        Me.CheckBox1.Visible = False
        '
        'chkActualizar
        '
        Me.chkActualizar.AutoSize = True
        Me.chkActualizar.Location = New System.Drawing.Point(467, 84)
        Me.chkActualizar.Name = "chkActualizar"
        Me.chkActualizar.Size = New System.Drawing.Size(72, 17)
        Me.chkActualizar.TabIndex = 194
        Me.chkActualizar.Text = "Actualizar"
        Me.chkActualizar.UseVisualStyleBackColor = True
        Me.chkActualizar.Visible = False
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.ForeColor = System.Drawing.Color.Red
        Me.lblEstado.Location = New System.Drawing.Point(5, 29)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(46, 13)
        Me.lblEstado.TabIndex = 193
        Me.lblEstado.Text = "Estado"
        '
        'cmbFormaFarmaceutica
        '
        Me.cmbFormaFarmaceutica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFormaFarmaceutica.FormattingEnabled = True
        Me.cmbFormaFarmaceutica.Location = New System.Drawing.Point(115, 128)
        Me.cmbFormaFarmaceutica.Name = "cmbFormaFarmaceutica"
        Me.cmbFormaFarmaceutica.Size = New System.Drawing.Size(254, 21)
        Me.cmbFormaFarmaceutica.TabIndex = 192
        '
        'txtPrecioCompra
        '
        Me.txtPrecioCompra.Location = New System.Drawing.Point(115, 282)
        Me.txtPrecioCompra.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPrecioCompra.Name = "txtPrecioCompra"
        Me.txtPrecioCompra.Size = New System.Drawing.Size(58, 20)
        Me.txtPrecioCompra.TabIndex = 191
        Me.txtPrecioCompra.Text = "0.00"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(5, 286)
        Me.Label28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(94, 13)
        Me.Label28.TabIndex = 190
        Me.Label28.Text = "Precio de Compra:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = CType(resources.GetObject("btnAceptar.Image"), System.Drawing.Image)
        Me.btnAceptar.Location = New System.Drawing.Point(448, 352)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 32)
        Me.btnAceptar.TabIndex = 188
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnVolver.Image = CType(resources.GetObject("btnVolver.Image"), System.Drawing.Image)
        Me.btnVolver.Location = New System.Drawing.Point(529, 352)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 32)
        Me.btnVolver.TabIndex = 189
        Me.btnVolver.Text = "&Volver"
        Me.btnVolver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Image = Global.FARMACIA.My.Resources.Resources.buscar
        Me.Button1.Location = New System.Drawing.Point(337, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(202, 32)
        Me.Button1.TabIndex = 187
        Me.Button1.Text = "Buscar Medicamento Registrado"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'cmbLaboratorio
        '
        Me.cmbLaboratorio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLaboratorio.FormattingEnabled = True
        Me.cmbLaboratorio.Location = New System.Drawing.Point(115, 153)
        Me.cmbLaboratorio.Name = "cmbLaboratorio"
        Me.cmbLaboratorio.Size = New System.Drawing.Size(254, 21)
        Me.cmbLaboratorio.TabIndex = 186
        '
        'chkControlado
        '
        Me.chkControlado.AutoSize = True
        Me.chkControlado.Location = New System.Drawing.Point(296, 311)
        Me.chkControlado.Name = "chkControlado"
        Me.chkControlado.Size = New System.Drawing.Size(15, 14)
        Me.chkControlado.TabIndex = 185
        Me.chkControlado.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(180, 337)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 13)
        Me.Label6.TabIndex = 184
        Me.Label6.Text = "Cantidad Paquete:"
        '
        'txtCantidadPaquete
        '
        Me.txtCantidadPaquete.Location = New System.Drawing.Point(279, 334)
        Me.txtCantidadPaquete.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtCantidadPaquete.Name = "txtCantidadPaquete"
        Me.txtCantidadPaquete.Size = New System.Drawing.Size(32, 20)
        Me.txtCantidadPaquete.TabIndex = 183
        Me.txtCantidadPaquete.Text = "1"
        '
        'txtPrecioPaquete
        '
        Me.txtPrecioPaquete.Location = New System.Drawing.Point(115, 334)
        Me.txtPrecioPaquete.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPrecioPaquete.Name = "txtPrecioPaquete"
        Me.txtPrecioPaquete.Size = New System.Drawing.Size(58, 20)
        Me.txtPrecioPaquete.TabIndex = 182
        Me.txtPrecioPaquete.Text = "0.00"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(5, 337)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(98, 13)
        Me.Label10.TabIndex = 181
        Me.Label10.Text = "Precio de Paquete:"
        '
        'txtAccion
        '
        Me.txtAccion.Location = New System.Drawing.Point(115, 203)
        Me.txtAccion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtAccion.Multiline = True
        Me.txtAccion.Name = "txtAccion"
        Me.txtAccion.Size = New System.Drawing.Size(400, 46)
        Me.txtAccion.TabIndex = 180
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(5, 207)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(43, 13)
        Me.Label13.TabIndex = 179
        Me.Label13.Text = "Accion:"
        '
        'txtPrecioVenta
        '
        Me.txtPrecioVenta.Location = New System.Drawing.Point(115, 308)
        Me.txtPrecioVenta.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtPrecioVenta.Name = "txtPrecioVenta"
        Me.txtPrecioVenta.Size = New System.Drawing.Size(58, 20)
        Me.txtPrecioVenta.TabIndex = 178
        Me.txtPrecioVenta.Text = "0.00"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(5, 312)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 13)
        Me.Label14.TabIndex = 177
        Me.Label14.Text = "Precio de Venta:"
        '
        'txtStockMinimo
        '
        Me.txtStockMinimo.Location = New System.Drawing.Point(398, 255)
        Me.txtStockMinimo.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtStockMinimo.Name = "txtStockMinimo"
        Me.txtStockMinimo.Size = New System.Drawing.Size(107, 20)
        Me.txtStockMinimo.TabIndex = 175
        Me.txtStockMinimo.Text = "0"
        '
        'txtMarca
        '
        Me.txtMarca.Location = New System.Drawing.Point(115, 256)
        Me.txtMarca.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(197, 20)
        Me.txtMarca.TabIndex = 174
        '
        'txtComposicion
        '
        Me.txtComposicion.Location = New System.Drawing.Point(115, 177)
        Me.txtComposicion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtComposicion.Name = "txtComposicion"
        Me.txtComposicion.Size = New System.Drawing.Size(400, 20)
        Me.txtComposicion.TabIndex = 173
        '
        'txtConcentracion
        '
        Me.txtConcentracion.Location = New System.Drawing.Point(115, 106)
        Me.txtConcentracion.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtConcentracion.Name = "txtConcentracion"
        Me.txtConcentracion.Size = New System.Drawing.Size(254, 20)
        Me.txtConcentracion.TabIndex = 170
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(184, 311)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(107, 13)
        Me.Label18.TabIndex = 169
        Me.Label18.Text = "Producto Controlado:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(324, 259)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(74, 13)
        Me.Label19.TabIndex = 168
        Me.Label19.Text = "Stock Minimo:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(5, 259)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 13)
        Me.Label20.TabIndex = 167
        Me.Label20.Text = "Marca:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(5, 179)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 13)
        Me.Label21.TabIndex = 166
        Me.Label21.Text = "Composicion:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(5, 155)
        Me.Label22.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(63, 13)
        Me.Label22.TabIndex = 165
        Me.Label22.Text = "Laboratorio:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(5, 132)
        Me.Label23.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(106, 13)
        Me.Label23.TabIndex = 164
        Me.Label23.Text = "Forma Farmaceutica:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(5, 108)
        Me.Label24.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(79, 13)
        Me.Label24.TabIndex = 163
        Me.Label24.Text = "Concentracion:"
        '
        'txtMedicamento
        '
        Me.txtMedicamento.Location = New System.Drawing.Point(115, 81)
        Me.txtMedicamento.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtMedicamento.Name = "txtMedicamento"
        Me.txtMedicamento.Size = New System.Drawing.Size(346, 20)
        Me.txtMedicamento.TabIndex = 162
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(5, 84)
        Me.Label25.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(74, 13)
        Me.Label25.TabIndex = 161
        Me.Label25.Text = "Medicamento:"
        '
        'txtCodigoBarra
        '
        Me.txtCodigoBarra.Location = New System.Drawing.Point(316, 57)
        Me.txtCodigoBarra.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtCodigoBarra.Name = "txtCodigoBarra"
        Me.txtCodigoBarra.Size = New System.Drawing.Size(189, 20)
        Me.txtCodigoBarra.TabIndex = 160
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(225, 59)
        Me.Label26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(86, 13)
        Me.Label26.TabIndex = 159
        Me.Label26.Text = "Codigo de Barra:"
        '
        'txtRegistro
        '
        Me.txtRegistro.Location = New System.Drawing.Point(115, 57)
        Me.txtRegistro.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.ReadOnly = True
        Me.txtRegistro.Size = New System.Drawing.Size(106, 20)
        Me.txtRegistro.TabIndex = 157
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(5, 59)
        Me.Label27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(49, 13)
        Me.Label27.TabIndex = 158
        Me.Label27.Text = "Registro:"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'form_relacionVademecum
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1184, 479)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "form_relacionVademecum"
        Me.Text = "Relacion Vademecum - Medicamento"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAccionVademecum As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtMarcaVademecum As System.Windows.Forms.TextBox
    Friend WithEvents txtComposicionVademecum As System.Windows.Forms.TextBox
    Friend WithEvents txtLaboratorioVademecum As System.Windows.Forms.TextBox
    Friend WithEvents txtFormaFarmaceuticaVademecum As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMedicamentoVademecum As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbLaboratorio As System.Windows.Forms.ComboBox
    Friend WithEvents chkControlado As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadPaquete As System.Windows.Forms.TextBox
    Friend WithEvents txtPrecioPaquete As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtAccion As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtPrecioVenta As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtStockMinimo As System.Windows.Forms.TextBox
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents txtComposicion As System.Windows.Forms.TextBox
    Friend WithEvents txtConcentracion As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtMedicamento As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtRegistro As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents txtPrecioCompra As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cmbFormaFarmaceutica As System.Windows.Forms.ComboBox
    Friend WithEvents txtConcentracionVademecum As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents chkActualizar As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents lblLaboratorio As System.Windows.Forms.Label
    Friend WithEvents lblFormaFarmaceutica As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class
