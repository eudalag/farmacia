﻿Imports CAPA_DATOS
Imports System.Transactions
Public Class modalKardex
    Private idMedicamento As Integer

    Public Sub New(ByVal _idMedicamento As Integer)
        InitializeComponent()
        idMedicamento = _idMedicamento
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click        
        Try
            Using scope As New TransactionScope()
                insertarKardex()
                scope.Complete()
            End Using

            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            Dim title As String
            Dim style As MsgBoxStyle
            Dim response As MsgBoxResult
            style = MsgBoxStyle.OkOnly Or MsgBoxStyle.Critical
            title = "Error Inconsistencia en la base de datos"
            Dim msg = ex.Message.ToString
            response = MsgBox(msg, style, title)
        End Try
    End Sub

    Private Sub insertarKardex()
        Dim tblKardex = New datos()
        tblKardex.tabla = "KARDEX"
        tblKardex.tabla = "KARDEX"
        tblKardex.campoID = "ID_KARDEX"
        tblKardex.modoID = "sql"
        tblKardex.agregarCampoValor("NRO_LOTE", IIf(CBool(chkSinLote.Checked), "null", txtNroLote.Text))
        Dim fecha As String
        fecha = txtFechaVencimiento.Text
        tblKardex.agregarCampoValor("F_VENCIMIENTO", IIf(CBool(chkSinLote.Checked), "null", fecha))
        tblKardex.agregarCampoValor("SIN_LOTE", chkSinLote.Checked)
        tblKardex.agregarCampoValor("ID_MEDICAMENTO", idMedicamento)
        tblKardex.insertar()
    End Sub

    Private Sub chkActualizarKardex_CheckedChanged(sender As Object, e As EventArgs) Handles chkSinLote.CheckedChanged
        If chkSinLote.Checked Then
            txtFechaVencimiento.Enabled = False
            txtNroLote.ReadOnly = True
        Else
            txtFechaVencimiento.Enabled = True
            txtNroLote.ReadOnly = False
        End If
    End Sub
End Class