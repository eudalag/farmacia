﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Public Class form_medicamentos
    Private mod_conf As modulo = New modulo()
    Private dtSourceGeneral As DataTable
    Private dtSource As DataTable    
    Private tipo As Integer

    Private InString As String
    Private PistolaEnviando As Boolean = False
    Public Sub New(Optional _tipo As Integer = 1)
        InitializeComponent()
        iniciarComboLaboratorio()
        tipo = _tipo
    End Sub
    Protected Sub iniciarComboLaboratorio()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = "SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO UNION ALL"
        sSql += " SELECT -1 , '[Seleccione un Laboratorio]' ORDER BY DESCRIPCION"

        cmbLaboratorio.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
    End Sub
    
    Private Sub LoadPage()
       
        dgvGrilla.DataSource = dtSource

    End Sub
    

    Private Sub inicializarParametros()        
        With (dgvGrilla)
            .Width = 1150
            .Height = 400
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            Select Case tipo
                Case 1
                    .ColumnCount = 7
                    .Columns(0).Name = "Registro"
                    .Columns(0).HeaderText = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Width = 90

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 350


                    .Columns(2).Name = "laboratorio"
                    .Columns(2).HeaderText = "Laboratorio"
                    .Columns(2).DataPropertyName = "LABORATORIO"
                    .Columns(2).Width = 120

                    .Columns(3).Name = "Composicion"
                    .Columns(3).HeaderText = "Composición"
                    .Columns(3).DataPropertyName = "COMPOSICION"
                    .Columns(3).Width = 270

                    .Columns(4).Name = "tipo_medicamento"
                    .Columns(4).HeaderText = "Form. Farm."
                    .Columns(4).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
                    .Columns(4).Width = 120

                    .Columns(5).Name = "minimo"
                    .Columns(5).HeaderText = "Stock Min."
                    .Columns(5).DataPropertyName = "MINIMO"
                    .Columns(5).Width = 100
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

                    .Columns(6).Name = "precio_unitario"
                    .Columns(6).HeaderText = "Prec. Unit"
                    .Columns(6).DataPropertyName = "PRECIO_UNITARIO"
                    .Columns(6).Width = 100
                    .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                Case 2
                    .ColumnCount = 6
                    .Columns(0).Name = "Registro"
                    .Columns(0).HeaderText = "Registro"
                    .Columns(0).DataPropertyName = "ID_MEDICAMENTO"
                    .Columns(0).Width = 90

                    .Columns(1).Name = "nombre"
                    .Columns(1).HeaderText = "Nombre"
                    .Columns(1).DataPropertyName = "MEDICAMENTO"
                    .Columns(1).Width = 320


                    .Columns(2).Name = "laboratorio"
                    .Columns(2).HeaderText = "Laboratorio"
                    .Columns(2).DataPropertyName = "LABORATORIO"
                    .Columns(2).Width = 120

                    .Columns(3).Name = "Composicion"
                    .Columns(3).HeaderText = "Composición"
                    .Columns(3).DataPropertyName = "COMPOSICION"
                    .Columns(3).Width = 370

                    .Columns(4).Name = "tipo_medicamento"
                    .Columns(4).HeaderText = "Form. Farm."
                    .Columns(4).DataPropertyName = "DESC_TIPO_MEDICAMENTO"
                    .Columns(4).Width = 120

                    .Columns(5).Name = "minimo"
                    .Columns(5).HeaderText = "Stock"
                    .Columns(5).DataPropertyName = "STOCK"
                    .Columns(5).Width = 100
                    .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            End Select            
        End With
        LoadPage()
    End Sub

    Private Sub form_medicamentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        Select Case tipo
            Case 1
                Dim tMedicamento As eMedicamento = New eMedicamento()
                dtSourceGeneral = tMedicamento.mostrarMedicamento()
            Case 2
                dtSourceGeneral = obtenerMedicamentoStock(-1)
        End Select
        buscar()
        inicializarParametros()
        txtBuscar.Focus()
        If tipo = 2 Then
            btnNuevo.Visible = False
            btnEliminar.Visible = False
        End If
    End Sub
    Function buscar() As DataTable
        dtSourceGeneral = obtenerMedicamentoStock(cmbLaboratorio.SelectedValue)

        Dim cadenaFiltro = ""
        Dim parametros = txtBuscar.Text.Split("*")
        If parametros.Length > 1 Then
            cadenaFiltro += " AND COMPOSICION LIKE '%" + parametros(1) + "%'"
        End If
        If parametros.Length > 2 Then
            cadenaFiltro += " AND DESC_TIPO_MEDICAMENTO LIKE '%" + parametros(2) + "%'"
        End If

        Dim dtaux = dtSourceGeneral
        Dim dtSourceAux As DataTable = dtSourceGeneral
        dtSourceGeneral = dtaux
        Dim tbl = dtSourceGeneral.Select("(MEDICAMENTO like '%" + parametros(0) + "%' OR COMPOSICION LIKE '%" + parametros(0) + "%')" + cadenaFiltro)
        If tbl.Count > 0 Then
            dtSource = tbl.CopyToDataTable
        Else
            dtSource = dtSourceGeneral.Clone
        End If
    End Function
    Function obtenerMedicamentoStock(ByVal idLaboratorio As Integer) As DataTable
        Dim sSql As String = ""

        sSql += " DECLARE @BUSCAR AS NVARCHAR(150),@ID_LABORATORIO AS INTEGER;"
        sSql += " SET @BUSCAR = '" + txtBuscar.Text + "';"
        sSql += " SET @ID_LABORATORIO = " + idLaboratorio.ToString + ";"        
        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " UPPER(L.DESCRIPCION) AS LABORATORIO,L.ID_LABORATORIO, UPPER(M.FORMA_FARMACEUTICA ) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION, "
        sSql += " M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, "
        sSql += " ISNULL(SUM(MK.CANTIDAD),0) AS STOCK,"
        sSql += " ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR"
        sSql += " FROM VW_MEDICAMENTO M"

        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " LEFT JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO "

        sSql += " LEFT JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE L.ID_LABORATORIO = @ID_LABORATORIO AND"
        sSql += " ((M.MEDICAMENTO LIKE '%' + @BUSCAR + '%') OR (M.COMPOSICION LIKE '%' + @BUSCAR + '%')"
        sSql += " OR (L.DESCRIPCION LIKE '%' + @BUSCAR + '%') OR (M.FORMA_FARMACEUTICA   LIKE '%' + @BUSCAR + '%'))"
        sSql += " GROUP BY M.ID_MEDICAMENTO, M.MEDICAMENTO, M.CONCENTRACION,M.MARCA,L.DESCRIPCION, M.FORMA_FARMACEUTICA,M.COMPOSICION,M.MINIMO,"
        sSql += " M.MAXIMO,M.PRECIO_UNITARIO,M.CANTIDAD_PAQUETE,M.PRECIO_PAQUETE,L.ID_LABORATORIO"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Me.Close()
        Dim form_gestion = New form_gestionMedicamento()
        mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
    End Sub

    Private Sub dgvGrilla_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGrilla.CellDoubleClick

        Dim idMedicamento As Integer = CType(dgvGrilla.Rows(e.RowIndex).Cells(0), DataGridViewTextBoxCell).Value
        If tipo = 1 Then
            Me.Close()
            Dim form_gestion As form_gestionMedicamento = New form_gestionMedicamento(idMedicamento)
            mod_conf.cambiarForm(form_principal_adminitrador.panelContenedor, form_gestion)
        Else
            Using kardex = New form_kardex(idMedicamento)
                If DialogResult.OK = kardex.ShowDialog() Then

                End If
            End Using
        End If
        
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim idMedicamento As Integer = 0
        idMedicamento = dgvGrilla.SelectedCells(0).Value
        Dim tMedicamento As eMedicamento = New eMedicamento()
        tMedicamento.ID_MEDICAMENTO = idMedicamento
        tMedicamento.eliminar()
        If tMedicamento.mensajesError = "" Then
            dtSourceGeneral = tMedicamento.mostrarMedicamento()
            buscar()            
            LoadPage()
        Else
            mod_conf.mensajeError("Error", tMedicamento.mensajesError)
        End If
    End Sub

    Private Sub cargarDatos()
        Select Case tipo
            Case 1
                Dim tMedicamento As eMedicamento = New eMedicamento()
                'dtSourceGeneral = tMedicamento.mostrarMedicamento(txtBuscar.Text, -1, chkControlado.Checked, -1, cmbLaboratorio.SelectedValue)
            Case 2
                dtSourceGeneral = obtenerMedicamentoStock(cmbLaboratorio.SelectedValue)
        End Select        
        LoadPage()
    End Sub
   

    Private Sub cmbLaboratorio_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub
    
    Private Sub form_medicamentos_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Dim carAsc As Integer
        carAsc = Asc(e.KeyChar)
        If carAsc = 169 Then
            InString = ""
            PistolaEnviando = True
            e.Handled = True
        ElseIf PistolaEnviando = True Then
            If carAsc = 174 Then
                PistolaEnviando = False
                'dgvGrilla.SelectedCells(1).Value = InString
                Dim med As DataTable = mod_conf.obtenerMedicamento(InString, 2)
                txtBuscar.Text = med.Rows(0)("NOMBRE")
                cmbLaboratorio.SelectedValue = med.Rows(0)("ID_LABORATORIO")

            Else
                InString &= e.KeyChar.ToString
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If IsNumeric(cmbLaboratorio.SelectedValue) Then
            buscar()            
            LoadPage()
        End If
    End Sub
End Class