﻿Imports System.Data
Imports CAPA_NEGOCIO

Public Class form_sesion

    Public Shared cusuario As eUsuario
    Public Shared cConfiguracion As eConfiguracion
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        cusuario = New eusuario()
        cusuario.usuario = txtUsuario.Text
        cusuario.CONTRASENHA = New modulo().GenerarHash(txtContrasenha.Text)
        cusuario.sesion()

        variableSesion.idUsuario = cusuario.ID_USUARIO
        variableSesion.Usuario = cusuario.USUARIO
        variableSesion.idBanco = New generico().obtenerBancoFarmacia(cusuario.ID_USUARIO)

        Dim msg As String
        Dim title As String
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        If cusuario.mensajesError = "" Then
            If CBool(cusuario.CAMBIO_CONTRASENHA) Then
                Using obliga_cambio = New form_cambiar_contrasenha()
                    If DialogResult.OK = obliga_cambio.ShowDialog() Then
                        abrirFormulario()
                    End If
                End Using
            Else
                abrirFormulario()
            End If
        Else
            msg = cusuario.mensajesError
            style = MsgBoxStyle.OkOnly Or _
               MsgBoxStyle.Critical
            title = "Error"   ' Define title.
            response = MsgBox(msg, style, title)            
        End If
    End Sub
    Private Sub abrirFormulario()
        txtUsuario.Focus()
        Me.Hide()
        Dim tipoUsuario = cusuario.ID_TIPO_USUARIO
        variableSesion.administrador = False
        Select Case tipoUsuario
            Case 1 'Administrador
                form_principal_adminitrador.Show()
                form_principal_adminitrador.lblUsuario.Text = (cusuario.NOMBRE_APELLIDOS).ToUpper
                variableSesion.administrador = True
            Case 2 'Ventas
                form_principal_venta.Text = cusuario.USUARIO
                form_principal_venta.Show()
                form_principal_venta.lblUsuario.Text = (cusuario.NOMBRE_APELLIDOS).ToUpper
            Case 3 'Medico

        End Select        
        cConfiguracion = New eConfiguracion()
    End Sub
End Class
