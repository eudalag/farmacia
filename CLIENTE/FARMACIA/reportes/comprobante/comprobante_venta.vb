﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Public Class comprobante_venta
    Private tPedido As eVenta
    Private _idVenta As Integer
    Private _fecha As Date

    Public Shared dtSource As DataTable
    Public Sub New(ByVal idVenta As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().



        _idVenta = idVenta
        cargarDatos()
    End Sub
    Protected Sub cargarDatos()
        tPedido = New eVenta()


        Dim tabla As DataTable = tPedido.cargarDatosPedido(_idVenta)
        txtCliente.Text = tabla.Rows(0)("NOMBRE_CLIENTE")

        txtNit.Text = tabla.Rows(0)("NIT")
        txtTipo.Text = tabla.Rows(0)("DETALLE")
        txtFecha.Text = tabla.Rows(0)("FECHA")
        txtNro.Text = tabla.Rows(0)("NRO_VENTA")
        chkCredito.Checked = CBool(tabla.Rows(0)("CREDITO"))
        txtTotal.Text = tabla.Rows(0)("TOTAL_PEDIDO")
        txtTotalCancelado.Text = tabla.Rows(0)("PAGADO")
        txtTotalDescuento.Text = tabla.Rows(0)("TOTAL_DESCUENTO")
        If CInt(tabla.Rows(0)("ID_TIPO_COMPROBANTE")) = 2 Then
            Button1.Visible = False
        Else
            Button1.Visible = True
        End If
    End Sub
    Protected Function obtenerMedicamentos()
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + _idVenta.ToString + ";"

        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " UPPER(M.MEDICAMENTO) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " LABORATORIO, MARCA, UPPER(FORMA_FARMACEUTICA ) AS DESC_TIPO_MEDICAMENTO, UPPER(M.COMPOSICION) AS COMPOSICION, "
        sSql += " M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, ISNULL(CAST(M.PRECIO_UNITARIO AS nvarchar(18)),'') AS PRECIO_UNITARIO, "
        sSql += " ISNULL(S.CANTIDAD,0) AS STOCK,R.CANTIDAD,R.PRECIO_UNITARIO * R.CANTIDAD AS TOTAL,"
        sSql += " ISNULL(M.CANTIDAD_PAQUETE,1) AS CANTIDAD_MAYOR, ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO ) AS PRECIO_MAYOR, ISNULL(CODIGO_BARRA,'') AS CODIGO_BARRA"
        sSql += " FROM VW_MEDICAMENTO M "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS  R ON R.ID_MEDICAMENTO = M.ID_MEDICAMENTO   "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = R.ID_VENTA "
        sSql += " LEFT JOIN INVENTARIO S ON S.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE V.ID_VENTA =  @ID_VENTA "
        Return datos.ejecutarConsulta(sSql)
    End Function

    Private Sub inicializarParametros()
        
        With (dgvGrilla)            
            .ReadOnly = True
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim colMedicamento As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colMedicamento)
            With colMedicamento
                .Name = "nombre"
                .HeaderText = "Detalle"
                .DataPropertyName = "MEDICAMENTO"
                .Width = 200
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colLaboratorio As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colLaboratorio)
            With colLaboratorio
                .Name = "laboratorio"
                .HeaderText = "Laboratorio"
                .DataPropertyName = "LABORATORIO"
                .Width = 100
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            End With

            Dim colCantidad As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colCantidad)
            With colCantidad
                .Name = "cantidad"
                .HeaderText = "Cant."
                .DataPropertyName = "CANTIDAD"
                .Width = 50
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With

            Dim colPrecioUnitario As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colPrecioUnitario)
            With colPrecioUnitario
                .Name = "precio_unitario"
                .HeaderText = "P. Uni."
                .DataPropertyName = "PRECIO_UNITARIO"
                .Width = 80
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colTotal As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colTotal)
            With colTotal
                .Name = "total"
                .HeaderText = "Total"
                .Width = 80
                .DataPropertyName = "TOTAL"
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            End With

            Dim colRegistro As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
            .Columns.Add(colRegistro)
            With colRegistro
                .Name = "registro"
                .DataPropertyName = "ID_MEDICAMENTO"
                .Visible = False
            End With

        End With
        dgvGrilla.DataSource = dtSource
    End Sub

    Private Sub comprobante_venta_Load(sender As Object, e As EventArgs) Handles Me.Load
        dtSource = obtenerMedicamentos()
        inicializarParametros()
    End Sub
   
    
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        PrintReport()
    End Sub

    Private Sub PrintReport()
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim print = New imprimir(_idVenta, IIf(chkDetalle.Checked, 2, 1))
        print.SetInvoiceHead(e.Graphics)
        print.SetOrderData(e.Graphics)
        print.SetInvoiceData(e.Graphics, e)        
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim form As emitirFactura = New emitirFactura(_idVenta)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                cargarDatos()
            End If
        End Using
    End Sub
End Class