﻿Imports CAPA_DATOS
Imports CAPA_NEGOCIO
Imports System.Transactions
Public Class emitirFactura
    Private tCliente As eCliente
    Public Shared _nit As String
    Public Shared idVenta As String


    Public Sub New(ByVal _idVenta As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        idVenta = _idVenta
        tCliente = New eCliente()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub cargarDatos()
        Dim sSql As String = ""
        sSql += " DECLARE @VENTA AS INTEGER"
        sSql += " SET @VENTA = " + idVenta.ToString + ";"
        sSql += " SELECT TOTAL_PEDIDO, EFECTIVO FROM VENTA WHERE ID_VENTA = @VENTA"
        Dim tbl = New datos().ejecutarConsulta(sSql)
        If tbl.Rows.Count > 0 Then
            txtTotalPagar.Text = tbl.Rows(0)("TOTAL_PEDIDO")
            txtTotalCancelado.Text = tbl.Rows(0)("EFECTIVO")
        End If
    End Sub

    Private Sub txtNit_TextChanged(sender As Object, e As EventArgs) Handles txtNit.TextChanged

        tCliente.buscarClientePedido(txtNit.Text)
        Dim cliente_factura = ""
        If tCliente.NOMBRE_FACTURA = "" Then
            If tCliente.PERSONA.NOMBRE_APELLIDOS = "" Then
                txtCliente.Text = ""
            Else
                txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
            End If
        Else
            txtCliente.Text = tCliente.NOMBRE_FACTURA
        End If
    End Sub

    Private Sub btnNuevoCliente_Click(sender As Object, e As EventArgs) Handles btnNuevoCliente.Click
        Dim form As form_gestionCliente = New form_gestionCliente(txtNit.Text, True, 2)
        Using form
            If DialogResult.OK = form.ShowDialog() Then
                txtNit.Text = _nit
                Dim cliente_buscar = New eCliente()
                cliente_buscar.buscarClientePedido(txtNit.Text)
                If cliente_buscar.PERSONA.NOMBRE_APELLIDOS <> "" Then
                    tCliente = cliente_buscar
                    txtCliente.Text = tCliente.PERSONA.NOMBRE_APELLIDOS
                Else
                    txtCliente.Text = ""
                End If
            End If
        End Using
    End Sub

    Private Sub emitirFactura_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtFecha.Text = Now.ToString("dd/MM/yyyy")
        cargarDatos()
    End Sub

    Protected Sub guardarFactura(ByVal idVenta As Integer)
        Dim tDosificacion As eDosificacion = New eDosificacion()
        Dim tPedido As eVenta = New eVenta()

        tDosificacion.cargarDatosDosificacionActiva()
        Dim nroVenta = tDosificacion.N_FACT_INICIAL + CInt(tPedido.obtenerNroPedido(2, tDosificacion.ID_DOSIFICACION))


        Dim CODIGO_CONTROL As String = ""



        Dim gen_cod_control = New CODIGO_CONTROL()
        gen_cod_control.nro_autorizacion = Trim(tDosificacion.NUMERO_AUTORIZACION)
        gen_cod_control.nit = IIf(txtNit.Text.Length = 0, "0", Trim(txtNit.Text))
        gen_cod_control.fecha = CDate(txtFecha.Text).ToString("yyyyMMdd")
        gen_cod_control.qrFecha = txtFecha.Text
        gen_cod_control.llave = Trim(tDosificacion.LLAVE_DOSIFICACION)
        gen_cod_control.nro_factura = Trim(tPedido.NRO_VENTA)
        gen_cod_control.monto = Math.Round(CDec(Trim(txtTotalPagar.Text)), 0)
        gen_cod_control.qrMonto = txtTotalPagar.Text

        CODIGO_CONTROL = gen_cod_control.getCodigoControl()

        gen_cod_control.empresa = form_sesion.cConfiguracion.NOMBRE
        gen_cod_control.nit_empresa = form_sesion.cConfiguracion.NIT
        gen_cod_control.fecha_lim_emision = tDosificacion.F_LIMITE_EMISION
        gen_cod_control.cliente = txtCliente.Text

        cimgQR.Text = gen_cod_control.datosCodigoQr()


        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()

        cimgQR.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)

        Dim CODIGO_QR = ms.GetBuffer()


        Dim tblDatos = New datos()
        tblDatos.tabla = "FACTURA"
        tblDatos.modoID = "auto"
        tblDatos.campoID = "ID_FACTURA"
        tblDatos.agregarCampoValor("NRO_FACTURA", nroVenta)
        tblDatos.agregarCampoValor("TOTAL_FACTURA", txtTotalPagar.Text)
        tblDatos.agregarCampoValor("ID_CLIENTE", tCliente.ID_CLIENTE)
        tblDatos.agregarCampoValor("FECHA", "serverDateTime")
        tblDatos.agregarCampoValor("ID_DOSIFICACION", tDosificacion.ID_DOSIFICACION)
        tblDatos.agregarCampoValor("CODIGO_CONTROL", CODIGO_CONTROL)
        tblDatos.agregarCampoValor("CODIGO_QR", CODIGO_QR)
        tblDatos.agregarCampoValor("FACTURA_MANUAL", 0)
        tblDatos.agregarCampoValor("ID_VENTA", idVenta)
        tblDatos.agregarCampoValor("ANULADO", 0)
        tblDatos.insertar()

    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Try
            Using scope = New TransactionScope
                guardarFactura(idVenta)
                Dim tblDatos = New datos()
                tblDatos.tabla = "VENTA"
                tblDatos.campoID = "ID_VENTA"
                tblDatos.valorID = idVenta
                tblDatos.agregarCampoValor("ID_TIPO_COMPROBANTE", 2)                
                tblDatos.modificar()
                scope.Complete()
                Me.DialogResult = Windows.Forms.DialogResult.OK
            End Using

        Catch ex As Exception
            Dim mensaje = New modulo()
            mensaje.mensajeError("Error", "Error al generar la factura")
        End Try
       
    End Sub

    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class