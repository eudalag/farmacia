﻿Imports CAPA_DATOS
Public Class ventaLaboratorio_reporte
    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        sSql += " SELECT U.ID_USUARIO, P.NOMBRE_APELLIDOS  FROM USUARIO U "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = U.ID_PERSONA "

        cmbUsuario.DataSource = New datos().ejecutarConsulta(sSql)
        cmbUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        cmbUsuario.ValueMember = "ID_USUARIO"
        cmbUsuario.Refresh()
        If cmbUsuario.Items.Count = 0 Then
            cmbUsuario.ResetText()
        End If

    End Sub

    Private Sub ventaLaboratorio_reporte_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarCombo()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim parametros As String = cmbUsuario.SelectedValue.ToString + ";"
        parametros += txtFechaDesde.Text + ";"
        parametros += txtFechaHasta.Text
        Dim form = New visualizador_reporte(10, parametros)
        form.Show()
    End Sub
End Class