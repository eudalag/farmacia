﻿Imports CAPA_DATOS
Public Class seleccionarCodigoBarra
    Public Sub New(ByVal _nombre As String, ByVal _idMedicamento As Integer)
        InitializeComponent()
        iniciarCombo(_idMedicamento)
        txtNombre.Text = _nombre
    End Sub
    Private Sub iniciarCombo(ByVal idMedicamento As String)
        Dim sSql As String = "SELECT CODIGO_BARRA FROM CODIGO_BARRA"
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento
        cmbBarCode.DataSource = New datos().ejecutarConsulta(sSql)
        cmbBarCode.DisplayMember = "CODIGO_BARRA"
        cmbBarCode.ValueMember = "CODIGO_BARRA"
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        form_inventario.codigoBarra = cmbBarCode.SelectedValue
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class