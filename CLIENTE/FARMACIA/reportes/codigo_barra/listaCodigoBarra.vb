﻿Public Class listaCodigoBarra
    Private item1
    Private item2
    Private item3
    Private item4
    Shared contItem As Integer = 0
    Shared fila As Integer = 1
    Shared columna As Integer = 0
    Public Sub New(ByVal _tblItems As DataTable)
        InitializeComponent()
        inicializarParametrosCodigoBarra()
        dgvGrilla.DataSource = _tblItems
    End Sub
    Private Sub inicializarParametrosCodigoBarra()
        With (dgvGrilla)
            .Width = 600
            .Height = 286
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .AutoGenerateColumns = False
            .ColumnCount = 5
            .Columns(0).Name = "NOMBRE"
            .Columns(0).HeaderText = "Medicamento"
            .Columns(0).DataPropertyName = "NOMBRE"
            .Columns(0).Width = 300
            .Columns(0).ReadOnly = True

            .Columns(1).Name = "FORM_FARM"
            .Columns(1).HeaderText = "Form. Farm."
            .Columns(1).DataPropertyName = "FORM_FARM"
            .Columns(1).Width = 125
            .Columns(1).ReadOnly = True

            .Columns(2).Name = "CODIGO_BARRA"
            .Columns(2).HeaderText = "Codigo Barra"
            .Columns(2).DataPropertyName = "CODIGO_BARRA"
            .Columns(2).Width = 135
            .Columns(2).ReadOnly = True

            .Columns(3).Name = "CANTIDAD"
            .Columns(3).HeaderText = "Cant."
            .Columns(3).DataPropertyName = "CANTIDAD"
            .Columns(3).Width = 50
            .Columns(3).ReadOnly = False

            .Columns(4).Name = "ID_MEDICAMENTO"
            .Columns(4).DataPropertyName = "ID_MEDICAMENTO"
            .Columns(4).Visible = False
        End With
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        Dim report = New visualizador_reporte(5, obtenerCodigoBarra())
        report.ShowDialog()
    End Sub
    Protected Function crearTabla() As DataTable
        Dim tblItems = New DataTable
        Dim columna As DataColumn

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Int32")
        columna.ColumnName = "nro_fila"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Byte[]")
        columna.ColumnName = "item1"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Byte[]")
        columna.ColumnName = "item2"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Byte[]")
        columna.ColumnName = "item3"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.Byte[]")
        columna.ColumnName = "item4"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "nombre1"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "nombre2"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "nombre3"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "nombre4"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "codigo_form_farm1"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "codigo_form_farm2"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "codigo_form_farm3"
        tblItems.Columns.Add(columna)

        columna = New DataColumn()
        columna.DataType = System.Type.GetType("System.String")
        columna.ColumnName = "codigo_form_farm4"
        tblItems.Columns.Add(columna)


        Return tblItems
    End Function
    Public Shared Sub nuevoItem(ByRef tblItems As DataTable, ByVal idatos As Byte(), ByVal nombre As String, ByVal codigo_form_farm As String)

        'Dim row As DataRow
        Dim row = (From tbl In tblItems Where tbl!nro_fila = fila)

        If row.Count > 0 Then
            Select Case columna
                Case 1
                    row(0)("item1") = idatos
                    row(0)("nombre1") = nombre
                    row(0)("codigo_form_farm1") = codigo_form_farm
                    columna += 1
                Case 2
                    row(0)("item2") = idatos
                    row(0)("nombre2") = nombre
                    row(0)("codigo_form_farm2") = codigo_form_farm
                    columna += 1
                Case 3
                    row(0)("item3") = idatos
                    row(0)("nombre3") = nombre
                    row(0)("codigo_form_farm3") = codigo_form_farm
                    columna += 1
                Case 4
                    row(0)("item4") = idatos
                    row(0)("nombre4") = nombre
                    row(0)("codigo_form_farm4") = codigo_form_farm
                    columna = 1
                    fila += 1
            End Select
        Else
            columna = 2
            Dim dr = tblItems.NewRow
            dr("nro_fila") = fila
            dr("item1") = idatos
            dr("nombre1") = nombre
            dr("codigo_form_farm1") = codigo_form_farm
            tblItems.Rows.Add(dr)
        End If
        tblItems.AcceptChanges()
    End Sub
    Function obtenerCodigoBarra() As DataTable
        Dim tblItem As DataTable = crearTabla()
        Dim cont As Integer
        Dim datos As Byte()
        Dim cantidad As Integer
        Dim CODIGO_BARRA As String
        Dim codigo_form_farm As String
        For Each row As DataGridViewRow In dgvGrilla.Rows
            cont = 1
            cantidad = row.Cells("CANTIDAD").Value
            CODIGO_BARRA = row.Cells("CODIGO_BARRA").Value
            datos = New codigoBarra().cargarImagen(CODIGO_BARRA)
            codigo_form_farm = Microsoft.VisualBasic.Right(New String("0", 6) + row.Cells("ID_MEDICAMENTO").Value.ToString, 6) + "-"
            codigo_form_farm += row.Cells("FORM_FARM").Value
            For i As Integer = 1 To cantidad
                nuevoItem(tblItem, datos, row.Cells("NOMBRE").Value, codigo_form_farm)
            Next
        Next
        Return tblItem
    End Function
End Class