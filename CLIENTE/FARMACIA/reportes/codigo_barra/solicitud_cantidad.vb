﻿Imports CAPA_DATOS
Public Class solicitud_cantidad

    Private codigoBarra As String
    Private nombre As String
    Public Sub New(ByVal _codigoBarra As String, ByVal _nombre As String, ByVal _idMedicamento As Integer, ByVal _cantidad As Integer)
        InitializeComponent()
        iniciarCombo(_idMedicamento)
        If _codigoBarra <> -1 Then
            cmbBarCode.SelectedValue = _codigoBarra
        End If


        nombre = _nombre
        txtCantidad.Text = _cantidad
    End Sub
    Private Sub iniciarCombo(ByVal idMedicamento As String)
        Dim sSql As String = "SELECT CODIGO_BARRA FROM CODIGO_BARRA"
        sSql += " WHERE ID_MEDICAMENTO = " + idMedicamento
        cmbBarCode.DataSource = New datos().ejecutarConsulta(sSql)
        cmbBarCode.DisplayMember = "CODIGO_BARRA"
        cmbBarCode.ValueMember = "CODIGO_BARRA"
    End Sub
    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If IsNumeric(txtCantidad.Text) Then
            Me.Close()
            Dim datos As String = ""
            datos += nombre + ";"
            datos += cmbBarCode.SelectedValue + ";"
            datos += IIf(Trim(txtCantidad.Text).Length = 0, 0, txtCantidad.Text)
            Dim report = New visualizador_reporte(5, datos)
            report.ShowDialog()
        Else
            Dim mod_conf = New modulo()
            mod_conf.mensajeError("NUMERO INVALIDO", "El monto o numero ingresado no es correcto")
        End If

    End Sub
End Class