﻿Imports CAPA_DATOS
Imports CrystalDecisions.CrystalReports.Engine
Public Class visualizadorKardexMedicamento
    Private idMedicamento As Integer
    Private Sub visualizadorKardexMedicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim crystalReport As rptKardexMedicamento = New rptKardexMedicamento()
        'crystalReport.SetDataSource(obtenerKardexMedicamento())
        'reportViewer.ReportSource = crystalReport
        'reportViewer.RefreshReport()

        Dim crystalReport As New rptKardexMedicamento()
        'Dim dsCustomers As Customers = GetData()
        crystalReport.SetDataSource(obtenerKardexMedicamento())
        Me.reportViewer.ReportSource = crystalReport
        Me.reportViewer.RefreshReport()
    End Sub
    Function obtenerKardexMedicamento() As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER, @ID_KARDEX AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = 2026;"
        sSql += " SET @ID_KARDEX =  -1 ;"
        sSql += " SELECT REGISTRO, COD_BARRA AS CODIGO_BARRA, NOMBRE AS MEDICAMENTO, CONCENTRACION, FORMA_FARMACEUTICA, LABORATORIO,"
        sSql += " COMPOSICION, MARCA, MINIMO AS STOCK_MIN, MAXIMO AS STOCK_MAX, CONTROLADO, NRO_LOTE AS NRO_KARDEX, F_VENCIMIENTO,"
        sSql += " FECHA AS F_MOVIMIENTO, ENTRADA, SALIDA, MOVIMIENTO AS TIPO, USUARIO   FROM ("
        sSql += " SELECT TOP 1 M.ID_MEDICAMENTO,RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS NVARCHAR(10)),6) AS REGISTRO,ISNULL(CB.CODIGO_BARRA,'') AS COD_BARRA,"
        sSql += " M.NOMBRE, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, TM.DESCRIPCION AS FORMA_FARMACEUTICA,L.DESCRIPCION AS LABORATORIO,"
        sSql += " M.COMPOSICION, M.MARCA, M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, CASE WHEN M.CONTROLADO = 0 THEN 'No' ELSE 'Si' END AS CONTROLADO  FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO"
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " LEFT JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO  "
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO) CABECERA "
        sSql += " INNER JOIN ("
        sSql += " SELECT K.ID_MEDICAMENTO ,MK.FECHA, TM.DESCRIPCION AS MOVIMIENTO,K.NRO_LOTE, CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103) AS F_VENCIMIENTO ,"
        sSql += " CASE WHEN (MK.CANTIDAD > 0) THEN MK.CANTIDAD ELSE 0 END AS ENTRADA, "
        sSql += " CASE WHEN (MK.CANTIDAD < 0) THEN MK.CANTIDAD ELSE 0 END AS SALIDA,"
        sSql += " U.USUARIO FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " INNER JOIN TIPO_MOVIMIENTO TM ON TM.ID_TIPO_MOVIMIENTO = MK.ID_TIPO_MOVIMIENTO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = MK.ID_USUARIO "
        sSql += " WHERE K.ID_MEDICAMENTO = @ID_MEDICAMENTO AND K.ID_KARDEX = CASE WHEN @ID_KARDEX = -1 THEN K.ID_KARDEX ELSE @ID_KARDEX END)"
        sSql += " AS MOVIMIENTO ON MOVIMIENTO.ID_MEDICAMENTO = CABECERA.ID_MEDICAMENTO "
        Return tbl.ejecutarConsulta(sSql)
    End Function
End Class