﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms
Public Class form_reporte_caja
    Dim id_caja As Integer
    Public Sub New(ByVal _idCaja As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        id_caja = _idCaja
    End Sub
    Private Sub form_reporte_caja_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @ID_CAJA AS INTEGER;"
        sSql += " SET @ID_CAJA = " & id_caja & ";"
        sSql += " SELECT 1 AS ORDEN,CASE WHEN F_CIERRE IS NULL THEN 'Abierta' ELSE 'Cerrada' END AS ESTADO,FECHA,IMPORTE_APERTURA,ISNULL(IMPORTE_CIERRE,0) AS IMPORTE_CIERRE ,NRO_VENTA,P.NOMBRE_APELLIDOS,C.NIT,SUM(R.CANTIDAD*R.PRECIO_UNITARIO) AS TOTAL_PEDIDO  FROM VENTA V"
        sSql += " INNER JOIN CAJA CA ON CA.ID_CAJA = V.ID_CAJA "
        sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        sSql += " WHERE V.ID_CAJA = @ID_CAJA "
        sSql += " GROUP BY FECHA,NRO_VENTA, NOMBRE_APELLIDOS, NIT,IMPORTE_APERTURA,IMPORTE_CIERRE,F_CIERRE  "
        sSql += " UNION"
        sSql += " SELECT CASE M.ID_TIPO WHEN 1 THEN 2 WHEN 3 THEN 3 ELSE 4 END AS ORDEN,CASE WHEN F_CIERRE IS NULL THEN 'Abierta' ELSE 'Cerrada' END AS ESTADO,F_APERTURA,ISNULL(IMPORTE_APERTURA,0) ,IMPORTE_CIERRE ,0,'Movimiento de ' + t.DESCRIPCION AS NOMBRE_APELLIDOS,'',CASE M.ID_TIPO WHEN 2 THEN IMPORTE * -1 ELSE IMPORTE END AS TOTAL_PEDIDO  FROM MOVIMIENTO M"
        sSql += " INNER JOIN CAJA C ON C.ID_CAJA = M.ID_CAJA "
        sSql += " INNER JOIN TIPO T ON T.ID_TIPO = M.ID_TIPO "
        sSql += " WHERE M.ID_CAJA = @ID_CAJA "
        sSql += " ORDER BY ORDEN "
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("dsMovimientos", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class