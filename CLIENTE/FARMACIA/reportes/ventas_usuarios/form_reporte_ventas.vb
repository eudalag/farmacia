﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms

Public Class form_reporte_ventas
    Private fechasDesde As String
    Private fechaHasta As String
    Private usuario As Integer    


    Public Sub New(ByVal _fDesde As String, ByVal _fHasta As String, Optional ByVal _usuario As Integer = 0)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        fechasDesde = _fDesde
        fechaHasta = _fHasta
        usuario = _usuario
    End Sub
    Private Sub form_informe_ventas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'dsVentas.VW_VENTAS' Puede moverla o quitarla según sea necesario.
        Dim sSql As String = ""
        sSql += " DECLARE @F_DESDE AS DATE, @F_HASTA AS DATE, @USUARIO AS INTEGER;"
        sSql += " SET @F_DESDE = CONVERT(DATE,'" + fechasDesde + "',103);"
        sSql += " SET @F_HASTA = CONVERT(DATE,'" + fechaHasta + "',103);"
        sSql += " SET @USUARIO = " + usuario.ToString + ";"
        sSql += " SELECT M.ID_MEDICAMENTO, R.CANTIDAD, TM.DESCRIPCION AS FORMA_FARMACEUTICA,M.NOMBRE AS MEDICAMENTO,M.MARCA,M.LABORATORIO,R.PRECIO_UNITARIO,"
        sSql += " P.NOMBRE_APELLIDOS,U.ID_USUARIO,TC.DETALLE     FROM VENTA V "
        sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = V.ID_USUARIO "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = U.ID_PERSONA "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " WHERE (V.FECHA >= @F_DESDE AND V.FECHA <= @F_HASTA) AND V.ID_USUARIO = CASE WHEN @USUARIO = -1 THEN V.ID_USUARIO ELSE @USUARIO END AND V.ANULADA = 0"
        sSql += " ORDER BY V.NRO_VENTA "
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("dsVentas", dt)
        ReportViewer2.LocalReport.DataSources.Clear()
        ReportViewer2.LocalReport.DataSources.Add(rds)
        ReportViewer2.RefreshReport()
        Me.ReportViewer2.RefreshReport()
    End Sub

    Private Sub form_reporte_ventas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.ReportViewer2.RefreshReport()
    End Sub
End Class