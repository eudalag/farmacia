﻿Imports Microsoft.Reporting.WinForms
Imports CAPA_DATOS

Public Class form_reporte_venta_producto
    Private fechasDesde As Date
    Private fechaHasta As Date

    Public Sub New(ByVal _fDesde As Date, ByVal _fHasta As Date)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        fechasDesde = _fDesde
        fechaHasta = _fHasta
    End Sub
    Private Sub form_reporte_venta_producto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sSql As String = ""
        sSql += " DECLARE @F_DESDE AS DATE, @F_HASTA AS DATE;"
        sSql += " SET @F_DESDE = CONVERT(DATE,'" + fechasDesde.ToString + "',103);"
        sSql += " SET @F_HASTA = CONVERT(DATE,'" + fechaHasta.ToString + "',103);"
        sSql += " SELECT SUM(R.CANTIDAD) AS CANTIDAD, TM.DESCRIPCION AS FORMA_FARMACEUTICA,M.NOMBRE AS MEDICAMENTO,M.MARCA,M.LABORATORIO"
        sSql += " FROM VENTA V "
        sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = V.ID_USUARIO "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = U.ID_PERSONA "
        sSql += " WHERE (V.FECHA >= @F_DESDE AND V.FECHA <= @F_HASTA) "
        sSql += " GROUP BY TM.DESCRIPCION,M.NOMBRE,M.MARCA,M.LABORATORIO "
        sSql += " ORDER BY CANTIDAD DESC,M.LABORATORIO,M.MARCA,TM.DESCRIPCION,M.NOMBRE"

        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("DataSet1", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class