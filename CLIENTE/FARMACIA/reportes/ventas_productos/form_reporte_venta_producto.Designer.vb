﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_reporte_venta_producto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.dsVentaProducto = New FARMACIA.dsVentaProducto()
        Me.VW_VENTAS_PRODUCTOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dsVentaProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VW_VENTAS_PRODUCTOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DataSet1"
        ReportDataSource1.Value = Me.VW_VENTAS_PRODUCTOBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "FARMACIA.rptVentasProductos.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(670, 262)
        Me.ReportViewer1.TabIndex = 0
        '
        'dsVentaProducto
        '
        Me.dsVentaProducto.DataSetName = "dsVentaProducto"
        Me.dsVentaProducto.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VW_VENTAS_PRODUCTOBindingSource
        '
        Me.VW_VENTAS_PRODUCTOBindingSource.DataMember = "VW_VENTAS_PRODUCTO"
        Me.VW_VENTAS_PRODUCTOBindingSource.DataSource = Me.dsVentaProducto
        '
        'form_reporte_venta_producto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(670, 262)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "form_reporte_venta_producto"
        Me.Text = "form_reporte_venta_producto"
        CType(Me.dsVentaProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VW_VENTAS_PRODUCTOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents VW_VENTAS_PRODUCTOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents dsVentaProducto As FARMACIA.dsVentaProducto
End Class
