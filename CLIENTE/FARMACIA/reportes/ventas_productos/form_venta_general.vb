﻿Imports CAPA_DATOS
Public Class form_venta_general
    Protected Sub iniciarComboUsuario()
        Dim sSql As String = ""
        Dim tblCombo = New datos()

        sSql = " SELECT NOMBRE_APELLIDOS, U.ID_USUARIO  FROM USUARIO U INNER JOIN PERSONA P ON P.ID_PERSONA = U.ID_PERSONA  UNION ALL "
        sSql += " SELECT '[Todos los Usuarios]', -1  ORDER BY NOMBRE_APELLIDOS"

        cmbUsuario.DataSource = tblCombo.ejecutarConsulta(sSql)
        cmbUsuario.DisplayMember = "NOMBRE_APELLIDOS"
        cmbUsuario.ValueMember = "ID_USUARIO"
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim parametros = cmbUsuario.SelectedValue.ToString + ";"
        parametros += txtFechaDesde.Text
        Dim form = New visualizador_reporte(4, parametros)
        form.Show()
    End Sub

    Private Sub form_venta_general_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarComboUsuario()
    End Sub
End Class