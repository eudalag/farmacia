﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms

Public Class form_reporte_medicamento_vencidos
    Private mes As String
    Private anio As String
    Public Sub New(ByVal _mes As String, ByVal _anio As String)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        mes = _mes
        anio = _anio
    End Sub
    Private Sub form_reporte_medicamento_vencidos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " DECLARE @MES AS INTEGER, @ANIO AS INTEGER;"
        sSql += " SET @MES = CAST ('" + mes + "' AS INTEGER);"
        sSql += " SET @ANIO = CAST ('" + anio + "' AS INTEGER);"
        sSql += " SELECT M.ID_MEDICAMENTO, NOMBRE,MARCA,LABORATORIO,FF.DESCRIPCION AS FORMA_FARMACEUTICA,"
        sSql += " ISNULL(II.TOTAL_INICIAL,0) + ISNULL(CM.TOTAL_COMPRA,0) + ISNULL(VM.TOTAL_VENTA,0) AS TOTAL_STOCK,"
        sSql += " II.F_VENCIMIENTO AS VENCIMIENTO_INICIAL, CM.F_VENCIMIENTO AS VENCIMIENTO_COMPRA"
        sSql += " FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_MEDICAMENTO,F_VENCIMIENTO,SUM(CANTIDAD) AS TOTAL_INICIAL FROM INVENTARIO "
        sSql += " WHERE ID_COMPRA IS NULL AND ID_VENTA IS NULL AND NOT F_VENCIMIENTO IS NULL AND CONTROL_FECHA = 1"
        sSql += " AND ((YEAR(F_VENCIMIENTO) < @ANIO) OR (YEAR(F_VENCIMIENTO) = @ANIO AND MONTH(F_VENCIMIENTO) <= @MES))"
        sSql += " GROUP BY ID_MEDICAMENTO, F_VENCIMIENTO"
        sSql += " ) AS II ON II.ID_MEDICAMENTO = M.ID_MEDICAMENTO    "
        sSql += " LEFT JOIN( "
        sSql += " SELECT ID_MEDICAMENTO,FECHA_COMPRA, F_VENCIMIENTO, SUM(CANTIDAD) AS TOTAL_COMPRA  FROM COMPRA_MEDICAMENTO CM"
        sSql += " INNER JOIN INVENTARIO I ON I.ID_COMPRA = CM.ID_COMPRA"
        sSql += " WHERE (YEAR(F_VENCIMIENTO) < @ANIO) OR (YEAR(F_VENCIMIENTO) = @ANIO AND MONTH(F_VENCIMIENTO) <= @MES) "
        sSql += " GROUP BY ID_MEDICAMENTO, FECHA_COMPRA, F_VENCIMIENTO"
        sSql += " ) CM ON CM.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " LEFT JOIN ("
        sSql += " SELECT ID_MEDICAMENTO , SUM (CANTIDAD) AS TOTAL_VENTA FROM VENTA V"
        sSql += " INNER JOIN INVENTARIO I ON I.ID_VENTA = V.ID_VENTA"
        sSql += " WHERE (YEAR(FECHA) < @ANIO) OR (YEAR(FECHA) = @ANIO AND MONTH(FECHA) <= @MES) "
        sSql += " GROUP BY ID_MEDICAMENTO,F_VENCIMIENTO "
        sSql += " ) AS VM ON VM.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE "
        sSql += " (NOT II.F_VENCIMIENTO IS NULL OR NOT CM.F_VENCIMIENTO IS NULL)"
        sSql += " AND (ISNULL(II.TOTAL_INICIAL,0) + ISNULL(CM.TOTAL_COMPRA,0) + ISNULL(VM.TOTAL_VENTA,0) > 0)"
        sSql += " ORDER BY NOMBRE, MARCA,LABORATORIO,FORMA_FARMACEUTICA"
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("MEDICAMENTO", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.SetParameters(New ReportParameter("vencimiento", mes + "/" + anio))
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class