﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_reporte_medicamento_vencidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_reporte_medicamento_vencidos))
        Me.MEDICAMENTOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dsMedicamentosVencidos = New FARMACIA.dsMedicamentosVencidos()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.MEDICAMENTOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dsMedicamentosVencidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MEDICAMENTOBindingSource
        '
        Me.MEDICAMENTOBindingSource.DataMember = "MEDICAMENTO"
        Me.MEDICAMENTOBindingSource.DataSource = Me.dsMedicamentosVencidos
        '
        'dsMedicamentosVencidos
        '
        Me.dsMedicamentosVencidos.DataSetName = "dsMedicamentosVencidos"
        Me.dsMedicamentosVencidos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "MEDICAMENTO"
        ReportDataSource1.Value = Me.MEDICAMENTOBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "FARMACIA.rptMedicamentoVencido.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(733, 674)
        Me.ReportViewer1.TabIndex = 0
        '
        'form_reporte_medicamento_vencidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(733, 674)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "form_reporte_medicamento_vencidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Medicamentos Vencidos"
        CType(Me.MEDICAMENTOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dsMedicamentosVencidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents MEDICAMENTOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents dsMedicamentosVencidos As FARMACIA.dsMedicamentosVencidos
End Class
