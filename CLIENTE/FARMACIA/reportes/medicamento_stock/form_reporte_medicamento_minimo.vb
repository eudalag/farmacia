﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms

Public Class form_reporte_medicamento_minimo

    Private Sub form_reporte_medicamento_minimo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim datos = New datos()
        Dim Sql As String = ""
        Sql += " SELECT NOMBRE, DESCRIPCION , DESC_TIPO_MEDICAMENTO, ISNULL(COMPRA.TOTAL_COMPRA,0) AS TOTAL_COMPRA,ISNULL(VENTA.TOTAL_VENTA,0) AS TOTAL_VENTA  ,SUM(CANTIDAD) AS TOTAL_STOCK , MINIMO  FROM  VW_MEDICAMENTO M "
        Sql += " INNER JOIN INVENTARIO I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        Sql += " LEFT JOIN (SELECT ID_MEDICAMENTO ,SUM(CANTIDAD) AS TOTAL_COMPRA FROM INVENTARIO "
        Sql += " WHERE NOT ID_COMPRA IS NULL AND ID_VENTA IS NULL"
        Sql += " GROUP BY ID_MEDICAMENTO ) COMPRA ON COMPRA.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        Sql += " LEFT JOIN (SELECT ID_MEDICAMENTO ,SUM(CANTIDAD) * - 1 AS TOTAL_VENTA FROM INVENTARIO "
        Sql += " WHERE NOT ID_VENTA IS NULL AND ID_COMPRA IS NULL"
        Sql += " GROUP BY ID_MEDICAMENTO ) VENTA ON VENTA.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        Sql += " WHERE NOT M.MINIMO IS NULL "
        Sql += " GROUP BY NOMBRE, DESCRIPCION , DESC_TIPO_MEDICAMENTO,  MINIMO,TOTAL_COMPRA,TOTAL_VENTA "
        Sql += " HAVING SUM(CANTIDAD) <= MINIMO        "
        Dim dt = datos.ejecutarConsulta(Sql)
        Dim rds As ReportDataSource = New ReportDataSource("MEDICAMENTO", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class