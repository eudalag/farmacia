﻿Imports CAPA_DATOS
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class visualizador_reporte
    Private tipo As Integer
    Private parametros As String
    Private dtSource As DataTable

    Public Sub New(ByVal _tipo As Integer, ByVal _parametros As String)
        InitializeComponent()
        tipo = _tipo
        parametros = _parametros
    End Sub
    Public Sub New(ByVal _tipo As Integer, ByVal tbl As DataTable)
        InitializeComponent()
        tipo = _tipo
        dtSource = tbl
    End Sub
    Private idMedicamento As Integer
    Private Sub visualizadorKardexMedicamento_Load(sender As Object, e As EventArgs) Handles Me.Load
        Select Case tipo
            Case 1 ' KARDEX POR MEDICAMENTO
                Dim crystalReport As New rptKardexMedicamento()
                Dim datos = parametros.Split(";")
                crystalReport.SetDataSource(obtenerKardexMedicamento(datos(0), datos(1)))
                Me.ReportViewer.ReportSource = crystalReport
            Case 2 ' INVENTARIO GENERAL
                Dim crystalReport As New rptInventarioGeneral()
                crystalReport.SetDataSource(obtenerInventario())
                Me.ReportViewer.ReportSource = crystalReport
            Case 3 ' PEDIDO COMPRA
                Dim crystalReport As New rptOrdenCompra()
                crystalReport.SetDataSource(obtenerOrdenCompra(parametros))
                Me.ReportViewer.ReportSource = crystalReport
            Case 4 'VENTAS GENERALES
                Dim crystalReport As New rptVentasGeneral()
                Dim datos = parametros.Split(";")
                crystalReport.SetDataSource(obtenerVentasGenerales(datos(0), datos(1)))
                Me.ReportViewer.ReportSource = crystalReport
            Case 5 'BARCODE
                Dim crystalReport As New barCode()
                crystalReport.SetDataSource(dtSource)
                Me.ReportViewer.ReportSource = crystalReport
            Case 6 'CIERRE DE CAJA
                Dim crystalReport As New rptCierre()
                crystalReport.SetDataSource(obtenerCierre(parametros))
                Me.ReportViewer.ReportSource = crystalReport
            Case 7 'INVENTARIO GENERAL EXISTENCIA
                Dim crystalReport As New rptInventarioGeneral()
                crystalReport.SetDataSource(obtenerInventarioGeneral())
                Me.ReportViewer.ReportSource = crystalReport
            Case 8 ' RECIBO PAGO CREDITO
                Dim crystalReport As New rptCreditoPago()
                crystalReport.SetDataSource(obtenerDatosPago(parametros))
                Me.ReportViewer.ReportSource = crystalReport
                Me.ReportViewer.PrintReport()
            Case 9 ' MOVIMIENTOS
                Dim crystalReport As New rptMovimientos()
                crystalReport.SetDataSource(obtenerMovimientos(parametros))
                crystalReport.SetParameterValue("f_desde", "01/11/2016")
                crystalReport.SetParameterValue("f_hasta", "30/11/2016")
                'Dim crParameterDiscreteValue As ParameterDiscreteValue
                'Dim crParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions
                'Dim crParameterFieldLocation As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition
                '' Get the report parameters collection.  
                'Dim crParameterValues As ParameterValues
                'crParameterFieldDefinitions = crystalReport.DataDefinition.ParameterFields
                '' Add a parameter value - START 
                'crParameterFieldLocation = crParameterFieldDefinitions.Item("usuario")
                'crParameterValues = crParameterFieldLocation.CurrentValues
                'crParameterDiscreteValue =
                'New CrystalDecisions.Shared.ParameterDiscreteValue
                'crParameterDiscreteValue.Value = "Quiero q funcione"
                'crParameterValues.Add(crParameterDiscreteValue)
                'crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                crystalReport.SetParameterValue("usuario", "Jorge Calvimontes")

                Me.ReportViewer.ReportSource = crystalReport
            Case 10 'VENTA LABORATORIO
                Dim crystalReport As New rptVentaLaboratorio()
                Dim datos = parametros.Split(";")
                crystalReport.SetDataSource(obtenerVentasMedicamento(datos(0), datos(1), datos(2)))
                crystalReport.SetParameterValue("desde", datos(1))
                crystalReport.SetParameterValue("hasta", datos(2))
                Me.ReportViewer.ReportSource = crystalReport

            Case 11 'INVENTARIO GENERAL AGRUPADO POR LABORATORIO
                Dim crystalReport As New rptInventarioLaboratorio()
                Dim datos = parametros.Split(";")
                crystalReport.SetDataSource(obtenerInventarioGeneralLaboratorio(datos(0), datos(1), datos(2)))
                crystalReport.SetParameterValue("desde", datos(1))
                crystalReport.SetParameterValue("hasta", datos(2))
                Me.ReportViewer.ReportSource = crystalReport
            Case 12 'CUENTAS POR COBRAR POR GRUPO ECONOMICO
                Try
                    Dim crystalReport As New rptCuentasCobrar()
                    Dim datos = parametros.Split(";")
                    crystalReport.SetDataSource(obtenerCuentasPorCobrar(datos(0), datos(1), datos(2)))
                    crystalReport.SetParameterValue("desde", datos(1))
                    crystalReport.SetParameterValue("hasta", datos(2))
                    Me.ReportViewer.ReportSource = crystalReport
                Catch ex As Exception
                    Dim modulo = New modulo()
                    modulo.mensajeError("Error", ex.ToString)
                End Try
            Case 13 'ESTADO DE RESULTADO
                Try
                    Dim crystalReport As New rptEstadoResultado()
                    Dim datos = parametros.Split(";")
                    crystalReport.SetDataSource(obtenerEstadoResultado(datos(0), datos(1)))
                    crystalReport.SetParameterValue("desde", datos(0))
                    crystalReport.SetParameterValue("hasta", datos(1))
                    Me.ReportViewer.ReportSource = crystalReport
                Catch ex As Exception
                    Dim modulo = New modulo()
                    modulo.mensajeError("Error", ex.ToString)
                End Try
            Case 14 'INFORME VENTAS POR MEDICAMENTO
                Try
                    Dim crystalReport As New rptVentaMedicamentos()
                    Dim datos = parametros.Split(";")
                    Dim tblDatos As DataTable = obtenerVentaMedicamento(datos(0), datos(1), datos(2))
                    crystalReport.SetDataSource(tblDatos)
                    crystalReport.SetParameterValue("desde", datos(0))
                    crystalReport.SetParameterValue("hasta", datos(1))
                    crystalReport.SetParameterValue("laboratorio", IIf(CInt(datos(2)) = -1, "Todos los Laboratorio", CStr(tblDatos(0)(0))))
                    Me.ReportViewer.ReportSource = crystalReport
                Catch ex As Exception
                    Dim modulo = New modulo()
                    modulo.mensajeError("Error", ex.ToString)
                End Try
        End Select
        'Me.ReportViewer.RefreshReport()
    End Sub

    Function obtenerCuentasPorCobrar(ByVal grupoEconomico As Integer, ByVal desde As String, ByVal hasta As String)
        Dim sSql As String = ""

        sSql += " DECLARE @GRUPO AS INTEGER,@DESDE AS DATE, @HASTA AS DATE;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SET @GRUPO = " + grupoEconomico.ToString + ";"
        ''sSql += " SELECT GRUPO_PERSONAS,NRO_CI, NOMBRE_APELLIDOS, TOTAL_CREDITO FROM ("
        ''sSql += " SELECT CL.ID_CLIENTE, P.CELULAR,CL.NIT,P.NRO_CI, P.NOMBRE_APELLIDOS, SUM(C.IMPORTE_CREDITO) - SUM(ISNULL(PC.PAGO,0)) AS TOTAL_CREDITO,GC.GRUPO_PERSONAS,GC.ID_GRUPO    FROM CREDITO C"
        ''sSql += " INNER JOIN VENTA  V ON V.ID_VENTA = C.ID_VENTA "
        ''sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        ''sSql += " INNER  JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA "
        ''sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        ''sSql += " LEFT JOIN (SELECT PC.ID_CREDITO, SUM(PC.IMPORTE) AS PAGO FROM PAGOS P "
        ''sSql += " INNER JOIN PAGO_CREDITO PC ON PC.ID_PAGO = P.ID_PAGO "
        ''sSql += " GROUP BY PC.ID_CREDITO ) PC ON PC.ID_CREDITO = C.ID_CREDITO "
        ''sSql += " WHERE ESTADO = 0 AND"
        ''sSql += " CONVERT(DATE,V.FECHA,103) >= @DESDE AND CONVERT(DATE,V.FECHA,103) <= @HASTA"
        ''sSql += " GROUP BY CL.ID_CLIENTE, P.CELULAR,CL.NIT, P.NOMBRE_APELLIDOS, GC.GRUPO_PERSONAS,GC.ID_GRUPO,P.NRO_CI  )"
        ''sSql += " C WHERE C.TOTAL_CREDITO > 0 AND ID_GRUPO = @GRUPO"
        ''sSql += " ORDER BY NOMBRE_APELLIDOS "

        'sSql += " SELECT NRO_CI,NOMBRE_APELLIDOS,GRUPO_PERSONAS,SUM(TOTAL_PEDIDO) AS TOTAL_CREDITO FROM ("
        'sSql += " SELECT P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CONVERT(NVARCHAR(10),V.FECHA,103) AS FECHA,V.NRO_VENTA,V.TOTAL_PEDIDO "
        'sSql += " FROM VENTA V "
        'sSql += " INNER JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
        'sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA =  CL.ID_PERSONA"
        'sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        'sSql += " WHERE V.CREDITO = 1 AND ANULADA = 0 AND GC.ID_GRUPO = @GRUPO AND"
        'sSql += " CONVERT(DATE,V.FECHA,103) >= @DESDE AND CONVERT(DATE,V.FECHA,103) <= @HASTA"
        'sSql += " UNION ALL"
        'sSql += " SELECT P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CONVERT(NVARCHAR(10),PGS.FECHA ,103) AS FECHA,PG.ID_PAGO ,PG.IMPORTE * -1 AS IMPORTE FROM PAGO_CREDITO PG"
        'sSql += " INNER JOIN PAGOS PGS ON PGS.ID_PAGO = PG.ID_PAGO "
        'sSql += " INNER JOIN CREDITO CR ON CR.ID_CREDITO = PG.ID_CREDITO "
        'sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA =  CL.ID_PERSONA"
        'sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO"
        'sSql += " WHERE CONVERT(DATE,PGS.FECHA,103) >= @DESDE AND CONVERT(DATE,PGS.FECHA,103) <= @HASTA AND GC.ID_GRUPO = @GRUPO "
        'sSql += " UNION ALL"
        'sSql += " SELECT P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CONVERT(NVARCHAR(10),'11/03/2019' ,103) AS FECHA,CR.ID_CREDITO  ,CR.IMPORTE_CREDITO  FROM CREDITO CR"
        'sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA =  CL.ID_PERSONA"
        'sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        'sSql += " WHERE ID_VENTA IS NULL AND GC.ID_GRUPO = @GRUPO AND"
        'sSql += " CONVERT(DATE,CONVERT(NVARCHAR(10),'11/03/2019' ,103),103) >= @DESDE AND CONVERT(DATE,CONVERT(NVARCHAR(10),'11/03/2019' ,103),103) <= @HASTA "
        'sSql += " ) V "
        'sSql += " GROUP BY NOMBRE_APELLIDOS, GRUPO_PERSONAS,NRO_CI "
        'sSql += " HAVING SUM(TOTAL_PEDIDO) > 0"
        '    ORDER BY NOMBRE_APELLIDOS "


        sSql += " SELECT NRO_CI,NOMBRE_APELLIDOS,GRUPO_PERSONAS,SUM(TOTAL_PEDIDO - PAGO) AS TOTAL_CREDITO FROM ("
        sSql += " SELECT P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CONVERT(NVARCHAR(10),V.FECHA,103) AS FECHA,V.NRO_VENTA,V.TOTAL_PEDIDO,SUM(ISNULL(PC.IMPORTE,0)) AS PAGO"
        sSql += " FROM VENTA V "
        sSql += " INNER JOIN CREDITO CR ON CR.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA =  CL.ID_PERSONA"
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " LEFT JOIN PAGO_CREDITO PC ON PC.ID_CREDITO = CR.ID_CREDITO "
        sSql += " WHERE V.CREDITO = 1 AND ANULADA = 0 AND GC.ID_GRUPO = @GRUPO AND"
        sSql += " CONVERT(DATE,V.FECHA,103) >= @DESDE AND CONVERT(DATE,V.FECHA,103) <= @HASTA "
        sSql += " GROUP BY P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,V.FECHA,V.NRO_VENTA,V.TOTAL_PEDIDO"
        sSql += " UNION ALL"
        sSql += " SELECT P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CONVERT(NVARCHAR(10),'11/03/2019' ,103) AS FECHA,CR.ID_CREDITO  ,ISNULL(CR.IMPORTE_CREDITO,0), ISNULL(SUM(PC.IMPORTE),0) AS PAGO FROM CREDITO CR"
        sSql += " INNER JOIN CLIENTE CL ON CR.ID_CLIENTE = CL.ID_CLIENTE"
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA =  CL.ID_PERSONA"
        sSql += " INNER JOIN GRUPO_CLIENTES GC ON GC.ID_GRUPO = CL.ID_GRUPO "
        sSql += " LEFT JOIN PAGO_CREDITO PC ON PC.ID_CREDITO = CR.ID_CREDITO "
        sSql += " WHERE ID_VENTA IS NULL AND GC.ID_GRUPO = @GRUPO AND"
        sSql += " CONVERT(DATE,CONVERT(NVARCHAR(10),'11/03/2019' ,103),103) >= @DESDE AND CONVERT(DATE,CONVERT(NVARCHAR(10),'11/03/2019' ,103),103) <= @HASTA "
        sSql += " GROUP BY P.NRO_CI ,P.NOMBRE_APELLIDOS ,GC.GRUPO_PERSONAS,CR.ID_CREDITO,CR.IMPORTE_CREDITO) V "
        sSql += " GROUP BY NOMBRE_APELLIDOS, GRUPO_PERSONAS,NRO_CI "
        sSql += " HAVING SUM(TOTAL_PEDIDO - PAGO) > 0"
        sSql += " ORDER BY NOMBRE_APELLIDOS "
        Dim tbl = New datos().ejecutarConsulta(sSql)
        Return tbl
    End Function
    Function obtenerEstadoResultado(ByVal desde As String, ByVal hasta As String)
        Dim sSql As String = ""

        sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
       
        sSql += " SELECT CAST(PC1.CODIGO AS NVARCHAR) AS CODIGO1,PC1.CUENTA AS CUENTA1,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) AS CODIGO2,PC2.CUENTA AS CUENTA2,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) AS CODIGO3,PC3.CUENTA AS CUENTA3,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) + '.' +  CAST(PC4.CODIGO AS NVARCHAR)  AS CODIGO4,PC4.CUENTA AS CUENTA4,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) + '.' +  CAST(PC4.CODIGO AS NVARCHAR) + '.' +  CAST(PC.CODIGO AS NVARCHAR) AS CODIGO,PC.CUENTA,"
        sSql += " TM.DESCRIPCION,SUM(MB.IMPORTE) * CASE WHEN SUM(MB.IMPORTE) < 0 THEN -1 ELSE 1 END AS TOTAL    FROM MOVIMIENTO_BANCO MB"
        sSql += " INNER JOIN TIPO_MOVIMIENTO_CAJA TM ON TM.ID_TIPO_MOVIMIENTO = MB.ID_TIPO_MOVIMIENTO "
        sSql += " INNER  JOIN PLAN_CUENTAS PC ON PC.ID_PLAN_CUENTAS  = MB.ID_PLAN_CUENTA "
        sSql += " INNER JOIN PLAN_CUENTAS PC4 ON PC4.ID_PLAN_CUENTAS = PC.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC3 ON PC3.ID_PLAN_CUENTAS = PC4.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC2 ON PC2.ID_PLAN_CUENTAS = PC3.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC1 ON PC1.ID_PLAN_CUENTAS = PC2.ID_PLAN_CUENTA_PADRE  "
        sSql += " WHERE MB.ID_TIPO_MOVIMIENTO IN (1,2,4) AND "
        sSql += " CONVERT(DATE,FH_MOVIMIENTO,103) >= @DESDE AND CONVERT(DATE,FH_MOVIMIENTO,103) <= @HASTA"
        sSql += " GROUP BY PC1.CODIGO ,PC1.CUENTA ,"
        sSql += " PC2.CODIGO  ,PC2.CUENTA  ,"
        sSql += " PC3.CODIGO  ,PC3.CUENTA  ,"
        sSql += " PC4.CODIGO  ,PC4.CUENTA  ,"
        sSql += " PC.CODIGO,PC.CUENTA,"
        sSql += " TM.DESCRIPCION"


        sSql += " UNION ALL "
        sSql += " SELECT CAST(PC1.CODIGO AS NVARCHAR) AS CODIGO1,PC1.CUENTA AS CUENTA1,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) AS CODIGO2,PC2.CUENTA AS CUENTA2,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) AS CODIGO3,PC3.CUENTA AS CUENTA3,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) + '.' +  CAST(PC4.CODIGO AS NVARCHAR)  AS CODIGO4,PC4.CUENTA AS CUENTA4,"
        sSql += " CAST(PC1.CODIGO AS NVARCHAR) + '.' +  CAST(PC2.CODIGO AS NVARCHAR) + '.' +  CAST(PC3.CODIGO AS NVARCHAR) + '.' +  CAST(PC4.CODIGO AS NVARCHAR) + '.' +  CAST(PC.CODIGO AS NVARCHAR) AS CODIGO,PC.CUENTA,"
        sSql += " 'VENTAS NETAS' ,SUM(V.TOTAL_PEDIDO) AS TOTAL  FROM VENTA V"
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = V.ID_CLIENTE"
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = CL.ID_PERSONA"
        sSql += " INNER  JOIN PLAN_CUENTAS PC ON PC.ID_PLAN_CUENTAS  = 38"
        sSql += " INNER JOIN PLAN_CUENTAS PC4 ON PC4.ID_PLAN_CUENTAS = PC.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC3 ON PC3.ID_PLAN_CUENTAS = PC4.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC2 ON PC2.ID_PLAN_CUENTAS = PC3.ID_PLAN_CUENTA_PADRE "
        sSql += " INNER JOIN PLAN_CUENTAS PC1 ON PC1.ID_PLAN_CUENTAS = PC2.ID_PLAN_CUENTA_PADRE  "
        sSql += " WHERE  CONVERT(DATE,FECHA_AUD,103) >= @DESDE AND CONVERT(DATE,FECHA_AUD,103) <= @HASTA"
        sSql += " AND ANULADA = 0"
        sSql += " GROUP BY PC1.CODIGO,PC1.CUENTA,"
        sSql += " PC2.CODIGO,PC2.CUENTA,"
        sSql += " PC3.CODIGO,PC3.CUENTA,"
        sSql += " PC4.CODIGO,PC4.CUENTA,"
        sSql += " PC.CODIGO,PC.CUENTA"

        Dim tbl = New datos().ejecutarConsulta(sSql)
        Return tbl
    End Function

    Function obtenerInventarioGeneralLaboratorio(ByVal idLaboratorio As Integer, ByVal desde As String, ByVal hasta As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @LABORATORIO AS INTEGER, @DESDE AS DATE, @HASTA AS DATE;
        sSql += " SET @LABORATORIO = " + idLaboratorio.ToString + ";"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SELECT M.ID_LABORATORIO, M.LABORATORIO,M.ID_MEDICAMENTO,M.MEDICAMENTO,SUM(VM.CANTIDAD) AS CANTIDAD, VM.PRECIO_UNITARIO, V.FECHA  "
        sSql += " FROM VENTA V  INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA  "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO "
        sSql += " WHERE (CONVERT(DATE,V.FECHA,103) >= @DESDE AND CONVERT(DATE,V.FECHA,103) <= @HASTA) "
        sSql += " AND M.ID_LABORATORIO = @LABORATORIO AND V.ANULADA = 0 "
        sSql += " GROUP BY M.ID_LABORATORIO, M.LABORATORIO,M.ID_MEDICAMENTO,M.MEDICAMENTO, VM.PRECIO_UNITARIO, V.FECHA"
        sSql += " ORDER BY LABORATORIO, FECHA"
        Dim tbl = New datos()
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerVentasMedicamento(ByVal idUsuario As Integer, ByVal fDesde As String, ByVal fHasta As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @USUARIO AS INTEGER, @DESDE AS DATE, @HASTA AS DATE;"
        sSql += " SET @USUARIO = " + idUsuario.ToString + ";"
        sSql += " SET @DESDE = CONVERT (DATE, '" + fDesde + "',103);"
        sSql += " SET @HASTA = CONVERT (DATE, '" + fHasta + "',103); "
        sSql += " SELECT V.FECHA,P.NOMBRE_APELLIDOS AS USUARIO,L.ID_LABORATORIO, L.DESCRIPCION AS LABORATORIO, M.NOMBRE AS MEDICAMENTO,M.CONCENTRACION,TM.DESCRIPCION AS FORMA_FARMACEUTICA,"
        sSql += " VM.CANTIDAD,VM.PRECIO_PAQUETE,VM.PRECIO_UNITARIO,V.TOTAL_PEDIDO   FROM MEDICAMENTO M"
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += "  INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = VM.ID_VENTA "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = V.ID_PERSONA "
        sSql += " WHERE V.ID_USUARIO = @USUARIO"
        sSql += " AND V.FECHA >= @DESDE AND V.FECHA <= @HASTA"
        sSql += " ORDER BY FECHA"
        Dim tbl = New datos()
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerKardexMedicamento(ByVal idMedicamento As Integer, ByVal idKardex As Integer) As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()
        sSql += " DECLARE @ID_MEDICAMENTO AS INTEGER, @ID_KARDEX AS INTEGER;"
        sSql += " SET @ID_MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @ID_KARDEX =  " + idKardex.ToString + ";"
        sSql += " SELECT REGISTRO, COD_BARRA AS CODIGO_BARRA, NOMBRE AS MEDICAMENTO, CONCENTRACION, FORMA_FARMACEUTICA, LABORATORIO,"
        sSql += " COMPOSICION, MARCA, MINIMO AS STOCK_MIN, MAXIMO AS STOCK_MAX, CONTROLADO, NRO_LOTE AS NRO_KARDEX, F_VENCIMIENTO,"
        sSql += " FECHA AS F_MOVIMIENTO, ENTRADA, SALIDA, MOVIMIENTO AS TIPO, USUARIO   FROM ("
        sSql += " SELECT TOP 1 M.ID_MEDICAMENTO,RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS NVARCHAR(10)),6) AS REGISTRO,ISNULL(CB.CODIGO_BARRA,'') AS COD_BARRA,"
        sSql += " M.NOMBRE, ISNULL(M.CONCENTRACION,'') AS CONCENTRACION, TM.DESCRIPCION AS FORMA_FARMACEUTICA,L.DESCRIPCION AS LABORATORIO,"
        sSql += " M.COMPOSICION, M.MARCA, M.MINIMO,ISNULL(M.MAXIMO,0) AS MAXIMO, CASE WHEN M.CONTROLADO = 0 THEN 'No' ELSE 'Si' END AS CONTROLADO  FROM MEDICAMENTO M"
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO"
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " LEFT JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO = M.ID_MEDICAMENTO  "
        sSql += " WHERE M.ID_MEDICAMENTO = @ID_MEDICAMENTO) CABECERA "
        sSql += " INNER JOIN ("
        sSql += " SELECT K.ID_MEDICAMENTO ,MK.FECHA, TM.DESCRIPCION AS MOVIMIENTO,K.NRO_LOTE, CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103) AS F_VENCIMIENTO ,"
        sSql += " CASE WHEN (MK.CANTIDAD > 0) THEN MK.CANTIDAD ELSE 0 END AS ENTRADA, "
        sSql += " CASE WHEN (MK.CANTIDAD < 0) THEN MK.CANTIDAD ELSE 0 END AS SALIDA,"
        sSql += " U.USUARIO FROM KARDEX K"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " INNER JOIN TIPO_MOVIMIENTO TM ON TM.ID_TIPO_MOVIMIENTO = MK.ID_TIPO_MOVIMIENTO "
        sSql += " INNER JOIN USUARIO U ON U.ID_USUARIO = MK.ID_USUARIO "
        sSql += " WHERE K.ID_MEDICAMENTO = @ID_MEDICAMENTO AND K.ID_KARDEX = CASE WHEN @ID_KARDEX = -1 THEN K.ID_KARDEX ELSE @ID_KARDEX END)"
        sSql += " AS MOVIMIENTO ON MOVIMIENTO.ID_MEDICAMENTO = CABECERA.ID_MEDICAMENTO "
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerInventario() As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()        
        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,M.ID_LABORATORIO ,M.LABORATORIO ,M.MEDICAMENTO,M.FORMA_FARMACEUTICA ,ISNULL(I.CANTIDAD,0) AS CANTIDAD,ISNULL(K.SALDOS,0) AS SALDOS,ISNULL(K.NRO_LOTE,'') AS NRO_LOTE,ISNULL(CONVERT(NVARCHAR(10),K.F_VENCIMIENTO,103),'') AS F_VENCIMIENTO,ISNULL(K.SIN_LOTE,0) AS SIN_LOTE,ISNULL(SUM(MK.CANTIDAD),0) AS CANTIDAD_LOTE ,M.PRECIO_UNITARIO  FROM VW_MEDICAMENTO M "
        sSql += " LEFT JOIN INVENTARIO I ON I.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE NOT M.ID_MEDICAMENTO IN (1907,3671,3688,3691)"
        sSql += " GROUP BY M.ID_MEDICAMENTO,M.ID_LABORATORIO ,M.LABORATORIO ,M.MEDICAMENTO,M.FORMA_FARMACEUTICA ,I.CANTIDAD,K.SALDOS,K.NRO_LOTE,K.F_VENCIMIENTO,K.SIN_LOTE, M.PRECIO_UNITARIO   "
        sSql += " HAVING "
        sSql += " ISNULL(SUM(MK.CANTIDAD),0) > 0"
        sSql += " ORDER BY M.MEDICAMENTO, K.F_VENCIMIENTO"
        Return tbl.ejecutarConsulta(sSql)
    End Function

    Function obtenerInventarioGeneral() As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()
        sSql += " SELECT RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,"
        sSql += " UPPER(M.NOMBRE) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO, "
        sSql += " L.DESCRIPCION AS LABORATORIO, TM.DESCRIPCION AS FORMA_FARMACEUTICA, SUM(MK.CANTIDAD) AS STOCK, "
        sSql += " M.PRECIO_UNITARIO,ISNULL(M.PRECIO_PAQUETE,M.PRECIO_UNITARIO) AS PRECIO_PAQUETE,"
        sSql += " CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,1) = 0 THEN 1 ELSE ISNULL(M.CANTIDAD_PAQUETE,1) END AS CANTIDAD_PAQUETE   FROM MEDICAMENTO M "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO"
        sSql += " INNER JOIN KARDEX K ON K.ID_MEDICAMENTO = M.ID_MEDICAMENTO"
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " GROUP BY M.ID_MEDICAMENTO, M.NOMBRE, M.CONCENTRACION, M.MARCA, L.DESCRIPCION, TM.DESCRIPCION, k.NRO_LOTE, K.F_VENCIMIENTO,"
        sSql += " M.PRECIO_UNITARIO,M.PRECIO_PAQUETE,M.CANTIDAD_PAQUETE"
        sSql += " HAVING SUM(MK.CANTIDAD) > 0"
        sSql += " ORDER BY MEDICAMENTO, LABORATORIO"
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerOrdenCompra(ByVal idOrdenCompra As Integer) As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()
        sSql += " DECLARE @ID_ORDEN_COMPRA AS INTEGER;"
        sSql += " SET @ID_ORDEN_COMPRA = " + idOrdenCompra.ToString + ";"
        sSql += " SELECT RIGHT('000000' + CAST(OC.NRO_ORDEN AS nvarchar(6)),6) AS NRO_ORDEN, OC.FECHA, P.NIT, P.NOMBRE AS PROVEEDOR,OC.OBSERVACION AS GLOSA,EP.ESTADO,"
        sSql += " RIGHT('000000' + CAST(M.ID_MEDICAMENTO AS nvarchar(6)),6) AS ID_MEDICAMENTO,EP.ESTADO,"
        sSql += " UPPER(M.NOMBRE) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " FF.DESCRIPCION AS FORMA_FARMACEUTICA,L.DESCRIPCION AS LABORATORIO, I.CANTIDAD_SOLICITADA     FROM ORDEN_COMPRA OC "
        sSql += " INNER JOIN INSUMOS I ON I.ID_ORDEN_COMPRA = OC.ID_ORDEN_COMPRA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN PROVEEDORES P ON P.ID_PROVEEDOR = OC.ID_PROVEEDOR  "
        sSql += " INNER JOIN ESTADO_PEDIDO EP ON EP.ID_ESTADO = OC.ID_ESTADO "
        sSql += " WHERE OC.ID_ORDEN_COMPRA = @ID_ORDEN_COMPRA"
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerVentasGenerales(ByVal idUsuario As String, ByVal desde As String) As DataTable
        Dim sSql As String = ""
        Dim tbl = New datos()
        sSql += " DECLARE @DESDE AS DATE, @USUARIO AS INTEGER;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @USUARIO = " + idUsuario + ";"

        sSql += " SELECT V.FECHA,P.NOMBRE_APELLIDOS, "
        sSql += " UPPER(M.NOMBRE) + CASE WHEN NOT M.CONCENTRACION IS NULL AND LEN(M.CONCENTRACION) > 0 THEN ' x ' + UPPER(M.CONCENTRACION) ELSE '' END "
        sSql += " + CASE WHEN NOT M.MARCA IS NULL AND LEN(M.MARCA) > 0 THEN + ' - ' + UPPER(M.MARCA) ELSE '' END AS MEDICAMENTO,"
        sSql += " FF.DESCRIPCION AS FORMA_FARMACEUTICA,"
        sSql += " L.DESCRIPCION AS LABORATORIO,CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,1) = 1 OR ISNULL(M.CANTIDAD_PAQUETE,1) = 0 THEN 0 ELSE"
        sSql += " CAST(SUM(CANTIDAD) / ISNULL(M.CANTIDAD_PAQUETE,1)  AS INTEGER) "
        sSql += " END AS CANT_PAQUETE, VM.PRECIO_PAQUETE, CASE WHEN ISNULL(M.CANTIDAD_PAQUETE,1) = 1 OR ISNULL(M.CANTIDAD_PAQUETE,1) = 0 THEN SUM(CANTIDAD)"
        sSql += " ELSE SUM(CANTIDAD) % M.CANTIDAD_PAQUETE END AS CANT_UNIDAD, VM.PRECIO_UNITARIO"
        sSql += " FROM VENTA V"
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO FF ON FF.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = V.ID_PERSONA "
        sSql += " WHERE V.ID_USUARIO = CASE WHEN @USUARIO = -1 THEN V.ID_USUARIO ELSE @USUARIO END"
        sSql += " AND (V.FECHA = @DESDE )"
        sSql += " GROUP BY V.FECHA, P.NOMBRE_APELLIDOS, M.NOMBRE, FF.DESCRIPCION,L.DESCRIPCION,VM.PRECIO_UNITARIO , VM.PRECIO_PAQUETE, M.CANTIDAD_PAQUETE, "
        sSql += " M.MARCA, M.CONCENTRACION"
        Return tbl.ejecutarConsulta(sSql)
    End Function
    Function obtenerCierre(ByVal idCaja As Integer) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @ID_CAJA AS INTEGER;"
        sSql += " SET @ID_CAJA = " + idCaja.ToString + ";"
        sSql += " SELECT P.NOMBRE_APELLIDOS, C.F_APERTURA,C.F_CIERRE,"
        sSql += " CI.BOLIVIANOS, CI.DOLARES, CI.TC, CI.TARJETA, CI.EXCEDENTE,"
        sSql += " PED.TOTAL_VENTA AS TOTAL_PEDIDO, ISNULL(M.MOVIMIENTO,0) AS MOVIMIENTOS,C.IMPORTE_APERTURA"
        sSql += " FROM CAJA C"
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA"
        sSql += " INNER JOIN CIERRE CI ON CI.ID_CAJA = C.ID_CAJA"
        sSql += " INNER JOIN ("
        sSql += " SELECT SUM(TOTAL_PEDIDO - TOTAL_DESCUENTO) AS TOTAL_VENTA,ID_CAJA  "
        sSql += " FROM VENTA V "
        sSql += " WHERE ID_CAJA = @ID_CAJA  AND ID_TIPO_COMPROBANTE <> 3 AND ANULADA = 0"
        sSql += " GROUP BY V.ID_CAJA) PED ON PED.ID_CAJA  = C.ID_CAJA"
        sSql += " LEFT JOIN (SELECT SUM(IMPORTE) AS MOVIMIENTO, ID_CAJA FROM MOVIMIENTO"
        sSql += " WHERE ID_TIPO_MOVIMIENTO <> 3 AND ID_CAJA = @ID_CAJA GROUP BY ID_CAJA) M ON M.ID_CAJA = C.ID_CAJA "
        sSql += " WHERE C.ID_CAJA = @ID_CAJA "
        Return New datos().ejecutarConsulta(sSql)
    End Function

    Function obtenerDatosPago(ByVal idPago As Integer)
        Dim sSql As String = ""
        sSql = " SELECT P.FECHA ,PER.NOMBRE_APELLIDOS AS NOMBRE_APELLIDO,  CL.NIT, C.ID_CREDITO,CP.IMPORTE,V.NRO_VENTA, V.FECHA AS FECHA_VENTA,"
        sSql += " P.IMPORTE_PAGO,P.DESCUENTO,P.TARJETA,P.DOLARES,P.TC,P.EFECTIVO FROM PAGOS P  "
        sSql += " INNER JOIN PAGO_CREDITO CP ON CP.ID_PAGO = P.ID_PAGO "
        sSql += " INNER JOIN CREDITO C ON C.ID_CREDITO = CP.ID_CREDITO "
        sSql += " INNER JOIN PERSONA PER ON PER.ID_PERSONA = C.ID_PERSONA "
        sSql += " INNER JOIN CLIENTE CL ON CL.ID_CLIENTE = C.ID_CLIENTE "
        sSql += " INNER JOIN VENTA V ON V.ID_VENTA = C.ID_VENTA "
        sSql += " WHERE P.ID_PAGO = " + idPago.ToString
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Function obtenerMovimientos(ByVal parametros As String)
        'Dim param = parametros.Split(";")
        'Dim desde = param(0)
        'Dim hasta = param(1)
        'Dim caja = param(2)
        'Dim usuario = param(3)
        Dim sSql As String = ""
        sSql += " DECLARE @F_DESDE AS DATE, @F_HASTA AS DATE, @ID_CAJA AS INTEGER ;"
        sSql += " SET @F_DESDE = CONVERT (DATE, '01/11/2016', 103);"
        sSql += " SET @F_HASTA = CONVERT (DATE, '30/11/2016', 103);"
        sSql += " SET @ID_CAJA = -1;"
        sSql += " SELECT M.NOMBRE AS MEDICAMENTO, M.COMPOSICION AS COMPOSICION, TM.DESCRIPCION AS FORMA_FARMACEUTICA,"
        sSql += "  L.DESCRIPCION AS LABORATORIO, SUM(VM.CANTIDAD) AS CANTIDAD  FROM VENTA V "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA"
        sSql += " INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN LABORATORIO L ON L.ID_LABORATORIO = M.ID_LABORATORIO"
        sSql += " WHERE (V.FECHA >= @F_DESDE AND V.FECHA <= @F_HASTA)"
        sSql += " AND V.ID_CAJA = CASE WHEN @ID_CAJA = -1 THEN V.ID_CAJA ELSE @ID_CAJA END"
        sSql += " GROUP BY M.NOMBRE, M.COMPOSICION, TM.DESCRIPCION, L.DESCRIPCION"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Function obtenerVentaMedicamento(ByVal desde As String, ByVal hasta As String, ByVal laboratorio As Integer)
        Dim sSql As String = ""

        sSql += " DECLARE @DESDE AS DATE, @HASTA AS DATE, @LABORATORIO AS INTEGER;"
        sSql += " SET @DESDE = CONVERT(DATE,'" + desde + "',103);"
        sSql += " SET @HASTA = CONVERT(DATE,'" + hasta + "',103);"
        sSql += " SET @LABORATORIO = " + laboratorio.ToString + ";"
        
        sSql += " SELECT M.LABORATORIO, M.MEDICAMENTO, SUM(VM.CANTIDAD) AS CANTIDAD, VM.PRECIO_UNITARIO FROM VENTA V "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS VM ON VM.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN VW_MEDICAMENTO M ON M.ID_MEDICAMENTO = VM.ID_MEDICAMENTO"
        sSql += " WHERE ANULADA = 0 AND "
        sSql += " CONVERT(DATE,V.FECHA_AUD ,103) >= @DESDE AND CONVERT(DATE,V.FECHA_AUD,103) <= @HASTA"
        sSql += " AND M.ID_LABORATORIO = CASE WHEN @LABORATORIO = -1 THEN M.ID_LABORATORIO ELSE @LABORATORIO END"
        sSql += " GROUP BY M.LABORATORIO, M.MEDICAMENTO, VM.PRECIO_UNITARIO "
        sSql += " ORDER BY M.LABORATORIO, M.MEDICAMENTO "

        Dim tbl = New datos().ejecutarConsulta(sSql)
        Return tbl
    End Function

End Class