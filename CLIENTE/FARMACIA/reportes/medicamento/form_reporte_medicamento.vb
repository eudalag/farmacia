﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms

Public Class form_reporte_medicamento

    Private Sub form_reporte_medicamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim datos = New datos()
        Dim sSql As String = ""
        sSql += " SELECT NOMBRE,DESCRIPCION ,DESC_TIPO_MEDICAMENTO,MINIMO,PRECIO_UNITARIO  FROM VW_MEDICAMENTO"
        sSql += " ORDER BY  DESC_TIPO_MEDICAMENTO, NOMBRE, DESCRIPCION "
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("MEDICAMENTO", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class