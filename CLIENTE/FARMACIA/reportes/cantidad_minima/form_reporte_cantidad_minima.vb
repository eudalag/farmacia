﻿Imports Microsoft.Reporting.WinForms
Imports CAPA_DATOS

Public Class form_reporte_cantidad_minima
    Private Sub form_reporte_cantidad_minima_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sSql As String = ""
        sSql += " SELECT M.NOMBRE,M.DESCRIPCION,M.DESC_TIPO_MEDICAMENTO,M.MINIMO,"
        sSql += " STOCK, VENTA,CASE WHEN (STOCK - ISNULL(VENTA,0)) < MINIMO THEN 1 ELSE 0 END AS ALERTA"
        sSql += " FROM VW_MEDICAMENTO M INNER JOIN ("
        sSql += " SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS STOCK FROM VW_COMPRAS_PRODUCTOS"
        sSql += " GROUP BY ID_MEDICAMENTO "
        sSql += " ) CM ON CM.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " LEFT JOIN (SELECT ID_MEDICAMENTO, SUM(CANTIDAD) AS VENTA FROM VW_VENTAS_PRODUCTOS"
        sSql += " GROUP BY ID_MEDICAMENTO)VM ON VM.ID_MEDICAMENTO = M.ID_MEDICAMENTO "
        sSql += " WHERE STOCK - ISNULL(VENTA,0) < MINIMO"
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("dsMedicamentosMinimos", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class