﻿Imports CAPA_DATOS
Imports Microsoft.Reporting.WinForms

Public Class form_reporte_compra
    Private id_compra As Integer

    Public Sub New(ByVal _idCompra As Integer)
        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        id_compra = _idCompra        
    End Sub
    Private Sub form_reporte_compra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sSql As String = ""
        sSql += " DECLARE @COMPRA AS INTEGER;"
        sSql += " SET @COMPRA = " + id_compra.ToString + ";"
        sSql += " SELECT CM.FECHA_COMPRA,CM.DETALLE ,M.NOMBRE, TM.DESCRIPCION AS FORMA_FARMACEUTICA	, M.LABORATORIO,M.MARCA,M.DESCRIPCION AS COMPOSICION, I.CANTIDAD, "
        sSql += " I.PRECIO_UNITARIO AS PRECIO_COMPRA,M.PRECIO_UNITARIO AS PRECIO_VENTA,I.F_VENCIMIENTO, I.CONTROL_FECHA "
        sSql += " FROM INVENTARIO I  INNER JOIN MEDICAMENTO M ON M.ID_MEDICAMENTO = I.ID_MEDICAMENTO  "
        sSql += " INNER JOIN TIPO_MEDICAMENTO TM ON TM.ID_TIPO_MEDICAMENTO = M.ID_TIPO_MEDICAMENTO "
        sSql += " INNER JOIN COMPRA_MEDICAMENTO CM ON CM.ID_COMPRA = I.ID_COMPRA "
        sSql += " WHERE I.ID_COMPRA = @COMPRA "
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("COMPRAS", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class