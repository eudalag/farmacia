﻿Imports CAPA_DATOS
Public Class inventario_general_laboratorio_reporte
    Protected Sub iniciarCombo()
        Dim sSql As String = ""
        sSql += " SELECT ID_LABORATORIO,LTRIM(DESCRIPCION) AS DESCRIPCION FROM LABORATORIO"
        sSql += " UNION SELECT  -1 , '[Seleccione un Laboratorio]'"
        sSql += " ORDER BY DESCRIPCION "
        cmbLaboratorio.DataSource = New datos().ejecutarConsulta(sSql)
        cmbLaboratorio.DisplayMember = "DESCRIPCION"
        cmbLaboratorio.ValueMember = "ID_LABORATORIO"
        cmbLaboratorio.Refresh()
        If cmbLaboratorio.Items.Count = 0 Then
            cmbLaboratorio.ResetText()
        End If

    End Sub
    

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim parametros As String = cmbLaboratorio.SelectedValue.ToString + ";"
        parametros += txtDesde.Text + ";" + txtHasta.Text
        Dim form = New visualizador_reporte(11, parametros)
        form.Show()
    End Sub

    Private Sub inventario_general_laboratorio_reporte_Load(sender As Object, e As EventArgs) Handles Me.Load
        iniciarCombo()        
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim parametros As String = ""
        parametros += txtDesde.Text + ";" + txtHasta.Text + ";"
        parametros += cmbLaboratorio.SelectedValue.ToString
        Dim form = New visualizador_reporte(14, parametros)
        form.Show()
    End Sub
End Class