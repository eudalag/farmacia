﻿Imports System
Imports System.IO
Imports System.Data
Imports System.Text
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports Microsoft.Reporting.WinForms
Imports CAPA_DATOS
Public Class factura
    Private m_currentPageIndex As Integer
    Private m_streams As IList(Of Stream)
    Private Function CreateStream(ByVal name As String, ByVal fileNameExtension As String, ByVal encoding As Encoding, ByVal mimeType As String, ByVal willSeek As Boolean) As Stream
        Dim stream As Stream = New MemoryStream()
        m_streams.Add(stream)
        Return stream
    End Function
    Private Sub Export(ByVal report As LocalReport)
        Dim deviceInfo As String = "<DeviceInfo>" & _
            "<OutputFormat>EMF</OutputFormat>" & _
            "<PageWidth>5in</PageWidth>" & _
            "<PageHeight>11in</PageHeight>" & _
            "<MarginTop>0.25in</MarginTop>" & _
            "<MarginLeft>0.25in</MarginLeft>" & _
            "<MarginRight>0.25in</MarginRight>" & _
            "<MarginBottom>0.25in</MarginBottom>" & _
            "</DeviceInfo>"
        Dim warnings As Warning()
        m_streams = New List(Of Stream)()
        report.Render("Image", deviceInfo, AddressOf CreateStream, warnings)
        For Each stream As Stream In m_streams
            stream.Position = 0
        Next
    End Sub
    Private Sub Print()
        If m_streams Is Nothing OrElse m_streams.Count = 0 Then
            Throw New Exception("Error: no stream to print.")
        End If
        Dim printDoc As New PrintDocument()
        If Not printDoc.PrinterSettings.IsValid Then
            Throw New Exception("Error: cannot find the default printer.")
        Else
            AddHandler printDoc.PrintPage, AddressOf PrintPage
            m_currentPageIndex = 0
            printDoc.Print()
        End If
    End Sub
    Private Sub PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim pageImage As New Metafile(m_streams(m_currentPageIndex))

        ' Adjust rectangular area with printer margins.
        Dim adjustedRect As New Rectangle(ev.PageBounds.Left - CInt(ev.PageSettings.HardMarginX), _
                                          ev.PageBounds.Top - CInt(ev.PageSettings.HardMarginY), _
                                          ev.PageBounds.Width, _
                                          ev.PageBounds.Height)

        ' Draw a white background for the report
        ev.Graphics.FillRectangle(Brushes.White, adjustedRect)

        ' Draw the report content
        ev.Graphics.DrawImage(pageImage, adjustedRect)

        ' Prepare for the next page. Make sure we haven't hit the end.
        m_currentPageIndex += 1
        ev.HasMorePages = (m_currentPageIndex < m_streams.Count)
    End Sub
    Public Sub imprimirComprobante(ByVal idVenta As Integer)
        'Dim report As New LocalReport()
        'report.ReportPath = "rptFactura.rdlc"

        'Dim par1 = New ReportParameter("empresa", form_sesion.cConfiguracion.NOMBRE)
        'Dim par2 = New ReportParameter("direccion", form_sesion.cConfiguracion.DIRECCION)
        'Dim par3 = New ReportParameter("telefono", form_sesion.cConfiguracion.TELEFONO)
        'Dim par4 = New ReportParameter("nit", CStr(form_sesion.cConfiguracion.NIT))
        'report.SetParameters(New ReportParameter() {par1, par2, par3, par4})
        'Dim sSql As String = ""
        'sSql += " SELECT TC.DETALLE,V.NRO_VENTA,D.NUMERO_AUTORIZACION,"
        'sSql += " V.FECHA,C.NIT,P.NOMBRE_APELLIDOS,R.CANTIDAD,R.PRECIO_UNITARIO,M.NOMBRE AS DESC_MEDICAMENTO ,M.DESC_TIPO_MEDICAMENTO,"
        'sSql += " V.CODIGO_CONTROL,V.CODIGO_QR    FROM VENTA V"
        'sSql += " LEFT JOIN DOSIFICACION D ON D.ID_DOSIFICACION = V.ID_DOSIFICACION "
        'sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        'sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        'sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        'sSql += " INNER JOIN VW_MEDICAMENTO  M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        'sSql += " WHERE V.ID_VENTA = " & idVenta
        'Dim datos = New datos()
        'Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        'report.DataSources.Add(New ReportDataSource("dsFactura", dt))
        'Export(report)
        Print()
    End Sub
    Public Function mostrarComprobante(ByVal idVenta As String) As DataTable
        Dim sSql As String = ""
        sSql += " DECLARE @ID_VENTA AS INTEGER;"
        sSql += " SET @ID_VENTA = " + idVenta + ";"
        sSql += " SELECT TC.DETALLE,CASE WHEN F.NRO_FACTURA IS NULL THEN V.NRO_VENTA ELSE F.NRO_FACTURA END AS NRO_VENTA,D.NUMERO_AUTORIZACION,"
        sSql += " CASE WHEN F.NRO_FACTURA IS NULL THEN V.FECHA ELSE F.FECHA END AS FECHA,"
        sSql += " CASE WHEN V.ID_TIPO_COMPROBANTE = 2 THEN CF.NIT ELSE C.NIT END AS NIT,"
        sSql += " CASE WHEN V.ID_TIPO_COMPROBANTE = 2 THEN PF.NOMBRE_APELLIDOS ELSE"
        sSql += " P.NOMBRE_APELLIDOS END AS NOMBRE_APELLIDOS,"
        sSql += " R.CANTIDAD,M.ID_MEDICAMENTO,"
        sSql += " R.PRECIO_UNITARIO ,M.MEDICAMENTO AS DESC_MEDICAMENTO ,M.FORMA_FARMACEUTICA  AS DESC_TIPO_MEDICAMENTO,"
        sSql += " ISNULL(F.CODIGO_CONTROL,'') AS CODIGO_CONTROL,d.F_LIMITE_EMISION,F.CODIGO_QR, ISNULL(U.USUARIO,'') AS USUARIO, V.PAGADO, V.EFECTIVO, V.TARJETA, V.DOLARES, V.TC,V.TOTAL_PEDIDO,V.TOTAL_DESCUENTO,"
        sSql += " V.CREDITO,R.CANTIDAD   FROM VENTA V        "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " INNER JOIN VENTA_MEDICAMENTOS R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN VW_MEDICAMENTO  M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        sSql += " LEFT JOIN CAJA CJ ON CJ.ID_CAJA = V.ID_CAJA "
        sSql += " LEFT JOIN USUARIO U ON U.ID_USUARIO = CJ.ID_USUARIO "
        sSql += " LEFT JOIN FACTURA F ON F.ID_VENTA = V.ID_VENTA "
        sSql += " LEFT JOIN CLIENTE CF ON CF.ID_CLIENTE = F.ID_CLIENTE "
        sSql += " LEFT JOIN PERSONA PF ON PF.ID_PERSONA = CF.ID_PERSONA "
        sSql += " LEFT JOIN DOSIFICACION D ON D.ID_DOSIFICACION = F.ID_DOSIFICACION "
        sSql += " WHERE V.ID_VENTA =  @ID_VENTA "
        'sSql += " AND CAST((R.CANTIDAD / M.CANTIDAD_PAQUETE) AS INTEGER) > 0"
        'sSql += " AND NOT M.CANTIDAD_PAQUETE IS NULL AND M.CANTIDAD_PAQUETE > 0"
        'sSql += " UNION ALL"
        'sSql += " SELECT TC.DETALLE,V.NRO_VENTA,D.NUMERO_AUTORIZACION,"
        'sSql += " V.FECHA,C.NIT,P.NOMBRE_APELLIDOS,"
        'sSql += " CASE WHEN M.CANTIDAD_PAQUETE IS NULL THEN R.CANTIDAD WHEN ISNULL(M.CANTIDAD_PAQUETE,0) = 0 THEN R.CANTIDAD ELSE CAST((R.CANTIDAD % M.CANTIDAD_PAQUETE) AS INTEGER) END AS CANTIDAD, "
        'sSql += " M.ID_MEDICAMENTO,R.PRECIO_UNITARIO,M.MEDICAMENTO AS DESC_MEDICAMENTO ,M.FORMA_FARMACEUTICA  AS DESC_TIPO_MEDICAMENTO,"
        'sSql += " V.CODIGO_CONTROL,d.F_LIMITE_EMISION,V.CODIGO_QR, U.USUARIO, V.PAGADO, V.EFECTIVO, V.TARJETA, V.DOLARES, V.TC,V.TOTAL_PEDIDO,V.TOTAL_DESCUENTO,"
        'sSql += " V.CREDITO FROM VENTA V"
        'sSql += " LEFT JOIN DOSIFICACION D ON D.ID_DOSIFICACION = V.ID_DOSIFICACION "
        'sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        'sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        'sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_TIPO_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        'sSql += " INNER JOIN VENTA_MEDICAMENTOS R ON R.ID_VENTA = V.ID_VENTA "
        'sSql += " INNER JOIN VW_MEDICAMENTO  M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        'sSql += " LEFT JOIN CAJA CJ ON CJ.ID_CAJA = V.ID_CAJA "
        'sSql += " LEFT JOIN USUARIO U ON U.ID_USUARIO = CJ.ID_USUARIO "
        'sSql += " WHERE V.ID_VENTA =  @ID_VENTA "
        'sSql += " AND CASE WHEN M.CANTIDAD_PAQUETE IS NULL THEN R.CANTIDAD WHEN ISNULL(M.CANTIDAD_PAQUETE,0) = 0 THEN R.CANTIDAD ELSE CAST((R.CANTIDAD % M.CANTIDAD_PAQUETE) AS INTEGER) END > 0"
        Dim datos = New datos()
        Return New datos().ejecutarConsulta(sSql)
    End Function
End Class
