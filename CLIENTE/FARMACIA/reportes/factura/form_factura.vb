﻿Imports Microsoft.Reporting.WinForms
Imports CAPA_DATOS
Public Class form_factura

    Private Sub form_factura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sSql As String = ""
        sSql += " SELECT TC.DETALLE,V.NRO_VENTA,D.NUMERO_AUTORIZACION,"
        sSql += " V.FECHA,C.NIT,P.NOMBRE_APELLIDOS,R.CANTIDAD,R.PRECIO_UNITARIO,M.NOMBRE AS DESC_MEDICAMENTO ,M.DESC_TIPO_MEDICAMENTO,"
        sSql += " V.CODIGO_CONTROL,V.CODIGO_QR    FROM VENTA V"
        sSql += " INNER JOIN DOSIFICACION D ON D.ID_DOSIFICACION = V.ID_DOSIFICACION "
        sSql += " INNER JOIN CLIENTE C ON C.ID_CLIENTE = V.ID_CLIENTE "
        sSql += " INNER JOIN PERSONA P ON P.ID_PERSONA = C.ID_PERSONA "
        sSql += " INNER JOIN TIPO_COMPROBANTE TC ON TC.ID_COMPROBANTE = V.ID_TIPO_COMPROBANTE "
        sSql += " INNER JOIN RECETA R ON R.ID_VENTA = V.ID_VENTA "
        sSql += " INNER JOIN VW_MEDICAMENTO  M ON M.ID_MEDICAMENTO = R.ID_MEDICAMENTO "
        sSql += " WHERE V.ID_VENTA = 2"
        Dim dt As DataTable = New datos().ejecutarConsulta(sSql)
        Dim rds As ReportDataSource = New ReportDataSource("dsFactura", dt)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class