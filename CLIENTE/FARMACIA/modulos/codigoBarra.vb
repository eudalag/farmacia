﻿Imports System.Drawing.Text
Imports System.IO
Imports System.Drawing.Imaging

Public Class codigoBarra
    Private font As Font
    Private barcodeBitmap As Bitmap
    Function Generar_EAN13(Codigo As String) As String
        ' Esta función permite generar el código de barras para mostrarlo con la fuente EAN13.TTF
        ' - Parametros : código de 12 o 13 dígitos
        ' - Retorno: retorna una cadena que permite representar generar el código de barras con la fuente EAN13.TTF
        ' retorna una cadena vacía si no se puede representar el código de barras

        Dim i, first, checksum As Integer
        Dim code, code13 As String
        Dim tableA As Boolean

        ' Evaluar los dígitos del código
        If Len(Codigo) = 12 Then
            code = Codigo
        ElseIf Len(Codigo) = 13 Then
            code = Left(Codigo, 12)
        Else
            code = ""
        End If

        ' VerIficar los dígitos del código
        For i = 1 To LEN(code)
            If Asc(Mid(code, i, 1)) < 48 Or Asc(Mid(code, i, 1)) > 57 Then
                code = ""
                Exit For
            End If
        Next

        ' Chequea los 12 dígitos y cálcula el digito de control
        If Len(code) = 12 Then
            For i = 12 To 1 Step -2
                checksum = checksum + Val(Mid(code, i, 1))
            Next
            checksum = checksum * 3
            For i = 11 To 1 Step -2
                checksum = checksum + Val(Mid(code, i, 1))
            Next
            code = code & ((10 - checksum Mod 10) Mod 10).ToString

            ' Si el código inicial tenía 13 dígitos comprueba si el nuevo código generado
            ' es igual y en caso contrario no se generar ningún código
            If Len(Codigo) = 13 And Codigo <> code Then
                code = ""
            End If
        End If

        ' Chequea los 13 dígitos
        If Len(code) = 13 Then
            ' Los primeros 2 dígitos que suelen corresponder al código del país
            code13 = Left(code, 1) & Chr(65 + Val(Mid(code, 2, 1)))
            first = Val(Left(code, 1))

            ' Generar los códigos del primer bloque de dígitos
            For i = 3 To 7
                tableA = False
                Select Case i
                    Case 3
                        Select Case first
                            Case 0 To 3
                                tableA = True
                        End Select
                    Case 4
                        Select Case first
                            Case 0, 4, 7, 8
                                tableA = True
                        End Select
                    Case 5
                        Select Case first
                            Case 0, 1, 4, 5, 9
                                tableA = True
                        End Select
                    Case 6
                        Select Case first
                            Case 0, 2, 5, 6, 7
                                tableA = True
                        End Select
                    Case 7
                        Select Case first
                            Case 0, 3, 6, 8, 9
                                tableA = True
                        End Select
                End Select
                If tableA Then
                    code13 = code13 & Chr(65 + Val(Mid(code, i, 1)))
                Else
                    code13 = code13 & Chr(75 + Val(Mid(code, i, 1)))
                End If
            Next

            ' Añadir el separador de los bloques
            code13 = code13 & "*"

            ' Generar los códigos del segundo bloque de dígitos
            For i = 8 To 13
                code13 = code13 & Chr(97 + Val(Mid(code, i, 1)))
            Next

            ' Añadir la marca final
            code13 = code13 & "+"
        End If
        Return code13
    End Function

    Public Sub cargarFuente()
        'Dim a = Context.Server.MapPath(dir)
        'Dim pfc = New PrivateFontCollection()
        'pfc.AddFontFile(a)
        Dim family = New FontFamily("Code EAN13")
        font = New Font(family, 20)
    End Sub
    Public Sub fijarTamaño(ByVal emSize As Single)
        font = New Font(font.FontFamily, emSize)
    End Sub
    Public Sub Generar(ByVal code As String)
        'If (code.IndexOf("*") <> 0) Then
        'code = "*" + code
        'End If
        'If (code.LastIndexOf("*") <> code.Length - 1) Then
        'code += "*"
        'End If
        code = Generar_EAN13(code)
        barcodeBitmap = New Bitmap(1, 1)
        Dim objGraphics = Graphics.FromImage(barcodeBitmap)
        Dim barCodeSize = objGraphics.MeasureString(code, font)
        barcodeBitmap = New Bitmap(CInt(barCodeSize.Width), CInt(barCodeSize.Height))
        objGraphics = Graphics.FromImage(barcodeBitmap)
        objGraphics.FillRectangle(New SolidBrush(Color.White), 0, 0, barcodeBitmap.Width, barcodeBitmap.Height)
        objGraphics.DrawString(code, font, New SolidBrush(Color.Black), 0, 0)
    End Sub
    Public Function Guardar() As MemoryStream
        Dim ms = New MemoryStream()
        barcodeBitmap.Save(ms, ImageFormat.Jpeg)
        Return ms
    End Function
    Public Sub GuardarEn(ByVal Dir As String)
        barcodeBitmap.Save(Dir, ImageFormat.Gif)
    End Sub
    Public Function cargarImagen(ByVal codigo) As Byte()
        cargarFuente()
        fijarTamaño(100)
        Generar(codigo)
        Dim Stream = Guardar()
        'Dim streamLength As Integer = Convert.ToInt32(Stream.Length)
        Dim fileData = Stream.GetBuffer()
        barcodeBitmap.Dispose()
        Stream.Close()
        'Stream.Read(fileData, 0, streamLength)
        'Stream.Close()
        Return fileData
    End Function
End Class
