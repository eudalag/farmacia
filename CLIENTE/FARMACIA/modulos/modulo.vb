﻿Imports System.Text
Imports System.Security.Cryptography
Imports CAPA_DATOS
Public Class modulo

    Public Function GenerarHash(ByVal Texto As String) As String
        'Creamos un objeto de codificación Unicode que
        'representa una codificación UTF-16 de caracteres Unicode. 
        Dim Codificar As New UnicodeEncoding()
        'Declaramos una matriz (array) de tipo Byte para recuperar dentro los bytes del texto
        'utilizando el objeto Codificar
        Dim ByteTexto() As Byte = Codificar.GetBytes(Texto)
        'Instanciamos el objeto MD5 
        Dim Md5 As New MD5CryptoServiceProvider()
        'Se calcula el Hash del Texto en bytes 
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteTexto)
        'convertimos el texto en bytes en texto legible(cadena) 
        Return Convert.ToBase64String(ByteHash)
        'Eliminamos los objetos usados con Nothing
        Codificar = Nothing
        ByteTexto = Nothing
    End Function
    Public Sub cambiarForm(ByRef panelContenedor As Panel, ByRef formulario As Form)
        If panelContenedor.Controls.Count > 0 Then
            Try
                Dim formulario_contenedor As Form = CType(panelContenedor.Controls(0), Form)
                formulario_contenedor.Close()
            Catch ex As Exception

            End Try

            'panelContenedor.Controls.RemoveAt(0)
        End If
        Try
            formulario.TopLevel = False
            formulario.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            formulario.Dock = DockStyle.Fill
            panelContenedor.Controls.Add(formulario)
            panelContenedor.Tag = formulario
            formulario.Show()
        Catch ex As Exception

        End Try

    End Sub
    Public Sub mensajeError(ByVal title As String, ByVal msg As String)
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        style = MsgBoxStyle.OkOnly Or _
           MsgBoxStyle.Critical
        response = MsgBox(msg, style, title)
    End Sub
    Public Sub mensaje(ByVal title As String, ByVal msg As String)
        Dim style As MsgBoxStyle
        Dim response As MsgBoxResult
        style = MsgBoxStyle.OkOnly Or _
           MsgBoxStyle.Information
        response = MsgBox(msg, style, title)
    End Sub
    Public Function mensajeConfirmacion(ByVal title As String, ByVal msg As String) As Microsoft.VisualBasic.MsgBoxResult
        Dim style As MsgBoxStyle
        'Dim response As MsgBoxResult
        style = MsgBoxStyle.YesNo Or _
           MsgBoxStyle.Exclamation
        Return MsgBox(msg, style, title)
    End Function
    Public Sub volver(ByRef formulario As Form)
        cambiarForm(form_principal_adminitrador.panelContenedor, formulario)
    End Sub
    Public Sub volver_venta(ByRef formulario As Form)
        cambiarForm(form_principal_venta.panelContenedor, formulario)
    End Sub
    Public Function esNulo(ByVal obj As Object) As Object
        If IsDBNull(obj) Then
            Return ""
        Else
            Return obj
        End If
    End Function
    Public Function EnLetras(ByVal numero As String) As String
        Dim b, paso As Integer
        Dim expresion, entero, deci, flag As String

        flag = "N"
        For paso = 1 To Len(numero)
            If Mid(numero, paso, 1) = "." Then
                flag = "S"
            Else
                If flag = "N" Then
                    entero = entero + Mid(numero, paso, 1) 'Extae la parte entera del numero
                Else
                    deci = deci + Mid(numero, paso, 1) 'Extrae la parte decimal del numero
                End If
            End If
        Next paso

        If Len(deci) = 1 Then
            deci = deci & "0"
        End If

        flag = "N"
        If Val(numero) >= -999999999 And Val(numero) <= 999999999 Then 'si el numero esta dentro de 0 a 999.999.999
            For paso = Len(entero) To 1 Step -1
                b = Len(entero) - (paso - 1)
                Select Case paso
                    Case 3, 6, 9
                        Select Case Mid(entero, b, 1)
                            Case "1"
                                If Mid(entero, b + 1, 1) = "0" And Mid(entero, b + 2, 1) = "0" Then
                                    expresion = expresion & "cien "
                                Else
                                    expresion = expresion & "ciento "
                                End If
                            Case "2"
                                expresion = expresion & "doscientos "
                            Case "3"
                                expresion = expresion & "trescientos "
                            Case "4"
                                expresion = expresion & "cuatrocientos "
                            Case "5"
                                expresion = expresion & "quinientos "
                            Case "6"
                                expresion = expresion & "seiscientos "
                            Case "7"
                                expresion = expresion & "setecientos "
                            Case "8"
                                expresion = expresion & "ochocientos "
                            Case "9"
                                expresion = expresion & "novecientos "
                        End Select

                    Case 2, 5, 8
                        Select Case Mid(entero, b, 1)
                            Case "1"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    flag = "S"
                                    expresion = expresion & "diez "
                                End If
                                If Mid(entero, b + 1, 1) = "1" Then
                                    flag = "S"
                                    expresion = expresion & "once "
                                End If
                                If Mid(entero, b + 1, 1) = "2" Then
                                    flag = "S"
                                    expresion = expresion & "doce "
                                End If
                                If Mid(entero, b + 1, 1) = "3" Then
                                    flag = "S"
                                    expresion = expresion & "trece "
                                End If
                                If Mid(entero, b + 1, 1) = "4" Then
                                    flag = "S"
                                    expresion = expresion & "catorce "
                                End If
                                If Mid(entero, b + 1, 1) = "5" Then
                                    flag = "S"
                                    expresion = expresion & "quince "
                                End If
                                If Mid(entero, b + 1, 1) > "5" Then
                                    flag = "N"
                                    expresion = expresion & "dieci"
                                End If

                            Case "2"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "veinte "
                                    flag = "S"
                                Else
                                    expresion = expresion & "veinti"
                                    flag = "N"
                                End If

                            Case "3"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "treinta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "treinta y "
                                    flag = "N"
                                End If

                            Case "4"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "cuarenta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "cuarenta y "
                                    flag = "N"
                                End If

                            Case "5"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "cincuenta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "cincuenta y "
                                    flag = "N"
                                End If

                            Case "6"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "sesenta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "sesenta y "
                                    flag = "N"
                                End If

                            Case "7"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "setenta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "setenta y "
                                    flag = "N"
                                End If

                            Case "8"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "ochenta "
                                    flag = "S"
                                Else
                                    expresion = expresion & "ochenta y "
                                    flag = "N"
                                End If

                            Case "9"
                                If Mid(entero, b + 1, 1) = "0" Then
                                    expresion = expresion & "noventa "
                                    flag = "S"
                                Else
                                    expresion = expresion & "noventa y "
                                    flag = "N"
                                End If
                        End Select

                    Case 1, 4, 7
                        Select Case Mid(entero, b, 1)
                            Case "0"
                                If flag = "N" Then
                                    expresion = expresion & "cero "
                                End If
                            Case "1"
                                If flag = "N" Then
                                    If paso = 1 Then
                                        expresion = expresion & "uno "
                                    Else
                                        expresion = expresion & "un "
                                    End If
                                End If
                            Case "2"
                                If flag = "N" Then
                                    expresion = expresion & "dos "
                                End If
                            Case "3"
                                If flag = "N" Then
                                    expresion = expresion & "tres "
                                End If
                            Case "4"
                                If flag = "N" Then
                                    expresion = expresion & "cuatro "
                                End If
                            Case "5"
                                If flag = "N" Then
                                    expresion = expresion & "cinco "
                                End If
                            Case "6"
                                If flag = "N" Then
                                    expresion = expresion & "seis "
                                End If
                            Case "7"
                                If flag = "N" Then
                                    expresion = expresion & "siete "
                                End If
                            Case "8"
                                If flag = "N" Then
                                    expresion = expresion & "ocho "
                                End If
                            Case "9"
                                If flag = "N" Then
                                    expresion = expresion & "nueve "
                                End If
                        End Select
                End Select
                If paso = 4 Then
                    If Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or _
                      (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
                       Len(entero) <= 6) Then
                        expresion = expresion & "mil "
                    End If
                End If
                If paso = 7 Then
                    If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
                        expresion = expresion & "millón "
                    Else
                        expresion = expresion & "millones "
                    End If
                End If
            Next paso

            If deci <> "" Then
                If Mid(entero, 1, 1) = "-" Then 'si el numero es negativo
                    EnLetras = "menos " & expresion & "con " & deci & "/100  Bs."
                Else
                    EnLetras = expresion & "con " & deci & "/100  Bs."
                End If
            Else
                If Mid(entero, 1, 1) = "-" Then 'si el numero es negativo
                    EnLetras = "menos " & expresion
                Else
                    EnLetras = expresion & "con 00/100 Bs."
                End If
            End If
        Else 'si el numero a convertir esta fuera del rango superior e inferior
            EnLetras = ""
        End If

    End Function

    Function obtenerMedicamento(ByVal dato As String, tipo As Integer) As DataTable 'TIPO 1 = REGISTRO ; TIPO 2 = CODIGO_BARRA
        Dim sSql As String = ""
        If Microsoft.VisualBasic.Left(dato, 7) = form_sesion.cConfiguracion.CODIGO_BARRA Then
            dato = Microsoft.VisualBasic.Left(dato, 12)
        End If
        sSql += " DECLARE @BUSCAR AS NVARCHAR(25);"
        sSql += " SET @BUSCAR = '" + dato + "';"

        sSql += " SELECT NOMBRE,ID_LABORATORIO,ID_TIPO_MEDICAMENTO FROM MEDICAMENTO M "
        sSql += " INNER JOIN CODIGO_BARRA CB ON CB.ID_MEDICAMENTO  = M.ID_MEDICAMENTO "
        sSql += " WHERE CB.CODIGO_BARRA = @BUSCAR "
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Public Sub registrarVentaPerdida(ByVal medicamento As Integer, ByVal tipo As Integer) ' 1: Sistema; 2: Vademecum
        If Not existeRegistroVentaPerdida(medicamento, tipo) Then
            Dim tbl = New datos()
            tbl.tabla = "VENTA_PERDIDA"
            tbl.campoID = "ID_VENTA_PERDIDA"
            tbl.modoID = "sql"
            tbl.agregarCampoValor("FECHA", "serverDateTime")
            If tipo = 1 Then
                tbl.agregarCampoValor("ID_MEDICAMENTO", medicamento)

            Else
                tbl.agregarCampoValor("ID_VADEMECUM", medicamento)
            End If
            tbl.insertar()
            mensaje("Venta Perdida", "Medicamento registrado como venta perdida.")
        Else
            mensajeError("Venta Perdida", "El medicamento seleccionado ya se encuentra registrado")
        End If

    End Sub
    Private Function existeRegistroVentaPerdida(ByVal idMedicamento As Integer, ByVal tipo As Integer) As Boolean
        Dim sSql = ""
        If tipo = 1 Then
            sSql = "SELECT * FROM VENTA_PERDIDA WHERE ID_MEDICAMENTO = " + idMedicamento.ToString + " AND ID_ORDEN_COMPRA IS NULL"
        Else
            sSql = "SELECT * FROM VENTA_PERDIDA WHERE ID_VADEMECUM = " + idMedicamento.ToString + " AND ID_ORDEN_COMPRA IS NULL"
        End If
        Return New datos().ejecutarConsulta(sSql).Rows.Count > 0
    End Function

    Public Function SelectDistinct(ByVal pColumnNames As String(), ByVal pOriginalTable As DataTable) As DataTable
        Dim distinctTable As DataTable = New DataTable()
        Dim numColumns As Integer = pColumnNames.Length
        For i As Integer = 0 To numColumns - 1
            distinctTable.Columns.Add(pColumnNames(i), pOriginalTable.Columns(pColumnNames(i)).DataType)
        Next
        Dim trackData As Hashtable = New Hashtable()
        For Each currentOriginalRow As DataRow In pOriginalTable.Rows
            Dim hashData As StringBuilder = New StringBuilder()
            Dim newRow As DataRow = distinctTable.NewRow()
            For i As Integer = 0 To numColumns - 1
                hashData.Append(currentOriginalRow(pColumnNames(i)).ToString())
                newRow(pColumnNames(i)) = currentOriginalRow(pColumnNames(i))
            Next

            If Not trackData.ContainsKey(hashData.ToString()) Then
                trackData.Add(hashData.ToString(), "NULL")
                distinctTable.Rows.Add(newRow)
            End If
        Next
        Return distinctTable
    End Function
End Class
