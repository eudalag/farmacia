﻿Imports CAPA_NEGOCIO
Imports CAPA_DATOS
Imports System.Data
Public Class imprimir    

    Private CurrentY As Integer
    Private CurrentX As Integer
    Private leftMargin As Integer
    Private rightMargin As Integer
    Private topMargin As Integer
    Private bottomMargin As Integer
    Private InvoiceWidth As Integer
    Private InvoiceHeight As Integer
    Private CustomerName As String
    Private CustomerCity As String
    Private SellerName As String
    Private SaleID As String
    Private SaleDate As String
    Private SaleFreight As Decimal
    Private SubTotal As Decimal
    Private InvoiceTotal As Decimal
    Private ReadInvoice As Boolean
    Private AmountPosition As Integer
    Private tVenta As DataTable

    ' Font and Color:------------------
    ' Title Font
    Private FontCabecera As Font = New Font("Arial", 16, FontStyle.Bold)
    Private InvTitleFont As Font = New Font("Arial", 11, FontStyle.Bold)
    ' Title Font height
    Private InvTitleHeight As Integer
    ' SubTitle Font
    Private InvSubTitleFont As Font = New Font("Arial", 8, FontStyle.Regular)
    Private InvSubTitleFontBold As Font = New Font("Arial", 8, FontStyle.Bold)
    Private fuenteMensaje As Font = New Font("Arial", 8, FontStyle.Bold)
    Private InvSubTitleHeight As Integer
    ' Invoice Font
    Private InvoiceFont As Font = New Font("Arial", 10, FontStyle.Regular)
    Private InvoiceFontBold As Font = New Font("Arial", 10, FontStyle.Bold)
    ' Invoice Font height
    Private InvoiceFontHeight As Integer
    ' Blue Color
    Private BlueBrush As SolidBrush = New SolidBrush(Color.Blue)
    ' Red Color
    Private RedBrush As SolidBrush = New SolidBrush(Color.Red)
    ' Black Color
    Private BlackBrush As SolidBrush = New SolidBrush(Color.Black)

    ' for Invoice Head:
    Private InvTitle As String
    Private InvSubTitle1 As String
    Private InvSubTitle2 As String
    Private InvSubTitle3 As String
    Private idPedido As Integer
    Private tipoVenta As Integer

    Private conf As eConfiguracion
    Public Sub New()

    End Sub
    Public Sub New(ByVal _idPedido As Integer, Optional ByVal _tipoVenta As Integer = 1)
        leftMargin = 10
        topMargin = 10
        InvoiceWidth = 200
        tVenta = New factura().mostrarComprobante(_idPedido)
        idPedido = _idPedido
        conf = New eConfiguracion()
        tipoVenta = _tipoVenta
    End Sub
    Private Sub datosCabecera()
       'Titles and Image of invoice:        
        InvTitle = conf.NOMBRE
        InvSubTitle1 = "Farmacia"
        InvSubTitle2 = conf.DIRECCION
        InvSubTitle3 = conf.TELEFONO
    End Sub
    Public Sub SetInvoiceHead(ByVal g As Graphics)
        datosCabecera()
        CurrentY = topMargin
        CurrentX = leftMargin
                InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "SFC: 0"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        g.DrawString(tVenta.Rows(0)("DETALLE").ToString.ToUpper, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, tVenta.Rows(0)("DETALLE").ToString.ToUpper), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
    End Sub
    Public Sub SetOrderData(ByVal g As Graphics)
        ' Set Company Name, City, Salesperson, Order ID and Order Date
        Dim FieldValue As String = ""
        Dim espaciado As Integer = 130
        ' Set Order ID:
        CurrentX = 10
        CurrentY = CurrentY + 4
        FieldValue = "Nit:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
        FieldValue = conf.NIT
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
        CurrentX = 10
        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "Nro. " & tVenta.Rows(0)("DETALLE") & ": "
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
        FieldValue = tVenta.Rows(0)("NRO_VENTA")
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
        If tVenta.Rows(0)("DETALLE").ToString.ToUpper <> "RECIBO" Then
            CurrentX = 10
            CurrentY = CurrentY + InvSubTitleHeight
            FieldValue = "Nro. Autorización:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
            FieldValue = tVenta.Rows(0)("NUMERO_AUTORIZACION")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
        End If

        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        espaciado = 67
        CurrentX = 10
        CurrentY = CurrentY + 4
        FieldValue = "Fecha:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
        FieldValue = tVenta.Rows(0)("FECHA")
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "NIT/CI:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
        FieldValue = tVenta.Rows(0)("NIT")
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "Nombre:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, CurrentX, CurrentY)
        Dim longNombre As Integer = Len(tVenta.Rows(0)("NOMBRE_APELLIDOS"))
        Dim inicio = 1
        While longNombre > 0
            FieldValue = Mid(tVenta.Rows(0)("NOMBRE_APELLIDOS"), inicio, 37)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, espaciado, CurrentY)
            CurrentY = CurrentY + InvSubTitleHeight
            inicio = inicio + 37
            longNombre = longNombre - 37
        End While
        'CurrentY = CurrentY + InvSubTitleHeight
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2

        Dim Mensaje = "Venta al por menor de productos farmaceuticos,"
        g.DrawString(Mensaje, fuenteMensaje, BlackBrush, alinearCentro(g, fuenteMensaje, Mensaje), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        Mensaje = "medicinales, cosméticos y artículos de tocador"
        g.DrawString(Mensaje, fuenteMensaje, BlackBrush, alinearCentro(g, fuenteMensaje, Mensaje), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Cantidad"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = "P. Unitario"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 100, CurrentY)

        FieldValue = "SubTotal"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 210, CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "Detalle"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, alinearCentro(g, InvSubTitleFontBold, FieldValue), CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
    End Sub
    Protected Function obtenerkardex(ByVal idMedicamento As Integer, ByVal idVenta As Integer)
        Dim sSql As String = ""
        sSql += " DECLARE @MEDICAMENTO AS INTEGER, @VENTA AS INTEGER;"
        sSql += " SET @MEDICAMENTO = " + idMedicamento.ToString + ";"
        sSql += " SET @VENTA = " + idVenta.ToString + ";"
        sSql += " SELECT NRO_LOTE, CONVERT(NVARCHAR(10),F_VENCIMIENTO,103) AS F_VENCIMIENTO, CANTIDAD * -1 AS CANTIDAD FROM KARDEX K "
        sSql += " INNER JOIN MOVIMIENTO_KARDEX MK ON MK.ID_KARDEX = K.ID_KARDEX "
        sSql += " WHERE ID_MEDICAMENTO = @MEDICAMENTO  AND MK.ID_VENTA_MEDICAMENTOS = @VENTA"
        Return New datos().ejecutarConsulta(sSql)
    End Function
    Public Sub SetInvoiceData(ByVal g As Graphics, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        ' Set Invoice Table:
        Dim FieldValue As String = ""
        CurrentY = CurrentY + 2
        Dim total As Decimal = 0
        Dim subTotal As Decimal
        For Each row In tVenta.Rows
            FieldValue = row("CANTIDAD")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 20, CurrentY)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + row("PRECIO_UNITARIO").ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 100, CurrentY)
            subTotal = (CDec(row("PRECIO_UNITARIO")) * CDec(row("CANTIDAD")))
            total = total + subTotal
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight
            If Len(row("DESC_MEDICAMENTO")) > 35 Then
                FieldValue = Microsoft.VisualBasic.Left(row("DESC_MEDICAMENTO"), 35) + "..."
            Else
                FieldValue = row("DESC_MEDICAMENTO")
            End If

            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight

            If tipoVenta = 2 Then
                Dim tbl As DataTable = obtenerkardex(row("ID_MEDICAMENTO"), idPedido)
                For Each item In tbl.Rows
                    FieldValue = item("NRO_LOTE").ToString + " - " + item("F_VENCIMIENTO").ToString + " (" + item("CANTIDAD").ToString + ")"
                    g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
                    CurrentY = CurrentY + InvSubTitleHeight
                Next
                CurrentY = CurrentY + InvSubTitleHeight
            End If
        Next
        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        SetInvoiceTotal(g)
        g.Dispose()
    End Sub
    Public Sub SetInvoiceTotal(ByVal g As Graphics)
        CurrentY = CurrentY + 2
        Dim FieldValue As String = ""
        FieldValue = "Sub Total Bs.:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tVenta.Rows(0)("TOTAL_PEDIDO")), 27)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)

        If (CDec(tVenta.Rows(0)("TOTAL_DESCUENTO") > 0)) Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Descuento Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tVenta.Rows(0)("TOTAL_DESCUENTO")), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Total Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", CDec(tVenta.Rows(0)("TOTAL_PEDIDO")) - (tVenta.Rows(0)("TOTAL_DESCUENTO"))), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If
        If (CDec(tVenta.Rows(0)("EFECTIVO") > 0)) Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Efectivo Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tVenta.Rows(0)("EFECTIVO")), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If
        If (CDec(tVenta.Rows(0)("DOLARES") > 0)) Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Dolares (t/c: " + tVenta.Rows(0)("TC").ToString + ") Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", Math.Round(CDec(tVenta.Rows(0)("DOLARES")) * CDec(tVenta.Rows(0)("TC")), 2)), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)

        End If
        If (CDec(tVenta.Rows(0)("TARJETA") > 0)) Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Tarjeta Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tVenta.Rows(0)("TARJETA")), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If
        
        If ((CDec(tVenta.Rows(0)("EFECTIVO")) > 0) And (CDec(tVenta.Rows(0)("TARJETA")) > 0) And (CDec(tVenta.Rows(0)("DOLARES")) > 0) Or _
            (CDec(tVenta.Rows(0)("EFECTIVO")) > 0) And (CDec(tVenta.Rows(0)("DOLARES")) > 0) Or _
            (CDec(tVenta.Rows(0)("EFECTIVO")) > 0) And (CDec(tVenta.Rows(0)("TARJETA")) > 0) Or _
            (CDec(tVenta.Rows(0)("TARJETA")) > 0) And (CDec(tVenta.Rows(0)("DOLARES")) > 0)) Then

            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Pagado Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tVenta.Rows(0)("PAGADO")), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If

        Dim cambio = (CDec(tVenta.Rows(0)("TOTAL_PEDIDO")) - CDec(tVenta.Rows(0)("TOTAL_DESCUENTO")) - CDec(tVenta.Rows(0)("PAGADO"))) * (-1)
        If cambio >= 0 Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Cambio Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + cambio.ToString("#,#0.00"), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If        

        Dim importeCredito As Decimal = 0
        If (CBool(tVenta.Rows(0)("CREDITO"))) Then
            CurrentY = CurrentY + InvSubTitleHeight

            FieldValue = "Vta. al Credito Bs.:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)
            importeCredito = obtenerImporteCredito(idPedido)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", importeCredito.ToString), 27)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
        End If

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

        CurrentY = CurrentY + 4
        Dim Total = CDec(tVenta.Rows(0)("TOTAL_PEDIDO")) - CDec(tVenta.Rows(0)("TOTAL_DESCUENTO"))
        FieldValue = "Son:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)
        FieldValue = New modulo().EnLetras(Total.ToString)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 50, CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

        Dim Mensaje As String
        Dim heightMensaje = Convert.ToInt32(fuenteMensaje.GetHeight(g))
        If tVenta.Rows(0)("DETALLE").ToString.ToUpper <> "RECIBO" Then
            CurrentY = CurrentY + 4
            FieldValue = "Código de Control:" + tVenta.Rows(0)("CODIGO_CONTROL")
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)
            CurrentY = CurrentY + InvSubTitleHeight + 4
            FieldValue = "Fecha Limite Emisión:"
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)
            FieldValue = tVenta.Rows(0)("F_LIMITE_EMISION")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)
            CurrentY = CurrentY + InvSubTitleHeight

            'Dim oInvImage As Bitmap = New Bitmap(tVenta.Rows(0)("CODIGO_QR"))
            ' Set Image Left to center Image:
            Dim nuevaImagen As Image
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(CType(tVenta.Rows(0)("CODIGO_QR"), Byte()))
            nuevaImagen = System.Drawing.Image.FromStream(ms)

            Dim ImageHeight = 100
            g.DrawImage(nuevaImagen, 100, CurrentY, 100, 100)

            nuevaImagen.Dispose()


            CurrentY = CurrentY + ImageHeight
            Mensaje = Chr(34) + "ESTA FACTURA CONTRIBUYE AL"

            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, alinearCentro(g, fuenteMensaje, Mensaje), CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje
            Mensaje = "DESARROLLO DEL PAIS. EL USO ILICITO DE"

            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, alinearCentro(g, fuenteMensaje, Mensaje), CurrentY)
            CurrentY = CurrentY + heightMensaje

            Mensaje = "ESTA SERA SANCIONADO DE ACUERDO A LEY" + Chr(34)
            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, alinearCentro(g, fuenteMensaje, Mensaje), CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje
            g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
            CurrentY = CurrentY + 4            
            Mensaje = "LEY Nro. 453: Esta prohibido importar, distribuir o "
            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, 10, CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje
            Mensaje = "comercializar productos prohibidos o retirados en "
            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, 10, CurrentY)

            CurrentY = CurrentY + heightMensaje
            Mensaje = "el pais de origen por atentar  a la integridad fisica"
            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, 10, CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje
            Mensaje = "y a la salud."
            g.DrawString(Mensaje, fuenteMensaje, BlackBrush, 10, CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje

            g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
            'CurrentY = CurrentY + InvSubTitleHeight + 20
            CurrentY = CurrentY + heightMensaje
        End If
        'CurrentY = CurrentY + InvSubTitleHeight
        Mensaje = Chr(34) + "No se aceptan cambios ni devoluciones" + Chr(34)
        g.DrawString(Mensaje, InvSubTitleFontBold, BlackBrush, alinearCentro(g, InvSubTitleFontBold, Mensaje), CurrentY)
        If tVenta.Rows(0)("DETALLE").ToString.ToUpper = "RECIBO" Then
            CurrentY = CurrentY + InvSubTitleHeight
            Mensaje = Chr(34) + "NO VALIDO PARA CREDITO FISCAL"
            g.DrawString(Mensaje, InvSubTitleFontBold, BlackBrush, alinearCentro(g, InvSubTitleFontBold, Mensaje), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 75) + tVenta.Rows(0)("USUARIO"), 75)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
    End Sub
    'Public Sub SetInvoiceHead(ByVal g As Graphics)
    '    datosCabecera()
    '    CurrentY = topMargin
    '    CurrentX = leftMargin
    '    Dim ImageHeight As Integer = 0
    '    InvTitleHeight = Convert.ToInt32(FontCabecera.GetHeight(g))
    '    InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
    '    ' Get Titles Length:
    '    Dim lenInvTitle As Integer = Convert.ToInt32(g.MeasureString(InvTitle, FontCabecera).Width)
    '    Dim lenInvSubTitle1 As Integer = Convert.ToInt32(g.MeasureString(InvTitle, FontCabecera).Width)
    '    Dim lenInvSubTitle2 As Integer = Convert.ToInt32(g.MeasureString(InvSubTitle2, InvSubTitleFont).Width)
    '    Dim lenInvSubTitle3 As Integer = Convert.ToInt32(g.MeasureString(InvSubTitle3, InvSubTitleFont).Width)
    '    ' Set Titles Left:
    '    Dim xInvTitle As Integer = CurrentX + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    Dim xInvSubTitle1 As Integer = CurrentX + 10 + (InvoiceWidth - lenInvSubTitle1) / 2
    '    Dim xInvSubTitle2 As Integer = CurrentX + 10 + (InvoiceWidth - lenInvSubTitle2) / 2
    '    Dim xInvSubTitle3 As Integer = CurrentX + 10 + (InvoiceWidth - lenInvSubTitle3) / 2

    '    ' Draw Invoice Head:
    '    If (InvSubTitle1 <> "") Then
    '        g.DrawString(InvSubTitle1, FontCabecera, BlackBrush, 95, CurrentY)
    '        CurrentY = CurrentY + InvTitleHeight
    '    End If
    '    If (InvTitle <> "") Then
    '        g.DrawString(InvTitle, FontCabecera, BlackBrush, 80, CurrentY)
    '        CurrentY = CurrentY + InvTitleHeight
    '    End If
    '    If (InvSubTitle2 <> "") Then
    '        CurrentY = CurrentY + InvTitleHeight
    '        g.DrawString(InvSubTitle2, FontCabecera, BlackBrush, xInvSubTitle2, CurrentY)
    '    End If
    '    If (InvSubTitle3 <> "") Then
    '        CurrentY = CurrentY + InvSubTitleHeight
    '        g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, xInvSubTitle3, CurrentY)
    '    End If

    '    ' Draw line:
    '    'CurrentY = CurrentY + InvSubTitleHeight + 8
    '    'g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
    '    'CurrentY = CurrentY + InvSubTitleHeight
    '    'Dim texto As String = "RECIBO"
    '    'lenInvTitle = Convert.ToInt32(g.MeasureString(texto.ToUpper, InvTitleFont).Width)
    '    'xInvTitle = CurrentX + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    'g.DrawString(texto, InvTitleFont, BlueBrush, xInvTitle, CurrentY)
    '    'CurrentY = CurrentY + InvSubTitleHeight + 20
    '    'g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
    'End Sub
    'Public Sub SetOrderData(ByVal g As Graphics)
    '    ' Set Company Name, City, Salesperson, Order ID and Order Date
    '    Dim FieldValue As String = ""
    '    Dim espaciado As Integer = 135
    '    InvoiceFontHeight = Convert.ToInt32(InvoiceFont.GetHeight(g))
    '    ' Set Order ID:
    '    CurrentX = 10        
    '    FieldValue = "Nro de Pedido:"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, CurrentX, CurrentY)

    '    FieldValue = tVenta.Rows(0)("NRO_PEDIDO")
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, espaciado, CurrentY)

    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = tVenta.Rows(0)("TIPO_ATENCION")
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 85, CurrentY)
    '    If tVenta.Rows(0)("TIPO_ATENCION") = "Pedido Para la Mesa" Then
    '        CurrentY = CurrentY + InvoiceFontHeight
    '        FieldValue = "Mesa Nro: " + tVenta.Rows(0)("NRO_MESA").ToString
    '        g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 100, CurrentY)
    '    End If


    '    'CurrentY = CurrentY + InvoiceFontHeight + 8
    '    'g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
    '    espaciado = 67
    '    CurrentX = 10
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = "Fecha:"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, CurrentX, CurrentY)
    '    FieldValue = tVenta.Rows(0)("FECHA")
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, espaciado, CurrentY)
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = "Nombre:"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, CurrentX, CurrentY)
    '    Dim aux As String = ""
    '    If tVenta.Rows(0)("ID_CLIENTE") = 1 Then
    '        aux = tVenta.Rows(0)("OBSERVACION")
    '    Else
    '        aux = tVenta.Rows(0)("NOMBRE")
    '    End If
    '    Dim longNombre As Integer = Len(aux)
    '    Dim inicio = 1
    '    While longNombre > 0
    '        FieldValue = Mid(aux, inicio, 30)
    '        g.DrawString(FieldValue, InvoiceFont, BlackBrush, espaciado, CurrentY)
    '        CurrentY = CurrentY + InvoiceFontHeight
    '        inicio = inicio + 30
    '        longNombre = longNombre - 30
    '    End While
    '    'g.DrawString(FieldValue, InvoiceFont, BlackBrush, espaciado, CurrentY)

    '    FieldValue = "NIT/CI:"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, CurrentX, CurrentY)
    '    FieldValue = tVenta.Rows(0)("NIT")
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, espaciado, CurrentY)
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    Dim dashValues As Single() = {1, 2}
    '    Dim blackPen As New Pen(Color.Black, 2)
    '    blackPen.DashPattern = dashValues
    '    g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    '    CurrentY = CurrentY + InvoiceFontHeight - 15
    '    FieldValue = "Cant."
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 10, CurrentY)

    '    FieldValue = "Detalle"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 100, CurrentY)

    '    FieldValue = "P. Unit."
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 190, CurrentY)

    '    FieldValue = "Total"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 245, CurrentY)

    '    CurrentY = CurrentY + InvoiceFontHeight
    '    g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)        


    '    'CurrentY = CurrentY + InvoiceFontHeight


    '    'Dim lenInvTitle = Convert.ToInt32(g.MeasureString(FieldValue, InvTitleFont).Width)
    '    'Dim xInvTitle = leftMargin + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    'g.DrawString(FieldValue, InvTitleFont, BlueBrush, xInvTitle, CurrentY)

    '    'CurrentY = CurrentY + InvoiceFontHeight + 8
    '    'g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    'End Sub
    'Public Sub SetInvoiceData(ByVal g As Graphics, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
    '    ' Set Invoice Table:
    '    Dim FieldValue As String = ""
    '    Dim total As Decimal = 0
    '    Dim subTotal As Decimal
    '    Dim fuenteDetalle As Font = New Font("Arial", 8, FontStyle.Regular)
    '    InvoiceFontHeight = Convert.ToInt32(fuenteDetalle.GetHeight(g))
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    For Each row In tVenta.Rows
    '        FieldValue = row("CANTIDAD")
    '        g.DrawString(FieldValue, fuenteDetalle, BlackBrush, 15, CurrentY)

    '        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 8) + row("PRECIO_UNITARIO").ToString, 8)
    '        g.DrawString(FieldValue, fuenteDetalle, BlackBrush, 190, CurrentY)

    '        FieldValue = row("DETALLE")
    '        g.DrawString(FieldValue, fuenteDetalle, BlackBrush, 50, CurrentY)

    '        subTotal = (CDec(row("PRECIO_UNITARIO")) * CDec(row("CANTIDAD")))
    '        total = total + subTotal
    '        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
    '        g.DrawString(FieldValue, fuenteDetalle, BlackBrush, 230, CurrentY)
    '        CurrentY = CurrentY + InvoiceFontHeight
    '    Next
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    Dim dashValues As Single() = {1, 2}
    '    Dim blackPen As New Pen(Color.Black, 2)
    '    blackPen.DashPattern = dashValues
    '    g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    '    SetInvoiceTotal(g, total)
    '    g.Dispose()
    'End Sub
    'Public Sub imprimirComprobanteAtencion(ByVal g As Graphics)
    '    Dim FieldValue As String = ""
    '    CurrentX = 10
    '    CurrentY = 10
    '    InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
    '    InvoiceFontHeight = Convert.ToInt32(InvoiceFont.GetHeight(g))
    '    'Dim texto As String = "ATENCION"
    '    'Dim lenInvTitle = Convert.ToInt32(g.MeasureString(texto.ToUpper, InvTitleFont).Width)
    '    'Dim xInvTitle = CurrentX + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    'g.DrawString(texto, InvTitleFont, BlueBrush, 120, CurrentY)
    '    'CurrentY = CurrentY + InvTitleHeight

    '    Dim dashValues As Single() = {1, 2}
    '    Dim blackPen As New Pen(Color.Black, 2)
    '    blackPen.DashPattern = dashValues
    '    'g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    '    'CurrentY = CurrentY + 8

    '    FieldValue = "Nro de Pedido:"
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, CurrentX, CurrentY)
    '    FieldValue = tVenta.Rows(0)("NRO_PEDIDO")        
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, 120, CurrentY)

    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = tVenta.Rows(0)("TIPO_ATENCION")
    '    g.DrawString(FieldValue, FontCabecera, BlackBrush, 30, CurrentY)
    '    CurrentY = CurrentY + FontCabecera.Height
    '    If tVenta.Rows(0)("TIPO_ATENCION") = "Pedido Para la Mesa" Then
    '        FieldValue = "Mesa Nro: " + tVenta.Rows(0)("NRO_MESA").ToString
    '        g.DrawString(FieldValue, FontCabecera, BlackBrush, 65, CurrentY)
    '        CurrentY = CurrentY + FontCabecera.Height
    '    End If
    '    g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    '    Dim fuenteDetalle As Font = New Font("Arial", 11, FontStyle.Regular)
    '    InvoiceFontHeight = Convert.ToInt32(fuenteDetalle.GetHeight(g))
    '    For Each row In tVenta.Rows
    '        CurrentY = CurrentY + InvoiceFontHeight
    '        FieldValue = row("CANTIDAD")
    '        g.DrawString(FieldValue, fuenteDetalle, Brushes.Black, 20, CurrentY)
    '        FieldValue = row("DETALLE")
    '        g.DrawString(FieldValue, fuenteDetalle, Brushes.Black, 50, CurrentY)
    '    Next
    '    CurrentY = CurrentY + InvoiceFontHeight + 8
    '    g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    '    g.Dispose()
    'End Sub

    'Private Sub SetInvoiceTotal(ByVal g As Graphics, ByVal Total As Decimal)
    '    Dim FieldValue As String = ""
    '    CurrentY = CurrentY + 10
    '    FieldValue = "Total Bs.:  "
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 125, CurrentY)
    '    'FieldValue = Microsoft.VisualBasic.Right(New String(" ", 25) + Total.ToString, 25)
    '    FieldValue = Total.ToString
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, alinearDerecha(g, InvoiceFont, FieldValue, 150, 280), CurrentY)
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = "Efectivo Bs.:"
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 125, CurrentY)
    '    If pagado < 0 Then
    '        pagado = Total
    '    End If
    '    'FieldValue =   Microsoft .VisualBasic.Right(New String(" ", 25) + pagado.ToString("#0.00"), 25)
    '    FieldValue = pagado.ToString("#0.00")
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, alinearDerecha(g, InvoiceFont, FieldValue, 150, 280), CurrentY)
    '    CurrentY = CurrentY + InvoiceFontHeight
    '    FieldValue = "Cambio Bs.:  "
    '    g.DrawString(FieldValue, InvoiceFontBold, BlackBrush, 125, CurrentY)
    '    Dim cambio = pagado - Total
    '    'FieldValue = Microsoft.VisualBasic.Right(New String(" ", 25) + cambio.ToString("#0.00"), 25)
    '    FieldValue = cambio.ToString("#0.00")
    '    'g.DrawString(FieldValue, InvoiceFont, BlackBrush, 150, CurrentY)
    '    g.DrawString(FieldValue, InvoiceFont, BlackBrush, alinearDerecha(g, InvoiceFont, FieldValue, 150, 280), CurrentY)
    '    'CurrentY = CurrentY + InvoiceFontHeight + 8
    '    'g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

    '    'Dim Mensaje As String
    '    'Dim xInvTitle
    '    'Dim lenInvTitle
    '    'Dim FontMensajeBold As Font = New Font("Arial", 8, FontStyle.Bold)
    '    'CurrentY = CurrentY + InvoiceFontHeight
    '    'Mensaje = Chr(34) + "NO VALIDO PARA CREDITO FISCAL"
    '    'lenInvTitle = Convert.ToInt32(g.MeasureString(Mensaje, FontMensajeBold).Width)
    '    'xInvTitle = leftMargin + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    'g.DrawString(Mensaje, FontMensajeBold, BlueBrush, xInvTitle, CurrentY)        

    '    'Dim FontMensaje As Font = New Font("Arial", 8, FontStyle.Italic)
    '    'CurrentY = CurrentY + InvoiceFontHeight
    '    'Mensaje = "No se aceptan cambios ni devoluciones"
    '    'lenInvTitle = Convert.ToInt32(g.MeasureString(Mensaje, FontMensaje).Width)
    '    'xInvTitle = leftMargin + 10 + (InvoiceWidth - lenInvTitle) / 2
    '    'g.DrawString(Mensaje, FontMensaje, BlueBrush, xInvTitle, CurrentY)
    'End Sub
    Public Sub imprimirApertura(ByVal g As Graphics, ByVal idCaja As String)
        Dim tApertura = New generico().obtenerApertura(idCaja)
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 10, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 10, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL
        Dim dashValues As Single() = {1, 2}
        Dim blackPen As New Pen(Color.Black, 2)
        blackPen.DashPattern = dashValues

        Dim FieldValue As String = ""
        CurrentX = 10
        CurrentY = 10

        FieldValue = "APERTURA DE CAJA"
        g.DrawString(FieldValue, fteTitulo, BlackBrush, alinearCentro(g, fteTitulo, FieldValue), CurrentY)
        CurrentY = CurrentY + tituloHeight

        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + InvoiceFontHeight + 8

        Dim puntoMedio = CurrentX + Convert.ToInt32(g.MeasureString("Fecha de Apertura: ", fteNormalNegrita).Width)
        FieldValue = "Usuario:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tApertura.Rows(0)("NOMBRE_APELLIDOS")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha de Apertura:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tApertura.Rows(0)("F_APERTURA")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Importe:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tApertura.Rows(0)("IMPORTE_APERTURA")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    End Sub
    Function alinearDerecha(ByVal g As Graphics, ByVal fuente As Font, ByVal importe As String, ByVal posicionInicial As Integer, ByVal anchoImporte As Integer)
        Return (anchoImporte - Convert.ToInt32(g.MeasureString(importe, fuente).Width))
    End Function
    Function alinearCentro(ByVal g As Graphics, ByVal fuente As Font, ByVal texto As String)
        Dim LongTitulo As Integer = Convert.ToInt32(g.MeasureString(texto, fuente).Width)
        Dim anchoHoja = 285
        Dim xCentro As Integer = CurrentX + (anchoHoja - LongTitulo) / 2
        Return xCentro
    End Function

    Sub imprimirCierre(ByVal g As Graphics, ByVal idCaja As String)
        Dim tCierre = New generico().obtenerCierreCaja(idCaja)
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 10, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 10, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL
        Dim dashValues As Single() = {1, 2}
        Dim blackPen As New Pen(Color.Black, 2)
        blackPen.DashPattern = dashValues

        Dim FieldValue As String = ""
        CurrentX = 10
        CurrentY = 10

        FieldValue = "CIERRE DE CAJA"
        g.DrawString(FieldValue, fteTitulo, BlackBrush, alinearCentro(g, fteTitulo, FieldValue), CurrentY)
        CurrentY = CurrentY + tituloHeight

        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + InvoiceFontHeight + 8

        Dim puntoMedio = CurrentX + Convert.ToInt32(g.MeasureString("Fecha de Apertura: ", fteNormalNegrita).Width)
        FieldValue = "Usuario:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("NOMBRE_APELLIDOS")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha de Apertura:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("F_APERTURA")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha de Cierre:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("F_CIERRE")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight        

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 3

        FieldValue = "VENTAS:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        CurrentY = CurrentY + normalHeight



        FieldValue = "Total Credito:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("TOTAL_CREDITO")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight


        FieldValue = "Total Efectivo:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("TOTAL_EFECTIVO")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + normalHeight + 3

        FieldValue = "MOVIMIENTO ECONOMICO:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Importe Apertura:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("IMPORTE_APERTURA_MN")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Vtas en Efectivo:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("TOTAL_EFECTIVO")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Ctas. Por Cobrar:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("TOTAL_CTA_X_COBRAR")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight
        g.DrawLine(New Pen(Brushes.Black, 2), 200, CurrentY, 280, CurrentY)
        CurrentY = CurrentY + 3

        Dim totalSistema As Decimal = CDec(tCierre.Rows(0)("IMPORTE_APERTURA_MN")) + CDec(tCierre.Rows(0)("TOTAL_EFECTIVO")) + CDec(tCierre.Rows(0)("TOTAL_CTA_X_COBRAR"))
        FieldValue = "Total:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = totalSistema
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, alinearDerecha(g, fteNormalNegrita, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight * 2

        FieldValue = "Transferencia:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("TOTAL_TRANSFERENCIA") * (-1)
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Efectivo en Caja (Cierre):"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("IMPORTE_CIERRE_MN")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearDerecha(g, fteNormal, FieldValue, puntoMedio, 280), CurrentY)
        CurrentY = CurrentY + normalHeight

        g.DrawLine(New Pen(Brushes.Black, 2), 200, CurrentY, 280, CurrentY)
        CurrentY = CurrentY + 3

        Dim totalCaja As Decimal = (CDec(tCierre.Rows(0)("TOTAL_TRANSFERENCIA")) * (-1)) + CDec(tCierre.Rows(0)("IMPORTE_CIERRE_MN"))
        FieldValue = "Total:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = totalCaja
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, alinearDerecha(g, fteNormalNegrita, FieldValue, puntoMedio, 280), CurrentY)

        CurrentY = CurrentY + normalHeight

        Dim ajuste As Decimal = totalCaja - totalSistema
        If ajuste <> 0 Then
            CurrentY = CurrentY + normalHeight
            FieldValue = IIf(ajuste > 0, "Ajuste por Sobrante:", "Ajuste por Faltante:")
            g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
            FieldValue = ajuste
            g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, alinearDerecha(g, fteNormalNegrita, FieldValue, puntoMedio, 280), CurrentY)
            CurrentY = CurrentY + normalHeight
        End If

        CurrentY = CurrentY + normalHeight
        FieldValue = "Recepciono:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = tCierre.Rows(0)("USUARIO_RECEPCION")
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, alinearDerecha(g, fteNormalNegrita, FieldValue, puntoMedio, 280), CurrentY)

        CurrentY = CurrentY + (normalHeight * 3)
        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
    End Sub
    Private Function obtenerImporteCredito(ByVal idVenta As Integer) As Decimal
        Dim sSql As String = ""
        sSql = "SELECT IMPORTE_CREDITO FROM CREDITO WHERE ID_VENTA =" + idVenta.ToString
        Return New datos().ejecutarConsulta(sSql).Rows(0)(0)
    End Function

    Public Sub imprimireExistenciaLaboratorio(ByVal g As Graphics, ByVal Laboratorio As String, ByVal idLaboratorio As Integer, ByVal cantCero As Boolean, ByVal cantNeg As Boolean, ByVal detLote As Boolean)
        Dim tblMedicamentos = New generico().obtenerExistenciaLaboratorios(idLaboratorio, cantCero, cantNeg)
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL
        Dim dashValues As Single() = {1, 2}
        Dim blackPen As New Pen(Color.Black, 2)
        blackPen.DashPattern = dashValues

        Dim FieldValue As String = ""
        CurrentX = 10
        CurrentY = 10

        FieldValue = "EXISTENCIA POR LABORATORIO"
        g.DrawString(FieldValue, fteTitulo, BlackBrush, alinearCentro(g, fteTitulo, FieldValue), CurrentY)
        CurrentY = CurrentY + tituloHeight

        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + InvoiceFontHeight + 8

        Dim puntoMedio = CurrentX + Convert.ToInt32(g.MeasureString("Laboratorio: ", fteNormalNegrita).Width)
        FieldValue = "Fecha:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = Now.ToString("dd/MM/yyyy")
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Laboratorio:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        FieldValue = Laboratorio
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight        

        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)

        g.DrawLine(New Pen(Brushes.Black, 2), 50, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Medicamento"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = "Cant."
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 220, CurrentY)

        FieldValue = "Cont."
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 255, CurrentY)


        Dim vectorString As String() = {"ID_MEDICAMENTO", "MEDICAMENTO", "CANTIDAD"}
        Dim results = New modulo().SelectDistinct(vectorString, tblMedicamentos)
        CurrentY = CurrentY + normalHeight
        For Each item In results.Select()

            If Len(item("MEDICAMENTO")) > 30 Then
                FieldValue = Microsoft.VisualBasic.Left(item("MEDICAMENTO"), 30) + "..."
            Else
                FieldValue = item("MEDICAMENTO")
            End If
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + item("CANTIDAD").ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 210, CurrentY)
            If detLote Then
                FieldValue = ""
            Else
                FieldValue = "     _____"
            End If

            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 240, CurrentY)

            CurrentY = CurrentY + normalHeight

            If detLote Then
                For Each lote In tblMedicamentos.Select("ID_MEDICAMENTO =" + item("ID_MEDICAMENTO").ToString)
                    If CBool(lote("SIN_LOTE")) Then
                        FieldValue = "Sin Lote"
                    Else
                        FieldValue = lote("NRO_LOTE").ToString + " - " + lote("F_VENCIMIENTO").ToString
                    End If

                    g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 20, CurrentY)
                    FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + lote("CANTIDAD_LOTE").ToString, 10)
                    g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

                    FieldValue = "     _____"
                    g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 240, CurrentY)
                    CurrentY = CurrentY + normalHeight
                    g.DrawLine(blackPen, 20, CurrentY, 265, CurrentY)
                    CurrentY = CurrentY + 5
                Next
            End If

        Next
        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        
    End Sub
    Public Sub imprimirRecetaMedica(ByVal g As Graphics, ByVal receta As String)
        Dim gen = New generico()
        Dim tblMedicamentos = gen.obtenerRecetaIndicaciones(receta)
        Dim tblMedico = gen.obtenerDatosMedico(tblMedicamentos.Rows(0)("ID_MEDICO"))
        conf = New eConfiguracion()
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL

        datosCabecera()
        InvSubTitle1 = "Consultorio Medico"
        CurrentY = topMargin
        CurrentX = leftMargin
        InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight

        ' Draw line:
        Dim FieldValue As String = ""
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        FieldValue = "I N D I C A C I O N E S"
        g.DrawString(FieldValue, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, FieldValue), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2

        For Each item In tblMedicamentos.Rows
            FieldValue = "Med: " + item("NOMBRE")
            g.DrawString(FieldValue, fteNormal, BlackBrush, 10, CurrentY)
            CurrentY = CurrentY + normalHeight
            FieldValue = item("INDICACIONES")
            g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, 10, CurrentY)
            CurrentY = CurrentY + normalHeight * 2
        Next
        Dim nuevaImagen As Image
        Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream(CType(tblMedico.Rows(0)("FIRMA"), Byte()))
        nuevaImagen = System.Drawing.Image.FromStream(ms)
        Dim ImageHeight = 70
        g.DrawImage(nuevaImagen, 40, CurrentY, 200, 70)
        nuevaImagen.Dispose()
        CurrentY = CurrentY + 50
        FieldValue = tblMedico.Rows(0)("NOMBRE")
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, alinearCentro(g, fteNormalNegrita, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = tblMedico.Rows(0)("MATRICULA")
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        FieldValue = "ATENCIÓN GRATUITA"
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Lunes a Viernes de 16:00 a 20 hrs."
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
    End Sub
    Public Sub imprimirVoucherCredito(ByVal g As Graphics, ByVal venta As String)
        Dim gen = New generico()
        Dim tblVentaCredito As DataTable = gen.obtenerVentasCredito(venta)
        conf = New eConfiguracion()
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL

        datosCabecera()
        CurrentY = topMargin
        CurrentX = leftMargin
        InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight

        ' Draw line:
        Dim FieldValue As String = ""
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        FieldValue = "VENTAS AL CREDITO"
        g.DrawString(FieldValue, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, FieldValue), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "NOTA NRO. " + tblVentaCredito.Rows(0)("NRO_CREDITO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha: " + tblVentaCredito.Rows(0)("FECHA").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight * 2
        FieldValue = "Nombre: " + tblVentaCredito.Rows(0)("NOMBRE_APELLIDOS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Direccion: " + tblVentaCredito.Rows(0)("DIRECCION").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Cliente: " + tblVentaCredito.Rows(0)("GRUPO_PERSONAS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Cantidad"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = "P. Unitario"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 100, CurrentY)

        FieldValue = "SubTotal"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 210, CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "Detalle"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, alinearCentro(g, InvSubTitleFontBold, FieldValue), CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)


        FieldValue = ""
        CurrentY = CurrentY + 2
        Dim total As Decimal = 0
        Dim subTotal As Decimal
        For Each row In tblVentaCredito.Rows
            FieldValue = row("CANTIDAD")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 20, CurrentY)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + row("PRECIO_UNITARIO").ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 100, CurrentY)
            subTotal = (CDec(row("PRECIO_UNITARIO")) * CDec(row("CANTIDAD")))
            total = total + subTotal
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight
            If Len(row("DESC_MEDICAMENTO")) > 35 Then
                FieldValue = Microsoft.VisualBasic.Left(row("DESC_MEDICAMENTO"), 35) + "..."
            Else
                FieldValue = row("DESC_MEDICAMENTO")
            End If

            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight
        Next
        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)


        CurrentY = CurrentY + 2

        FieldValue = "Sub Total Bs.:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 27) + String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblVentaCredito.Rows(0)("TOTAL_PEDIDO")), 27)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 150, CurrentY)


        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

        CurrentY = CurrentY + 4
        total = CDec(tblVentaCredito.Rows(0)("TOTAL_PEDIDO"))
        FieldValue = "Son:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)
        FieldValue = New modulo().EnLetras(Total.ToString)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 50, CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight * 5

        FieldValue = tblVentaCredito.Rows(0)("NOMBRE_APELLIDOS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)

        CurrentY = CurrentY + normalHeight
        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 75) + tblVentaCredito.Rows(0)("USUARIO"), 75)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
    End Sub

    Public Sub imprimirVoucherPagoCredito(ByVal g As Graphics, ByVal pago As String)
        Dim gen = New generico()
        Dim tbls As DataSet = gen.obtenerDetallePago(pago)
        Dim tblRecibos = tbls.Tables(0)
        Dim tblPago = tbls.Tables(1)

        conf = New eConfiguracion()
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL

        datosCabecera()
        CurrentY = topMargin
        CurrentX = leftMargin
        InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight

        ' Draw line:
        Dim FieldValue As String = ""
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        FieldValue = "COMPROBANTE DE COBRANZA"
        g.DrawString(FieldValue, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, FieldValue), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        
        FieldValue = "Pago Nro: " + tblPago.Rows(0)("NRO_PAGO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha de Pago: " + tblPago.Rows(0)("FECHA").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Nombre: " + tblPago.Rows(0)("NOMBRE_APELLIDOS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Cliente: " + tblPago.Rows(0)("GRUPO_PERSONAS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Tipo Pago: " + tblPago.Rows(0)("DESC_TIPO_PAGO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Ventas Cobradas: "
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Nro. Comp."
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = "Fecha Comp."
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 100, CurrentY)

        FieldValue = "Importe"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 210, CurrentY)

        CurrentY = CurrentY + normalHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)


        FieldValue = ""
        CurrentY = CurrentY + 2
        Dim total As Decimal = 0
        Dim subTotal As Decimal
        For Each row In tblRecibos.Rows
            FieldValue = row("NRO_COMPROBANTE")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 15, CurrentY)

            FieldValue = row("FECHA")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 105, CurrentY)
            
            subTotal = CDec(row("IMPORTE"))

            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

            CurrentY = CurrentY + normalHeight            
        Next
        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)


        CurrentY = CurrentY + 2

        FieldValue = "Total Pago Credito Bs.:"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        subTotal = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblPago.Rows(0)("IMPORTE_PAGO"))
        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

        'CurrentY = CurrentY + normalHeight

        'FieldValue = "Total Cancelado Bs.:"
        'g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        'subTotal = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblPago.Rows(0)("IMPORTE_PAGO"))
        'FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
        'g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)        

        'CurrentY = CurrentY + normalHeight

        'FieldValue = "Cambio Bs.:"
        'g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        'subTotal = String.Format(New Globalization.CultureInfo("en-US"), "{0:#,##0.00}", tblPago.Rows(0)("CAMBIO"))
        'FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
        'g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

        CurrentY = CurrentY + normalHeight

        'CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

        CurrentY = CurrentY + 2
        

        CurrentY = CurrentY + normalHeight
        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 75) + tblPago.Rows(0)("USUARIO"), 75)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
    End Sub

    Public Sub imprimirVencimientos(ByVal g As Graphics, ByVal desde As String, ByVal hasta As String)
        Dim tblMedicamentos As DataTable = New generico().obtenerMedicamentosVencidos(desde, hasta)
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL
        Dim dashValues As Single() = {1, 2}
        Dim blackPen As New Pen(Color.Black, 2)
        blackPen.DashPattern = dashValues

        Dim FieldValue As String = ""
        CurrentX = 10
        CurrentY = 10

        FieldValue = "VENCIMIENTOS"
        g.DrawString(FieldValue, fteTitulo, BlackBrush, alinearCentro(g, fteTitulo, FieldValue), CurrentY)
        CurrentY = CurrentY + tituloHeight

        g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + InvoiceFontHeight + 8

        Dim puntoMedio = CurrentX + Convert.ToInt32(g.MeasureString("Desde: ", fteNormalNegrita).Width)
        FieldValue = "Desde:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)
        
        FieldValue = desde
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight

        FieldValue = "Hasta:"
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, CurrentX, CurrentY)

        FieldValue = hasta
        g.DrawString(FieldValue, fteNormal, BlackBrush, puntoMedio, CurrentY)
        CurrentY = CurrentY + normalHeight + 2


        'g.DrawLine(blackPen, 10, CurrentY, 285, CurrentY)

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Medicamento"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        CurrentY = CurrentY + normalHeight
        For Each item In tblMedicamentos.Rows

            If Len(item("MEDICAMENTO")) > 40 Then
                FieldValue = Microsoft.VisualBasic.Left(item("MEDICAMENTO"), 40) + "..."
            Else
                FieldValue = item("MEDICAMENTO")
            End If
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)


            CurrentY = CurrentY + normalHeight


            FieldValue = item("NRO_LOTE").ToString + " - " + item("F_VENCIMIENTO").ToString

            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 20, CurrentY)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + item("TOTAL_STOCK").ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

            'FieldValue = "     _____"
            'g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 240, CurrentY)
            CurrentY = CurrentY + normalHeight
            g.DrawLine(blackPen, 20, CurrentY, 265, CurrentY)
            CurrentY = CurrentY + 5
        Next

        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

    End Sub

    Public Sub imprimirCotizacion(ByVal g As Graphics, ByVal idVenta As Integer)
        Dim gen = New generico()
        Dim tblVentaCredito As DataTable = gen.obtenerVentasCredito(idVenta)
        conf = New eConfiguracion()
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL

        datosCabecera()
        CurrentY = topMargin
        CurrentX = leftMargin
        InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight

        ' Draw line:
        Dim FieldValue As String = ""
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        FieldValue = "COTIZACION"
        g.DrawString(FieldValue, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, FieldValue), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "NOTA NRO. " + tblVentaCredito.Rows(0)("NRO_CREDITO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha: " + tblVentaCredito.Rows(0)("FECHA").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, alinearCentro(g, fteNormal, FieldValue), CurrentY)
        CurrentY = CurrentY + normalHeight * 2
        FieldValue = "Nombre: " + tblVentaCredito.Rows(0)("NOMBRE_APELLIDOS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        

        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2
        FieldValue = "Cantidad"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

        FieldValue = "P. Unitario"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 100, CurrentY)

        FieldValue = "SubTotal"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 210, CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight
        FieldValue = "Detalle"
        g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, alinearCentro(g, InvSubTitleFontBold, FieldValue), CurrentY)

        CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)


        CurrentY = CurrentY + 2
        Dim total As Decimal = 0
        Dim subTotal As Decimal
        For Each row In tVenta.Rows
            FieldValue = row("CANTIDAD")
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 20, CurrentY)
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + row("PRECIO_UNITARIO").ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 100, CurrentY)
            subTotal = (CDec(row("PRECIO_UNITARIO")) * CDec(row("CANTIDAD")))
            total = total + subTotal
            FieldValue = Microsoft.VisualBasic.Right(New String(" ", 10) + subTotal.ToString, 10)
            g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 210, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight
            If Len(row("DESC_MEDICAMENTO")) > 35 Then
                FieldValue = Microsoft.VisualBasic.Left(row("DESC_MEDICAMENTO"), 35) + "..."
            Else
                FieldValue = row("DESC_MEDICAMENTO")
            End If

            g.DrawString(FieldValue, InvSubTitleFontBold, BlackBrush, 10, CurrentY)

            CurrentY = CurrentY + InvSubTitleHeight

            If tipoVenta = 2 Then
                Dim tbl As DataTable = obtenerkardex(row("ID_MEDICAMENTO"), idPedido)
                For Each item In tbl.Rows
                    FieldValue = item("NRO_LOTE").ToString + " - " + item("F_VENCIMIENTO").ToString + " (" + item("CANTIDAD").ToString + ")"
                    g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
                    CurrentY = CurrentY + InvSubTitleHeight
                Next
                CurrentY = CurrentY + InvSubTitleHeight
            End If
        Next
        CurrentY = CurrentY + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        SetInvoiceTotal(g)
        g.Dispose()
    End Sub

    Public Sub imprimirVoucherConsultaCampanha(ByVal g As Graphics, ByVal consulta As String)
        Dim gen = New generico()
        Dim tblConsulta As DataTable = gen.obtenerDetalleConsultaCampanha(consulta)      

        conf = New eConfiguracion()
        'FUENTES
        Dim fteTitulo As Font = New Font("Arial", 11, FontStyle.Bold)
        Dim fteNormal As Font = New Font("Arial", 8, FontStyle.Regular)
        Dim fteNormalNegrita As Font = New Font("Arial", 9, FontStyle.Bold)
        'ESPACIADO
        Dim tituloHeight = Convert.ToInt32(fteTitulo.GetHeight(g))
        Dim normalHeight = Convert.ToInt32(fteNormal.GetHeight(g))
        'LINEA HORIZONTAL

        datosCabecera()
        CurrentY = topMargin
        CurrentX = leftMargin
        InvTitleHeight = Convert.ToInt32(InvTitleFont.GetHeight(g))
        InvSubTitleHeight = Convert.ToInt32(InvSubTitleFont.GetHeight(g))
        If (InvSubTitle1 <> "") Then
            CurrentY = CurrentY
            g.DrawString(InvSubTitle1, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle1), CurrentY)
        End If
        If (InvTitle <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvTitle, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, InvTitle), CurrentY)
        End If
        If (InvSubTitle2 <> "") Then
            CurrentY = CurrentY + InvTitleHeight
            g.DrawString(InvSubTitle2, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle2), CurrentY)
        End If
        If (InvSubTitle3 <> "") Then
            CurrentY = CurrentY + InvSubTitleHeight
            g.DrawString(InvSubTitle3, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, InvSubTitle3), CurrentY)
        End If
        CurrentY = CurrentY + InvSubTitleHeight
        Dim linea As String = "CAMIRI - SANTA CRUZ"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        CurrentY = CurrentY + InvSubTitleHeight
        linea = "De: Lic. Shirley A. Flores Flores"
        g.DrawString(linea, InvSubTitleFont, BlackBrush, alinearCentro(g, InvSubTitleFont, linea), CurrentY)
        ' Draw line:
        CurrentY = CurrentY + InvSubTitleHeight

        ' Draw line:
        Dim FieldValue As String = ""
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 4
        FieldValue = "RECIBO CONSULTA MEDICA"
        g.DrawString(FieldValue, InvTitleFont, BlackBrush, alinearCentro(g, InvTitleFont, FieldValue), CurrentY)
        CurrentY = CurrentY + InvTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)
        CurrentY = CurrentY + 2

        FieldValue = "Pago Nro: " + tblConsulta.Rows(0)("ID_CONSULTA").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Fecha de Pago: " + tblConsulta.Rows(0)("F_PAGO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Cliente: " + tblConsulta.Rows(0)("NOMBRE_APELLIDOS").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight * 2

        FieldValue = "Campaña Medica: " + tblConsulta.Rows(0)("NOMBRE_CAMPANHA").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Medico: " + tblConsulta.Rows(0)("NOMBRE_MEDICO").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        FieldValue = "Paciente: " + tblConsulta.Rows(0)("PACIENTE").ToString
        g.DrawString(FieldValue, fteNormal, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight * 2

        FieldValue = "Total Pago: " + tblConsulta.Rows(0)("TOTAL_CONSULTA").ToString
        g.DrawString(FieldValue, fteNormalNegrita, BlackBrush, 20, CurrentY)
        CurrentY = CurrentY + normalHeight
        'CurrentY = CurrentY + InvSubTitleHeight + 2
        g.DrawLine(New Pen(Brushes.Black, 2), 10, CurrentY, 285, CurrentY)

        CurrentY = CurrentY + 2


        CurrentY = CurrentY + normalHeight
        FieldValue = Microsoft.VisualBasic.Right(New String(" ", 75) + tblConsulta.Rows(0)("USUARIO"), 75)
        g.DrawString(FieldValue, InvSubTitleFont, BlackBrush, 10, CurrentY)
    End Sub
End Class
